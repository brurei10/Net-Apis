﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de protocolo de atendimento
    /// </summary>
    /// <returns><see cref="IProtocoloAtendimentoRepository"/></returns>
    public static class ProtocoloAtendimentoRepositoryMockHelper
    {
        public static IProtocoloAtendimentoRepository GetProtocoloAtendimentoRepository()
        {
            var protocoloAtendimento = GeraProtocoloAtendimento();
            var listaProtocolo = GeraListaProtocoloAtendimento();

            var mockProtocoloAtendimentoRepository = new Mock<IProtocoloAtendimentoRepository>();

            mockProtocoloAtendimentoRepository.Setup(m => m.NumeroProtocoloValidar(It.IsAny<string>())).Returns(new ValidationResult());
            mockProtocoloAtendimentoRepository.Setup(m => m.ObterPorNumeroAsync(It.IsAny<string>())).ReturnsAsync(protocoloAtendimento);
            mockProtocoloAtendimentoRepository.Setup(m => m.PodeIncluir(It.IsAny<ProtocoloAtendimento>())).Returns(new ValidationResult());
            mockProtocoloAtendimentoRepository.Setup(m => m.AtualizarIntegracaoGfaAsync(It.IsAny<ProtocoloAtendimento>())).ReturnsAsync(true);
            mockProtocoloAtendimentoRepository.Setup(m => m.FinalizarAsync(It.IsAny<ProtocoloAtendimento>())).ReturnsAsync(true);
            mockProtocoloAtendimentoRepository.Setup(m => m.GerarAsync(It.IsAny<ProtocoloAtendimento>())).ReturnsAsync(protocoloAtendimento.Numero);
            mockProtocoloAtendimentoRepository.Setup(m => m.NumeroProtocoloValidar(It.IsAny<string>())).Returns(new ValidationResult());
            mockProtocoloAtendimentoRepository.Setup(m => m.ListarProtocolosAbertosPorNumeroESenhaGfaAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(listaProtocolo);
            mockProtocoloAtendimentoRepository.Setup(m => m.PodeFinalizar(It.IsAny<ProtocoloAtendimento>())).Returns(new ValidationResult());

            return mockProtocoloAtendimentoRepository.Object;
        }

        #region [Metodos privados]
        private static ProtocoloAtendimento GeraProtocoloAtendimento()
        {
            return new ProtocoloAtendimento
            {
                Id = 264904,
                IdContrato = 278720,
                DataInicio = Convert.ToDateTime("10/07/2020 10:15:26"),
                DataFim = null,
                IdTipoAtendimento = CanalAtendimentoEnum.Whatsapp,
                Observacao = "PROTOCOLO GERADO POR WHATSAPP.",
                Solicitante = "JOSE GUIRAO SANCHES",
                IdLocalAtendimento = 0,
                Prexifo = "20200111",
                Sequencia = 213222,
                IdUsuario = 0,
                TipoProtocolo = 1,
                Matricula = "0100000004",
                Telefone = "1532813356 ",
                CpfCnpj = "07804814887",
                OrigemInova = false,
                NumeroProtocoloPai = null,
                DataIntegracao = null,
                IdProtocoloAtendimentoGfa = 0,
                Contrato = null,
                ProtocoloAtendimentoGfa = null,
                Usuario = null,
                ValidationResult = null
            };
        }

        private static IEnumerable<ProtocoloAtendimento> GeraListaProtocoloAtendimento()
        {
            return new List<ProtocoloAtendimento>
            {
                GeraProtocoloAtendimento()
            };
        }

        #endregion
    }
}