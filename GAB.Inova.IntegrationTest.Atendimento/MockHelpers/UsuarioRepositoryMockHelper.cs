﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de usuario
    /// </summary>
    /// <returns><see cref="IUsuarioRepository"/></returns>
    public static class UsuarioRepositoryMockHelper
    {
        public static IUsuarioRepository GetUsuarioRepository()
        {
            var usuario = GeraUsuario();

            var mockUsuarioRepository = new Mock<IUsuarioRepository>();

            mockUsuarioRepository.Setup(m => m.ObterPorNomeAcessoAsync(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(usuario);

            return mockUsuarioRepository.Object;
        }

        #region [Metodos privados]

        private static Usuario GeraUsuario()
        {
            return new Usuario
            {
                Id = 1,
                NomeAcesso = "teste.gab",
                Email = "teste@grupoaguasdobrasil.com.br",
                NomeCompleto = "Usuário para teste automatizado",
                Situacao = true,
                SituacaoAd = true
            };
        }

        #endregion
    }
}
