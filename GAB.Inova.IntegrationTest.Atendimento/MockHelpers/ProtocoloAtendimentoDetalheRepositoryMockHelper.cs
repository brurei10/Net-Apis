﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de detalhe de protocolo de atendimento
    /// </summary>
    /// <returns><see cref="IProtocoloAtendimentoDetalheRepository"/></returns>
    public static class ProtocoloAtendimentoDetalheRepositoryMockHelper
    {
        public static IProtocoloAtendimentoDetalheRepository GetProtocoloAtendimentoDetalheRepository()
        {
            var mockProtocoloAtendimentoDetalheRepository = new Mock<IProtocoloAtendimentoDetalheRepository>();

            mockProtocoloAtendimentoDetalheRepository.Setup(m => m.VerificarExistenciaDeNaturezaAsync(It.IsAny<long>(), It.IsAny<int>())).ReturnsAsync(false);
            mockProtocoloAtendimentoDetalheRepository.Setup(m => m.InserirAsync(It.IsAny<ProtocoloAtendimentoDetalhe>())).ReturnsAsync(true);

            return mockProtocoloAtendimentoDetalheRepository.Object;
        }
    }
}
