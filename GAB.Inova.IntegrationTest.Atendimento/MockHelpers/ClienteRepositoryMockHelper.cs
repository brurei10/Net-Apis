﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela cliente
    /// </summary>
    /// <returns><see cref="IClienteRepository"/></returns>
    public static class ClienteRepositoryMockHelper
    {
        public static IClienteRepository GetClienteRepository()
        {
            var cliente = GeraCliente();

            var mockClienteRepository = new Mock<IClienteRepository>();

            mockClienteRepository.Setup(m => m.ObterPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(cliente);

            return mockClienteRepository.Object;
        }

        #region [Metodos privados]

        private static Domain.Cliente GeraCliente()
        {
            return new Domain.Cliente
            {
                Localizacao = "11101022883263002050026002",
                DigitoLocalizacao = "6",
                Matricula = "1100050342",
                Nome = "CLIENTE PARA TESTE AUTOMATIZADO",
                IdTipoFatura = 0,
                IdTipoCliente = TipoClienteEnum.Normal,
                SituacaoAgua = 1,
                SituacaoEsgoto = 1,
                SituacaoHd = 3,
                DiaVencimentoConta = 0,
                DigitoMatricula = "3",
                DebitoAutomatico = false,
                IdGrupoEntrega = 0,
                Contrato = new Contrato
                {
                    Id = 244796,
                    CpfCnpj = "01614459770",
                    Matricula = "0100000004",
                    NomeCliente = "CLIENTE PARA TESTE AUTOMATIZADO",
                    TelefoneResidencial = "2137417944 ",
                    TelefoneComercial = null,
                    Celular = "21981275101",
                    Email = "teste@grupoaguasdobrasil.com.br",
                    EmailContaDigital = "teste@grupoaguasdobrasil.com.br",
                    Numero = "372",
                    Complemento = "QD30 LT",
                    PontoReferencia = "NT         ",
                    Logradouro = new Logradouro
                    {
                        Id = 2200,
                        Nome = "AMERICO ALVES COSTA",
                        Cep = "24350350",
                        Bairro = new Bairro
                        {
                            Id = 110,
                            Nome = "PIRATININGA"
                        },
                        Cidade = new Cidade
                        {
                            Id = 22932,
                            Nome = "R.O. - NITEROI"
                        },
                        Estado = new Estado
                        {
                            Id = 19,
                            Nome = "RIO DE JANEIRO"
                        },
                        LogradouroTipo = new LogradouroTipo
                        {
                            Id = 41,
                            Nome = "RUA"
                        }
                    }
                },
                ValidationResult = null
            };
        }

        #endregion
    }
}
