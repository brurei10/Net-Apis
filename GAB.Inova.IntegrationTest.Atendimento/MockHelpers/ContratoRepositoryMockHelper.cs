﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela contrato
    /// </summary>
    /// <returns><see cref="IContratoRepository"/></returns>
    public static class ContratoRepositoryMockHelper
    {
        public static IContratoRepository GetContratoRepository()
        {
            var contrato = GeraContrato();

            var mockContratoRepository = new Mock<IContratoRepository>();

            mockContratoRepository.Setup(m => m.ObterPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(contrato);

            return mockContratoRepository.Object;
        }

        #region [Metodos privados]

        private static Contrato GeraContrato()
        {
            return new Contrato
            {
                Id = 244796,
                CpfCnpj = "01614459770",
                Matricula = "0100000004",
                NomeCliente = "CLIENTE PARA TESTE AUTOMATIZADO",
                TelefoneResidencial = "2137417944 ",
                TelefoneComercial = null,
                Celular = "21981275101",
                Email = "teste@grupoaguasdobrasil.com.br",
                EmailContaDigital = "teste@grupoaguasdobrasil.com.br",
                Numero = "372",
                Complemento = "QD30 LT",
                PontoReferencia = "NT         ",
                Logradouro = new Logradouro
                {
                    Id = 2200,
                    Nome = "AMERICO ALVES COSTA",
                    Cep = "24350350",
                    Bairro = new Bairro
                    {
                        Id = 110,
                        Nome = "PIRATININGA"
                    },
                    Cidade = new Cidade
                    {
                        Id = 22932,
                        Nome = "R.O. - NITEROI"
                    },
                    Estado = new Estado
                    {
                        Id = 19,
                        Nome = "RIO DE JANEIRO"
                    },
                    LogradouroTipo = new LogradouroTipo
                    {
                        Id = 41,
                        Nome = "RUA"
                    }
                }
            };
        }

        #endregion
    }
}