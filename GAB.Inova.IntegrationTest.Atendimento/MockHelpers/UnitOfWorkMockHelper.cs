﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using Moq;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de conexão
    /// </summary>
    /// <returns><see cref="IUnitOfWork"/></returns>
    public static class UnitOfWorkMockHelper
    {
        public static IUnitOfWork GetUnitOfWork()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            return mockUnitOfWork.Object;
        }
    }
}
