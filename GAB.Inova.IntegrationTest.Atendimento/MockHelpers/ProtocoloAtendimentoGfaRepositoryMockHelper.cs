﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Atendimento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de protocolo de atendimento do GFA
    /// </summary>
    /// <returns><see cref="IProtocoloAtendimentoGfaRepository"/></returns>
    public static class ProtocoloAtendimentoGfaRepositoryMockHelper
    {
        public static IProtocoloAtendimentoGfaRepository GetProtocoloAtendimentoGfaRepository()
        {
            var protocoloAtendimentoGfa = GeraProtocoloAtendimentoGfa();

            var mockProtocoloAtendimentoGfaRepository = new Mock<IProtocoloAtendimentoGfaRepository>();

            mockProtocoloAtendimentoGfaRepository.Setup(m => m.PodeAlterar(It.IsAny<ProtocoloAtendimentoGfa>())).Returns(new ValidationResult());
            mockProtocoloAtendimentoGfaRepository.Setup(m => m.FinalizarAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);
            mockProtocoloAtendimentoGfaRepository.Setup(m => m.ObterNumeroProtocoloAtendimentoPaiAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(string.Empty);
            mockProtocoloAtendimentoGfaRepository.Setup(m => m.PodeIncluir(It.IsAny<ProtocoloAtendimentoGfa>())).Returns(new ValidationResult());
            mockProtocoloAtendimentoGfaRepository.Setup(m => m.InserirAsync(It.IsAny<ProtocoloAtendimentoGfa>())).ReturnsAsync(true);
            mockProtocoloAtendimentoGfaRepository.Setup(m => m.ObterPorNumeroESenhaAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(protocoloAtendimentoGfa);

            return mockProtocoloAtendimentoGfaRepository.Object;
        }

        #region [Metodos privados]

        private static ProtocoloAtendimentoGfa GeraProtocoloAtendimentoGfa()
        {
            return new ProtocoloAtendimentoGfa
            {
                Id = 7090,
                SenhaGfa = "NG010",
                NumeroProtocoloGfa = "100012623",
                UsuarioGfa = "teste.gab",
                IdIntegrador = IntegradorGfaEnum.Atend,
                DataInicioAtendimento = Convert.ToDateTime("30/01/2020 14:16:55")
            }; ;
        }

        #endregion
    }
}
