﻿using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using GAB.Inova.IntegrationTest.Atendimento.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Atendimento
{
    /// <summary>
    /// Classe de orquestração para testes de funcionalidades referente a Conta Digital
    /// </summary>
    public class TestFixture<TStartup> 
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            #region [Aplicação]

            var webHostEnvironment = ConfigurationMockHelper.GetHostingEnvironment();
            services.AddSingleton<IWebHostEnvironment>(webHostEnvironment);
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor());
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(webHostEnvironment));

            #endregion

            #region [Repository]

            services.AddSingleton<IUnitOfWork>(UnitOfWorkMockHelper.GetUnitOfWork());
            services.AddSingleton<IProtocoloAtendimentoRepository>(ProtocoloAtendimentoRepositoryMockHelper.GetProtocoloAtendimentoRepository());
            services.AddSingleton<IProtocoloAtendimentoDetalheRepository>(ProtocoloAtendimentoDetalheRepositoryMockHelper.GetProtocoloAtendimentoDetalheRepository());
            services.AddSingleton<IProtocoloAtendimentoGfaRepository>(ProtocoloAtendimentoGfaRepositoryMockHelper.GetProtocoloAtendimentoGfaRepository());
            services.AddSingleton<IUsuarioRepository>(UsuarioRepositoryMockHelper.GetUsuarioRepository());
            services.AddSingleton<IClienteRepository>(ClienteRepositoryMockHelper.GetClienteRepository());
            services.AddSingleton<IContratoRepository>(ContratoRepositoryMockHelper.GetContratoRepository());
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());

            #endregion
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }
        
    }
}