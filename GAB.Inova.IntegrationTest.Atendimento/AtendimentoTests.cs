﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Atendimento.Base;
using GAB.Inova.Common.ViewModels.Token;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace GAB.Inova.IntegrationTest.Atendimento
{
    /// <summary>
    /// Classe de orquestração para testes de funcionalidades referente a Atendiemnto
    /// </summary>
    public class AtendimentoTests
        : IClassFixture<TestFixture<Startup>>
    {

        const int CODIGO_EMPRESA = (int)EmpresaTipoEnum.CAA;

        readonly TestFixture<Startup> _fixture;
        readonly IHttpContextAccessor _httpContextAccessor;

        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly ITokenBusiness _tokenBusiness;

        #region [Construtores]

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="fixture">Indica a classe acessóaria</param>
        public AtendimentoTests(TestFixture<Startup> fixture)
        {
            _fixture = fixture;

            #region [Instanciando variáveis de aplicação]

            _httpContextAccessor = _fixture?
                .ServiceProvider?
                .GetService<IHttpContextAccessor>();

            #endregion

            #region [Instanciando variáveis business]

            _atendimentoBusiness = _fixture?
                .ServiceProvider?
                .GetService<IAtendimentoBusiness>();

            _tokenBusiness = _fixture?
                .ServiceProvider?
                .GetService<ITokenBusiness>();

            #endregion

        }

        #endregion

        [Theory]
        [InlineData("202011031284677", 32, 9999999999)]
        public async Task TestRegistrarNaturezaAsync(string numeroProtocolo, int naturezaId, long rotinaId)
        {
            var model = new RegistrarNaturezaViewModel()
            {
                NumeroProtocolo = numeroProtocolo,
                NaturezaId = naturezaId,
                RotinaId = rotinaId
            };

            var retorno = await _atendimentoBusiness.RegistrarNaturezaAsync(model);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("NG010", "100012623", "teste.gab")]
        public async Task TestCancelarIntegracaoGfaAsync(string senhaGfa, string protocoloGfa, string usuarioGfa)
        {
            var model = new CancelarIntegracaoGfaRequestViewModel()
            {
                SenhaGfa = senhaGfa,
                NumeroProtocoloGfa = protocoloGfa,
                UsuarioGfa = usuarioGfa
            };

            var retorno = await _atendimentoBusiness.CancelarIntegracaoGfaAsync(model);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("NG010", "100012623", "teste.gab")]
        public async Task TestFinalizarIntegracaoGfaAsync(string senhaGfa, string protocoloGfa, string usuarioGfa)
        {
            var model = new FinalizarIntegracaoGfaRequestViewModel()
            {
                SenhaGfa = senhaGfa,
                NumeroProtocoloGfa = protocoloGfa,
                UsuarioGfa = usuarioGfa
            };

            var retorno = await _atendimentoBusiness.FinalizarIntegracaoGfaAsync(model);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("NG010", "100012623", "teste.gab")]
        public async Task TestIniciarIntegracaoGfaAsync(string senhaGfa, string protocoloGfa, string usuarioGfa)
        {
            var model = new IniciarIntegracaoGfaRequestViewModel()
            {
                SenhaGfa = senhaGfa,
                NumeroProtocoloGfa = protocoloGfa,
                UsuarioGfa = usuarioGfa
            };

            var retorno = await _atendimentoBusiness.IniciarIntegracaoGfaAsync(model);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("NG010", "100012623", "teste.gab")]
        public async Task TestVerificarIntegracaoGfaAsync(string senhaGfa, string protocoloGfa, string usuarioGfa)
        {
            var model = new VerificarIntegracaoGfaRequestViewModel()
            {
                SenhaGfa = senhaGfa,
                NumeroProtocoloGfa = protocoloGfa,
                UsuarioGfa = usuarioGfa
            };

            var retorno = await _atendimentoBusiness.VerificarIntegracaoGfaAsync(model);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("teste.gab")]
        public async Task TestVerificarAtendimentoEmAbertoAsync(string username)
        {
            var retorno = await _atendimentoBusiness.VerificarAtendimentoEmAbertoAsync(username);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("teste.gab")]
        public async Task TestFinalizarProtocoloAtendimentoAsync(string numeroProtocolo)
        {
            var retorno = await _atendimentoBusiness.FinalizarProtocoloAtendimentoAsync(numeroProtocolo);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("1100001248")]
        public async Task TestGerarProtocoloAtendimentoByAplicativoAsync(string matricula)
        {
            var retorno = await _atendimentoBusiness.GerarProtocoloAtendimentoAsync(matricula, CanalAtendimentoEnum.Aplicativo);

            Assert.True(retorno.Sucesso);
        }

        [Fact]
        public async Task TestEnviarEmailFaleConoscoByAplicativoAsync()
        {
            await ObterTokenAsync();

            var retorno = await _atendimentoBusiness
                .EnviarEmailFaleConoscoByAplicativoAsync((EnviarEmailFaleConoscoByAplicativoRequestViewModel)ObterBaseEmailRequestViewModel(false));

            Assert.True(retorno.Sucesso);
        }

        [Fact]
        public async Task TestEnviarEmailOuvidoriaByAplicativoAsync()
        {
            await ObterTokenAsync();

            var retorno = await _atendimentoBusiness
                .EnviarEmailOuvidoriaByAplicativoAsync((EnviarEmailOuvidoriaByAplicativoRequestViewModel)ObterBaseEmailRequestViewModel(true));

            Assert.True(retorno.Sucesso);
        }

        #region [Métodos privados]

        private async Task ObterTokenAsync()
        {
            var tokenRequest = new TokenRequestViewModel();

            tokenRequest.Claims.Add(new KeyValuePair<string, string>("CodigoEmpresa", EmpresaTipoEnum.CAA.ToString()));

            var tokenResponse = await _tokenBusiness.GerarTokenAsync(tokenRequest.Claims, 600);

            _httpContextAccessor?
                .HttpContext?
                .Request?
                .Headers?
                .Clear();

            if (!string.IsNullOrEmpty(tokenResponse.Token))
            {
                _httpContextAccessor?
                    .HttpContext?
                    .Request?
                    .Headers?
                    .Add("Authorization", $"Bearer {tokenResponse.Token}");
            }
        }

        private BaseEmailFaleConoscoRequestViewModel ObterBaseEmailRequestViewModel(bool ouvidoria)
        {
            BaseEmailFaleConoscoRequestViewModel retorno;

            if (ouvidoria)
            {
                retorno = new EnviarEmailOuvidoriaByAplicativoRequestViewModel();
            }
            else
            {
                retorno = new EnviarEmailFaleConoscoByAplicativoRequestViewModel();

            }

            retorno.CodigoEmpresa = EmpresaTipoEnum.CAA.ToString();
            retorno.CpfCnpj = "01614459770";
            retorno.NomeCliente = "CLIENTE PARA TESTE AUTOMATIZADO";
            retorno.Email = "teste@grupoaguasdobrasil.com.br";

            return retorno;
        }

        #endregion

    }
}
