﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.CallCenter;

namespace GAB.Inova.API.CallCenter.Controllers
{    /// <summary>
     /// Classe de orquestração ao serviços do Call Center
     /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize("Bearer")]
    public class CallCenterController : ControllerBase
    {
        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly ICallCenterBusiness _callCenterBusiness;
        readonly ILogger<CallCenterController> _logger;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="callCenterBusiness">Instância do objeto com as regras de negócio referente ao Call Center</param>
        public CallCenterController(ILogger<CallCenterController> logger,
                                    IAtendimentoBusiness atendimentoBusiness,
                                    ICallCenterBusiness callCenterBusiness)
        {
            _logger = logger;
            _atendimentoBusiness = atendimentoBusiness;
            _callCenterBusiness = callCenterBusiness;
        }

        /// <summary>
        /// Método utilizado no CallCenter para saber se API está online
        /// </summary>
        /// <returns><see cref="IActionResult"/></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("Online")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult OnlineAsync()
        {
            return StatusCode(StatusCodes.Status200OK, DateTime.Now);
        }

        /// <summary>
        /// Gera autenticação para o Call Center
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="GerarAutenticacaoResponseViewModel"/><seealso cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [Route("GerarAutenticacao")]
        [ProducesResponseType(typeof(GerarAutenticacaoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GerarAutenticacaoAsync(GerarAutenticacaoRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation("Gerando autenticação para o call center", "");
                _logger.LogInformation($"ModelRecebido: { JsonConvert.SerializeObject(viewModel)}");

                var result = await _callCenterBusiness.GerarAutenticacaoAsync(viewModel);

                if (result.Sucesso)
                {
                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao gerar autenticação para o call center. ModelRecebido: { JsonConvert.SerializeObject(viewModel)} ");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Retorna cliente com suas ligações
        /// </summary>
        /// <param name="protocolo">Número do protocolo a ser utilizado</param>
        /// <param name="documento">Número do documento a ser pesquisado</param>
        /// <param name="matricula">Número da ligação a ser pesquisada</param>
        /// <returns><see cref="ClienteResponseViewModel"/></returns>
        [HttpGet]
        [Route("ObterCliente")]
        [ProducesResponseType(typeof(ClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterClienteAsync([FromHeader] string protocolo, [FromHeader] string documento, [FromHeader] string matricula)
        {
            try
            {
                _logger.LogInformation($"Obter cliente com suas ligações. protocolo [nº:{protocolo}], documento [nº:{documento}] e matrícula [nº:{matricula}].");

                var result = await _callCenterBusiness.ObterClienteAsync(protocolo, documento, matricula);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter cliente com suas ligações documento [nº:{documento}] e ligação [nº:{matricula}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        /// <summary>
        /// Retorna informações da conta
        /// </summary>
        /// <param name="protocolo">Número do protocolo a ser utilizado</param>
        /// <param name="matricula">Número da ligação a ser pesquisada</param>
        /// <returns><see cref="InformacaoContaResponseViewModel"/></returns>
        [HttpGet]
        [Route("ObterInformacaoConta")]
        [ProducesResponseType(typeof(InformacaoContaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterInformacaoContaAsync([FromHeader] string protocolo, [FromHeader] string matricula)
        {
            try
            {
                _logger.LogInformation($"Obter informação de conta. protocolo [nº:{protocolo}] e matrícula [nº:{matricula}].");

                var result = await _callCenterBusiness.ObterInformacaoContaAsync(protocolo, matricula);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter informação de conta. protocolo [nº:{protocolo}] e matrícula [nº:{matricula}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("RegistrarNaturezaAtendimento")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RegistrarNaturezaAtendimentoAsync([FromBody] RegistrarNaturezaRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Registrar natureza do atendimento [nº protocolo:{model?.Protocolo}; id natureza:{model?.CodNatureza}].");

                var result = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel
                {
                    NumeroProtocolo = model.Protocolo,
                    NaturezaId = Convert.ToInt32(model.CodNatureza)
                });

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao registrar natureza do atendimento [nº protocolo:{model?.Protocolo}; id natureza:{model?.CodNatureza}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("GerarProtocolo")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GerarProtocoloAsync()
        {
            try
            {
                _logger.LogInformation($"Criar protocolo {CanalAtendimentoEnum.CallCenter.GetDescription()}.");

                var result = await _atendimentoBusiness.GerarProtocoloAtendimentoAsync(string.Empty, CanalAtendimentoEnum.CallCenter, matriculaObrigatoria: false);

                if (!result.Sucesso)
                {
                    result.AdicionaMensagem("Sistema indisponível! Tente novamente.");

                    _logger.LogInformation($"Não foi possível gerar protocolo de atendimento via {CanalAtendimentoEnum.CallCenter.GetDescription()}.");

                    return BadRequest(result);
                }

                _logger.LogInformation($"Protocolo de atendimento [nº:{result.NumeroProtocolo}] gerado via {CanalAtendimentoEnum.CallCenter.GetDescription()}.");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao Autenticar via {CanalAtendimentoEnum.CallCenter.GetDescription()}.");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("FinalizarProtocoloAtendimento")]
        [ProducesResponseType(typeof(FinalizarProtocoloAtendimentoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> FinalizarProtocoloAtendimentoAsync([FromHeader] string protocolo, [FromHeader] string resolucao)
        {
            try
            {
                var result = await _callCenterBusiness.FinalizarProtocoloAtendimentoAsync(protocolo, resolucao);

                if (result.Sucesso) return Ok(result);

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar protocolo de atendimento [nº:{protocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("AssociarLigacaoProtocolo")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AssociarLigacaoProtocoloAsync(AssociarLigacaoProtocoloViewModel model)
        {
            try
            {
                _logger.LogInformation($"Vinculando ligação a protocolo [nº protocolo:{model.Protocolo}; matricula:{model.NumLigacao}].");

                var result = await _callCenterBusiness.AssociarLigacaoProtocoloAsync(model);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao vincular ligação a protocolo [nº protocolo:{model.Protocolo}; matricula:{model.NumLigacao}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        
    }
}
