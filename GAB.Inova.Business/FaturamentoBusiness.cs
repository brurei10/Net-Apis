﻿using AutoMapper;
using GAB.Inova.Business.Bases;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Faturamento;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class FaturamentoBusiness
        : BasePdfBusiness, IFaturamentoBusiness
    {

        readonly IConfiguration _configuration;
        readonly IContratoRepository _contratoRepository;
        readonly IEmailIntegradorRepository _emailIntegradorRepository;
        readonly IFeriadoRepository _feriadoRepository;
        readonly IIntegradorConfigFactory _integradorConfigFactory;
        readonly IProcessoClienteRepository _processoClienteRepository;
        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        readonly ITempCobrancaCtRepository _tempCobrancaCtRepository;
        readonly ICompoeCtRepository _compoeCtRepository;
        readonly IGRPRepository _GRPRepository;
        readonly IParcelaRepository _parcelaRepository;
        readonly IVwClienteRepository _vwClienteRepository;

        readonly IUnitOfWork _unitOfWork;

        readonly ILogger<FaturamentoBusiness> _logger;

        public FaturamentoBusiness(IMapper mapper,
                                   IConfiguration configuration,
                                   IWebHostEnvironment webHostEnvironment,
                                   ILoggerFactory loggerFactory,
                                   IBarcodeProvider barcodeProvider,
                                   IHtmlToPdfProvider htmlToPdfProvider,
                                   IContaRepository contaRepository,
                                   IContratoRepository contratoRepository,
                                   IEmpresaRepository empresaRepository,
                                   IEmailIntegradorRepository emailIntegradorRepository,
                                   IFaturamentoRepository faturamentoRepository,
                                   IFeriadoRepository feriadoRepository,
                                   IIntegradorConfigFactory integradorConfigFactory,
                                   IParametroSistemaRepository parametroSistemaRepository,
                                   IPdfGenericoRepository pdfGenericoRepository,
                                   IProcessoClienteRepository processoClienteRepository,
                                   IProtocoloAtendimentoRepository protocoloAtendimentoRepository,
                                   ITabAnaliseAguaRepository tabAnaliseAguaRepository,
                                   ITarifaDiariaRepository tarifaDiariaRepository,
                                   ITempHistoricoConsumoRepository tempHistoricoConsumoRepository,
                                   ITempImprimeContaRepository tempImprimeContaRepository,
                                   ITempCobrancaCtRepository tempCobrancaCtRepository,
                                   ICompoeCtRepository compoeCtRepository,
                                   IGRPRepository GRPRepository,
                                   IParcelaRepository parcelaRepository,
                                   IVwClienteRepository vwClienteRepository,
                                   IImprimeGRPRepository imprimeGRPRepository,
                                   IDocumentoModeloRepository documentoModeloRepository,
                                   IUnitOfWork unitOfWork)
            : base(mapper, webHostEnvironment, barcodeProvider, htmlToPdfProvider, contaRepository, faturamentoRepository, 
                   parametroSistemaRepository, pdfGenericoRepository, tabAnaliseAguaRepository, tarifaDiariaRepository, 
                   tempHistoricoConsumoRepository, tempImprimeContaRepository, documentoModeloRepository, imprimeGRPRepository, 
                   empresaRepository)
        {
            _configuration = configuration;
            _contratoRepository = contratoRepository;
            _emailIntegradorRepository = emailIntegradorRepository;
            _feriadoRepository = feriadoRepository;
            _integradorConfigFactory = integradorConfigFactory;
            _processoClienteRepository = processoClienteRepository;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
            _tempCobrancaCtRepository = tempCobrancaCtRepository;
            _compoeCtRepository = compoeCtRepository;
            _GRPRepository = GRPRepository;
            _parcelaRepository = parcelaRepository;
            _vwClienteRepository = vwClienteRepository;
            _unitOfWork = unitOfWork;

            _logger = loggerFactory.CreateLogger<FaturamentoBusiness>();
        }

        #region [Public methods]

        public async Task<HistoricoContasResponseViewModel> ObterHistoricoContasAsync(string numeroProtocolo)
        {
            var retorno = new HistoricoContasResponseViewModel()
            {
                Sucesso = false
            };

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);
            if (protocoloAtendimento is null)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não existe [nº:{numeroProtocolo}].");
                return retorno;
            }

            if (protocoloAtendimento.IdContrato == 0)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não possui uma ligação vinculada [nº:{numeroProtocolo}].");
                return retorno;
            }

            retorno.Sucesso = true;

            var processoCliente = await _processoClienteRepository.VerificarClienteSobJudice(protocoloAtendimento.Matricula);
            if (processoCliente != null && processoCliente.Processo?.EmissaoSegundaVia == 2)
            {
                retorno.AdicionaMensagem("Relação de contas indisponíveis para impressão. Ligação com processo judicial.");
                return retorno;
            }

            var contas = await _contaRepository.ObterUltimasDozeContasEmAbertoOuPagasPorContratoAsync(protocoloAtendimento.IdContrato);
            if (contas.Any())
            {
                retorno.ContasContrato = _mapper.Map<IList<ContasContratoViewModel>>(contas);
            }
            else retorno.AdicionaMensagem("Você não possui histórico de contas: pagas, ou, em aberto, ou em atraso.");
            
            return retorno;
        }

        public async Task<HistoricoContasResponseViewModel> ObterHistoricoContasEmAbertoAsync(string numeroProtocolo)
        {
            var retorno = new HistoricoContasResponseViewModel()
            {
                Sucesso = false
            };

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);

            if (protocoloAtendimento is null)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não existe [nº:{numeroProtocolo}].");

                return retorno;
            }

            if (protocoloAtendimento.IdContrato == 0)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não possui uma ligação vinculada [nº:{numeroProtocolo}].");

                return retorno;
            }

            var processoCliente = await _processoClienteRepository.VerificarClienteSobJudice(protocoloAtendimento.Matricula);

            if (processoCliente != null && processoCliente.Processo?.EmissaoQuitacaoDebito == 2)
            {
                retorno.AdicionaMensagem("Relação de contas indisponíveis para impressão. Ligação com processo judicial.");

                return retorno;
            }

            var contas = await _contaRepository.ObterContasEmAbertoPorContratoAsync(protocoloAtendimento.IdContrato);

            if (contas.Any())
            {
                retorno.ContasContrato = _mapper.Map<IList<ContasContratoViewModel>>(contas);
            }
            else
            {
                retorno.AdicionaMensagem("Você não possui contas em aberto.");
            }

            retorno.Sucesso = true;
            return retorno;
        }

        public async Task<SegundaViaContaResponseViewModel> ObterSegundaViaAsync(long seqOriginal)
        {
            var retorno = new SegundaViaContaResponseViewModel()
            {
                Sucesso = false
            };

            var conta = await _contaRepository.ObterContaPorSeqOriginalAsync(seqOriginal);
            if (conta is null)
            {
                retorno.AdicionaMensagem($"Não foi possível localizar a conta [nº:{seqOriginal}].");
                return retorno;
            }

            var buffer = await GerarSegundaViaAsync(conta);
            if (buffer.Length > 0)
            {
                retorno.EspelhoDaConta = Convert.ToBase64String(buffer);
                retorno.Sucesso = true;
            }
            else retorno.AdicionaMensagem($"Não foi possível gerar o espelho da conta [nº:{conta.Id}].");

            return retorno;
        }

        public async Task<BaseResponseViewModel> EnviarEmailSegundaViaAsync(long seqOriginal, string email)
        {
            var retorno = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            if (!email.IsValidEmail())
            {
                retorno.AdicionaMensagem("E-mail inválido! Revise o endereço de e-mail digitado para concluir a ação.");
                return retorno;
            }

            var conta = await _contaRepository.ObterContaPorSeqOriginalAsync(seqOriginal);
            if (conta is null)
            {
                retorno.AdicionaMensagem($"Não foi possível localizar a conta [nº:{seqOriginal}].");
                return retorno;
            }

            var buffer = await GerarSegundaViaAsync(conta);
            if (buffer.Length > 0)
            {
                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.SegundaViaDeContaPeloAplicativo);
                var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);
                var contrato = await _contratoRepository.ObterPorMatriculaAsync(conta.Matricula);
                
                mensagemModel.Destinatarios = sendGridConfig.TratarERetornarListaDestinatariosConcatenados(email);
                mensagemModel.Conteudo = new TemplateEmailSegundaViaAplicativoViewModel
                {
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    NomeCliente = contrato.NomeCliente
                };
                mensagemModel.AnexoAdiciona(new AnexoEmailViewModel
                {
                    ArquivoBase64 = Convert.ToBase64String(buffer),
                    NomeArquivo = $"SegundaVia-{conta.Id}.pdf",
                    Tipo = "application/pdf"
                });

                var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

                retorno.Sucesso = emailIntegradorResponse.Success;
            }

            if (!retorno.Sucesso)
            {
                retorno.AdicionaMensagem($"Não foi possível enviar e-mail de segunda via da conta [nº:{conta.Id}].");
            }

            return retorno;
        }

        public async Task<CodigoBarrasResponseViewModel> ObterCodigoBarrasNotaFiscalAsync(long seqOriginal)
        {
            var retorno = new CodigoBarrasResponseViewModel()
            {
                Sucesso = false
            };

            var conta = await _contaRepository.ObterContaPorSeqOriginalAsync(seqOriginal);
            if (conta is null)
            {
                retorno.AdicionaMensagem($"Não foi possível localizar a conta [nº:{seqOriginal}].");
                return retorno;
            }

            if (conta.SituacaoConta != SituacaoContaEnum.NaoPaga)
            {
                retorno.AdicionaMensagem($"Conta não se encontra em aberto. Não é possivel obter o código de barras. [Situação:{conta.SituacaoConta.GetDescription()}].");
                return retorno;
            }

            retorno = _mapper.Map<CodigoBarrasResponseViewModel>(conta);
            retorno.CodigoBarras = ObterCodigoBarrasAsync(conta, TipoContaEnum.ContaNormal).GetAwaiter().GetResult().ToBarCode();
            retorno.Sucesso = !string.IsNullOrEmpty(retorno.CodigoBarras);

            return retorno;
        }

        public async Task<CodigoBarrasResponseViewModel> ObterCodigoBarrasQuitacaoDebitoAsync(IEnumerable<long> seqOriginais, string numeroProtocolo)
        {
            var imprimeGRP = await ProcessaQuitacaoDebitoAsync(seqOriginais, numeroProtocolo);

            return _mapper.Map<CodigoBarrasResponseViewModel>(imprimeGRP);
        }

        public async Task<PdfQuitacaoDebitoResponseViewModel> ObterPdfQuitacaoDebitoAsync(IEnumerable<long> seqOriginais, string numeroProtocolo)
        {
            var retorno = new PdfQuitacaoDebitoResponseViewModel()
            {
                Sucesso = false
            };

            var imprimeGRP = await ProcessaQuitacaoDebitoAsync(seqOriginais, numeroProtocolo);

            if (imprimeGRP is null)
            {
                retorno.AdicionaMensagem("Ocorreu um erro ao gerar a quitação de débito");

                return retorno;
            }
            else
            {
                var buffer = await GerarPdfQuitacaoDebitoAsync(imprimeGRP);

                if (buffer.Length > 0)
                {
                    retorno.EspelhoDaQuitacao = Convert.ToBase64String(buffer);

                    retorno.Sucesso = true;
                }
                else
                {
                    retorno.AdicionaMensagem($"Não foi possível gerar o espelho da quitação de débito [nº:{string.Join(",", seqOriginais)}].");
                }
            }

            return retorno;
        }


        #endregion

        #region [Private methods]

        private async Task<byte[]> GerarSegundaViaAsync(Conta conta)
        {
            var pathNotaFiscal = _configuration.GetValue<string>("PathMotaFiscal");
            if (string.IsNullOrEmpty(pathNotaFiscal)) throw new ArgumentNullException("Não foi possível encontrar o parâmetro [PathMotaFiscal].");

            var contaLocalizacao = await _contaRepository.ObterContaLocalizacaoAsync(conta.Id);
            if (contaLocalizacao is null) throw new ArgumentNullException($"Não foi possível encontrar a rota da conta [nº:{conta.Id}].");

            var pathSchema = $"{contaLocalizacao.SiglaEmpresa}\\NOTAFISCAL\\{contaLocalizacao.Emissao.ToString("yyyyMMdd")}\\{contaLocalizacao.IdCiclo}\\{contaLocalizacao.Rota}\\{((int)contaLocalizacao.SituacaoConta).ToString()}";
            var filePath = $"{pathNotaFiscal}\\{pathSchema}";
            var fileName = $"{contaLocalizacao.Id}";

            if (File.Exists($"{filePath}\\{fileName}.pdf"))
                File.Delete($"{filePath}\\{fileName}.pdf");

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            return await base.GerarSegundaViaContaAsync(conta, filePath, fileName);
        }

        private async Task<string> ObterCodigoBarrasAsync(Conta conta, TipoContaEnum tipoConta)
        {
            var empresa = await _empresaRepository.ObterAsync();

            var barCode43 = await _barcodeProvider.GerarCodigoBarrasAsync(tipoConta, 
                                                                          conta.Id,
                                                                          conta.ValorTotal > 0 ? conta.ValorTotal : conta.ValorServico, 
                                                                          conta.Matricula, 
                                                                          empresa.IdFebraban);

            var barCode48 = await _contaRepository.ObterDigitoCodigoBarrasAsync(barCode43);

            return barCode48;
        }

        private async Task<ImprimeGRP> ProcessaQuitacaoDebitoAsync(IEnumerable<long> seqOriginais, string numeroProtocolo)
        {
            var contas = await _contaRepository.ObterContasPorSeqOriginalAsync(seqOriginais);

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);

            var parametro = await _parametroSistemaRepository.ObterAsync(361); //QUANTIDADE DE DIAS ÚTEIS PARA VENCIMENTO DA DNF

            if (protocoloAtendimento is null) throw new ArgumentNullException($"O protocolo de atendimento informado não existe [nº:{numeroProtocolo}].");

            if (contas is null && !contas.Any()) throw new ArgumentNullException($"Não foi possível localizar conta(s) com situação: em atraso.");

            if (contas.Count() != seqOriginais.Count()) throw new ArgumentNullException($"Foi informado {seqOriginais.Count()} conta(s) mas somente foi encontrado {contas.Count()} contas");

            if (contas.GroupBy(c => c.Matricula).Count() > 1) throw new ArgumentNullException($"Foi encontrado mais de uma matrícula para as contas informadas. Matrículas: {string.Join(",", contas.GroupBy(c => c.Matricula).Select(r => r.Key).ToList())}");

            if (parametro is null) throw new ArgumentNullException("Ocorreu um erro ao obter parâmetro para o sistema [nº:361]");

            if (contas.FirstOrDefault().Matricula != protocoloAtendimento.Matricula) throw new ArgumentNullException($"Matrícula da(s) conta(s) diferente da matrícula do protocolo. [Declaração anual: {contas.FirstOrDefault().Matricula}, protocolo: {protocoloAtendimento.Matricula}]");

            try
            {
                _unitOfWork.BeginTransaction();

                var quitacaoDebito = await GerarOuRecuperarQuitacaoDebito(parametro, contas, protocoloAtendimento);

                if (quitacaoDebito is null)
                {
                    _unitOfWork.Rollback();

                    throw new ArgumentNullException("Ocorreu um erro ao obter ou gerar a quitação de débito");
                }
                else
                {
                    var codigoBarras = await ObterCodigoBarrasAsync(quitacaoDebito, TipoContaEnum.DocumentoNaoFiscal);

                    if (!string.IsNullOrEmpty(codigoBarras))
                    {
                        var imprimeGRP = await _imprimeGRPRepository.ObterPorSeqOriginal(quitacaoDebito.Id);

                        if (imprimeGRP is null)
                        {
                            var vwCliente = await _vwClienteRepository.ObterPorMatriculaAsync(quitacaoDebito.Matricula);

                            imprimeGRP = _mapper.Map<Conta, ImprimeGRP>(quitacaoDebito, imprimeGRP);
                            imprimeGRP = _mapper.Map<VwCliente, ImprimeGRP>(vwCliente, imprimeGRP);

                            imprimeGRP.CodigoBarra = codigoBarras;
                            imprimeGRP.CompoeValor02 = await _parcelaRepository.ObterTotalPorMatriculaSituacaoContratoAsync(quitacaoDebito.Matricula, 0, quitacaoDebito.IdContrato);

                            var inseriuImprimeGRP = await _imprimeGRPRepository.InserirAsync(imprimeGRP);

                            if (!inseriuImprimeGRP)
                            {
                                _unitOfWork.Rollback();

                                throw new ArgumentNullException($"Ocorreu um erro ao inseir o ImprimeGRP.");
                            }
                        }

                        _unitOfWork.Commit();

                        return imprimeGRP;
                    }
                    else
                    {
                        _unitOfWork.Rollback();

                        throw new ArgumentNullException($"Não foi gerado o código de barras quitação de débito.");
                    }
                }
            }
            catch
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task<Conta> GerarOuRecuperarQuitacaoDebito(ParametroSistema parametro, IEnumerable<Conta> contas, ProtocoloAtendimento protocoloAtendimento)
        {
            var dataVencimento = await _feriadoRepository.ObterProximoDiaUtil(DateTime.Today, (int)parametro.Valor, 1);

            var quitacaoDebito = await _contaRepository.ObterQuitacaoDebitoAsync(contas.FirstOrDefault().Matricula, dataVencimento, contas.Sum(c => c.ValorTotal));

            if (quitacaoDebito is null)
            {
                return await GerarQuitacaoDebitoAsync(contas, dataVencimento, protocoloAtendimento);
            }
            else
            {
                return quitacaoDebito;
            }
        }

        private async Task<Conta> GerarQuitacaoDebitoAsync(IEnumerable<Conta> contas, DateTime dataVencimento, ProtocoloAtendimento protocoloAtendimento)
        {
            try
            {
                var listaTempCobrancaCt = _mapper.Map<IEnumerable<TempCobrancaCt>>(contas);

                foreach (var tempCobrancaCt in listaTempCobrancaCt)
                {
                    var inseriuTempCobrancaCt = await _tempCobrancaCtRepository.InserirAsync(tempCobrancaCt);

                    if (!inseriuTempCobrancaCt)
                    {
                        throw new InvalidOperationException($"Ocorreu um erro ao inserir TempCobrancaCt seq original: [nº:{tempCobrancaCt.SeqOriginalPago}]");
                    }
                }

                var quitacaoDebito = _mapper.Map<Conta>(contas.FirstOrDefault());
                quitacaoDebito.Vencimento = dataVencimento;
                quitacaoDebito.ValorServico = contas.Sum(c => c.ValorTotal);

                quitacaoDebito.Id = await _contaRepository.InserirAsync(quitacaoDebito);

                if (quitacaoDebito.Id != Int64.MinValue)
                {
                    var listaCompoeCt = _mapper.Map<IEnumerable<CompoeCt>>(contas);

                    foreach (var compoeCt in listaCompoeCt)
                    {
                        compoeCt.DocumentoPago = (int)quitacaoDebito.Id;

                        var inseriuCompoeCt = await _compoeCtRepository.InserirAsync(compoeCt);

                        if (!inseriuCompoeCt)
                        {
                            throw new InvalidOperationException($"Ocorreu um erro ao inserir CompoeCt seq original: [nº:{compoeCt.DocumentoOriginal}]");
                        }
                    }

                    var grp = new GRP();
                    grp = _mapper.Map<ProtocoloAtendimento, GRP>(protocoloAtendimento, grp);
                    grp = _mapper.Map<Conta, GRP>(quitacaoDebito, grp);

                    var inseriuGRP = await _GRPRepository.InserirAsync(grp);

                    if (!inseriuGRP)
                    {
                        throw new InvalidOperationException($"Ocorreu um erro ao inserir GRP seq original: [nº:{grp.Id}]");
                    }
                    else
                    {
                        return quitacaoDebito;
                    }
                }
                else
                {
                    throw new InvalidOperationException($"Ocorreu um erro ao inserir a quitação de débito.");
                }
            }
            catch
            {
                throw;
            }
        }
        private async Task<byte[]> GerarPdfQuitacaoDebitoAsync(ImprimeGRP imprimeGRP)
        {
            var pathNotaFiscal = _configuration.GetValue<string>("PathMotaFiscal");
            if (string.IsNullOrEmpty(pathNotaFiscal)) throw new ArgumentNullException("Não foi possível encontrar o parâmetro [PathMotaFiscal].");

            var contaLocalizacao = await _contaRepository.ObterContaLocalizacaoAsync(imprimeGRP.Id);
            if (contaLocalizacao is null) throw new ArgumentNullException($"Não foi possível encontrar a rota da conta [nº:{imprimeGRP.Id}].");

            var pathSchema = $"{contaLocalizacao.SiglaEmpresa}\\QUITACAO_DEBITO\\{contaLocalizacao.Emissao:yyyyMMdd}\\{contaLocalizacao.IdCiclo}\\{contaLocalizacao.Rota}\\{(int)contaLocalizacao.SituacaoConta}";
            var filePath = $"{pathNotaFiscal}\\{pathSchema}";
            var fileName = $"{contaLocalizacao.Id}";

            if (File.Exists($"{filePath}\\{fileName}.pdf"))
                File.Delete($"{filePath}\\{fileName}.pdf");

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            return await base.GerarPdfQuitacaoDebitoAsync(imprimeGRP, filePath, fileName);
        }

        #endregion

    }
}
