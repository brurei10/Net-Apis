﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class ClienteBusiness
        : IClienteBusiness
    {

        readonly IMapper _mapper;
        readonly IClienteRepository _clienteRepository;
        readonly IClienteTokenRepository _clienteTokenRepository;
        readonly IContratoRepository _contratoRepository;
        readonly IProcessoClienteRepository _processoClienteRepository;
        readonly IVwClienteRepository _vwClienteRepository;
        readonly IInformacaoContaRepository _informacaoContaRepository;
        readonly ITokenProvider _tokenProvider;

        public ClienteBusiness(IMapper mapper, 
                               IClienteRepository clienteRepository,
                               IClienteTokenRepository clienteTokenRepository,
                               IContratoRepository contratoRepository,
                               IProcessoClienteRepository processoClienteRepository,
                               IVwClienteRepository vwClienteRepository,
                               IInformacaoContaRepository informacaoContaRepository,
                               ITokenProvider tokenProvider)
        {
            _mapper = mapper;
            _clienteRepository = clienteRepository;
            _clienteTokenRepository = clienteTokenRepository;
            _contratoRepository = contratoRepository;
            _processoClienteRepository = processoClienteRepository;
            _vwClienteRepository = vwClienteRepository;
            _informacaoContaRepository = informacaoContaRepository;
            _tokenProvider = tokenProvider;
        }

        public async Task<ClienteResponseViewModel> ObterClientePorMatriculaAsync(string matricula)
        {
            var clienteResponse = default(ClienteResponseViewModel);

            var cliente = await _clienteRepository.ObterPorMatriculaAsync(matricula);

            if (cliente != null)
            {
                clienteResponse = _mapper.Map<Cliente, ClienteResponseViewModel>(cliente, clienteResponse);

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(matricula);

                if (contrato != null) clienteResponse = _mapper.Map<Contrato, ClienteResponseViewModel>(contrato, clienteResponse);
            }

            return clienteResponse;
        }

        public async Task<ClienteTokenResponseViewModel> ObterClientePorTokenAsync(string token)
        {
            var clienteTokenResponse = default(ClienteTokenResponseViewModel);

           var clienteToken = await _clienteTokenRepository.ObterPorTokenAsync(token);

            if (clienteToken != null)
            {
                clienteTokenResponse = _mapper.Map<ClienteToken, ClienteTokenResponseViewModel>(clienteToken, clienteTokenResponse);
                clienteTokenResponse.EmailDoToken = await _tokenProvider.ObterClaimAsync(clienteToken.Token, "EmailCadastro");

                var cliente = await _clienteRepository.ObterPorMatriculaAsync(clienteToken.Matricula);
                if (cliente != null) clienteTokenResponse = _mapper.Map<Cliente, ClienteTokenResponseViewModel>(cliente, clienteTokenResponse);

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(clienteToken.Matricula);
                if (contrato != null) clienteTokenResponse = _mapper.Map<Contrato, ClienteTokenResponseViewModel>(contrato, clienteTokenResponse);
            }

            return clienteTokenResponse;
        }

        public async Task<ClienteSobJudiceResponseViewModel> VerificarClienteSobJudiceAsync(string matricula)
        {
            var clienteSobJudiceResponse = default(ClienteSobJudiceResponseViewModel);

            var cliente = await _clienteRepository.ObterPorMatriculaAsync(matricula);
            if (cliente is null) throw new InvalidOperationException($"Não foi possível encontrar o cliente com a matrícula informada [nº:{matricula}].");

            var processoCliente = await _processoClienteRepository.VerificarClienteSobJudice(matricula);
            if (processoCliente != null)
            {
                clienteSobJudiceResponse = _mapper.Map<ProcessoCliente, ClienteSobJudiceResponseViewModel>(processoCliente, clienteSobJudiceResponse);
            }
            else
            {
                clienteSobJudiceResponse = new ClienteSobJudiceResponseViewModel()
                {
                    Matricula = matricula,
                    PermiteEmissaoQuitacaoDebito = true,
                    PermiteEmissaoSegundaVia = true
                };
            }

            clienteSobJudiceResponse.Sucesso = true;

            return clienteSobJudiceResponse;
        }

        public async Task<ObterLigacoesResponseViewModel> ObterLigacoesAsync(string documento)
        {
            var obterLigacoesResponseViewModel = default(ObterLigacoesResponseViewModel);

            var ligacoes = await _vwClienteRepository.ObterPorDocumentoAsync(documento);

            if (ligacoes != null)
            {
                obterLigacoesResponseViewModel = new ObterLigacoesResponseViewModel();
                obterLigacoesResponseViewModel.Ligacoes = _mapper.Map<IList<LigacaoViewModel>>(ligacoes);
            }

            return obterLigacoesResponseViewModel;
        }
    }
}
 