﻿using AutoMapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Enums;
using GAB.Inova.Common.PdfLibrary.Structures;
using GAB.Inova.Common.SendGrid;
using GAB.Inova.Common.ViewModels;
using GAB.Inova.Common.ViewModels.AgenciaVirtual;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.CallCenter;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Common.ViewModels.Consumo;
using GAB.Inova.Common.ViewModels.Faturamento;
using GAB.Inova.Common.ViewModels.Perdas;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Common.ViewModels.Servico;
using GAB.Inova.Domain;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GAB.Inova.Business.Mappings
{
    public class DomainToViewModelMappingProfile 
        : Profile
    {
        public DomainToViewModelMappingProfile()
        {

            #region [Atendimento]

            CreateMap<ProtocoloAtendimento, IniciarIntegracaoGfaResponseViewModel>()
                .ForMember(dest => dest.NumeroProtocoloInova, opt => opt.MapFrom(src => src.Numero));

            CreateMap<ProtocoloAtendimentoGfa, IniciarIntegracaoGfaResponseViewModel>();

            CreateMap<TabNaturezaSolicitacao, NaturezaSolicitacaoViewModel>()
                .ForMember(m => m.IdNatureza, o => o.MapFrom(s => s.Id))
                .ForMember(m => m.Descricao, o => o.MapFrom(s => s.Descricao));

            CreateMap<Atendimento, FinalizarProtocoloAtendimentoResponseViewModel>();
            CreateMap<Natureza, NaturezaViewModel>();
            #endregion

            #region [Cliente - Conta Digital]

            CreateMap<MensagemIntegrador, EmailResponseViewModel>()
               .ForMember(dest => dest.Guid, opt => opt.MapFrom(src => src.GuidMensagem));

            CreateMap<SendGridContaDigitalConfig, EmailRequestViewModel>();
            CreateMap<SendGridContaDigitalClienteSemAdesaoConfig, EmailRequestViewModel>();
            CreateMap<SendGridEmailAdesaoConfig, EmailRequestViewModel>();
            CreateMap<SendGridEmailAlteracaoConfig, EmailRequestViewModel>();
            CreateMap<SendGridEmailCancelamentoConfig, EmailRequestViewModel>();
            CreateMap<SendGridEmailBoasVindasConfig, EmailRequestViewModel>();

            #endregion

            #region [Cliente]

            CreateMap<Cliente, Common.ViewModels.Cliente.ClienteResponseViewModel>()
                .ForMember(dest => dest.EhContaDigital, opt => opt.MapFrom((s, d) => ContaDigitalResolver(s, d)))
                .ForMember(dest => dest.GrupoEntregaId, opt => opt.MapFrom(src => (int)src.IdGrupoEntrega))
                .ForMember(dest => dest.GrupoEntrega, opt => opt.MapFrom((s, d) => GrupoEntregaResolver(s, d)))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.Localizacao, opt => opt.MapFrom(src => src.Localizacao))
                .ForMember(dest => dest.TipoFaturaId, opt => opt.MapFrom(src => (int)src.IdTipoFatura));

            CreateMap<Cliente, ClienteTokenResponseViewModel>()
                .ForMember(dest => dest.EhContaDigital, opt => opt.MapFrom((s, d) => ContaDigitalResolver(s, d)))
                .ForMember(dest => dest.GrupoEntregaId, opt => opt.MapFrom(src => (int)src.IdGrupoEntrega))
                .ForMember(dest => dest.GrupoEntrega, opt => opt.MapFrom((s, d) => GrupoEntregaResolver(s, d)))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.Localizacao, opt => opt.MapFrom(src => src.Localizacao));

            CreateMap<Contrato, Common.ViewModels.Cliente.ClienteResponseViewModel>()
                .ForMember(dest => dest.Bairro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(Bairro))))
                .ForMember(dest => dest.Celular, opt => opt.MapFrom(src => src.Celular))
                .ForMember(dest => dest.CelularFormatado, opt => opt.MapFrom(src => src.CelularFormatado))
                .ForMember(dest => dest.CEP, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(string), "cep")))
                .ForMember(dest => dest.Documento, opt => opt.MapFrom(src => src.CpfCnpj))
                .ForMember(dest => dest.DocumentoFormatado, opt => opt.MapFrom(src => src.CpfCnpjFormatado))
                .ForMember(dest => dest.Logradouro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(string), "nome")))
                .ForMember(dest => dest.LogradouroComplemento, opt => opt.MapFrom(src => src.Complemento))
                .ForMember(dest => dest.LogradouroNumero, opt => opt.MapFrom(src => src.Numero))
                .ForMember(dest => dest.Nome, opt => opt.MapFrom(src => src.NomeCliente));

            CreateMap<Contrato, ClienteTokenResponseViewModel>()
                .ForMember(dest => dest.Bairro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(Bairro))))
                .ForMember(dest => dest.Celular, opt => opt.MapFrom(src => src.Celular))
                .ForMember(dest => dest.CelularFormatado, opt => opt.MapFrom(src => src.CelularFormatado))
                .ForMember(dest => dest.CEP, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(string), "cep")))
                .ForMember(dest => dest.Documento, opt => opt.MapFrom(src => src.CpfCnpj))
                .ForMember(dest => dest.DocumentoFormatado, opt => opt.MapFrom(src => src.CpfCnpjFormatado))
                .ForMember(dest => dest.Logradouro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s, typeof(string), "nome")))
                .ForMember(dest => dest.LogradouroComplemento, opt => opt.MapFrom(src => src.Complemento))
                .ForMember(dest => dest.LogradouroNumero, opt => opt.MapFrom(src => src.Numero))
                .ForMember(dest => dest.Nome, opt => opt.MapFrom(src => src.NomeCliente));

            CreateMap<ClienteToken, ClienteTokenResponseViewModel>()
                .ForMember(dest => dest.Token, opt => opt.MapFrom(src => src.Token))
                .ForMember(dest => dest.TokenTipo, opt => opt.MapFrom(src => src.Tipo));

            CreateMap<ProcessoCliente, ClienteSobJudiceResponseViewModel>()
                .ForMember(dest => dest.PermiteEmissaoQuitacaoDebito, opt => opt.MapFrom((s, d) => ClienteSobJudiceResolver(s, d, s.Processo.EmissaoQuitacaoDebito)))
                .ForMember(dest => dest.PermiteEmissaoSegundaVia, opt => opt.MapFrom((s, d) => ClienteSobJudiceResolver(s, d, s.Processo.EmissaoSegundaVia)));

            CreateMap<Cliente, Common.ViewModels.Cliente.ClienteAutenticadoViewModel>()
                .ForMember(dest => dest.Bairro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s.Contrato, typeof(Bairro))))
                .ForMember(dest => dest.Cep, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s.Contrato, typeof(string), "cep")))
                .ForMember(dest => dest.Cidade, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s.Contrato, typeof(Cidade))))
                .ForMember(dest => dest.CpfCnpj, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "cpfcnpj")))
                .ForMember(dest => dest.Email, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "email")))
                .ForMember(dest => dest.EnderecoCompleto, opt => opt.MapFrom((s, d) => EnderecoCompletoResolver(s, d)))
                .ForMember(dest => dest.Estado, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s.Contrato, typeof(Estado))))
                .ForMember(dest => dest.Ligacao, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.Logradouro, opt => opt.MapFrom((s, d) => ContratoLogradouroResolver(s.Contrato, typeof(string), "logradouro")))
                .ForMember(dest => dest.LogradouroComplemento, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "complemento")))
                .ForMember(dest => dest.LogradouroNumero, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "numero")))
                .ForMember(dest => dest.NomeCliente, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "nomecliente")))
                .ForMember(dest => dest.PontoReferencia, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "pontoreferencia")))
                .ForMember(dest => dest.TelefoneCelular, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "telefonecelular")))
                .ForMember(dest => dest.TelefoneFixo, opt => opt.MapFrom((s, d) => ContratoResolver(s.Contrato, "telefonefixo")));

            CreateMap<VwCliente, Common.ViewModels.Cliente.LigacaoViewModel>()
                .ForMember(dest => dest.Ligacao, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.LogradouroEndereco, opt => opt.MapFrom(src => src.Endereco))
                .ForMember(dest => dest.NumeroEndereco, opt => opt.MapFrom(src => src.NumeroImovel))
                .ForMember(dest => dest.ComplementoEndereco, opt => opt.MapFrom(src => src.Complemento))
                .ForMember(dest => dest.BairroEndereco, opt => opt.MapFrom(src => src.NomeBairro));

            CreateMap<ClienteCallCenter, Common.ViewModels.CallCenter.ClienteResponseViewModel>();

            CreateMap<LigacaoCallCenter, Common.ViewModels.CallCenter.LigacaoViewModel>();

            CreateMap<InstanciaCallCenter, GerarAutenticacaoResponseViewModel>();

            CreateMap<ClienteContrato, Common.ViewModels.AgenciaVirtual.ClienteAutenticadoViewModel>();
            #endregion

            #region [Mensagem]

            CreateMap<EmailIntegradorResponse, EmailResponseViewModel>()
                .ForMember(dest => dest.Guid, opt => opt.MapFrom(src => src.MessageGuid))
                .ForMember(dest => dest.Sucesso, opt => opt.MapFrom(src => src.Success));

            #endregion

            #region [Faturamento]

            CreateMap<Conta, ContasContratoViewModel>()
                .ForMember(dest => dest.DataEmissao, opt => opt.MapFrom(src => src.Emissao))
                .ForMember(dest => dest.DataReferencia, opt => opt.MapFrom(src => src.AnoMes))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.SeqOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.SituacaoConta, opt => opt.MapFrom(src => SituacaoContaResolver(src)))
                .ForMember(dest => dest.ValorAgua, opt => opt.MapFrom(src => src.ValorAgua))
                .ForMember(dest => dest.ValorDesconto, opt => opt.MapFrom(src => ValorDescontoContaResolver(src)))
                .ForMember(dest => dest.ValorDescontoRetificado, opt => opt.MapFrom(src => src.ValorDescontoRetificado))
                .ForMember(dest => dest.ValorDevolucao, opt => opt.MapFrom(src => src.ValorDevolucao))
                .ForMember(dest => dest.ValorEsgoto, opt => opt.MapFrom(src => ValorEsgotoContaResolver(src)))
                .ForMember(dest => dest.ValorIcfrf, opt => opt.MapFrom(src => src.ValorIcfrf))
                .ForMember(dest => dest.ValorMultaJuros, opt => opt.MapFrom(src => ValorMultaEJurosContaResolver(src)))
                .ForMember(dest => dest.ValorRecursosHidricos, opt => opt.MapFrom(src => ValorRecursosHidricosContaResolver(src)))
                .ForMember(dest => dest.ValorServico, opt => opt.MapFrom(src => src.ValorServico))
                .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(src => src.ValorTotal))
                .ForMember(dest => dest.ValorTotalAtualizado, opt => opt.MapFrom(src => ValorTotalAtualizadoContaResolver(src)));

            CreateMap<VwGeraArquivoImpressao, ContaImpressaoViewModel>()
                .ForMember(dest => dest.AvisoDebitoCodigoBarras, opt => opt.MapFrom(src => NumeroCodigoBarrasAvisoDebitoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.AvisoDebitoCodigodeBarrasFormatado, opt => opt.MapFrom(src => FormatoCodigoBarrasAvisoDebitoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.AvisoDebitoLote, opt => opt.MapFrom(src => (src.LoteAvisoDebito.HasValue ? src.LoteAvisoDebito.Value.ToString() : string.Empty)))
                .ForMember(dest => dest.AvisoDebitoMensagemParte1, opt => opt.MapFrom(src => src.TextoMensagemAvisoDebito))
                .ForMember(dest => dest.AvisoDebitoNumero, opt => opt.MapFrom(src => (src.NumeroAvisoDebito.HasValue ? src.NumeroAvisoDebito.Value.ToString() : string.Empty)))
                .ForMember(dest => dest.CargaTributariaSobreValorServicos, opt => opt.MapFrom(src => PercentualCargaTributariaArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.CategoriaNumeroEconomiasComercial, opt => opt.MapFrom(src => src.EconomiaComercial))
                .ForMember(dest => dest.CategoriaNumeroEconomiasIndustrial, opt => opt.MapFrom(src => src.EconomiaIndustrial))
                .ForMember(dest => dest.CategoriaNumeroEconomiasPublica, opt => opt.MapFrom(src => src.EconomiaPublica))
                .ForMember(dest => dest.CategoriaNumeroEconomiasResidencial, opt => opt.MapFrom(src => src.EconomiaResidencial))
                .ForMember(dest => dest.Complemento, opt => opt.MapFrom(src => src.Complemento))
                .ForMember(dest => dest.ConsumoFaturado, opt => opt.MapFrom(src => (src.ConsumoFaturado.HasValue ? src.ConsumoFaturado.Value.ToString() : "0")))
                .ForMember(dest => dest.ConsumoMedido, opt => opt.MapFrom(src => (src.ConsumoMedido.HasValue ? src.ConsumoMedido.Value.ToString() : "0")))
                .ForMember(dest => dest.CpfCnpj, opt => opt.MapFrom(src => src.CpfCnpj))
                .ForMember(dest => dest.CreditoConsumo, opt => opt.MapFrom(src => (src.ConsumoCredito.HasValue ? src.ConsumoCredito.Value.ToString() : "0")))
                .ForMember(dest => dest.CustoRegulacaoFiscalizacao, opt => opt.MapFrom(src => (src.ValorCustoRegulacaoFiscalizacao.HasValue ? src.ValorCustoRegulacaoFiscalizacao.Value.ToString() : "0,00")))
                .ForMember(dest => dest.DataEmissao, opt => opt.MapFrom(src => DataEmissaoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.DataLeituraAnterior, opt => opt.MapFrom(src => DataLeituraAnteriorArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.DataLeituraAtual, opt => opt.MapFrom(src => DataLeituraAtualArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.DiasConsumo, opt => opt.MapFrom(src => (src.QtdeDiasConsumo.HasValue ? src.QtdeDiasConsumo.Value.ToString() : "0")))
                .ForMember(dest => dest.EnderecoLigacao, opt => opt.MapFrom(src => src.Endereco))
                .ForMember(dest => dest.IdentificadorDebitoAutomatico, opt => opt.MapFrom(src => IdentificadorDebitoAutomaticoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.InscricaoEstadualMunicipal, opt => opt.MapFrom(src => src.InscricaoMunicipalEstadual))
                .ForMember(dest => dest.LeituraAnterior, opt => opt.MapFrom(src => LeituraAnteriorArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.LeituraAtual, opt => opt.MapFrom(src => LeituraAtualArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.Ligacao, opt => opt.MapFrom(src => MatriculaArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.NomeRazaoSocial, opt => opt.MapFrom(src => src.NomeCliente))
                .ForMember(dest => dest.NumeroCodigodeBarras, opt => opt.MapFrom(src => NumeroCodigoBarrasArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.NumeroCodigodeBarrasFormatado, opt => opt.MapFrom(src => FormatoCodigoBarrasArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.NumeroConta, opt => opt.MapFrom(src => src.SeqOriginal))
                .ForMember(dest => dest.NumeroHidrometro, opt => opt.MapFrom(src => src.IdHd))
                .ForMember(dest => dest.Pipa, opt => opt.MapFrom(src => (src.ConsumoPipa.HasValue ? src.ConsumoPipa.Value.ToString() : "0")))
                .ForMember(dest => dest.PrevisaoProximaLeitura, opt => opt.MapFrom(src => DataProximaLeituraArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.QualidadeAguaSistemaAbastecimento, opt => opt.MapFrom(src => src.SistemaAbastecimento))
                .ForMember(dest => dest.QualidadeAguaObservacoes, opt => opt.MapFrom(src => src.Observacao))
                .ForMember(dest => dest.Referencia, opt => opt.MapFrom(src => src.AnoMesCt.ToReferencia()))
                .ForMember(dest => dest.Residual, opt => opt.MapFrom(src => (src.ConsumoResidual.HasValue ? src.ConsumoResidual.Value.ToString() : "0")))
                .ForMember(dest => dest.RetencaoTributos, opt => opt.MapFrom(src => ValorRetencaoImpostoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.Roteirizacao, opt => opt.MapFrom(src => RoteirizacaoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.SituacaoConta, opt => opt.MapFrom(src => (src.SituacaoConta.HasValue ? src.SituacaoConta.Value.ToString() : string.Empty)))
                .ForMember(dest => dest.TextoAlternativoCodigoBarras, opt => opt.MapFrom(src => TextoAlternativoCodigoBarrasArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.TipoEntrega, opt => opt.MapFrom(src => src.NomeTipoEntrega))
                .ForMember(dest => dest.TipoFaturamento, opt => opt.MapFrom(src => src.NomeTipoFaturamento))
                .ForMember(dest => dest.TotalPagar, opt => opt.MapFrom(src => ValorTotalArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.Vencimento, opt => opt.MapFrom(src => DataVencimentoArquivoImpressaoResolver(src)))
                .ForMember(dest => dest.Via, opt => opt.MapFrom(src => $"{src.Via}ª"));

            CreateMap<SendGridEmailSegundaViaContaAplicativoConfig, EmailRequestViewModel>();

            CreateMap<Conta, ImprimeGRP>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.Titulo, opt => opt.MapFrom(src => "GUIA DE PAGAMENTO"))
                .ForMember(dest => dest.ConsumoAnterior01, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior02, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior03, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior04, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior05, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior06, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior07, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior08, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior09, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior10, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior11, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoAnterior12, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.AnoMesCt, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ConsumoFaturado, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.LeituraAtual, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.LeituraAnterior, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.Media, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeCt01, opt => opt.MapFrom(src => "Pagamento de débito(s)"))
                .ForMember(dest => dest.CompoeCt02, opt => opt.MapFrom(src => "Parcelas existentes"))
                .ForMember(dest => dest.CompoeCt03, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt04, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt05, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt06, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt07, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt08, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt09, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeCt10, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CompoeValor01, opt => opt.MapFrom(src => src.ValorServico))
                .ForMember(dest => dest.CompoeValor03, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor04, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor05, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor06, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor07, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor08, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor09, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.CompoeValor10, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.EstruturaTarifaria01, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria02, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria03, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria04, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria05, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria06, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria07, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria08, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria09, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria10, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria11, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria12, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria13, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria14, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria15, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.EstruturaTarifaria16, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.ValorGRP, opt => opt.MapFrom(src => src.ValorServico))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.DataValor, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.Observacao, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.IdSolicitacao, opt => opt.MapFrom(src => 8));

            CreateMap<VwCliente, ImprimeGRP>()
                .ForMember(dest => dest.NomeCliente, opt => opt.MapFrom(src => src.Nome))
                .ForMember(dest => dest.Endereco, opt => opt.MapFrom(src => ImprimeGrpEnderecoResolver(src)))
                .ForMember(dest => dest.NomeLocal, opt => opt.MapFrom(src => src.NomeLocal))
                .ForMember(dest => dest.EconomiaResidencial, opt => opt.MapFrom(src => src.EconomiaResidencial))
                .ForMember(dest => dest.EconomiaComercial, opt => opt.MapFrom(src => src.EconomiaComercial))
                .ForMember(dest => dest.EconomiaIndustrial, opt => opt.MapFrom(src => src.EconomiaIndustrial))
                .ForMember(dest => dest.EconomiaPublica, opt => opt.MapFrom(src => src.EconomiaPublica))
                .ForMember(dest => dest.IdCiclo, opt => opt.MapFrom(src => src.IdCiclo))
                .ForMember(dest => dest.SetorRotaSequencia, opt => opt.MapFrom(src => ImprimeGrpSetorRotaSequenciaResolver(src)))
                .ForMember(dest => dest.IdHd, opt => opt.MapFrom(src => src.IdHd))
                .ForMember(dest => dest.TipoEntrega, opt => opt.MapFrom(src => src.TipoEntrega));

            CreateMap<Conta, TempCobrancaCt>()
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.IdUsuario, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.AnoMesCt, opt => opt.MapFrom(src => src.AnoMes))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.ValorOriginal, opt => opt.MapFrom(src => TempCobrancaCtValorOriginalResolver(src)))
                .ForMember(dest => dest.SeqOriginalPago, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Localizacao, opt => opt.MapFrom(src => src.Localizacao));

            //Geração de quitação de débito
            CreateMap<Conta, Conta>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => default(long)))
                .ForMember(dest => dest.TipoConta, opt => opt.MapFrom(src => TipoContaEnum.DocumentoNaoFiscal))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.Localizacao, opt => opt.MapFrom(src => src.Localizacao))
                .ForMember(dest => dest.AnoMes, opt => opt.MapFrom(src => DateTime.Today.ToString("yyyyMM")))
                .ForMember(dest => dest.IdCiclo, opt => opt.MapFrom(src => src.IdCiclo))
                .ForMember(dest => dest.SituacaoConta, opt => opt.MapFrom(src => SituacaoContaEnum.NaoPaga))
                .ForMember(dest => dest.Vencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.TipoEntrega, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.IdCentralizador, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.QuantidadeDebito, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.QuantidadePagamento, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.IdMensagemMes, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.IdAviso, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.PagamentoPrazo, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.PagamentoFora, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorAgua, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorEsgoto, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorComercial, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorICMS, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorMulta, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorIcfrf, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDesconto, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorJuro, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorTerceiro, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorCorrecaoMonetaria, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDevolucao, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorRecursosHidricosAgua, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorRecursosHidricosEsgoto, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDescontoResidencialSocial, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDescontoPequenoComercio, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorEsgotoAlternativo, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDescontoLigacaoEstimada, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDescontoRetificado, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorDescontoPoderPublicoConcedente, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.Emissao, opt => opt.MapFrom(src => DateTime.Today))
                .ForMember(dest => dest.IdContrato, opt => opt.MapFrom(src => src.IdContrato));

            CreateMap<Conta, CompoeCt>()
                .ForMember(dest => dest.DocumentoOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Valor, opt => opt.MapFrom(src => src.ValorTotal))
                .ForMember(dest => dest.TipoContaDocumentoPago, opt => opt.MapFrom(src => 2));

            CreateMap<Conta, GRP>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DataHoraEmissaoGRP, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.NroOs, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ValorGRP, opt => opt.MapFrom(src => src.ValorServico))
                .ForMember(dest => dest.DataVencimentoGRP, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.DataValorGRP, opt => opt.MapFrom(src => src.Vencimento));

            CreateMap<ProtocoloAtendimento, GRP>()
                .ForMember(dest => dest.NomeSolicitante, opt => opt.MapFrom(src => src.IdTipoAtendimento.GetDescription()))
                .ForMember(dest => dest.Observacao, opt => opt.MapFrom(src => GRPObservacaoResolver(src)))
                .ForMember(dest => dest.IdProtocoloAtendimento, opt => opt.MapFrom(src => src.Id));

            CreateMap<ImprimeGRP, Common.ViewModels.Faturamento.CodigoBarrasResponseViewModel>()
                .ForMember(dest => dest.SeqOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.DataVencimento))
                .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(src => src.ValorGRP))
                .ForMember(dest => dest.CodigoBarras, opt => opt.MapFrom(src => src.CodigoBarra.ToBarCode()))
                .ForMember(dest => dest.Sucesso, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.CodigoBarra)));

            CreateMap<Conta, Common.ViewModels.Faturamento.CodigoBarrasResponseViewModel>()
                .ForMember(dest => dest.SeqOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(src => src.ValorTotal));

            CreateMap<SendGridQuitacaoDebitoConfig, EmailRequestViewModel>();

            CreateMap<DeclaracaoAnual, DeclaracaoAnualViewModel>()
                .ForMember(dest => dest.SeqDeclaracao, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AnoReferencia, opt => opt.MapFrom(src => src.AnoRef))
                .ForMember(dest => dest.TipoDeclaracao, opt => opt.MapFrom(src => src.TipoDeclaracao.GetDescription()));

            CreateMap<Conta, ContaResponseViewModel>()
                .ForMember(dest => dest.SeqOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DataReferencia, opt => opt.MapFrom(src => DataReferenciaContaResolver(src)))
                .ForMember(dest => dest.DataEmissao, opt => opt.MapFrom(src => src.Emissao))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.Vencimento))
                .ForMember(dest => dest.IdSituacao, opt => opt.MapFrom(src => (int)src.SituacaoConta))
                .ForMember(dest => dest.DsSituacao, opt => opt.MapFrom(src => src.SituacaoConta.GetDescription()))
                .ForMember(dest => dest.IdContrato, opt => opt.MapFrom(src => src.IdContrato))
                .ForMember(dest => dest.Matricula, opt => opt.MapFrom(src => src.Matricula))
                .ForMember(dest => dest.VlAgua, opt => opt.MapFrom(src => src.ValorAgua))
                .ForMember(dest => dest.VlDesconto, opt => opt.MapFrom(src => ValorDescontoContaResolver(src)))
                .ForMember(dest => dest.VlDescontoRetificado, opt => opt.MapFrom(src => src.ValorDescontoRetificado))
                .ForMember(dest => dest.VlDevolucao, opt => opt.MapFrom(src => src.ValorDevolucao))
                .ForMember(dest => dest.VlEsgoto, opt => opt.MapFrom(src => ValorEsgotoContaResolver(src)))
                .ForMember(dest => dest.VlIcfrf, opt => opt.MapFrom(src => src.ValorIcfrf))
                .ForMember(dest => dest.VlMultaJuros, opt => opt.MapFrom(src => ValorMultaEJurosContaResolver(src)))
                .ForMember(dest => dest.VlRecHidricos, opt => opt.MapFrom(src => ValorRecursosHidricosContaResolver(src)))
                .ForMember(dest => dest.VlServico, opt => opt.MapFrom(src => src.ValorServico))
                .ForMember(dest => dest.VlTotal, opt => opt.MapFrom(src => src.ValorTotal))
                .ForMember(dest => dest.VlTotalAtualizado, opt => opt.MapFrom(src => ValorTotalAtualizadoContaResolver(src)))
                .ForMember(dest => dest.DsSitNegativacao, opt => opt.MapFrom(src => SituacaoNegativacaoContaResolver(src)));

            CreateMap<ImprimeGRP, Common.ViewModels.AgenciaVirtual.CodigoBarrasResponseViewModel>()
                .ForMember(dest => dest.SeqOriginal, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DataVencimento, opt => opt.MapFrom(src => src.DataVencimento))
                .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(src => src.ValorGRP))
                .ForMember(dest => dest.CodigoBarras, opt => opt.MapFrom(src => src.CodigoBarra.ToBarCode()))
                .ForMember(dest => dest.Sucesso, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.CodigoBarra)));

            #endregion

            #region [Pdf Generico]

            CreateMap<PdfGenModeloDoc, PdfDocumentLayout>()
                .ForMember(dest => dest.CustomPageHeight, opt => opt.MapFrom(src => src.AlturaPagina))
                .ForMember(dest => dest.CustomPageWidth, opt => opt.MapFrom(src => src.LarguraPagina))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Descricao))
                .ForMember(dest => dest.IdDocType, opt => opt.MapFrom(src => src.IdTipoDocumento))
                .ForMember(dest => dest.IdPdfItemLayout, opt => opt.MapFrom(src => src.IdItemDocumento))
                .ForMember(dest => dest.ItemsPerPage, opt => opt.MapFrom(src => src.ItensPorPagina))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Nome))
                .ForMember(dest => dest.PageSize, opt => opt.MapFrom(src => src.TamanhoPagina))
                .ForMember(dest => dest.RepetitionOffset, opt => opt.MapFrom(src => src.OffsetRepeticao))
                .ForMember(dest => dest.RepsOrientation, opt => opt.MapFrom(src => src.OrientacaoRepeticoes));

            CreateMap<PdfGenItemDoc, PdfItemLayout>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Nome))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Descricao))
                .ForMember(dest => dest.IdDocType, opt => opt.MapFrom(src => src.IdTipoDocumento))
                .ForMember(dest => dest.PdfLayoutItemType, opt => opt.MapFrom(src => (PdfLayoutItemTypeEnum)src.TipoItemDocumento))
                .ForMember(dest => dest.Style, opt => opt.MapFrom((s, d) => PdfLayoutItemStyleResolver(s, d)));

            #endregion

            #region [Consumo]

            CreateMap<Consumo, ConsumosMatriculaViewModel>()
                .ForMember(dest => dest.ConsumoFaturado, opt => opt.MapFrom(src => src.Faturado))
                .ForMember(dest => dest.DataReferencia, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QtdeDiasLeitura, opt => opt.MapFrom(src => src.QtdeDiasLeitura));

            #endregion

            #region [Servico]

            CreateMap<Interrupcao, InterrupcaoAbastecimentoResponseViewModel>()
                .ForMember(dest => dest.DataHoraInicio, opt => opt.MapFrom(src => src.DataHoraInicio))
                .ForMember(dest => dest.DataHoraFim, opt => opt.MapFrom(src => src.DataHoraFim));

            CreateMap<VwDadosConsultaOrdemServico, ConsultaOsResponseViewModel>();

            CreateMap<InformacaoConta, InformacaoContaResponseViewModel>();

            #endregion

            #region [Perdas]

            CreateMap<TabMeterwiseHD, MI_STRUCT_Meter_List>();
            CreateMap<Leitura, MI_STRUCT_Reading_List>();
            CreateMap<LeituraMacro, MI_STRUCT_Reading_List>();




            #endregion
        }

        #region [Métodos privados]

        #region [Cliente]

        private bool ContaDigitalResolver<TModel>(Cliente source, TModel dest)
        {
            return source.IdGrupoEntrega == GrupoEntregaEnum.EMAIL;
        }

        private string GrupoEntregaResolver<TModel>(Cliente source, TModel dest)
        {
            return source.IdGrupoEntrega.GetDescription();
        }

        private bool ClienteSobJudiceResolver<TModel>(ProcessoCliente source, TModel dest, short valueSource)
        {
            return valueSource == 2 ? false : true;
        }

        private string EnderecoCompletoResolver<TModel>(Cliente source, TModel dest)
        {
            var logradouro = string.Empty;
            var numero = string.Empty;
            var complemento = string.Empty;
            var logradouroTipo = string.Empty;

            if (source.Contrato != null)
            {
                if (source.Contrato.Logradouro != null) logradouro = source.Contrato.Logradouro.Nome;
                if (source.Contrato.Logradouro != null && source.Contrato.Logradouro.LogradouroTipo != null) logradouroTipo = $"{source.Contrato.Logradouro.LogradouroTipo.Nome} ";
                if (!string.IsNullOrEmpty(source.Contrato.Numero) && !string.IsNullOrWhiteSpace(source.Contrato.Numero)) numero = $", nº {source.Contrato.Numero}";
                if (!string.IsNullOrEmpty(source.Contrato.Complemento) && !string.IsNullOrWhiteSpace(source.Contrato.Complemento)) complemento = $" - {source.Contrato.Complemento}";
            }

            return $"{logradouroTipo}{logradouro}{numero}{complemento}";
        }

        private string ContratoResolver(Contrato source, string property = "")
        {
            if (source is null) return string.Empty;
            switch (property.ToLower())
            {
                case "cpfcnpj":
                    return source.CpfCnpj;
                case "complemento":
                    return source.Complemento;
                case "email":
                    return source.Email;
                case "nomecliente":
                    return source.NomeCliente;
                case "numero":
                    return source.Numero;
                case "pontoreferencia":
                    return source.PontoReferencia;
                case "telefonefixo":
                    return source.TelefoneResidencial;
                case "telefonecelular":
                    return source.Celular;
                default:
                    return string.Empty;
            }
        }

        private string ContratoLogradouroResolver(Contrato source, Type type, string property = "")
        {
            if (source?.Logradouro != null)
            {
                if (type == typeof(Bairro)) return (source.Logradouro.Bairro is null) ? string.Empty : source.Logradouro.Bairro.Nome;
                if (type == typeof(Cidade)) return (source.Logradouro.Cidade is null) ? string.Empty : source.Logradouro.Cidade.Nome;
                if (type == typeof(Estado)) return (source.Logradouro.Estado is null) ? string.Empty : source.Logradouro.Estado.Nome;
                if (type == typeof(LogradouroTipo)) return (source.Logradouro.LogradouroTipo is null) ? string.Empty : source.Logradouro.LogradouroTipo.Nome;
                if (type == typeof(string) && property.ToLower() == "cep") return source.Logradouro.Cep;
                if (type == typeof(string) && property.ToLower() == "nome") return source.Logradouro.Nome;
                if (type == typeof(string) && property.ToLower() == "logradouro")
                {
                    var logradouro = string.Empty;
                    var logradouroTipo = string.Empty;
                    if (source.Logradouro != null) logradouro = source.Logradouro.Nome;
                    if (source.Logradouro != null && source.Logradouro.LogradouroTipo != null) logradouroTipo = $"{source.Logradouro.LogradouroTipo.Nome} ";
                    return $"{logradouroTipo}{logradouro}";
                }
            }
            return string.Empty;
        }

        #endregion

        #region [PDF Genérico]

        private PdfLayoutItemStyle PdfLayoutItemStyleResolver(PdfGenItemDoc source, PdfItemLayout dest)
        {
            var style = dest.Style ?? new PdfLayoutItemStyle();

            #region [FontFormat]

            if (source.EstiloFonteFormato.HasValue) style.FontFormat = (PdfFontFormatEnum)source.EstiloFonteFormato.Value;
            else if (dest.Parent != null) style.FontFormat = dest.Parent.Style.FontFormat;

            #endregion

            #region [FontName]

            if (source.EstiloFonteNome.HasValue) style.FontName = (PdfFontNameEnum)source.EstiloFonteNome.Value;
            else if (dest.Parent != null) style.FontName = dest.Parent.Style.FontName;

            #endregion

            #region [FontSize]

            if (source.EstiloFonteTamanho.HasValue) style.FontSize = (PdfFontSize)source.EstiloFonteTamanho.Value;
            else if (dest.Parent != null) style.FontSize = dest.Parent.Style.FontSize;

            #endregion

            #region [ShadowText]

            if (source.EstiloSombra.HasValue) style.ShadowText = source.EstiloSombra.Value == 1;
            else if (dest.Parent != null) style.ShadowText = dest.Parent.Style.ShadowText;

            #endregion

            #region [HorizontalAlignment]

            if (source.EstiloAlinhamentoHorizontal.HasValue) style.HorizontalAlignment = (PdfHorizontalAlignmentEnum)source.EstiloAlinhamentoHorizontal.Value;
            else if (dest.Parent != null) style.HorizontalAlignment = dest.Parent.Style.HorizontalAlignment;

            #endregion

            #region [VerticalAlignment]

            if (source.EstiloAlinhamentoVertical.HasValue) style.VerticalAlignment = (PdfVerticalAlignmentEnum)source.EstiloAlinhamentoVertical.Value;
            else if (dest.Parent != null) style.VerticalAlignment = dest.Parent.Style.VerticalAlignment;

            #endregion

            #region [Position]

            if (source.EstiloPosicao.HasValue) style.Position = (PdfPositionEnum)source.EstiloPosicao.Value;

            #endregion

            #region [Left]

            if (source.EstiloPosicaoEsquerda.HasValue) style.Left = source.EstiloPosicaoEsquerda.Value;

            #endregion

            #region [Top]

            if (source.EstiloPosicaoTopo.HasValue) style.Top = source.EstiloPosicaoTopo.Value;

            #endregion

            #region [Height]

            if (source.EstiloAltura.HasValue) style.Height = source.EstiloAltura.Value;

            #endregion

            #region [Width]

            if (source.EstiloLargura.HasValue) style.Width = source.EstiloLargura.Value;

            #endregion

            #region [MarginLeft]

            if (source.EstiloMargemEsquerda.HasValue) style.MarginLeft = source.EstiloMargemEsquerda.Value;

            #endregion

            #region [MarginTop]

            if (source.EstiloMargemTopo.HasValue) style.MarginTop = source.EstiloMargemTopo.Value;

            #endregion

            #region [RepetitionStyle]

            if (source.EstiloRepeticaoOrientacao.HasValue) style.RepetitionStyle = (PdfRepetitionOrientationEnum)source.EstiloRepeticaoOrientacao.Value;

            #endregion

            #region [RepetitionMax]

            if (source.EstiloRepeticaoMaxima.HasValue) style.RepetitionMax = source.EstiloRepeticaoMaxima.Value;

            #endregion

            #region [RepetitionOffset]

            if (source.EstiloRepeticaoEspacamento.HasValue) style.RepetitionOffset = source.EstiloRepeticaoEspacamento.Value;

            #endregion

            #region [WordWrapLeading]

            if (source.EstiloEspacoQuebraLinha.HasValue) style.WordWrapLeading = source.EstiloEspacoQuebraLinha.Value;

            #endregion

            return style;
        }

        #endregion

        #region [Faturamento]

        private string SituacaoContaResolver(Conta source)
        {
            return (source.SituacaoConta == SituacaoContaEnum.NaoPaga && DateTime.Today > source.Vencimento) ? "EM ATRASO" : source.SituacaoConta.GetDescription();
        }

        private decimal ValorDescontoContaResolver(Conta source)
        {
            return source.ValorDesconto + source.ValorDescontoLigacaoEstimada + source.ValorDescontoPequenoComercio + source.ValorDescontoPoderPublicoConcedente + source.ValorDescontoResidencialSocial;
        }

        private decimal ValorEsgotoContaResolver(Conta source)
        {
            return source.ValorEsgoto + source.ValorEsgotoAlternativo;
        }

        private decimal ValorMultaEJurosContaResolver(Conta source)
        {
            return source.ValorMulta + source.ValorJuro;
        }

        private decimal ValorRecursosHidricosContaResolver(Conta source)
        {
            return source.ValorRecursosHidricosAgua + source.ValorRecursosHidricosEsgoto;
        }

        private decimal ValorTotalAtualizadoContaResolver(Conta source)
        {
            return source.ValorTotal - source.ValorDescontoRetificado;
        }

        private string MatriculaArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return !string.IsNullOrEmpty(source.DigitoMatricula) ? $"{source.Matricula} - {source.DigitoMatricula}" : source.Matricula;
        }

        private string DataEmissaoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.DataEmissao.HasValue ? source.DataEmissao.Value.ToString("dd/MM/yyyy") : string.Empty;
        }

        private string DataLeituraAnteriorArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.DataLeituraAnterior.HasValue ? source.DataLeituraAnterior.Value.ToString("dd/MM/yyyy") : string.Empty;
        }

        private string DataLeituraAtualArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.DataLeituraAtual.HasValue ? source.DataLeituraAtual.Value.ToString("dd/MM/yyyy") : string.Empty;
        }

        private string DataProximaLeituraArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.DataProximaLeitura.HasValue ? source.DataProximaLeitura.Value.ToString("dd/MM/yyyy") : string.Empty;
        }

        private string DataVencimentoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.DataVencimento.HasValue ? source.DataVencimento.Value.ToString("dd/MM/yyyy") : string.Empty;
        }

        private string RoteirizacaoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return !string.IsNullOrEmpty(source.Roteirizacao) && !"..".Equals(source.Roteirizacao) ? source.Roteirizacao : string.Empty;
        }

        private string LeituraAnteriorArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.LeituraAnterior.HasValue && source.LeituraAnterior.Value < 0 ? "S/L" : source.LeituraAnterior.Value.ToString();
        }

        private string LeituraAtualArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.LeituraAtual.HasValue && source.LeituraAtual.Value < 0 ? "S/L" : source.LeituraAtual.Value.ToString();
        }

        private string IdentificadorDebitoAutomaticoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return !string.IsNullOrEmpty(source.CompoeCt11) ? source.CompoeCt11 : string.Empty;
        }

        private string ValorRetencaoImpostoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.ValorRetecaoImposto.HasValue ? source.ValorRetecaoImposto.Value.ToString("N2") : "0,00";
        }

        private string ValorTotalArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.ValorTotal.HasValue ? source.ValorTotal.Value.ToString("N2") : "0,00";
        }

        private string PercentualCargaTributariaArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            return source.PercentualCargaTributaria.HasValue ? $"{source.PercentualCargaTributaria.Value.ToString("N2")}%" : string.Empty;
        }

        private string TextoAlternativoCodigoBarrasArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            if (!source.ImpressaoCodigoBarra.HasValue) return string.Empty;
            if (source.ImpressaoCodigoBarra.Value == 0) return "CONTA EM DÉBITO AUTOMÁTICO";
            if (source.ImpressaoCodigoBarra.Value == 2) return "CONTA EM FATURÃO";
            if (source.ImpressaoCodigoBarra.Value == 3) return source.Mensagem;
            return string.Empty;
        }

        private string NumeroCodigoBarrasArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            if (!source.ImpressaoCodigoBarra.HasValue) return string.Empty;
            if (source.ImpressaoCodigoBarra.Value == 1) return Regex.Replace(source.CodigoBarra, @"(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})", @"$1$3$5$7");
            return string.Empty;
        }

        private string NumeroCodigoBarrasAvisoDebitoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            if (!source.SituacaoConta.HasValue || string.IsNullOrEmpty(source.CodigoBarraAvisoDebito)) return string.Empty;
            if (source.SituacaoConta.Value == 0) return Regex.Replace(source.CodigoBarraAvisoDebito, @"(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})", @"$1$3$5$7");
            return string.Empty;
        }

        private string FormatoCodigoBarrasArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            if (!source.ImpressaoCodigoBarra.HasValue) return string.Empty;
            if (source.ImpressaoCodigoBarra.Value == 1) return Regex.Replace(source.CodigoBarra, @"(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})", @"$1-$2   $3-$4   $5-$6   $7-$8");
            return string.Empty;
        }

        private string FormatoCodigoBarrasAvisoDebitoArquivoImpressaoResolver(VwGeraArquivoImpressao source)
        {
            if (!source.SituacaoConta.HasValue || string.IsNullOrEmpty(source.CodigoBarraAvisoDebito)) return string.Empty;
            if (source.SituacaoConta.Value == 0) return Regex.Replace(source.CodigoBarraAvisoDebito, @"(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})(\w{11})(\w{1})", @"$1-$2   $3-$4   $5-$6   $7-$8");
            return string.Empty;
        }

        private string ImprimeGrpEnderecoResolver(VwCliente source)
        {
            var logradouro = string.Empty;
            var numero = string.Empty;
            var complemento = string.Empty;

            if (source.Endereco != null) logradouro = source.Endereco;
            if (!string.IsNullOrEmpty(source.NumeroImovel) && !string.IsNullOrWhiteSpace(source.NumeroImovel)) numero = $", nº {source.NumeroImovel}";
            if (!string.IsNullOrEmpty(source.Complemento) && !string.IsNullOrWhiteSpace(source.Complemento)) complemento = $" - {source.Complemento}";

            return $"{logradouro}{numero}{complemento} - {source.NomeBairro} - {source.NomeCidade}";
        }

        private string ImprimeGrpSetorRotaSequenciaResolver(VwCliente source)
        {
            return $"{source.IdSetor.Substring(4, 3)}.{source.Rota}.{source.Sequencia}";
        }

        private double TempCobrancaCtValorOriginalResolver(Conta source)
        {
            return (double)(source.ValorTotal - source.ValorDescontoRetificado);
        }

        private string GRPObservacaoResolver(ProtocoloAtendimento source)
        {
            return $"Gerado pelo canal de atendimento {source.IdTipoAtendimento.GetDescription()}";
        }

        private string SituacaoNegativacaoContaResolver(Conta source)
        {
            return source.SituacaoNegativacao.ToString().ToInt().ToEnum(SituacaoSerasaNegativacaoEnum.Invalido).GetDescription();
        }

        private string DataReferenciaContaResolver(Conta source)
        {
            return $"{source.AnoMes.Substring(4, 2)}/{source.AnoMes.Substring(0, 4)}";
        }

        #endregion

        #endregion

    }
    
}
