﻿using AutoMapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Common.ViewModels.Servico;
using GAB.Inova.Domain;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace GAB.Inova.Business.Mappings
{
    public class ViewModelToDomainMappingProfile
        : Profile
    {
        public ViewModelToDomainMappingProfile()
        {

            #region [Atendimento]

            CreateMap<NaturezaSolicitacaoViewModel, TabNaturezaSolicitacao>()
                .ForMember(m => m.Id, o => o.MapFrom(s => s.IdNatureza))
                .ForMember(m => m.Descricao, o => o.MapFrom(s => s.Descricao));

            CreateMap<CancelarIntegracaoGfaRequestViewModel, ProtocoloAtendimentoGfa>()
                .ForMember(m => m.IdIntegrador, o => o.MapFrom(s => IntegradorGfaEnum.Atend))
                .ForMember(m => m.DataInicioAtendimento, o => o.MapFrom(s => DateTime.Now));

            CreateMap<FinalizarIntegracaoGfaRequestViewModel, ProtocoloAtendimentoGfa>()
                .ForMember(m => m.IdIntegrador, o => o.MapFrom(s => IntegradorGfaEnum.Atend))
                .ForMember(m => m.DataInicioAtendimento, o => o.MapFrom(s => DateTime.Now));

            CreateMap<IniciarIntegracaoGfaRequestViewModel, ProtocoloAtendimentoGfa>()
                .ForMember(m => m.IdIntegrador, o => o.MapFrom(s => IntegradorGfaEnum.Atend))
                .ForMember(m => m.DataInicioAtendimento, o => o.MapFrom(s => DateTime.Now));

            CreateMap<CancelarIntegracaoGfaRequestViewModel, CancelarIntegracaoGfaResponseViewModel>()
                .ReverseMap();

            CreateMap<FinalizarIntegracaoGfaRequestViewModel, FinalizarIntegracaoGfaResponseViewModel>()
                .ReverseMap();

            CreateMap<IniciarIntegracaoGfaRequestViewModel, IniciarIntegracaoGfaResponseViewModel>()
                .ReverseMap();

            CreateMap<VerificarIntegracaoGfaRequestViewModel, VerificarIntegracaoGfaResponseViewModel>()
                .ReverseMap();

            #endregion

            #region OS Eletronica

            CreateMap<OsEletronicaViewModel, OrdemServicoEletronica>()
                .ForMember(d => d.Latitude, o => o.MapFrom(s => s.Latitude.HasValue ? s.Latitude.Value.ToString("N8") : null))
                .ForMember(d => d.Longitude, o => o.MapFrom(s => s.Longitude.HasValue ? s.Longitude.Value.ToString("N8") : null))
                .ForMember(d => d.HidrometroID, o => o.MapFrom(s => !string.IsNullOrEmpty(s.HidrometroID) ? s.HidrometroID.ToUpper() : null))
                .ForMember(d => d.HidrometroInstaladoID, o => o.MapFrom(s => !string.IsNullOrEmpty(s.HidrometroInstaladoID) ? s.HidrometroInstaladoID.ToUpper() : null))
                .ForMember(d => d.NumeroOS, o => o.MapFrom((s, d) => OSEletronicaIdentificadorOsToNumeroOsResolver(s, d)))
                .ForMember(d => d.BairroID, o => o.MapFrom((s, d) => OSEletronicaIdBairroResolver(s, d)))
                .ForMember(d => d.CodigoMunicipio, o => o.MapFrom(s => s.CidadeID))
                .ForMember(d => d.LocalInstalacaoHidrometroID, o => o.MapFrom(s => s.LocalInstalacaoHidrometroID ?? 3))
                .ForMember(d => d.FrequenciaUtilizacaoImovelID, o => o.MapFrom((s, d) => OSEletronicaFrequenciaUtilizacaoImovelResolver(s, d)))
                .ForMember(d => d.IdentificadorOS, o => o.MapFrom(s => s.IdentificadorExterno))
                .ForMember(d => d.IdentificadorExterno, o => o.MapFrom(s => s.IdentificadorOS)); //a inversão foi solicitada pela fabrica do GeoCall(nutelinha)

            CreateMap<OsEletronicaLinkAnexoViewModel, OrdemServicoAnexoExterno>();

            #endregion

            #region [Cliente - Conta digital]

            CreateMap<ClienteResponseViewModel, Cliente>();

            CreateMap<EmailRequestViewModel, MensagemIntegrador>()
                .ForMember(d => d.Integrador, o => o.MapFrom(s => IntegradorTipoEnum.SendGrid))
                .ForMember(d => d.TemplateId, o => o.MapFrom(s => s.TemplateId))
                .ForMember(d => d.Tipo, o => o.MapFrom(s => s.Tipo))
                .ForMember(d => d.De, o => o.MapFrom(s => s.RemetenteEnderecoEmail))
                .ForMember(d => d.Destinatarios, o => o.MapFrom((s, d) => MapDestinatariosMensagemIntegrador(s, d)))
                .ForMember(d => d.Conteudo, o => o.MapFrom(s => JsonConvert.SerializeObject(s.Conteudo)));

            #endregion

        }

        #region [Métodos privados]

        private long? OSEletronicaIdentificadorOsToNumeroOsResolver(OsEletronicaViewModel source, OrdemServicoEletronica dest)
        {
            if (!string.IsNullOrEmpty(source.IdentificadorExterno)) //a inversão foi solicitada pela fabrica do GeoCall(nutelinha)
            {
                var onlyNumbers = new String(source.IdentificadorExterno.Where(Char.IsNumber).ToArray());
                return onlyNumbers.Length > 0 ? long.Parse(onlyNumbers) : default(long?);
            }
            return default(long?);
        }
        
        private long? OSEletronicaIdBairroResolver(OsEletronicaViewModel source, OrdemServicoEletronica dest)
        {
            if (source.BairroID.HasValue)
            {
                var sourceValueToString = source.BairroID.ToString();
                if (sourceValueToString.Length == 10)
                {
                    return long.Parse(sourceValueToString.Remove(0, 2));
                }
                else if (sourceValueToString.Length == 9)
                {
                    return long.Parse(sourceValueToString.Remove(0, 1));
                }
                else
                {
                    throw new InvalidCastException("O Campo de 'BairroID' foi informado fora do padrão definido!");
                }
            }
            return default(long?);
        }

        private long? OSEletronicaIdCidadeResolver(OsEletronicaViewModel source, OrdemServicoEletronica dest)
        {
            if (!string.IsNullOrEmpty(source.CidadeID))
            {
                if (source.CidadeID.Length == 10)
                {
                    return long.Parse(source.CidadeID.Remove(0, 2));
                }
                else if (source.CidadeID.Length == 9)
                {
                    return long.Parse(source.CidadeID.Remove(0, 1));
                }
                else
                {
                    throw new InvalidCastException("O Campo de 'CidadeID' foi informado fora do padrão definido!");
                }
            }

            return default(long?);
        }

        private long? OSEletronicaFrequenciaUtilizacaoImovelResolver(OsEletronicaViewModel source, OrdemServicoEletronica dest)
        {
            return (source.FrequenciaUtilizacaoImovelID.HasValue && source.FrequenciaUtilizacaoImovelID.Value == 0) ? 6 : source.FrequenciaUtilizacaoImovelID;
        }

        private string MapDestinatariosMensagemIntegrador(EmailRequestViewModel source, MensagemIntegrador dest)
        {
            return string.IsNullOrEmpty(dest.Destinatarios) ? source.Destinatarios : $"{dest.Destinatarios};{source.Destinatarios}";
        }

        #endregion
    }
}
