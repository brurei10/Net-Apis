﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Consumo;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;

namespace GAB.Inova.Business
{
    public class ConsumoBusiness
        : IConsumoBusiness
    {

        readonly IMapper _mapper;
        readonly IConsumoRepository _consumoRepository;
        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        readonly ILogger<ConsumoBusiness> _logger;

        public ConsumoBusiness(IMapper mapper,
                               ILoggerFactory loggerFactory, 
                               IConsumoRepository consumoRepository, 
                               IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            _mapper = mapper;
            _consumoRepository = consumoRepository;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;

            _logger = loggerFactory.CreateLogger<ConsumoBusiness>();
        }

        public async Task<HistoricoConsumosResponseViewModel> ObterHistoricoConsumosAsync(string numeroProtocolo)
        {
            var retorno = new HistoricoConsumosResponseViewModel()
            {
                Sucesso = false
            };

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);
            if (protocoloAtendimento is null)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não existe [nº:{numeroProtocolo}].");
                return retorno;
            }

            if (string.IsNullOrEmpty(protocoloAtendimento.Matricula))
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não possui uma ligação vinculada [nº:{numeroProtocolo}].");
                return retorno;
            }

            retorno.Sucesso = true;

            var consumos = await _consumoRepository.ObterUltimosDozeConsumosPorMatriculaAsync(protocoloAtendimento.Matricula);
            if (consumos.Any())
            {
                retorno.ConsumosMatricula = _mapper.Map<IList<ConsumosMatriculaViewModel>>(consumos);
            }
            else retorno.AdicionaMensagem("Você não possui histórico de consumos.");

            return retorno;
        }

    }
}
