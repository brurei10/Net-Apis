﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Common.ViewModels.Token;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class AutenticacaoBusiness
        : IAutenticacaoBusiness
    {

        readonly IMapper _mapper;
        readonly IConfiguration _configuration;
        readonly ITokenProvider _tokenProvider;
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly IClienteRepository _clienteRepository;
        readonly ILogger _logger;

        public AutenticacaoBusiness(IMapper mapper,
                                    IConfiguration configuration,
                                    ITokenProvider tokenProvider,
                                    IHttpContextAccessor httpContextAccessor,
                                    IClienteRepository clienteRepository,
                                    ILoggerFactory loggerFactory)
        {
            _mapper = mapper;
            _configuration = configuration;
            _tokenProvider = tokenProvider;
            _httpContextAccessor = httpContextAccessor;
            _clienteRepository = clienteRepository;
            
            _logger = loggerFactory.CreateLogger<AutenticacaoBusiness>();
        }

        public async Task<AutenticarClienteResponseViewModel> AutenticarClienteAsync(AutenticarClienteRequestViewModel viewModel, IEnumerable<EmpresaTipoEnum> indisponibilidades = null)
        {
            var retorno = new AutenticarClienteResponseViewModel()
            {
                Sucesso = false
            };

            var codigoEmpresa = ObterCodigoEmpresaPelaLigacao(viewModel.Ligacao);

            if (codigoEmpresa == EmpresaTipoEnum.NULO || !viewModel.CpfCnpj.IsValidCpfOrCnpj())
            {
                retorno.AdicionaMensagem("Dados inválidos.");
                return retorno;
            }

            if (indisponibilidades != null && indisponibilidades.Any())
            {
                if (VerificarIndisponibilidade(codigoEmpresa, indisponibilidades, retorno)) return retorno;
            }

            var tokenResponse = await GerarTokenResponseAsync(viewModel, codigoEmpresa);

            if (tokenResponse.Sucesso)
            {
                var cliente = await _clienteRepository.ObterPorMatriculaECpfCnpjAsync(viewModel.Ligacao, viewModel.CpfCnpj);
                if (cliente is null)
                {
                    retorno.AdicionaMensagem("Dados inválidos.");
                    return retorno;
                }

                retorno.ClienteAutenticado = _mapper.Map<ClienteAutenticadoViewModel>(cliente);
                retorno.CodigoEmpresa = codigoEmpresa.ToString();
                retorno.Token = tokenResponse.Token;
                retorno.Sucesso = true;
            }
            else retorno.AdicionaMensagem("Falha no serviço de autenticação.");

            return retorno;
        }

        #region [Métodos privados]

        /// <summary>
        /// Obter o código da empresa através da ligação
        /// </summary>
        /// <param name="ligacao">Indica uma ligação de um cliente</param>
        /// <returns><see cref="EmpresaTipoEnum"></returns>
        private EmpresaTipoEnum ObterCodigoEmpresaPelaLigacao(string ligacao)
        {
            if (string.IsNullOrEmpty(ligacao) || ligacao.Length != 10) return EmpresaTipoEnum.NULO;
            return ligacao.Substring(0, 2).ToInt().ToEnum(EmpresaTipoEnum.NULO);
        }

        /// <summary>
        /// Verificar se o serviço está indisponível para a concessionária
        /// </summary>
        /// <param name="codigoEmpresa">Indica uma concessão/empresa</param>
        /// <param name="indisponibilidades">Indica a lista de empresas indisponíveis</param>
        /// <param name="retorno">Indica um objeto/classe modelo<see cref="BaseResponseViewModel"/></param>
        /// <returns><see cref="true">Serviço indisponível para concessão</see><seealso cref="false">Serviço disponível para concessão</seealso></returns>
        private bool VerificarIndisponibilidade(EmpresaTipoEnum codigoEmpresa, IEnumerable<EmpresaTipoEnum> indisponibilidades, AutenticarClienteResponseViewModel retorno)
        {
            var indisponivel = false;
            var mensagem = "O serviço não está disponível para concessionária {0}.";
            foreach (var empresaTipo in indisponibilidades)
            {
                if (codigoEmpresa == empresaTipo)
                {
                    indisponivel = true;
                    retorno.AdicionaMensagem(string.Format(mensagem, empresaTipo.GetDescription()));
                    break;
                }
            }
            return indisponivel;
        }
        
        private async Task<TokenResponseViewModel> GerarTokenResponseAsync(AutenticarClienteRequestViewModel viewModel, EmpresaTipoEnum codigoEmpresa)
        {
            var tempo = _configuration.GetSection("TempoExpiracaoToken")?
                .GetValue<int>("AutenticacaoAplicativoSeconds");

            if (!tempo.HasValue || tempo == 0) tempo = 7200; //[7200 segundos = 120 minutos = 2 horas]

            var claims = new List<KeyValuePair<string, string>>();

            claims.Add(new KeyValuePair<string, string>("CodigoEmpresa", codigoEmpresa.ToString()));

            var tokenResponse = await _tokenProvider.GerarTokenModelAsync(claims, tempo);

            if (!string.IsNullOrEmpty(tokenResponse.Token) && tokenResponse.Sucesso)
            {
                var header = _httpContextAccessor?
                    .HttpContext?
                    .Request?
                    .Headers;

                if ((bool)header?.ContainsKey("Authorization"))
                {
                    _httpContextAccessor?
                        .HttpContext?
                        .Request?
                        .Headers?
                        .Remove("Authorization");
                }

                _httpContextAccessor?
                    .HttpContext?
                    .Request?
                    .Headers?
                    .Add("Authorization", $"Bearer {tokenResponse.Token}");
            }

            return tokenResponse;
        }

        #endregion

    }
}
