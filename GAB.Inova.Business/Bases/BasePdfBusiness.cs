﻿using AutoMapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.ViewModels.Faturamento;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Bases
{
    public abstract class BasePdfBusiness
    {

        protected readonly IMapper _mapper;
        protected readonly IWebHostEnvironment _webHostEnvironment;
        protected readonly IBarcodeProvider _barcodeProvider;
        protected readonly IHtmlToPdfProvider _htmlToPdfProvider;
        protected readonly IContaRepository _contaRepository;
        protected readonly IFaturamentoRepository _faturamentoRepository;
        protected readonly IParametroSistemaRepository _parametroSistemaRepository;
        protected readonly IPdfGenericoRepository _pdfGenericoRepository;
        protected readonly ITarifaDiariaRepository _tarifaDiariaRepository;
        protected readonly ITabAnaliseAguaRepository _tabAnaliseAguaRepository;
        protected readonly ITempHistoricoConsumoRepository _tempHistoricoConsumoRepository;
        protected readonly ITempImprimeContaRepository _tempImprimeContaRepository;
        protected readonly IDocumentoModeloRepository _documentoModeloRepository;
        protected readonly IImprimeGRPRepository _imprimeGRPRepository;
        protected readonly IEmpresaRepository _empresaRepository;

        protected BasePdfBusiness(IMapper mapper,
                                  IWebHostEnvironment webHostEnvironment,
                                  IBarcodeProvider barcodeProvider,
                                  IHtmlToPdfProvider htmlToPdfProvider,
                                  IContaRepository contaRepository,
                                  IFaturamentoRepository faturamentoRepository,
                                  IParametroSistemaRepository parametroSistemaRepository,
                                  IPdfGenericoRepository pdfGenericoRepository,
                                  ITabAnaliseAguaRepository tabAnaliseAguaRepository,
                                  ITarifaDiariaRepository tarifaDiariaRepository,
                                  ITempHistoricoConsumoRepository tempHistoricoConsumoRepository,
                                  ITempImprimeContaRepository tempImprimeContaRepository,
                                  IDocumentoModeloRepository documentoModeloRepository,
                                  IImprimeGRPRepository imprimeGRPRepository,
                                  IEmpresaRepository empresaRepository)
        {
            _mapper = mapper;
            _webHostEnvironment = webHostEnvironment;
            _barcodeProvider = barcodeProvider;
            _htmlToPdfProvider = htmlToPdfProvider;
            _contaRepository = contaRepository;
            _faturamentoRepository = faturamentoRepository;
            _parametroSistemaRepository = parametroSistemaRepository;
            _pdfGenericoRepository = pdfGenericoRepository;
            _tabAnaliseAguaRepository = tabAnaliseAguaRepository;
            _tarifaDiariaRepository = tarifaDiariaRepository;
            _tempHistoricoConsumoRepository = tempHistoricoConsumoRepository;
            _tempImprimeContaRepository = tempImprimeContaRepository;
            _documentoModeloRepository = documentoModeloRepository;
            _imprimeGRPRepository = imprimeGRPRepository;
            _empresaRepository = empresaRepository;
        }

        #region [Public methods]

        protected async Task<byte[]> GerarSegundaViaContaAsync(Conta conta, string filePath, string fileName)
        {
            var tempImprimeConta = new TempImprimeConta()
            {
                Id = conta.Id,
                IdMotivoImpressao = MotivoSegundaViaEnum.EXTRAVIO_NAO_RECEBIMENTO,
                IdUsuario = new Random(DateTime.Now.Millisecond).Next(),
                Matricula = conta.Matricula,
                TipoImpressao = 99,
                Via = 2 // 2ª via
            };

            var pdfDocumentLayout = await ObterDocumentoLayoutAsync(PdfModeloEnum.CONTA_SEGUNDA_VIA_AGENCIA_VIRTUAL);

            var pdfItemLayout = await ObterItemDocumentoLayoutAsync(pdfDocumentLayout.IdPdfItemLayout, pdfDocumentLayout.Id, tempImprimeConta.IdUsuario.Value);
            if (pdfItemLayout is null) throw new InvalidOperationException($"Não foi possível encontrar o item de layout do documento [{PdfModeloEnum.CONTA_SEGUNDA_VIA_AGENCIA_VIRTUAL}]");

            await _tempImprimeContaRepository.DeletarTempSeqAsync(tempImprimeConta);
            await _tempImprimeContaRepository.InserirTempSeqAsync(tempImprimeConta);
            await _tempImprimeContaRepository.GerarTempContaAsync(tempImprimeConta);

            var dicionarioLayout = await ObterDicionarioTradutorContaAsync();
            var pdfItemLayoutEspacoMensagem = await ObterPdfItemLayoutByNameAsync(pdfItemLayout, "ESPACO_MENSAGEM.MENSAGEM");
            var impressaoConta = await ObterImpressaoContaAsync(tempImprimeConta.IdUsuario.Value, conta.Id, pdfItemLayoutEspacoMensagem.Style.RepetitionMax);
            var documento = PdfGeneric.CreateDocument(pdfDocumentLayout, filePath, fileName);

            documento.Open();

            PdfGeneric.AddDataToDocument(
                documento,
                pdfDocumentLayout,
                pdfItemLayout,
                impressaoConta,
                dicionarioLayout);

            documento.Close();
            
            return documento.GetBytes();
        }
        protected async Task<byte[]> GerarPdfQuitacaoDebitoAsync(ImprimeGRP imprimeGRP, string filePath, string fileName)
        {
            var documentoModelo = await _documentoModeloRepository.ObterPorDocumentoModeloTipoAsync((int)TipoDocumentoModeloeEnum.QuitacaoDebito);

            if (documentoModelo is null)
            {
                throw new InvalidOperationException($"Modelo de documento não encontrado. Modelo tipo: [{TipoDocumentoModeloeEnum.QuitacaoDebito.GetDescription()}]");
            }
            else
            {
                var headerHtml = await GeraHeaderHtmlQuitacaoDebito(imprimeGRP, documentoModelo);
                var bodyHtml = await GeraBodyHtmlQuitacaoDebito(headerHtml, imprimeGRP, documentoModelo);

                return await _htmlToPdfProvider.GerarPdfAsync(fileName, filePath, bodyHtml, documentoModelo.CssHtml);
            }
        }

        protected async Task<byte[]> GerarPdfDeclaracaoAnualAsync(DeclaracaoAnual declaracaoAnual,
                                                                  IEnumerable<VwDadosDebitoAnual> listaDebitosAnual,
                                                                  VwCliente vwCliente,
                                                                  string canalAtendimento,
                                                                  string filePath,
                                                                  string fileName)
        {
            var documentoModelo = await _documentoModeloRepository.ObterPorDocumentoModeloTipoAsync((int)TipoDocumentoModeloeEnum.DeclaracaoAnualQuitacaoDebito);

            if (documentoModelo is null)
            {
                throw new InvalidOperationException($"Modelo de documento não encontrado. Modelo tipo: [{TipoDocumentoModeloeEnum.DeclaracaoAnualQuitacaoDebito.GetDescription()}]");
            }
            else
            {
                var headerHtml = await GeraHeaderHtmlDeclaracaoAnual(declaracaoAnual, vwCliente, documentoModelo);
                var footerHtml = await GeraFooterHtmlDeclaracaoAnual(canalAtendimento, documentoModelo);
                var bodyHtml = await GeraBodyHtmlDeclaracaoAnual(headerHtml, footerHtml, declaracaoAnual, listaDebitosAnual, documentoModelo);

                return await _htmlToPdfProvider.GerarPdfAsync(fileName, filePath, bodyHtml, documentoModelo.CssHtml);
            }
        }

        #endregion

        #region [Private methods]

        private async Task<PdfDocumentLayout> ObterDocumentoLayoutAsync(PdfModeloEnum pdfModelo)
        {
            var pdfGenModelo = await _pdfGenericoRepository.ObterDocumentoLayoutAsync(pdfModelo);
            if (pdfGenModelo is null) throw new InvalidOperationException($"Não foi possível encontrar o layout do documento [{pdfModelo.ToString()}]");
            return _mapper.Map<PdfDocumentLayout>(pdfGenModelo);
        }

        private async Task<PdfItemLayout> ObterItemDocumentoLayoutAsync(long idItemDocumento, long idModelo, long idUsuario, PdfItemLayout pdfItemLayoutParent = null)
        {
            var pdfItemLayout = default(PdfItemLayout);
            var pdfGenItensDoc = await _pdfGenericoRepository.ObterItensDocumentoLayoutAsync(idItemDocumento);
            if (pdfGenItensDoc != null && pdfGenItensDoc.Any())
            {
                var parent = pdfGenItensDoc.FirstOrDefault(q => q.Level == 1);
                if (parent != null)
                {
                    pdfItemLayout = new PdfItemLayout();

                    if (pdfItemLayoutParent != null)
                    {
                        pdfItemLayout.Parent = pdfItemLayoutParent;
                    }

                    pdfItemLayout = _mapper.Map<PdfGenItemDoc, PdfItemLayout>(parent, pdfItemLayout);

                    foreach (var pdfGenItemDoc in pdfGenItensDoc.Where(q => q.Level == 2))
                    {
                        pdfItemLayout.AddChild(await ObterItemDocumentoLayoutAsync(pdfGenItemDoc.Id, idModelo, idUsuario, pdfItemLayout));
                    }
                }
            }
            return pdfItemLayout;
        }

        private async Task<PdfItemLayout> ObterPdfItemLayoutByNameAsync(PdfItemLayout pdfItemLayout, string name)
        {
            if (pdfItemLayout is null) return pdfItemLayout;
            if (pdfItemLayout.Children != null && pdfItemLayout.Children.Any())
            {
                var itemLayout = default(PdfItemLayout);
                foreach (var item in pdfItemLayout.Children)
                {
                    itemLayout = await ObterPdfItemLayoutByNameAsync(item, name);
                    if (itemLayout != null) break;
                }
                return itemLayout;
            }
            if (pdfItemLayout.Name == name) return pdfItemLayout;
            return null;
        }

        private Task<Dictionary<string, string>> ObterDicionarioTradutorContaAsync()
        {
            var dic = new Dictionary<string, string>();

            dic.Add("IDENTIFICACAO_CONTA.NUMERO_LIGACAO", "Ligacao");
            dic.Add("IDENTIFICACAO_CONTA.ROTEIRIZACAO", "Roteirizacao");
            dic.Add("IDENTIFICACAO_CONTA.CONTA_NUMERO", "NumeroConta");
            dic.Add("IDENTIFICACAO_CONTA.REFERENCIA", "Referencia");
            dic.Add("IDENTIFICACAO_CONTA.DATA_EMISSAO", "DataEmissao");
            dic.Add("IDENTIFICACAO_CONTA.DATA_VENCIMENTO", "Vencimento");
            dic.Add("IDENTIFICACAO_CONTA.VIA", "Via");
            dic.Add("IDENTIFICACAO_CONTA.NOME_RAZAO_SOCIAL", "NomeRazaoSocial");
            dic.Add("IDENTIFICACAO_CONTA.ENDERECO_LIGACAO", "EnderecoLigacao");
            dic.Add("IDENTIFICACAO_CONTA.COMPLEMENTO", "Complemento");
            dic.Add("IDENTIFICACAO_CONTA.NUM_HIDROMETRO", "NumeroHidrometro");
            dic.Add("IDENTIFICACAO_CONTA.LEITURA_ANTERIOR", "LeituraAnterior");
            dic.Add("IDENTIFICACAO_CONTA.LEITURA_ATUAL", "LeituraAtual");

            dic.Add("IDENTIFICACAO_CONTA.DATA_LEITURA_ANT", "DataLeituraAnterior");
            dic.Add("IDENTIFICACAO_CONTA.DATA_LEITURA_ATUAL", "DataLeituraAtual");
            dic.Add("IDENTIFICACAO_CONTA.PREV_PROX_LEITURA", "PrevisaoProximaLeitura");
            dic.Add("IDENTIFICACAO_CONTA.TIPO_ENTREGA", "TipoEntrega");
            dic.Add("IDENTIFICACAO_CONTA.CPF_CNPJ", "CpfCnpj");
            dic.Add("IDENTIFICACAO_CONTA.INSCRICAO_ESTADUAL_MUNICIPAL", "InscricaoEstadualMunicipal");

            dic.Add("IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.RES", "CategoriaNumeroEconomiasResidencial");
            dic.Add("IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.COM", "CategoriaNumeroEconomiasComercial");
            dic.Add("IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.IND", "CategoriaNumeroEconomiasIndustrial");
            dic.Add("IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.PUB", "CategoriaNumeroEconomiasPublica");

            dic.Add("HISTORICO_CONSUMO.REFERENCIA", "HistoricosConsumoReferencia");
            dic.Add("HISTORICO_CONSUMO.CONSUMO", "HistoricosConsumoConsumo");
            dic.Add("HISTORICO_CONSUMO.DIAS", "HistoricosConsumoDias");

            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.DIAS_CONSUMO", "DiasConsumo");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_MEDIDO", "ConsumoMedido");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.CREDITO_CONSUMO", "CreditoConsumo");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_PIPA", "Pipa");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_RESIDUAL", "Residual");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_FATURADO", "ConsumoFaturado");
            dic.Add("INFORMACOES_FATURAMENTO_CONSUMO.TIPO_FATURAMENTO", "TipoFaturamento");

            dic.Add("FAIXA_CONSUMO.FAIXA_CONSUMO", "FaixasConsumo");
            dic.Add("FAIXA_CONSUMO.CONSUMO_FATURADO", "ConsumosFaturado");
            dic.Add("FAIXA_CONSUMO.TARIFA_AGUA", "TarifasAgua");
            dic.Add("FAIXA_CONSUMO.TARIFA_ESGOTO", "TarifasEsgoto");

            dic.Add("DISCRIMINACAO_FATURAMENTO.ESPECIFICACAO_SERVICO", "EspecificacaoServico");
            dic.Add("DISCRIMINACAO_FATURAMENTO.VALOR_FATURADO", "ValorFaturado");

            dic.Add("DISCRIMINACAO_FATURAMENTO.BASE_CALCULO", "BaseCalculo");
            dic.Add("DISCRIMINACAO_FATURAMENTO.ALIQUOTA", "Aliquota");
            dic.Add("DISCRIMINACAO_FATURAMENTO.IMPOSTO", "Imposto");
            dic.Add("DISCRIMINACAO_FATURAMENTO.RETENCAO_TRUBUTOS", "RetencaoTributos");
            dic.Add("DISCRIMINACAO_FATURAMENTO.TOTAL_PAGAR", "TotalPagar");
            dic.Add("DISCRIMINACAO_FATURAMENTO.CARGA_TRIBUTARIA_SERVICOS", "CargaTributariaSobreValorServicos");
            dic.Add("DISCRIMINACAO_FATURAMENTO.CUSTO_REGULACAO_FISCALIZACAO", "CustoRegulacaoFiscalizacao");

            dic.Add("ESPACO_MENSAGEM.MENSAGEM", "Mensagens");
            dic.Add("ESPACO_MENSAGEM.IDENTIFICADOR_DEBITO_AUTOMATICO", "IdentificadorDebitoAutomatico");

            dic.Add("INFORMACOES_QUALIDADE_AGUA.SISTEMA_ABASTECIMENTO", "QualidadeAguaSistemaAbastecimento");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.OBSERVACOES", "QualidadeAguaObservacoes");

            dic.Add("INFORMACOES_QUALIDADE_AGUA.FLUOR", "QualidadeAguaFluor");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.CLORO", "QualidadeAguaCloro");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.TURBIDEZ", "QualidadeAguaTurbidez");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.COR", "QualidadeAguaCor");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.PH", "QualidadeAguaPh");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.COLIFORMESTOTAIS", "QualidadeAguaColiformesTotais");
            dic.Add("INFORMACOES_QUALIDADE_AGUA.ESCHERICHIACOLI", "QualidadeAguaEscherichiaColi");

            dic.Add("CODIGO_BARRAS.LINHA_DIGITAVEL", "NumeroCodigodeBarrasFormatado");
            dic.Add("CODIGO_BARRAS.CODIGO_BARRAS", "NumeroCodigodeBarras");
            dic.Add("CODIGO_BARRAS.TEXTO_ALTERNNATIVO_CODIGO_BARRAS", "TextoAlternativoCodigoBarras");

            dic.Add("VIA_BANCO.IDENTIFICACAO.NUMERO_LIGACAO", "Ligacao");
            dic.Add("VIA_BANCO.IDENTIFICACAO.CONTA_NUMERO", "NumeroConta");
            dic.Add("VIA_BANCO.IDENTIFICACAO.REFERENCIA", "Referencia");
            dic.Add("VIA_BANCO.IDENTIFICACAO.DATA_EMISSAO", "DataEmissao");
            dic.Add("VIA_BANCO.IDENTIFICACAO.DATA_VENCIMENTO", "Vencimento");
            dic.Add("VIA_BANCO.IDENTIFICACAO.TOTAL_PAGAR", "TotalPagar");

            dic.Add("AVISO_DEBITO.AVISODEBITO_MENSAGEM_PARTE", "AvisoDebitoMensagemParte1");
            dic.Add("AVISO_DEBITO.AVISODEBITO_LINHA_DIGITAVEL", "AvisoDebitoCodigodeBarrasFormatado");
            dic.Add("AVISO_DEBITO.AVISODEBITO_CODIGOBARRAS", "AvisoDebitoCodigoBarras");

            dic.Add("BACKGROUND", "ImagemBackground");

            return Task.FromResult(dic);
        }

        private async Task<List<object>> ObterImpressaoContaAsync(long idUsuario, long seqOriginal, int mensagensPadraoLength = 10)
        {
            var arquivoImpressao = await _contaRepository.ObterArquivoImpressaoAsync(seqOriginal, idUsuario);
            if (arquivoImpressao is null) throw new InvalidOperationException($"Não foi possível encontrar a impressão do documento [nº:{seqOriginal}]");

            var contaImpressao = _mapper.Map<ContaImpressaoViewModel>(arquivoImpressao);
            
            //ICMS - Incluído no valor do consumo
            contaImpressao.BaseCalculo = "0,00";
            contaImpressao.Aliquota = "0,00";
            contaImpressao.Imposto = "0,00";
            
            #region [Discriminação do Faturamento]

            var indice = default(string);
            var especificacaoServico = default(string);
            var valorServico = default(double?);

            for (var i = 1; i <= 7; i++)
            {
                indice = i.ToString().PadLeft(2, '0');
                especificacaoServico = arquivoImpressao.GetPropertyValue<string>($"CompoeCt{indice}")?.Trim();

                if (string.IsNullOrEmpty(especificacaoServico))
                {
                    contaImpressao.EspecificacaoServico.Add("");
                    contaImpressao.ValorFaturado.Add("");
                }
                else
                {
                    contaImpressao.EspecificacaoServico.Add(especificacaoServico);

                    valorServico = arquivoImpressao.GetPropertyValue<double?>($"CompoeVl{indice}");

                    if (valorServico.HasValue)
                    {
                        contaImpressao.ValorFaturado.Add(valorServico.Value.ToString("N2"));
                    }
                    else
                    {
                        contaImpressao.ValorFaturado.Add("0,00");
                    }
                }
            }

            #endregion

            #region [Mensagens padrão]

            mensagensPadraoLength = mensagensPadraoLength <= 10 ? mensagensPadraoLength : 10;
            for (var i = 1; i <= mensagensPadraoLength; i++)
            {
                contaImpressao.Mensagens.Add(arquivoImpressao.GetPropertyValue<string>($"MensagemPadrao{i.ToString().PadLeft(2, '0')}"));
            }

            #endregion

            #region [Históricos de consumo]

            var historicoConsumos = await _tempHistoricoConsumoRepository.ObterTempHistoricoConsumoAsync(arquivoImpressao.Matricula, idUsuario);
            if (historicoConsumos.Any())
            {
                if (historicoConsumos.Any(q => q.Consumo.HasValue))
                {
                    contaImpressao.HistoricosConsumoConsumo = historicoConsumos.Select(q => q.Consumo.Value.ToString()).ToList();
                }
                else contaImpressao.HistoricosConsumoConsumo = new List<string>();

                if (historicoConsumos.Any(q => q.QtdeDias.HasValue))
                {
                    contaImpressao.HistoricosConsumoDias = historicoConsumos.Select(q => q.QtdeDias.Value.ToString()).ToList();
                }
                else contaImpressao.HistoricosConsumoDias = new List<string>();

                contaImpressao.HistoricosConsumoReferencia = historicoConsumos.Select(q => q.Referencia).ToList();
            }

            #endregion

            #region [Qualidade da água]

            var analisesAgua = await _tabAnaliseAguaRepository.ObterAnaliseAguaParaImpressaoContaAsync(seqOriginal, idUsuario);
            if (analisesAgua.Any())
            {
                var cloro = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.CLORO);
                if (cloro != null)
                {
                    contaImpressao.QualidadeAguaCloro?.Add(cloro.AmostraExigida);
                    contaImpressao.QualidadeAguaCloro?.Add(cloro.AmostraRealizada);
                    contaImpressao.QualidadeAguaCloro?.Add(cloro.ValorMedioDetectado);
                }

                var oliformesTotais = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.COLTOTAL);
                if (oliformesTotais != null)
                {
                    contaImpressao.QualidadeAguaColiformesTotais?.Add(oliformesTotais.AmostraExigida);
                    contaImpressao.QualidadeAguaColiformesTotais?.Add(oliformesTotais.AmostraRealizada);
                    contaImpressao.QualidadeAguaColiformesTotais?.Add(oliformesTotais.ValorMedioDetectado);
                }

                var cor = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.COR);
                if (cor != null)
                {
                    contaImpressao.QualidadeAguaCor?.Add(cor.AmostraExigida);
                    contaImpressao.QualidadeAguaCor?.Add(cor.AmostraRealizada);
                    contaImpressao.QualidadeAguaCor?.Add(cor.ValorMedioDetectado);
                }

                var escherichiaColi = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.COLTERM);
                if (escherichiaColi != null)
                {
                    contaImpressao.QualidadeAguaEscherichiaColi?.Add(escherichiaColi.AmostraExigida);
                    contaImpressao.QualidadeAguaEscherichiaColi?.Add(escherichiaColi.AmostraRealizada);
                    contaImpressao.QualidadeAguaEscherichiaColi?.Add(escherichiaColi.ValorMedioDetectado);
                }

                var fluor = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.FLUOR);
                if (fluor != null)
                {
                    contaImpressao.QualidadeAguaFluor?.Add(fluor.AmostraExigida);
                    contaImpressao.QualidadeAguaFluor?.Add(fluor.AmostraRealizada);
                    contaImpressao.QualidadeAguaFluor?.Add(fluor.ValorMedioDetectado);
                }

                var ph = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.PH);
                if (ph != null)
                {
                    contaImpressao.QualidadeAguaPh?.Add(ph.AmostraExigida);
                    contaImpressao.QualidadeAguaPh?.Add(ph.AmostraRealizada);
                    contaImpressao.QualidadeAguaPh?.Add(ph.ValorMedioDetectado);
                }

                var turbidez = analisesAgua.FirstOrDefault(q => q.DescricaoColuna == TipoAnaliseAguaEnum.TURBIDEZ);
                if (turbidez != null)
                {
                    contaImpressao.QualidadeAguaTurbidez?.Add(turbidez.AmostraExigida);
                    contaImpressao.QualidadeAguaTurbidez?.Add(turbidez.AmostraRealizada);
                    contaImpressao.QualidadeAguaTurbidez?.Add(turbidez.ValorMedioDetectado);
                }
            }

            #endregion

            #region [Faixas de tarifas]
            
            var tarifas = await ObterTarifaImpressaoContaAsync(arquivoImpressao);
            if (tarifas.Any())
            {
                var tipoTarifa = tarifas.FirstOrDefault()?.TipoTarifa;
                if (tipoTarifa == 3 || tipoTarifa == 4)
                {
                    var parametroSistema = await _parametroSistemaRepository.ObterAsync((long)TabParametroEnum.IMPRIMIR_FAIXAS_CONSUMO_AGRUPADAS_CATEGORIA_MISTAS);
                    if (parametroSistema?.Valor == 1)
                    {
                        //Verificar economia mista
                        if (tarifas.Select(q => q.NomeEconomia).Distinct().Count() > 1)
                        {
                            var faixas = await SepararFaixasAsync(tarifas);

                            foreach (var faixa in faixas)
                            {
                                if (faixa.ValorTarifa > 0)
                                {
                                    contaImpressao.ConsumosFaturado.Add("Água");
                                    contaImpressao.TarifasEsgoto.Add(faixa.ValorTarifa.ToString("N2"));
                                }
                                if (faixa.ValorTarifaEsgoto > 0)
                                {
                                    contaImpressao.ConsumosFaturado.Add("Esgoto");
                                    contaImpressao.TarifasEsgoto.Add(faixa.ValorTarifaEsgoto.ToString("N2"));
                                }

                                if (faixa.ValorTarifa > 0 && faixa.ValorTarifaEsgoto > 0)
                                {
                                    contaImpressao.FaixasConsumo.Add(faixa.NomeEconomia);
                                    contaImpressao.FaixasConsumo.Add("");
                                }
                                else
                                {
                                    contaImpressao.FaixasConsumo.Add(faixa.NomeEconomia);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (var tarifa in tarifas)
                        {
                            contaImpressao.FaixasConsumo.Add(tarifa.NomeEconomia);
                            contaImpressao.ConsumosFaturado.Add(tarifa.ConsumoFaturado.ToString());
                            contaImpressao.TarifasAgua.Add(tarifa.ValorTarifa.ToString());
                            contaImpressao.TarifasEsgoto.Add(tarifa.ValorTarifaEsgoto.ToString());
                        }
                    }
                }
                else
                {
                    foreach (var tarifa in tarifas)
                    {
                        contaImpressao.FaixasConsumo.Add($"{tarifa.NomeEconomia} {tarifa.Faixa}");
                        contaImpressao.ConsumosFaturado.Add(tarifa.ConsumoFaturado.ToString("N2"));
                        contaImpressao.TarifasAgua.Add(tarifa.ValorTarifa.ToString());
                        contaImpressao.TarifasEsgoto.Add(tarifa.ValorTarifaEsgoto.ToString());
                    }
                }
            }

            #endregion

            contaImpressao.ImagemBackground = $"{_webHostEnvironment.ContentRootPath}\\Imagem\\Contas\\Bg2aViaContaInterna01{arquivoImpressao.SiglaEmpresa}.jpg";

            var lista = new List<object>();

            lista.Add(contaImpressao);

            return lista;
        }

        private async Task<IEnumerable<int>> ObterCategoriasLoteImpressaoContaAsync(VwGeraArquivoImpressao arquivoImpressao)
        {
            var categorias = new List<int>();

            if (arquivoImpressao.EconomiaResidencial.HasValue && arquivoImpressao.EconomiaResidencial.Value > 0)
            {
                categorias.Add(1);
            }
            if (arquivoImpressao.EconomiaComercial.HasValue && arquivoImpressao.EconomiaComercial.Value > 0)
            {
                categorias.Add(2);
            }
            if (arquivoImpressao.EconomiaIndustrial.HasValue && arquivoImpressao.EconomiaIndustrial.Value > 0)
            {
                categorias.Add(3);
            }
            if (arquivoImpressao.EconomiaPublica.HasValue && arquivoImpressao.EconomiaPublica.Value > 0)
            {
                categorias.Add(4);
            }

            return await Task.FromResult(categorias);
        }

        private async Task<IEnumerable<TarifaContaImpressaoViewModel>> ObterTarifaImpressaoContaAsync(VwGeraArquivoImpressao arquivoImpressao)
        {
            var tipoTarifa = default(int);
            var consumoFaturado = default(double);
            var idTarifa = arquivoImpressao.IdTarifa.HasValue ? arquivoImpressao.IdTarifa.Value : 1;
            var grupoTarifa = (arquivoImpressao.SituacaoHd.HasValue ? arquivoImpressao.SituacaoHd.Value : 3) == 0 ? 2 : 1;

            var categorias = await ObterCategoriasLoteImpressaoContaAsync(arquivoImpressao);

            // Busca o registro de faturamento para identificar a tarifa utilizada, e então identificar seu tipo (cascata ou direta)
            foreach (var categoria in categorias)
            {
                var faturamentos = await _faturamentoRepository.ObterFaturamentoParaImpressaoContaAsync(new Faturamento()
                {
                    AnoMesFaturamento = arquivoImpressao.AnoMesCt,
                    Categoria = categoria,
                    Matricula = arquivoImpressao.Matricula,
                    SeqOriginal = arquivoImpressao.SeqOriginal
                });
                
                consumoFaturado = faturamentos.FirstOrDefault()?.VolumePorEconomiaFaturado ?? 0;

                var tarifasDiaria = await _tarifaDiariaRepository.ObterTarifaParaImpressaoContaAsync(new TarifaDiaria()
                {
                    DataTarifa = arquivoImpressao.DataLeituraAtual.Value,
                    FaixaAnterior = consumoFaturado,
                    GrupoTarifa = grupoTarifa.ToString(),
                    IdCategoria = categoria,
                    IdTarifa = idTarifa
                });

                if (tarifasDiaria.Any())
                {
                    tipoTarifa = int.Parse(tarifasDiaria.FirstOrDefault().TipoTarifa);
                    break;
                }

                return new List<TarifaContaImpressaoViewModel>();
            }

            // Passa a avaliar o tipo de tarifa para identificar se é direta ou cascata, em vez de avaliar o parâmetro da tabela 
            // EMPRESA (tipoImpFat), em conjunto com modoFat. O sistema estava imprimindo faixa de consumo de tarifa direta para as ligações estimadas.
            if (tipoTarifa == 1 || tipoTarifa == 2)
            {
                return await ObterTarifaCascataAsync(arquivoImpressao, tipoTarifa);
            }

            return await ObterTarifaDiretaAsync(arquivoImpressao, tipoTarifa);
        }

        public async Task<IEnumerable<TarifaContaImpressaoViewModel>> ObterTarifaDiretaAsync(VwGeraArquivoImpressao arquivoImpressao, int tipoTarifa)
        {
            var economias = default(int);
            var consumoFaturado = default(double);
            var consumoTotalFaturado = default(double);
            var idTarifa = arquivoImpressao.IdTarifa.HasValue ? arquivoImpressao.IdTarifa.Value : 1;
            var grupoTarifa = (arquivoImpressao.SituacaoHd.HasValue ? arquivoImpressao.SituacaoHd.Value : 3) == 0 ? 2 : 1;
            var percentualAgua = (double)(arquivoImpressao.PercentualAgua.HasValue ? arquivoImpressao.PercentualAgua.Value : 0);
            var percentualEsgoto = (double)(arquivoImpressao.PercentualEsgoto.HasValue ? arquivoImpressao.PercentualEsgoto.Value : 0);

            var categorias = await ObterCategoriasLoteImpressaoContaAsync(arquivoImpressao);

            var tarifas = new List<TarifaContaImpressaoViewModel>();
            
            foreach (var categoria in categorias)
            {
                switch (categoria)
                {
                    case 1:
                        economias = arquivoImpressao.EconomiaResidencial.Value;
                        break;
                    case 2:
                        economias = arquivoImpressao.EconomiaComercial.Value;
                        break;
                    case 3:
                        economias = arquivoImpressao.EconomiaIndustrial.Value;
                        break;
                    case 4:
                        economias = arquivoImpressao.EconomiaPublica.Value;
                        break;
                }

                var faturamentos = await _faturamentoRepository.ObterFaturamentoParaImpressaoContaAsync(new Faturamento()
                {
                    AnoMesFaturamento = arquivoImpressao.AnoMesCt,
                    Categoria = categoria,
                    Matricula = arquivoImpressao.Matricula,
                    SeqOriginal = arquivoImpressao.SeqOriginal
                });

                consumoFaturado = faturamentos.FirstOrDefault()?.VolumePorEconomiaFaturado ?? 0;
                consumoTotalFaturado = faturamentos.FirstOrDefault()?.VolumeTotalFaturado ?? 0;

                var tarifasDiaria = await _tarifaDiariaRepository.ObterTarifaParaImpressaoContaAsync(new TarifaDiaria()
                {
                    DataTarifa = arquivoImpressao.DataLeituraAtual.Value,
                    FaixaAnterior = consumoFaturado,
                    GrupoTarifa = grupoTarifa.ToString(),
                    IdCategoria = categoria,
                    IdTarifa = idTarifa
                });
                
                #region [Separa as faixas de consumo e escreve na LISTA]

                if (!tarifasDiaria.Any()) return tarifas;
                
                var tarifaDiaria = tarifasDiaria.LastOrDefault();
                
                var tarifaConta = new TarifaContaImpressaoViewModel()
                {
                    Matricula = arquivoImpressao.Matricula,
                    Referencia = arquivoImpressao.AnoMesCt,
                    SeqOriginal = arquivoImpressao.SeqOriginal,
                    TipoTarifa = tipoTarifa
                };

                switch (categoria)
                {
                    case 1:
                        tarifaConta.NomeEconomia = "Res";
                        break;
                    case 2:
                        tarifaConta.NomeEconomia = "Com";
                        break;
                    case 3:
                        tarifaConta.NomeEconomia = "Ind";
                        break;
                    case 4:
                        tarifaConta.NomeEconomia = "Pub";
                        break;
                }

                tarifaConta.VolumeTotalFaturado = consumoTotalFaturado;
                tarifaConta.Faixa = $"{tarifaDiaria.FaixaAnterior} a {tarifaDiaria.FaixaConsumo}";

                if (tarifaDiaria.FaixaConsumo <= consumoFaturado)
                {
                    tarifaConta.ConsumoFaturado = ((tarifaDiaria.FaixaConsumo - tarifaDiaria.FaixaAnterior) * economias);
                }
                else
                {
                    //ultima faixa
                    tarifaConta.ConsumoFaturado = ((consumoFaturado - tarifaDiaria.FaixaAnterior) * economias);
                }

                #region [Valor Tarifa Água]

                tarifaConta.ValorTarifa = 0;

                if (arquivoImpressao.ValorAgua.HasValue && arquivoImpressao.ValorAgua.Value > 0)
                {
                    tarifaConta.ValorTarifa = ((tarifaConta.ConsumoFaturado * tarifaDiaria.ValorFaixa) * economias);
                    tarifaConta.ValorTarifa = tarifaConta.ValorTarifa.Truncate(2);

                    if (percentualAgua > 0 && percentualAgua < 100)
                    {
                        tarifaConta.ValorTarifa = ((percentualAgua / 100) * tarifaConta.ValorTarifa);
                        tarifaConta.ValorTarifa = tarifaConta.ValorTarifa.Truncate(2);
                    }
                    else if (percentualAgua == 0)
                    {
                        tarifaConta.ValorTarifa = 0;
                    }
                }

                #endregion

                #region [Valor Tarifa Esgoto]

                tarifaConta.ValorTarifaEsgoto = 0;

                if (arquivoImpressao.ValorEsgoto.HasValue && arquivoImpressao.ValorEsgoto.Value > 0)
                {
                    if (arquivoImpressao.SituacaoEsgoto.HasValue && arquivoImpressao.SituacaoEsgoto.Value == 1)
                    {
                        tarifaConta.ValorTarifaEsgoto = ((tarifaConta.ConsumoFaturado * tarifaDiaria.ValorFaixaEsgoto) * economias);
                        tarifaConta.ValorTarifaEsgoto = tarifaConta.ValorTarifaEsgoto.Truncate(2);

                        if (percentualEsgoto > 0 && percentualEsgoto < 100)
                        {
                            tarifaConta.ValorTarifaEsgoto = ((percentualEsgoto / 100) * tarifaConta.ValorTarifaEsgoto);
                            tarifaConta.ValorTarifaEsgoto = tarifaConta.ValorTarifaEsgoto.Truncate(2);
                        }
                        else if (percentualEsgoto == 0)
                        {
                            tarifaConta.ValorTarifaEsgoto = 0;
                        }
                    }
                }

                #endregion

                //1- SOMENTE AGUA | 2 - SOMENTE ESGOTO
                if (arquivoImpressao.CompoeVl16.HasValue && arquivoImpressao.CompoeVl16.Value != 0)
                {
                    if (arquivoImpressao.CompoeVl16.Value == 1)
                    {
                        tarifaConta.ValorTarifaEsgoto = 0;
                    }
                    if (arquivoImpressao.CompoeVl16.Value == 2)
                    {
                        tarifaConta.ValorTarifa = 0;
                    }
                }

                tarifas.Add(tarifaConta);

                #endregion
            }

            return tarifas;
        }

        private async Task<IEnumerable<TarifaContaImpressaoViewModel>> ObterTarifaCascataAsync(VwGeraArquivoImpressao arquivoImpressao, int tipoTarifa)
        {
            var economias = default(int);
            var consumoFaturado = default(double);
            var valorTotalAguaResidencial = default(double);
            var valorTotalEsgotoResidencial = default(double);
            var idTarifa = arquivoImpressao.IdTarifa.HasValue ? arquivoImpressao.IdTarifa.Value : 1;
            var grupoTarifa = (arquivoImpressao.SituacaoHd.HasValue ? arquivoImpressao.SituacaoHd.Value : 3) == 0 ? 2 : 1;
            var percentualAgua = (double)(arquivoImpressao.PercentualAgua.HasValue ? arquivoImpressao.PercentualAgua.Value : 0);
            var percentualEsgoto = (double)(arquivoImpressao.PercentualEsgoto.HasValue ? arquivoImpressao.PercentualEsgoto.Value : 0);

            var categorias = await ObterCategoriasLoteImpressaoContaAsync(arquivoImpressao);

            var tarifas = new List<TarifaContaImpressaoViewModel>();
            
            foreach (var categoria in categorias)
            {
                switch (categoria)
                {
                    case 1:
                        economias = arquivoImpressao.EconomiaResidencial.Value;
                        break;
                    case 2:
                        economias = arquivoImpressao.EconomiaComercial.Value;
                        break;
                    case 3:
                        economias = arquivoImpressao.EconomiaIndustrial.Value;
                        break;
                    case 4:
                        economias = arquivoImpressao.EconomiaPublica.Value;
                        break;
                }

                var faturamentos = await _faturamentoRepository.ObterFaturamentoParaImpressaoContaAsync(new Faturamento()
                {
                    AnoMesFaturamento = arquivoImpressao.AnoMesCt,
                    Categoria = categoria,
                    Matricula = arquivoImpressao.Matricula,
                    SeqOriginal = arquivoImpressao.SeqOriginal
                });

                consumoFaturado = faturamentos.FirstOrDefault()?.VolumePorEconomiaFaturado ?? 0;

                var tarifasDiaria = await _tarifaDiariaRepository.ObterTarifaParaImpressaoContaAsync(new TarifaDiaria()
                {
                    DataTarifa = arquivoImpressao.DataLeituraAtual.Value,
                    FaixaAnterior = consumoFaturado,
                    GrupoTarifa = grupoTarifa.ToString(),
                    IdCategoria = categoria,
                    IdTarifa = idTarifa
                });

                #region [Separa as faixas de consumo e escreve na LISTA]

                var tarifaConta = default(TarifaContaImpressaoViewModel);

                foreach (var tarifaDiaria in tarifasDiaria)
                {
                    tarifaConta = new TarifaContaImpressaoViewModel()
                    {
                        Matricula = arquivoImpressao.Matricula,
                        Referencia = arquivoImpressao.AnoMesCt,
                        SeqOriginal = arquivoImpressao.SeqOriginal,
                        TipoTarifa = tipoTarifa
                    };

                    switch (categoria)
                    {
                        case 1:
                            tarifaConta.NomeEconomia = "Res";
                            break;
                        case 2:
                            tarifaConta.NomeEconomia = "Com";
                            break;
                        case 3:
                            tarifaConta.NomeEconomia = "Ind";
                            break;
                        case 4:
                            tarifaConta.NomeEconomia = "Pub";
                            break;
                    }

                    tarifaConta.Faixa = $"{tarifaDiaria.FaixaAnterior} a {tarifaDiaria.FaixaConsumo}";

                    if (tarifaDiaria.FaixaConsumo <= consumoFaturado)
                    {
                        tarifaConta.ConsumoFaturado = ((tarifaDiaria.FaixaConsumo - tarifaDiaria.FaixaAnterior) * economias);
                    }
                    else
                    {
                        //ultima faixa
                        tarifaConta.ConsumoFaturado = ((consumoFaturado - tarifaDiaria.FaixaAnterior) * economias);
                    }

                    #region [Valor Tarifa Água]

                    tarifaConta.ValorTarifa = 0;

                    if (arquivoImpressao.ValorAgua.HasValue && arquivoImpressao.ValorAgua.Value > 0)
                    {
                        tarifaConta.ValorTarifa = (tarifaConta.ConsumoFaturado * tarifaDiaria.ValorFaixa);
                        tarifaConta.ValorTarifa = tarifaConta.ValorTarifa.Truncate(3);

                        if (percentualAgua > 0 && percentualAgua < 100)
                        {
                            tarifaConta.ValorTarifa = ((percentualAgua / 100) * tarifaConta.ValorTarifa);
                            tarifaConta.ValorTarifa = tarifaConta.ValorTarifa.Truncate(3);
                        }
                        else if (percentualAgua == 0)
                        {
                            tarifaConta.ValorTarifa = 0;
                        }
                    }

                    #endregion

                    #region [Valor Tarifa Esgoto]

                    tarifaConta.ValorTarifaEsgoto = 0;

                    if (arquivoImpressao.ValorEsgoto.HasValue && arquivoImpressao.ValorEsgoto.Value > 0)
                    {
                        if (arquivoImpressao.SituacaoEsgoto.HasValue && arquivoImpressao.SituacaoEsgoto.Value == 1)
                        {
                            tarifaConta.ValorTarifaEsgoto = (tarifaConta.ConsumoFaturado * tarifaDiaria.ValorFaixaEsgoto);
                            tarifaConta.ValorTarifaEsgoto = tarifaConta.ValorTarifaEsgoto.Truncate(3);

                            if (percentualEsgoto > 0 && percentualEsgoto < 100)
                            {
                                tarifaConta.ValorTarifaEsgoto = ((percentualEsgoto / 100) * tarifaConta.ValorTarifaEsgoto);
                                tarifaConta.ValorTarifaEsgoto = tarifaConta.ValorTarifaEsgoto.Truncate(3);
                            }
                            else if (percentualEsgoto == 0)
                            {
                                tarifaConta.ValorTarifaEsgoto = 0;
                            }
                        }
                    }

                    #endregion

                    // Se for categoria residencial, acumula os valores de água e esgoto para possível utilização
                    // no tratamento de fator redutor.
                    if (categoria == 1)
                    {
                        valorTotalAguaResidencial += tarifaConta.ValorTarifa;
                        valorTotalEsgotoResidencial += tarifaConta.ValorTarifaEsgoto;
                    }

                    //1- SOMENTE AGUA | 2 - SOMENTE ESGOTO
                    if (arquivoImpressao.CompoeVl16.HasValue && arquivoImpressao.CompoeVl16.Value != 0)
                    {
                        if (arquivoImpressao.CompoeVl16.Value == 1)
                        {
                            tarifaConta.ValorTarifaEsgoto = 0;
                        }
                        if (arquivoImpressao.CompoeVl16.Value == 2)
                        {
                            tarifaConta.ValorTarifa = 0;
                        }
                    }

                    tarifas.Add(tarifaConta);
                }

                //Se for residencial
                if (categoria == 1)
                {
                    await TratarFaixaConsumoFatorRedutorAsunc(faturamentos, valorTotalAguaResidencial, valorTotalEsgotoResidencial, tarifas);
                }

                #endregion
            }

            return tarifas;
        }

        private async Task TratarFaixaConsumoFatorRedutorAsunc(IEnumerable<Faturamento> faturamentos, double valorTotalAgua, double valorTotalEsgoto, List<TarifaContaImpressaoViewModel> tarifas)
        {
            var parametroSistema = await _parametroSistemaRepository.ObterAsync((long)TabParametroEnum.TEMPO_EXPIRACAO_TOKEN_CONTA_DIGITAL);
            if (parametroSistema is null) return;

            if (parametroSistema.Valor == 1)
            {
                var valorAgua = default(double);
                var valorEsgoto = default(double);
                var valorDescontoFatorRedutorAgua = default(double);
                var valorDescontoFatorRedutorEsgoto = default(double);

                foreach (var faturamento in faturamentos)
                {
                    //Água
                    if (faturamento.TipoServicoFaturado == 1)
                    {
                        valorAgua = faturamento.ValorFaturamento;
                    }
                    //Esgoto
                    if (faturamento.TipoServicoFaturado == 2)
                    {
                        valorEsgoto = faturamento.ValorFaturamento;
                    }
                }

                valorDescontoFatorRedutorAgua = valorTotalAgua - valorAgua;
                valorDescontoFatorRedutorEsgoto = valorTotalEsgoto - valorEsgoto;

                // Percorre a lista com a distribuição das faixas de consumo de RES.
                for (int i = tarifas.Count - 1; i >= 0; i--)
                {
                    // Trata desconto de fator redutor para água
                    if (valorDescontoFatorRedutorAgua > 0 && tarifas[i].NomeEconomia == "Res")
                    {
                        if (tarifas[i].ValorTarifa >= valorDescontoFatorRedutorAgua)
                        {
                            tarifas[i].ValorTarifa = tarifas[i].ValorTarifa - valorDescontoFatorRedutorAgua;
                            valorDescontoFatorRedutorAgua = 0;
                        }
                        else
                        {
                            valorDescontoFatorRedutorAgua = valorDescontoFatorRedutorAgua - tarifas[i].ValorTarifa;
                            tarifas[i].ValorTarifa = 0;
                        }
                    }

                    // Trata desconto de fator redutor para esgoto
                    if (valorDescontoFatorRedutorEsgoto > 0 && tarifas[i].NomeEconomia == "Res")
                    {
                        if (tarifas[i].ValorTarifaEsgoto >= valorDescontoFatorRedutorEsgoto)
                        {
                            tarifas[i].ValorTarifaEsgoto = tarifas[i].ValorTarifaEsgoto - valorDescontoFatorRedutorEsgoto;
                            valorDescontoFatorRedutorEsgoto = 0;
                        }
                        else
                        {
                            valorDescontoFatorRedutorEsgoto = valorDescontoFatorRedutorEsgoto - tarifas[i].ValorTarifaEsgoto;
                            tarifas[i].ValorTarifaEsgoto = 0;
                        }
                    }

                    if (valorDescontoFatorRedutorAgua <= 0 & valorDescontoFatorRedutorEsgoto <= 0) break;
                }
            }
        }

        private Task<IEnumerable<TarifaContaImpressaoViewModel>> SepararFaixasAsync(IEnumerable<TarifaContaImpressaoViewModel> tarifas)
        {
            var tarifa = default(TarifaContaImpressaoViewModel);
            var economias = default(IEnumerable<TarifaContaImpressaoViewModel>);
            var faixas = new List<TarifaContaImpressaoViewModel>();

            foreach (var nomeEconomia in tarifas.Select(q => q.NomeEconomia).Distinct())
            {
                economias = tarifas.Where(q => q.NomeEconomia == nomeEconomia);

                tarifa = economias.FirstOrDefault();

                faixas.Add(new TarifaContaImpressaoViewModel()
                {
                    Matricula = tarifa.Matricula,
                    NomeEconomia = tarifa.NomeEconomia,
                    Referencia = tarifa.Referencia,
                    SeqOriginal = tarifa.SeqOriginal,
                    ValorTarifa = economias.Sum(v => v.ValorTarifa),
                    ValorTarifaEsgoto = economias.Sum(v => v.ValorTarifaEsgoto)
                });
            }

            return Task.FromResult(faixas.AsEnumerable());
        }

        private async Task<string> GeraHeaderHtmlQuitacaoDebito(ImprimeGRP imprimeGRP, DocumentoModelo documentoModelo)
        {
            string htmlHeader = documentoModelo.HeaderHtml;

            var empresa = await _empresaRepository.ObterAsync();

            htmlHeader = htmlHeader.Replace("[[SIGLA]]", empresa.Sigla);
            htmlHeader = htmlHeader.Replace("[[HOST]]", $@"{_webHostEnvironment.ContentRootPath.Replace("\\", "/")}/");
            htmlHeader = htmlHeader.Replace("[[MATRICULA]]", imprimeGRP.Matricula);
            htmlHeader = htmlHeader.Replace("[[NOME_CLIENTE]]", imprimeGRP.NomeCliente);
            htmlHeader = htmlHeader.Replace("[[ENDERECO]]", imprimeGRP.Endereco);
            htmlHeader = htmlHeader.Replace("[[NOME_LOCAL]]", imprimeGRP.NomeLocal);
            htmlHeader = htmlHeader.Replace("[[SEQ_ORIGINAL]]", imprimeGRP.Id.ToString());

            var dataAtual = DateTime.Now;
            htmlHeader = htmlHeader.Replace("[[DATA_EMISSAO]]", $"{dataAtual.ToShortDateString()} {dataAtual:HH:mm:ss}");
            htmlHeader = htmlHeader.Replace("[[VALIDADE_DOCUMENTO]]", Convert.ToDateTime(imprimeGRP.DataVencimento).ToShortDateString());

            return htmlHeader;
        }

        private async Task<string> GeraBodyHtmlQuitacaoDebito(string headerHtml, ImprimeGRP imprimeGRP, DocumentoModelo documentoModelo)
        {
            string htmlBody = documentoModelo.BodyHtml;
            string htmlBodyFinal = string.Empty;

            var templateTableContasVinculadas = htmlBody.Slice("[[listaQuitacaoDebitoTrIni]]", "[[listaQuitacaoDebitoTrFim]]");

            var listaContas = await _contaRepository.ObterContasQuitacaoDebitoAsync(imprimeGRP.Id);

            if (listaContas.Count() > 0)
            {
                decimal valorTotal = 0;
                int qtdLinhas = 0;
                int qtdLinhasParcial = 0;

                var tableContasVinculadas = string.Empty;

                foreach (var conta in listaContas)
                {
                    decimal valor = conta.ValorTotal;
                    DateTime dtVencimento = conta.Vencimento;
                    valorTotal += valor;

                    var tr = templateTableContasVinculadas.Replace("[[listaQuitacaoDebitoTrIni]]", string.Empty).Replace("[[listaQuitacaoDebitoTrFim]]", string.Empty); 
                    tr = tr.Replace("[[ANOMES]]", $"{conta.AnoMes.Substring(4)}/{conta.AnoMes.Substring(0, 4)}");
                    tr = tr.Replace("[[SEQORIGINALPAGO]]", conta.Id.ToString());
                    tr = tr.Replace("[[DATAVENCIMENTO]]", dtVencimento.ToString("dd/MM/yyyy"));
                    tr = tr.Replace("[[VALORORIGINAL]]", valor.ToString("N2"));

                    tableContasVinculadas += tr;

                    qtdLinhas++;
                    qtdLinhasParcial++;

                    // Verifica se atingiu o limite da página ou o final do conteúdo
                    if (qtdLinhasParcial == 8 || qtdLinhas == listaContas.Count())
                    {
                        htmlBody = htmlBody.Replace("[[COUNT_ANOMES]]", qtdLinhasParcial.ToString());
                        htmlBody = htmlBody.Replace("[[SUM_VALORORIGINAL]]", valorTotal.ToString("N2"));
                        htmlBody = htmlBody.Replace(templateTableContasVinculadas, tableContasVinculadas);

                        // Significa que ainda existe conteúdo a ser impresso. Qubra a página e reinia as variáveis para a montagem do HTML.
                        if (qtdLinhas < listaContas.Count())
                        {
                            htmlBody = htmlBody.Replace("[[LINHA_DIGITAVEL]]", string.Empty);
                            htmlBody = htmlBody.Replace("[[IMG_COD_BARRA]]", string.Empty);

                            // Armazena o HTML da página anterior
                            htmlBodyFinal += $"{headerHtml}{htmlBody}{GeraEspacamentoEntrePaginas()}";

                            // Obtem HTML inicial para o print da próxima página.
                            htmlBody = documentoModelo.BodyHtml;
                        }
                        else // Chegou ao final do conteúdo, então imprime o código de barras.
                        {
                            htmlBody = htmlBody.Replace("[[LINHA_DIGITAVEL]]", imprimeGRP.CodigoBarra.ToBarCode());
                            htmlBody = htmlBody.Replace("[[IMG_COD_BARRA]]", await _barcodeProvider.GerarImagemCodigoBarrasBase64Async(imprimeGRP.CodigoBarra));

                            htmlBodyFinal += headerHtml + htmlBody;
                        }

                        qtdLinhasParcial = 0;
                        valorTotal = 0;
                        tableContasVinculadas = string.Empty;
                    }
                }
            }

            return htmlBodyFinal;
        }
        private string GeraEspacamentoEntrePaginas()
        {
            return "<table class=\"table_espacamento_pagina\"><tr><td>&nbsp;</td></tr></table>";
        }

        private async Task<string> GeraHeaderHtmlDeclaracaoAnual(DeclaracaoAnual declaracaoAnual, VwCliente vwCliente, DocumentoModelo documentoModelo)
        {
            string htmlHeader = documentoModelo.HeaderHtml;

            var empresa = await _empresaRepository.ObterAsync();

            htmlHeader = htmlHeader.Replace("[[SIGLA]]", empresa.Sigla);
            htmlHeader = htmlHeader.Replace("[[HOST]]", $@"{_webHostEnvironment.ContentRootPath.Replace("\\", "/")}/");
            htmlHeader = htmlHeader.Replace("[[MATRICULA]]", declaracaoAnual.Matricula);
            htmlHeader = htmlHeader.Replace("[[NOME_CLIENTE]]", vwCliente.Nome);
            htmlHeader = htmlHeader.Replace("[[ENDERECO]]", GeraEnderecoCompletoCliente(vwCliente));
            htmlHeader = htmlHeader.Replace("[[SEQ_ORIGINAL]]", declaracaoAnual.Id.ToString());

            var dataAtual = DateTime.Now;
            htmlHeader = htmlHeader.Replace("[[DATA_EMISSAO]]", $"{dataAtual.ToShortDateString()} {dataAtual:HH:mm:ss}");

            return htmlHeader;
        }

        private string GeraEnderecoCompletoCliente(VwCliente vwCliente)
        {
            var logradouro = string.Empty;
            var numero = string.Empty;
            var complemento = string.Empty;

            if (vwCliente.Endereco != null) logradouro = vwCliente.Endereco;
            if (!string.IsNullOrEmpty(vwCliente.NumeroImovel) && !string.IsNullOrWhiteSpace(vwCliente.NumeroImovel)) numero = $", nº {vwCliente.NumeroImovel}";
            if (!string.IsNullOrEmpty(vwCliente.Complemento) && !string.IsNullOrWhiteSpace(vwCliente.Complemento)) complemento = $" - {vwCliente.Complemento}";

            return $"{logradouro}{numero}{complemento} - {vwCliente.NomeBairro} - {vwCliente.NomeCidade}";
        }

        private async Task<string> GeraBodyHtmlDeclaracaoAnual(string headerHtml,
                                                               string footerHtml,
                                                               DeclaracaoAnual declaracaoAnual,
                                                               IEnumerable<VwDadosDebitoAnual> listaDebitosAnual,
                                                               DocumentoModelo documentoModelo)
        {
            string htmlBody = documentoModelo.BodyHtml;

            htmlBody = htmlBody.Replace("[[TEXTOCOMPLEMENTAR]]", await GeraTextoComplementarDeclaracaoAnual(declaracaoAnual));
            htmlBody = htmlBody.Replace("[[TIPODOCUMENTO]]", await GeraTextoTipoDocumentoDeclaracaoAnual(declaracaoAnual));
            htmlBody = htmlBody.Replace("[[ANO_REF]]", declaracaoAnual.AnoRef);

            var templateTableQuitDeb = htmlBody.Slice("[[listaQuitacaoDebitoIni]]", "[[listaQuitacaoDebitoFim]]");

            if (listaDebitosAnual.Count() > 0)
            {
                var templateTableContasVinculadas = htmlBody.Slice("[[listaQuitacaoDebitoTrIni]]", "[[listaQuitacaoDebitoTrFim]]");

                decimal valorTotal = 0;
                int qtdLinhas = 0;
                int qtdLinhasParcial = 0;

                var tableContasVinculadas = string.Empty;

                foreach (var conta in listaDebitosAnual)
                {
                    decimal valor = conta.ValorOriginal;
                    DateTime dtVencimento = conta.DataVencimento;
                    valorTotal += valor;

                    var tr = templateTableContasVinculadas.Replace("[[listaQuitacaoDebitoTrIni]]", string.Empty).Replace("[[listaQuitacaoDebitoTrFim]]", string.Empty);
                    tr = tr.Replace("[[ANOMES]]", conta.AnoMesCt);
                    tr = tr.Replace("[[SEQORIGINALPAGO]]", conta.Id.ToString());
                    tr = tr.Replace("[[DATAVENCIMENTO]]", dtVencimento.ToString("dd/MM/yyyy"));
                    tr = tr.Replace("[[VALORORIGINAL]]", valor.ToString("N2"));

                    tableContasVinculadas += tr;

                    qtdLinhas++;
                    qtdLinhasParcial++;
                }

                htmlBody = htmlBody.Replace("[[COUNT_ANOMES]]", qtdLinhasParcial.ToString());
                htmlBody = htmlBody.Replace("[[SUM_VALORORIGINAL]]", valorTotal.ToString("N2"));
                htmlBody = htmlBody.Replace(templateTableContasVinculadas, tableContasVinculadas);
            }
            else
            {
                htmlBody = htmlBody.Replace(templateTableQuitDeb, string.Empty);
            }

            string htmlBodyFinal = headerHtml + htmlBody + footerHtml;

            return htmlBodyFinal;
        }

        private async Task<string> GeraTextoComplementarDeclaracaoAnual(DeclaracaoAnual declaracaoAnual)
        {
            return await Task.FromResult(declaracaoAnual.TipoDeclaracao == TipoDeclaracaoEnum.Parcial ?
                                         $@"Em atendimento a Lei 12.007/2009, informamos que referente ao período de vencimento
                                            de janeiro a dezembro/{declaracaoAnual.AnoRef},  as  notas fiscais abaixo relacionadas são objeto de
                                            parcelamento ainda em curso. Estando as demais quitadas." :
                                         $@"Em atendimento a Lei 12.007/2009, informamos que não constam débitos de notas fiscais vencidas 
                                            entre janeiro e dezembro/{declaracaoAnual.AnoRef}");
        }

        private async Task<string> GeraTextoTipoDocumentoDeclaracaoAnual(DeclaracaoAnual declaracaoAnual)
        {
            return declaracaoAnual.TipoDeclaraParcial switch
            {
                TipoDeclaraParcialEnum.NormalTotal => await Task.FromResult(string.Empty),
                TipoDeclaraParcialEnum.Judice => await Task.FromResult(TipoDeclaraParcialEnum.Judice.GetDescription()),
                TipoDeclaraParcialEnum.Parcelamento => await Task.FromResult(TipoDeclaraParcialEnum.Parcelamento.GetDescription()),
                _ => await Task.FromResult(string.Empty),
            };
        }

        private async Task<string> GeraFooterHtmlDeclaracaoAnual(string canalAtendimento, DocumentoModelo documentoModelo)
        {
            string htmlFooter = documentoModelo.FooterHtml;

            htmlFooter = htmlFooter.Replace("[[NOME_USUARIO]]", canalAtendimento);

            return await Task.FromResult(htmlFooter);
        }

        #endregion

    }
}
