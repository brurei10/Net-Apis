﻿using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Bases
{
    public abstract class BaseBusiness
    {
        const long PARAMETRO_INTEGRACAO_GFA = 728;

        protected readonly IParametroSistemaRepository _parametroSistemaRepository;

        public BaseBusiness(IParametroSistemaRepository parametroSistemaRepository)
        {
            _parametroSistemaRepository = parametroSistemaRepository;
        }

        public async Task<BaseResponseViewModel> OnlineAsync()
        {
            var parametro = await _parametroSistemaRepository?.ObterAsync(PARAMETRO_INTEGRACAO_GFA);

            var model = new BaseResponseViewModel()
            {
                // Parâmetro não existe ou inativo
                Sucesso = (parametro is null || (parametro.Valor != 1 || parametro.Situacao == 2)) ? false : true
            };

            if (!model.Sucesso)
            {
                model.AdicionaMensagem("Integração com iNova indisponível");
            }

            return model;
        }

    }
}
