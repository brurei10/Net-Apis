﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.CallCenter;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using GAB.Inova.Common.Interfaces.Providers;
using System;
using GAB.Inova.Common.Extensions;
using AutoMapper;
using GAB.Inova.Common.ViewModels.Base;
using Newtonsoft.Json;
using System.Linq;

namespace GAB.Inova.Business
{
    public class CallCenterBusiness : ICallCenterBusiness
    {
        readonly ILogger<CallCenterBusiness> _logger;
        readonly IMapper _mapper;
        readonly IConfiguration _configuration;
        readonly ITokenProvider _tokenProvider;
        readonly ICryptographyProvider _cryptographyProvider;
        readonly IEmpresaRepository _empresaRepository;
        readonly IInstanciaRepository _instanciaRepository;
        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        readonly IClienteCallCenterRepository _clienteCallCenterRepository;
        readonly IVwDadosClienteInterrupcaoRepository _vwDadosClienteInterrupcaoRepository;
        readonly IInterrupcaoRepository _interrupcaoRepository;
        readonly IInformacaoContaRepository _informacaoContaRepository;

        public CallCenterBusiness(ILoggerFactory loggerFactory, 
                                  IMapper mapper,
                                  IConfiguration configuration, 
                                  ITokenProvider tokenProvider,
                                  ICryptographyProvider cryptographyProvider,
                                  IEmpresaRepository empresaRepository,
                                  IInstanciaRepository instanciaRepository,
                                  IProtocoloAtendimentoRepository protocoloAtendimentoRepository,
                                  IClienteCallCenterRepository clienteCallCenterRepository,
                                  IVwDadosClienteInterrupcaoRepository vwDadosClienteInterrupcaoRepository,
                                  IInterrupcaoRepository interrupcaoRepository,
                                  IInformacaoContaRepository informacaoContaRepository)
        {
            _logger = loggerFactory.CreateLogger<CallCenterBusiness>();

            _mapper = mapper;
            _configuration = configuration;
            _tokenProvider = tokenProvider;
            _cryptographyProvider = cryptographyProvider;
            _empresaRepository = empresaRepository;
            _instanciaRepository = instanciaRepository;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
            _clienteCallCenterRepository = clienteCallCenterRepository;
            _vwDadosClienteInterrupcaoRepository = vwDadosClienteInterrupcaoRepository;
            _interrupcaoRepository = interrupcaoRepository;
            _informacaoContaRepository = informacaoContaRepository;
        }

        public async Task<GerarAutenticacaoResponseViewModel> GerarAutenticacaoAsync(GerarAutenticacaoRequestViewModel viewModel)
        {
            var retorno = new GerarAutenticacaoResponseViewModel()
            {
                Sucesso = false
            };

            if (string.IsNullOrEmpty(viewModel.codConcessao))
            {
                retorno.AdicionaMensagem($"O campo código da concessão é obrigatório.");

                return retorno;
            }

            if (string.IsNullOrEmpty(viewModel.LoginOperador))
            {
                retorno.AdicionaMensagem($"O campo LoginOperador é obrigatório.");

                return retorno;
            }

            if (string.IsNullOrEmpty(viewModel.Protocolo))
            {
                retorno.AdicionaMensagem($"O campo Protocolo é obrigatório.");

                return retorno;
            }

            var empresa = await _empresaRepository.ObterAsync();

            if (string.IsNullOrEmpty(empresa?.UrlEndPoint))
            {
                retorno.AdicionaMensagem($"Não foi encontratro URL do endpoint para a concessão [{empresa.Sigla}].");

                return retorno;
            }

            var securityKey = _configuration.GetValue<string>("TwoFactorMaskPrivateKey");

            if (string.IsNullOrEmpty(securityKey))
            {
                retorno.AdicionaMensagem($"Não foi encontrado a tag securityKey");

                return retorno;
            }

            var token = await _tokenProvider.ObterTokenContextoAtualAsync();

            // Configura de forma dinâmica a composição de parâmetros da rota de acesso
            var chave0 = _cryptographyProvider.Md5Encrypt(new Uri(empresa.UrlEndPoint).Authority);
            var chave1 = _cryptographyProvider.AddCryptographyt(empresa.Id.Substring(0, 2).ToInt().ToString());
            var chave2 = _cryptographyProvider.AddCryptographyt(viewModel.Protocolo);
            var chave3 = _cryptographyProvider.AddCryptographyt(viewModel.LoginOperador);
            var chave4 = _cryptographyProvider.AddCryptographyt($"Bearer {token}");
            var chave5 = _cryptographyProvider.AddCryptographyt("newflow");

            // Chave segura e os hashs gerados dinâmicamente
            var chavePrivada = _cryptographyProvider.Md5Encrypt(string.Format(securityKey, chave0, chave1, chave2, chave3, chave4, chave5), securityKey, true);

            var instancia = await _instanciaRepository.GerarAutenticacaoAsync(viewModel.LoginOperador, viewModel.Protocolo, chavePrivada);

            var gerarAutenticacaoResponseViewModel = _mapper.Map<GerarAutenticacaoResponseViewModel>(instancia);

            var urlCripto = $@"{empresa.UrlEndPoint}/{chave0}/{chave1}/{chave2}/{chave3}/{chave4}/{chave5}";

            gerarAutenticacaoResponseViewModel.Url = urlCripto;
            gerarAutenticacaoResponseViewModel.Sucesso = true;

            return gerarAutenticacaoResponseViewModel;
        }

        public async Task<FinalizarProtocoloAtendimentoResponseViewModel> FinalizarProtocoloAtendimentoAsync(string protocolo, string resolucao)
        {
            var result = new FinalizarProtocoloAtendimentoResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Finalizar protocolo de atendimento [nº:{protocolo}].");

            var validationResult = _protocoloAtendimentoRepository.NumeroProtocoloValidar(protocolo);
            if (!validationResult.IsValid)
            {
                result.AdicionaMensagem($"Protocolo inválido [nº:{protocolo}].");
                return result;
            }

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(protocolo);
            if (protocoloAtendimento is null)
            {
                result.AdicionaMensagem($"Protocolo não foi localizado [nº:{protocolo}].");
                return result;
            }

            var atendimento = await _protocoloAtendimentoRepository.FinalizarAsync(protocolo, resolucao);

            result = _mapper.Map<FinalizarProtocoloAtendimentoResponseViewModel>(atendimento);

            result.Sucesso = true;

            return result;
        }
        public async Task<BaseResponseViewModel> AssociarLigacaoProtocoloAsync(AssociarLigacaoProtocoloViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Associar ligação a protocolo model [{JsonConvert.SerializeObject(model)}].");

            if (string.IsNullOrEmpty(model.Protocolo))
            {
                result.AdicionaMensagem("Número do protocolo obrigatório");
                return result;
            }

            if (string.IsNullOrEmpty(model.NumLigacao))
            {
                result.AdicionaMensagem("Matrícula obrigatória");
                return result;
            }

            await _protocoloAtendimentoRepository.AssociarLigacaoAsync(model.Protocolo, model.NumLigacao);

            result.Sucesso = true;

            return result;
        }

        public async Task<ClienteResponseViewModel> ObterClienteAsync(string protocolo, string documento, string matricula)
        {
            var retorno = new ClienteResponseViewModel()
            {
                Sucesso = false
            };

            if (string.IsNullOrEmpty(protocolo))
            {
                retorno.AdicionaMensagem($"O campo protocolo é obrigatório.");
                return retorno;
            }

            if (string.IsNullOrEmpty(documento) && string.IsNullOrEmpty(matricula))
            {
                retorno.AdicionaMensagem($"Ao menos um dos parâmetros não obrigatórios deve ser preenchido( documento ou numLigacao ).");
                return retorno;
            }

            var cliente = await _clienteCallCenterRepository.ObterAsync(protocolo, documento, matricula);

            if (cliente.ligacoes != null && cliente.ligacoes.Count == 1)
            {
                matricula = cliente.ligacoes
                    .FirstOrDefault()?
                    .numLigacao;

                if (string.IsNullOrWhiteSpace(matricula))
                {
                    retorno.AdicionaMensagem($"O campo numLigacao é obrigatório.");
                    return retorno;
                }

                await _protocoloAtendimentoRepository.AssociarLigacaoAsync(protocolo, matricula);

                foreach (var ligacao in cliente.ligacoes)
                {
                    var dadosInterrupcao = await _vwDadosClienteInterrupcaoRepository.ObterAsync(matricula);

                    if (dadosInterrupcao is null)
                    {
                        retorno.AdicionaMensagem($"Dados do cliente não encontrado [matricula:{matricula}].");
                        return retorno;
                    }

                    var interrupcao = await _interrupcaoRepository.ConsultarInterrupcaoAsync(dadosInterrupcao);

                    ligacao.previsaoRetorno = interrupcao?.PrazoTerminoEmHoras;
                    ligacao.flgInterrupcaoAbast = interrupcao != null && interrupcao.IdStatus == 3 ? 1 : 0;
                }
            }

            var obterLigacoesResponseViewModel = _mapper.Map<ClienteResponseViewModel>(cliente);

            return obterLigacoesResponseViewModel;
        }

        public async Task<InformacaoContaResponseViewModel> ObterInformacaoContaAsync(string protocolo, string matricula)
        {
            var retorno = new InformacaoContaResponseViewModel()
            {
                Sucesso = false
            };

            if (string.IsNullOrEmpty(protocolo))
            {
                retorno.AdicionaMensagem($"O campo protocolo é obrigatório.");
                return retorno;
            }


            if (string.IsNullOrEmpty(matricula))
            {
                retorno.AdicionaMensagem($"O campo numLigacao é obrigatório.");
                return retorno;
            }

            var informacaoConta = await _informacaoContaRepository.ObterAsync(protocolo, matricula);

            var informacaoContaResponseViewModel = _mapper.Map<InformacaoContaResponseViewModel>(informacaoConta);

            return informacaoContaResponseViewModel;
        }


        #region [Métodos privados]

        #endregion
    }

}
