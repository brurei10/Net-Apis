﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Servico;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GAB.Inova.Business
{
    public class OrdemServicoEletronicaBusiness
        : IOrdemServicoEletronicaBusiness
    {

        readonly IMapper _mapper;
        readonly ILogger _logger;
        readonly IOrdemServicoAnexoExternoRepository _ordemServicoAnexoExternoRepository;
        readonly IOrdemServicoEletronicaControleRepository _ordemServicoEletronicaControleRepository;
        readonly IOrdemServicoEletronicaRepository _ordemServicoEletronicaRepository;
        readonly IOrdemServicoDetalheRepository _ordemServicoDetalheRepository;
        readonly IMunicipioRepository _municipioRepository;
        readonly IBairroRepository _bairroRepository;

        public OrdemServicoEletronicaBusiness(IMapper mapper,
                                              ILoggerFactory loggerFactory,
                                              IOrdemServicoAnexoExternoRepository ordemServicoAnexoExternoRepository,
                                              IOrdemServicoEletronicaControleRepository ordemServicoEletronicaControleRepository,
                                              IOrdemServicoEletronicaRepository ordemServicoEletronicaRepository,
                                              IOrdemServicoDetalheRepository ordemServicoDetalheRepository,
                                              IMunicipioRepository municipioRepository,
                                              IBairroRepository bairroRepository)
        {
            _mapper = mapper;
            _ordemServicoAnexoExternoRepository = ordemServicoAnexoExternoRepository;
            _ordemServicoEletronicaControleRepository = ordemServicoEletronicaControleRepository;
            _ordemServicoEletronicaRepository = ordemServicoEletronicaRepository;
            _ordemServicoDetalheRepository = ordemServicoDetalheRepository;
            _municipioRepository = municipioRepository;
            _bairroRepository = bairroRepository;


            _logger = loggerFactory.CreateLogger<OrdemServicoEletronicaBusiness>();
        }

        public async Task<BaseResponseViewModel> SalvarRequisicaoAsync(OsEletronicaRequestViewModel model)
        {
            using (var transaction = new TransactionScope())
            {
                var retorno = new BaseResponseViewModel(sucesso: false);

                var ordemServicoEletronica = MapToModel(model.OrdemServicoPrincipal);

                #region OS_ELETRONICA_CONTROLE

                var controle = new OrdemServicoEletronicaControle(ordemServicoEletronica.GetNroOS(), ordemServicoEletronica.GetSeqOS());
                controle.ValidationResult = _ordemServicoEletronicaControleRepository.PodeAlterar(controle);
                if (!controle.ValidationResult.IsValid)
                {
                    retorno.AdicionaMensagens(controle.ValidationResult.Errors.Select(e => e.Message));
                    return retorno;
                }

                await _ordemServicoEletronicaControleRepository.RegistrarRecebimentoOrdemServicoEletronicaAsync(controle);

                #endregion

                #region DET_ORDEM_SERVICO

                var detalheOs = new OrdemServicoDetalhe(ordemServicoEletronica.NumeroOS.GetValueOrDefault(0));
                detalheOs.ValidationResult = _ordemServicoDetalheRepository.PodeAlterarStatusParaAguardandoApropriacao(detalheOs);
                if (!detalheOs.ValidationResult.IsValid)
                {
                    retorno.AdicionaMensagens(detalheOs.ValidationResult.Errors.Select(e => e.Message));
                    return retorno;
                }

                await _ordemServicoDetalheRepository.AlterarStatusAsync(detalheOs.NumeroOS, StatusDetOrdemServicoEnum.AGUARDANDO_APROPRIACAO);

                #endregion

                #region OS_ELETRONICA (PRINCIPAL)

                retorno = await InserirOrdemServicoEletronicaAsync(ordemServicoEletronica);

                #endregion

                if (retorno.Sucesso)
                {
                    #region OS_ELETRONICA (ASSOCIADAS)

                    if (model.OrdensServicoAssociadas?.Count > 0)
                    {
                        foreach (var oOAssociadaViewModel in model.OrdensServicoAssociadas)
                        {
                            var oSAssociada = MapToModel(oOAssociadaViewModel, OSOrigemId: ordemServicoEletronica.Id, ehAssociada: true);
                            if (oSAssociada.MotivoBaixaID.HasValue)
                            {
                                retorno = await InserirOrdemServicoEletronicaAsync(oSAssociada, ehAssociada: true);

                                if (retorno.Sucesso)
                                {
                                    await _ordemServicoEletronicaRepository.GerarOSComOSEletronicaAssociadaAsync(oSAssociada);
                                }
                                else
                                {
                                    break;
                                }
                            }                            
                        }
                    }

                    #endregion

                    if (retorno.Sucesso)
                    {
                        transaction.Complete();
                    }
                }

                return retorno;
            }
        }

        #region [Métodos privados]
        private async Task<BaseResponseViewModel> InserirOrdemServicoEletronicaAsync(OrdemServicoEletronica ordemServicoEletronica, bool ehAssociada = false)
        {
            var retorno = new BaseResponseViewModel();

            ordemServicoEletronica.ValidationResult = _ordemServicoEletronicaRepository.PodeIncluir(ordemServicoEletronica);

            if (ordemServicoEletronica.ValidationResult.IsValid)
            {
                var valido = true;

                if (!string.IsNullOrEmpty(ordemServicoEletronica.CodigoMunicipio))
                {
                    ordemServicoEletronica.ValidationResult = _municipioRepository.MunicipioValido(new Municipio { Codigo = ordemServicoEletronica.CodigoMunicipio });

                    valido = ordemServicoEletronica.ValidationResult.IsValid;
                }

                if (valido)
                {
                    if (ordemServicoEletronica.BairroID.HasValue && ordemServicoEletronica.BairroID != null)
                    {
                        ordemServicoEletronica.ValidationResult = _bairroRepository.BairroValido(new Bairro { Id = ordemServicoEletronica.BairroID.HasValue ? (long)ordemServicoEletronica.BairroID : default(long) });

                        valido = ordemServicoEletronica.ValidationResult.IsValid;
                    }

                    if (valido)
                    {
                        await _ordemServicoEletronicaRepository.InserirAsync(ordemServicoEletronica);
                        retorno = await InserirFotosAsync(ordemServicoEletronica);
                    }
                    else
                    {
                        retorno.Sucesso = false;
                        var preMensagem = ehAssociada ? $"OS Associada de Sequencia '{ordemServicoEletronica.SequencialVinculada}'" : "OS Principal";
                        retorno.AdicionaMensagens(ordemServicoEletronica.ValidationResult.Errors.Select(e => $"{preMensagem} - {e.Message}"));
                    }
                }
                else
                {
                    retorno.Sucesso = false;
                    var preMensagem = ehAssociada ? $"OS Associada de Sequencia '{ordemServicoEletronica.SequencialVinculada}'" : "OS Principal";
                    retorno.AdicionaMensagens(ordemServicoEletronica.ValidationResult.Errors.Select(e => $"{preMensagem} - {e.Message}"));
                }
            }
            else
            {
                retorno.Sucesso = false;
                var preMensagem = ehAssociada ? $"OS Associada de Sequencia '{ordemServicoEletronica.SequencialVinculada}'" : "OS Principal";
                retorno.AdicionaMensagens(ordemServicoEletronica.ValidationResult.Errors.Select(e => $"{preMensagem} - {e.Message}"));
            }
            return retorno;
        }

        private OrdemServicoEletronica MapToModel(OsEletronicaViewModel viewModel, long? OSOrigemId = null, bool ehAssociada = false)
        {
            try
            {
                var domain = _mapper.Map<OrdemServicoEletronica>(viewModel);
                domain.OSEletronicaOrigemID = OSOrigemId;
                domain.EhAssociada = ehAssociada;

                if (ehAssociada)
                    domain.IdentificadorOS = null;

                return domain;
            }
            catch (Exception ex)
            {
                if (ehAssociada)
                {
                    _logger.LogCritical(ex, $"Erro ao realizar o automap entre a viewmodel e o dominio - OS Eletronica Origem {0} e Sequencial {1} - {2}", OSOrigemId, viewModel.SequencialVinculada, ex.Message);
                }
                else
                {
                    _logger.LogCritical(ex, $"Erro ao realizar o automap entre a viewmodel e o dominio - OS Eletronica {0} - {1}", viewModel.IdentificadorExterno, ex.Message);
                }

                throw;
            }
        }

        private async Task<BaseResponseViewModel> InserirFotosAsync(OrdemServicoEletronica os)
        {
            if (os.Fotos != null)
            {
                foreach (var foto in os.Fotos)
                {
                    foto.OSEletronicaID = os.Id;
                    foto.NumeroOS = os.NumeroOS;
                    var result = await InsertAnexoAsync(foto);
                    if (!result.Sucesso)
                    {
                        return result;
                    }
                }
            }
            return new BaseResponseViewModel();
        }

        private async Task<BaseResponseViewModel> InsertAnexoAsync(OrdemServicoAnexoExterno foto)
        {
            foto.ValidationResult = _ordemServicoAnexoExternoRepository.PodeIncluir(foto);
            var retorno = new BaseResponseViewModel();

            if (foto.ValidationResult.IsValid)
            {
                await _ordemServicoAnexoExternoRepository.InserirAsync(foto);
            }
            else
            {
                retorno.Sucesso = false;
                retorno.AdicionaMensagens(foto.ValidationResult.Errors.Select(e => $"Foto '{foto.Descricao}' - {e.Message}"));
            }
            return retorno;
        }

        #endregion
    }
}
