﻿using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Mensagem;
using GAB.Inova.Common.ViewModels.SendGrid;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IMensagemIntegradorBusiness
    {

        Task<EmailResponseViewModel> EnviarMensagemAsync(EmailRequestViewModel model);
        Task<BaseResponseViewModel> AtualizarStatusAsync(AtualizarStatusMensagemViewModel model);

    }
}
