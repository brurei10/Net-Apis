﻿using GAB.Inova.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Business.Interfaces
{
    public interface IContaBusiness
    {
        Conta ObterPorNumero(long numero);
    }

}
