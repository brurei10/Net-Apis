﻿using GAB.Inova.Common.ViewModels.Servico;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IOperacaoAguaBusiness
    {
        Task<InterrupcaoAbastecimentoResponseViewModel> ConsultarInterrupcaoAbastecimentoAsync(string numeroProtocolo);
    }
}
