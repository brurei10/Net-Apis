﻿using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Servico;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IOrdemServicoEletronicaBusiness
    {
        Task<BaseResponseViewModel> SalvarRequisicaoAsync(OsEletronicaRequestViewModel model);
    }
}
