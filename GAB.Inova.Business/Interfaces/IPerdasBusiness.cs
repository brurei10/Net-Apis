﻿using System;
using System.Collections.Generic;
using System.Text;
using GAB.Inova.Common.ViewModels.Perdas;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace GAB.Inova.Business.Interfaces
{
    public interface IPerdasBusiness
    {
        Task<ConsultarHidrometrosViewModel> ConsultarHidrometrosViewModel(DateTime dataInicio, DateTime dataFim);
        Task<ConsultarLeiturasViewModel> ConsultarLeiturasViewModel(DateTime dataInicio, DateTime DataFim);
        Task<ConsultarLeiturasViewModel> ConsultarLeiturasMacroViewModel();
        void InserirLog(StatusCodeResult resultado);


    }
}
