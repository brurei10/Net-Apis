﻿using GAB.Inova.Common.ViewModels.Consumo;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IConsumoBusiness
    {

        Task<HistoricoConsumosResponseViewModel> ObterHistoricoConsumosAsync(string numeroProtocolo);

    }
}
