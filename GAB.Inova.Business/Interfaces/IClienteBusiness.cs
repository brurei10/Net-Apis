﻿using GAB.Inova.Common.ViewModels.Cliente;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IClienteBusiness
    {

        Task<ClienteResponseViewModel> ObterClientePorMatriculaAsync(string matricula);
        Task<ClienteTokenResponseViewModel> ObterClientePorTokenAsync(string token);
        Task<ClienteSobJudiceResponseViewModel> VerificarClienteSobJudiceAsync(string matricula);
        Task<ObterLigacoesResponseViewModel> ObterLigacoesAsync(string documento);
    }
}
