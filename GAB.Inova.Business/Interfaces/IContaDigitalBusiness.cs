﻿using GAB.Inova.Common.ViewModels;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IContaDigitalBusiness
    {

        Task<BaseResponseViewModel> EnviarContaAsync(EnviarContaDigitalRequestViewModel model);
        Task<BaseResponseViewModel> EnviarContaClienteSemAdesaoAsync(EnviarContaDigitalRequestViewModel model);
        Task<BaseResponseViewModel> ConfirmarSolicitacaoAsync(string token, string protocolo);
        Task<BaseResponseViewModel> SolicitarAdesaoAsync(AderirContaDigitalRequestViewModel model);
        Task<BaseResponseViewModel> SolicitarAlteracaoAsync(AlterarContaDigitalRequestViewModel model);
        Task<BaseResponseViewModel> SolicitarCancelamentoAsync(CancelarContaDigitalRequestViewModel model);
        
    }
}
