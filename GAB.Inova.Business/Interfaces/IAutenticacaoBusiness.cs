﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Cliente;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IAutenticacaoBusiness
    {
        Task<AutenticarClienteResponseViewModel> AutenticarClienteAsync(AutenticarClienteRequestViewModel viewModel, IEnumerable<EmpresaTipoEnum> indisponibilidades = null);
    }
}
