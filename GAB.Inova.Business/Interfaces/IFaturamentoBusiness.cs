﻿using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Faturamento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IFaturamentoBusiness
    {

        Task<BaseResponseViewModel> EnviarEmailSegundaViaAsync(long seqOriginal, string email);
        Task<HistoricoContasResponseViewModel> ObterHistoricoContasAsync(string numeroProtocolo);
        Task<HistoricoContasResponseViewModel> ObterHistoricoContasEmAbertoAsync(string numeroProtocolo);
        Task<SegundaViaContaResponseViewModel> ObterSegundaViaAsync(long seqOriginal);
        Task<CodigoBarrasResponseViewModel> ObterCodigoBarrasNotaFiscalAsync(long seqOriginal);
        Task<CodigoBarrasResponseViewModel> ObterCodigoBarrasQuitacaoDebitoAsync(IEnumerable<long> seqOriginais, string numeroProtocolo);
        Task<PdfQuitacaoDebitoResponseViewModel> ObterPdfQuitacaoDebitoAsync(IEnumerable<long> seqOriginais, string numeroProtocolo);
    }
}
