﻿using GAB.Inova.Common.ViewModels.Base;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces.Bases
{
    public interface IBaseBusiness
    {

        Task<BaseResponseViewModel> OnlineAsync();

    }
}
