﻿using GAB.Inova.Business.Interfaces.Bases;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IAtendimentoBusiness
        : IBaseBusiness
    {

        Task<BaseResponseViewModel> RegistrarNaturezaAsync(RegistrarNaturezaViewModel model);

        Task<CancelarIntegracaoGfaResponseViewModel> CancelarIntegracaoGfaAsync(CancelarIntegracaoGfaRequestViewModel model);

        Task<FinalizarIntegracaoGfaResponseViewModel> FinalizarIntegracaoGfaAsync(FinalizarIntegracaoGfaRequestViewModel model);

        Task<IniciarIntegracaoGfaResponseViewModel> IniciarIntegracaoGfaAsync(IniciarIntegracaoGfaRequestViewModel model);

        Task<VerificarIntegracaoGfaResponseViewModel> VerificarIntegracaoGfaAsync(VerificarIntegracaoGfaRequestViewModel model);

        Task<BaseResponseViewModel> VerificarAtendimentoEmAbertoAsync(string username);

        Task<BaseResponseViewModel> FinalizarProtocoloAtendimentoAsync(string numeroProtocolo);

        Task<GerarProtocoloAtendimentoResponseViewModel> GerarProtocoloAtendimentoAsync(string matricula, 
                                                                                        CanalAtendimentoEnum canalAtendimento, 
                                                                                        DateTime? DataFim = null, 
                                                                                        bool matriculaObrigatoria = true);

        Task<BaseResponseViewModel> EnviarEmailFaleConoscoByAplicativoAsync(EnviarEmailFaleConoscoByAplicativoRequestViewModel model);

        Task<BaseResponseViewModel> EnviarEmailOuvidoriaByAplicativoAsync(EnviarEmailOuvidoriaByAplicativoRequestViewModel model);
    }
}
