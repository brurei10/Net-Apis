﻿using GAB.Inova.Common.ViewModels.AgenciaVirtual;
using GAB.Inova.Common.ViewModels.Base;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface IAgenciaVirtualBusiness
    {
        Task<AutenticarClienteResponseViewModel> AutenticarClienteAsync(AutenticarClienteRequestViewModel viewModel);
        Task<HistoricoContasResponseViewModel> ObterHistoricoContasAsync(string numeroProtocolo);
        Task<HistoricoContasResponseViewModel> ObterHistoricoContasEmAbertoAsync(string numeroProtocolo);
        Task<ContaResponseViewModel> ObterQuitacaoDebitoAsync(string[] seqOriginais, long numeroProtocolo);
        Task<CodigoBarrasResponseViewModel> ObterCodigoBarrasQuitacaoDebitoAsync(string seqOriginal, string numeroProtocolo);
        Task<PdfQuitacaoDebitoResponseViewModel> ObterPdfQuitacaoDebitoAsync(string seqOriginal, string numeroProtocolo);
        Task<DeclaracoesAnuaisResponseViewModel> ObterDeclaracoesAnuaisAsync(string numeroProtocolo);
        Task<PdfDeclaracaoAnualResponseViewModel> ObterPdfDeclaracaoAnualAsync(long seqDeclaracaoAnual, string numeroProtocolo);
        Task<BaseResponseViewModel> EnviarEmailQuitacaoDebitoAsync(string seqOriginal, string numeroProtocolo, string email);
    }
}
