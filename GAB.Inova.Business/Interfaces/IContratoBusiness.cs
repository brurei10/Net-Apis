﻿using GAB.Inova.Domain;


namespace GAB.Inova.Business.Interfaces
{
    public interface IContratoBusiness 
    {
        Contrato ObterPorMatricula(string matricula);
        void Persistir(Contrato contrato);


    }
}
