﻿

namespace GAB.Inova.Business.Interfaces
{
    public interface IConnectionTenantResolver
    {
        string ResolveConnectionString();
    }
}
