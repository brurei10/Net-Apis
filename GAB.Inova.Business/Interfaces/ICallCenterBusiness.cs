﻿using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.CallCenter;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface ICallCenterBusiness
    {
        Task<GerarAutenticacaoResponseViewModel> GerarAutenticacaoAsync(GerarAutenticacaoRequestViewModel viewModel);
        Task<FinalizarProtocoloAtendimentoResponseViewModel> FinalizarProtocoloAtendimentoAsync(string protocolo, string resolucao);
        Task<BaseResponseViewModel> AssociarLigacaoProtocoloAsync(AssociarLigacaoProtocoloViewModel model);
        Task<ClienteResponseViewModel> ObterClienteAsync(string protocolo, string documento, string matricula);
        Task<InformacaoContaResponseViewModel> ObterInformacaoContaAsync(string protocolo, string matricula);
    }
}
