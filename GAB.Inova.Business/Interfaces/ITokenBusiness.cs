﻿using GAB.Inova.Common.ViewModels;
using GAB.Inova.Common.ViewModels.Token;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GAB.Inova.Business.Interfaces
{
    public interface ITokenBusiness
    {

        Task<TokenResponseViewModel> GerarTokenAsync(IEnumerable<KeyValuePair<string, string>> claims, int? segundosParaExpirar = null);
        Task<ClaimsPrincipal> ValidarTokenAsync(string token);

    }
}
