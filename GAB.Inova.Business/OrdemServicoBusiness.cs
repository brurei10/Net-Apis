﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Servico;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class OrdemServicoBusiness : IOrdemServicoBusiness
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        private readonly IVwDadosConsultaOrdemServicoRepository _vwDadosConsultaOrdemServicoRepository;


        public OrdemServicoBusiness(IMapper mapper,
                                    ILoggerFactory loggerFactory,
                                    IVwDadosConsultaOrdemServicoRepository vwDadosConsultaOrdemServicoRepository)
        {
            _mapper = mapper;
            _vwDadosConsultaOrdemServicoRepository = vwDadosConsultaOrdemServicoRepository;

            _logger = loggerFactory.CreateLogger<OrdemServicoBusiness>();
        }

        public async Task<ConsultaOsResponseViewModel> ConsultarOrdemServicoAsync(long numeroOs)
        {
            var consultaOsResponseViewModel = new ConsultaOsResponseViewModel();

            var ordemServico = await _vwDadosConsultaOrdemServicoRepository.ObterAsync(numeroOs);

            if (ordemServico != null)
            {
                consultaOsResponseViewModel = _mapper.Map<ConsultaOsResponseViewModel>(ordemServico);

                consultaOsResponseViewModel.Sucesso = true;
            }

            return consultaOsResponseViewModel;
        }
    }
}
