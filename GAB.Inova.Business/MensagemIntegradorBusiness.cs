﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Common.ViewModels.Mensagem;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class MensagemIntegradorBusiness
        : IMensagemIntegradorBusiness
    {

        readonly IMapper _mapper;
        readonly IConfiguration _configuration;
        readonly IAvisoDebitoRepository _avisoDebitoRepository;
        readonly IClienteRepository _clienteRepository; 
        readonly IEmailIntegradorRepository _emailIntegradorRepository;
        readonly IMensagemIntegradorRepository _mensagemIntegradorRepository; 
        readonly IMensagemIntegradorMovimentoRepository _mensagemIntegradorMovimentoRepository;
        readonly IContaDigitalEnvioRepository _contaDigitalEnvioRepository; 
        readonly IContaDigitalEnvioMovimentoRepository _contaDigitalEnvioMovimentoRepository; 
        readonly IContaRepository _contaRepository;
        readonly IHistoricoLigacaoRepository _historicoLigacaoRepository;
        readonly IUnitOfWork _unitOfWork; 
        readonly ILogger<MensagemIntegradorBusiness> _logger; 

        public MensagemIntegradorBusiness(IMapper mapper,
                                          IConfiguration configuration,
                                          IAvisoDebitoRepository avisoDebitoRepository,
                                          IClienteRepository clienteRepository,
                                          IEmailIntegradorRepository emailIntegradorRepository,
                                          IMensagemIntegradorRepository mensagemIntegradorRepository,
                                          IContaDigitalEnvioRepository contaDigitalEnvioRepository,
                                          IContaDigitalEnvioMovimentoRepository contaDigitalEnvioMovimentoRepository,
                                          IContaRepository contaRepository,
                                          IMensagemIntegradorMovimentoRepository mensagemIntegradorMovimentoRepository,
                                          IHistoricoLigacaoRepository historicoLigacaoRepository,
                                          IUnitOfWork unitOfWork,
                                          ILoggerFactory loggerFactory)
        {
            _mapper = mapper;
            _configuration = configuration;
            _avisoDebitoRepository = avisoDebitoRepository;
            _clienteRepository = clienteRepository;
            _emailIntegradorRepository = emailIntegradorRepository;
            _mensagemIntegradorRepository = mensagemIntegradorRepository;
            _mensagemIntegradorMovimentoRepository = mensagemIntegradorMovimentoRepository;
            _contaDigitalEnvioRepository = contaDigitalEnvioRepository;
            _contaDigitalEnvioMovimentoRepository = contaDigitalEnvioMovimentoRepository;
            _contaRepository = contaRepository;
            _historicoLigacaoRepository = historicoLigacaoRepository;
            _unitOfWork = unitOfWork;
            _logger = loggerFactory.CreateLogger<MensagemIntegradorBusiness>();
        }

        
        public async Task<EmailResponseViewModel> EnviarMensagemAsync(EmailRequestViewModel model)
        {
            var integradorResult = await _emailIntegradorRepository.EnviarEmailAsync(model);
            var response = _mapper.Map<EmailResponseViewModel>(integradorResult);
            if (!integradorResult.Success) response.AdicionaMensagem(integradorResult.Erros);
            return response;
        }

        public async Task<BaseResponseViewModel> AtualizarStatusAsync(AtualizarStatusMensagemViewModel model)
        {
            switch (model.Event.ToLower())
            {
                case "open":
                    await TratarAberto(model);
                    break;
                case "processed":
                    await TratarProcessado(model);
                    break;
                case "delivered":
                    await TratarEntregue(model);
                    break;
                case "spamreport":
                    await TratarSpam(model);
                    break;
                case "deferred":
                    await TratarAdiamento(model);
                    break;
                case "dropped":
                case "bounce":
                    await TratarNaoEntregue(model);
                    break;
                default:
                    await TratarEventoDesconhecido(model);
                    break;
             }

            return new BaseResponseViewModel();
        }
               
        #region [Métodos privados]

        private async Task TratarAberto(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.Aberto)
                {
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Aberto;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento }))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }

                    _unitOfWork.Commit();
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarProcessado(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.Processado)
                {
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Processado;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento }))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }

                    _unitOfWork.Commit();
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarEntregue(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                var tratarAviso = false;

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.Entregue)
                {                    
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Entregue;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    var mensagemIntegradorMovimento = new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento };

                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(mensagemIntegradorMovimento))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }
                    
                    var contaDigitalEnvio = await _contaDigitalEnvioRepository.ObterPorIdMensagemEnvioAsync(mensagemIntegrador.Id);
                    var conta = default(Conta);

                    if (contaDigitalEnvio != null)
                    {
                        conta = await _contaRepository.ObterContaPorSeqOriginalAsync(contaDigitalEnvio.SeqOriginal);
                        if (conta == null) throw new ArgumentException($"Conta de numero fiscal {contaDigitalEnvio.SeqOriginal} não localizada - MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}'");

                        if (contaDigitalEnvio.Status != ContaDigitalEnvioStatusEnum.Enviado)
                        {                            
                            contaDigitalEnvio.Status = ContaDigitalEnvioStatusEnum.Enviado;

                            if (!await _contaDigitalEnvioRepository.AtualizarAsync(contaDigitalEnvio))
                            {
                                _logger.LogError($"Erro ao atualizar o status da ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] para [Status: {contaDigitalEnvio.Status.ToString()}].");
                            }

                            var mensagemEnvioContaMov = $"Novo status registrado pela IntegradorMovimento Id:{mensagemIntegradorMovimento.Id}";
                            if (!await _contaDigitalEnvioMovimentoRepository.InserirAsync(new ContaDigitalEnvioMovimento(contaDigitalEnvio) { Mensagem = mensagemEnvioContaMov }))
                            {
                                _logger.LogError($"Erro ao inserir Movimento para o ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] - [Mensagem: {mensagemEnvioContaMov}].");
                            }

                            tratarAviso = true;
                        }                                              
                    }
                    
                    _unitOfWork.Commit();
                    
                    if (tratarAviso)
                    {
                        await TratarAvisoDebido(conta, cancelar: false);
                    }
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarSpam(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.ReportadoSpam)
                {
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.ReportadoSpam;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    var mensagemIntegradorMovimento = new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento };

                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(mensagemIntegradorMovimento))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }

                    var contaDigitalEnvio = await _contaDigitalEnvioRepository.ObterPorIdMensagemEnvioAsync(mensagemIntegrador.Id);
                    if (contaDigitalEnvio != null)
                    {
                        var conta = await _contaRepository.ObterContaPorSeqOriginalAsync(contaDigitalEnvio.SeqOriginal);
                        if (conta == null) throw new ArgumentException($"Conta de numero fiscal {contaDigitalEnvio.SeqOriginal} não localizada - MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}'");

                        if (contaDigitalEnvio.Status != ContaDigitalEnvioStatusEnum.Enviado)
                        {
                            contaDigitalEnvio.Status = ContaDigitalEnvioStatusEnum.Enviado;

                            if (!await _contaDigitalEnvioRepository.AtualizarAsync(contaDigitalEnvio))
                            {
                                _logger.LogError($"Erro ao atualizar o status da ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] para [Status: {contaDigitalEnvio.Status.ToString()}].");
                            }

                            var mensagemEnvioContaMov = $"Novo status registrado pela IntegradorMovimento Id:{mensagemIntegradorMovimento.Id}";
                            if (!await _contaDigitalEnvioMovimentoRepository.InserirAsync(new ContaDigitalEnvioMovimento(contaDigitalEnvio) { Mensagem = mensagemEnvioContaMov }))
                            {
                                _logger.LogError($"Erro ao inserir Movimento para o ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] - [Mensagem: {mensagemEnvioContaMov}].");
                            }
                        }
                    }

                    _unitOfWork.Commit();                   
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarAdiamento(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.Adiado)
                {
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Adiado;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    var mensagemIntegradorMovimento = new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento };

                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(mensagemIntegradorMovimento))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }
                    
                    _unitOfWork.Commit();
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarNaoEntregue(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                var tratarAviso = false;

                if (mensagemIntegrador.Status != MensagemIntegradoStatusEnum.Negado)
                {
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Negado;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu um erro ao atualizar o status do MensagemIntegrador [Id: {mensagemIntegrador.Id}] para [Status: {mensagemIntegrador.Status.ToString()}].");
                    }

                    var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                    var mensagemIntegradorMovimento = new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento };

                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(mensagemIntegradorMovimento))
                    {
                        _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                    }

                    var contaDigitalEnvio = await _contaDigitalEnvioRepository.ObterPorIdMensagemEnvioAsync(mensagemIntegrador.Id);
                    var conta = default(Conta);

                    if (contaDigitalEnvio != null)
                    {
                        conta = await _contaRepository.ObterContaPorSeqOriginalAsync(contaDigitalEnvio.SeqOriginal);
                        if (conta == null) throw new ArgumentException($"Conta de numero fiscal {contaDigitalEnvio.SeqOriginal} não localizada - MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}'");

                        contaDigitalEnvio.Status = ContaDigitalEnvioStatusEnum.EmailNaoEntregue;

                        if (!await _contaDigitalEnvioRepository.AtualizarAsync(contaDigitalEnvio))
                        {
                            _logger.LogError($"Erro ao atualizar o status da ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] para [Status: {contaDigitalEnvio.Status.ToString()}].");
                        }

                        var mensagemEnvioContaMov = $"Novo status registrado pela IntegradorMovimento Id:{mensagemIntegradorMovimento.Id}";
                        if (!await _contaDigitalEnvioMovimentoRepository.InserirAsync(new ContaDigitalEnvioMovimento(contaDigitalEnvio) { Mensagem = mensagemEnvioContaMov }))
                        {
                            _logger.LogError($"Erro ao inserir Movimento para o ContaDigitalEnvio [Id: {contaDigitalEnvio.Id}] - [Mensagem: {mensagemEnvioContaMov}].");
                        }

                        tratarAviso = await VerificarGrupoEntregaDaReferenciaAsync(conta);

                    }

                    _unitOfWork.Commit();

                    if (tratarAviso)
                    {
                        await TratarAvisoDebido(conta, cancelar: true, motivo: MotivoCancelamentoContaDigitalEnum.CaixaIndisponivel);

                        // Desfazer o grupo de entrega de email para o definido nas configurações
                        if (!await _clienteRepository.AtualizaGrupoEntregaAsync(conta.Matricula, (long)GrupoEntregaEnum.DOMICILIO))
                        {
                            var mensagem = $"Ocorreu um erro ao remover o grupo de entrega 'Email' da matricula: {conta.Matricula}.";
                            _logger.LogError(mensagem);
                            await EnviarEmailDeAvisoDeErroAsync(mensagem, MensagemIntegradorTipoEnum.Informativo);
                        }

                        var mensagemContaDigital = $"Erro ao entregar Conta Digital Numero Fiscal {conta.Id}, matrícula {conta.Matricula}";

                        await EnviarEmailDeAvisoDeErroAsync(mensagemContaDigital, MensagemIntegradorTipoEnum.ContaDigitalErroRecebimentoEmail);
                    }
                }
            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        private async Task TratarEventoDesconhecido(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                var mensagemIntegrador = await _mensagemIntegradorRepository.ObterPorGuidAsync(model.MensagemId);
                if (mensagemIntegrador == null) throw new ArgumentException($"Não foi possível localizar a MensagemIntegrador de Guid: '{model.MensagemId}' para registrar o evento: '{model.Event}' ");

                _unitOfWork.BeginTransaction();

                var mensagemMovimento = $"Evento: {model.Event} - Guid: {model.MensagemId} - DataHora: {UnixTimestampToDateTime(model.Timestamp).ToString("dd/MM/yyyy HH:mm:ss")}";
                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = mensagemMovimento }))
                {
                    _logger.LogError($"Erro ao inserir Movimento para o MensagemIntegrador [Id: {mensagemIntegrador.Id}] - [Mensagem: {mensagemMovimento}].");
                }

                _unitOfWork.Commit();

            }
            catch (Exception)
            {
                _unitOfWork.Rollback();

                throw;
            }
        }
        
        private DateTime UnixTimestampToDateTime(double unixTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, DateTimeKind.Utc);
        }

        private async Task<bool> TratarAvisoDebido(Conta conta, bool cancelar, MotivoCancelamentoContaDigitalEnum? motivo = null)
        {
            try
            {
                if (await _avisoDebitoRepository.VerificarSeExisteAvisoAtivoAsync(conta.Id))
                {
                    _unitOfWork.BeginTransaction();

                    // Assinala o aviso de débito devido como entregue
                    await _avisoDebitoRepository.ApropriarOsAsync(conta.Id, DateTime.Today, cancelar, motivo);

                    _unitOfWork.Commit();
                }

                return true;
            }
            catch (Exception ex)
            {
                var mensagem = $"Erro ao apropriar o aviso de debito [Matricula:{conta.Matricula} - Conta:{conta.Id} - Ref:{conta.AnoMes}] como {(!cancelar ? "entregue" : "cancelado")}.";

                _logger.LogCritical(ex, mensagem);

                _unitOfWork.Rollback();

                await EnviarEmailDeAvisoDeErroAsync(mensagem, MensagemIntegradorTipoEnum.Informativo);

                return false;
            }
        }

        private async Task<bool> EnviarEmailDeAvisoDeErroAsync(string conteudo, MensagemIntegradorTipoEnum tipo)
        {
            try
            {
                var mensagemModel = new EmailRequestViewModel
                {
                    Conteudo = conteudo,
                    Destinatarios = _configuration.GetValue<string>("EmailGrupoFaturamento"),
                    RemetenteNome = _configuration.GetValue<string>("NomeRemetenteSistema"),
                    RemetenteEnderecoEmail = _configuration.GetValue<string>("EmailRemetenteSistema"),
                    Tipo = tipo
                };

                var mensagemIntegrador = _mapper.Map<MensagemIntegrador>(mensagemModel);

                _unitOfWork.BeginTransaction();

                if (!await _mensagemIntegradorRepository.InserirAsync(mensagemIntegrador))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para o envio de e-mail de aviso com o conteudo - {conteudo}");
                }

                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Aviso de erro na apropriação de Aviso de débito na atualização de mensagem" }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegradorMovimento para o MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
                }

                _unitOfWork.Commit();

                var result = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

                if (!result.Success)
                {
                    _logger.LogError($"Ocorreu erro ao enviar e-mail de informação no retorno do status do SendGrid - {result.Erros} ");

                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;

                    _unitOfWork.BeginTransaction();

                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu erro ao atualizar o status para '{MensagemIntegradoStatusEnum.Falha.ToString()}' do MensagemIntegrador de id: '{mensagemIntegrador.Id}'");
                    }

                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = result.Erros }))
                    {
                        _logger.LogError($"Ocorreu erro ao inserir movimentação com o status '{MensagemIntegradoStatusEnum.Falha.ToString()}' para o MensagemIntegrador de id: '{mensagemIntegrador.Id}'");
                    }

                    _unitOfWork.Commit();

                    return false;
                }
                else
                {
                    mensagemIntegrador.GuidMensagem = result.MessageGuid;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Ocorreu erro ao atualizar o Id Mensagem para {result.MessageGuid} do MensagemIntegrador de id: '{mensagemIntegrador.Id}'");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro no envio de email de Aviso de Erro na atualização de status - {ex.Message}");
                _unitOfWork.Rollback();
                return false;
            }
        }

        private async Task<bool> VerificarGrupoEntregaDaReferenciaAsync(Conta conta)
        {
            try
            {
                var historicoLigacao = await _historicoLigacaoRepository.ObterPorMatriculaEReferenciaAsync(conta.Matricula, conta.AnoMes);

                return historicoLigacao?.IdGrupoEntrega == GrupoEntregaEnum.EMAIL;
            }
            catch(Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar o grupo entrega da referência - {ex.Message}");

                return false;
            }
        }

        #endregion
        
    }
}
