﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Token;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class TokenBusiness
        : ITokenBusiness
    {

        readonly ITokenProvider _tokenProvider;

        public TokenBusiness(ITokenProvider tokenProvider)
        {
            _tokenProvider = tokenProvider;
        }

        public async Task<TokenResponseViewModel> GerarTokenAsync(IEnumerable<KeyValuePair<string, string>> claims, int? segundosParaExpirar = null)
        {
            return await _tokenProvider.GerarTokenModelAsync(claims, segundosParaExpirar);
        }

        public async Task<ClaimsPrincipal> ValidarTokenAsync(string token)
        {
            return await _tokenProvider.ObterTokenAsync(token);
        }
        
    }
}
