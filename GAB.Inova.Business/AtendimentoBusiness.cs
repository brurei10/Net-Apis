﻿using AutoMapper;
using GAB.Inova.Business.Bases;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class AtendimentoBusiness
        : BaseBusiness, IAtendimentoBusiness
    {

        #region [Constants]

        const string CONFIG_SENDGRID_EMAIL_APLICATIVO = "AplicativoEmails";

        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO = "FaleConosco";
        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO_TO = "Destinatario";
        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO_FROM = "Remetente";

        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA = "Ouvidoria";
        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA_TO = "Destinatario";
        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA_FROM = "Remetente";

        const string CONFIG_SENDGRID_EMAIL_APLICATIVO_AGENERSA = "Agenersa";

        #endregion

        readonly IMapper _mapper;
        readonly IConfiguration _configuration;
        readonly IEnvironmentVariables _environmentVariables;
        readonly IUnitOfWork _unitOfWork;
        readonly IClienteRepository _clienteRepository;
        readonly IContratoRepository _contratoRepository;
        readonly IEmailIntegradorRepository _emailIntegradorRepository;
        readonly IIntegradorConfigFactory _integradorConfigFactory;
        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        readonly IProtocoloAtendimentoGfaRepository _protocoloAtendimentoGfaRepository;
        readonly IProtocoloAtendimentoDetalheRepository _protocoloAtendimentoDetalheRepository;
        readonly IUsuarioRepository _usuarioRepository;
        readonly ILogger _logger;

        public AtendimentoBusiness(IMapper mapper,
                                   IConfiguration configuration,
                                   IEnvironmentVariables environmentVariables,
                                   IClienteRepository clienteRepository,
                                   IContratoRepository contratoRepository,
                                   IEmailIntegradorRepository emailIntegradorRepository,
                                   IIntegradorConfigFactory integradorConfigFactory,
                                   IParametroSistemaRepository parametroSistemaRepository,
                                   IProtocoloAtendimentoRepository protocoloAtendimentoRepository,
                                   IProtocoloAtendimentoDetalheRepository protocoloAtendimentoDetalheRepository,
                                   IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository,
                                   IUsuarioRepository usuarioRepository,
                                   IUnitOfWork unitOfWork,
                                   ILoggerFactory loggerFactory)
            : base(parametroSistemaRepository)
        {
            _mapper = mapper;
            _configuration = configuration;
            _environmentVariables = environmentVariables;
            _clienteRepository = clienteRepository;
            _contratoRepository = contratoRepository;
            _emailIntegradorRepository = emailIntegradorRepository;
            _integradorConfigFactory = integradorConfigFactory;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
            _protocoloAtendimentoGfaRepository = protocoloAtendimentoGfaRepository;
            _protocoloAtendimentoDetalheRepository = protocoloAtendimentoDetalheRepository;
            _usuarioRepository = usuarioRepository;
            _unitOfWork = unitOfWork;

            _logger = loggerFactory.CreateLogger<AtendimentoBusiness>();
        }

        public async Task<CancelarIntegracaoGfaResponseViewModel> CancelarIntegracaoGfaAsync(CancelarIntegracaoGfaRequestViewModel model)
        {
            var retorno = _mapper.Map<CancelarIntegracaoGfaRequestViewModel, CancelarIntegracaoGfaResponseViewModel>(model);

            try
            {
                _unitOfWork.BeginTransaction();

                var protocoloAtendimentoGfa = _mapper.Map<ProtocoloAtendimentoGfa>(model);

                _logger.LogInformation("Validando cancelamento dos dados da integração com o GFA.");

                protocoloAtendimentoGfa.ValidationResult = _protocoloAtendimentoGfaRepository.PodeAlterar(protocoloAtendimentoGfa);

                if (!protocoloAtendimentoGfa.ValidationResult.IsValid)
                {
                    _unitOfWork.Rollback();

                    retorno.AdicionaMensagens(protocoloAtendimentoGfa.ValidationResult.Errors.Select(e => e.Message));

                    return retorno;
                }

                _logger.LogInformation("Recuperando protocolo(s) aberto(s) através do número de protocolo e senha GFA.");

                var protocoloAtendimentos = await _protocoloAtendimentoRepository.ListarProtocolosAbertosPorNumeroESenhaGfaAsync(model.NumeroProtocoloGfa, model.SenhaGfa);

                if (protocoloAtendimentos != null && protocoloAtendimentos.Any())
                {
                    foreach (var pa in protocoloAtendimentos)
                    {
                        _logger.LogInformation("Inserindo a natureza [DESISTÊNCIA] no detalhe do protocolo de atendimento.");

                        var result = await _protocoloAtendimentoDetalheRepository.InserirAsync(new ProtocoloAtendimentoDetalhe()
                        {
                            IdNatureza = 37, //DESISTÊNCIA
                            IdProtocoloAtendimento = pa.Id,
                            IdRotina = _environmentVariables.IdRotina,
                            TipoProtocolo = pa.TipoProtocolo
                        });

                        if (!result)
                        {
                            _unitOfWork.Rollback();

                            retorno.AdicionaMensagem($"Erro ao executar cancelamento automático para o protocolo iNova [nº: {pa.Numero}].");

                            return retorno;
                        }

                        pa.Observacao += "CANCELAMENTO AUTOMÁTICO ATRAVÉS DA INTEGRAÇÃO COM GFA";

                        if (!await _protocoloAtendimentoRepository.FinalizarAsync(pa))
                        {
                            throw new InvalidOperationException($"Não foi possível cancelar no iNova o protocolo de atendimento [id: {pa.Id}].");
                        }
                    }
                }

                retorno.Sucesso = await _protocoloAtendimentoGfaRepository.FinalizarAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                if (retorno.Sucesso)
                {
                    var numeroProtocoloPai = await _protocoloAtendimentoGfaRepository.ObterNumeroProtocoloAtendimentoPaiAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                    if (!string.IsNullOrEmpty(numeroProtocoloPai) && await _protocoloAtendimentoRepository.AtualizarDataIntegracaoAsync(numeroProtocoloPai))
                    {
                        _logger.LogInformation($"Data de integração do(s) protocolo(s) do iNova atualizada(s).");
                    }

                    _logger.LogInformation($"Atendimento cancelado com suceso.");
                }
                else throw new InvalidOperationException($"Não foi possível cancelar a integração com o GFA [nº protocolo:{protocoloAtendimentoGfa.NumeroProtocoloGfa}; senha: {protocoloAtendimentoGfa.SenhaGfa}].");

                return retorno;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();

                throw (ex);
            }
            finally
            {
                _unitOfWork.Commit();
            }
        }

        public async Task<FinalizarIntegracaoGfaResponseViewModel> FinalizarIntegracaoGfaAsync(FinalizarIntegracaoGfaRequestViewModel model)
        {
            var retorno = _mapper.Map<FinalizarIntegracaoGfaRequestViewModel, FinalizarIntegracaoGfaResponseViewModel>(model);

            try
            {
                _unitOfWork.BeginTransaction();

                var protocoloAtendimentoGfa = _mapper.Map<ProtocoloAtendimentoGfa>(model);

                _logger.LogInformation("Validando finalização dos dados da integração com o GFA.");

                protocoloAtendimentoGfa.ValidationResult = _protocoloAtendimentoGfaRepository.PodeAlterar(protocoloAtendimentoGfa);

                if (!protocoloAtendimentoGfa.ValidationResult.IsValid)
                {
                    _unitOfWork.Rollback();

                    retorno.AdicionaMensagens(protocoloAtendimentoGfa.ValidationResult.Errors.Select(e => e.Message));

                    return retorno;
                }

                _logger.LogInformation("Recuperando protocolo(s) aberto(s) através do número de protocolo e senha GFA.");

                var protocoloAtendimentos = await _protocoloAtendimentoRepository.ListarProtocolosAbertosPorNumeroESenhaGfaAsync(model.NumeroProtocoloGfa, model.SenhaGfa);

                if (protocoloAtendimentos != null && protocoloAtendimentos.Any())
                {
                    foreach (var pa in protocoloAtendimentos)
                    {

                        _logger.LogInformation($"Verificando se existe possível finalizar o protocolo de atendimento do iNova de número [{pa.Numero}].");

                        pa.ValidationResult = _protocoloAtendimentoRepository.PodeFinalizar(pa);

                        if (!pa.ValidationResult.IsValid)
                        {
                            _unitOfWork.Rollback();

                            retorno.TemErroValidacaoProtocoloAtendimento = true;
                            retorno.AdicionaMensagens(pa.ValidationResult.Errors.Select(e => e.Message));

                            return retorno;
                        }

                        pa.Observacao += "FINALIZAÇÃO AUTOMÁTICA ATRAVÉS DA INTEGRAÇÃO COM GFA";

                        if (!await _protocoloAtendimentoRepository.FinalizarAsync(pa))
                        {
                            throw new InvalidOperationException($"Não foi possível finalizar no iNova o protocolo de atendimento [id: {pa.Id}].");
                        }
                    }

                    _logger.LogInformation("Protocolo(s) de atendimento finalizado(s) no iNova.");
                }

                retorno.Sucesso = await _protocoloAtendimentoGfaRepository.FinalizarAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                if (retorno.Sucesso)
                {
                    var numeroProtocoloPai = await _protocoloAtendimentoGfaRepository.ObterNumeroProtocoloAtendimentoPaiAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                    if (!string.IsNullOrEmpty(numeroProtocoloPai))
                    {
                        if (await _protocoloAtendimentoRepository.AtualizarDataIntegracaoAsync(numeroProtocoloPai))
                        {
                            _logger.LogInformation($"Data de integração do(s) protocolo(s) do iNova atualizada(s).");
                        }

                        var naturezasSolicitacao = await _protocoloAtendimentoRepository.ObterNaturezasSolicitacaoConsolidadoAsync(numeroProtocoloPai);

                        retorno.NaturezasSolicitacao = _mapper.Map<IEnumerable<NaturezaSolicitacaoViewModel>>(naturezasSolicitacao);

                        _logger.LogInformation($"Natureza(s) da solicitação consolidada(s) recuperada(s).");
                    }

                    _logger.LogInformation($"Atendimento finalizado com sucesso.");
                }
                else throw new InvalidOperationException($"Não foi possível finalizar a integração com o GFA [nº protocolo:{protocoloAtendimentoGfa.NumeroProtocoloGfa}; senha: {protocoloAtendimentoGfa.SenhaGfa}].");

                return retorno;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();

                throw (ex);
            }
            finally
            {
                _unitOfWork.Commit();
            }
        }

        public async Task<IniciarIntegracaoGfaResponseViewModel> IniciarIntegracaoGfaAsync(IniciarIntegracaoGfaRequestViewModel model)
        {
            var retorno = _mapper.Map<IniciarIntegracaoGfaRequestViewModel, IniciarIntegracaoGfaResponseViewModel>(model);

            try
            {
                _unitOfWork.BeginTransaction();

                #region [Recuperar e verificar situação do usuário no Inova]

                _logger.LogInformation("Recuperando e validando usuário no iNova.");

                var usuario = await _usuarioRepository.ObterPorNomeAcessoAsync(model.UsuarioGfa, true);
                if (usuario is null)
                {
                    // verificar se o usuário está inativo
                    usuario = await _usuarioRepository.ObterPorNomeAcessoAsync(model.UsuarioGfa, false);

                    if (usuario is null)
                    {
                        retorno.AdicionaMensagem($"Usuário [{model.UsuarioGfa}] não encontrado no iNova.");

                        return retorno;
                    }
                    else
                    {
                        retorno.AdicionaMensagem($"Usuário [{model.UsuarioGfa}] inativo no iNova.");

                        return retorno;
                    }
                }

                #endregion

                var protocoloAtendimentoGfa = await ObterProtocoloAtendimentoGfaAsync(model);

                if (!protocoloAtendimentoGfa.ValidationResult.IsValid)
                {
                    _unitOfWork.Rollback();

                    retorno.AdicionaMensagens(protocoloAtendimentoGfa.ValidationResult.Errors.Select(e => e.Message));

                    return retorno;
                }

                var protocoloAtendimento = await ObterProtocoloAsync(CanalAtendimentoEnum.PresencialLoja, idUsuario: usuario.Id);

                if (!protocoloAtendimento.ValidationResult.IsValid)
                {
                    _unitOfWork.Rollback();

                    retorno.AdicionaMensagens(protocoloAtendimento.ValidationResult.Errors.Select(e => e.Message));

                    return retorno;
                }

                retorno = await AssociarGfaAoProtocoloInovaAsync(protocoloAtendimentoGfa, protocoloAtendimento);

                if (!retorno.Sucesso) _unitOfWork.Rollback();
                else _logger.LogInformation($"Atendimento iniciado com sucesso.");

                return retorno;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();

                throw (ex);
            }
            finally
            {
                _unitOfWork.Commit();
            }
        }

        public async Task<VerificarIntegracaoGfaResponseViewModel> VerificarIntegracaoGfaAsync(VerificarIntegracaoGfaRequestViewModel model)
        {
            var retorno = _mapper.Map<VerificarIntegracaoGfaRequestViewModel, VerificarIntegracaoGfaResponseViewModel>(model);

            _logger.LogInformation("Recuperando protocolo atendimento por numero e senha do GFA.");

            var protocoloAtendimentoGfa = await _protocoloAtendimentoGfaRepository.ObterPorNumeroESenhaAsync(model.NumeroProtocoloGfa, model.SenhaGfa);

            if (protocoloAtendimentoGfa is null)
            {
                retorno.AdicionaMensagem("O número de protocolo e senha do GFA não existem.");

                return retorno;
            }

            retorno.DataInicio = protocoloAtendimentoGfa.DataInicioAtendimento;

            if (protocoloAtendimentoGfa.DataFinalAtendimento.HasValue)
            {
                retorno.DataFim = protocoloAtendimentoGfa.DataFinalAtendimento.Value;

                var numeroProtocoloPai = await _protocoloAtendimentoGfaRepository.ObterNumeroProtocoloAtendimentoPaiAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                _logger.LogInformation($"Número do protocolo pai [{numeroProtocoloPai}] do atendimento.");

                var protocoloAtendimentos = await _protocoloAtendimentoRepository.ListarProtocolosNaoIntegradosPorNumeroESenhaGfaAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

                _logger.LogInformation("Verificando data da integração.");

                if (protocoloAtendimentos != null && protocoloAtendimentos.Any())
                {
                    if (await _protocoloAtendimentoRepository.AtualizarDataIntegracaoAsync(numeroProtocoloPai))
                    {
                        _logger.LogInformation("Data de integração do(s) protocolo(s) do iNova atualizada(s).");
                    }
                }

                _logger.LogInformation("Recuperando natureza(s) consolidada(s) do atendimento.");

                var naturezasSolicitacao = await _protocoloAtendimentoRepository.ObterNaturezasSolicitacaoConsolidadoAsync(numeroProtocoloPai);

                retorno.NaturezasSolicitacao = _mapper.Map<IEnumerable<NaturezaSolicitacaoViewModel>>(naturezasSolicitacao);
                retorno.Sucesso = true;
            }
            else retorno.Sucesso = true;

            _logger.LogInformation("Verificação do atendimento realizado com sucesso.");

            return retorno;
        }

        public async Task<BaseResponseViewModel> RegistrarNaturezaAsync(RegistrarNaturezaViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Validando protocolo [nº:{model?.NumeroProtocolo}].");

            var validationResult = _protocoloAtendimentoRepository.NumeroProtocoloValidar(model?.NumeroProtocolo);
            if (!validationResult.IsValid)
            {
                result.AdicionaMensagens(validationResult.Errors.Select(e => e.Message));

                return result;
            }

            var protocolo = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(model?.NumeroProtocolo);
            if (protocolo is null)
            {
                result.AdicionaMensagem($"Protocolo não foi localizado [nº:{model?.NumeroProtocolo}].");

                return result;
            }

            _logger.LogInformation($"Verificando se a natureza [id:{model?.NaturezaId}] já foi registrada para o protocolo [nº:{model?.NumeroProtocolo}].");

            if (!await _protocoloAtendimentoDetalheRepository.VerificarExistenciaDeNaturezaAsync(protocolo.Id, model.NaturezaId))
            {
                result.Sucesso = await _protocoloAtendimentoDetalheRepository.InserirAsync(new ProtocoloAtendimentoDetalhe()
                {
                    IdProtocoloAtendimento = protocolo.Id,
                    IdNatureza = model.NaturezaId,
                    IdRotina = (int)model.RotinaId
                });

                _logger.LogInformation("Natureza registrada com sucesso.");
            }
            else result.AdicionaMensagem("Natureza já registrada para o protocolo.");

            return result;
        }

        public async Task<BaseResponseViewModel> VerificarAtendimentoEmAbertoAsync(string username)
        {
            var retorno = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            #region [Recuperar e verificar situação do usuário no Inova]

            _logger.LogInformation("Recuperando e validando usuário no iNova.");

            var usuario = await _usuarioRepository.ObterPorNomeAcessoAsync(username, true);
            if (usuario is null)
            {
                // verificar se o usuário está inativo
                usuario = await _usuarioRepository.ObterPorNomeAcessoAsync(username, false);

                if (usuario is null)
                {
                    retorno.AdicionaMensagem($"Usuário [{username}] não encontrado no iNova.");

                    return retorno;
                }
                else
                {
                    retorno.AdicionaMensagem($"Usuário [{username}] inativo no iNova.");

                    return retorno;
                }
            }

            #endregion

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterProtocoloNaoFinalizadoPorUsuarioAsync(usuario.Id);
            if (protocoloAtendimento != null)
            {
                retorno.AdicionaMensagem($"Protocolo nº [{protocoloAtendimento.Numero}] não finalizado no iNova para o usuário [{usuario.NomeAcesso}].");

                return retorno;
            }

            var protocoloAtendimentoGfa = await _protocoloAtendimentoGfaRepository.ObterAtendimentoNaoFinalizadoPorUsuarioAsync(usuario.NomeAcesso);
            if (protocoloAtendimentoGfa != null)
            {
                retorno.AdicionaMensagem($"Atendimento [nº protocolo gfa:{protocoloAtendimentoGfa.NumeroProtocoloGfa}, senha gfa:{protocoloAtendimentoGfa.SenhaGfa}] não finalizado para o usuário [{usuario.NomeAcesso}].");

                return retorno;
            }

            retorno.Sucesso = true;

            return retorno;
        }

        public async Task<BaseResponseViewModel> FinalizarProtocoloAtendimentoAsync(string numeroProtocolo)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Finalizar protocolo de atendimento [nº:{numeroProtocolo}].");

            var validationResult = _protocoloAtendimentoRepository.NumeroProtocoloValidar(numeroProtocolo);
            if (!validationResult.IsValid)
            {
                result.AdicionaMensagem($"Protocolo inválido [nº:{numeroProtocolo}].");
                return result;
            }

            var protocolo = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);
            if (protocolo is null)
            {
                result.AdicionaMensagem($"Protocolo não foi localizado [nº:{numeroProtocolo}].");
                return result;
            }

            result.Sucesso = await _protocoloAtendimentoRepository.FinalizarAsync(protocolo);

            if (!result.Sucesso)
            {
                result.AdicionaMensagem($"Não foi possível finalizar no iNova o protocolo de atendimento [nº: {protocolo.Numero}].");
            }

            return result;
        }

        public async Task<GerarProtocoloAtendimentoResponseViewModel> GerarProtocoloAtendimentoAsync(string matricula, CanalAtendimentoEnum canalAtendimento, DateTime? DataFim = null, bool matriculaObrigatoria = true)
        {
            _logger.LogInformation($"Gerar protocolo de atendimento via {canalAtendimento.GetDescription()} para cliente com a matrícula [nº:{matricula}].");

            var retorno = new GerarProtocoloAtendimentoResponseViewModel()
            {
                Sucesso = false
            };

            if (matriculaObrigatoria || !string.IsNullOrEmpty(matricula))
            {
                var cliente = await _clienteRepository.ObterPorMatriculaAsync(matricula);
                if (cliente is null)
                {
                    retorno.AdicionaMensagem($"Não foi possível encontrar o cliente com a matrícula informada [nº:{matricula}].");
                    return retorno;
                }
            }

            var protocoloAtendimento = await ObterProtocoloAsync(canalAtendimento, 0, matricula, DataFim);

            if (!protocoloAtendimento.ValidationResult.IsValid)
            {
                retorno.AdicionaMensagens(protocoloAtendimento.ValidationResult.Errors.Select(q => q.Message));
                return retorno;
            }

            retorno.NumeroProtocolo = protocoloAtendimento.Numero;
            retorno.Sucesso = true;

            return retorno;
        }

        public async Task<BaseResponseViewModel> EnviarEmailFaleConoscoByAplicativoAsync(EnviarEmailFaleConoscoByAplicativoRequestViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Enviar e-mail de fale conosco [{JsonConvert.SerializeObject(model)}].");

            var sectionFaleConosco = _configuration.GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO)?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO);

            var remetente = sectionFaleConosco?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO_FROM)?
                .GetValue<string>(model.CodigoEmpresa);

            if (string.IsNullOrEmpty(remetente))
            {
                throw new ArgumentNullException("Não foi possível encontrar na configuração do sistema o [E-mail de Remetente Fale Conosco]: parâmetro nulo ou vazio.");
            }

            var destinatario = sectionFaleConosco?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_FALECONOSCO_TO)
                .GetValue<string>(model.CodigoEmpresa);

            if (string.IsNullOrEmpty(destinatario))
            {
                throw new ArgumentNullException("Não foi possível encontrar na configuração do sistema o [E-mail de Destinatário Fale Conosco]: parâmetro nulo ou vazio.");
            }

            var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.FaleConoscoPeloAplicativo);
            var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);

            mensagemModel.RemetenteEnderecoEmail = remetente;
            mensagemModel.Destinatarios = destinatario;
            mensagemModel.Conteudo = model;
            mensagemModel.ReplayToNome = model.NomeCliente;
            mensagemModel.ReplayToEmail = model.Email;

            var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

            result.Sucesso = emailIntegradorResponse.Success;

            if (!result.Sucesso)
            {
                result.AdicionaMensagem($"Sistema indisponível! Tente novamente.");
            }
            else result.AdicionaMensagem($"Mensagem enviada com sucesso através do protocolo {model.NumeroProtocoloAtual}. Aguarde sua resposta por e-mail.");

            return result;
        }

        public async Task<BaseResponseViewModel> EnviarEmailOuvidoriaByAplicativoAsync(EnviarEmailOuvidoriaByAplicativoRequestViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            _logger.LogInformation($"Enviar e-mail de ouvidoria [{JsonConvert.SerializeObject(model)}].");

            var protocolo = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(model.NumeroProtocoloAnterior);
            if (protocolo is null)
            {
                result.AdicionaMensagem($"O protocolo de atendimento informado não existe [nº:{model.NumeroProtocoloAnterior}]");
                return result;
            }

            var sectionOuvidoria = _configuration.GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO)?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA);

            var remetente = sectionOuvidoria?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA_FROM)?
                .GetValue<string>(model.CodigoEmpresa);

            if (string.IsNullOrEmpty(remetente))
            {
                throw new ArgumentNullException("Não foi possível encontrar na configuração do sistema [E-mail de Remetente Ouvidoria]: parâmetro nulo ou vazio.");
            }

            var destinatario = sectionOuvidoria?
                .GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO_OUVIDORIA_TO)
                .GetValue<string>(model.CodigoEmpresa);

            if (string.IsNullOrEmpty(destinatario))
            {
                throw new ArgumentNullException("Não foi possível encontrar na configuração do sistema [E-mail de Destinatário Ouvidoria]: parâmetro nulo ou vazio.");
            }

            var destinatarios = new List<string>();

            destinatarios.Add(destinatario);

            if (model.CopiarAgenersa)
            {
                var emailAgenersa = _configuration.GetSection(CONFIG_SENDGRID_EMAIL_APLICATIVO)?
                    .GetValue<string>(CONFIG_SENDGRID_EMAIL_APLICATIVO_AGENERSA);

                if (!string.IsNullOrEmpty(emailAgenersa))
                {
                    destinatarios.Add(emailAgenersa);
                }
                else _logger.LogWarning("Não foi possível encontrar na configuração do sistema [Contato Agenersa]: parâmetro nulo ou vazio.");
            }

            var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.OuvidoriaPeloAplicativo);
            var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);

            mensagemModel.RemetenteEnderecoEmail = remetente;
            mensagemModel.Destinatarios = string.Join("; ", destinatarios);
            mensagemModel.Conteudo = model;
            mensagemModel.ReplayToNome = model.NomeCliente;
            mensagemModel.ReplayToEmail = model.Email;

            var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

            result.Sucesso = emailIntegradorResponse.Success;

            if (!result.Sucesso)
            {
                result.AdicionaMensagem($"Sistema indisponível! Tente novamente.");
            }
            else result.AdicionaMensagem($"Mensagem enviada com sucesso através do protocolo {model.NumeroProtocoloAtual}. Aguarde sua resposta por e-mail.");

            return result;
        }

        #region [Métodos privados]

        private async Task<ProtocoloAtendimento> ObterProtocoloAsync(CanalAtendimentoEnum canalAtendimento, long? idUsuario = null, string matricula = "", DateTime? DataFim = null)
        {
            var protocoloAtendimento = new ProtocoloAtendimento()
            {
                Observacao = $"PROTOCOLO GERADO POR {canalAtendimento.GetDescription()}.",
                IdTipoAtendimento = canalAtendimento,
                TipoProtocolo = 1,
                IdUsuario = (idUsuario.HasValue) ? idUsuario.Value : _environmentVariables.IdUsuario,
                DataFim = DataFim
            };

            _logger.LogInformation($"Objeto {protocoloAtendimento.GetType()} instanciado.");

            if (!string.IsNullOrEmpty(matricula))
            {
                var contrato = await _contratoRepository.ObterPorMatriculaAsync(matricula);
                
                protocoloAtendimento.Matricula = contrato?.Matricula;
                protocoloAtendimento.Solicitante = contrato?.NomeCliente;
                protocoloAtendimento.CpfCnpj = contrato?.CpfCnpj;
                protocoloAtendimento.Telefone = contrato?.TelefoneResidencial;
                protocoloAtendimento.Contrato = contrato;
            }

            _logger.LogInformation("Validando inclusão do protocolo de atendimento no iNova.");

            var validationResult = _protocoloAtendimentoRepository.PodeIncluir(protocoloAtendimento);

            if (!validationResult.IsValid)
            {
                protocoloAtendimento.ValidationResult = validationResult;

                return protocoloAtendimento;
            }

            _logger.LogInformation("Gerando protocolo de atendimento no iNova.");

            var numeroProtocolo = await _protocoloAtendimentoRepository.GerarAsync(protocoloAtendimento);

            _logger.LogInformation($"Validando o número de protocolo de atendimento gerado no iNova [{numeroProtocolo}].");

            validationResult = _protocoloAtendimentoRepository.NumeroProtocoloValidar(numeroProtocolo);

            if (!validationResult.IsValid)
            {
                protocoloAtendimento.ValidationResult = validationResult;

                return protocoloAtendimento;
            }

            _logger.LogInformation("Recuperando os dados protocolo de atendimento gerado no iNova.");

            protocoloAtendimento = await _protocoloAtendimentoRepository?.ObterPorNumeroAsync(numeroProtocolo);
            protocoloAtendimento.ValidationResult = validationResult;

            return protocoloAtendimento;
        }

        private async Task<ProtocoloAtendimentoGfa> ObterProtocoloAtendimentoGfaAsync(IniciarIntegracaoGfaRequestViewModel model)
        {
            var protocoloAtendimentoGfa = _mapper.Map<ProtocoloAtendimentoGfa>(model);

            _logger.LogInformation("Validando inclusão dos dados da integração com o GFA.");

            protocoloAtendimentoGfa.ValidationResult = _protocoloAtendimentoGfaRepository?.PodeIncluir(protocoloAtendimentoGfa);

            if (!protocoloAtendimentoGfa.ValidationResult.IsValid) return protocoloAtendimentoGfa;

            if (!await _protocoloAtendimentoGfaRepository.InserirAsync(protocoloAtendimentoGfa))
            {
                throw new InvalidOperationException("Não foi possível inserir dados da integração com o GFA.");
            }

            _logger.LogInformation("Dados da integração com o GFA incluídos.");

            return protocoloAtendimentoGfa;
        }
        
        private async Task<IniciarIntegracaoGfaResponseViewModel> AssociarGfaAoProtocoloInovaAsync(ProtocoloAtendimentoGfa protocoloAtendimentoGfa, ProtocoloAtendimento protocoloAtendimento)
        {
            _logger.LogInformation("Recuperando o número de protocolo de atendimento pai no iNova.");

            #region [Obter um protocolo pai para o protocolo e senha do GFA]

            protocoloAtendimento.NumeroProtocoloPai = await _protocoloAtendimentoGfaRepository?.ObterNumeroProtocoloAtendimentoPaiAsync(protocoloAtendimentoGfa.NumeroProtocoloGfa, protocoloAtendimentoGfa.SenhaGfa);

            if (string.IsNullOrEmpty(protocoloAtendimento.NumeroProtocoloPai))
            {
                protocoloAtendimento.NumeroProtocoloPai = protocoloAtendimento.Numero;
            }

            #endregion

            _logger.LogInformation("Associando dados da integração do GFA com o protocolo de atendimento do iNova.");

            protocoloAtendimento.IdProtocoloAtendimentoGfa = protocoloAtendimentoGfa.Id;

            if (!await _protocoloAtendimentoRepository.AtualizarIntegracaoGfaAsync(protocoloAtendimento))
            {
                throw new InvalidOperationException("Não foi possível associar dados da integração com o GFA a um protocolo no iNova.");
            }

            _logger.LogInformation("Dados associados com sucesso.");

            var result = new IniciarIntegracaoGfaResponseViewModel()
            {
                Sucesso = true
            };

            result = _mapper.Map<ProtocoloAtendimento, IniciarIntegracaoGfaResponseViewModel>(protocoloAtendimento, result);
            result = _mapper.Map<ProtocoloAtendimentoGfa, IniciarIntegracaoGfaResponseViewModel>(protocoloAtendimentoGfa, result);

            return result;
        }

        #endregion

    }
}
