﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class ClienteTokenBusiness
        : IClienteTokenBusiness
    {

        readonly IMapper _mapper;
        readonly ITokenProvider _tokenProvider;
        readonly IClienteTokenRepository _clienteTokenRepository;

        public ClienteTokenBusiness(IMapper mapper, ITokenProvider tokenProvider, IClienteTokenRepository clienteTokenRepository)
        {
            _mapper = mapper;
            _tokenProvider = tokenProvider;
            _clienteTokenRepository = clienteTokenRepository;
        }

        public async Task<ClienteTokenResponseViewModel> ObterViewModelPorTokenAsync(string token)
        {
            await _tokenProvider.ObterTokenAsync(token);

            var emailDoToken = await _tokenProvider.ObterClaimAsync(token, "EmailCadastro");
            var clienteToken = await _clienteTokenRepository.ObterPorTokenAsync(token);
            var tokenViewModel = _mapper.Map<ClienteTokenResponseViewModel>(clienteToken);

            tokenViewModel.EmailDoToken = emailDoToken;

            return tokenViewModel;
        }        
    }
}
