﻿using AutoMapper;
using GAB.Inova.Business.Bases;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Servico;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class OperacaoAguaBusiness: IOperacaoAguaBusiness
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IInterrupcaoRepository _interrupcaoRepository;
        private readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        private readonly IVwDadosClienteInterrupcaoRepository _vwDadosClienteInterrupcaoRepository;

        public OperacaoAguaBusiness(ILoggerFactory loggerFactory, 
                                    IMapper mapper,
                                    IInterrupcaoRepository interrupcaoRepository,
                                    IProtocoloAtendimentoRepository protocoloAtendimentoRepository,
                                    IVwDadosClienteInterrupcaoRepository vwDadosClienteInterrupcaoRepository)
        {
            _logger = loggerFactory.CreateLogger<OperacaoAguaBusiness>();
            _mapper = mapper;

            _interrupcaoRepository = interrupcaoRepository;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
            _vwDadosClienteInterrupcaoRepository = vwDadosClienteInterrupcaoRepository;
        }

        public async Task<InterrupcaoAbastecimentoResponseViewModel> ConsultarInterrupcaoAbastecimentoAsync(string numeroProtocolo)
        {
            var retorno = new InterrupcaoAbastecimentoResponseViewModel();

            var protocoloAtendimento = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);

            if (protocoloAtendimento is null)
            {
                retorno.AdicionaMensagem($"O protocolo de atendimento informado não existe [nº:{numeroProtocolo}].");

                return retorno;
            }

            var dadosInterrupcao = await _vwDadosClienteInterrupcaoRepository.ObterAsync(protocoloAtendimento.Matricula);

            if (dadosInterrupcao is null)
            {
                retorno.AdicionaMensagem($"Dados do cliente não encontrado [matricula:{protocoloAtendimento.Matricula}].");

                return retorno;
            }

            var interrupcao = await _interrupcaoRepository.ConsultarInterrupcaoAsync(dadosInterrupcao);

            if (interrupcao != null)
            {
                retorno = _mapper.Map<InterrupcaoAbastecimentoResponseViewModel>(interrupcao);
            }
            else
            {
                retorno.AdicionaMensagem("Não foi encontrado interrupção de abastecimento.");
            }

            retorno.Sucesso = true;
            return retorno;
        }
    }
}
