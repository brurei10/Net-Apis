﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Common.ViewModels.Token;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GAB.Inova.Business
{
    public class ContaDigitalBusiness
        : IContaDigitalBusiness
    {

        const int ID_NATUREZA = 39;// CONTA DIGITAL

        readonly IMapper _mapper;
        readonly ILogger _logger;
        readonly IConfiguration _configuration;
        readonly IEnvironmentVariables _environmentVariables;
        readonly IClienteRepository _clienteRepository;
        readonly IClienteTokenRepository _clienteTokenRepository;
        readonly IContaDigitalEnvioRepository _contaDigitalEnvioRepository;
        readonly IContaDigitalEnvioMovimentoRepository _contaDigitalEnvioMovimentoRepository;
        readonly IContaRepository _contaRepository;
        readonly IContratoMensagemIntegradorRepository _contratoMensagemIntegradorRepository;
        readonly IContratoRepository _contratoRepository;
        readonly IEmailIntegradorRepository _emailIntegradorRepository;
        readonly IGrupoEntregaRepository _grupoEntregaRepository;
        readonly IIntegradorConfigFactory _integradorConfigFactory;
        readonly IMensagemIntegradorRepository _mensagemIntegradorRepository;
        readonly IMensagemIntegradorMovimentoRepository _mensagemIntegradorMovimentoRepository;
        readonly IParametroSistemaRepository _parametroSistemaRepository;
        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;
        readonly IProtocoloAtendimentoDetalheRepository _protocoloAtendimentoDetalheRepository;
        readonly IProtocoloAtendimentoHistoricoRepository _protocoloAtendimentoHistoricoRepository;
        readonly ITokenProvider _tokenProvider;
        readonly IUnitOfWork _unitOfWork;

        private int _segundoExpiracaoToken;
        private string _urlBaseAgenciaVirtual;

        public ContaDigitalBusiness(IMapper mapper,
                                    IConfiguration configuration,
                                    IEnvironmentVariables environmentVariables,
                                    IClienteRepository clienteRepository,
                                    IClienteTokenRepository clienteTokenRepository,
                                    IContaDigitalEnvioRepository contaDigitalEnvioRepository,
                                    IContaDigitalEnvioMovimentoRepository contaDigitalEnvioMovimentoRepository,
                                    IContaRepository contaRepository,
                                    IContratoMensagemIntegradorRepository contratoMensagemIntegradorRepository,
                                    IContratoRepository contratoRepository,
                                    IEmailIntegradorRepository emailIntegradorRepository,
                                    IGrupoEntregaRepository grupoEntregaRepository,
                                    IIntegradorConfigFactory integradorConfigFactory,
                                    IMensagemIntegradorRepository mensagemIntegradorRepository,
                                    IMensagemIntegradorMovimentoRepository mensagemIntegradorMovimentoRepository,
                                    IParametroSistemaRepository parametroSistemaRepository,
                                    IProtocoloAtendimentoRepository protocoloAtendimentoRepository,
                                    IProtocoloAtendimentoDetalheRepository protocoloAtendimentoDetalheRepository,
                                    IProtocoloAtendimentoHistoricoRepository protocoloAtendimentoHistoricoRepository,
                                    ITokenProvider tokenProvider,
                                    IUnitOfWork unitOfWork,
                                    ILoggerFactory loggerFactory)
        {
            _mapper = mapper;
            _configuration = configuration;
            _environmentVariables = environmentVariables;
            _clienteRepository = clienteRepository;
            _clienteTokenRepository = clienteTokenRepository;
            _contaDigitalEnvioRepository = contaDigitalEnvioRepository;
            _contaDigitalEnvioMovimentoRepository = contaDigitalEnvioMovimentoRepository;
            _contaRepository = contaRepository;
            _contratoMensagemIntegradorRepository = contratoMensagemIntegradorRepository;
            _contratoRepository = contratoRepository;
            _emailIntegradorRepository = emailIntegradorRepository;
            _grupoEntregaRepository = grupoEntregaRepository;
            _integradorConfigFactory = integradorConfigFactory;
            _mensagemIntegradorRepository = mensagemIntegradorRepository;
            _mensagemIntegradorMovimentoRepository = mensagemIntegradorMovimentoRepository;
            _parametroSistemaRepository = parametroSistemaRepository;
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
            _protocoloAtendimentoDetalheRepository = protocoloAtendimentoDetalheRepository;
            _protocoloAtendimentoHistoricoRepository = protocoloAtendimentoHistoricoRepository;
            _tokenProvider = tokenProvider;
            _unitOfWork = unitOfWork;

            _logger = loggerFactory.CreateLogger<ContaDigitalBusiness>();

            _urlBaseAgenciaVirtual = _configuration.GetValue<string>("UrlAgenciaVirtualLandingSolicitacao");

            if (string.IsNullOrEmpty(_urlBaseAgenciaVirtual))
            {
                throw new ArgumentNullException("Não foi possível encontrar o paràmetro de configuração [UrlAgenciaVirtualLandingSolicitacao]");
            }
        }

        #region [Confirmar solicitação]

        public async Task<BaseResponseViewModel> ConfirmarSolicitacaoAsync(string token, string numeroProtocolo)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            try
            {
                #region [Obter e validar cliente token]

                var clienteToken = await _clienteTokenRepository.ObterPorTokenAsync(token);
                if (clienteToken is null) throw new InvalidOperationException($"Não foi possível encontrar o token informado [{token}].");

                clienteToken.ValidationResult = _clienteTokenRepository.PodeConfirmar(clienteToken);

                if (!clienteToken.ValidationResult.IsValid)
                {
                    result.AdicionaMensagens(clienteToken.ValidationResult.Errors.Select(e => e.Message));

                    return result;
                }

                #endregion

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(clienteToken.Matricula);
                if (contrato is null) throw new InvalidOperationException($"Matrícula não possui contrato ativo [nº:{clienteToken?.Matricula}].");

                contrato.EmailContaDigital = await _tokenProvider.ObterClaimAsync(token, "EmailCadastro");

                var historico = string.Empty;

                _unitOfWork.BeginTransaction();

                switch (clienteToken.Tipo)
                {
                    case TokenTipoEnum.ContaDigitalAdesao:
                        var cliente = await _clienteRepository.ObterPorMatriculaAsync(clienteToken.Matricula);

                        if (cliente?.IdGrupoEntrega == GrupoEntregaEnum.EMAIL)
                        {
                            _unitOfWork.Rollback();

                            result.Sucesso = true;
                            return result;
                        }

                        _logger.LogInformation($"Adesão ao conta digital para a matricula [nº:{clienteToken?.Matricula}]");

                        historico = "CONFIRMAÇÃO DE ADESÃO AO SERVIÇO DE CONTA DIGITAL";

                        if (!await _clienteRepository.AtualizaGrupoEntregaAsync(clienteToken.Matricula, (long)GrupoEntregaEnum.EMAIL))
                        {
                            throw new InvalidOperationException($"Não foi possível atualizar o grupo entrega para a matrícula [nº:{clienteToken?.Matricula}, grupo entrega:{GrupoEntregaEnum.EMAIL.GetDescription()}].");
                        }

                        _logger.LogInformation($"Grupo de entrega atualizado com sucesso para a matricula [nº:{clienteToken?.Matricula}, grupo entrega:{GrupoEntregaEnum.EMAIL.GetDescription()}]");

                        break;
                    case TokenTipoEnum.ContaDigitalAlteracao:
                        _logger.LogInformation($"Alteração de email(s) da conta digital para a matricula [nº:{clienteToken?.Matricula}]");

                        var modificaEmail = await _tokenProvider.ObterClaimAsync(token, "ModificaEmailContato");

                        if (bool.TryParse(modificaEmail, out bool modifica))
                        {
                            if (modifica)
                            {
                                contrato.Email = contrato.EmailContaDigital;

                                historico = "CONFIRMAÇÃO DE ALTERAÇÃO DE E-MAIL PRINCIPAL E PARA SERVIÇO DE CONTA DIGITAL";
                            }
                        }

                        if (!modifica) historico = "CONFIRMAÇÃO DE ALTERAÇÃO DE E-MAIL PARA SERVIÇO DE CONTA DIGITAL";

                        break;
                    default:
                        throw new InvalidOperationException($"Não é possível confirmar este tipo de solicitação [{clienteToken.Tipo.GetDescription()}].");
                }

                if (!await _contratoRepository.AtualizarEmailsAsync(contrato))
                {
                    throw new InvalidOperationException($"Não foi possível atualizar e-mail da conta digital para o contrato da matrícula [nº:{clienteToken?.Matricula}, id contrato:{contrato?.Id}].");
                }

                _logger.LogInformation($"E-mail(s) atualizado(s) com sucesso para o contrato [id:{contrato?.Id}]");

                #region [Inserir histórico para o protocolo]

                var protocolo = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);
                if (protocolo is null) throw new InvalidOperationException($"Protocolo de atendimento não localizado [nº:{numeroProtocolo}].");

                var protocoloAtendimentoHistorico = new ProtocoloAtendimentoHistorico()
                {
                    Historico = historico,
                    IdProtocoloAtendimento = protocolo.Id,
                    IdUsuario = protocolo.IdUsuario,
                    Matricula = contrato.Matricula,
                    IdLocalAtendimento = protocolo.IdLocalAtendimento,
                    IdTipoAtendimento = protocolo.IdTipoAtendimento,
                    TipoProtocolo = protocolo.TipoProtocolo
                };

                if (!await _protocoloAtendimentoHistoricoRepository.InserirAsync(protocoloAtendimentoHistorico))
                {
                    _logger.LogError($"Não foi possível inserir histórico para o protocolo de atendimento [id:{protocolo.Id}, histórico:{historico}]");
                }

                _logger.LogInformation($"Histórico inserido com sucesso para o protocolo de atendimento [id:{protocolo.Id}]");

                #endregion

                if (!await _clienteTokenRepository.ConfirmarAsync(clienteToken.Id))
                {
                    _logger.LogError($"Não foi possível confirmar token de uma solicitação de conta digital para a matrícula [nº:{clienteToken?.Matricula}, id:{clienteToken?.Id}].");
                }

                _unitOfWork.Commit();

                result.Sucesso = true;

                // Enviar e-mail de bem-vindo
                if (clienteToken.Tipo == TokenTipoEnum.ContaDigitalAdesao)
                {
                    await EnviarEmailBemVindoAsync(contrato);
                }

                return result;
            }
            catch
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        #endregion

        #region [Enviar conta digital]

        public async Task<BaseResponseViewModel> EnviarContaAsync(EnviarContaDigitalRequestViewModel model)
        {
            try
            {
                #region Obtenção e validação dos dados da conta digital

                var dto = await ObterDadosContaDigitalEnvio(model);
                var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();

                await InserirContaDigitalEnvio(dto, "Envio de conta digital solicitado ao integrador SendGrid");                

                #endregion

                #region Realizar o envio pelo integrador externo   ##ToDo: pegar do objeto body do response
                
                var sendGridResult = await _emailIntegradorRepository.EnviarEmailAsync(dto.EmailIntegradorContaDigitalRequestViewModel);

                await InserirMensagemIntegrador(dto, "Envio de conta digital solicitado ao integrador SendGrid");

                if (sendGridResult.Success)
                {
                    dto.MensagemIntegrador.GuidMensagem = sendGridResult.MessageGuid;
                    await AtualizarMensagemIntegrador(dto, "", false);

                    dto.ContaDigitalEnvio.IdMensagemIntegrador = dto.MensagemIntegrador.Id;
                    await AtualizarContaDigitalEnvio(dto, "", false);
                }
                else
                {
                    dto.MensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                    dto.ContaDigitalEnvio.Status = ContaDigitalEnvioStatusEnum.ErroNoEnvioDoEmail;

                    await AtualizarContaDigitalEnvio(dto, sendGridResult.StatusCode == HttpStatusCode.InternalServerError ? sendGridResult.Erros : "O Envio para o integrador externo foi Negado", false);
                    await AtualizarMensagemIntegrador(dto, sendGridResult.StatusCode == HttpStatusCode.InternalServerError ? sendGridResult.Erros : "O Envio para o integrador externo foi Negado", false);
                }

                #endregion

                return new BaseResponseViewModel();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<BaseResponseViewModel> EnviarContaClienteSemAdesaoAsync(EnviarContaDigitalRequestViewModel model)
        {
            try
            {
                #region Obtenção e validação dos dados da conta digital

                var dto = await ObterDadosContaDigitalClienteSemAdesaoEnvio(model);
                var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();

                await InserirContaDigitalEnvio(dto, "Envio de conta digital de cliente sem adesão solicitado ao integrador SendGrid");

                #endregion

                #region Realizar o envio pelo integrador externo   ##ToDo: pegar do objeto body do response

                var sendGridResult = await _emailIntegradorRepository.EnviarEmailAsync(dto.EmailIntegradorContaDigitalRequestViewModel);

                await InserirMensagemIntegrador(dto, "Envio de conta digital de cliente sem adesão solicitado ao integrador SendGrid");

                if (sendGridResult.Success)
                {
                    dto.MensagemIntegrador.GuidMensagem = sendGridResult.MessageGuid;
                    await AtualizarMensagemIntegrador(dto, "", false);

                    dto.ContaDigitalEnvio.IdMensagemIntegrador = dto.MensagemIntegrador.Id;
                    await AtualizarContaDigitalEnvio(dto, "", false);
                }
                else
                {
                    dto.MensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                    dto.ContaDigitalEnvio.Status = ContaDigitalEnvioStatusEnum.ErroNoEnvioDoEmail;

                    await AtualizarContaDigitalEnvio(dto, sendGridResult.StatusCode == HttpStatusCode.InternalServerError ? sendGridResult.Erros : "O Envio para o integrador externo foi Negado", false);
                    await AtualizarMensagemIntegrador(dto, sendGridResult.StatusCode == HttpStatusCode.InternalServerError ? sendGridResult.Erros : "O Envio para o integrador externo foi Negado", false);
                }

                #endregion

                return new BaseResponseViewModel();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region [Solitar Adesão]

        public async Task<BaseResponseViewModel> SolicitarAdesaoAsync(AderirContaDigitalRequestViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            try
            {
                #region [Obter e validar cliente]

                var cliente = await _clienteRepository.ObterPorMatriculaAsync(model.Matricula);
                if (cliente is null) throw new InvalidOperationException($"Não foi possível encontrar o cliente com a matrícula informada [nº:{model?.Matricula}].");

                cliente.ValidationResult = _clienteRepository.PodeAderirContaDigital(cliente);

                if (!cliente.ValidationResult.IsValid)
                {
                    result.AdicionaMensagens(cliente.ValidationResult.Errors.Select(e => e.Message));

                    return result;
                }

                #endregion

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(model.Matricula);
                if (contrato is null) throw new InvalidOperationException($"Matrícula não possui contrato ativo [nº:{model?.Matricula}].");

                var tokenResponse = await GerarTokenSolicitacaoContaDigitalAsync(model.Matricula, model.Email);
                if (tokenResponse is null || !tokenResponse.Sucesso) throw new InvalidOperationException("Não foi possível gerar um token para solicitação de adesão a Conta Digital.");

                _unitOfWork.BeginTransaction();

                var protocolo = await TratarProtocoloAsync(contrato, model.Protocolo, "SOLICITAÇÃO DE ADESÃO DA CONTA DIGITAL");

                var clienteToken = new ClienteToken
                {
                    DataExpiracao = DateTime.Now.AddSeconds(_segundoExpiracaoToken),
                    IdProtocoloAtendimento = protocolo.Id,
                    Matricula = cliente.Matricula,
                    Sistema = model.Sistema,
                    Tipo = TokenTipoEnum.ContaDigitalAdesao,
                    Token = tokenResponse.Token
                };

                if (!await _clienteTokenRepository.InserirAsync(clienteToken))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o token da solicitação de adesão da conta digital para a matricula [nº: {cliente?.Matricula}].");
                }

                var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();
                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalAdesao);
                var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);

                mensagemModel.Destinatarios = model.Email;
                mensagemModel.Conteudo = new EmailSolicitacaoContaDigitalRequestViewModel
                {
                    Matricula = contrato.Matricula,
                    Endereco = contrato.EnderecoCompleto,
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    Nome = contrato.NomeCliente,
                    UrlConfirmacao = $"{_urlBaseAgenciaVirtual}/?tokenconfirmacao={tokenResponse.Token}"
                };

                var mensagemIntegrador = _mapper.Map<MensagemIntegrador>(mensagemModel);

                if (!await _mensagemIntegradorRepository.InserirAsync(mensagemIntegrador))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para a matricula  [nº: {cliente?.Matricula}].");
                }

                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Envio de e-mail de solicitação de adesão dea conta digital pelo integrador [{integradorDesc}]" }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegradorMovimento para o MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
                }

                if (!await _contratoMensagemIntegradorRepository.InserirAsync(new ContratoMensagemIntegrador { IdContrato = contrato.Id, IdMensagemIntegrador = mensagemIntegrador.Id }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o ContratoMensagemIntegrador para o Contrato [nº: {contrato?.Id}].");
                }

                _unitOfWork.Commit();

                var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

                result.Sucesso = emailIntegradorResponse.Success;

                _unitOfWork.BeginTransaction();

                if (!result.Sucesso) //Erro no envio do e-mail - o integrador externo recusou o envio
                {
                    //adiciono mensagem de erro ao result do metodo
                    result.AdicionaMensagem($"Ocorreu um erro no envio de e-mail da solicitação de adesão da Conta Digital da matricula {cliente?.Matricula}");

                    //faço log do retorno do integrador externo
                    _logger.LogError($"Ocorreu um erro no envio de e-mail da solicitação de adesão da Conta Digital [Matricula:{cliente?.Matricula}, integrador:{integradorDesc}, Erro:{emailIntegradorResponse?.Erros}]");

                    //Atualizar o status do MensagemIntegrador colocando o status de erro (será o ultimo status)
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        throw new InvalidOperationException($"Não foi possível modificar status do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }

                    //Adiciono um MensagemIntegradorMovimento registrando o erro de envio
                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Falha no envio de e-mail da solicitação de adesão da conta digital pelo integrador [{integradorDesc}]" }))
                    {
                        throw new InvalidOperationException($"Não foi possível inserir Movimento de erro para o MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }
                else
                {
                    //Em caso de sucesso atualizo o guid do MensagemIntegrador com o MessageGuid retornado pelo integrador externo 
                    mensagemIntegrador.GuidMensagem = emailIntegradorResponse.MessageGuid;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        //A falha nesse momento não inviabiliza o processo
                        _logger.LogError($"Não foi possível modificar guid do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }

                _unitOfWork.Commit();

                return result;
            }
            catch
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        #endregion

        #region [Solicitar Alteração]

        public async Task<BaseResponseViewModel> SolicitarAlteracaoAsync(AlterarContaDigitalRequestViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            try
            {
                #region [Obter e validar cliente]

                var cliente = await _clienteRepository.ObterPorMatriculaAsync(model.Matricula);
                if (cliente is null) throw new InvalidOperationException($"Não foi possível encontrar o cliente com a matrícula informada [nº:{model?.Matricula}].");

                cliente.ValidationResult = _clienteRepository.PodeAlterarContaDigital(cliente);

                if (!cliente.ValidationResult.IsValid)
                {
                    result.AdicionaMensagens(cliente.ValidationResult.Errors.Select(e => e.Message));

                    return result;
                }

                #endregion

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(cliente.Matricula);
                if (contrato is null) throw new InvalidOperationException($"Matrícula não possui contrato ativo [nº:{model?.Matricula}].");

                var tokenResponse = await GerarTokenSolicitacaoContaDigitalAsync(cliente.Matricula, model.Email, model.ModificaEmailContato);
                if (tokenResponse is null || !tokenResponse.Sucesso) throw new InvalidOperationException("Não foi possível gerar um token para solicitação de alteração da Conta Digital.");

                _unitOfWork.BeginTransaction();

                string historico;

                if (model.ModificaEmailContato)
                {
                    historico = $"SOLICITAÇÃO DE ALTERAÇÃO DE E-MAIL DA CONTA DIGITAL E E-MAIL PRINCIPAL [CONTA DIGITAL:{contrato.EmailContaDigital}, PRINCIPAL:{contrato.Email}, PARA:{model.Email}]";
                }
                else
                {
                    historico = $"SOLICITAÇÃO DE ALTERAÇÃO DE E-MAIL DA CONTA DIGITAL [DE:{contrato.EmailContaDigital}, PARA:{model.Email}]";
                }

                var protocolo = await TratarProtocoloAsync(contrato, model.Protocolo, historico);

                var clienteToken = new ClienteToken
                {
                    DataExpiracao = DateTime.Now.AddSeconds(_segundoExpiracaoToken),
                    IdProtocoloAtendimento = protocolo.Id,
                    Matricula = cliente.Matricula,
                    Sistema = model.Sistema,
                    Tipo = TokenTipoEnum.ContaDigitalAlteracao,
                    Token = tokenResponse.Token
                };

                if (!await _clienteTokenRepository.InserirAsync(clienteToken))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o token da solicitação de alteração de e-mail da conta digital para a matricula [nº: {cliente?.Matricula}].");
                }

                var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();
                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalAlteracao);
                var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);

                mensagemModel.Destinatarios = model.Email;
                mensagemModel.Conteudo = new EmailSolicitacaoContaDigitalRequestViewModel
                {
                    Matricula = contrato.Matricula,
                    Endereco = contrato.EnderecoCompleto,
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    Nome = contrato.NomeCliente,
                    UrlConfirmacao = $"{_urlBaseAgenciaVirtual}/?tokenconfirmacao={tokenResponse.Token}"
                };

                var mensagemIntegrador = _mapper.Map<MensagemIntegrador>(mensagemModel);

                if (!await _mensagemIntegradorRepository.InserirAsync(mensagemIntegrador))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para a matricula  [nº: {cliente?.Matricula}].");
                }

                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Envio de e-mail de solicitação de alteração de e-mail da conta digital pelo integrador [{integradorDesc}]" }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegradorMovimento para o MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
                }

                if (!await _contratoMensagemIntegradorRepository.InserirAsync(new ContratoMensagemIntegrador { IdContrato = contrato.Id, IdMensagemIntegrador = mensagemIntegrador.Id }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o ContratoMensagemIntegrador para o Contrato [nº: {contrato?.Id}].");
                }

                _unitOfWork.Commit();

                var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

                result.Sucesso = emailIntegradorResponse.Success;

                _unitOfWork.BeginTransaction();

                if (!result.Sucesso)
                {
                    //adiciono mensagem de erro ao result do metodo
                    result.AdicionaMensagem("$Ocorreu um erro no envio de e - mail da solicitação de alteração da Conta Digital[Matricula:{ cliente?.Matricula}].");

                    //faço log do retorno do integrador externo
                    _logger.LogError($"Ocorreu um erro no envio de e-mail da solicitação de alteração da Conta Digital [Matricula:{cliente?.Matricula}, integrador:{integradorDesc}, Erro:{emailIntegradorResponse?.Erros}]");

                    //Atualizar o status do MensagemIntegrador colocando o status de erro (será o ultimo status)
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        throw new InvalidOperationException($"Não foi possível modificar status do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }

                    //Adiciono um MensagemIntegradorMovimento registrando o erro de envio
                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Falha no envio de e-mail da solicitação de alteração da conta digital pelo {integradorDesc}" }))
                    {
                        throw new InvalidOperationException($"Não foi possível inserir Movimento de erro para o MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }
                else
                {
                    //Em caso de sucesso atualizo o guid do MensagemIntegrador com o MessageGuid retornado pelo integrador externo 
                    mensagemIntegrador.GuidMensagem = emailIntegradorResponse.MessageGuid;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        //A falha nesse momento não inviabiliza o processo
                        _logger.LogError($"Não foi possível modificar guid do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }

                _unitOfWork.Commit();

                return result;
            }
            catch
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        #endregion

        #region [Solicitar Cancelamento]

        public async Task<BaseResponseViewModel> SolicitarCancelamentoAsync(CancelarContaDigitalRequestViewModel model)
        {
            var result = new BaseResponseViewModel()
            {
                Sucesso = false
            };

            try
            {
                #region [Obter e validar entidades]

                var cliente = await _clienteRepository.ObterPorMatriculaAsync(model.Matricula);
                if (cliente is null) throw new InvalidOperationException($"Não foi possível encontrar o cliente com a matrícula informada [nº:{model?.Matricula}].");

                cliente.ValidationResult = _clienteRepository.PodeCancelarContaDigital(cliente);

                if (!cliente.ValidationResult.IsValid)
                {
                    result.AdicionaMensagens(cliente.ValidationResult.Errors.Select(e => e.Message));
                    return result;
                }

                #endregion

                var contrato = await _contratoRepository.ObterPorMatriculaAsync(cliente.Matricula);
                if (contrato is null) throw new InvalidOperationException($"Matrícula não possui contrato ativo [nº:{model?.Matricula}].");

                var tokenResponse = await GerarTokenSolicitacaoContaDigitalAsync(cliente.Matricula, model.Email);
                if (tokenResponse is null || !tokenResponse.Sucesso) throw new InvalidOperationException("Não foi possível gerar um token para solicitação de cancelamento da Conta Digital.");

                _unitOfWork.BeginTransaction();

                var protocolo = await TratarProtocoloAsync(contrato, model.Protocolo, "SOLICITAÇÃO DE CANCELAMENTO DA CONTA DIGITAL");

                var clienteToken = new ClienteToken
                {
                    DataExpiracao = DateTime.Now.AddSeconds(_segundoExpiracaoToken),
                    IdProtocoloAtendimento = protocolo.Id,
                    Matricula = cliente.Matricula,
                    Sistema = model.Sistema,
                    Tipo = TokenTipoEnum.ContaDigitalExclusao,
                    Token = tokenResponse.Token
                };

                if (!await _clienteTokenRepository.InserirAsync(clienteToken))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o token da solicitação de cancelamento da conta digital para a matricula [nº: {cliente?.Matricula}].");
                }

                var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();
                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalExclusao);

                var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);
                mensagemModel.Destinatarios = contrato.EmailContaDigital;
                mensagemModel.Conteudo = new EmailSolicitacaoContaDigitalRequestViewModel
                {
                    Matricula = contrato.Matricula,
                    Endereco = contrato.EnderecoCompleto,
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    Nome = contrato.NomeCliente
                };

                var mensagemIntegrador = _mapper.Map<MensagemIntegrador>(mensagemModel);

                if (!await _mensagemIntegradorRepository.InserirAsync(mensagemIntegrador))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para o envio de cancelamento de adesão para a matricula  [nº: {cliente?.Matricula}].");
                }

                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Envio de e-mail de solicitação de cancelamento da conta digital pelo integrador [{integradorDesc}]" }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegradorMovimento para o MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
                }

                if (!await _contratoMensagemIntegradorRepository.InserirAsync(new ContratoMensagemIntegrador { IdContrato = contrato.Id, IdMensagemIntegrador = mensagemIntegrador.Id }))
                {
                    throw new InvalidOperationException($"Não foi possível inserir o ContratoMensagemIntegrador para o Contrato [nº: {contrato?.Id}] e MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
                }

                contrato.EmailContaDigital = null;
                cliente.IdGrupoEntrega = GrupoEntregaEnum.DOMICILIO;

                if (!await _contratoRepository.AtualizarEmailsAsync(contrato))
                {
                    throw new InvalidOperationException($"Não foi possível remover o e-mail de conta digital do Contrato [nº: {contrato?.Id}].");
                }

                if (!await _clienteRepository.AtualizaGrupoEntregaAsync(cliente.Matricula, (long)cliente.IdGrupoEntrega))
                {
                    throw new InvalidOperationException($"Não foi possível remover o e-mail de conta digital do Contrato [nº: {contrato?.Id}].");
                }

                if (!await _clienteTokenRepository.ConfirmarAsync(clienteToken.Id))
                {
                    _logger.LogError($"Não foi possível confirmar token de uma solicitação de conta digital para a matrícula [nº:{clienteToken?.Matricula}, id:{clienteToken?.Id}].");
                }

                _unitOfWork.Commit();

                result.Sucesso = true;

                var emailIntegradorResponse = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

                if (!emailIntegradorResponse.Success)
                {
                    //faço log do retorno do integrador externo
                    _logger.LogError($"Ocorreu um erro no envio de e-mail da solicitação de cancelamento da Conta Digital [Matricula:{cliente?.Matricula}, integrador:{integradorDesc}, Erro:{emailIntegradorResponse?.Erros}]");

                    //Atualizar o status do MensagemIntegrador colocando o status de erro (será o ultimo status)
                    mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Não foi possível modificar status do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }

                    //Adiciono um MensagemIntegradorMovimento registrando o erro de envio
                    if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Falha no envio de e-mail da solicitação de cancelamento da conta digital pelo integrador [{integradorDesc}]" }))
                    {
                        _logger.LogError($"Não foi possível inserir Movimento de erro para o MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }
                else
                {
                    mensagemIntegrador.GuidMensagem = emailIntegradorResponse.MessageGuid;
                    if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                    {
                        _logger.LogError($"Não foi possível modificar guid do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                    }
                }

                return result;
            }
            catch
            {
                _unitOfWork.Rollback();

                throw;
            }
        }

        #endregion

        #region [Métodos privados]

        #region [Específicos do Envio de conta digital]

        private async Task InserirMensagemIntegrador(DadosContaDigital dados, string mensagemDetalhe)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                if (await _mensagemIntegradorRepository.InserirAsync(dados.MensagemIntegrador) && 
                    await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(dados.MensagemIntegrador, mensagemDetalhe)))
                {
                    _unitOfWork.Commit();
                    return;
                }

                throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para o envio de conta digital [SeqOriginal:{dados.Conta?.Id}/Matricula:{dados.Cliente?.Matricula}].");

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError(ex.Message);
                throw;
            }
        }

        private async Task AtualizarMensagemIntegrador(DadosContaDigital dados, string mensagemDetalhe, bool throwErro = true)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                if (await _mensagemIntegradorRepository.AtualizarAsync(dados.MensagemIntegrador) &&
                    (string.IsNullOrEmpty(mensagemDetalhe) ||
                     await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(dados.MensagemIntegrador, mensagemDetalhe))))
                {
                    _unitOfWork.Commit();
                    return;
                }

                throw new InvalidOperationException($"Não foi possível atualizar o MensagemIntegrador [Id:{dados.MensagemIntegrador?.Id}] para o envio de conta digital [SeqOriginal:{dados.Conta?.Id}/Matricula:{dados.Cliente?.Matricula}].");

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError(ex.Message);
                if (throwErro)
                    throw;
            }
        }

        private async Task InserirContaDigitalEnvio(DadosContaDigital dados, string mensagemDetalhe)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                if (await _contaDigitalEnvioRepository.InserirAsync(dados.ContaDigitalEnvio) && 
                    await _contaDigitalEnvioMovimentoRepository.InserirAsync(new ContaDigitalEnvioMovimento(dados.ContaDigitalEnvio, mensagemDetalhe)))
                {
                    _unitOfWork.Commit();
                    return;
                }

                throw new InvalidOperationException($"Não foi possível inserir o ContaDigitalEnvio para o envio de conta digital [SeqOriginal:{dados.Conta?.Id}/Matricula:{dados.Cliente?.Matricula}].");
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError(ex.Message);
                throw;
            }
        }

        private async Task AtualizarContaDigitalEnvio(DadosContaDigital dados, string mensagemDetalhe, bool throwErro = true)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                if (await _contaDigitalEnvioRepository.AtualizarAsync(dados.ContaDigitalEnvio) && 
                    (string.IsNullOrEmpty(mensagemDetalhe) ||
                     await _contaDigitalEnvioMovimentoRepository.InserirAsync(new ContaDigitalEnvioMovimento(dados.ContaDigitalEnvio, mensagemDetalhe))))
                {
                    _unitOfWork.Commit();
                    return;
                }

                throw new InvalidOperationException($"Não foi possível Atualizar o ContaDigitalEnvio [Id:{dados.ContaDigitalEnvio?.Id}] para o envio de conta digital [SeqOriginal:{dados.Conta?.Id}/Matricula:{dados.Cliente?.Matricula}].");
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError(ex.Message);
                if (throwErro)
                    throw;
            }
        }

        private async Task<DadosContaDigital> ObterDadosContaDigitalEnvio(EnviarContaDigitalRequestViewModel model)
        {
            var dados = new DadosContaDigital();
            try
            {
                #region Obter as entidades e validar as entidades

                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalConta);
                dados.Cliente = await _clienteRepository.ObterPorMatriculaAsync(model.Matricula) ?? throw new InvalidOperationException($"Cliente de matricula '{model?.Matricula}' não foi localizado!");
                dados.Conta = await _contaRepository.ObterContaPorSeqOriginalAsync(model.SeqOriginal) ?? throw new InvalidOperationException($"Conta de SeqOriginal '{model?.SeqOriginal}' não foi localizada!");
                dados.Contrato = await _contratoRepository.ObterPorMatriculaAsync(dados.Cliente.Matricula) ?? throw new InvalidOperationException($"Contrato de matricula '{dados.Cliente?.Matricula}' não foi localizado!");
                dados.ContaDigitalEnvio = new ContaDigitalEnvio(dados.Conta);

                if (string.IsNullOrEmpty(model.ContaBase64))
                {
                    throw new InvalidOperationException($"O arquivo da conta digital está vazio");
                }

                #endregion

                #region [Validar e-mail destinatario]

                if (string.IsNullOrEmpty(dados.Contrato?.EmailContaDigital))
                {
                    throw new InvalidOperationException($"O e-mail destinatário está nulo ou vazio.");
                }

                if (!dados.Contrato.EmailContaDigital.IsValidEmail())
                {
                    throw new InvalidOperationException($"O e-mail destinatário é inválido ({dados.Contrato.EmailContaDigital})");
                }

                var destinatarios = sendGridConfig.TratarERetornarListaDestinatariosConcatenados(dados.Contrato.EmailContaDigital);

                if (string.IsNullOrEmpty(destinatarios))
                {
                    throw new InvalidOperationException($"O serviço sem autorização de enviar um e-mail para o destinatário: {dados.Contrato.EmailContaDigital}");
                }

                #endregion

                #region Criar o template e viewmodel de envio de email pelo integrador

                var template = new EmailEnvioContaDigitalRequestViewModel
                {
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    DataVencimento = dados.Conta.Vencimento.ToShortDateString(),
                    Endereco = dados.Contrato.EnderecoCompleto,
                    MesAnoReferencia = $"{dados.Conta.AnoMes.Substring(4, 2)}/{dados.Conta.AnoMes.Substring(0, 4)}",
                    ValorTotal = dados.Conta.ValorTotal.ToString("C"),
                    NomeCliente = dados.Cliente.Nome
                };

                dados.EmailIntegradorContaDigitalRequestViewModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);
                dados.EmailIntegradorContaDigitalRequestViewModel.Conteudo = template;
                dados.EmailIntegradorContaDigitalRequestViewModel.Destinatarios = destinatarios;
                dados.EmailIntegradorContaDigitalRequestViewModel.CustomArgAdiciona(new KeyValuePair<string, string>("tipoEmail", "ContaDigital"));
                dados.EmailIntegradorContaDigitalRequestViewModel.AnexoAdiciona(new AnexoEmailViewModel
                {
                    ArquivoBase64 = model.ContaBase64,
                    NomeArquivo = "ContaDigital.pdf",
                    Tipo = "application/pdf"
                });

                dados.MensagemIntegrador = _mapper.Map<MensagemIntegrador>(dados.EmailIntegradorContaDigitalRequestViewModel);
                
                #endregion

                return dados;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Não foi possível continuar com o envio da Conta [SeqOriginal:{model.SeqOriginal}/Matricula:{model.Matricula}] - Ocorreu erro ao obter as entidades [ex:{ex.Message}] e falhou o registro do EnvioContaDigital");
                dados.ContaDigitalEnvio = new ContaDigitalEnvio { SeqOriginal = model.SeqOriginal, Status = ContaDigitalEnvioStatusEnum.ErroNoEnvioDoEmail };
                await InserirContaDigitalEnvio(dados, ex.Message);
                throw;
            }
        }

        private async Task<DadosContaDigital> ObterDadosContaDigitalClienteSemAdesaoEnvio(EnviarContaDigitalRequestViewModel model)
        {
            var dados = new DadosContaDigital();
            try
            {
                #region Obter as entidades e validar as entidades

                var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalContaClienteSemAdesao);
                dados.Cliente = await _clienteRepository.ObterPorMatriculaAsync(model.Matricula) ?? throw new InvalidOperationException($"Cliente de matricula '{model?.Matricula}' não foi localizado!");
                dados.Conta = await _contaRepository.ObterContaPorSeqOriginalAsync(model.SeqOriginal) ?? throw new InvalidOperationException($"Conta de SeqOriginal '{model?.SeqOriginal}' não foi localizada!");
                dados.Contrato = await _contratoRepository.ObterPorMatriculaAsync(dados.Cliente.Matricula) ?? throw new InvalidOperationException($"Contrato de matricula '{dados.Cliente?.Matricula}' não foi localizado!");
                dados.ContaDigitalEnvio = new ContaDigitalEnvio(dados.Conta);

                if (string.IsNullOrEmpty(model.ContaBase64))
                {
                    throw new InvalidOperationException("O arquivo da conta digital está vazio");
                }

                #endregion

                #region [Validar e-mail destinatario]

                if (string.IsNullOrEmpty(dados.Contrato?.Email))
                {
                    throw new InvalidOperationException("O e-mail destinatário está nulo ou vazio.");
                }

                if (!dados.Contrato.Email.IsValidEmail())
                {
                    throw new InvalidOperationException($"O e-mail destinatário é inválido: {dados.Contrato.Email}");
                }

                var destinatarios = sendGridConfig.TratarERetornarListaDestinatariosConcatenados(dados.Contrato.Email);

                if (string.IsNullOrEmpty(destinatarios))
                {
                    throw new InvalidOperationException($"O serviço sem autorização de enviar um e-mail para o destinatário: {dados.Contrato.Email}");
                }

                #endregion

                #region Criar o template e viewmodel de envio de email pelo integrador

                var template = new EmailEnvioContaDigitalClienteSemAdesaoRequestViewModel
                {
                    EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                    Endereco = dados.Contrato.EnderecoCompleto,
                    Matricula = dados.Cliente.Matricula,
                    NomeCliente = dados.Cliente.Nome
                };

                dados.EmailIntegradorContaDigitalRequestViewModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);
                dados.EmailIntegradorContaDigitalRequestViewModel.Conteudo = template;
                dados.EmailIntegradorContaDigitalRequestViewModel.Destinatarios = destinatarios;
                dados.EmailIntegradorContaDigitalRequestViewModel.CustomArgAdiciona(new KeyValuePair<string, string>("tipoEmail", "ContaDigital"));
                dados.EmailIntegradorContaDigitalRequestViewModel.AnexoAdiciona(new AnexoEmailViewModel
                {
                    ArquivoBase64 = model.ContaBase64,
                    NomeArquivo = "ContaDigital.pdf",
                    Tipo = "application/pdf"
                });

                dados.MensagemIntegrador = _mapper.Map<MensagemIntegrador>(dados.EmailIntegradorContaDigitalRequestViewModel);

                #endregion

                return dados;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Não foi possível continuar com o envio da conta para cliente sem adesão [SeqOriginal:{model.SeqOriginal}/Matricula:{model.Matricula}] - Ocorreu erro ao obter as entidades [ex:{ex.Message}] e falhou o registro do EnvioContaDigital");
                dados.ContaDigitalEnvio = new ContaDigitalEnvio { SeqOriginal = model.SeqOriginal, Status = ContaDigitalEnvioStatusEnum.ErroNoEnvioDoEmail };
                await InserirContaDigitalEnvio(dados, ex.Message);
                throw;
            }
        }

        #endregion

        private async Task<TokenResponseViewModel> GerarTokenSolicitacaoContaDigitalAsync(string matricula, string email, bool modificaEmailContrato = false)
        {
            var parametroSistema = await _parametroSistemaRepository.ObterAsync((long)TabParametroEnum.TEMPO_EXPIRACAO_TOKEN_CONTA_DIGITAL);
            if (parametroSistema is null) throw new InvalidOperationException($"Não foi possível encontrar o parâmetro tempo de expiração do token de solicitação de Conta Digital [id:{(long)TabParametroEnum.TEMPO_EXPIRACAO_TOKEN_CONTA_DIGITAL}].");

            _segundoExpiracaoToken = (int)(parametroSistema.Valor * 60);

            var codigoEmpresa = await _tokenProvider.ObterEmpresaContextoAtualAsync();

            var claims = new List<KeyValuePair<string, string>>();

            claims.Add(new KeyValuePair<string, string>("CodigoEmpresa", codigoEmpresa.ToString()));
            claims.Add(new KeyValuePair<string, string>("Matricula", matricula));
            claims.Add(new KeyValuePair<string, string>("EmailCadastro", email));
            claims.Add(new KeyValuePair<string, string>("ModificaEmailContato", modificaEmailContrato.ToString()));

            return await _tokenProvider.GerarTokenModelAsync(claims, _segundoExpiracaoToken);
        }

        private async Task<ProtocoloAtendimento> TratarProtocoloAsync(Contrato contrato, string numeroProtocolo, string historico)
        {
            var result = false;
            var protocolo = await _protocoloAtendimentoRepository.ObterPorNumeroAsync(numeroProtocolo);

            if (protocolo is null) throw new InvalidOperationException($"Protocolo de atendimento não localizado [nº:{numeroProtocolo}].");


            if (!await _protocoloAtendimentoDetalheRepository.VerificarExistenciaDeNaturezaAsync(protocolo.Id, ID_NATUREZA))
            {
                result = await _protocoloAtendimentoDetalheRepository.InserirAsync(new ProtocoloAtendimentoDetalhe()
                {
                    IdProtocoloAtendimento = protocolo.Id,
                    IdNatureza = ID_NATUREZA,
                    IdRotina = _environmentVariables.IdRotina,
                    TipoProtocolo = protocolo.TipoProtocolo
                });

                if (!result)
                {
                    throw new InvalidOperationException($"Não foi possível inserir natureza para o protocolo de atendimento [id:{protocolo.Id}, id natureza:{ID_NATUREZA}]");
                }

                _logger.LogInformation($"Natureza inserida com sucesso para o protocolo de atendimento [id:{protocolo.Id}]");
            }

            result = await _protocoloAtendimentoHistoricoRepository.InserirAsync(new ProtocoloAtendimentoHistorico()
            {
                Historico = historico,
                IdProtocoloAtendimento = protocolo.Id,
                IdUsuario = protocolo.IdUsuario,
                Matricula = contrato.Matricula,
                IdLocalAtendimento = protocolo.IdLocalAtendimento,
                IdTipoAtendimento = protocolo.IdTipoAtendimento,
                TipoProtocolo = protocolo.TipoProtocolo
            });

            if (!result)
            {
                throw new InvalidOperationException($"Não foi possível inserir histórico para o protocolo de atendimento [id:{protocolo.Id}, histórico:{historico}]");
            }

            _logger.LogInformation($"Histórico inserido com sucesso para o protocolo de atendimento [id:{protocolo.Id}]");

            protocolo.Matricula = contrato.Matricula;
            protocolo.CpfCnpj = contrato.CpfCnpj;
            protocolo.IdContrato = contrato.Id;

            if (!await _protocoloAtendimentoRepository.AtualizarDadosClienteAsync(protocolo))
            {
                throw new InvalidOperationException($"Não foi possível alterar dados do cliente para o protocolo de atendimento [id:{protocolo.Id}, matricula:{contrato.Matricula}, cpfCnpj:{contrato.CpfCnpj}, id contrato:{contrato.Id}]");
            }

            _logger.LogInformation($"Dados do cliente alterado com sucesso para o protocolo de atendimento [id:{protocolo.Id}]");

            return protocolo;
        }

        private async Task EnviarEmailBemVindoAsync(Contrato contrato)
        {
            var dataProximaFatura = await _clienteRepository.ObterDataProximaLeituraAsync(contrato.Matricula);
            var sendGridConfig = _integradorConfigFactory.Construir(MensagemIntegradorTipoEnum.ContaDigitalBemVindo);
            var integradorDesc = IntegradorTipoEnum.SendGrid.GetDescription();

            var emailTemplate = new EmailBemVindoContaDigitalRequestViewModel()
            {
                Email = contrato.EmailContaDigital,
                EmpresaTipo = sendGridConfig.CodigoEmpresaEnum,
                DataProximaLeitura = dataProximaFatura.ToString("dd/MM/yyyy")
            };

            var mensagemModel = _mapper.Map<EmailRequestViewModel>(sendGridConfig);
            mensagemModel.Conteudo = emailTemplate;
            mensagemModel.Destinatarios = contrato.EmailContaDigital;

            var mensagemIntegrador = _mapper.Map<MensagemIntegrador>(mensagemModel);

            _unitOfWork.BeginTransaction();

            if (!await _mensagemIntegradorRepository.InserirAsync(mensagemIntegrador))
            {
                throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegrador para a matricula  [nº: {contrato?.Matricula}].");
            }

            if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Envio de e-mail de boas vindas de conta digital pelo integrador [{integradorDesc}]" }))
            {
                throw new InvalidOperationException($"Não foi possível inserir o MensagemIntegradorMovimento para o MensagemIntegrador [nº: {mensagemIntegrador?.Id}].");
            }

            if (!await _contratoMensagemIntegradorRepository.InserirAsync(new ContratoMensagemIntegrador { IdContrato = contrato.Id, IdMensagemIntegrador = mensagemIntegrador.Id }))
            {
                throw new InvalidOperationException($"Não foi possível inserir o ContratoMensagemIntegrador para o Contrato [nº: {contrato?.Id}].");
            }

            _unitOfWork.Commit();

            var integradorResult = await _emailIntegradorRepository.EnviarEmailAsync(mensagemModel);

            if (!integradorResult.Success) //Erro no envio do e-mail - o integrador externo recusou o envio
            {

                //faço log do retorno do integrador externo
                _logger.LogError($"Ocorreu um erro no envio de e-mail da solicitação de adesão da Conta Digital [Matricula:{contrato?.Matricula}, integrador:{integradorDesc}, Erro:{integradorResult?.Erros}]");

                //Atualizar o status do MensagemIntegrador colocando o status de erro (será o ultimo status)
                mensagemIntegrador.Status = MensagemIntegradoStatusEnum.Falha;
                if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                {
                    _logger.LogError($"Não foi possível modificar status do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                }

                //Adiciono um MensagemIntegradorMovimento registrando o erro de envio
                if (!await _mensagemIntegradorMovimentoRepository.InserirAsync(new MensagemIntegradorMovimento(mensagemIntegrador) { Mensagem = $"Falha no envio de e-mail da solicitação de adesão da conta digital pelo integrador [{integradorDesc}]" }))
                {
                    _logger.LogError($"Não foi possível inserir Movimento de erro para o MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                }
            }
            else
            {
                //Em caso de sucesso atualizo o guid do MensagemIntegrador com o MessageGuid retornado pelo integrador externo 
                mensagemIntegrador.GuidMensagem = integradorResult.MessageGuid;
                if (!await _mensagemIntegradorRepository.AtualizarAsync(mensagemIntegrador))
                {
                    //A falha nesse momento não inviabiliza o processo
                    _logger.LogError($"Não foi possível modificar guid do MensagemIntegrador [id: {mensagemIntegrador?.Id}].");
                }
            }
        }

        #endregion

        #region Models

        /// <summary>
        /// Classe POCO - para encapsular os muitos parametros de troca entre os métodos do envio de email
        /// </summary>
        class DadosContaDigital
        {
            public Conta Conta { get; set; }
            public Cliente Cliente { get; set; }
            public Contrato Contrato { get; set; }
            public MensagemIntegrador MensagemIntegrador { get; set; }
            public ContaDigitalEnvio ContaDigitalEnvio { get; set; }
            public EmailRequestViewModel EmailIntegradorContaDigitalRequestViewModel { get; set; }
        }

        #endregion
    }
}
