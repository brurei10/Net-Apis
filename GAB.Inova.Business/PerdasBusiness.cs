﻿using AutoMapper;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels;
using GAB.Inova.Common.ViewModels.Perdas;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GAB.Inova.Business
{
    public class PerdasBusiness : IPerdasBusiness
    {
        private readonly IMapper _mapper;
        private readonly IPerdasRepository _perdasRepository;


        public PerdasBusiness(IMapper mapper, IPerdasRepository perdasRepository)
        {
            _mapper = mapper;
            _perdasRepository = perdasRepository;


        }
        public async Task<ConsultarHidrometrosViewModel> ConsultarHidrometrosViewModel(DateTime dataInicio, DateTime DataFim)
        {
            var retorno = new ConsultarHidrometrosViewModel();

            var hidrometros = await _perdasRepository.ConsultarHidrometros(dataInicio, DataFim);

            if (hidrometros != null)
            {
                retorno.MI_STRUCT_Meter_List = _mapper.Map<IList<MI_STRUCT_Meter_List>>(hidrometros);

                retorno.Sucesso = true;
            }

            return retorno;
        }

        public async Task<ConsultarLeiturasViewModel> ConsultarLeiturasViewModel(DateTime dataInicio, DateTime DataFim)
        {
            var retorno = new ConsultarLeiturasViewModel();

            var leituras = await _perdasRepository.ConsultarLeituras(dataInicio, DataFim);

            if (leituras != null)
            {
                retorno.MI_STRUCT_Reading_List = _mapper.Map<IList<MI_STRUCT_Reading_List>>(leituras);

                retorno.Sucesso = true;
            }

            return retorno;
        }

        public async Task<ConsultarLeiturasViewModel> ConsultarLeiturasMacroViewModel()
        {

            var retorno = new ConsultarLeiturasViewModel();

            var leituras = await _perdasRepository.ConsultarLeiturasMacro();

            if (leituras != null)
            {
                retorno.MI_STRUCT_Reading_List = _mapper.Map<IList<MI_STRUCT_Reading_List>>(leituras);

                retorno.Sucesso = true;
            }

            return retorno;
        }

        public void InserirLog(StatusCodeResult resultado)
        {
            _perdasRepository.InserirLog(resultado);
        }
    }
}
