﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class CompoeCt
    {
        public int DocumentoPago { get; set; }
        public int DocumentoOriginal { get; set; }
        public double Valor { get; set; }
        public int TipoContaDocumentoPago { get; set; }
    }
}
