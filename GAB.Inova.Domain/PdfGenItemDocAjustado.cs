﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Domain
{
    public class PdfGenItemDocAjustado
    {

        public long IdImpressora { get; set; }
        public long IdModelo { get; set; }
        public long IdItemDocumento { get; set; }
        public decimal EstiloMargemEsquerda { get; set; }
        public decimal EstiloMargemTopo { get; set; }

    }
}
