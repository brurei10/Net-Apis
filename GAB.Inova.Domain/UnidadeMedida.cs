﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class UnidadeMedida
        : EntityBase<long>
    {
  
        public virtual string Descricao { get; set; }
        public virtual string Sigla { get; set; }

    }
}
