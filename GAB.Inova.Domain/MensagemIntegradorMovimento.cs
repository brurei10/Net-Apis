﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class MensagemIntegradorMovimento
        : EntityBase<long>
    {

        public MensagemIntegradorMovimento()
        {
            DataInclusao = DateTime.Now;
            Status = MensagemIntegradoStatusEnum.Criado;
        }

        public MensagemIntegradorMovimento(MensagemIntegrador mensagemIntegrador)
            : this()
        {
            Status = mensagemIntegrador.Status;
            IdMensagemIntegrador = mensagemIntegrador.Id;
        }

        public MensagemIntegradorMovimento(MensagemIntegrador mensagemIntegrador, string mensagem)
            : this(mensagemIntegrador)
        {
            Mensagem = mensagem;
        }

        public virtual long IdMensagemIntegrador { get; set; }        
        public virtual DateTime DataInclusao { get; set; }
        public virtual MensagemIntegradoStatusEnum Status { get; set; }
        public virtual string Mensagem { get; set; }
    }
}
