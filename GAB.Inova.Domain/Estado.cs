﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Estado
        : EntityBase<long>
    {
        public virtual string Nome { get; set; }
    }
}
