﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class TabNaturezaSolicitacao
        : EntityBase<long>
    {

        public virtual string Descricao { get; set; }

    }
}
