﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class TempHistoricoConsumo
        : EntityBase<long>
    {

        public string Referencia { get; set; }
        public long? Consumo { get; set; }
        public long? QtdeDias { get; set; }
        public string AnoMes { get; set; }
        public string Matricula { get; set; }
        public long? IdUsuario { get; set; }
        
    }
}
