﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class TabAnaliseAgua
        : EntityBase<long>
    {

        public string AnoMes { get; set; }
        public int? Tipo { get; set; }
        public string Descricao { get; set; }
        public string AmostraExigida { get; set; }
        public string AmostraRealizada { get; set; }
        public string ValorMedioDetectado { get; set; }
        public string Referencias { get; set; }
        public int? SeqAmostra { get; set; }
        public TipoAnaliseAguaEnum DescricaoColuna { get; set; }
        
    }
}
