﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class Interrupcao : EntityBase<long>
    {
        public int IdStatus { get; set; }
        public DateTime DataHoraInicio { get; set; }
        public DateTime DataHoraFim { get; set; }
        public string Observacao { get; set; }

        public int PrazoTerminoEmHoras
        {
            get
            {
                if (IdStatus != 3 || DataHoraFim < DateTime.Now) return 0;
                TimeSpan ts = DataHoraFim.Subtract(DateTime.Now);
                return (int)Math.Ceiling(ts.TotalHours);
            }
        }
    }
}
