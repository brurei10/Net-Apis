﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Domain
{
    public class Fiscal
    {
        public string STATUS { get; set; }
        public string COD_TRANSACAO { get; set; }
        public string COD_RELATORIO { get; set; }
        public string EMP_INOVA { get; set; }
        public string EMP_PROTHEUS { get; set; }
        public string RAZAO_SOCIAL { get; set; }
        public string CNPJ { get; set; }
        public string INSC_ESTADUAL { get; set; }
        public string ENDERECO { get; set; }
        public string UF { get; set; }
        public string COD_MUN { get; set; }
        public string MUNICIPIO { get; set; }
        public string BAIRRO { get; set; }
        public string CEP { get; set; }
        public string TEL { get; set; }
        public string EMAIL { get; set; }
        public string CAT_RETENCAO { get; set; }
        public string NOTA_FISCAL { get; set; }
        public string DATA_EMISSAO { get; set; }
        public string DATA_BAIXA { get; set; }
        public float VALOR { get; set; }
        public float VALOR_PIS { get; set; }
        public float VALOR_COFINS { get; set; }
        public float VALOR_CSLL { get; set; }
        public float VALOR_IRPJ { get; set; }

        


    }
}
