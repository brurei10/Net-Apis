﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ContaDigitalEnvio
        : EntityBase<long>
    {
        public ContaDigitalEnvio()
        {
            DataEnvio = DateTime.Now;
            Status = ContaDigitalEnvioStatusEnum.Gerado;
        }

        public ContaDigitalEnvio(Conta conta)
            : this()
        {
            SeqOriginal = conta.Id;
        }

        public virtual long SeqOriginal { get; set; }
        public virtual DateTime DataEnvio { get; set; }
        public virtual ContaDigitalEnvioStatusEnum Status { get; set; }
        public virtual long? IdMensagemIntegrador { get; set; }
    }
}
