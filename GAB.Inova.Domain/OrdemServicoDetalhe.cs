﻿using GAB.Inova.Common.Enums;
using DomainValidationCore.Validation;
using System;

namespace GAB.Inova.Domain
{
    public class OrdemServicoDetalhe
    {
        public virtual long NroOS { get; set; }

        public virtual int SeqOs { get; set; }

        public virtual long NumeroOS { get; set; }

        public virtual long CodigoServico { get; set; }

        public virtual StatusDetOrdemServicoEnum Status { get; set; }

        public virtual DateTime DataProgramadaExecucaoCliente { get; set; }

        public virtual DateTime? DataProgramacao { get; set; }

        public virtual DateTime? DataBaixa { get; set; }

        public virtual ValidationResult ValidationResult { get; set; }

        public OrdemServicoDetalhe()
        {
        }

        public OrdemServicoDetalhe(long numeroOS)
        {
            NumeroOS = numeroOS;
        }
    }
}
