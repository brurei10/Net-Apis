﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;

namespace GAB.Inova.Domain
{
    public class Cliente
    {

        public virtual string Localizacao { get; set; }
        public virtual string DigitoLocalizacao { get; set; }
        public virtual string Matricula { get; set; }
        public virtual string Nome { get; set; }
        public virtual TipoFaturaEnum IdTipoFatura { get; set; }
        public virtual TipoClienteEnum IdTipoCliente { get; set; }
        public virtual short SituacaoAgua { get; set; }
        public virtual short SituacaoEsgoto { get; set; }
        public virtual short SituacaoHd { get; set; }
        public virtual short DiaVencimentoConta { get; set; }
        public virtual string DigitoMatricula { get; set; }
        public virtual bool DebitoAutomatico { get; set; }
        public virtual GrupoEntregaEnum IdGrupoEntrega { get; set; }

        public virtual Contrato Contrato { get; set; }

        #region [Propriedade para validações]

        public virtual ValidationResult ValidationResult { get; set; }

        #endregion
        
    }
}
