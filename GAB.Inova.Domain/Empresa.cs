﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Empresa
        : EntityBase<string>
    {

        public virtual string Nome { get; set; }
        public virtual string Sigla { get; set; }
        public virtual string IdFebraban { get; set; }
        public virtual string UrlEndPoint { get; set; }

    }
}
