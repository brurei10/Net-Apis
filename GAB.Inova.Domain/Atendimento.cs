﻿using System.Collections.Generic;

namespace GAB.Inova.Domain
{
    public class Atendimento
    {
        public Atendimento()
        {
            naturezas = new HashSet<Natureza>();
        }
        public string nomeCliente { get; set; }
        public string documento { get; set; }
        public ICollection<Natureza> naturezas { get; set; }
    }
}
