﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class GRP: EntityBase<long>
    {
        public DateTime DataHoraEmissaoGRP { get; set; }

        public string Matricula { get; set; }

        public string Localizacao { get; set; }

        public int NroOs { get; set; }

        public double ValorGRP { get; set; }

        public DateTime DataVencimentoGRP { get; set; }

        public DateTime DataValorGRP { get; set; }

        public string NomeSolicitante { get; set; }

        public string IdentidadeSolicitante { get; set; }

        public string Observacao { get; set; }

        public int IdUsuario { get; set; }

        public int IdSolicitacao { get; set; }

        public long IdProtocoloAtendimento { get; set; }

    }
}