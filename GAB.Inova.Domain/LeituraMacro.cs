﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Domain
{
    public class LeituraMacro
    {
        public string Code { get; set; } //codigo do HD
        public DateTime Date { get; set; } //Data da leitura (ex.: "2014–12–31") 
        public double Volume { get; set; } //Leitura realizada (nao eh o volume)
    }
}
