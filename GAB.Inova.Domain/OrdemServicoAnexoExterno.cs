﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class OrdemServicoAnexoExterno
        : EntityBase<long>
    {
        public long OSEletronicaID { get; set; }

        public long? NumeroOS { get; set; }

        public string Descricao { get; set; }

        public string Identificador { get; set; }

        public virtual ValidationResult ValidationResult { get; set; }
    }
}
