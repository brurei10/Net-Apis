﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class ProtocoloAtendimentoDetalhe 
        : EntityBase<long>
    {

        public virtual long IdProtocoloAtendimento { get; set; }
        public virtual long? IdRotina { get; set; }
        public virtual long? NumeroOs { get; set; }
        public virtual long? NumeroDoc { get; set; }
        public virtual long IdNatureza { get; set; }
        public virtual int TipoProtocolo { get; set; }
        
        public virtual ProtocoloAtendimento ProtocoloAtendimento { get; set; }
        public virtual TabNaturezaSolicitacao TabNaturezaSolicitacao { get; set; }

    }
}
