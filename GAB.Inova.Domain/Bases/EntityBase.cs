﻿namespace GAB.Inova.Domain.Bases
{
    public abstract class EntityBase<TKey>
    {
        public virtual TKey Id { get; set; }     
    }
}

