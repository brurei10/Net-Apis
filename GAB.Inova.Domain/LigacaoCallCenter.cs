﻿namespace GAB.Inova.Domain
{
    public class LigacaoCallCenter
    {
        public string numLigacao { get; set; }
        public int flgLigacaoEspecial { get; set; }
        public int flgInterrupcaoAbast { get; set; }
        public int? previsaoRetorno { get; set; }
        public int flgTipoClienteGrande { get; set; }
    }
}
