﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Domain
{
    public class ProtocoloAtendimentoGfa
        : EntityBase<long>
    {

        public ProtocoloAtendimentoGfa()
        {
            DataInicioAtendimento = DateTime.Now;
            IdIntegrador = IntegradorGfaEnum.Atend;
            ProtocoloAtendimentos = new HashSet<ProtocoloAtendimento>();
        }

        public virtual string SenhaGfa { get; set; }

        public virtual string NumeroProtocoloGfa { get; set; }

        public virtual string UsuarioGfa { get; set; }

        public virtual IntegradorGfaEnum IdIntegrador { get; set; }

        public virtual DateTime DataInicioAtendimento { get; set; }

        public virtual DateTime? DataFinalAtendimento { get; set; }
        
        public virtual ICollection<ProtocoloAtendimento> ProtocoloAtendimentos { get; set; }

        #region [Propriedade para validações]

        public virtual ValidationResult ValidationResult { get; set; }

        #endregion

    }
}
