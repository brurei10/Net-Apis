﻿namespace GAB.Inova.Domain
{
    public class VwDadosClienteInterrupcao
    {
        public string Matricula { get; set; }
        public string NomeCliente { get; set; }
        public long IdLogradouro { get; set; }
        public long IdBairro { get; set; }
        public long IdCidade { get; set; }
        public long IdSetorOperacional { get; set; }
    }
}
