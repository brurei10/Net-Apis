﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ContratoMensagemIntegrador
        : EntityBase<long>
    {

        public ContratoMensagemIntegrador()
        {
            DataInclusao = DateTime.Now;
        }

        public virtual long IdMensagemIntegrador { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual long IdContrato { get; set; }

    }
}
