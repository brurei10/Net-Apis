﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Domain
{
    public class OrdemServicoEletronica
        : EntityBase<long>
    {
        #region Header

        public virtual long? OSEletronicaOrigemID { get; set; }
        public virtual string IdentificadorRequisicao { get; set; }
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }
        public virtual string CentroOperativo { get; set; }
        public virtual bool EhAssociada { get; set; }
        #endregion

        #region OS

        public virtual string IdentificadorOS { get; set; }
        public virtual long? NumeroOS { get; set; }
        public virtual string IdentificadorExterno { get; set; }
        public virtual long CodigoServico { get; set; }
        public virtual int? SequencialVinculada { get; set; }
        public virtual DateTime? DataHoraInicioExecucao { get; set; }
        public virtual DateTime? DataHoraFimExecucao { get; set; }
        public virtual string EquipeExecutora { get; set; }
        public virtual long? MotivoAberturaID { get; set; }
        public virtual long? MotivoBaixaID { get; set; }
        public virtual bool? PossuiBomba { get; set; }
        public virtual long? NumeroNotificacao { get; set; }
        public virtual bool? EhTrocaTitularidadeID { get; set; }
        public virtual long? SituacaoImovelID { get; set; }
        public virtual string HidrometroID { get; set; }
        public virtual int? LeituraDeRetirada { get; set; }
        public virtual string HidrometroInstaladoID { get; set; }
        public virtual int? LeituraDeInstalacao { get; set; }
        public virtual long? LocalInstalacaoHidrometroID { get; set; }
        public virtual long? PadraoInstalacaoHidrometroID { get; set; }
        public virtual string NumeroSeloVirola { get; set; }
        public virtual string NumeroLacreInmetro { get; set; }
        public virtual int? Leitura { get; set; }
        public virtual string Laudo { get; set; }
        public virtual string Observacao { get; set; }
        public virtual StatusTesteCloroEnum? TesteCloro { get; set; }

        #endregion

        #region Contrato 

        public virtual string NomeOuRazaoSocial { get; set; }
        public virtual string CPFCNPJ { get; set; }
        public virtual string Celular { get; set; }
        public virtual string Telefone { get; set; }
        public virtual DateTime? DataNascimento { get; set; } 
        public virtual string NomeMae { get; set; } 
        public virtual string NumeroRG { get; set; } 
        
        #endregion

        #region Endereco

        public virtual string TipoLogradouro { get; set; }
        public virtual string Logradouro { get; set; }
        public virtual int? Numero { get; set; }
        public virtual string Complemento { get; set; }
        public virtual string PontoDeReferencia { get; set; }
        public virtual long? BairroID { get; set; }
        public virtual String CodigoMunicipio { get; set; }

        #endregion

        #region Cliente

        public virtual long? CategoriaID { get; set; }
        public virtual long? SubCategoriaID { get; set; }
        public virtual bool? EhAreaRisco { get; set; }
        public virtual int? NumeroEconomiasComercial { get; set; }
        public virtual int? NumeroEconomiasResidencial { get; set; }
        public virtual int? NumeroEconomiasIndustrial { get; set; }
        public virtual int? NumeroEconomiasPublico { get; set; }
        public virtual long? DiametroRedeID { get; set; }
        public virtual long? DiametroHidrometroID { get; set; }
        public virtual long? DiametroRamalID { get; set; }
        public virtual long? FrequenciaUtilizacaoImovelID { get; set; }
        public virtual long? LocalizacaoRamalID { get; set; }
        public virtual string MaterialRede { get; set; }
        public virtual string MaterialRamal { get; set; }
        public virtual long? ProfundidadeRedeID { get; set; }
        public virtual long? ProfundidadeRamalID { get; set; }
        public virtual long? TipoFonteAlternativaID { get; set; }
        public virtual long? TipoIrregularidadeID { get; set; }
        public virtual long? TipoPavimentoCalcadaID { get; set; }
        public virtual long? TipoPavimentoRuaID { get; set; }
        public virtual bool? TemCaixaCorreios { get; set; }
        public virtual bool? EhClienteFlutuante { get; set; }
        public virtual bool? TemReloajoariaPreEquipada { get; set; }
        public virtual bool? TemFonteAlternativa { get; set; }
        public virtual bool? TemBoiaCaixaDagua { get; set; }
        public virtual bool? TemBoiaSisterna { get; set; }
        public virtual bool? TemEquipamentoTelemetriaAcoplada { get; set; }
        public virtual bool? TemValvulaRetencao { get; set; }
        public virtual int? NumeroFuncionarios { get; set; }
        public virtual int? NumeroMoradores { get; set; }
        public virtual int? QuantidadeBanheirosOuLavabos { get; set; }
        public virtual int? QuantidadeLojas { get; set; }
        public virtual int? QuantidadeQuartosImovel { get; set; }
        public virtual int? QuantidadeSalas { get; set; }
        public virtual long? VazaoComBomba { get; set; }
        public virtual long? VazaoSemBomba { get; set; }
        public virtual long? VolumeCaixaDagua { get; set; }
        public virtual long? VolumeCisterna { get; set; }
        public virtual long? VolumePiscina { get; set; }

        #endregion

        #region Anexos

        public virtual IEnumerable<OrdemServicoAnexoExterno> Fotos { get; set; }

        #endregion

        public virtual ValidationResult ValidationResult { get; set; }

        public OrdemServicoEletronica()
        {
            Fotos = new HashSet<OrdemServicoAnexoExterno>();
        }       
        
        public long GetNroOS()
        {
            if (NumeroOS.HasValue)
            {
                var stringValue = NumeroOS.ToString();
                return int.Parse(stringValue.Remove(stringValue.Length - 1));
            }
            return default(int);
        }

        public int GetSeqOS()
        {
            if (NumeroOS.HasValue)
            {
                var stringValue = NumeroOS.ToString();
                return int.Parse(stringValue.Substring(stringValue.Length - 1));
            }
            return default(int);
        }
    }
}
