﻿namespace GAB.Inova.Domain
{
    public class VwGeraArquivoImpressao
        : TempImprimeConta
    {

        public double? ValorEsgoto { get; set; }
        public double? ValorCustoRegulacaoFiscalizacao { get; set; }
        public string RotaOrdem { get; set; }
        public string CepOrdem { get; set; }

        public string MensagemPadrao01 { get; set; }
        public string MensagemPadrao02 { get; set; }
        public string MensagemPadrao03 { get; set; }
        public string MensagemPadrao04 { get; set; }
        public string MensagemPadrao05 { get; set; }
        public string MensagemPadrao06 { get; set; }
        public string MensagemPadrao07 { get; set; }
        public string MensagemPadrao08 { get; set; }
        public string MensagemPadrao09 { get; set; }
        public string MensagemPadrao10 { get; set; }
        public string MensagemDebitoAnterior01 { get; set; }
        public string MensagemDebitoAnterior02 { get; set; }
        public string MensagemDebitoAnterior03 { get; set; }
        public string MensagemDebitoAnterior04 { get; set; }
        public string MensagemDebitoAnterior05 { get; set; }
        public string MensagemDebitoAnterior06 { get; set; }
        public string MensagemDebitoAnterior07 { get; set; }
        public string MensagemDebitoAnterior08 { get; set; }
        public string MensagemDebitoAnterior09 { get; set; }
        public string MensagemDebitoAnterior10 { get; set; }
        public string MensagemDebitoAnterior11 { get; set; }
        public string MensagemDebitoAnterior12 { get; set; }
        public string MensagemDebitoAnterior13 { get; set; }
        public string MensagemDebitoAnterior14 { get; set; }
        public string MensagemDebitoAnterior15 { get; set; }
        public string MensagemDebitoAnterior16 { get; set; }
        public string MensagemDebitoAnterior17 { get; set; }
        public string MensagemDebitoAnterior18 { get; set; }
        public string MensagemDebitoAnterior19 { get; set; }
        public string MensagemDebitoAnterior20 { get; set; }
        public string MensagemDebitoAnterior21 { get; set; }
        public string MensagemDebitoAnterior22 { get; set; }
        public string MensagemDebitoAnterior23 { get; set; }
        public string MensagemDebitoAnterior24 { get; set; }
        public string MensagemDebitoAnterior25 { get; set; }
        public string MensagemDebitoAnterior26 { get; set; }
        public string MensagemDebitoAnterior27 { get; set; }
        public string MensagemDebitoAnterior28 { get; set; }
        public string MensagemDebitoAnterior29 { get; set; }
        public string MensagemDebitoAnterior30 { get; set; }

        public string FaixaEconomia01 { get; set; }
        public string FaixaConsumo01 { get; set; }
        public string FaixaConsumoFaturado01 { get; set; }
        public string FaixaValorTarifa01 { get; set; }
        public string FaixaValorTarifaEsgoto01 { get; set; }

        public string FaixaEconomia02 { get; set; }
        public string FaixaConsumo02 { get; set; }
        public string FaixaConsumoFaturado02 { get; set; }
        public string FaixaValorTarifa02 { get; set; }
        public string FaixaValorTarifaEsgoto02 { get; set; }

        public string FaixaEconomia03 { get; set; }
        public string FaixaConsumo03 { get; set; }
        public string FaixaConsumoFaturado03 { get; set; }
        public string FaixaValorTarifa03 { get; set; }
        public string FaixaValorTarifaEsgoto03 { get; set; }

        public string FaixaEconomia04 { get; set; }
        public string FaixaConsumo04 { get; set; }
        public string FaixaConsumoFaturado04 { get; set; }
        public string FaixaValorTarifa04 { get; set; }
        public string FaixaValorTarifaEsgoto04 { get; set; }

        public string FaixaEconomia05 { get; set; }
        public string FaixaConsumo05 { get; set; }
        public string FaixaConsumoFaturado05 { get; set; }
        public string FaixaValorTarifa05 { get; set; }
        public string FaixaValorTarifaEsgoto05 { get; set; }

        public string FaixaEconomia06 { get; set; }
        public string FaixaConsumo06 { get; set; }
        public string FaixaConsumoFaturado06 { get; set; }
        public string FaixaValorTarifa06 { get; set; }
        public string FaixaValorTarifaEsgoto06 { get; set; }

        public string FaixaEconomia07 { get; set; }
        public string FaixaConsumo07 { get; set; }
        public string FaixaConsumoFaturado07 { get; set; }
        public string FaixaValorTarifa07 { get; set; }
        public string FaixaValorTarifaEsgoto07 { get; set; }

        public string FaixaEconomia08 { get; set; }
        public string FaixaConsumo08 { get; set; }
        public string FaixaConsumoFaturado08 { get; set; }
        public string FaixaValorTarifa08 { get; set; }
        public string FaixaValorTarifaEsgoto08 { get; set; }

        public string FaixaEconomia09 { get; set; }
        public string FaixaConsumo09 { get; set; }
        public string FaixaConsumoFaturado09 { get; set; }
        public string FaixaValorTarifa09 { get; set; }
        public string FaixaValorTarifaEsgoto09 { get; set; }

        public string FaixaEconomia10 { get; set; }
        public string FaixaConsumo10 { get; set; }
        public string FaixaConsumoFaturado10 { get; set; }
        public string FaixaValorTarifa10 { get; set; }
        public string FaixaValorTarifaEsgoto10 { get; set; }

        public long? LoteAvisoDebito { get; set; }
        public long? NumeroAvisoDebito { get; set; }
        public double? ValorAvisoDebito { get; set; }
        public string CodigoBarraAvisoDebito { get; set; }
        public string TextoMensagemAvisoDebito { get; set; }

        public decimal? PercentualCargaTributaria { get; set; }
        public int? SituacaoConta { get; set; }
        public string ObservacaoGrp { get; set; }
        public string SiglaEmpresa { get; set; }

    }
}

