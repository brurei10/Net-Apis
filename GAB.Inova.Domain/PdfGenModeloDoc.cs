﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class PdfGenModeloDoc
        : EntityBase<long>
    {

        public string Nome { get; set; }
        public string Descricao { get; set; }
        public long? IdTipoDocumento { get; set; }
        public int TamanhoPagina { get; set; }
        public int OrientacaoPagina { get; set; }
        public decimal? AlturaPagina { get; set; }
        public decimal? LarguraPagina { get; set; }
        public int ItensPorPagina { get; set; }
        public int OrientacaoRepeticoes { get; set; }
        public decimal? OffsetRepeticao { get; set; }
        public long IdItemDocumento { get; set; }

    }
}
