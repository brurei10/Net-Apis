﻿using System.Net;

namespace GAB.Inova.Domain
{
    public class EmailIntegradorResponse
    {
        public bool Success => !string.IsNullOrEmpty(MessageGuid);
        public string MessageGuid { get; }
        public string Erros {get;}
        public HttpStatusCode StatusCode { get; }

        public EmailIntegradorResponse(string guid)
        {
            StatusCode = HttpStatusCode.Accepted;
            MessageGuid = guid;
        }

        public EmailIntegradorResponse(HttpStatusCode statusCode, string erros)
        {
            StatusCode = statusCode;
            Erros = erros;
        }
    }
}
