﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class VwDadosDebitoAnual
        : EntityBase<long>
    {
        public string AnoMesCt { get; set; }
        public long SeqOriginalPago { get; set; }
        public DateTime DataVencimento { get; set; }
        public long ValorOriginal { get; set; }
    }
}
