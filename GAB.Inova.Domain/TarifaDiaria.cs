﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class TarifaDiaria
        : EntityBase<long>
    {

        public string TipoTarifa { get; set; }
        public DateTime DataTarifa { get; set; }
        public long IdCategoria { get; set; }
        public double FaixaAnterior { get; set; }
        public double FaixaConsumo { get; set; }
        public double ValorFaixa { get; set; }
        public double ValorFaixaEsgoto { get; set; }
        public double ValorAcumulado { get; set; }
        public double ValorAcumuladoEsgoto { get; set; }
        public double? ValorFaixaSocial { get; set; }
        public double? ValorAcumuladoSocial { get; set; }
        public string Localidade { get; set; }
        public string GrupoTarifa { get; set; }
        public long IdTarifa { get; set; }
        public long IdFaixaTarifa { get; set; }
        
    }
}
