﻿using System.Collections.Generic;

namespace GAB.Inova.Domain
{
    public class ClienteCallCenter
    {
        public ClienteCallCenter()
        {
            ligacoes = new HashSet<LigacaoCallCenter>();
        }
        public string nomeCliente { get; set; }
        public string documento { get; set; }
        public int possuiEmail { get; set; }
        public int possuiCelular { get; set; }
        public ICollection<LigacaoCallCenter> ligacoes { get; set; }
    }
}
