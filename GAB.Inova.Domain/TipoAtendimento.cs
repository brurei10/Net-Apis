﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class TipoAtendimento
        : EntityBase<long>
    {
        public virtual string Nome { get; set; }

    }
}
