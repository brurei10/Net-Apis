﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class DeclaracaoAnual : EntityBase<long>
    {
        public virtual string Matricula { get; set; }
        public virtual string AnoRef { get; set; }
        public virtual DateTime DataEnvio { get; set; }
        public virtual DateTime DataRetorno { get; set; }
        public virtual string AnoMesRef { get; set; }
        public virtual string IndiceA { get; set; }
        public virtual TipoDeclaracaoEnum TipoDeclaracao { get; set; }
        public virtual int Impressao { get; set; }
        public virtual TipoDeclaraParcialEnum TipoDeclaraParcial { get; set; }
    }
}
