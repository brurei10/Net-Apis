﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ImprimeGRP : EntityBase<long>
    {
        public string Matricula { get; set; }

        public string Titulo { get; set; }

        public string NomeCliente { get; set; }

        public string Endereco { get; set; }

        public string NomeLocal { get; set; }

        public string ConsumoAnterior01 { get; set; }

        public string ConsumoAnterior02 { get; set; }

        public string ConsumoAnterior03 { get; set; }

        public string ConsumoAnterior04 { get; set; }

        public string ConsumoAnterior05 { get; set; }

        public string ConsumoAnterior06 { get; set; }

        public string ConsumoAnterior07 { get; set; }

        public string ConsumoAnterior08 { get; set; }

        public string ConsumoAnterior09 { get; set; }

        public string ConsumoAnterior10 { get; set; } 

        public string ConsumoAnterior11 { get; set; }

        public string ConsumoAnterior12 { get; set; }

        public int EconomiaResidencial { get; set; }

        public int EconomiaComercial { get; set; }

        public int EconomiaIndustrial{ get; set; }

        public int EconomiaPublica { get; set; }

        public int IdCiclo { get; set; }

        public string SetorRotaSequencia { get; set; }

        public string IdHd { get; set; }

        public int TipoEntrega { get; set; }

        public string AnoMesCt { get; set; }

        public DateTime? DataLeituraAtual { get; set; }

        public DateTime? DataLeituraAnterior { get; set; }

        public int ConsumoFaturado { get; set; }

        public Int64 LeituraAtual { get; set; }

        public Int64 LeituraAnterior { get; set; }

        public int Media { get; set; }

        public double ValorGRP { get; set; }

        public DateTime? DataVencimento { get; set; }

        public DateTime? DataValor { get; set; }

        public string Observacao { get; set; }

        public string CodigoBarra { get; set; }

        public string CompoeCt01 { get; set; }

        public string CompoeCt02 { get; set; }

        public string CompoeCt03 { get; set; }

        public string CompoeCt04 { get; set; }

        public string CompoeCt05 { get; set; }

        public string CompoeCt06 { get; set; }

        public string CompoeCt07 { get; set; }

        public string CompoeCt08 { get; set; }

        public string CompoeCt09 { get; set; }

        public string CompoeCt10 { get; set; }

        public string CompoeCt11 { get; set; }

        public string CompoeCt12 { get; set; }

        public string CompoeCt13 { get; set; }

        public string CompoeCt14 { get; set; }

        public string CompoeCt15 { get; set; }

        public string CompoeCt16 { get; set; }

        #region Estrutura Tarifária

        public string EstruturaTarifaria01 { get; set; }

        public string EstruturaTarifaria02 { get; set; }

        public string EstruturaTarifaria03 { get; set; }

        public string EstruturaTarifaria04 { get; set; }

        public string EstruturaTarifaria05 { get; set; }

        public string EstruturaTarifaria06 { get; set; }

        public string EstruturaTarifaria07 { get; set; }

        public string EstruturaTarifaria08 { get; set; }

        public string EstruturaTarifaria09 { get; set; }

        public string EstruturaTarifaria10 { get; set; }

        public string EstruturaTarifaria11 { get; set; }

        public string EstruturaTarifaria12 { get; set; }

        public string EstruturaTarifaria13 { get; set; }

        public string EstruturaTarifaria14 { get; set; }

        public string EstruturaTarifaria15 { get; set; }

        public string EstruturaTarifaria16 { get; set; }

        #endregion Estrutura Tarifária

        public double CompoeValor01 { get; set; }

        public double CompoeValor02 { get; set; }

        public double CompoeValor03 { get; set; }

        public double CompoeValor04 { get; set; }

        public double CompoeValor05 { get; set; }

        public double CompoeValor06 { get; set; }

        public double CompoeValor07 { get; set; }

        public double CompoeValor08 { get; set; }

        public double CompoeValor09 { get; set; }

        public double CompoeValor10 { get; set; }

        public double CompoeValor11 { get; set; }

        public double CompoeValor12 { get; set; }

        public double CompoeValor13 { get; set; }

        public double CompoeValor14 { get; set; }

        public double CompoeValor15 { get; set; }

        public double CompoeValor16 { get; set; }

        public int IdSolicitacao { get; set; }

        //public int Economia { get; set; }

        public DateTime DataEmissao { get; set; }
        public int DiasConsumo { get; set; }

        //public int IdSituacaoHd { get; set; }

        //public int IdTarifa { get; set; }

        //public double ValorIcms { get; set; }

        //public double ValorRetidoImposto { get; set; }

        //public double ValorRecursoHidrico { get; set; }

        //public int ValorEsgotoAlternativo { get; set; }
    }
}