﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using System;

namespace GAB.Inova.Domain
{
    /// <summary>
    /// Objeto que controla o fluxo da Ordem de Serviço Eletronica
    /// </summary>
    public class OrdemServicoEletronicaControle
    {
        public virtual long NumeroOS { get; set; }
        public virtual int SequenciaOS { get; set; }
        public virtual SituacaoOSEletronicaEnum Situacao { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual DateTime? DataEnvio { get; set; }
        public virtual DateTime? DataRetorno { get; set; }

        public virtual ValidationResult ValidationResult { get; set; }

        public OrdemServicoEletronicaControle()
        {
        }

        public OrdemServicoEletronicaControle(long numeroOS, int sequenciaOS)
        {
            NumeroOS = numeroOS;
            SequenciaOS = sequenciaOS;
        }
    }
}
