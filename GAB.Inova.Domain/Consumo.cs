﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Consumo
        : EntityBase<string>
    {

        public string Matricula { get; set; }
        public long Faturado { get; set; }
        public long QtdeDiasLeitura { get; set; }

    }
}
