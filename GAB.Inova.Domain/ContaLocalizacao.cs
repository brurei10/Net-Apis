﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ContaLocalizacao
        : EntityBase<long>
    {

        public string SiglaEmpresa { get; set; }
        public int IdCiclo { get; set; }
        public string Rota { get; set; }
        public DateTime Emissao { get; set; }
        public SituacaoContaEnum SituacaoConta { get; set; }

    }
}
