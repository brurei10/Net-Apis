﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ParametroSistema
        : EntityBase<long>
    {

        public virtual string Descricao { get; set; }
        public virtual decimal Valor { get; set; }
        public virtual long IdUnidadeMedida { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual int Situacao { get; set; }

        public virtual UnidadeMedida UnidadeMedida { get; set; }

    }
}
