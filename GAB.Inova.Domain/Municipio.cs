﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Municipio
        : EntityBase<long>
    {
        public virtual string Codigo { get; set; }
        public virtual string Nome { get; set; }
    }
}
