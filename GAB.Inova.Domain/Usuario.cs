﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Usuario 
        : EntityBase<long>
    {

        public virtual string NomeAcesso { get; set; }

        public virtual string NomeCompleto { get; set; }

        public virtual string Email { get; set; }

        public virtual bool Situacao { get; set; }

        public virtual bool SituacaoAd { get; set; }

    }
}
