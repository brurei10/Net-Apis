﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class PdfGenItemDoc
        : EntityBase<long>
    {

        public string Nome { get; set; }
        public string IdentacaoNome { get; set; }
        public int Level { get; set; }
        public string Descricao { get; set; }
        public long IdTipoDocumento { get; set; }
        public int TipoItemDocumento { get; set; }
        public long? IdItemDocumentoParent { get; set; }
        public int? Ordem { get; set; }
        public short SomentePreview { get; set; }
        public string ValorPreview { get; set; }
        public int? EstiloPosicao { get; set; }
        public float? EstiloPosicaoEsquerda { get; set; }
        public float? EstiloPosicaoTopo { get; set; }
        public float? EstiloAltura { get; set; }
        public float? EstiloLargura { get; set; }
        public float? EstiloMargemEsquerda { get; set; }
        public float? EstiloMargemTopo { get; set; }
        public int? EstiloFonteNome { get; set; }
        public int? EstiloFonteTamanho { get; set; }
        public int? EstiloFonteFormato { get; set; }
        public string EstiloFonteColor { get; set; }
        public int? EstiloSombra { get; set; }
        public int? EstiloAlinhamentoVertical { get; set; }
        public int? EstiloAlinhamentoHorizontal { get; set; }
        public int? EstiloRepeticaoOrientacao { get; set; }
        public int? EstiloRepeticaoMaxima { get; set; }
        public float? EstiloRepeticaoEspacamento { get; set; }
        public float? EstiloEspacoQuebraLinha { get; set; }

    }
}
