﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Processo
        : EntityBase<long>
    {

        /// <summary>
        /// Identifica se o número do processo cadastrado segue o padrão novo (somente se a natureza for jurídica). 
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual int ProcessoNovo { get; set; }
        /// <summary>
        /// Identifica o Ano de Cadastro do Processo.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual int AnoProcesso { get; set; }
        /// <summary>
        /// Identifica se as próximas contas emitidas deverão ser vinculadas ao processo.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short VincularContaFutura { get; set; }
        /// <summary>
        /// Identifica os processos que desmembram a conta do cliente em duas contas, uma cobrando água e outra cobrando esgoto.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short DesmembrarConta { get; set; }
        /// <summary>
        /// Identifica se deve ser cumprida tutela cautelar.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short CumprirTutelaCautelar { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir uma ação de cobrança.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short AcaoCobranca { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir emissão de auto de infração.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short AutoInfracao { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir alterações cadastrais.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short AlteracaoCadastral { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir emissões de corte.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoCorte { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir emissão de contas.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoConta { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir emissões de notificações.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoNotificacao { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir parcelamentos de débitos.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short ParcelamentoDebitos { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir emissões de ordens de serviço.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoOrdemServico { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir retificações de contas.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short RetificacaoConta { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou não permitir impressão de segunda via.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoSegundaVia { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou n?o permitir emitir quitação de débito.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short EmissaoQuitacaoDebito { get; set; }
        /// <summary>
        /// Identifica se o processo deve ou nao permitir a negativacao do cliente.
        /// (Ex: 1 - SIM / 2 - NÃO)
        /// </summary>
        public virtual short Negativacao { get; set; }

    }
}
