﻿using System;

namespace GAB.Inova.Domain
{
    public class Natureza
    {
        public int codNatureza { get; set; }
        public string descNatureza { get; set; }
        public DateTime datahora { get; set; }
    }
}
