﻿using System;

namespace GAB.Inova.Domain
{
    public class VwDadosConsultaOrdemServico
    {
        public long NumeroOs { get; set; }
        public int SituacaoOs { get; set; }
        public string DescricaoSituacaoOs { get; set; }
        public DateTime DataPrevista { get; set; }
        public DateTime? DataProgramada { get; set; }
        public DateTime? DataBaixa { get; set; }
        public string Matricula { get; set; }
        public string Documento { get; set; }
        public string CodigoServico { get; set; }
        public string DescricaoServico { get; set; }
    }
}
