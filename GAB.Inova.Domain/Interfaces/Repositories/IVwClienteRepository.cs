﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IVwClienteRepository
    {
        Task<IEnumerable<VwCliente>> ObterPorDocumentoAsync(string documento);

        Task<VwCliente> ObterPorMatriculaAsync(string matricula);
    }
}
