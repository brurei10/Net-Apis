﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IInstanciaRepository
    {
        Task<InstanciaCallCenter> GerarAutenticacaoAsync(string loginOperador, string protocolo, string chavePrivada);
    }
}
