﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IParcelaRepository
    {
        Task<double> ObterTotalPorMatriculaSituacaoContratoAsync(string matricula, int situacao, long contrato);
    }
}
