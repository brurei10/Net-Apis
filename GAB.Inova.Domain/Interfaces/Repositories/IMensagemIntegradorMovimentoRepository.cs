﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IMensagemIntegradorMovimentoRepository
    {

        Task<bool> InserirAsync(MensagemIntegradorMovimento entity);

    }
}
