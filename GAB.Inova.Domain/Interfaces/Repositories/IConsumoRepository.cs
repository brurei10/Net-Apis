﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IConsumoRepository
    {

        Task<IEnumerable<Consumo>> ObterUltimosDozeConsumosPorMatriculaAsync(string matricula);

    }
}
