﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IVwDadosConsultaOrdemServicoRepository
    {
        Task<VwDadosConsultaOrdemServico> ObterAsync(long numeroOS);
    }
}
