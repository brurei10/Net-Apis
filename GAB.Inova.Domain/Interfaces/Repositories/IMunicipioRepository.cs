﻿using DomainValidationCore.Validation;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IMunicipioRepository
    {
        bool VerificarExistencia(Municipio municipio);

        #region [Métodos públicos para validação]

        ValidationResult MunicipioValido(Municipio entity);

        #endregion

    }
}
