﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ITarifaDiariaRepository
    {

        Task<IEnumerable<TarifaDiaria>> ObterTarifaParaImpressaoContaAsync(TarifaDiaria tarifaDiaria);

    }
}
