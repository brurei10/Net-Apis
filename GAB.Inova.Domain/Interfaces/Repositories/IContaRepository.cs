﻿using GAB.Inova.Common.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IContaRepository
    {

        Task<ContaLocalizacao> ObterContaLocalizacaoAsync(long seqOriginal);

        /// <summary>
        /// Obter as contas pelo numero da conta (sequência original)
        /// </summary>
        /// <param name="seqOriginal">Indica uma chave identificadora de uma conta</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>uma conta</returns>
        Task<Conta> ObterContaPorSeqOriginalAsync(long seqOriginal, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal);

        /// <summary>
        /// Obter as contas pelo numeros das contas (sequência original)
        /// </summary>
        /// <param name="seqOriginal">Indica uma lista de chaves identificadoras de contas</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>uma conta</returns>
        Task<IEnumerable<Conta>> ObterContasPorSeqOriginalAsync(IEnumerable<long> seqOriginais, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal);

        /// <summary>
        /// Obter as contas com situação em aberto pelo contrato do cliente
        /// </summary>
        /// <param name="idContrato">Indica uma chave identificadora de um contrato</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>Uma lista de contas</returns>
        Task<IEnumerable<Conta>> ObterContasEmAbertoPorContratoAsync(long idContrato, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal);

        /// <summary>
        /// Obter as últimas 12 contas com situação: em aberto ou pagas
        /// </summary>
        /// <param name="idContrato">Indica uma chave identificadora de um contrato</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>Uma lista de contas</returns>
        Task<IEnumerable<Conta>> ObterUltimasDozeContasEmAbertoOuPagasPorContratoAsync(long idContrato, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal);

        Task<VwGeraArquivoImpressao> ObterArquivoImpressaoAsync(long seqOriginal, long idUsuario);

        /// <summary>
        /// Obter o dígito do código de barras
        /// </summary>
        /// <param name="codigoBarras">Indica o codigo de barras de uma conta (barcode43)</param>
        /// <returns>uma string contendo o codigo de barras com os dígitos (barCode48)</returns>
        Task<string> ObterDigitoCodigoBarrasAsync(string codigoBarras);

        /// <summary>
        /// Obter um novo número (SeqOriginal) para um documento não fiscal (DNF)
        /// </summary>
        /// <returns>o novo número para o documento não fiscal (DNF)</returns>
        Task<long> ObterNovoSeqOriginalDnfAsync();

        /// <summary>
        /// Inserir uma nova conta
        /// </summary>
        /// <returns>sequencia original gerada</returns>
        Task<long> InserirAsync(Conta entity);

        /// <summary>
        /// Obter conta de quitação de débito
        /// </summary>
        /// <returns>conta de quitação de débito</returns>
        Task<Conta> ObterQuitacaoDebitoAsync(string matricula, DateTime vencimento, Decimal valor);

        /// <summary>
        /// Obter contas vinculadas a quitação de débito
        /// </summary>
        /// <returns>lista de contas da quitação de débito</returns>
        Task<IEnumerable<Conta>> ObterContasQuitacaoDebitoAsync(long seqOriginal);
    }
}
