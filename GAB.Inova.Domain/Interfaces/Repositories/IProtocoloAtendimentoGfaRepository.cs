﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IProtocoloAtendimentoGfaRepository
        : IValidationSpec<ProtocoloAtendimentoGfa>
    {

        Task<ProtocoloAtendimentoGfa> ObterPorNumeroESenhaAsync(string numeroProtocolo, string senha);
        Task<string> ObterNumeroProtocoloAtendimentoPaiAsync(string numeroProtocolo, string senha);
        Task<ProtocoloAtendimentoGfa> ObterAtendimentoNaoFinalizadoPorUsuarioAsync(string usuario);

        Task<bool> FinalizarAsync(string numeroProtocolo, string senha);
        Task<bool> InserirAsync(ProtocoloAtendimentoGfa entity);

    }
}
