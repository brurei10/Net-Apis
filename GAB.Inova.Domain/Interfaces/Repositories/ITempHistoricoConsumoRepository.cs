﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ITempHistoricoConsumoRepository
    {

        Task<IEnumerable<TempHistoricoConsumo>> ObterTempHistoricoConsumoAsync(string matricula, long idUsuario);

    }
}
