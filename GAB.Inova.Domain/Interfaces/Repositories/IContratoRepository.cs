﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IContratoRepository
    {

        Task<Contrato> ObterPorMatriculaAsync(string matricula);

        Task<bool> AtualizarEmailsAsync(Contrato entity);

    }
}
