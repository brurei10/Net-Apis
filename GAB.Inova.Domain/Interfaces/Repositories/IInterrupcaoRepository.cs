﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IInterrupcaoRepository
    {
        Task<Interrupcao> ConsultarInterrupcaoAsync(VwDadosClienteInterrupcao dadosClienteInterrupcao);
    }
}
