﻿using GAB.Inova.Common.Enums;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    /// <summary>
    /// Interface de acesso a dados referente ao Aviso de Débito usando ORM Dapper
    /// </summary>
    public interface IAvisoDebitoRepository
    {
        void ApropriarOs(long seqOriginal, DateTime dataEntrega, bool cancelamento, MotivoCancelamentoContaDigitalEnum? motivo = null);

        bool ExisteAvisoAtivo(long seqOriginal);

        Task<bool> VerificarSeExisteAvisoAtivoAsync(long seqOriginal);

        Task ApropriarOsAsync(long seqOriginal, DateTime dataEntrega, bool cancelamento, MotivoCancelamentoContaDigitalEnum? motivo = null);
    }
}
