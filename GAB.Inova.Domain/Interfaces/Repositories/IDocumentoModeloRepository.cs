﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IDocumentoModeloRepository
    {
        Task<DocumentoModelo> ObterPorDocumentoModeloTipoAsync(long modeloTipo);
    }
}
