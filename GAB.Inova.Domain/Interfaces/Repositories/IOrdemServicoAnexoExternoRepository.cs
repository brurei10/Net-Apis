﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IOrdemServicoAnexoExternoRepository : IValidationSpec<OrdemServicoAnexoExterno>
    {
        Task InserirAsync(OrdemServicoAnexoExterno entity);
        Task<bool> VerificarExistenciaAsync(OrdemServicoAnexoExterno entity);
    }
}
