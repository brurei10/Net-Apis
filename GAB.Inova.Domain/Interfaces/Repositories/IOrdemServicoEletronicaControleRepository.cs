﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IOrdemServicoEletronicaControleRepository : IValidationSpec<OrdemServicoEletronicaControle>
    {
        OrdemServicoEletronicaControle Obter(long numeroOS, int sequenciaOS);
        Task<OrdemServicoEletronicaControle> ObterAsync(long numeroOS, int sequenciaOS);
        void RegistrarRecebimentoOrdemServicoEletronica(OrdemServicoEletronicaControle controle);
        Task RegistrarRecebimentoOrdemServicoEletronicaAsync(OrdemServicoEletronicaControle controle);
    }
}
