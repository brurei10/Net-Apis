﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IGrupoEntregaRepository 
    {
        Task<GrupoEntrega> ObterAsync(long id);

    }
}
