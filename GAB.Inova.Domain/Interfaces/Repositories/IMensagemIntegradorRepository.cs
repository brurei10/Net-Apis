﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IMensagemIntegradorRepository 
    {

        Task<MensagemIntegrador> ObterPorGuidAsync(string guid);
        Task<bool> InserirAsync(MensagemIntegrador entity);
        Task<bool> AtualizarAsync(MensagemIntegrador entity);     
        
    }
}
