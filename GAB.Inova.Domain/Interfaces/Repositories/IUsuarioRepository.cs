﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository
    {

        Task<Usuario> ObterPorIdAsync(long idUsuario);
        Task<Usuario> ObterPorNomeAcessoAsync(string nomeAcesso, bool? ativo = null);

    }
}
