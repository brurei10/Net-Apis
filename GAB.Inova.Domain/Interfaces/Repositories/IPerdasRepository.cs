﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IPerdasRepository
    {
      
        Task<IEnumerable<TabMeterwiseHD>> ConsultarHidrometros(DateTime dataInicio, DateTime DataFim);
        Task<IEnumerable<Leitura>> ConsultarLeituras(DateTime dataInicio, DateTime DataFim);
        Task<IEnumerable<LeituraMacro>> ConsultarLeiturasMacro();
        void InserirLog(StatusCodeResult resultado);


    }
}

