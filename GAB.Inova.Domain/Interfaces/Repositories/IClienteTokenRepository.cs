﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IClienteTokenRepository 
    {

        Task<ClienteToken> ObterPorTokenAsync(string token);
        Task<GrupoEntregaEnum> ObterGrupoEntregaClientePorTokenAsync(string token);

        Task<bool> ConfirmarAsync(long id);
        Task<bool> InserirAsync(ClienteToken entity);

        #region [Métodos públicos para validação]

        ValidationResult PodeConfirmar(ClienteToken entity);

        #endregion

    }
}
