﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IClienteCallCenterRepository
    {
        Task<ClienteCallCenter> ObterAsync(string protocolo, string documento, string matricula);
    }
}
