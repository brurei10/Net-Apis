﻿using System;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IFeriadoRepository
    {

        /// <summary>
        /// Obter o próximo dia útil de uma data
        /// </summary>
        /// <param name="data">Indica uma data válida</param>
        /// <param name="qtdeDias">Indica a quantidade de dias úteis</param>
        /// <param name="indice">Indica um identador de dias (soma)</param>
        /// <returns>o próximo dia útil</returns>
        Task<DateTime> ObterProximoDiaUtil(DateTime data, int qtdeDias, int indice);

    }
}
