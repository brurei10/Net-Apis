﻿using GAB.Inova.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IPdfGenericoRepository
    {

        Task<PdfGenModeloDoc> ObterDocumentoLayoutAsync(PdfModeloEnum pdfModelo);
        Task<IEnumerable<PdfGenItemDoc>> ObterItensDocumentoLayoutAsync(long idItemDocumento);
        Task<PdfGenItemDocAjustado> ObterItemDocumentoLayoutAjustadoAsync(long idItemDocumento, long idModelo, long idUsuario);

    }
}
