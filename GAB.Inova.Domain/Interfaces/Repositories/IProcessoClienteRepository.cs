﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IProcessoClienteRepository
    {

        Task<ProcessoCliente> VerificarClienteSobJudice(string matricula);

    }
}
