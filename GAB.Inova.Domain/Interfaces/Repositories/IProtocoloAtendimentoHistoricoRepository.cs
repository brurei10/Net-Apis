﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IProtocoloAtendimentoHistoricoRepository
    {

        Task<bool> InserirAsync(ProtocoloAtendimentoHistorico entity);

    }
}
