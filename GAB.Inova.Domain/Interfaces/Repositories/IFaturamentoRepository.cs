﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IFaturamentoRepository
    {

        Task<IEnumerable<Faturamento>> ObterFaturamentoParaImpressaoContaAsync(Faturamento faturamento);

    }
}
