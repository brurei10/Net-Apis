﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IImprimeGRPRepository
    {
        Task<bool> InserirAsync(ImprimeGRP entity);
        Task<ImprimeGRP> ObterPorSeqOriginal(long seqOriginal);
    }
}
