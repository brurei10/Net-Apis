﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IVwDadosClienteInterrupcaoRepository
    {
        Task<VwDadosClienteInterrupcao> ObterAsync(string matricula);
    }
}
