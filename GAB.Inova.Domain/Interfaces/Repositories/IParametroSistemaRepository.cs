﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IParametroSistemaRepository
    {

        Task<ParametroSistema> ObterAsync(long id);

    }
}
