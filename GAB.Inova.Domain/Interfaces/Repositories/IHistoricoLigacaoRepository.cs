﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IHistoricoLigacaoRepository
    {

        Task<HistoricoLigacao> ObterPorMatriculaAsync(string matricula);
        Task<HistoricoLigacao> ObterPorMatriculaEReferenciaAsync(string matricula, string anoMes);

    }
}
