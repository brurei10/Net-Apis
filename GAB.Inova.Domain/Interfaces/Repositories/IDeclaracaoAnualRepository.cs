﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IDeclaracaoAnualRepository
    {
        Task<IEnumerable<DeclaracaoAnual>> ObterDeclaracoesAnuaisAsync(long IdContrato);

        Task<DeclaracaoAnual> ObterAsync(long seqDeclaracaoAnual);

        Task<bool> AtualizarImpressaoAsync(long seqDeclaracaoAnual);
    }
}
