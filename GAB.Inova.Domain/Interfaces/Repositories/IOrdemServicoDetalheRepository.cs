﻿using GAB.Inova.Common.Enums;
using DomainValidationCore.Validation;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IOrdemServicoDetalheRepository
    {
        bool AlterarStatus(long NumeroOS, StatusDetOrdemServicoEnum novoStatus);
        Task<bool> AlterarStatusAsync(long NumeroOS, StatusDetOrdemServicoEnum novoStatus);
        OrdemServicoDetalhe Obter(long NumeroOS);
        Task<OrdemServicoDetalhe> ObterAsync(long NumeroOS);
        ValidationResult PodeAlterarStatusParaAguardandoApropriacao(OrdemServicoDetalhe entity);
    }
}
