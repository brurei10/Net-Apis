﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ITempCobrancaCtRepository
    {
        Task<bool> InserirAsync(TempCobrancaCt entity);
    }
}
