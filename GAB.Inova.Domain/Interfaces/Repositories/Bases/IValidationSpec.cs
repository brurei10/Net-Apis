﻿using DomainValidationCore.Validation;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories.Bases
{
    public interface IValidationSpec<T> where T : class
    {

        ValidationResult PodeAlterar(T entity);

        ValidationResult PodeExcluir(T entity);

        ValidationResult PodeIncluir(T entity);

    }
}
