﻿using System;

namespace GAB.Inova.Domain.Interfaces.Repositories.Bases
{
    public interface IUnitOfWork
        : IDisposable
    {

        //I GetSession<I>();
        void BeginTransaction();
        void Commit();
        void Rollback();

    }
}
