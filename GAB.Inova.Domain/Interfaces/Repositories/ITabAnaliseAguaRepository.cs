﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ITabAnaliseAguaRepository
    {

        Task<IEnumerable<TabAnaliseAgua>> ObterAnaliseAguaParaImpressaoContaAsync(long seqOriginal, long idUsuario);

    }
}
