﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IEmpresaRepository
    {

        Task<Empresa> ObterAsync();

    }
}
