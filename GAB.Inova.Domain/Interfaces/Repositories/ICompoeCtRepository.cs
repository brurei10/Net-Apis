﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ICompoeCtRepository
    {
        Task<bool> InserirAsync(CompoeCt entity);
    }
}
