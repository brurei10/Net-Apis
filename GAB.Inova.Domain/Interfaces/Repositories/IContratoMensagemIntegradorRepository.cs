﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IContratoMensagemIntegradorRepository
    {

        Task<bool> InserirAsync(ContratoMensagemIntegrador entity);

    }
}
