﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IProtocoloAtendimentoRepository 
        : IValidationSpec<ProtocoloAtendimento>
    {
        
        Task<ProtocoloAtendimento> ObterPorIdAsync(long idProtocoloAtendimento);
        Task<ProtocoloAtendimento> ObterPorNumeroAsync(string numeroProtocolo);

        Task<IEnumerable<ProtocoloAtendimento>> ListarProtocolosAbertosPorNumeroESenhaGfaAsync(string numeroProtocoloGfa, string senhaGfa);
        Task<IEnumerable<ProtocoloAtendimento>> ListarProtocolosNaoIntegradosPorNumeroESenhaGfaAsync(string numeroProtocoloGfa, string senhaGfa);
        Task<ProtocoloAtendimento> ObterProtocoloNaoFinalizadoPorUsuarioAsync(long idUsuario);

        Task<IEnumerable<TabNaturezaSolicitacao>> ObterNaturezasSolicitacaoConsolidadoAsync(string numeroProtocoloPai);

        Task<bool> VerificarCadastramentoDeNaturezaAsync(long idProtocoloAtendimento);
        
        Task<bool> AtualizarIntegracaoGfaAsync(ProtocoloAtendimento protocoloAtendimento);
        Task<bool> AtualizarDadosClienteAsync(ProtocoloAtendimento protocoloAtendimento);
        Task<bool> AtualizarDataIntegracaoAsync(string numeroProtocoloPai);
        Task<bool> FinalizarAsync(ProtocoloAtendimento protocoloAtendimento);
        Task<Atendimento> FinalizarAsync(string protocolo, string resolucao);
        Task<string> GerarAsync(ProtocoloAtendimento protocoloAtendimento);
        Task AssociarLigacaoAsync(string protocolo, string matricula);

        #region [Métodos para validação]

        ValidationResult NumeroProtocoloValidar(string numeroProtocolo);
        ValidationResult PodeFinalizar(ProtocoloAtendimento entity);

        #endregion

    }
}
