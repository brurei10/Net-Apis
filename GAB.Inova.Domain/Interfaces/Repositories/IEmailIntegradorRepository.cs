﻿using GAB.Inova.Common.ViewModels.SendGrid;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IEmailIntegradorRepository
    {
        Task<EmailIntegradorResponse> EnviarEmailAsync(EmailRequestViewModel model);
    }
}
