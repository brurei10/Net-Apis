﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IInformacaoContaRepository
    {
        Task<InformacaoConta> ObterAsync(string protocolo, string matricula);
    }
}
