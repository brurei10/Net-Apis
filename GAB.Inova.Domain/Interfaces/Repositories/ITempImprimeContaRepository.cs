﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface ITempImprimeContaRepository
    {

        Task<bool> DeletarTempSeqAsync(TempImprimeConta entity);
        Task<bool> InserirTempSeqAsync(TempImprimeConta entity);
        Task<bool> GerarTempContaAsync(TempImprimeConta entity);

    }
}
