﻿using DomainValidationCore.Validation;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IClienteRepository 
    {

        Task<Cliente> ObterPorMatriculaAsync(string matricula);
        Task<DateTime> ObterDataProximaLeituraAsync(string matricula);
        Task<Cliente> ObterPorMatriculaECpfCnpjAsync(string matricula, string cpfCnpj);

        Task<bool> AtualizaGrupoEntregaAsync(string matricula, long idGrupoEntrega);
        
        #region [Métodos públicos para validação]

        ValidationResult PodeAderirContaDigital(Cliente entity);

        ValidationResult PodeAlterarContaDigital(Cliente entity);

        ValidationResult PodeCancelarContaDigital(Cliente entity);

        #endregion

    }
}
