﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IContaDigitalEnvioRepository
    {

        Task<bool> InserirAsync(ContaDigitalEnvio entity);
        Task<bool> AtualizarAsync(ContaDigitalEnvio entity);
        Task<ContaDigitalEnvio> ObterPorIdMensagemEnvioAsync(long idMensagemEnvio);


    }
}
