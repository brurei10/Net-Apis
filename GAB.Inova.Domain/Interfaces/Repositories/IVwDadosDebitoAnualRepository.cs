﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IVwDadosDebitoAnualRepository
    {
        Task<IEnumerable<VwDadosDebitoAnual>> ObterAsync(long seqDeclaracaoAnual);
    }
}
