﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IContaDigitalEnvioMovimentoRepository
    {

        Task<bool> InserirAsync(ContaDigitalEnvioMovimento entity);

    }
}
