﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IProtocoloAtendimentoDetalheRepository
    {

        Task<bool> VerificarExistenciaDeNaturezaAsync(long idProtocoloAtendimento, int idNatureza);
        Task<bool> InserirAsync(ProtocoloAtendimentoDetalhe entity);

    }
}
