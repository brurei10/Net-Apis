﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IClienteContratoRepository
    {
        Task<ClienteContrato> ObterAsync(string documento, string matricula);
    }
}
