﻿using DomainValidationCore.Validation;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IBairroRepository
    {
        bool VerificarExistencia(Bairro bairro);

        #region [Métodos públicos para validação]

        ValidationResult BairroValido(Bairro entity);

        #endregion

    }
}
