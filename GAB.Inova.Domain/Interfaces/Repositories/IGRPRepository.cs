﻿using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IGRPRepository
    {
        Task<bool> InserirAsync(GRP entity);
    }
}
