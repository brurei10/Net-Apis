﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Threading.Tasks;

namespace GAB.Inova.Domain.Interfaces.Repositories
{
    public interface IOrdemServicoEletronicaRepository : IValidationSpec<OrdemServicoEletronica>
    {
        OrdemServicoEletronica ObterPorId(long id);
        Task<OrdemServicoEletronica> ObterPorIdAsync(long id);

        void Inserir(OrdemServicoEletronica os);
        Task InserirAsync(OrdemServicoEletronica os);

        void GerarOSComOSEletronicaAssociada(OrdemServicoEletronica os);
        Task GerarOSComOSEletronicaAssociadaAsync(OrdemServicoEletronica os);

        bool VerificarExistencia(OrdemServicoEletronica os);

        Task<bool> VerificarExistenciaAsync(OrdemServicoEletronica os);

        bool VerificarCodigoServico(OrdemServicoEletronica os);

        Task<bool> VerificarCodigoServicoAsync(OrdemServicoEletronica os);

        bool VerificarEquipeExecutora(OrdemServicoEletronica os);

        Task<bool> VerificarEquipeExecutoraAsync(OrdemServicoEletronica os);

        bool VerificarTipoLogradouro(OrdemServicoEletronica os);

        Task<bool> VerificarTipoLogradouroAsync(OrdemServicoEletronica os);

        bool VerificarExistenciaOrdemServicoEletronicaAssociada(OrdemServicoEletronica os);

        Task<bool> VerificarExistenciaOrdemServicoEletronicaAssociadaAsync(OrdemServicoEletronica os);
    }
}
