﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class GrupoEntrega
        : EntityBase<long>
    {

        public virtual string Descricao { get; set; }
        public virtual int ImprimeLis { get; set; }
        public virtual int Situacao { get; set; }

    }
}
