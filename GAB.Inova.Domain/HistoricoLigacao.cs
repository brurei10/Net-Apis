﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;
using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Domain
{
    public class HistoricoLigacao
        : EntityBase<long>
    {

        public DateTime DataHistorico { get; set; }
        public string Matricula { get; set; }
        public int IdCiclo { get; set; }
        public virtual short SituacaoAgua { get; set; }
        public virtual short SituacaoEsgoto { get; set; }
        public virtual short SituacaoHd { get; set; }
        public virtual string AnoMes { get; set; }
        public virtual GrupoEntregaEnum IdGrupoEntrega { get; set; }
        public virtual bool DebitoAutomatico { get; set; }

        public virtual Contrato Contrato { get; set; }

        #region [Propriedade para validações]

        public virtual ValidationResult ValidationResult { get; set; }

        #endregion

    }
}
