﻿namespace GAB.Inova.Domain
{
    public class VwCliente
    {
        public string Matricula { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string NumeroImovel { get; set; }
        public string Complemento { get; set; }
        public string NomeBairro { get; set; }
        public string NomeCidade { get; set; }
        public string NomeLocal { get; set; }
        public int EconomiaResidencial { get; set; }
        public int EconomiaComercial { get; set; }
        public int EconomiaIndustrial { get; set; }
        public int EconomiaPublica { get; set; }
        public int IdCiclo { get; set; }
        public string IdSetor { get; set; }
        public string Rota { get; set; }
        public string Sequencia { get; set; }
        public string IdHd { get; set; }
        public int TipoEntrega { get; set; }
        public string Documento { get; set; }
    }
}
