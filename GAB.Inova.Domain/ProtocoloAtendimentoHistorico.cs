﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class ProtocoloAtendimentoHistorico
        : EntityBase<long>
    {
        
        public virtual long IdProtocoloAtendimento { get; set; }
        public virtual long IdUsuario { get; set; }
        public virtual long? IdModulo { get; set; }
        public virtual string Historico { get; set; }
        public virtual string Matricula { get; set; }
        public virtual int? TipoProtocolo { get; set; }
        public virtual int? Tempo { get; set; }
        public virtual int? NivelSatisfacao { get; set; }
        public virtual string Localidade { get; set; }
        public virtual LocalAtendimentoTipoEnum IdLocalAtendimento { get; set; }
        public virtual CanalAtendimentoEnum IdTipoAtendimento { get; set; }

    }
}
