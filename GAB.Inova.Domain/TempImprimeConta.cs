﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class TempImprimeConta
        : EntityBase<long>
    {

        public long SeqOriginal { get; set; }
        public string Matricula { get; set; }
        public string NomeCliente { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string AnoMesCt { get; set; }
        public DateTime? DataEmissao { get; set; }
        public DateTime? DataVencimento { get; set; }
        public string IdHd { get; set; }
        public long? LeituraAtual { get; set; }
        public long? LeituraAnterior { get; set; }
        public DateTime? DataLeituraAtual { get; set; }
        public DateTime? DataLeituraAnterior { get; set; }
        public DateTime? DataProximaLeitura { get; set; }
        public string NomeTipoEntrega { get; set; }
        public string CpfCnpj { get; set; }
        public string InscricaoMunicipalEstadual { get; set; }
        public int? EconomiaResidencial { get; set; }
        public int? EconomiaComercial { get; set; }
        public int? EconomiaIndustrial { get; set; }
        public int? EconomiaPublica { get; set; }
        public int? Economia { get; set; }
        public int? QtdeDiasConsumo { get; set; }
        public int? ConsumoMedido { get; set; }
        public int? ConsumoCredito { get; set; }
        public int? ConsumoPipa { get; set; }
        public int? ConsumoResidual { get; set; }
        public int? ConsumoFaturado { get; set; }
        public string NomeTipoFaturamento { get; set; }
        public string CompoeCt01 { get; set; }
        public string CompoeCt02 { get; set; }
        public string CompoeCt03 { get; set; }
        public string CompoeCt04 { get; set; }
        public string CompoeCt05 { get; set; }
        public string CompoeCt06 { get; set; }
        public string CompoeCt07 { get; set; }
        public string CompoeCt08 { get; set; }
        public string CompoeCt09 { get; set; }
        public string CompoeCt10 { get; set; }
        public string CompoeCt11 { get; set; }
        public string CompoeCt12 { get; set; }
        public string CompoeCt13 { get; set; }
        public string CompoeCt14 { get; set; }
        public string CompoeCt15 { get; set; }
        public string CompoeCt16 { get; set; }
        public double? CompoeVl01 { get; set; }
        public double? CompoeVl02 { get; set; }
        public double? CompoeVl03 { get; set; }
        public double? CompoeVl04 { get; set; }
        public double? CompoeVl05 { get; set; }
        public double? CompoeVl06 { get; set; }
        public double? CompoeVl07 { get; set; }
        public double? CompoeVl08 { get; set; }
        public double? CompoeVl09 { get; set; }
        public double? CompoeVl10 { get; set; }
        public double? CompoeVl11 { get; set; }
        public double? CompoeVl12 { get; set; }
        public double? CompoeVl13 { get; set; }
        public double? CompoeVl14 { get; set; }
        public double? CompoeVl15 { get; set; }
        public double? CompoeVl16 { get; set; }
        public double? ValorTotal { get; set; }
        public double? ValorRetecaoImposto { get; set; }
        public string Mensagem { get; set; }
        public string Observacao { get; set; }
        public string CodigoBarra { get; set; }
        public string NomeUsuario { get; set; }
        public int? ImpressaoCodigoBarra { get; set; }
        public long? IdSolicitacao { get; set; }
        public long? IdUsuario { get; set; }
        public int? SituacaoHd { get; set; }
        public int? IdTarifa { get; set; }
        public string Roteirizacao { get; set; }
        public string SistemaAbastecimento { get; set; }
        public int? TipoImpressao { get; set; }
        public string DigitoMatricula { get; set; }
        public int? SituacaoEsgoto { get; set; }
        public MotivoSegundaViaEnum IdMotivoImpressao { get; set; }
        public string Rota { get; set; }
        public double? ValorRecursosHidricos { get; set; }
        public int? ValorEsgotoAlternativo { get; set; }
        public long? IdSistemaAbastecimento { get; set; }
        public decimal? PercentualAgua { get; set; }
        public decimal? PercentualEsgoto { get; set; }
        public double? ValorAgua { get; set; }
        //criado para atender NFA que nao necessitam de impressao dos dados de consumo
        public int? ImpressaoConsumo { get; set; }
        public string ModoFaturamento { get; set; }
        public short? Via { get; set; }

    }
}
