﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Logradouro
        : EntityBase<long>
    {

        public virtual string Nome { get; set; }
        public virtual string Cep { get; set; }

        public virtual Bairro Bairro { get; set; }
        public virtual Cidade Cidade { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual LogradouroTipo LogradouroTipo { get; set; }

    }
}
