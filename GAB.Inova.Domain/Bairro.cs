﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Bairro
        : EntityBase<long>
    {
        public virtual string Nome { get; set; }
    }
}
