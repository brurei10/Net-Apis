﻿using System;

namespace GAB.Inova.Domain
{
    public class InformacaoConta
    {
        public int sitConta { get; set; }
        public DateTime? dataVencimento { get; set; }
        public decimal? valor { get; set; }
    }
}
