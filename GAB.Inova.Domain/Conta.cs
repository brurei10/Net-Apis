﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class Conta
        : EntityBase<long>
    {

        public virtual TipoContaEnum TipoConta { get; set; }
        public virtual string Matricula { get; set; }
        public virtual string AnoMes { get; set; }
        public virtual SituacaoContaEnum SituacaoConta { get; set; }
        public virtual DateTime Vencimento { get; set; }
        public virtual decimal ValorAgua { get; set; }
        public virtual decimal ValorEsgoto { get; set; }
        public virtual decimal ValorServico { get; set; }
        public virtual decimal ValorMulta { get; set; }
        public virtual decimal ValorIcfrf { get; set; }
        public virtual decimal ValorDesconto { get; set; }
        public virtual decimal ValorJuro { get; set; }
        public virtual decimal ValorDevolucao { get; set; }
        public virtual decimal ValorTotal { get; set; }
        public virtual DateTime Emissao { get; set; }
        public virtual long IdContrato { get; set; }
        public virtual decimal ValorRecursosHidricosAgua { get; set; }
        public virtual decimal ValorRecursosHidricosEsgoto { get; set; }
        public virtual decimal ValorDescontoResidencialSocial { get; set; }
        public virtual decimal ValorDescontoPequenoComercio { get; set; }
        public virtual decimal ValorEsgotoAlternativo { get; set; }
        public virtual decimal ValorDescontoLigacaoEstimada { get; set; }
        public virtual decimal ValorDescontoRetificado { get; set; }
        public virtual decimal ValorDescontoPoderPublicoConcedente { get; set; }
        public virtual short SituacaoNegativacao { get; set; }
        public virtual string Localizacao { get; set; }
        public virtual int IdCiclo { get; set; }
        public virtual int TipoEntrega { get; set; }
        public virtual int IdCentralizador { get; set; }
        public virtual int QuantidadeDebito { get; set; }
        public virtual int QuantidadePagamento { get; set; }
        public virtual int IdMensagemMes { get; set; }
        public virtual int IdAviso { get; set; }
        public virtual int PagamentoPrazo { get; set; }
        public virtual int PagamentoFora { get; set; }
        public virtual decimal ValorComercial { get; set; }
        public virtual decimal ValorICMS { get; set; }
        public virtual decimal ValorTerceiro { get; set; }
        public virtual decimal ValorCorrecaoMonetaria { get; set; }
    }
}
