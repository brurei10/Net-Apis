﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Parcela : EntityBase<long>
    {
        public string Matricula { get; set; }
        public int SituacaoParcela { get; set; }
        public int IdContrato { get; set; }
        public decimal ValorParcela { get; set; }
    }
}
