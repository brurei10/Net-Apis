﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Domain
{
    public class MensagemIntegrador
        : EntityBase<long>
    {
        public MensagemIntegrador()
        {
            DataCriacao = DateTime.Now;
            Status = MensagemIntegradoStatusEnum.Criado;
            Movimentos = new HashSet<MensagemIntegradorMovimento>();
            GuidMensagem = Guid.NewGuid().ToString();
        }

        public virtual string GuidMensagem { get; set; }
        public virtual string TemplateId { get; set; }
        public virtual string Conteudo { get; set; }
        public virtual DateTime DataCriacao { get; set; }
        public virtual MensagemIntegradorTipoEnum Tipo { get; set; }
        public virtual IntegradorTipoEnum Integrador { get; set; }
        public virtual MensagemIntegradoStatusEnum Status { get; set; }
        public virtual string Destinatarios { get; set; }
        public virtual string DestinatariosCopia { get; set; }
        public virtual string DestinatariosCopiaOculta { get; set; }
        public virtual string De { get; set; }

        public virtual ICollection<MensagemIntegradorMovimento> Movimentos { get; set; }
        
    }
}
