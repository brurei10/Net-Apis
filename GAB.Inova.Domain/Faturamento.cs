﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Faturamento
        : EntityBase<long>
    {

        public string AnoMesFaturamento { get; set; }
        public string Matricula { get; set; }
        public int Categoria { get; set; }
        public long SeqOriginal { get; set; }
        public int TipoServicoFaturado { get; set; }
        public double ValorFaturamento { get; set; }
        public double VolumePorEconomiaFaturado { get; set; }
        public double VolumeTotalFaturado { get; set; }

    }
}
