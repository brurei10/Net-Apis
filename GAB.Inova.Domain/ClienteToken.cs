﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ClienteToken
        : EntityBase<long>
    {
        public ClienteToken()
        {
            DataInclusao = DateTime.Now;
        }

        public virtual string Matricula { get; set; }      
        public virtual string Token { get; set; }
        public virtual TokenTipoEnum Tipo { get; set; }
        public virtual SistemaEnum Sistema { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual DateTime? DataExpiracao { get; set; }
        public virtual DateTime? DataConfirmacao { get; set; }
        public virtual long? IdProtocoloAtendimento { get; set; }

        #region [Propriedade para validações]

        public virtual ValidationResult ValidationResult { get; set; }

        #endregion
        
    }
}
