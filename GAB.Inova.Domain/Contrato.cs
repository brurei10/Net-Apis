﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class Contrato
        : EntityBase<long>
    {

        public virtual string CpfCnpj { get; set; }
        public virtual string Matricula { get; set; }
        public virtual string NomeCliente { get; set; }
        public virtual string TelefoneResidencial { get; set; }
        public virtual string TelefoneComercial { get; set; }
        public virtual string Celular { get; set; }
        public virtual string Email { get; set; }
        public virtual string EmailContaDigital { get; set; }
        public virtual string Numero { get; set; }
        public virtual string Complemento { get; set; }
        public virtual string PontoReferencia { get; set; }
        
        public virtual Logradouro Logradouro { get; set; }

        public virtual string CpfCnpjFormatado
        {
            get
            {
                switch (CpfCnpj.Length)
                {
                    case 11:
                        return long.Parse(CpfCnpj).ToString(@"000\.000\.000\-00");
                    case 14:
                        return long.Parse(CpfCnpj).ToString(@"00\.000\.000\/0000\-00");
                    default:
                        return CpfCnpj;
                }
            }
        }

        public virtual string TelefoneResidencialFormatado
        {
            get
            {
                var tel = TelefoneResidencial?.Trim();
                if (tel?.Length == 10)
                {
                    return $"({tel.Remove(2)}) {tel.Substring(2, 8).Insert(4, "-")}";
                }
                else if (tel?.Length == 11)
                {
                    return $"({tel.Remove(2)}) {tel.Substring(2, 9).Insert(5, "-")}";
                }

                return TelefoneResidencial;
            }
        }

        public virtual string TelefoneComercialFormatado
        {
            get
            {
                var tel = TelefoneComercial?.Trim();
                if (tel?.Length == 10)
                {
                    return $"({tel.Remove(2)}) {tel.Substring(2, 8).Insert(4, "-")}";
                }
                else if (tel?.Length == 11)
                {
                    return $"({tel.Remove(2)}) {tel.Substring(2, 9).Insert(5, "-")}";
                }

                return TelefoneComercial;
            }
        }

        public virtual string CelularFormatado
        {
            get
            {
                if (Celular?.Length == 11)
                {
                    return $"({Celular.Remove(2)}) {Celular.Substring(2, 9).Insert(5, "-")}";
                }

                return Celular;
            }
        }

        public virtual string EnderecoCompleto
        {
            get
            {
                var logradouro = Logradouro == null ? string.Empty : Logradouro.Nome;
                var numero = string.IsNullOrEmpty(Numero) ? string.Empty : $", {Numero}";
                var complemento = string.IsNullOrEmpty(Complemento) ? string.Empty : $" - {Complemento}";
                var bairro = Logradouro == null ? string.Empty : Logradouro.Bairro == null ? string.Empty : $"- {Logradouro.Bairro.Nome}";
                var cidade = Logradouro == null ? string.Empty : Logradouro.Cidade == null ? string.Empty : $"- {Logradouro.Cidade.Nome}";

                return $"{logradouro}{numero}{complemento}{bairro}{cidade}";
            }
        }
        
    }
}
