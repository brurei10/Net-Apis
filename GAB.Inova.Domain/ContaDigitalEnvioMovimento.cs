﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ContaDigitalEnvioMovimento
        : EntityBase<long>
    {

        public virtual long IdContaDigitalEnvio { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual ContaDigitalEnvioStatusEnum Status { get; set; }
        public virtual string Mensagem { get; set; }

        public ContaDigitalEnvioMovimento()
        {
            DataInclusao = DateTime.Now;
            Status = ContaDigitalEnvioStatusEnum.Gerado;
        }

        public ContaDigitalEnvioMovimento(ContaDigitalEnvio contaDigitalEnvio)
        {
            DataInclusao = DateTime.Now;
            Status = contaDigitalEnvio.Status;
            IdContaDigitalEnvio = contaDigitalEnvio.Id;
        }

        public ContaDigitalEnvioMovimento(ContaDigitalEnvio contaDigitalEnvio, string mensagem)
            :this(contaDigitalEnvio)
        {
            Mensagem = mensagem;
        }
    }
}
