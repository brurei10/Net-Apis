﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class ProcessoCliente
        : EntityBase<long>
    {

        /// <summary>
        /// Código Interno de identificação das relações entre os clientes e os processos
        /// </summary>
        public virtual long IdProcesso { get; set; }
        /// <summary>
        /// Indica o número da matricula, código Interno de identificação do Cliente (FK Tabela CLIENTE)
        /// </summary>
        public virtual string Matricula { get; set; }
        /// <summary>
        /// Código de identificação do contrato (FK Tabela CONTRATO)
        /// </summary>
        public virtual long IdContrato { get; set; }

        public virtual Processo Processo { get; set; }

    }
}
