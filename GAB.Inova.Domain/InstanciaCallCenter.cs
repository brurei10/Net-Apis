﻿using System.Text;

namespace GAB.Inova.Domain
{
    public class InstanciaCallCenter
    {
        public string NomeCliente { get; set; }
        public string Documento { get; set; }
        public string Url { get; set; }
        public bool Permitido { get; set; }
        public int? IdControleAutenticacao { get; set; }
        public override string ToString()
        {
            var props = this.GetType().GetProperties();
            var sb = new StringBuilder();
            foreach (var p in props)
            {
                sb.AppendLine(p.Name + ": " + p.GetValue(this, null));
            }
            return sb.ToString();
        }
    }
}
