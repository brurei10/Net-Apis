﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class DocumentoModelo : EntityBase<long>
    {
        public int IdDocumentoModeloTipo { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataInclusao { get; set; }
        public decimal Situacao { get; set; }
        public string HeaderHtml { get; set; }
        public string BodyHtml { get; set; }
        public string FooterHtml { get; set; }
        public string CssHtml { get; set; }
        public decimal HeaderTamanho { get; set; }
        public decimal FooterTamanho { get; set; }
        public decimal DocTamanho { get; set; }
        public decimal DocOrientacao { get; set; }
        public decimal DocMargemSuperior { get; set; }
        public decimal DocMargemInferior { get; set; }
        public decimal DocMargemEsquerda { get; set; }
        public decimal DocMargemDireita { get; set; }
        public string Observacao { get; set; }
        public decimal IdUsuarioCriacao { get; set; }
        public DateTime DataCriacaoRegistro { get; set; }
        public decimal IdUsuarioUltimaModif { get; set; }
        public DateTime DataUltimaModificacao { get; set; }
        public int Origem { get; set; }
    }
}
