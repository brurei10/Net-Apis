﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class ProtocoloAtendimento 
        : EntityBase<long>
    {

        public virtual long IdContrato { get; set; }
        public virtual DateTime DataInicio { get; set; }
        public virtual DateTime? DataFim { get; set; }
        public virtual CanalAtendimentoEnum IdTipoAtendimento { get; set; }
        public virtual string Observacao { get; set; }
        public virtual string Solicitante { get; set; }
        public virtual LocalAtendimentoTipoEnum IdLocalAtendimento { get; set; }
        public virtual string Prexifo { get; set; }
        public virtual long Sequencia { get; set; }
        public virtual string Numero { get { return Prexifo + Sequencia; } }
        public virtual long IdUsuario { get; set; }
        public virtual int TipoProtocolo { get; set; }
        public virtual string Matricula { get; set; }
        public virtual string Telefone { get; set; }
        public virtual string CpfCnpj { get; set; }
        public virtual bool OrigemInova { get; set; }
        public virtual string NumeroProtocoloPai { get; set; }
        public virtual DateTime? DataIntegracao { get; set; }
        public virtual long IdProtocoloAtendimentoGfa { get; set; }

        public virtual Contrato Contrato { get; set; }
        public virtual ProtocoloAtendimentoGfa ProtocoloAtendimentoGfa { get; set; }
        public virtual Usuario Usuario { get; set; }

        #region [Propriedade para validações]

        public virtual ValidationResult ValidationResult { get; set; }

        #endregion

    }
}

