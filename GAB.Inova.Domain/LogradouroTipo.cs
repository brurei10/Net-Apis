﻿using GAB.Inova.Domain.Bases;

namespace GAB.Inova.Domain
{
    public class LogradouroTipo
        : EntityBase<long>
    {

        public virtual string Nome { get; set; }

    }
}
