﻿using GAB.Inova.Domain.Bases;
using System;

namespace GAB.Inova.Domain
{
    public class TempCobrancaCt : EntityBase<long>
    {
        public string Matricula { get; set; }
        public int IdUsuario { get; set; }
        public string AnoMesCt { get; set; }
        public DateTime DataVencimento { get; set; }
        public DateTime DataPagamento { get; set; }
        public int TipoCobranca { get; set; }
        public int Origem { get; set; }
        public double ValorOriginal { get; set; }
        public double IndiceVencimento { get; set; }
        public int SeqOriginalPago { get; set; }
        public double PercentualCorrecao { get; set; }
        public string Localizacao { get; set; }
        public DateTime DataFaturamento { get; set; }
    }
}
