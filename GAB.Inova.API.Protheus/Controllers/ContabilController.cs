﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GAB.Inova.Common.ViewModels.Base;
using ApiFechamento.Data;

namespace Apicontabil.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize("Bearer")]

    public class ContabilController : ControllerBase
    {
        private const string Value = "Produto não encontrado com o id = informado";

        [HttpGet("{codOperacao}")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public IActionResult GetInovaProtheus(string codOperacao)
        {
            if (codOperacao == null || codOperacao.Equals("")) {

                    return BadRequest("Atributo codOperacao obrigatório");
                
            }

            List<Contabil> contabil = new List<Contabil>();
            ContabilRepository dao = new ContabilRepository();
            contabil = dao.findAll(codOperacao);
            
            return Ok(contabil);
        }


        [HttpPut("{codOperacao}")]
        public IActionResult UpdateStatus(string codOperacao, string status)
        {

            if (codOperacao == null || codOperacao.Equals("") || status == null || status.Equals(""))
            {

                return BadRequest("Atributos codOperacao e status obrigatórios");

            }
            else if(int.Parse(status) <2) {
                return BadRequest("Código atualização inválido.");

            }
            else if (int.Parse(status) > 3)
            {
                return BadRequest("Código atualização inválido.");

            }

            Boolean updated = false;

            ContabilRepository dao = new ContabilRepository();
            updated = dao.updateStatus(codOperacao, status);


            return Ok(updated);

        }

    }
}
