﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiFechamento.Data;
using GAB.Inova.Common.ViewModels.Base;


namespace ApiFechamento.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize("Bearer")]

    public class FiscalController : ControllerBase
    {
        private const string Value = "Produto não encontrado com o id = informado";

        [HttpGet("{codOperacao}")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public IActionResult GetInovaProtheus(string codOperacao)
        {
            if (codOperacao == null || codOperacao.Equals(""))
            {

                return BadRequest("Atributo codOperacao obrigatório");

            }
            List<Fiscal> fechamento = new List<Fiscal>();
            FiscalRepository dao = new FiscalRepository();
            fechamento = dao.findAll(codOperacao);
           
            return Ok(fechamento);
        }

        [HttpPut("{codOperacao}")]
        public IActionResult UpdateStatus(string codOperacao, string status)
        {
            if (codOperacao == null || codOperacao.Equals("") || status == null || status.Equals(""))
            {

                return BadRequest("Atributos codOperacao e status obrigatórios");

            }
            else if (int.Parse(status) < 2)
            {
                return BadRequest("Código atualização inválido.");

            }
            else if (int.Parse(status) > 3)
            {
                return BadRequest("Código atualização inválido.");

            }

            Boolean updated = false;
          
                FiscalRepository dao = new FiscalRepository();
                updated = dao.updateStatus(codOperacao, status);
           

            return Ok(updated);
        
        }

    }
}
