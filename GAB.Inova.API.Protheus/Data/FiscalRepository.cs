﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFechamento.Data
{

    public class FiscalRepository
    {
        string connectinString = "Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.0.134)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = DLIS11)));Persist Security Info=True;User ID=NETUNO;Password=N-3-t-u-n-0;Pooling=True;Connection Timeout=60;";

        public List<Fiscal> findAll(string codTransacao)
        {
            List<Fiscal> fiscais = new List<Fiscal>();

            using (var conn = new OracleConnection(connectinString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();

                    cmd.CommandText = "SELECT STATUS, COD_TRANSACAO," +
                        "COD_RELATORIO,EMP_INOVA,EMP_PROTHEUS,RAZAO_SOCIAL," +
                        "CNPJ,INSC_ESTADUAL,ENDERECO,UF,COD_MUN,MUNICIPIO," +
                        "BAIRRO,CEP,TEL,EMAIL,CAT_RETENCAO,NOTA_FISCAL," +
                        "DATA_EMISSAO,DATA_BAIXA,VALOR,VALOR_PIS," +
                        "VALOR_COFINS,VALOR_CSLL,VALOR_IRPJ" 
                                                + " FROM INT_INV_PROT_FISCAL " +
                                                "WHERE STATUS = '1' OR STATUS = '3' AND COD_TRANSACAO = '" + codTransacao + "' " +
                                                "ORDER BY EMP_PROTHEUS,COD_RELATORIO";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Fiscal fiscal = new Fiscal();
                        fiscal.STATUS = (string)(reader["STATUS"]);
                        fiscal.COD_TRANSACAO = (string)reader["COD_TRANSACAO"];
                        fiscal.COD_RELATORIO = (string)reader["COD_RELATORIO"];
                        fiscal.EMP_INOVA = (string)reader["EMP_INOVA"];
                        fiscal.EMP_PROTHEUS = (string)reader["EMP_PROTHEUS"];
                        fiscal.RAZAO_SOCIAL = (string)reader["RAZAO_SOCIAL"];
                        fiscal.CNPJ = (string)reader["CNPJ"];
                        fiscal.INSC_ESTADUAL = (string)reader["INSC_ESTADUAL"];
                        fiscal.ENDERECO = (string)reader["ENDERECO"];
                        fiscal.UF = (string)reader["UF"];
                        fiscal.COD_MUN = (string)reader["COD_MUN"];
                        fiscal.MUNICIPIO = (string)reader["MUNICIPIO"];
                        fiscal.BAIRRO = (string)reader["BAIRRO"];
                        fiscal.CEP = (string)reader["CEP"];
                        fiscal.TEL = (string)reader["TEL"];
                        fiscal.EMAIL = (string)reader["EMAIL"];
                        fiscal.CAT_RETENCAO = (string)reader["CAT_RETENCAO"];
                        fiscal.NOTA_FISCAL = (string)reader["NOTA_FISCAL"];
                        fiscal.DATA_EMISSAO = (string)reader["DATA_EMISSAO"];
                        fiscal.DATA_BAIXA = (string)reader["DATA_BAIXA"];
                        fiscal.VALOR = float.Parse((string)reader["VALOR"]);
                        fiscal.VALOR_PIS = float.Parse((string)reader["VALOR_PIS"]);
                        fiscal.VALOR_COFINS = float.Parse((string)reader["VALOR_COFINS"]);
                        fiscal.VALOR_CSLL = float.Parse((string)reader["VALOR_CSLL"]);
                        fiscal.VALOR_IRPJ = float.Parse((string)reader["VALOR_IRPJ"]);

                        fiscais.Add(fiscal);
                        Console.WriteLine("===========================");

                        Console.WriteLine($"STATUS: {reader["STATUS"]}");
                    }

                    Console.WriteLine("===========================");

                    reader.Dispose();

                }
                return fiscais;
            }

        }

        public Boolean updateStatus(string codTransacao, string status)
        {

            Boolean updated = false;

            using (var conn = new OracleConnection(connectinString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();

                    cmd.CommandText = "UPDATE INT_INV_PROT_FISCAL SET STATUS ='" + status + "' WHERE COD_TRANSACAO = '" + codTransacao + "'";

                    var reader = cmd.ExecuteReader();


                    reader.Dispose();
                    updated = true;
                }
                return updated;
            }

        }
    }
}
