﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFechamento.Data
{
   
    public class ContabilRepository
    {
        string connectinString = "Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.0.134)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = DLIS11)));Persist Security Info=True;User ID=NETUNO;Password=N-3-t-u-n-0;Pooling=True;Connection Timeout=60;";

        public List<Contabil> findAll(string codTransacao)
        {
            List <Contabil> contabeis = new List<Contabil>();

            using (var conn = new OracleConnection(connectinString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();

                     cmd.CommandText = "select STATUS," +
                        "COD_TRANSACAO,COD_RELATORIO,EMP_INOVA,EMP_PROTHEUS," +
                        "RUBRICAS,CDATA,CONTA_CONTABIL,DC,CENTRO_CUSTO,VALOR,HISTORICO," +
                        "LOTE " +
                        "from INT_INV_PROT_CONTAB " +
                        "WHERE STATUS = '1' OR STATUS = '3' AND COD_TRANSACAO = '"+codTransacao+"'" +
                        "ORDER BY EMP_PROTHEUS,CDATA,COD_RELATORIO,DC,CENTRO_CUSTO,RUBRICAS";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Contabil contabil = new Contabil();
                        contabil.STATUS = (string)(reader["STATUS"]);
                        contabil.COD_TRANSACAO = (string)reader["COD_TRANSACAO"];
                        contabil.COD_RELATORIO = (string)reader["COD_RELATORIO"];
                        contabil.EMP_INOVA = (string)reader["EMP_INOVA"];
                        contabil.EMP_PROTHEUS = (string)reader["EMP_PROTHEUS"];
                        contabil.RUBIRCAS = (string)reader["RUBRICAS"];
                        contabil.CDATA = (string)reader["CDATA"];
                        contabil.CONTA_CONTABIL = (string)reader["CONTA_CONTABIL"];
                        contabil.DC = (string)reader["DC"];
                        if (reader["CENTRO_CUSTO"] != DBNull.Value)
                        {
                            contabil.CENTRO_CUSTO = (string)reader["CENTRO_CUSTO"];

                        }
                        contabil.VALOR = float.Parse((string)reader["VALOR"].ToString());
                        contabil.HISTORICO = (string)reader["HISTORICO"];
                        contabil.LOTE = (string)reader["LOTE"];


                        contabeis.Add(contabil);
                        Console.WriteLine("===========================");

                        Console.WriteLine($"STATUS: {reader["STATUS"]}");
                    }

                    Console.WriteLine("===========================");

                    reader.Dispose();

                }
                return contabeis;
            }

        }

        public Boolean updateStatus(string codTransacao, string status)
        {

            Boolean updated = false;

            using (var conn = new OracleConnection(connectinString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();          
               
                    cmd.CommandText = "UPDATE INT_INV_PROT_CONTAB SET STATUS ='" + status+"' WHERE COD_TRANSACAO = '"+codTransacao+"'";

                    var reader = cmd.ExecuteReader();
                                      

                    reader.Dispose();
                    updated = true;
                }
                return updated;
            }

        }
    }
}
