﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFechamento.Data
{
    public class Contabil
    {

        public string STATUS { get; set; }
        public string COD_TRANSACAO { get; set; }
        public string COD_RELATORIO { get; set; }
        public string EMP_INOVA { get; set; }
        public string EMP_PROTHEUS { get; set; }
        public string RUBIRCAS { get; set; }
        public string CDATA { get; set; }
        public string CONTA_CONTABIL { get; set; }
        public string DC { get; set; }
        public string CENTRO_CUSTO { get; set; }
        public float VALOR { get; set; }
        public string HISTORICO { get; set; }
        public string LOTE { get; set; }



    }
}
