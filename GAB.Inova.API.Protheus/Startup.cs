using ApiFechamento.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Oracle.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using GAB.Inova.IoC;


namespace ApiFechamento
{
    public class Startup

    {

        const string CORS_POLICY_NAME = "AllowCors";

        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _webHostEnvironment;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           string connectinString = "Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.0.134)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = DLIS11)));Persist Security Info=True;User ID=NETUNO;Password=N-3-t-u-n-0;Pooling=True;Connection Timeout=60;";
            Console.WriteLine("Hello Oracle!");
            services
              .ConfigureProtheusServices();

            using (var conn = new OracleConnection(connectinString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();

                    cmd.CommandText = "select * FROM INT_INV_PROT_CONTAB ";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Console.WriteLine("===========================");
                        Console.WriteLine($"STATUS: {reader["STATUS"]}");
                    }

                    Console.WriteLine("===========================");

                    reader.Dispose();
                }
             
            }

            services.AddControllers();
         
            services
              .AddCors(o => o.AddPolicy(CORS_POLICY_NAME,
                  builder =>
                  {
                      builder
                          .AllowAnyOrigin()
                          .AllowAnyMethod()
                          .AllowAnyHeader();
                  }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inova Protheus API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors(CORS_POLICY_NAME);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
