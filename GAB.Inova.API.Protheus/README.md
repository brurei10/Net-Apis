# API GAB Inova Protheus


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

[![Build Status](https://drive.google.com/file/d/16In6zKKjOSzujdUyEMZaeRkJv6_5wxJk/view?usp=sharing)](https://travis-ci.org/joemccann/dillinger)

Conforme necessidade da empresa, hoje, existe uma rotina de fechamento que � executada uma vez ao m�s pelo sistema Protheus, o qual envia via DbLink esta solicita��o a uma tabela com as empresas a serem fechadas no sistema INOVA. Ap�s a consulta destas informa��es, s�o executadas rotinas dentro do Protheus para validar e organizar estes processos.
Hoje, conforme solicitado, estas execu��es devem ser feitas atrav�s de uma Api, a qual, ser� constru�da inicialmente para o Protheus mas que, futuramente, poder� ser escalada de acordo com a necessidade da empresa.

# New Features!
Autentica��o da Api via servi�o de gera��o de Token existente nas Apis Inova;
Ao iniciar o processo, o requisitante deve acionar o payload de gera��o de token passando as credenciais com atributo nomeEmpresa. Ao gerar o token, o mesmo ser� utilizado para tr�nsito nas demais requests do sistema. Hoje existem dois m�dulos com endpoints equiparados:
- Fiscal;
- Cont�bil;

#### EndPoint GET para solicita��o dos dados da tabela INOVA:
Este endpoint � respons�vel por buscar as informa��es na tabela Inova de acordo com o c�digo operacao passado pelo solicitante filtrando em sua consulta interna, apenas os status da tabela que estejam com o valor 1(A processar) ou 3(Reprocessamento).
Ap�s a gera��o do Token, o usu�rio poder� solicitar este endpoint, passando a token com a informa��o codOperacao, o qual possui as informa��es que formar�o o filtro condicional na consulta a tabela inova;
O sistema valida se o atributo obrigat�rio codOperacao foi enviado;
Com os dados em m�os, o sistema tenta conectar � tabela inova;
Ao conectar na tabela, o sistema executa a consulta passando como filtro codOperacao e o c�digo de verifica��o do status da opera��o conforme informados abaixo:
- 1 - Informa que o registro est� pronto para ser processado;
- 2 - Informa que o registro j� foi processado;
- 3 - Informa que o registro foi processado com erro (podendo assim ser reprocessado);
Desta forma, a consulta seria mais ou menos da seguinte forma: traga todos os registros que tiverem o codOperacao xxx e o status diferente de 2;
Ao receber as informa��es da consulta, o sistema deve retornar registros para processar, ou retornar vazio. O Fluxo se encerra.

#### EndPoint PUT Para atualiza��o das transa��es
Este endpoint � respons�vel por atualizar o status dos registros na tabela Inova ao ser requisitado. Esta regra serve para que, ao ser realizada uma nova solicita��o de fechamento, os dados j� enviados n�o sejam encaminhados novamente. Para este caso, existem dois par�metros que ser�o enviados:
codOperacao, que ser� a refer�ncia no update para que saibamos qual o bloco de dados ser� atualizado na tabela.
status, atributo respons�vel por passar qual o status a ser atualizado. Os poss�veis status s�o:
- 3 - ERRO: Ao tentar processar, o sistema que consumiu as informa��es n�o conseguiu validar ou persistir os dados. Neste cen�rio, ser� enviado status 3 que, caso estes dados queiram ser processados novamente, ser� poss�vel.
- 2 - PROCESSADO: O processamento dos dados foi realizado com sucesso. Desta forma, ao solicitar um novo, os dados com c�digo 2 n�o ser�o considerados no filtro.
Ao acionar o endpoint passando o status e o codOperacao, o sistema valida os atributos e se os status s�o os permitidos (2 ou 3). Caso contr�rio, o sistema informa dados incorretos.
Ap�s os dados validados, o sistema aciona o banco de dados para validar comunica��o. Caso seja imposs�vel, o mesmo retornar� dizendo que a comunica��o est� indispon�vel, podendo ser acionado novamente.
Ap�s a conex�o com a base, o sistema monta a consulta, passando os par�metros de filtro codTransacao e status para execu��o da atualiza��o.
Ap�s a atualiza��o realizada o sistema devolve verdadeiro para o requisitante (TRUE), caso algum problema aconte�a ele retornar� FALSE, podendo ser solicitada a requisi��o novamente. O Fluxo se encerra.


### Tech

Tecnologias GAB Inova Protheus API
* .NET CORE SDK 5;
* VISUAL STUDIO 2019 COMMUNITY (16.8.2)
* ASP .NET CORE 5.0
* EF CORE 5.0.1
* ORACLE DATABASE
* SQL DEVELOPER
* PROVEDOR POMELO.ENTITYFRAMEWORKCORE.ORACLE
* PROJETO ASP .NET CORE WEB API (ENABLE OPEN API SUPPORT)
* ORACLE.EntityFrameworkCore
* ORACLE.EntityFrameworkCore.Tools
* ORACLE.EntityFrameworkCore.Design
* Scaffolding

### Installation

- Deve-se baixar o projeto do repositorio;
- Deve-se Instalar o .NET CORE SDK 5 na maquina;
- Deve-se atualizar os plugins do NuGet;
- Deve-se efetuar as seguintes etapas de build:

```
$ dotnet restore
$ Iniciar a Api de token GAB.Inova.API.TokenFactory
$ iniciar a Api Protheus GAB.Inova.API.Protheus
```