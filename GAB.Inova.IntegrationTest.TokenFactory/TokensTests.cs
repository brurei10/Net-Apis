using GAB.Inova.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using GAB.Inova.Common.ViewModels.Token;
using System.Collections.Generic;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;

namespace GAB.Inova.IntegrationTest.TokenFactory
{
    public class TokensTests : IClassFixture<TestFixture<Startup>>
    {
        #region [Vari�veis de aplica��o]

        readonly TestFixture<Startup> _fixture;
        readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region [Vari�veis de business]

        readonly ITokenBusiness _tokenBusiness;

        #endregion

        #region [Construtores]

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="fixture">Indica a classe acess�aria</param>
        public TokensTests(TestFixture<Startup> fixture)
        {
            _fixture = fixture;

            #region [Instanciando vari�veis de aplica��o]

            _httpContextAccessor = _fixture?
                .ServiceProvider?
                .GetService<IHttpContextAccessor>();

            #endregion

            #region [Instanciando vari�veis business]

            _tokenBusiness = _fixture?
                .ServiceProvider?
                .GetService<ITokenBusiness>();

            #endregion
        }
        #endregion

        [Fact]
        public async Task TestPostAsync()
        {
            var tokenRequest = new TokenRequestViewModel();

            tokenRequest.Claims.Add(new KeyValuePair<string, string>("CodigoEmpresa", EmpresaTipoEnum.CAA.ToString()));

            var result = await _tokenBusiness.GerarTokenAsync(tokenRequest.Claims, 600);

            Assert.True(result.Sucesso);
        }

        [Theory]
        [InlineData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjM0Zjk0YjY3LTE0YjktNDU2Ny04YzMyLTY2ODVjNDIxYjc3ZSIsIkNvZGlnb0VtcHJlc2EiOiJDQUEiLCJuYmYiOjE1OTM4ODE0ODgsImV4cCI6MTkwOTQxNDI4OCwiaWF0IjoxNTkzODgxNDkwLCJpc3MiOiJodHRwOi8vYXBpLmdydXBvYWd1YXNkb2JyYXNpbC5jb20uYnIiLCJhdWQiOiJJbm92YSJ9.5OKV1PvB3sg_-2yWTWM0e1N7SFzGJR-8fl3aS6ZxAQw")]
        public async Task ValidarAsync(string token)
        {
            var result = await _tokenBusiness.ValidarTokenAsync(token);

            Assert.True(result != null);
        }
    }
}
