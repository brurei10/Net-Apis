﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using System.IO;

namespace GAB.Inova.IntegrationTest.TokenFactory.MockHelper
{
    /// <summary>
    /// Classe de orquestrações falsas
    /// </summary>
    public class ConfigurationMockHelper
    {

        /// <summary>
        /// Obter o ambiente da aplicação (host)
        /// </summary>
        /// <param name="environmentName">Indica o nome do ambiente</param>
        /// <returns><see cref="IWebHostEnvironment"/></returns>
        public static IWebHostEnvironment GetHostingEnvironment(string environmentName = "Tester")
        {
            var hostingEnvironment = new Mock<IWebHostEnvironment>();

            hostingEnvironment
                .Setup(m => m.ContentRootPath)
                .Returns(Directory.GetCurrentDirectory());

            hostingEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns(environmentName);

            return hostingEnvironment.Object;
        }

        /// <summary>
        /// Obter a configurações
        /// </summary>
        /// <param name="hostingEnvironment"><see cref="IWebHostEnvironment"/></param>
        /// <returns><see cref="IConfiguration"/></returns>
        public static IConfiguration GetConfiguration(IWebHostEnvironment hostingEnvironment = null)
        {
            var basePath = hostingEnvironment?.ContentRootPath ?? Directory.GetCurrentDirectory();
            var jsonFile = (hostingEnvironment is null) ? $"appsettings.json" : $"appsettings.{hostingEnvironment.EnvironmentName}.json";

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(jsonFile, optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return configurationBuilder.Build();
        }

        /// <summary>
        /// Obter o contexto
        /// </summary>
        /// <returns><see cref="IHttpContextAccessor"/></returns>
        public static IHttpContextAccessor GetHttpContextAccessor()
        {
            var httpContextAccessor = new Mock<IHttpContextAccessor>();

            httpContextAccessor
                .Setup(s => s.HttpContext)
                .Returns(new DefaultHttpContext());

            return httpContextAccessor.Object;
        }
    }
}