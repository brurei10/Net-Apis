﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Perdas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace GAB.Inova.API.Perdas.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class PerdasController : Controller
    {
        readonly IPerdasBusiness _perdasBusiness;

        public PerdasController(IPerdasBusiness perdasBusiness)
        {
            _perdasBusiness = perdasBusiness;
        }


        [HttpGet]
        [Route("meterwise/addMeter")]
        [ProducesResponseType(typeof(ConsultarHidrometrosViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConsultarHidrometrosViewModel(DateTime dataInicio, DateTime dataFim)
        {
            ConsultarHidrometrosViewModel result = new ConsultarHidrometrosViewModel();
            try
            {
                result = await _perdasBusiness.ConsultarHidrometrosViewModel(dataInicio, dataFim);
                if (result.Sucesso)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("meterwise/addReading")]
        [ProducesResponseType(typeof(ConsultarLeiturasViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConsultarLeiturasViewModel(DateTime dataInicio, DateTime dataFim)
        {
            BaseResponseViewModel result = new ConsultarLeiturasViewModel();
            try
            {
                result = await _perdasBusiness.ConsultarLeiturasViewModel(dataInicio, dataFim);
                if (result.Sucesso)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("flowise/addReading")]
        [ProducesResponseType(typeof(ConsultarLeiturasViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConsultarLeiturasMacroViewModel()
        {
            BaseResponseViewModel result = new ConsultarLeiturasViewModel();
            try
            {
                result = await _perdasBusiness.ConsultarLeiturasMacroViewModel();
                if (result.Sucesso)
                {
                    
                    var resultado = StatusCode(StatusCodes.Status200OK);
                    _perdasBusiness.InserirLog(resultado);
                    return Ok(result);
                }
                else
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                var retorno = StatusCode(StatusCodes.Status500InternalServerError);
                _perdasBusiness.InserirLog(retorno);
                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }
    }
}
