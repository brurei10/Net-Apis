using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace GAB.Inova.API.Perdas
{
    /// <summary>
    /// API de mensagem
    /// </summary>
    public class Program
    {

        /// <summary>
        /// M�todos principal
        /// </summary>
        /// <param name="args">Indica um lista de arqgumentos</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Construir um aplica��o web
        /// </summary>
        /// <param name="args">Indica um lista de arqgumentos</param>
        /// <returns><see cref="IWebHost"/></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    webBuilder.ConfigureLogging(logging =>
                    {
                        logging.ClearProviders();
                        logging.SetMinimumLevel(LogLevel.Information);
                    });

                    webBuilder.UseNLog();
                });
    }
}
