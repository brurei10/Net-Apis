using GAB.Inova.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace GAB.Inova.API.Perdas
{
    /// <summary>
    /// Classe para inicio de uma orquestra��o
    /// </summary>
    public class Startup
    {
        const string CORS_POLICY_NAME = "AllowCors";

        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public Startup(IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true) //{ env.EnvironmentName}
                .AddEnvironmentVariables();

            _configuration = builder.Build();
        }

        /// <summary>
        /// Adicionar servi�os ao cont�iner
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .ConfigurePerdasServices();

            services
                .AddControllers()
                .AddNewtonsoftJson(opcoes => opcoes.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);

            services
                .AddCors(o => o.AddPolicy(CORS_POLICY_NAME,
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    }));
        }


        /// <summary>
        /// Configurar o pipeline de solicita��o de HTTP
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Grupo Aguas do Brasil");
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(CORS_POLICY_NAME);
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
