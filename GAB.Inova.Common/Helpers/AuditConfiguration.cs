﻿using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using Microsoft.AspNetCore.Http;

namespace GAB.Inova.Common.Helpers
{
    public class AuditConfiguration
        : IAuditConfiguration
    {

        readonly IEnvironmentVariables _environmentVariables;
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly ITokenProvider _tokenProvider;

        public AuditConfiguration(IHttpContextAccessor httpContextAccessor, ITokenProvider tokenProvider, IEnvironmentVariables environmentVariables)
        {
            _environmentVariables = environmentVariables;
            _httpContextAccessor = httpContextAccessor;
            _tokenProvider = tokenProvider;
        }

        long _idUsuario;
        long _idRotina;
        string _ipRequisicao;
        string _tokenAtual;

        private string TokenAtual
        {
            get
            {
                if (string.IsNullOrEmpty(_tokenAtual))
                {
                    _tokenAtual = _tokenProvider.ObterTokenContextoAtualAsync()
                        .GetAwaiter()
                        .GetResult();
                }
                return _tokenAtual;
            }
        }

        public string ObterEnderecoIP()
        {
            if (string.IsNullOrEmpty(_ipRequisicao) && !string.IsNullOrEmpty(TokenAtual))
            {
                var ipUsuario = _tokenProvider.ObterClaimAsync(TokenAtual, "UsuarioIP")
                    .GetAwaiter()
                    .GetResult();

                _ipRequisicao = string.IsNullOrEmpty(ipUsuario) ? ObterIpRequisicao() : "127.0.0.1";
            }
            return _ipRequisicao;
        }

        public long ObterRotina()
        {
            if (_idRotina == default(long) && !string.IsNullOrEmpty(TokenAtual))
            {
                var rotinaId = _tokenProvider.ObterClaimAsync(TokenAtual, "RotinaId")
                    .GetAwaiter()
                    .GetResult();

                _idRotina = string.IsNullOrEmpty(rotinaId) ? _environmentVariables.IdRotina : long.Parse(rotinaId);
            }
            return _idRotina;
        }

        public long ObterUsuarioId()
        {
            if (_idUsuario == default(long) && !string.IsNullOrEmpty(TokenAtual))
            {
                var usuarioId = _tokenProvider.ObterClaimAsync(TokenAtual, "UsuarioId")
                    .GetAwaiter()
                    .GetResult();

                _idUsuario = string.IsNullOrEmpty(usuarioId) ? _environmentVariables.IdUsuario : long.Parse(usuarioId);
            }
            return _idUsuario;
        }

        #region [Métodos privados]
        
        private string ObterIpRequisicao()
        {
            return _httpContextAccessor.HttpContext.Connection.RemoteIpAddress == null ? string.Empty : _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        #endregion
    }
}
