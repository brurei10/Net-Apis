﻿using GAB.Inova.Common.Interfaces.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.Helpers
{
    public class SigningConfiguration : ISigningConfiguration
    {

        public SecurityKey Key { get; }

        public SigningCredentials SigningCredentials { get; }

        public List<SecurityKey> SecurityKeys { get; }

        public SigningConfiguration()
        {
            Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("f813af62180a4ac094f07bfe10x249c5"));

            SecurityKeys = new List<SecurityKey>();
            SecurityKeys.Add(Key);

            SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256Signature);
        }

    }
}
