﻿using GAB.Inova.Common.Interfaces.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace GAB.Inova.Common
{
    public class EnvironmentVariables
        : IEnvironmentVariables
    {
        public EnvironmentVariables(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            Environment = webHostEnvironment.EnvironmentName;
            IdRotina = configuration.GetValue<long>("IdRotinaApi");
            IdUsuario = configuration.GetValue<long>("IdUsuarioApi");
        }

        public string Environment { get; }
        public long IdRotina { get; }
        public long IdUsuario { get; }
    }
}
