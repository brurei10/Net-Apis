﻿using GAB.Inova.Common.Interfaces.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace GAB.Inova.Common.Helpers
{
    public class TokenConfiguration : ITokenConfiguration
    {

        public TokenConfiguration(IConfiguration configuration)
        {
            var cfgOptions = new ConfigureFromConfigurationOptions<TokenConfiguration>(configuration.GetSection("TokenConfigurations"));

            cfgOptions.Configure(this);
        }

        public string Audience { get; set; }

        public string Issuer { get; set; }

        public int Seconds { get; set; }
    }
}
