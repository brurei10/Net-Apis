﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoDeclaraParcialEnum
    {
        [Display(Description = "Normal-Total")]
        NormalTotal = 0,
        [Display(Description = "Parcelamento")]
        Parcelamento = 1,
        [Display(Description = "Judice")]
        Judice = 2
    }
}
