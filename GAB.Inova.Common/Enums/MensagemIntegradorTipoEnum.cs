﻿using System.ComponentModel;

namespace GAB.Inova.Common.Enums
{
    public enum MensagemIntegradorTipoEnum
    {
        [Description("Adesão à Conta Digital")]
        ContaDigitalAdesao = 1,
        [Description("Exclusão à Conta Digital")]
        ContaDigitalExclusao = 2,
        [Description("Alteração à Conta Digital")]
        ContaDigitalAlteracao = 3,
        [Description("Bem Vindo à Conta Digital")]
        ContaDigitalBemVindo = 4,
        [Description("Informativo")]
        Informativo = 5,
        [Description("Envio de Conta Digital")]
        ContaDigitalConta = 6,
        [Description("Erro no envio do Email da Conta Digital")]
        ContaDigitalErroEnvioConta = 7,
        [Description("Erro na Geração do Arquivo da Conta Digital")]
        ContaDigitalErroGeracaoArquivo = 8,
        [Description("Erro no Recebimento do Email da Conta Digital")]
        ContaDigitalErroRecebimentoEmail = 9,
        [Description("Segunda via de conta pelo aplicativo")]
        SegundaViaDeContaPeloAplicativo = 10,
        [Description("Fale conosco pelo aplicativo")]
        FaleConoscoPeloAplicativo = 11,
        [Description("Ouvidoria pelo aplicativo")]
        OuvidoriaPeloAplicativo = 12,
        [Description("Conta Digital para cliente sem adesão")]
        ContaDigitalContaClienteSemAdesao = 13,
        [Description("Quitação de débito")]
        QuitacaoDebito = 14,
        [Description("Erro no envio massivo do e-mail da Conta Digital")]
        ContaDigitalErroEnvioMassivoConta = 15
    }
}
