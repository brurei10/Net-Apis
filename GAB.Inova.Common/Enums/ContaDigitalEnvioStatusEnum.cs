﻿namespace GAB.Inova.Common.Enums
{
    public enum ContaDigitalEnvioStatusEnum
    {
        Gerado = 1,
        Enviado = 2,
        ErroGeracaoArquivo = 3,
        ErroNoEnvioDoEmail = 4,
        EmailNaoEntregue = 5,

    }
}
