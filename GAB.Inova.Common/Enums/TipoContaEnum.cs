﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoContaEnum
    {
        [Display(Description = "CONTA NORMAL")]
        ContaNormal = 0,
        [Display(Description = "AVISO DE DÉBITO")]
        AvisoDeDebito = 1,
        [Display(Description = "DOCUMENTO NÃO FISCAL")]
        DocumentoNaoFiscal = 2,
        [Display(Description = "FATURA")]
        Fatura = 3,
        [Display(Description = "NOTA FISCAL AVULSA")]
        NotaFiscalAvulsa = 4,
        [Display(Description = "CONVERSÃO")]
        Conversao = 5
    }
}
