﻿namespace GAB.Inova.Common.Enums
{
    public enum StatusDetOrdemServicoEnum
    {
        GERADA = 0,
        EMITIDA = 1,
        REPROGRAMADA = 2,
        PRE_BAIXADA = 3,
        AGENDADA = 4,
        CANCELADA = 5,
        BAIXADA = 6,
        PROGRAMADA = 7,
        BLOQUEADA = 8,
        PAUSADA = 9,
        AGUARDANDO_ENVIO = 12,
        EM_SISTEMA_EXTERNO = 13,
        AGUARDANDO_APROPRIACAO = 14
    }
}
