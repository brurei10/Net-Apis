﻿namespace GAB.Inova.Common.Enums
{
    public enum PdfModeloEnum
    {
        CONTA_INDIVIDUAL = 0,
        CONTA_LOTE = 1,
        CONTA_SEGUNDA_VIA = 2,
        CONTA_SEGUNDA_VIA_AGENCIA_VIRTUAL = 3
    }
}
