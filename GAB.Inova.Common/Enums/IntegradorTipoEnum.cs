﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum IntegradorTipoEnum
    {
        [Display(Description = "SENDGRID")]
        SendGrid = 1
    }
}
