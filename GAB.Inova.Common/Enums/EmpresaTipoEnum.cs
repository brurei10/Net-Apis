﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum EmpresaTipoEnum
    {
        NULO = 0,
        [Display(Description = "Águas de Araçoiaba")]
        CAA = 1,
        [Display(Description = "Águas de Nova Friburgo")]
        CANF = 2,
        [Display(Description = "Águas de Paraty")]
        CAPY = 3,
        [Display(Description = "Águas do Imperador")]
        CAI = 4,
        [Display(Description = "Águas de Votorantim")]
        CAV = 5,
        [Display(Description = "Águas das Agulhas Negras")]
        CAAN = 6,
        [Display(Description = "Águas de Jahu")]
        CAJA = 7,
        [Display(Description = "Águas de Juturnaíba")]
        CAJ = 8,
        [Display(Description = "Águas de Pará de Minas")]
        CAPAM = 9,
        [Display(Description = "Águas do Paraíba")]
        CAP = 10,
        [Display(Description = "Águas de Niterói")]
        CAN = 11,
        [Display(Description = "Águas da Condessa")]
        CAC = 12
    }
}
