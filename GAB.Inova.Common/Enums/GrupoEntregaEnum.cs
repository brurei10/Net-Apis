﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum GrupoEntregaEnum
    {
        [Display(Description = "DOMICÍLIO")]
        DOMICILIO = 0,
        [Display(Description = "IMOBILIÁRIA")]
        IMOBILIARIA = 2,
        [Display(Description = "CORREIOS")]
        CORREIOS = 3,
        [Display(Description = "LOJA")]
        LOJA = 4,
        [Display(Description = "JUSTIÇA")]
        JUSTICA = 5,
        [Display(Description = "RESPONSÁVEL")]
        RESPONSAVEL = 6,
        [Display(Description = "E-MAIL")]
        EMAIL = 7
    }
}
