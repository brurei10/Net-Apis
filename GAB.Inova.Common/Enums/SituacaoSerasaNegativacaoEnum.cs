﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum SituacaoSerasaNegativacaoEnum
    {
        [Display(Description = "INVÁLIDO")]
        Invalido = 0,
        [Display(Description = "NÃO NEGATIVADO")]
        NaoNegativado = 1,
        [Display(Description = "EM PROCESSO DE NEGATIVAÇÃO")]
        EmProcessoInclisaoNegativacao = 2,
        [Display(Description = "NEGATIVADO")]
        Negativado = 3,
        [Display(Description = "EM PROCESSO DE EXCLUSÃO")]
        EmProcessoExclusaoNegativacao = 4
    }
}
