﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoDeclaracaoEnum
    {
        [Display(Description = "Parcial")]
        Parcial = 1,
        [Display(Description = "Total")]
        Total = 2
    }
}
