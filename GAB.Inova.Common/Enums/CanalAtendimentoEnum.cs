﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum CanalAtendimentoEnum
    {
        [Display(Description = "CALL CENTER")]
        CallCenter = 1,
        [Display(Description = "ATENDIMENTO DIGITAL")]
        AtendimentoDigital = 2,
        [Display(Description = "PRESENCIAL LOJA")]
        PresencialLoja = 3,
        [Display(Description = "SEGUNDO NÍVEL")]
        SegundoNivel = 4,
        [Display(Description = "OUVIDORIA")]
        Ouvidoria = 5,
        [Display(Description = "ATENDIMENTO ELETRÔNICO")]
        AtendimentoEletronico = 6,
        [Display(Description = "BACKOFFICE")]
        Backoffice = 7,
        [Display(Description = "CONSULTA INTERNA")]
        ConsultaInterna = 9,
        [Display(Description = "PRESENCIAL MÓVEL")]
        PresencialMovel = 10,
        [Display(Description = "WHATSAPP")]
        Whatsapp = 11,
        [Display(Description = "REDES SOCIAIS")]
        RedesSociais = 12,
        [Display(Description = "APLICATIVO")]
        Aplicativo = 13,
        [Display(Description = "WEB CHAT")]
        WebChat = 14
    }
}
