﻿using System.ComponentModel;

namespace GAB.Inova.Common.Enums
{
    public enum SituacaoOSEletronicaEnum
    {
        [Description("Aguardando envio sistema especialista")]
        CRIADA = 1,
        [Description("Enviada ao sistema especialista")]
        ENVIADA = 2,
        [Description("Retornada do sistema especialista")]
        FINALIZADA = 3
    }
}
