﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoDocumentoModeloeEnum
    {
        [Display(Description = "Termo de compromisso")]
        TermoCompromisso = 1,
        [Display(Description = "Declaração de Quitação Anual de Débito")]
        DeclaracaoAnualQuitacaoDebito = 2,
        [Display(Description = "Documento para Quitação de Débitos")]
        QuitacaoDebito = 3,
        [Display(Description = "Termo de Adesão para impressão")]
        TermoAdesaoImpressao = 4,
        [Display(Description = "Termo de Adesão para envio email")]
        TermoAdesaoEnvioEmail = 5
    }
}
