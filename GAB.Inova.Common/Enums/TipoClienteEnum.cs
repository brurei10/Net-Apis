﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoClienteEnum
    {
        [Display(Description = "NORMAL")]
        Normal = 1,
        [Display(Description = "MÉDIO")]
        Medio = 2,
        [Display(Description = "GRANDE")]
        Grande = 3
    }
}
