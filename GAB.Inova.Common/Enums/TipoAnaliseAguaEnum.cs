﻿namespace GAB.Inova.Common.Enums
{
    public enum TipoAnaliseAguaEnum
    {
        FLUOR = 1,
        CLORO = 2,
        TURBIDEZ = 3,
        COR = 4,
        PH = 5,
        COLTOTAL = 6,
        COLTERM = 7
    }
}
