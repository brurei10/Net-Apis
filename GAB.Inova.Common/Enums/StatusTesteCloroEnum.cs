﻿using System.ComponentModel;

namespace GAB.Inova.Common.Enums
{
    public enum StatusTesteCloroEnum
    {
        [Description("Resultado de Teste de Cloro positivo")]
        POSITICO = 1,
        [Description("Resultado de Teste de Cloro negativo")]
        NEGATIVO = 2,
        [Description("Teste de Cloro não realizado")]
        NAO_REALIZADO = 3            
    }
}
