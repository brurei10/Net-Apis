﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TipoFaturaEnum
    {
        [Display(Description = "FATURA NORMAL")]
        FaturaNormal = 0 ,
        [Display(Description = "FATURÃO PODER PÚBLICO FEDERAL COM ICRFR")]
        FaturaoPPFederalComIcrfr = 1,
        [Display(Description = "FATURÃO PODER PÚBLICO ESTADUAL")]
        FaturaoPPEstadual = 2,
        [Display(Description = "FATURÃO PODER PÚBLICO MUNICIPAL")]
        FaturaoPPMunicipal = 3,
        [Display(Description = "FATURÃO PODER PÚBLICO FEDERAL SEM ICRFR")]
        FaturaoPPFederalSemIcrfr = 4,
        [Display(Description = "FATURÃO PODER PÚBLICO PARTICULAR")]
        FaturaoParticular = 5
    }
}
