﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum TokenTipoEnum
    {
        [Display(Description = "LOGIN")]
        Login = 1,
        [Display(Description = "ADESÃO DE CONTA DIGITAL")]
        ContaDigitalAdesao = 2,
        [Display(Description = "CANCELAMENTO DE CONTA DIGITAL")]
        ContaDigitalExclusao = 3,
        [Display(Description = "ALTERAÇÃO DE CONTA DIGITAL")]
        ContaDigitalAlteracao = 4
    }
}
