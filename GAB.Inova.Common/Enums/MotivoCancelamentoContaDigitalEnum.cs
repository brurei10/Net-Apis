﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum MotivoCancelamentoContaDigitalEnum
    {
        [Display(Name = "Caixa do Usuário Indisponível")]
        CaixaIndisponivel,
        [Display(Name = "Erro no envio do e-mail")]
        ErroEnvioEmail,
        [Display(Name = "Erro na geração da conta")]
        ErroGeracaoConta
    }
}
