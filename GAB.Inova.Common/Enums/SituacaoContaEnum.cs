﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum SituacaoContaEnum
    {
        [Display(Description = "EM ABERTO")]
        NaoPaga = 0,
        [Display(Description = "PAGA")]
        PagaForaDoPrazoMultaNaoFaturada = 1,
        [Display(Description = "PAGA")]
        PagaNoPrazo = 2,
        [Display(Description = "PAGA")]
        PagaForaDoPrazoMultaFaturada = 3,
        [Display(Description = "EM COBRANÇA JURÍDICA")]
        EmCobrancaJuridica = 5,
        [Display(Description = "PRESCRITA")]
        Prescita = 6,
        [Display(Description = "PARCELADA")]
        Parcelada = 7,
        [Display(Description = "BLOQUEADA")]
        RetidoNaAnaliseVoltaDaSimultanea = 8,
        [Display(Description = "CANCELADA")]
        Retificada = 9
    }
}
