﻿namespace GAB.Inova.Common.Enums
{
    public enum MensagemIntegradoStatusEnum
    {
        Criado = 1,
        Enviado = 2,
        Entregue = 3,
        ReportadoSpam = 4,
        Negado = 5,
        Aberto = 6,
        Processado = 7,
        Falha = 8,
        Adiado = 9
    }
}
