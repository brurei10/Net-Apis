﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.Enums
{
    public enum SistemaEnum
    {
        [Display(Description = "INOVA")]
        Inova = 1,
        [Display(Description = "AGÊNCIA VIRTUAL")]
        AgenciaVirtual = 2
    }
}
