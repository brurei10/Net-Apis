﻿namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class LigacaoViewModel
    {
        public string Ligacao { get; set; }
        public string LogradouroEndereco { get; set; }
        public string NumeroEndereco { get; set; }
        public string ComplementoEndereco { get; set; }
        public string BairroEndereco { get; set; }
    }
}
