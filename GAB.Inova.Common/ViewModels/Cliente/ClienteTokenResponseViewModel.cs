﻿using GAB.Inova.Common.ViewModels.Cliente.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class ClienteTokenResponseViewModel
        : BaseClienteResponseViewModel
    {
        public string Token { get; set; }
        public int TokenTipo { get; set; }
        public string EmailDoToken { get; set; }
        public string EnderecoCompleto { get; set; }
    }
}
