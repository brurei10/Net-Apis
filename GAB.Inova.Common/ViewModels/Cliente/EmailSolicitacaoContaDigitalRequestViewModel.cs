﻿using GAB.Inova.Common.ViewModels.Cliente.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class EmailSolicitacaoContaDigitalRequestViewModel
        : BaseEmailContaDigitalRequestViewModel
    {
        public string Matricula { get; set; }
        public string Endereco { get; set; }
        public string UrlConfirmacao { get; set; }
        public object Nome { get; set; }
    }
}
