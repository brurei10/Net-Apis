﻿using GAB.Inova.Common.ViewModels.Cliente.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class EmailBemVindoContaDigitalRequestViewModel
        : BaseEmailContaDigitalRequestViewModel
    {
        public string DataProximaLeitura { get; set; }
        public string Email { get; set; }

    }
}
