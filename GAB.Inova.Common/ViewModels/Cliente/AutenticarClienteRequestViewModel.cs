﻿using GAB.Inova.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class AutenticarClienteRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string Ligacao { get; set; }

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string CpfCnpj { get; set; }
    }
}
