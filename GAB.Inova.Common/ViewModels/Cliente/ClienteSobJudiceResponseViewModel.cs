﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class ClienteSobJudiceResponseViewModel
        : BaseResponseViewModel
    {
        public ClienteSobJudiceResponseViewModel()
        {
            Sucesso = false;
        }

        public string Matricula { get; set; }

        public bool PermiteEmissaoSegundaVia { get; set; }

        public bool PermiteEmissaoQuitacaoDebito { get; set; }

    }
}
