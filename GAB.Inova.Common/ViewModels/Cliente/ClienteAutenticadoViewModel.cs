﻿namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class ClienteAutenticadoViewModel
    {

        public virtual string CpfCnpj { get; set; }
        public virtual string Ligacao { get; set; }
        public virtual string NomeCliente { get; set; }
        public virtual string EnderecoCompleto { get; set; }
        public virtual string PontoReferencia { get; set; }
        public virtual string Bairro { get; set; }
        public virtual string Cidade { get; set; }
        public virtual string Cep { get; set; }
        public virtual string TelefoneFixo { get; set; }
        public virtual string TelefoneCelular { get; set; }
        public virtual string Logradouro { get; set; }
        public virtual string LogradouroNumero { get; set; }
        public virtual string LogradouroComplemento { get; set; }
        public virtual string Estado { get; set; }
        public virtual string Email { get; set; }

    }
}
