﻿using GAB.Inova.Common.ViewModels.Cliente.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class EmailEnvioContaDigitalRequestViewModel
        : BaseEmailContaDigitalRequestViewModel
    {
        public string NomeCliente { get; set; }
        public string Endereco { get; set; }
        public string MesAnoReferencia { get; set; }
        public string DataVencimento { get; set; }
        public string ValorTotal { get; set; }
    }
}
