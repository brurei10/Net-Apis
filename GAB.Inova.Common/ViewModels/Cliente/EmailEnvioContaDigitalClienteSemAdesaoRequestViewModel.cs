﻿using GAB.Inova.Common.ViewModels.Cliente.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class EmailEnvioContaDigitalClienteSemAdesaoRequestViewModel
        : BaseEmailContaDigitalRequestViewModel
    {

        public string NomeCliente { get; set; }
        public string Matricula { get; set; }
        public string Endereco { get; set; }

    }
}
