﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class ObterLigacoesResponseViewModel: BaseResponseViewModel
    {
        public ObterLigacoesResponseViewModel()
        {
            Ligacoes = new HashSet<LigacaoViewModel>();
        }

        public ICollection<LigacaoViewModel> Ligacoes { get; set; }
    }
}