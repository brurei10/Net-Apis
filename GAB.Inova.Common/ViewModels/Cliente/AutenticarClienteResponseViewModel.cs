﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class AutenticarClienteResponseViewModel
        : BaseResponseViewModel
    {

        public virtual ClienteAutenticadoViewModel ClienteAutenticado { get; set; }
        public virtual string CodigoEmpresa { get; set; }
        public virtual string NumeroProtocolo { get; set; }
        public virtual string Token { get; set; }

    }
}
