﻿using GAB.Inova.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Cliente.Base
{
    public abstract class BaseSolicitacaoContaDigitalRequestViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Matricula { get; set; }
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "O campo email é inválido!")]
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public SistemaEnum Sistema { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Protocolo { get; set; }

    }
}
