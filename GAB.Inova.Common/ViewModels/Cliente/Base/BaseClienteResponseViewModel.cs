﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Cliente.Base
{
    public abstract class BaseClienteResponseViewModel
        : BaseResponseViewModel
    {
        public string Nome { get; set; }
        public string Localizacao { get; set; }
        public string GrupoEntrega { get; set; }
        public long GrupoEntregaId { get; set; }
        public bool EhContaDigital { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Documento { get; set; }
        public string DocumentoFormatado { get; set; }
        public string Logradouro { get; set; }
        public string LogradouroNumero { get; set; }
        public string LogradouroComplemento { get; set; }
        public string Matricula { get; set; }
        public string Email { get; set; }
        public string EmailContaDigital { get; set; }
        public string Celular { get; set; }
        public string CelularFormatado { get; set; } 
        public long TipoFaturaId { get; set; }
    }
}
