﻿using GAB.Inova.Common.ViewModels.Cliente.Base;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class AlterarContaDigitalRequestViewModel
        : BaseSolicitacaoContaDigitalRequestViewModel
    {

        public bool ModificaEmailContato { get; set; }

    }
}
