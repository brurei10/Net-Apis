﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Cliente
{
    public class EnviarContaDigitalRequestViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Matricula { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string ContaBase64 { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public long SeqOriginal { get; set; }
    }
}
