﻿using GAB.Inova.Common.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels.Perdas
{
    public class ConsultarLeiturasMacroViewModel : BaseResponseViewModel
    {

        public int WaterUtility { get; set; }
        public ICollection<MI_STRUCT_Reading_List> MI_STRUCT_Reading_List { get; set; }

        public ConsultarLeiturasMacroViewModel()
        {
            MI_STRUCT_Reading_List = new HashSet<MI_STRUCT_Reading_List>();
        }

    }
}