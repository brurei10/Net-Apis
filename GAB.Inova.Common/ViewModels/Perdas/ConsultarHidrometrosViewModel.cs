﻿using GAB.Inova.Common.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels.Perdas
{
    public class ConsultarHidrometrosViewModel : BaseResponseViewModel
    {
    
        public int WaterUtility { get; set; }
        public ICollection<MI_STRUCT_Meter_List> MI_STRUCT_Meter_List { get; set; }
        
        public ConsultarHidrometrosViewModel()
        {
            MI_STRUCT_Meter_List = new HashSet<MI_STRUCT_Meter_List>();
        }

    }
}
