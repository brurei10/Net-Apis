﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels.Perdas
{
    public class MI_STRUCT_Meter_List
    {
        public string SerialNumber { get; set; } //codigo do HD
        public string ClientId { get; set; } //matricula
        public string Location { get; set; } //setor ou sistema de abastecimento
        public string Area { get; set; } //setor ou sistema de abastecimento
        public string Diameter { get; set; }
        public string ManufactureYear { get; set; }
        public DateTime InstallDate { get; set; }
        public string ServiceType { get; set; } //categoria
        public string EquipmentType { get; set; } //funcionamento do HD
        public string EquipmentSubType { get; set; } //Deixar em branco
        public string Brand { get; set; }
        public string Model { get; set; }
        public string PrecisionClass { get; set; } //classe metrologica
        public string MinimumFlow { get; set; } //Usuario enviara de um de-para
        public string NominalFlow { get; set; } //vazão nominal
        public string MaximumFlow { get; set; } //vazão nominal * 2
        public string Condition { get; set; }
        public string ReadingArea { get; set; } //Deixar em branco
        public string InstallationType { get; set; } //Deixar em branco
    }
}
