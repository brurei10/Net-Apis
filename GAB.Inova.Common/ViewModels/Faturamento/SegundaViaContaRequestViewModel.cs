﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class SegundaViaContaRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string NumeroProtocolo { get; set; }
        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public long SeqOriginal { get; set; }

    }
}
