﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class HistoricoContasResponseViewModel
        : BaseResponseViewModel
    {

        public HistoricoContasResponseViewModel()
        {
            ContasContrato = new HashSet<ContasContratoViewModel>();
        }

        public ICollection<ContasContratoViewModel> ContasContrato { get; set; }

    }
}
