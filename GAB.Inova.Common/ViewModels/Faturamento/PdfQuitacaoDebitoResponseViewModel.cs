﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class PdfQuitacaoDebitoResponseViewModel : BaseResponseViewModel
    {
        public string EspelhoDaQuitacao { get; set; }
    }
}
