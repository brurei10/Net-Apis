﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class EnviarEmailSegundaViaRequestViewModel
        : SegundaViaContaRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string Email { get; set; }

    }
}
