﻿using GAB.Inova.Common.Enums;

namespace GAB.Inova.Common.ViewModels.Faturamento.Base
{
    public abstract class BaseTemplateEmailViewModel
    {

        public EmpresaTipoEnum EmpresaTipo { get; set; }

        public bool EmpresaCAA => EmpresaTipo == EmpresaTipoEnum.CAA;
        public bool EmpresaCANF => EmpresaTipo == EmpresaTipoEnum.CANF;
        public bool EmpresaCAPY => EmpresaTipo == EmpresaTipoEnum.CAPY;
        public bool EmpresaCAI => EmpresaTipo == EmpresaTipoEnum.CAI;
        public bool EmpresaCAV => EmpresaTipo == EmpresaTipoEnum.CAV;
        public bool EmpresaCAAN => EmpresaTipo == EmpresaTipoEnum.CAAN;
        public bool EmpresaCAJA => EmpresaTipo == EmpresaTipoEnum.CAJA;
        public bool EmpresaCAJ => EmpresaTipo == EmpresaTipoEnum.CAJ;
        public bool EmpresaCAPAM => EmpresaTipo == EmpresaTipoEnum.CAPAM;
        public bool EmpresaCAP => EmpresaTipo == EmpresaTipoEnum.CAP;
        public bool EmpresaCAN => EmpresaTipo == EmpresaTipoEnum.CAN;

    }
}
