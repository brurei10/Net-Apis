﻿using GAB.Inova.Common.ViewModels.Faturamento.Base;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class TemplateEmailSegundaViaAplicativoViewModel
        : BaseTemplateEmailViewModel
    {

        public string NomeCliente { get; set; }
    }
}
