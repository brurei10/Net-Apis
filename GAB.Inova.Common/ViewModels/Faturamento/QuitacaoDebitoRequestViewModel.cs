﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class QuitacaoDebitoRequestViewModel
    {
        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string NumeroProtocolo { get; set; }

        [Description("Contas para quitação de débito e geração de código de barras")]
        public IEnumerable<long> Contas { get; set; }

        public QuitacaoDebitoRequestViewModel()
        {
            Contas = new HashSet<long>();
        }
    }
}
