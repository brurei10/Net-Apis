﻿using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class ContaImpressaoViewModel
    {

        public ContaImpressaoViewModel()
        {
            Mensagens = new List<string>();

            //Qualidade da água - Fluor, Cloro, Turbidez, Cor, pH, ColiformesTotais, ColiformesTermotolerantes
            QualidadeAguaCor = new List<string>();
            QualidadeAguaColiformesTotais = new List<string>();
            QualidadeAguaCloro = new List<string>();
            QualidadeAguaEscherichiaColi = new List<string>();
            QualidadeAguaFluor = new List<string>();
            QualidadeAguaPh = new List<string>();
            QualidadeAguaTurbidez = new List<string>();
            
            //Histórico de consumo
            HistoricosConsumoConsumo = new List<string>();
            HistoricosConsumoDias = new List<string>();
            HistoricosConsumoReferencia = new List<string>();

            //Faixa de consumo
            ConsumosFaturado = new List<string>();
            FaixasConsumo = new List<string>();
            TarifasAgua = new List<string>();
            TarifasEsgoto = new List<string>();

            //Discriminacao do faturamento
            EspecificacaoServico = new List<string>();
            ValorFaturado = new List<string>();
        }

        public string Ligacao { get; set; }
        public string Roteirizacao { get; set; }
        public string NumeroConta { get; set; }
        public string Referencia { get; set; }
        public string DataEmissao { get; set; }
        public string Vencimento { get; set; }
        public string Via { get; set; }
        public string NomeRazaoSocial { get; set; }
        public string EnderecoLigacao { get; set; }
        public string Complemento { get; set; }
        public string NumeroHidrometro { get; set; }
        public string LeituraAnterior { get; set; }
        public string LeituraAtual { get; set; }
        public string DataLeituraAnterior { get; set; }
        public string DataLeituraAtual { get; set; }
        public string PrevisaoProximaLeitura { get; set; }
        public string TipoEntrega { get; set; }
        public string CpfCnpj { get; set; }
        public string InscricaoEstadualMunicipal { get; set; }
        public string CategoriaNumeroEconomiasResidencial { get; set; }
        public string CategoriaNumeroEconomiasComercial { get; set; }
        public string CategoriaNumeroEconomiasIndustrial { get; set; }
        public string CategoriaNumeroEconomiasPublica { get; set; }
        public string DiasConsumo { get; set; }
        public string ConsumoMedido { get; set; }
        public string CreditoConsumo { get; set; }
        public string Pipa { get; set; }
        public string Residual { get; set; }
        public string ConsumoFaturado { get; set; }
        public string TipoFaturamento { get; set; }
        public string BaseCalculo { get; set; }
        public string Aliquota { get; set; }
        public string Imposto { get; set; }
        public string RetencaoTributos { get; set; }
        public string TotalPagar { get; set; }
        public string CargaTributariaSobreValorServicos { get; set; }
        public string IdentificadorDebitoAutomatico { get; set; }
        public string QualidadeAguaSistemaAbastecimento { get; set; }
        public string QualidadeAguaObservacoes { get; set; }
        public string NumeroCodigodeBarras { get; set; }
        public string NumeroCodigodeBarrasFormatado { get; set; }
        public string TextoAlternativoCodigoBarras { get; set; }
        public string AvisoDebitoNumero { get; set; }
        public string AvisoDebitoLote { get; set; }
        public string AvisoDebitoMensagemParte1 { get; set; }
        public string AvisoDebitoMensagemParte2 { get; set; }
        public string AvisoDebitoMensagemParte3 { get; set; }
        public string AvisoDebitoCodigoBarras { get; set; }
        public string CustoRegulacaoFiscalizacao { get; set; }
        public string AvisoDebitoCodigodeBarrasFormatado { get; set; }
        public string ImagemBackground { get; set; }
        public string SituacaoConta { get; set; }

        public List<string> Mensagens { get; set; }

        //Qualidade da água - Fluor, Cloro, Turbidez, Cor, pH, ColiformesTotais, ColiformesTermotolerantes
        public List<string> QualidadeAguaFluor { get; set; }
        public List<string> QualidadeAguaCloro { get; set; }
        public List<string> QualidadeAguaTurbidez { get; set; }
        public List<string> QualidadeAguaCor { get; set; }
        public List<string> QualidadeAguaPh { get; set; }
        public List<string> QualidadeAguaColiformesTotais { get; set; }
        public List<string> QualidadeAguaEscherichiaColi { get; set; }

        //Histórico de consumo
        public List<string> HistoricosConsumoReferencia { get; set; }
        public List<string> HistoricosConsumoConsumo { get; set; }
        public List<string> HistoricosConsumoDias { get; set; }

        //Faixa de consumo
        public List<string> FaixasConsumo { get; set; }
        public List<string> ConsumosFaturado { get; set; }
        public List<string> TarifasAgua { get; set; }
        public List<string> TarifasEsgoto { get; set; }

        //Discriminacao do faturamento
        public List<string> EspecificacaoServico { get; set; }
        public List<string> ValorFaturado { get; set; }

    }
}
