﻿using GAB.Inova.Common.ViewModels.Base;
using System;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class CodigoBarrasResponseViewModel: BaseResponseViewModel
    {
        public long SeqOriginal { get; set; }
        public DateTime DataVencimento { get; set; }
        public Double ValorTotal { get; set; }
        public string CodigoBarras { get; set; }
    }
}
