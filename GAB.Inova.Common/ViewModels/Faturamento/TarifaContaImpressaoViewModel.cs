﻿namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class TarifaContaImpressaoViewModel
    {

        public string NomeEconomia { get; set; }
        public string Faixa { get; set; }
        public double ConsumoFaturado { get; set; }
        public double ValorTarifa { get; set; }
        public double ValorTarifaEsgoto { get; set; }
        public string Referencia { get; set; }
        public string Matricula { get; set; }
        public long SeqOriginal { get; set; }
        public double VolumeTotalFaturado { get; set; }
        public int TipoTarifa { get; set; }

    }
}
