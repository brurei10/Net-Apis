﻿using System;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class ContasContratoViewModel
    {

        public string DataReferencia { get; set; }
        public DateTime DataEmissao { get; set; }
        public DateTime DataVencimento { get; set; }
        public string Matricula { get; set; }
        public long SeqOriginal { get; set; }
        public decimal ValorAgua { get; set; }
        public decimal ValorEsgoto { get; set; }
        public decimal ValorRecursosHidricos { get; set; }
        public decimal ValorServico { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorDevolucao { get; set; }
        public decimal ValorMultaJuros { get; set; }
        public decimal ValorIcfrf { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal ValorDescontoRetificado { get; set; }
        public decimal ValorTotalAtualizado { get; set; }
        public string SituacaoConta { get; set; }

    }
}
