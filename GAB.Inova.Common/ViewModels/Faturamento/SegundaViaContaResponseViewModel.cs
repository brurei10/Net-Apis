﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class SegundaViaContaResponseViewModel
        : BaseResponseViewModel
    {

        public string EspelhoDaConta { get; set; }

    }
}
