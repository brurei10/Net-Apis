﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Faturamento
{
    public class EnviarEmailQuitacaoDebitoRequestViewModel
        : QuitacaoDebitoRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string Email { get; set; }

    }
}
