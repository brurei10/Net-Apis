﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Token
{
    public class TokenResponseViewModel
        : BaseResponseViewModel
    {

        public string Token { get; set; }

    }
}
