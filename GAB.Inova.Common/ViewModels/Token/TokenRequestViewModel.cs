﻿using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Token
{
    public class TokenRequestViewModel
    {
        public TokenRequestViewModel()
        {
            Claims = new List<KeyValuePair<string, string>>();
        }

        public List<KeyValuePair<string, string>> Claims { get; set; }
    }
}
