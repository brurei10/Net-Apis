﻿using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Base
{
    public class BaseResponseViewModel
    {

        ICollection<string> _mensagens { get; set; }

        public bool Sucesso { get; set; }

        public BaseResponseViewModel()
        {
            Sucesso = true;
            _mensagens = new HashSet<string>();
        }

        public BaseResponseViewModel(bool sucesso) : this() => Sucesso = sucesso;

        public IEnumerable<string> Mensagens => _mensagens;

        public void AdicionaMensagem(string mensagem) => _mensagens.Add(mensagem);

        public void AdicionaMensagens(IEnumerable<string> mensagens)
        {
            foreach (var mensagem in mensagens)
            {
                AdicionaMensagem(mensagem);
            }
        }
    }
}
