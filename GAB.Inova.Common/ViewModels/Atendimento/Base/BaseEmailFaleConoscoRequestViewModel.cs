﻿namespace GAB.Inova.Common.ViewModels.Atendimento.Base
{
    public abstract class BaseEmailFaleConoscoRequestViewModel
    {

        public string CodigoEmpresa { get; set; }
        public string CpfCnpj { get; set; }
        public string NomeCliente { get; set; }
        public string Email { get; set; }
        public string TelefoneFixo { get; set; }
        public string TelefoneCelular { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Logradouro { get; set; }
        public string LogradouroNumero { get; set; }
        public string LogradouroComplemento { get; set; }
        public string Ligacao { get; set; }
        public string MotivoContato { get; set; }
        public string AssuntoContato { get; set; }
        public string MensagemContato { get; set; }
        public string NumeroProtocoloAtual { get; set; }

    }
}
