﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Atendimento.Base
{
    public abstract class BaseIntegracaoGfaResponseViewModel
        : BaseResponseViewModel
    {

        public BaseIntegracaoGfaResponseViewModel()
        {
            Sucesso = false;
        }

        public string SenhaGfa { get; set; }

        public string NumeroProtocoloGfa { get; set; }

        public string UsuarioGfa { get; set; }

    }
}
