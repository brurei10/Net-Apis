﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Atendimento.Base
{
    public abstract class BaseIntegracaoGfaRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro [{0}] é obrigatório!")]
        public string SenhaGfa { get; set; }
        [Required(ErrorMessage = "O parâmetro [{0}] é obrigatório!")]
        public string NumeroProtocoloGfa { get; set; }
        [Required(ErrorMessage = "O parâmetro [{0}] é obrigatório!")]
        public string UsuarioGfa { get; set; }

    }
}
