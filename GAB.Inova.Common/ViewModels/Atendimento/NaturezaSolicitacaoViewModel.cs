﻿namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class NaturezaSolicitacaoViewModel
    {

        public long IdNatureza { get; set; }

        public string Descricao { get; set; }

    }
}
