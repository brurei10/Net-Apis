﻿namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class UsuarioPontoAtendimentoRequestViewModel
    {

        public string Username { get; set; }
        public string CodigoEmpresa { get; set; }

    }
}
