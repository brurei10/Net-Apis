﻿using System;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class ProtocoloAtendimentoRequestViewModel
    {
        public string Matricula { get; set; }
        public DateTime? DataFim { get; set; }
        public int? NaturezaSolicitacao { get; set; }
    }
}
