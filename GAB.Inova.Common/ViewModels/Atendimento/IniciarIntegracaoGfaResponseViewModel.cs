﻿using GAB.Inova.Common.ViewModels.Atendimento.Base;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class IniciarIntegracaoGfaResponseViewModel
        : BaseIntegracaoGfaResponseViewModel
    {

        public string NumeroProtocoloInova { get; set; }

    }
}
