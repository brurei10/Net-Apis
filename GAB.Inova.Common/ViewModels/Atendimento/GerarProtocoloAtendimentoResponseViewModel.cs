﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class GerarProtocoloAtendimentoResponseViewModel
        : BaseResponseViewModel
    {

        public string NumeroProtocolo { get; set; }

    }
}
