﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class RegistrarNaturezaViewModel
    {

        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string NumeroProtocolo { get; set; }
        public int NaturezaId { get; set; }
        public long RotinaId { get; set; }
        
    }
}
