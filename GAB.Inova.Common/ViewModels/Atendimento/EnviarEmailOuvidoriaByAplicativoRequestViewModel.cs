﻿using GAB.Inova.Common.ViewModels.Atendimento.Base;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class EnviarEmailOuvidoriaByAplicativoRequestViewModel
        : BaseEmailFaleConoscoRequestViewModel
    {

        public string NumeroProtocoloAnterior { get; set; }
        public bool CopiarAgenersa { get; set; }

    }
}
