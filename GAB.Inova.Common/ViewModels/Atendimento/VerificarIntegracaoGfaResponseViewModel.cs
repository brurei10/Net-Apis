﻿using GAB.Inova.Common.ViewModels.Atendimento.Base;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class VerificarIntegracaoGfaResponseViewModel
        : BaseIntegracaoGfaResponseViewModel
    {

        public VerificarIntegracaoGfaResponseViewModel()
        {
            NaturezasSolicitacao = new HashSet<NaturezaSolicitacaoViewModel>();
        }

        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public IEnumerable<NaturezaSolicitacaoViewModel> NaturezasSolicitacao { get; set; }

        public string Status
        {
            get
            {
                return DataInicio.HasValue ? (DataFim.HasValue ? "Finalizado" : "Em Andamento") : "";
            }
        }

    }
}
