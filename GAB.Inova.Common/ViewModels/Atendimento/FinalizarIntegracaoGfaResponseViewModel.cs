﻿using GAB.Inova.Common.ViewModels.Atendimento.Base;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GAB.Inova.Common.ViewModels.Atendimento
{
    public class FinalizarIntegracaoGfaResponseViewModel
        : BaseIntegracaoGfaResponseViewModel
    {

        public FinalizarIntegracaoGfaResponseViewModel()
        {
            NaturezasSolicitacao = new HashSet<NaturezaSolicitacaoViewModel>();
        }

        [IgnoreDataMember]
        public bool TemErroValidacaoProtocoloAtendimento { get; set; }

        public IEnumerable<NaturezaSolicitacaoViewModel> NaturezasSolicitacao { get; set; }

    }
}
