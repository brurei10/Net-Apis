﻿using System;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class NaturezaViewModel
    {
        public int codNatureza { get; set; }

        public string descNatureza { get; set; }
        public DateTime datahora { get; set; }
    }
}
