﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class GerarAutenticacaoResponseViewModel : BaseResponseViewModel
    {
        public string NomeCliente { get; set; }
        public string Documento { get; set; }
        public string Url { get; set; }
    }
}
