﻿namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class AssociarLigacaoProtocoloViewModel
    {
        public string Protocolo { get; set; }
        public string NumLigacao { get; set; }
    }
}
