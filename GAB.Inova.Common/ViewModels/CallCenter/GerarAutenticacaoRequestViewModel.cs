﻿namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class GerarAutenticacaoRequestViewModel
    {
        public string codConcessao { get; set; }
        public string LoginOperador { get; set; }
        public string Protocolo { get; set; }
    }
}
