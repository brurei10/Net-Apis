﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class AutenticarClienteResponseViewModel: BaseResponseViewModel
    {

        public virtual string access_token { get; set; }
        public virtual string token_type { get; set; }
        public virtual int expires_in { get; set; }
    }
}
