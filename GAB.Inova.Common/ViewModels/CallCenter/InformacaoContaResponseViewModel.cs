﻿using GAB.Inova.Common.ViewModels.Base;
using System;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class InformacaoContaResponseViewModel : BaseResponseViewModel
    {
        public int sitConta { get; set; }
        public DateTime? dataVencimento { get; set; }
        public decimal? valor { get; set; }
    }
}