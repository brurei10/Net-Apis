﻿namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class RegistrarNaturezaRequestViewModel
    {
        public string Protocolo { get; set; }
        public string CodNatureza { get; set; }
    }
}
