﻿namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class LigacaoViewModel
    {
        public string numLigacao { get; set; }
        public int flgLigacaoEspecial { get; set; }
        public int flgInterrupcaoAbast { get; set; }
        public int? previsaoRetorno { get; set; }
        public int flgTipoClienteGrande { get; set; }
    }
}
