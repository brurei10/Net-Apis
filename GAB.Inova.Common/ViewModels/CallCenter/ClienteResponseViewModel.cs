﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class ClienteResponseViewModel : BaseResponseViewModel
    {
        public ClienteResponseViewModel()
        {
            ligacoes = new HashSet<LigacaoViewModel>();
        }

        public string nomeCliente { get; set; }
        public string documento { get; set; }
        public int possuiEmail { get; set; }
        public int possuiCelular { get; set; }

        public ICollection<LigacaoViewModel> ligacoes { get; set; }
    }
}