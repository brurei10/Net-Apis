﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.CallCenter
{
    public class FinalizarProtocoloAtendimentoResponseViewModel : BaseResponseViewModel
    {
        public FinalizarProtocoloAtendimentoResponseViewModel()
        {
            naturezas = new HashSet<NaturezaViewModel>();
        }
        public string nomeCliente { get; set; }
        public string documento { get; set; }
        public ICollection<NaturezaViewModel> naturezas { get; set; }
    }
}
