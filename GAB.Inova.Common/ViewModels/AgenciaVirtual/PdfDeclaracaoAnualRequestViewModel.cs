﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class PdfDeclaracaoAnualRequestViewModel
    {

        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public string NumeroProtocolo { get; set; }
        [Required(ErrorMessage = "O parâmetro {0} é obrigatório!")]
        public long SeqDeclaracao { get; set; }

    }
}