﻿namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class DeclaracaoAnualViewModel
    {
        public int SeqDeclaracao { get; set; }
        public string AnoReferencia { get; set; }
        public string TipoDeclaracao { get; set; }
    }
}
