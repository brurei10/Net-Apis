﻿namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class GerarQuitacaoDebitoRequestViewModel
    {
        public long Protocolo { get; set; }
        public string[] SeqOriginal { get; set; }
    }
}
