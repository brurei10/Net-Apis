﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class DeclaracoesAnuaisResponseViewModel : BaseResponseViewModel
    {
        public DeclaracoesAnuaisResponseViewModel()
        {
            DeclaracoesAnuais = new HashSet<DeclaracaoAnualViewModel>();
        }

        public ICollection<DeclaracaoAnualViewModel> DeclaracoesAnuais { get; set; }
    }
}
