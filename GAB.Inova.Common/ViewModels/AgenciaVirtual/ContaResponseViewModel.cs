﻿using GAB.Inova.Common.ViewModels.Base;
using System;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class ContaResponseViewModel : BaseResponseViewModel
    {

        public long SeqOriginal { get; set; }
        public string DataReferencia { get; set; }
        public DateTime DataEmissao { get; set; }
        public DateTime DataVencimento { get; set; }
        public int IdSituacao { get; set; }
        public string DsSituacao { get; set; }
        public long IdContrato { get; set; }
        public string Matricula { get; set; }
        public decimal VlAgua { get; set; }
        public decimal VlEsgoto { get; set; }
        public decimal VlRecHidricos { get; set; }
        public decimal VlServico { get; set; }
        public decimal VlDesconto { get; set; }
        public decimal VlDevolucao { get; set; }
        public decimal VlMultaJuros { get; set; }
        public decimal VlIcfrf { get; set; }
        public decimal VlDescontoRetificado { get; set; }
        public decimal VlTotalAtualizado { get; set; }
        public decimal VlTotal { get; set; }
        public string DsSitNegativacao { get; set; }
    }
}
