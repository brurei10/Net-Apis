﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class HistoricoContasResponseViewModel: BaseResponseViewModel
    {
        public HistoricoContasResponseViewModel()
        {
            ContasContrato = new HashSet<ContaResponseViewModel>();
        }

        public ICollection<ContaResponseViewModel> ContasContrato { get; set; }

    }
}
