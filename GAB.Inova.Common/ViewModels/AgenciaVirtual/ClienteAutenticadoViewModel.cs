﻿using System;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class ClienteAutenticadoViewModel
    {
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string CodigoInova { get; set; }
        public string Complemento { get; set; }
        public string CpfCnpj { get; set; }
        public int CpfCnpjValido { get; set; }
        public DateTime? DataExpedicao { get; set; }
        public DateTime? DataNascimento { get; set; }
        public int EcoCom { get; set; }
        public int EcoInd { get; set; }
        public int EcoPub { get; set; }
        public int EcoRes { get; set; }
        public string Email { get; set; }
        public int EmpresaClienteId { get; set; }
        public int EmpresaId { get; set; }
        public int Id { get; set; }
        public int IdContrato { get; set; }
        public long IdProtocoloAtendimento { get; set; }
        public string Logradouro { get; set; }
        public string Matricula { get; set; }
        public string NomeCliente { get; set; }
        public string NomeEmpresa { get; set; }
        public string NomeMae { get; set; }
        public string NomePai { get; set; }
        public string Numero { get; set; }
        public string OrgaoExpedidor { get; set; }
        public string PontoReferencia { get; set; }
        public string ProtocoloAtendimento { get; set; }
        public int QtdEco { get; set; }
        public string Rg { get; set; }
        public string Sigla { get; set; }
        public string TelefoneCelular { get; set; }
        public string TelefoneCelular2 { get; set; }
        public string TelefoneComercial { get; set; }
        public string TelefoneFixo { get; set; }
        public string TipoPessoa { get; set; }
        public string Uf { get; set; }
    }
}
