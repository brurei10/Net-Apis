﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class PdfDeclaracaoAnualResponseViewModel : BaseResponseViewModel
    {
        public string EspelhoDaDeclaracaoAnual { get; set; }
    }
}
