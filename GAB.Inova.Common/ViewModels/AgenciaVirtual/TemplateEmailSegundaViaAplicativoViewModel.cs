﻿using GAB.Inova.Common.ViewModels.AgenciaVirtual.Base;

namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class TemplateEmailSegundaViaAplicativoViewModel
        : BaseTemplateEmailViewModel
    {

        public string NomeCliente { get; set; }

    }
}
