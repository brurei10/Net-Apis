﻿namespace GAB.Inova.Common.ViewModels.AgenciaVirtual
{
    public class AutenticarClienteRequestViewModel
    {
        public string matricula { get; set; }
        public string documento { get; set; }
    }
}
