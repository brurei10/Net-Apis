﻿namespace GAB.Inova.Common.ViewModels.Consumo
{
    public class ConsumosMatriculaViewModel
    {

        public string DataReferencia { get; set; }
        public long ConsumoFaturado { get; set; }
        public long QtdeDiasLeitura { get; set; }

    }
}
