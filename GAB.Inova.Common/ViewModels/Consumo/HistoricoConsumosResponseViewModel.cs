﻿using GAB.Inova.Common.ViewModels.Base;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.Consumo
{
    public class HistoricoConsumosResponseViewModel
        : BaseResponseViewModel
    {

        public HistoricoConsumosResponseViewModel()
        {
            ConsumosMatricula = new HashSet<ConsumosMatriculaViewModel>();
        }

        public ICollection<ConsumosMatriculaViewModel> ConsumosMatricula { get; set; }

    }
}
