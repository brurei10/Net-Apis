﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Servico
{
    public class OsEletronicaViewModel
    {
        #region Header

        [Description("Identificativo único de mensagem lado iNova")]
        public string IdentificadorRequisicao { get; set; }

        [Description("Sigla da concessão a qual pertence a Ordem de Serviço")]
        public string Empresa { get; set; }

        [Description("Latitude")]
        public decimal? Latitude { get; set; }

        [Description("Longitude")]
        public decimal? Longitude { get; set; }

        [Description("Centro Operativo")]
        public string CentroOperativo { get; set; }

        #endregion

        #region OS

        [Description("Identificador único da Ordem de Serviço - Ex: CAN1058470")]
        public string IdentificadorOS { get; set; }

        [Description("Identificador interno do sistema especialista para a Ordem de Servico")]
        public string IdentificadorExterno { get; set; }

        [Description("Código Tipo do serviço")]
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public long CodigoServico { get; set; }

        public int? SequencialVinculada { get; set; }

        public DateTime? DataHoraInicioExecucao { get; set; }

        public DateTime? DataHoraFimExecucao { get; set; }

        public string EquipeExecutora { get; set; }

        public long? MotivoAberturaID { get; set; }

        public long? MotivoBaixaID { get; set; }

        public bool? PossuiBomba { get; set; }

        public long? NumeroNotificacao { get; set; }

        public bool? EhTrocaTitularidadeID { get; set; }

        public long? SituacaoImovelID { get; set; }

        public string HidrometroID { get; set; }

        public int? LeituraDeRetirada { get; set; }

        public string HidrometroInstaladoID { get; set; }

        public int? LeituraDeInstalacao { get; set; }

        public long? LocalInstalacaoHidrometroID { get; set; }

        public long? PadraoInstalacaoHidrometroID { get; set; }

        public string NumeroSeloVirola { get; set; }

        public string NumeroLacreInmetro { get; set; }

        public int? Leitura { get; set; }

        public string Laudo { get; set; }

        public string Observacao { get; set; }

        public virtual int? TesteCloro { get; set; }

        #endregion

        #region Contrato 

        public string NomeOuRazaoSocial { get; set; }

        public string CPFCNPJ { get; set; }

        public string Celular { get; set; }

        public DateTime? DataNascimento { get; set; }

        public string NomeMae { get; set; }

        public string NumeroRG { get; set; }

        public string Telefone { get; set; }

        #endregion

        #region Endereco

        public string TipoLogradouro { get; set; }

        public string Logradouro { get; set; }

        public int? Numero { get; set; }

        public string Complemento { get; set; }

        public string PontoDeReferencia { get; set; }

        public long? BairroID { get; set; }

        public string CidadeID { get; set; }

        #endregion

        #region Cliente

        public long? CategoriaID { get; set; }

        public long? SubCategoriaID { get; set; }

        public bool? EhAreaRisco { get; set; }

        public int? NumeroEconomiasComercial { get; set; }

        public int? NumeroEconomiasResidencial { get; set; }

        public int? NumeroEconomiasIndustrial { get; set; }

        public int? NumeroEconomiasPublico { get; set; }

        public long? DiametroRedeID { get; set; }

        public long? DiametroHidrometroID { get; set; }

        public long? DiametroRamalID { get; set; }

        public long? FrequenciaUtilizacaoImovelID { get; set; }

        public long? LocalizacaoRamalID { get; set; }

        public string MaterialRede { get; set; }

        public string MaterialRamal { get; set; }

        public long? ProfundidadeRedeID { get; set; }

        public long? ProfundidadeRamalID { get; set; }

        public long? TipoFonteAlternativaID { get; set; }

        public long? TipoIrregularidadeID { get; set; }

        public long? TipoPavimentoCalcadaID { get; set; }

        public long? TipoPavimentoRuaID { get; set; }

        public bool? TemCaixaCorreios { get; set; }

        public bool? EhClienteFlutuante { get; set; }

        public bool? TemReloajoariaPreEquipada { get; set; }

        public bool? TemFonteAlternativa { get; set; }

        public bool? TemBoiaCaixaDagua { get; set; }

        public bool? TemBoiaSisterna { get; set; }

        public bool? TemEquipamentoTelemetriaAcoplada { get; set; }

        public bool? TemValvulaRetencao { get; set; }

        public int? NumeroFuncionarios { get; set; }

        public int? NumeroMoradores { get; set; }

        public int? QuantidadeBanheirosOuLavabos { get; set; }

        public int? QuantidadeLojas { get; set; }

        public int? QuantidadeQuartosImovel { get; set; }

        public int? QuantidadeSalas { get; set; }

        public long? VazaoComBomba { get; set; }

        public long? VazaoSemBomba { get; set; }

        public long? VolumeCaixaDagua { get; set; }

        public long? VolumeCisterna { get; set; }

        public long? VolumePiscina { get; set; }

        #endregion

        public ICollection<OsEletronicaLinkAnexoViewModel> Fotos { get; set; }

        public OsEletronicaViewModel()
        {
            Fotos = new HashSet<OsEletronicaLinkAnexoViewModel>();
        }
    }
}
