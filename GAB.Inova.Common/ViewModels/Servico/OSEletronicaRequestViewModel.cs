﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GAB.Inova.Common.ViewModels.Servico
{
    public class OsEletronicaRequestViewModel
    {
        [Description("Ordem de Serviço Principal")]
        public OsEletronicaViewModel OrdemServicoPrincipal { get; set; }
        
        [Description("Ordens de serviço Associadas")]
        public ICollection<OsEletronicaViewModel> OrdensServicoAssociadas { get; set; }

        public OsEletronicaRequestViewModel()
        {
            OrdensServicoAssociadas = new HashSet<OsEletronicaViewModel>();
        }
    }
}
