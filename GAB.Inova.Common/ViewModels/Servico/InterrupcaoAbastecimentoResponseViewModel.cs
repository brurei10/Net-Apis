﻿using GAB.Inova.Common.ViewModels.Base;
using System;

namespace GAB.Inova.Common.ViewModels.Servico
{
    public class InterrupcaoAbastecimentoResponseViewModel : BaseResponseViewModel
    {
        public DateTime? DataHoraInicio { get; set; }
        public DateTime? DataHoraFim { get; set; }        
    }
}
