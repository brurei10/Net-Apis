﻿using System.ComponentModel.DataAnnotations;

namespace GAB.Inova.Common.ViewModels.Servico
{
    public class OsEletronicaLinkAnexoViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        public string Identificador { get; set; }
    }
}
