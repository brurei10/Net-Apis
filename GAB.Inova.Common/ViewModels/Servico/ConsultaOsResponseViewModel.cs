﻿using GAB.Inova.Common.ViewModels.Base;
using System;

namespace GAB.Inova.Common.ViewModels.Servico
{
    public class ConsultaOsResponseViewModel : BaseResponseViewModel
    {

        public long? NumeroOs { get; set; }
        public int? SituacaoOs { get; set; }
        public string DescricaoSituacaoOs { get; set; }
        public DateTime? DataPrevista { get; set; }
        public DateTime? DataProgramada { get; set; }
        public DateTime? DataBaixa { get; set; }
        public string Matricula { get; set; }
        public string CodigoServico { get; set; }
        public string DescricaoServico { get; set; }

        public ConsultaOsResponseViewModel()
        {
            this.Sucesso = false;
        }
    }
}
