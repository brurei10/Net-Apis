﻿using GAB.Inova.Common.Enums;
using System.Collections.Generic;

namespace GAB.Inova.Common.ViewModels.SendGrid
{
    public class EmailRequestViewModel
    {

        #region [Attributes]

        ISet<AnexoEmailViewModel> _anexos;
        ISet<KeyValuePair<string, string>> _customArgs;

        #endregion

        #region [Properties]

        public string CodigoEmpresa { get; set; }
        public string RemetenteEnderecoEmail { get; set; }
        public string RemetenteNome { get; set; }
        public string Destinatarios { get; set; }
        public string DestinatariosCopia { get; set; }
        public string DestinatariosCopiaOculta { get; set; }
        public object Conteudo { get; set; }
        public string TemplateId { get; set; }
        public MensagemIntegradorTipoEnum Tipo { get; set; }
        public string ReplayToNome { get; set; }
        public string ReplayToEmail { get; set; }

        public IEnumerable<AnexoEmailViewModel> Anexos { get { return _anexos; } }
        public IEnumerable<KeyValuePair<string, string>> CustomArgs { get { return _customArgs; } }

        #endregion

        public EmailRequestViewModel()
        {
            _anexos = new HashSet<AnexoEmailViewModel>();
            _customArgs = new HashSet<KeyValuePair<string, string>>();
        }

        public void AnexoAdiciona(AnexoEmailViewModel model)
        {
            _anexos.Add(model);
        }
        public void CustomArgAdiciona(KeyValuePair<string, string> keyValue)
        {
            _customArgs.Add(keyValue);
        }

    }
}
