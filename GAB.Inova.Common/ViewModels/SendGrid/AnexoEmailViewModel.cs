﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels.SendGrid
{
    public class AnexoEmailViewModel
    {

        public AnexoEmailViewModel()
        {
            Disposicao = "attachment";
        }

        public string ArquivoBase64 { get; set; }
        public string Tipo { get; set; }
        public string NomeArquivo { get; set; }
        public string Disposicao { get; set; }

    }
}
