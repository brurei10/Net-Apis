﻿using GAB.Inova.Common.ViewModels.Base;

namespace GAB.Inova.Common.ViewModels.SendGrid
{
    public class EmailResponseViewModel
        : BaseResponseViewModel
    {

        public string Guid { get; set; }

    }
}
