﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.ViewModels
{
    public class ContaViewModel
    {
        public virtual long Numero { get; set; }
        public virtual string AnoMes { get; set; }
        public virtual int Ciclo { get; set; }
    }
}
