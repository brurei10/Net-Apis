﻿namespace GAB.Inova.Common.ViewModels.Mensagem
{

    public class AtualizarStatusMensagemViewModel
    {
        public int Timestamp { get; set; }
        public string MensagemId { get; set; }
        public string Event { get; set; }        
        
    }
}
