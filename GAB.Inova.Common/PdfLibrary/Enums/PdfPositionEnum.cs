﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfPositionEnum
    {
        Relative = 0,
        Absolute = 1
    }
}
