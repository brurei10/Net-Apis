﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfPageSizeEnum
    {
        CUSTOM = 0,
        A4 = 1,
        LETTER = 2
    }
}
