﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfIndentationParagraphEnum
    {
        NOME = 0,
        _2PX = 2,
        _4PX = 4,
        _6PX = 6,
        _8XP = 8
    }
}
