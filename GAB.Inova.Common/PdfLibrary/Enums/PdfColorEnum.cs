﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfColorEnum
    {
        BLUE = 0,
        BLACK = 1,
        BROWN = 2,
        GRAY = 3,
        GREEN = 4,
        ORANGE = 5,
        RED = 6,
        WHITE = 7,
        YELLOW = 8
    }
}
