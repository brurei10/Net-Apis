﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfFontFormatEnum
    {
        NONE = 0,
        BOLD = 1,
        ITALIC = 2,
        BOLD_ITALIC = 3,
        UNDERLINE = 4
    }
}
