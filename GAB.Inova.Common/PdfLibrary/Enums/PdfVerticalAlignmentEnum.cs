﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfVerticalAlignmentEnum
    {
        TOP     = 4,
        MIDDLE  = 5,
        BOTTON  = 6
    }
}
