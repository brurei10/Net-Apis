﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfFontNameEnum
    {
        ARIAL = 0,
        CALIBRI = 1,
        CENTURYGOTHIC = 2,
        HELVETICA = 3,
        TAHOMA = 4,
        TIMES = 5,
        VERDANA = 6,
        COURIERNEW = 7
    }
}
