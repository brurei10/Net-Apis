﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfPicturePositionEnum
    {
        FRONT = 0,
        BACK = 1
    }
}
