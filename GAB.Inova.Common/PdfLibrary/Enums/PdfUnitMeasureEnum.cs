﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfUnitMeasureEnum
    {
        PIXEL   = 0,
        MM      = 1,
        CM      = 2
    }
}
