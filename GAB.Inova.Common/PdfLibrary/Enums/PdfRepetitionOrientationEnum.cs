﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfRepetitionOrientationEnum
    {
        None = 0,
        Horizontal = 1,
        Vertical = 2
    }
}
