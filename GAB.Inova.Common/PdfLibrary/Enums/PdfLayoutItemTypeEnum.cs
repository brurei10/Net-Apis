﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfLayoutItemTypeEnum
    {
        Group = 0,  //Não possui formato, seu conteúdo é composto pelos elementos filho...
        Text = 1,
        Rectangle = 2,
        Barcode = 3,
        Line = 4,
        Image = 5
    }
}
