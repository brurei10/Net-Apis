﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfBorderTypeEnum
    {
        NONE = 0,
        TOP = 1,
        BOTTOM = 2,
        TOPBOTTOM = 3,
        VERTICAL = 12,
        COMPLETE = 15
    }
}
