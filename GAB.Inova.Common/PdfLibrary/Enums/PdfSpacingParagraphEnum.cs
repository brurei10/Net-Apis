﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfSpacingParagraphEnum
    {
        NONE = 0,
        _6PX = 6,
        _12PX = 12,
        _18PX = 18,
        _24PX = 24,
        _32PX = 32
    }
}
