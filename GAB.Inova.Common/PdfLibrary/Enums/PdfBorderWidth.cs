﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfBorderWidth
    {
        _1PX = 0,
        _2PX = 1,
        _3PX = 2,
        _4PX = 3,
        _5PX = 4
    }
}
