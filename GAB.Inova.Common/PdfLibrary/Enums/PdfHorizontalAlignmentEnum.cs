﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfHorizontalAlignmentEnum
    {
        LEFT = 0,
        CENTER = 1,
        RIGHT = 2,
        JUSTIFIED = 3
    }
}
