﻿namespace GAB.Inova.Common.PdfLibrary.Enums
{
    public enum PdfPageOrientationEnum
    {
        NONE = 0,
        PORTRAIT = 1,
        LANDSCAPE = 2
    }
}
