﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using textSharpLGPL::iTextSharp.text.pdf;
using System;

namespace GAB.Inova.Common.PdfLibrary.Components.Table
{
    public class PdfColumn
    {

        #region Attributes

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        
        #endregion
        
        #region Properties

        public int Id { get; private set; }

        public float Width { get; private set; }

        public Guid UniqueId { get; private set; }

        public string Text { get; set; }

        #endregion
        
        #region [Constructor]

        public PdfColumn(PdfDoc pdfDoc, int id, float width)
        {
            UniqueId = Guid.NewGuid();
            Id = id;
            Width = width;

            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;
        }

        #endregion

    }
}
