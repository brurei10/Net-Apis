﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text.pdf;
using System.Collections;
using System.Collections.Generic;

namespace GAB.Inova.Common.PdfLibrary.Components.Table
{
    public sealed class PdfRowCollection
        : IEnumerable
    {

        #region atributes

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        readonly List<PdfRow> _rows;
        readonly PdfSmallInt _numberOfCols;
        readonly float[] _widthColumn;
        
        #endregion

        #region [Constructor]

        public PdfRowCollection(PdfDoc pdfDoc, PdfSmallInt numberOfCols, float[] widthColumn)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc?.Writer;

            _rows = new List<PdfRow>();
            _numberOfCols = numberOfCols;
            _widthColumn = widthColumn;
        }

        #endregion
        
        #region [Properties]

        public PdfRow this[int index]
        {
            get
            {
                return _rows[index];
            }
            set
            {
                _rows[index] = value;
            }
        }

        public int Count
        {
            get { return _rows.Count; }
        }

        public IEnumerator<PdfRow> GetEnumerator()
        {
            return _rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rows.GetEnumerator();
        }

        #endregion 
        
        #region [Public methods]

        public void Add()
        {
            _rows.Add(new PdfRow(_pdfDoc, _rows.Count, _numberOfCols, _widthColumn));
        }

        public void Clear()
        {
            _rows.Clear();
        }

        public bool Contains(PdfRow item)
        {
            foreach (var row in _rows)
            {
                if (row.UniqueId == item.UniqueId) return true;
            }
            return false;
        }

        public void RemoveLast()
        {
            _rows.RemoveAt(_rows.Count - 1);
        }

        #endregion

    }
}
