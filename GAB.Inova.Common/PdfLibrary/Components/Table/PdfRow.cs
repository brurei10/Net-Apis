﻿using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using System;

namespace GAB.Inova.Common.PdfLibrary.Components.Table
{
    public class PdfRow
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        
        #endregion
        
        #region [Properties]

        public int Id { get; private set; }
        public Guid UniqueId { get; private set; }
        public PdfColumnCollection Columns { get; set; }

        #endregion
        
        #region [Constructor]

        public PdfRow(PdfDoc pdfDoc, int id, PdfSmallInt numberOfCols, float[] widthColumn)
        {
            _pdfDoc = pdfDoc;

            Id = id;
            UniqueId = Guid.NewGuid();
            Columns = new PdfColumnCollection(_pdfDoc, numberOfCols, widthColumn);
        }

        #endregion

    }
}
