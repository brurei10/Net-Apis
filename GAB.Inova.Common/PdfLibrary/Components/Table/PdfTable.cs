﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text.pdf;
using System.Linq;

namespace GAB.Inova.Common.PdfLibrary.Components.Table
{
    public sealed class PdfTable
    {

        #region [Atributtes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        
        #endregion

        #region [Properties]

        public bool UseHeader { get; set; }
        public PdfRowCollection Rows { get; set; }

        #endregion
        
        #region [Constructor]

        public PdfTable(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc?.Writer;
        }

        #endregion
        
        #region [Public methods]

        public void Create(float x, float y, PdfSmallInt numberOfCols, float width)
        {
            var widthColumns = new float[numberOfCols];
            var widthColumn = width / numberOfCols;
            
            for (var i = 0; i < numberOfCols; i++)
            {
                widthColumns[i] = widthColumn;
            }

            Rows = new PdfRowCollection(_pdfDoc, numberOfCols, widthColumns);
        }

        public void Create(float x, float y, float[] width)
        {
            var _numberOfCols = width.Count();

            Rows = new PdfRowCollection(_pdfDoc, _numberOfCols, width);
        }

        #endregion

    }
}
