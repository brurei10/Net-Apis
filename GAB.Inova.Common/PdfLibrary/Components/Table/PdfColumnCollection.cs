﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text.pdf;
using System.Collections;
using System.Collections.Generic;

namespace GAB.Inova.Common.PdfLibrary.Components.Table
{
    public sealed class PdfColumnCollection
        : IEnumerable
    {

        #region [Atributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        readonly List<PdfColumn> _columns;

        #endregion
        
        #region [Constructor]

        public PdfColumnCollection(PdfDoc pdfDoc, PdfSmallInt numberOfCols, float[] widthColumn)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc?.Writer;

            _columns = new List<PdfColumn>();

            for (var i = 0; i < numberOfCols; i++)
            {
                _columns.Add(new PdfColumn(pdfDoc, (i + 1), widthColumn[i]));
            }
        }

        #endregion
        
        #region [Properties]

        public PdfColumn this[int index]
        {
            get
            {
                return _columns[index];
            }
            set
            {
                _columns[index] = value;
            }
        }

        public int Count
        {
            get { return _columns.Count; }
        }

        public IEnumerator<PdfColumn> GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        #endregion 
        
        #region [Public Methods]

        public bool Contains(PdfColumn item)
        {
            foreach (var column in _columns)
            {
                if (column.UniqueId == item.UniqueId) return true;
            }
            return false;
        }

        #endregion

    }
}
