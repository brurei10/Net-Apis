﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Enums;
using textSharpLGPL::iTextSharp.text;
using textSharpLGPL::iTextSharp.text.pdf;

namespace GAB.Inova.Common.PdfLibrary.Components.Drawing
{
    public class PdfPicture
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;

        #endregion
        
        #region [Constructor]

        public PdfPicture(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;
        }

        #endregion
        
        #region [Public methods]

        public void Add(float x, float y, string filePath, float scalarPercent, PdfPicturePositionEnum pdfPicturePosition)
        {
            var tif = Image.GetInstance(filePath);

            tif.ScalePercent(scalarPercent);

            var _y = _pdfDoc.Page.GetHeightUnitMeasure(y) - tif.PlainHeight;
            var _x = _pdfDoc.Page.GetWidthUnitMeasure(x);

            tif.SetAbsolutePosition(_x, _y);

            if (pdfPicturePosition == PdfPicturePositionEnum.BACK)
            {
                _pdfWriter.DirectContentUnder.AddImage(tif);
            }
            else
            {
                _pdfWriter.DirectContent.AddImage(tif);
            }
        }

        public void Add(float x, float y, string filePath)
        {
            Add(x, y, filePath, 100f, PdfPicturePositionEnum.BACK);
        }

        public void Add(float x, float y, string filePath, PdfPicturePositionEnum position)
        {
            Add(x, y, filePath, 100f, position);
        }

        public void Add(float x, float y, string filePath, float scalarPercent)
        {
            Add(x, y, filePath, scalarPercent, PdfPicturePositionEnum.BACK);
        }

        public void Add(float x, float y, byte[] buffer, float scalarPercent)
        {
            Add(x, y, buffer, scalarPercent, PdfPicturePositionEnum.BACK);
        }
        
        public void Add(float x, float y, byte[] buffer, float scalarPercent, PdfPicturePositionEnum position)
        {
            Image tif = Image.GetInstance(buffer);

            tif.ScalePercent(scalarPercent);

            var _y = _pdfDoc.Page.GetHeightUnitMeasure(y) - tif.PlainHeight;
            var _x = _pdfDoc.Page.GetWidthUnitMeasure(x);

            tif.SetAbsolutePosition(_x, _y);

            if (position == PdfPicturePositionEnum.BACK)
            {
                _pdfWriter.DirectContentUnder.AddImage(tif);
            }
            else
            {
                _pdfWriter.DirectContent.AddImage(tif);
            }
        }

        public void Add(float x, float y, byte[] buffer, PdfPicturePositionEnum position)
        {
            Add(x, y, buffer, 100f, position);
        }

        #endregion

    }
}
