﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text.pdf;

namespace GAB.Inova.Common.PdfLibrary.Components.Drawing
{
    public class PdfRectangle
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;

        #endregion
        
        #region [Constructor]

        public PdfRectangle(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;
        }

        #endregion
        
        #region [Public methods]

        public void Add(float x, float y, float width, float height, PdfColor BackgroundColor)
        {
            Add(x, y, width, height, PdfColor.White, 0, BackgroundColor);
        }

        public void Add(float x, float y, float width, float height, PdfColor BorderColor, float BorderSize)
        {
            Add(x, y, width, height, BorderColor, BorderSize, PdfColor.White);
        }

        public void Add(float x, float y, float width, float height, PdfColor borderColor, float borderSize, PdfColor backgroundColor)
        {
            var _y2 = _pdfDoc.Page.GetHeightUnitMeasure(y + height);
            var _x2 = _pdfDoc.Page.GetWidthUnitMeasure(x + width);
            var _x1 = _pdfDoc.Page.GetWidthUnitMeasure(x);
            var _y1 = _pdfDoc.Page.GetHeightUnitMeasure(y);

            var _height = _y1 - _y2;
            var _width = _x2 - _x1;

            var canvas = _pdfWriter.DirectContentUnder;

            canvas.SaveState();
            canvas.Rectangle(_x1, _y2, _width, _height);
            canvas.SetColorFill(backgroundColor.GetBaseColor());

            if (borderSize > 0)
            {
                canvas.SetColorStroke(borderColor.GetBaseColor());
                canvas.SetLineWidth(borderSize);
                canvas.FillStroke();
            }

            canvas.Fill();
            canvas.RestoreState();
        }

        #endregion

        #region [Private methods]

        private void Add(float x, float y, float width, float height, float x1, float y1, float x2, float y2, PdfColor backgroundColorBegin, PdfColor backgroundColorEnd)
        {
            var canvas = _pdfWriter.DirectContentUnder;

            canvas.SaveState();
            canvas.SetShadingFill(new PdfShadingPattern(PdfShading.SimpleAxial(_pdfWriter, x1, y1, x2, y2, backgroundColorBegin.GetBaseColor(), backgroundColorEnd.GetBaseColor())));
            canvas.Rectangle(x, y, width, height);
            canvas.Fill();
            canvas.RestoreState();
        }

        #endregion

    }
}
