﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using textSharpLGPL::iTextSharp.text.pdf;


namespace GAB.Inova.Common.PdfLibrary.Components.Drawing
{
    public class PdfDraw
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        
        #endregion
        
        #region [Properties]

        public PdfBarcode Barcode { get; set; }
        public PdfLine Line { get; set; }
        public PdfPicture Picture { get; set; }
        public PdfRectangle Rectangle { get; set; }

        #endregion
        
        #region [Constructor]

        public PdfDraw(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;

            Barcode = new PdfBarcode(pdfDoc);
            Line = new PdfLine(pdfDoc);
            Picture = new PdfPicture(pdfDoc);
            Rectangle = new PdfRectangle(pdfDoc);
        }

        #endregion

    }
}
