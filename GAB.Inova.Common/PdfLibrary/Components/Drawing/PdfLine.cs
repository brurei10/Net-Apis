﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text.pdf;

namespace GAB.Inova.Common.PdfLibrary.Components.Drawing
{
    public class PdfLine
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;

        #endregion
        
        #region [Constructor]

        public PdfLine(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;
        }

        #endregion
        
        #region [Public methods]

        public void Add(float x, float y, float x1, float y1, PdfColor pdfColor)
        {
            var _y = _pdfDoc.Page.GetHeightUnitMeasure(y);
            var _x = _pdfDoc.Page.GetWidthUnitMeasure(x);
            var _x1 = _pdfDoc.Page.GetWidthUnitMeasure(x1);
            var _y1 = _pdfDoc.Page.GetHeightUnitMeasure(y1);

            var canvas = _pdfWriter.DirectContentUnder;

            canvas.SaveState();
            canvas.SetColorStroke(pdfColor.GetBaseColor());
            canvas.MoveTo(_x, _y);
            canvas.LineTo(_x1, _y1);
            canvas.SetLineWidth(0.5f);
            canvas.Stroke();
            canvas.RestoreState();
        }

        #endregion

    }
}
