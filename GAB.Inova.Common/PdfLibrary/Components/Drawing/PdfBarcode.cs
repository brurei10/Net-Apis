﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using textSharpLGPL::iTextSharp.text;
using textSharpLGPL::iTextSharp.text.pdf;

namespace GAB.Inova.Common.PdfLibrary.Components.Drawing
{
    public class PdfBarcode
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;

        #endregion

        #region [Constructor]

        public PdfBarcode(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc.Writer;
        }

        #endregion

        #region [Public methods]

        public void Add(float x, float y, float height, float width, string code)
        {
            var _x1 = _pdfDoc.Page.GetWidthUnitMeasure(width);
            var _y1 = _pdfDoc.Page.GetOnlyHeightUnitMeasure(height);

            BarcodeInter25 bc = new BarcodeInter25()
            {
                Code = code,
                StartStopText = false,
                CodeType = BarcodeInter25.CODE128,
                AltText = string.Empty
            };
            
            Image tif = bc.CreateImageWithBarcode(_pdfWriter.DirectContentUnder, BaseColor.Black, BaseColor.Black);

            tif.ScaleAbsolute(_x1, _y1);

            var _x = _pdfDoc.Page.GetWidthUnitMeasure(x);
            var _y = _pdfDoc.Page.GetHeightUnitMeasure(y) - tif.PlainHeight;

            tif.SetAbsolutePosition(_x, _y);

            _pdfWriter.DirectContent.AddImage(tif);
        }

        #endregion

    }
}
