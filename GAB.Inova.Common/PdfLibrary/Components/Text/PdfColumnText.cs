﻿extern alias textSharpLGPL;

using textSharpLGPL::iTextSharp.text.pdf;

namespace GAB.Inova.Common.PdfLibrary.Components.Text
{
    public class PdfColumnText
        : ColumnText
    {

        public PdfColumnText(PdfContentByte canvas)
            : base(canvas) { }

        public float GetCurrentLeading()
        {
            return base.CurrentLeading;
        }

    }
}
