﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Entities;
using GAB.Inova.Common.PdfLibrary.Enums;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text;
using textSharpLGPL::iTextSharp.text.pdf;
using System;

namespace GAB.Inova.Common.PdfLibrary.Components.Text
{
    public sealed class PdfTextBox
    {

        #region [Attributes]

        readonly PdfDoc _pdfDoc;
        readonly PdfWriter _pdfWriter;
        
        #endregion
        
        #region Propriedades

        public Entities.PdfFont Font { get; set; }
        public PdfHorizontalAlignmentEnum HorizontalAlignment { get; set; }
        public PdfVerticalAlignmentEnum VerticalAlignment { get; set; }
        public bool ShadowText { get; set; }

        #endregion
        
        #region [Construtor]

        public PdfTextBox(PdfDoc pdfDoc)
        {
            _pdfDoc = pdfDoc;
            _pdfWriter = _pdfDoc?.Writer;

            Font = _pdfDoc.Font;
            HorizontalAlignment = PdfHorizontalAlignmentEnum.LEFT;
            VerticalAlignment = PdfVerticalAlignmentEnum.TOP;
        }

        #endregion

        #region [Public methods]

        public void Add(float x, float y, float height, float width, string text)
        {
            Add(x, y, height, width, text, ShadowText, HorizontalAlignment, VerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, ShadowText, HorizontalAlignment, VerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, PdfVerticalAlignmentEnum pdfVerticalAlignment)
        {
            Add(x, y, height, width, text, ShadowText, HorizontalAlignment, pdfVerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, PdfVerticalAlignmentEnum pdfVerticalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, ShadowText, HorizontalAlignment, pdfVerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment)
        {
            Add(x, y, height, width, text, ShadowText, pdfHorizontalAlignment, pdfVerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, ShadowText, pdfHorizontalAlignment, pdfVerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, PdfHorizontalAlignmentEnum pdfHorizontalAlignment)
        {
            Add(x, y, height, width, text, ShadowText, pdfHorizontalAlignment, VerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, ShadowText, pdfHorizontalAlignment, VerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText)
        {
            Add(x, y, height, width, text, shadowText, HorizontalAlignment, VerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, shadowText, HorizontalAlignment, VerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfVerticalAlignmentEnum pdfVerticalAlignment)
        {
            Add(x, y, height, width, text, shadowText, HorizontalAlignment, pdfVerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfVerticalAlignmentEnum pdfVerticalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, shadowText, HorizontalAlignment, pdfVerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfHorizontalAlignmentEnum pdfHorizontalAlignment)
        {
            Add(x, y, height, width, text, shadowText, pdfHorizontalAlignment, VerticalAlignment);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, shadowText, pdfHorizontalAlignment, VerticalAlignment, pdfFont);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment)
        {
            Add(x, y, height, width, text, shadowText, pdfHorizontalAlignment, pdfVerticalAlignment, Font);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment, Entities.PdfFont pdfFont)
        {
            Add(x, y, height, width, text, shadowText, pdfHorizontalAlignment, pdfVerticalAlignment, pdfFont, 0);
        }

        public void Add(float x, float y, float height, float width, string text, bool shadowText, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment, Entities.PdfFont pdfFont, float wordWrapleading)
        {
            var numberOfLines = 1;
            var leading = (float)0;
            var canvas = _pdfWriter.DirectContent;

            var _y2 = _pdfDoc.Page.GetHeightUnitMeasure(y + height);
            var _x2 = _pdfDoc.Page.GetWidthUnitMeasure(x + width);
            var _x1 = _pdfDoc.Page.GetWidthUnitMeasure(x);
            var _y1 = _pdfDoc.Page.GetHeightUnitMeasure(y);

            var _height = _y1 - _y2;
            var _width = _x2 - _x1;

            if (pdfFont == null)
            {
                pdfFont = Font;
            }

            var ct = new PdfColumnText(canvas);
            var phrase = GetPhrase(text, pdfFont);

            ct.UseAscender = true;
            ct.SetSimpleColumn(phrase, _x1, _y2, _x2, _y1, wordWrapleading, (int)pdfHorizontalAlignment);

            ct.Go(true);

            numberOfLines = ct.LinesWritten;

            ct.UseAscender = true;
            ct.SetSimpleColumn(phrase, 0, _y2, _pdfDoc.Page.Rec.Width, _y1, wordWrapleading, (int)pdfHorizontalAlignment);

            ct.Go(true);

            leading = (ct.GetCurrentLeading() * 1.5f);

            var difLead = leading - ct.GetCurrentLeading();

            switch (pdfVerticalAlignment)
            {
                case PdfVerticalAlignmentEnum.BOTTON:
                    _y1 = _y2 + (leading * numberOfLines) - difLead;
                    break;
                case PdfVerticalAlignmentEnum.MIDDLE:
                    _y1 = _y2 + ((leading * numberOfLines) + ((_height / 2) - ((leading * numberOfLines) / 2))) - (difLead / 2);
                    break;
            }

            var shadowColorText = PdfColor.FromRGB(217, 217, 217);
            var shadowHeightText = (float)1f;

            if (ShadowText)
            {
                ct.UseAscender = true;
                ct.SetSimpleColumn(GetPhrase(text, shadowColorText, pdfFont), (_x1 + shadowHeightText), (_y2 - shadowHeightText), (_x2 + shadowHeightText), (_y1 - shadowHeightText), (numberOfLines == 1 ? 0 : leading), (int)pdfHorizontalAlignment);
                ct.Go();
            }

            // Adiciona o texto com a cor normal
            ct.UseAscender = true;
            ct.SetSimpleColumn(phrase, _x1, _y2, _x2, _y1, (numberOfLines == 1 ? 0 : leading), (int)pdfHorizontalAlignment);
            ct.Go();
        }

        public void Add(float x, float y, float height, float width, string text, float rotate, Entities.PdfFont pdfFont, PdfHorizontalAlignmentEnum pdfHorizontalAlignment, PdfVerticalAlignmentEnum pdfVerticalAlignment)
        {
            PdfContentByte canvas = _pdfWriter.DirectContent;

            var _y2 = _pdfDoc.Page.GetHeightUnitMeasure(y + height);
            var _x2 = _pdfDoc.Page.GetWidthUnitMeasure(x + width);
            var _x1 = _pdfDoc.Page.GetWidthUnitMeasure(x);
            var _y1 = _pdfDoc.Page.GetHeightUnitMeasure(y);

            var _height = _y1 - _x2;
            var _width = _x2 - _x1;
            var _YAux = _y1 - _width;

            var _y = 0F;

            var template = canvas.CreateTemplate(_width, _height);

            var ct = new PdfColumnText(template);

            ct.UseAscender = true;
            ct.SetSimpleColumn(GetPhrase(text, pdfFont), 0, 0, _width, _height, 1, (int)pdfHorizontalAlignment);
            ct.Go(true);

            var numberOfLines = ct.LinesWritten;

            ct.UseAscender = true;
            ct.SetSimpleColumn(GetPhrase(text, pdfFont), 0, 0, _pdfDoc.Page.Rec.Width, _height, 1, (int)pdfHorizontalAlignment);

            ct.Go(true);

            var leading = (ct.GetCurrentLeading() * 1.5F);
            var difLead = leading - ct.GetCurrentLeading();

            switch (VerticalAlignment)
            {
                case PdfVerticalAlignmentEnum.BOTTON:
                    _y = _height - (_height - (leading * numberOfLines)) - difLead;
                    break;
                case PdfVerticalAlignmentEnum.MIDDLE:
                    _y = _height - ((_height - (leading * numberOfLines)) / 2) - (difLead / 2);
                    break;
                default:
                    _y = 0;
                    break;
            }

            ct.UseAscender = true;
            ct.SetSimpleColumn(GetPhrase(text, pdfFont), 0, _y, _width, 0, (numberOfLines == 1 ? 0 : leading), (int)pdfHorizontalAlignment);

            ct.Go();

            //Create de image wrapper for the template
            Image textImg = Image.GetInstance(template);

            //Asign the dimentions of the image, in this case, the text
            textImg.Interpolation = (true);
            textImg.ScalePercent(100);
            textImg.RotationDegrees = rotate;
            textImg.SetAbsolutePosition(_x1, _y1);

            canvas.AddImage(textImg);
        }

        public float[] GetTextDimension(string text, Entities.PdfFont pdfFont)
        {
            var baseFont = pdfFont.GetFontFactory().BaseFont;
            var width = baseFont.GetWidthPoint(text, Convert.ToSingle(pdfFont.FontSize)) / _pdfDoc.Page.GetPageUnitConvWidth();
            var height = baseFont.GetAscentPoint(text, Convert.ToSingle(pdfFont.FontSize)) / _pdfDoc.Page.GetPageUnitConvHeight();

            return new[]
            {
                width,
                height
            };
        }

        #endregion

        #region [Private methods]

        private Phrase GetPhrase(string text, Entities.PdfFont pdfFont)
        {
            return new Phrase(new Chunk(text, pdfFont.GetFontFactory()));
        }

        private Phrase GetPhrase(string text, PdfColor pdfColor, Entities.PdfFont pdfFont)
        {
            return new Phrase(new Chunk(text, GetCustomizedFont(pdfColor, pdfFont).GetFontFactory()));
        }

        private Entities.PdfFont GetCustomizedFont(PdfColor pdfColor, Entities.PdfFont pdfFont)
        {
            var font = (Entities.PdfFont)pdfFont.Clone();
            font.FontColor = pdfColor;
            return font;
        }

        #endregion

    }
}
