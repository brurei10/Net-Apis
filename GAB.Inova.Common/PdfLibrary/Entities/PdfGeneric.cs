﻿using GAB.Inova.Common.PdfLibrary.Enums;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfGeneric
    {

        #region [Private methods]

        /// <summary>
        /// Recupera o objeto de dados a ser usado para obter o valor do elemento
        /// </summary>
        /// <param name="layoutItemName">String que representa o nome do item de leiaute.</param>
        /// <param name="dataObjectToLayoutItemDictionary">Dicionário que contém o nome do objeto de onde o valor deve ser obtido.</param>
        /// <param name="dataObject">Retorna objeto que contém o valor.</param>
        /// <returns></returns>
        private static object TranslateLayoutItemToDataObject(string layoutItemName, Dictionary<string, string> dataObjectToLayoutItemDictionary, object dataObject)
        {
            //Recupera o nome da property cujo valor será recuperado no dataObject; 
            var translatedPropertyName = string.Empty;
            if (dataObjectToLayoutItemDictionary != null)
            {
                if (!dataObjectToLayoutItemDictionary.TryGetValue(layoutItemName, out translatedPropertyName))
                {
                    throw new Exception($"Key {layoutItemName} does not exists in dataObjectToLayoutItemDictionary.");
                }
            }
            else translatedPropertyName = layoutItemName;
            var propInfo = dataObject.GetType()?.GetProperty(translatedPropertyName);
            if (propInfo == null) return propInfo;
            return propInfo.GetValue(dataObject, null);
        }

        /// <summary>
        /// Gera um elemento de texto no documento
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="pageRepControlOffsetX"></param>
        /// <param name="pageRepControlOffsetY"></param>
        /// <param name="layoutItem"></param>
        /// <param name="dataObject"></param>
        private static void WriteItemToDocument(PdfDoc doc, float left, float top, PdfItemLayout layoutItem, Object dataObject)
        {
            switch (layoutItem.PdfLayoutItemType)
            {
                case PdfLayoutItemTypeEnum.Text:
                    doc.TextBox.Add(left,
                                    top,
                                    layoutItem.Style.Height,
                                    layoutItem.Style.Width,
                                    (dataObject != null ? dataObject.ToString() : String.Empty),
                                    layoutItem.Style.ShadowText,
                                    layoutItem.Style.HorizontalAlignment,
                                    layoutItem.Style.VerticalAlignment,
                                    layoutItem.Style.Font,
                                    layoutItem.Style.WordWrapLeading);
                    break;
                case PdfLayoutItemTypeEnum.Image:
                    try
                    {
                        if (!string.IsNullOrEmpty(dataObject.ToString()))
                            doc.Draw.Picture.Add(left,
                                                 top,
                                                 dataObject.ToString(),
                                                 24.5f, //23.975f, //???#Todo: Entender esse número.
                                                 PdfPicturePositionEnum.FRONT);
                    }
                    catch
                    {
                        doc.TextBox.Add(left,
                                    top,
                                    layoutItem.Style.Height,
                                    layoutItem.Style.Width,
                                    "File not found: " + dataObject.ToString(),
                                    false,
                                    PdfHorizontalAlignmentEnum.CENTER,
                                    PdfVerticalAlignmentEnum.MIDDLE);
                    }
                    break;
                case PdfLayoutItemTypeEnum.Barcode:
                    if (!string.IsNullOrEmpty(dataObject.ToString()))
                        doc.Draw.Barcode.Add(left, top, layoutItem.Style.Height, layoutItem.Style.Width, dataObject.ToString());
                    break;
                case PdfLayoutItemTypeEnum.Line:
                    throw new NotImplementedException("WriteLineToDocument ainda não foi implementado");
                case PdfLayoutItemTypeEnum.Rectangle:
                    throw new NotImplementedException("WriteRectangleToDocument ainda não foi implementado");
                default:
                    throw new Exception($"Tratamento para PdfLayoutItemType = {layoutItem.PdfLayoutItemType.ToString()} ainda não implementado.");
            }

        }

        /// <summary>
        /// Controla impressão de elementos de diversos tipos no documento
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="pageRepControlOffsetX"></param>
        /// <param name="pageRepControlOffsetY"></param>
        /// <param name="layoutItem"></param>
        /// <param name="DataObject"></param>
        /// <param name="dataObjectToLayoutItemDictionary"></param>
        private static void WriteObjectToDocument(PdfDoc doc, float pageRepControlOffsetX, float pageRepControlOffsetY, PdfItemLayout layoutItem, object dataObject, Dictionary<string, string> dataObjectToLayoutItemTranslator)
        {
            //Se o objeto é um agrupador...
            if ((layoutItem.Children != null) && (layoutItem.Children.Count > 0))
                //para cada item agrupado
                foreach (var child in layoutItem.Children)
                {
                    //Chama recursivamente o WriteObjectToDocument...
                    WriteObjectToDocument(doc, pageRepControlOffsetX, pageRepControlOffsetY, child, dataObject, dataObjectToLayoutItemTranslator);
                }
            else
            {
                //Se o dataObject é uma lista de strings...
                var dO = TranslateLayoutItemToDataObject(layoutItem.Name, dataObjectToLayoutItemTranslator, dataObject);
                if (dO != null)
                {
                    if (dO is List<String>)
                    {
                        float localRepsOffsetControl = 0;
                        int repQtyControl = 1;
                        foreach (var item in (List<String>)dO)
                        {
                            float calcLeft = 0, calcTop = 0;
                            switch (layoutItem.Style.RepetitionStyle)
                            {
                                case PdfRepetitionOrientationEnum.Horizontal:
                                    calcLeft = layoutItem.ComputedLeft + pageRepControlOffsetX + localRepsOffsetControl;
                                    calcTop = layoutItem.ComputedTop + pageRepControlOffsetY;
                                    localRepsOffsetControl = localRepsOffsetControl + layoutItem.Style.RepetitionOffset + pageRepControlOffsetX + layoutItem.TotalWidth;
                                    break;
                                case PdfRepetitionOrientationEnum.Vertical:
                                    calcLeft = layoutItem.ComputedLeft + pageRepControlOffsetX;
                                    calcTop = layoutItem.ComputedTop + pageRepControlOffsetY + localRepsOffsetControl;
                                    localRepsOffsetControl = localRepsOffsetControl + layoutItem.Style.RepetitionOffset + pageRepControlOffsetY + layoutItem.TotalHeight;
                                    break;
                                case PdfRepetitionOrientationEnum.None:
                                    calcLeft = layoutItem.ComputedLeft + pageRepControlOffsetX;
                                    calcTop = layoutItem.ComputedTop + pageRepControlOffsetY;
                                    break;
                            }

                            WriteItemToDocument(doc, calcLeft, calcTop, layoutItem, item);

                            if (layoutItem.Style.RepetitionStyle == PdfRepetitionOrientationEnum.None) break;
                            if (repQtyControl >= layoutItem.Style.RepetitionMax) break;
                            repQtyControl++;
                        }
                    }
                    else
                    {
                        WriteItemToDocument(doc, layoutItem.ComputedLeft + pageRepControlOffsetX, layoutItem.ComputedTop + pageRepControlOffsetY, layoutItem, TranslateLayoutItemToDataObject(layoutItem.Name, dataObjectToLayoutItemTranslator, dataObject));
                    }
                }
            }
        }

        #endregion
        
        #region [Public methods]

        /// <summary>
        /// Escreve os dados de cada object em DataObjectList, a partir da página atual, obedecendo o leiaute e estilo fornecido pelo objeto Layout.
        /// </summary>
        /// <param name="doc">Objeto que represaenta o documento ao qual os dados serão escritos.</param>
        /// <param name="itemsPerPage">Quantidade de itens por página.</param>
        /// <param name="repsOrientation">Orientação a utilizar no caso de mais de um item por página.</param>
        /// <param name="Layout">Definie o leiaute e estilo de cada página.</param>
        /// <param name="DataObjectList">Cada elemento nessa lista representa um objeto que contém os dados a serem escritos em uma página do documento, sendo uma página por objeto de dados em DataObjectList.</param>
        /// <param name="DataObjectToLayoutItemTranslator">Opcional - Hash que resolve o de x para entre o atributo do de dados e o seu leiaute. A chave desse hash é o nome do item de leiaute (Layout.Name) e o valor é o nome do atributo no elemento de dados.</param>
        public static void AddDataToDocument(PdfDoc doc, PdfDocumentLayout docModel, PdfItemLayout layout, List<Object> dataObjectList, Dictionary<string, string> dataObjectToLayoutItemTranslator)
        {
            if (doc == null || layout == null) throw new ArgumentNullException();

            //variável de controle usada para calcular o offset do item na página.
            float repControlOffsetX = 0f;
            float repControlOffsetY = 0f;

            int i = 0;
            while (i < dataObjectList.Count)
            {
                for (int j = 0; (j < docModel.ItemsPerPage) && (i < dataObjectList.Count); j++)
                {
                    if (j == 0)
                    {
                        repControlOffsetX = 0f;
                        repControlOffsetY = 0f;
                    }
                    else
                    {
                        if (docModel.RepsOrientation == PdfRepetitionOrientationEnum.Horizontal)
                            repControlOffsetX = (docModel.RepetitionOffset * j) + layout.TotalWidth;
                        else
                            repControlOffsetY = (docModel.RepetitionOffset * j) + layout.TotalHeight;
                    }
                    var item = dataObjectList[i];
                    WriteObjectToDocument(doc, repControlOffsetX, repControlOffsetY, layout, item, dataObjectToLayoutItemTranslator);
                    i++;

                }
                doc.NewPage();
            }
        }

        /// <summary>
        /// Creates a document
        /// </summary>
        /// <param name="docModel"></param>
        /// <returns></returns>
        public static PdfDoc CreateDocument(PdfDocumentLayout model, string directory, string fileName)
        {
            var doc = default(PdfDoc);

            if ((!string.IsNullOrEmpty(directory)) && (!string.IsNullOrEmpty(fileName)))
            {
                doc = new PdfDoc(directory, fileName);
            }
            else
            {
                doc = new PdfDoc();
            }

            if (model.PageSize == PdfPageSizeEnum.CUSTOM)
            {
                doc.Create(model.CustomPageHeight, model.CustomPageWidth, PdfUnitMeasureEnum.MM);
            }

            return doc;
        }

        #endregion

    }
}
