﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Components.Drawing;
using GAB.Inova.Common.PdfLibrary.Components.Text;
using PdfLibraryTable = GAB.Inova.Common.PdfLibrary.Components.Table;
using GAB.Inova.Common.PdfLibrary.Enums;
using textSharpLGPL::iTextSharp.text;
using textSharpLGPL::iTextSharp.text.pdf;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfDoc
    {

        #region [Atributes]

        Document _document;
        MemoryStream _ms;

        string _filePath;
        string _password;

        bool _ehFileStream;
        bool _isDocOpen;
        bool _isDocCreate;

        int _pageCount;

        #endregion

        #region [Properties]

        public int CurrentPage => _pageCount;
        public int NumOfPages => _pageCount;
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Author { get; set; }
        public bool DoEncrypt { get; private set; }

        public PdfWriter Writer { get; set; }
        public PdfFont Font { get; set; }
        public PdfPage Page { get; set; }
        public PdfTextBox TextBox { get; set; }
        public PdfDraw Draw { get; set; }
        public PdfLibraryTable.PdfTable Table { get; set; }
        
        #endregion

        #region [Constructor]

        public PdfDoc()
        {
            InitializedVariables();
        }

        public PdfDoc(string directory, string fileName)
        {
            InitializedVariables(directory, fileName);
        }

        #endregion

        #region [Public methods]

        public void Create(PdfPageSizeEnum pdfPageSize)
        {
            Create(pdfPageSize, PdfPageOrientationEnum.PORTRAIT, PdfUnitMeasureEnum.MM, 0, 0, 0, 0);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation)
        {
            Create(pdfPageSize, pdfPageOrientation, PdfUnitMeasureEnum.MM, 0, 0, 0, 0);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfUnitMeasureEnum pdfUnitMeasure)
        {
            Create(pdfPageSize, pdfUnitMeasure, 0, 0, 0, 0);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation, PdfUnitMeasureEnum pdfUnitMeasure)
        {
            Create(pdfPageSize, pdfPageOrientation, pdfUnitMeasure, 0, 0, 0, 0);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            Create(pdfPageSize, PdfUnitMeasureEnum.MM, marginLeft, marginRight, marginTop, marginBotton);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            Create(pdfPageSize, pdfPageOrientation, PdfUnitMeasureEnum.MM, marginLeft, marginRight, marginTop, marginBotton);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            Create(pdfPageSize, PdfPageOrientationEnum.PORTRAIT, pdfUnitMeasure, marginLeft, marginRight, marginTop, marginBotton);
        }

        public void Create(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            if (_isDocCreate) throw new DocumentException("Document already create.");

            _isDocCreate = true;

            Page = new PdfPage(pdfPageSize, pdfPageOrientation, pdfUnitMeasure, marginLeft, marginRight, marginTop, marginBotton);

            _document = new Document();
            _document.SetPageSize(Page.Rec);
            _document.SetMargins(Page.MarginLeft, Page.MarginRight, Page.MarginTop, Page.MarginBotton);

            Writer = PdfWriter.GetInstance(_document, _ms);

            _pageCount = 0;
        }

        public void Create(float height, float width)
        {
            Create(height, width, PdfUnitMeasureEnum.MM, 0, 0, 0, 0);
        }

        public void Create(float height, float width, PdfUnitMeasureEnum pdfUnitMeasure)
        {
            Create(height, width, pdfUnitMeasure, 0, 0, 0, 0);
        }

        public void Create(float height, float width, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            Create(height, width, PdfUnitMeasureEnum.MM, marginLeft, marginRight, marginTop, marginBotton);
        }

        public void Create(float height, float width, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            if (_isDocCreate) throw new DocumentException("Document already create.");

            _isDocCreate = true;

            Page = new PdfPage(height, width, pdfUnitMeasure, marginLeft, marginRight, marginTop, marginBotton);

            _document = new Document();
            _document.SetPageSize(Page.Rec);
            _document.SetMargins(Page.MarginLeft, Page.MarginRight, Page.MarginTop, Page.MarginBotton);

            Writer = PdfWriter.GetInstance(_document, _ms);
                
            _pageCount = 0;
        }
        
        public void Unify(List<byte[]> bufferList)
        {
            if (bufferList != null && bufferList.Any())
            {
                var pdfFile = new PdfReader(bufferList[0]);

                _document = new Document();

                PdfWriter pCopy = new PdfSmartCopy(_document, _ms);

                _document.Open();

                foreach (var buff in bufferList)
                {
                    pdfFile = new PdfReader(buff);

                    int numberOfPages = pdfFile.NumberOfPages;

                    for (int i = 1; i < numberOfPages + 1; i++)
                    {
                        var pageDict = pdfFile.GetPageN(i);
                        float width = pdfFile.GetPageSize(i).Width;
                        float height = pdfFile.GetPageSize(i).Height;
                        if (width > height)
                            pageDict.Put(PdfName.Rotate, new PdfNumber(-90));

                        ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
                    }

                    pCopy.FreeReader(pdfFile);
                }

                pdfFile.Close();
                pCopy.Close();
                _document.Close();
            }

            // Salva o buffer na variável interna
            var buffer = GetBytesMemoryStream();

            // Grava o Arquivo quando for devido
            if (_ehFileStream)
            {
                SaveFileStream(buffer);
            }
        }

        public void Open()
        {
            if (_isDocOpen) throw new DocumentException("Exists a open Document.");

            _isDocOpen = true;

            _document.Open();
            _document.NewPage();

            _pageCount++;

            InitializedDocVariables();
        }

        public void Close()
        {
            if (!_isDocOpen) throw new DocumentException("Exists a open Document.");

            AddDocInformation();

            _document.Close();

            _isDocOpen = false;

            // Salva o buffer na variável interna
            var buffer = GetBytesMemoryStream();

            // Grava o Arquivo quando for devido
            if (_ehFileStream)
            {
                SaveFileStream(buffer);
            }
        }

        public void NewPage()
        {
            if (!_isDocOpen) throw new DocumentException("Can't find a open Document.");

            _document.SetMargins(Page.MarginLeft, Page.MarginRight, Page.MarginTop, Page.MarginBotton);
            _document.NewPage();

            _pageCount++;
        }

        public byte[] GetBytes()
        {
            return GetBytesMemoryStream();
        }

        public void Encrypt(string password)
        {
            DoEncrypt = true;
            _password = password;
        }

        public bool RemoveEncrypt()
        {
            _password = string.Empty;
            DoEncrypt = false;
            return true;
        }


        #endregion

        #region [Private methods]

        private void InitializedVariables()
        {
            _isDocOpen = false;

            _ms = new MemoryStream();
        }

        private void InitializedVariables(string directory, string fileName)
        {
            InitializedVariables();

            _filePath = $"{directory}\\{fileName}.pdf";
            _ehFileStream = true;
        }

        private void InitializedDocVariables()
        {
            Font = new PdfFont();
            TextBox = new PdfTextBox(this);
            Draw = new PdfDraw(this);
            Table = new PdfLibraryTable.PdfTable(this);
        }

        private void AddDocObject(IElement Element)
        {
            _document.Add(Element);
        }

        private void AddDocInformation()
        {
            // add Title
            if (!string.IsNullOrEmpty(Title)) _document.AddTitle(Title);
            // add Author
            if (!string.IsNullOrEmpty(Author)) _document.AddAuthor(Author);
            // add Subject
            if (!string.IsNullOrEmpty(Subject)) _document.AddSubject(Subject);
            // add Creation Date
            _document.AddCreationDate();
        }

        private void SaveFileStream(byte[] Buffer)
        {
            FileStream fs = new FileStream(_filePath, FileMode.Create, FileAccess.Write, FileShare.None);
            fs.Write(Buffer, 0, Buffer.Length);
            fs.Close();
        }

        private byte[] GetBytesMemoryStream()
        {
            var bytes = _ms.ToArray();

            _ms.Close();

            if (DoEncrypt && !string.IsNullOrEmpty(_password))
            {
                bytes = EncryptBuffer(bytes);
            }
            return bytes;
        }

        private byte[] EncryptBuffer(byte[] BufferMemoryStream)
        {
            byte[] buffer = null;
            using (var input = new MemoryStream(BufferMemoryStream))
            {
                using (var output = new MemoryStream())
                {
                    PdfReader reader = new PdfReader(input);
                    PdfEncryptor.Encrypt(reader, output, true, _password, _password, PdfWriter.ALLOW_SCREENREADERS);
                    buffer = output.ToArray();
                }
            }
            return buffer;
        }

        #endregion

    }
}
