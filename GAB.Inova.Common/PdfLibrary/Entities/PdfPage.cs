﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Enums;
using textSharpLGPL::iTextSharp.text;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfPage
    {

        #region [Attributes]

        float _pageWidth;
        float _pageHeight;
        float _pageUnitConvWidth;
        float _pageUnitConvHeight;

        #endregion
        
        #region [Properties]

        public PdfUnitMeasureEnum UnitMeasure { get; set; }
        public PdfPageSizeEnum Size { get; private set; }
        public PdfPageOrientationEnum Orientation { get; private set; }
        public Rectangle Rec { get; private set; }

        public float MarginLeft { get; private set; }
        public float MarginTop { get; private set; }
        public float MarginRight { get; private set; }
        public float MarginBotton { get; private set; }

        #endregion
        
        #region [Construtor]

        public PdfPage(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            InitializedPage(pdfPageSize, pdfPageOrientation, pdfUnitMeasure, marginLeft, marginRight, marginTop, marginBotton);
        }

        public PdfPage(float height, float width, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            InitializedPage(height, width, pdfUnitMeasure, marginLeft, marginRight, marginTop, marginBotton);
        }

        #endregion

        #region [Public methods]

        public float GetPageUnitConvWidth()
        {
            return _pageUnitConvWidth;
        }

        public float GetPageUnitConvHeight()
        {
            return _pageUnitConvHeight;
        }

        public float GetWidthUnitMeasure(float width)
        {
            return (width * _pageUnitConvWidth);
        }

        public float GetHeightUnitMeasure(float height)
        {
            return _pageHeight - (height * _pageUnitConvHeight);
        }

        public float GetOnlyHeightUnitMeasure(float height)
        {
            return (height * _pageUnitConvHeight);
        }

        #endregion

        #region [Private methods]

        private void InitializedPage(float height, float width, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            Size = PdfPageSizeEnum.CUSTOM;
            Orientation = PdfPageOrientationEnum.NONE;
            UnitMeasure = pdfUnitMeasure;

            CalculateUnitMeasureConversion();

            Rec = new RectangleReadOnly((width * _pageUnitConvWidth), (height * _pageUnitConvHeight));

            _pageHeight = Rec.Height;
            _pageWidth = Rec.Width;

            MarginLeft = GetWidthUnitMeasure(MarginLeft);
            MarginTop = GetHeightUnitMeasure(MarginTop);
            MarginRight = GetWidthUnitMeasure(MarginRight);
            MarginBotton = GetHeightUnitMeasure(MarginBotton);
        }

        private void InitializedPage(PdfPageSizeEnum pdfPageSize, PdfPageOrientationEnum pdfPageOrientation, PdfUnitMeasureEnum pdfUnitMeasure, float marginLeft, float marginRight, float marginTop, float marginBotton)
        {
            switch (pdfPageSize)
            {
                case PdfPageSizeEnum.A4:
                    if (pdfPageOrientation == PdfPageOrientationEnum.PORTRAIT)
                        Rec = new RectangleReadOnly(PageSize.A4);
                    else
                        Rec = new RectangleReadOnly(PageSize.A4.Rotate());
                    break;
                case PdfPageSizeEnum.LETTER:
                    if (pdfPageOrientation == PdfPageOrientationEnum.PORTRAIT)
                        Rec = new RectangleReadOnly(PageSize.Letter);
                    else
                        Rec = new RectangleReadOnly(PageSize.Letter.Rotate());
                    break;
            }

            Size = pdfPageSize;
            Orientation = pdfPageOrientation;
            UnitMeasure = pdfUnitMeasure;

            _pageHeight = Rec.Height;
            _pageWidth = Rec.Width;
            
            CalculateUnitMeasureConversion();

            MarginLeft = GetWidthUnitMeasure(MarginLeft);
            MarginTop = GetHeightUnitMeasure(MarginTop);
            MarginRight = GetWidthUnitMeasure(MarginRight);
            MarginBotton = GetHeightUnitMeasure(MarginBotton);
        }

        private void CalculateUnitMeasureConversion()
        {
            switch (UnitMeasure)
            {
                case PdfUnitMeasureEnum.MM:
                    _pageUnitConvWidth = 2.833333333f;
                    _pageUnitConvHeight = 2.801346801f;
                    break;
                case PdfUnitMeasureEnum.CM:
                    _pageUnitConvWidth = 28.33333333f;
                    _pageUnitConvHeight = 28.33333333f;
                    break;
                default:
                    _pageUnitConvWidth = 1f;
                    _pageUnitConvHeight = 1f;
                    break;
            }
        }

        #endregion

    }
}
