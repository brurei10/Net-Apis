﻿using GAB.Inova.Common.PdfLibrary.Enums;
using GAB.Inova.Common.PdfLibrary.Structures;
using System;
using System.Collections.Generic;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfItemLayout
    {

        #region {Properties}

        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Int64 IdDocType { get; set; }
        public PdfLayoutItemStyle Style { get; set; }
        public List<PdfItemLayout> Children { get; private set; }
        public PdfLayoutItemStyle UserCustomStyle { get; private set; }
        public PdfLayoutItemTypeEnum PdfLayoutItemType { get; set; }
        public PdfItemLayout Parent { get; set; }

        public float ComputedLeft => GetComputedLeft();
        public float ComputedTop => GetComputedTop();
        public float TotalHeight => GetTotalHeight();
        public float TotalWidth => GetTotalWidth();
        
        #endregion

        #region [Constructors]

        public PdfItemLayout()
        {
            Children = new List<PdfItemLayout>();

            PdfLayoutItemType = PdfLayoutItemTypeEnum.Text;

            Style = new PdfLayoutItemStyle();
            Style.Position = PdfPositionEnum.Absolute;
            Style.Left = 0;
            Style.Top = 0;
            Style.Width = 20;
            Style.Height = 20;
            Style.MarginLeft = 0;
            Style.MarginTop = 0;
            Style.FontName = PdfFontNameEnum.ARIAL;
            Style.FontSize = 10;
            Style.FontFormat = PdfFontFormatEnum.NONE;
            Style.FontColor = PdfColor.Black;
            Style.RepetitionStyle = PdfRepetitionOrientationEnum.Vertical;
            Style.RepetitionOffset = 0f;
            Style.RepetitionMax = 10;
            Style.VerticalAlignment = PdfVerticalAlignmentEnum.MIDDLE;
            Style.HorizontalAlignment = PdfHorizontalAlignmentEnum.LEFT;
            Style.WordWrapLeading = Style.Height + 1.5f;
            Style.ShadowText = false;
        }

        #endregion

        #region [Private methods]

        /// <summary>
        /// Indica a posição do elemento em relação a borda esquerda da página.
        /// No caso de estilo de posicionamento absoluto, considera o ajuste de mergem customizado para o usuário em cascata...
        /// </summary>
        /// <returns></returns>
        private float GetComputedLeft()
        {
            switch (Style.Position)
            {
                case PdfPositionEnum.Absolute:
                    return Style.Left + Style.MarginLeft + GetCascadeUserCustomMarginLeft();
                case PdfPositionEnum.Relative:
                    return Parent.ComputedLeft + Style.Left + Style.MarginLeft;
                default:
                    return Style.Left + Style.MarginLeft;
            }
        }

        private float GetCascadeUserCustomMarginLeft()
        {
            if (Parent != null)
            {
                return UserCustomStyle != null ? UserCustomStyle.MarginLeft + Parent.GetCascadeUserCustomMarginLeft() : Parent.GetCascadeUserCustomMarginLeft();
            }
            if (UserCustomStyle != null) return UserCustomStyle.MarginLeft;
            return 0;
        }

        /// <summary>
        /// Indica a posição do elemento em relação a borda superior da página
        /// </summary>
        /// <returns></returns>
        private float GetComputedTop()
        {
            switch (Style.Position)
            {
                case PdfPositionEnum.Absolute:
                    return Style.Top + Style.MarginTop + GetCascadeUserCustomMarginTop();
                case PdfPositionEnum.Relative:
                    return Parent.ComputedTop + Style.Top + Style.MarginTop;
                default:
                    return Style.Top + Style.MarginTop;
            }
        }

        private float GetCascadeUserCustomMarginTop()
        {
            if (Parent != null)
            {
                return (UserCustomStyle != null) ? UserCustomStyle.MarginTop + Parent.GetCascadeUserCustomMarginTop() : Parent.GetCascadeUserCustomMarginTop();
            }
            if (UserCustomStyle != null) return UserCustomStyle.MarginTop;
            return 0;
        }

        /// <summary>
        /// Indica a altura total do elemento incluindo a margem superior
        /// </summary>
        /// <returns></returns>
        private float GetTotalHeight()
        {
            return Style.Height + Style.MarginLeft;
        }

        /// <summary>
        /// Indica a largura total do elemento incluindo a margem esquerda
        /// </summary>
        /// <returns></returns>
        private float GetTotalWidth()
        {
            return Style.Width + Style.MarginTop;
        }

        #endregion

        #region {public methods}

        /// <summary>
        /// Adiciona um elemento à lista de subitens de leiaute
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(PdfItemLayout child)
        {
            child.Parent = this;
            Children.Add(child);
        }

        #endregion

    }
}
