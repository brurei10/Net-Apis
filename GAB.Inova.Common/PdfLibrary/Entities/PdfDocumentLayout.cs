﻿using GAB.Inova.Common.PdfLibrary.Enums;
using System;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfDocumentLayout
    {

        #region [Attributes]

        int _itemsPerPage { get; set; }

        #endregion

        #region {Properties}

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long IdDocType { get; set; }
        public float CustomPageHeight { get; set; }
        public float CustomPageWidth { get; set; }
        /// <summary>
        /// Espaço entre um item e outro quando ocorrer mais de um item em cada página.
        /// </summary>
        public float RepetitionOffset { get; set; }
        public long IdPdfItemLayout { get; set; }
        public PdfPageSizeEnum PageSize { get; set; }
        /// <summary>
        /// Quantidade de itens por página;
        /// </summary>
        public int ItemsPerPage
        {
            get => _itemsPerPage;
            set
            {
                _itemsPerPage = SetItemsPerPage(value);
            }
        }
        /// <summary>
        /// indica a orientação, horizontal ou vertical, quando ocorrer mais de um item em cada página.
        /// </summary>
        public PdfRepetitionOrientationEnum RepsOrientation { get; set; }

        #endregion

        #region {Constructors}

        public PdfDocumentLayout()
        {
            ItemsPerPage = 1;
            RepsOrientation = PdfRepetitionOrientationEnum.None;
            PageSize = PdfPageSizeEnum.A4;
        }

        #endregion
        
        #region [Private methods]
        
        private int SetItemsPerPage(int value)
        {
            if (value < 1) throw new ArgumentOutOfRangeException("Minumum ItemsPerPage allowed is 1.");
            return value;
        }

        #endregion

    }
}
