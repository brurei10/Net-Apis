﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Enums;
using GAB.Inova.Common.PdfLibrary.Structures;
using textSharpLGPL::iTextSharp.text;
using System;
using System.Linq;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfFont
        : ICloneable
    {

        #region [Constructors]

        public PdfFont()
        {
            FontColor = PdfColor.Black;
            FontFormat = PdfFontFormatEnum.NONE;
            FontName = PdfFontNameEnum.ARIAL;
            FontSize = 10;
            Name = PdfFontNameEnum.ARIAL.ToString();

            var fontPath = $"{Environment.GetEnvironmentVariable("windir")}\\Fonts";
            if (FontFactory.RegisteredFonts.Count == 14) FontFactory.RegisterDirectory(fontPath);
        }

        public PdfFont(PdfFontSize fontSize)
            : this()
        {
            FontSize = fontSize;
        }

        public PdfFont(PdfFontSize fontSize, PdfColor color)
            : this()
        {
            FontSize = fontSize;
            FontColor = color;
        }

        public PdfFont(PdfFontSize fontSize, PdfFontFormatEnum fontFormat)
            : this()
        {
            FontSize = fontSize;
            FontFormat = fontFormat;
        }

        public PdfFont(PdfFontSize fontSize, PdfColor color, PdfFontFormatEnum fontFormat)
            : this()
        {
            FontSize = fontSize;
            FontFormat = fontFormat;
            FontColor = color;
        }

        public PdfFont(PdfColor color, PdfFontFormatEnum fontFormat)
            : this()
        {
            FontColor = color;
            FontFormat = fontFormat;
        }

        public PdfFont(PdfFontFormatEnum fontFormat)
            : this()
        {
            FontFormat = fontFormat;
        }

        #endregion

        #region [Propriedades]

        public string Name { get; private set; }
        public PdfColor FontColor { get; set; }
        public PdfFont Font { get; set; }
        public PdfFontFormatEnum FontFormat { get; set; }
        public PdfFontNameEnum FontName { get; set; }
        public PdfFontSize FontSize { get; set; }

        #endregion

        #region [Métodos públicos]

        public Font GetFontFactory()
        {
            return FontFactory.GetFont(fontname: Name, size: Convert.ToInt32(FontSize), style: (int)FontFormat, color: FontColor.GetBaseColor());
        }

        public void SetFontName(PdfFontNameEnum fontName)
        {
            Name = GetFontName(fontName);
        }

        public void SetFontName(string fontName)
        {
            if (!FontFactory.RegisteredFamilies.Where(e => e.Contains(fontName)).Any())
            {
                throw new DocumentException("FontName not found.");
            }
            Name = fontName;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion

        #region [Métodos privados]

        private string GetFontName(PdfFontNameEnum fontName)
        {
            switch (fontName)
            {
                case PdfFontNameEnum.CENTURYGOTHIC:
                    return "Century Gothic";
                case PdfFontNameEnum.COURIERNEW:
                    return "Courier New";
                default:
                    return fontName.ToString();
            }
        }

        #endregion

    }
}
