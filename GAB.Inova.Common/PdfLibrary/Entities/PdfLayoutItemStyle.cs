﻿using GAB.Inova.Common.PdfLibrary.Enums;
using GAB.Inova.Common.PdfLibrary.Structures;

namespace GAB.Inova.Common.PdfLibrary.Entities
{
    public class PdfLayoutItemStyle
    {

        public PdfLayoutItemStyle()
        {
            Font = new PdfFont();
        }
        
        #region Properties

        public bool ShadowText { get; set; }

        #region [Box]

        public PdfPositionEnum Position { get; set; }
        public float Left { get; set; }
        public float Top { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float MarginLeft { get; set; }
        public float MarginTop { get; set; }

        #endregion

        #region [Font]

        public PdfFont Font { get; set; }
        public PdfColor FontColor
        {
            get { return Font.FontColor; }
            set { Font.FontColor = value; }
        }
        public PdfFontFormatEnum FontFormat
        {
            get { return Font.FontFormat; }
            set { Font.FontFormat = value; }
        }
        public PdfFontNameEnum FontName
        {
            get { return Font.FontName; }
            set { Font.FontName = value; }
        }
        public PdfFontSize FontSize
        {
            get { return Font.FontSize; }
            set { Font.FontSize = value; }
        }

        #endregion

        #region {Alignment}

        public PdfVerticalAlignmentEnum VerticalAlignment { get; set; }
        public PdfHorizontalAlignmentEnum HorizontalAlignment { get; set; }

        #endregion

        #region [Repetition]

        /// <summary>
        /// Representa o estilo de repetição: Horizontal ou vertical. Somente aplicável para listas.
        /// </summary>
        public PdfRepetitionOrientationEnum RepetitionStyle { get; set; }

        /// <summary>
        /// Representa a quantidade máxima de repetições do PdfLayoutItem, caso esse item seja uma lista.
        /// Caso essa lista possua um número de elementos maior do que o definido em RepetitionMax, os elementos adicionas serão desconsiderados.
        /// </summary> 
        public int RepetitionMax { get; set; }

        /// <summary>
        /// Define a distância entre cada repetição de PdfLayoutItem.
        /// </summary> 
        public float RepetitionOffset { get; set; }

        /// <summary>
        /// Define a distância entre cada linha no caso de quebra de linha.
        /// </summary> 
        public float WordWrapLeading { get; set; }

        #endregion

        #endregion

    }
}
