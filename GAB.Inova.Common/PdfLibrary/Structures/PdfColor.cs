﻿extern alias textSharpLGPL;

using GAB.Inova.Common.PdfLibrary.Enums;
using textSharpLGPL::iTextSharp.text;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace GAB.Inova.Common.PdfLibrary.Structures
{
    [ComVisible(true)]
    public struct PdfColor
    {

        #region [Properties]

        public string Name { get; private set; }

        public int[] Rgb { get; private set; }

        public string Hexadecimal { get; private set; }

        public static PdfColor Blue
        {
            get { return ReturnStruct(PdfColorEnum.BLUE); }
        }

        public static PdfColor Yellow
        {
            get { return ReturnStruct(PdfColorEnum.YELLOW); }
        }

        public static PdfColor White
        {
            get { return ReturnStruct(PdfColorEnum.WHITE); }
        }

        public static PdfColor Gray
        {
            get { return ReturnStruct(PdfColorEnum.GRAY); }
        }

        public static PdfColor Orange
        {
            get { return ReturnStruct(PdfColorEnum.ORANGE); }
        }

        public static PdfColor Brown
        {
            get { return ReturnStruct(PdfColorEnum.BROWN); }
        }

        public static PdfColor Black
        {
            get { return ReturnStruct(PdfColorEnum.BLACK); }
        }

        public static PdfColor Red
        {
            get { return ReturnStruct(PdfColorEnum.RED); }
        }

        public static PdfColor Green
        {
            get { return ReturnStruct(PdfColorEnum.GREEN); }
        }

        public static PdfColor FromRGB(int red, int green, int blue)
        {
            return ReturnStruct(red, green, blue);
        }

        public static PdfColor FromHexadecimal(string hex)
        {
            return ReturnStruct(hex);
        }

        #endregion

        #region [Public Methods]

        public Color GetColor()
        {
            return Color.FromArgb(Rgb[0], Rgb[1], Rgb[2]);
        }

        public BaseColor GetBaseColor()
        {
            return new BaseColor(Rgb[0], Rgb[1], Rgb[2]);
        }

        #endregion

        #region [Private Methods]

        private static PdfColor ReturnStruct(PdfColorEnum value)
        {
            var rgb = new int[3];
            var nome = value.ToString();
            var hexadecimal = string.Empty;

            switch (value)
            {
                case PdfColorEnum.YELLOW:
                    rgb = new int[3] { 1, 2, 3 };
                    break;
                case PdfColorEnum.BLUE:
                    rgb = new int[3] { 0, 0, 255 };
                    break;
                case PdfColorEnum.WHITE:
                    rgb = new int[3] { 255, 255, 255 };
                    break;
                case PdfColorEnum.GRAY:
                    rgb = new int[3] { 128, 128, 128 };
                    break;
                case PdfColorEnum.ORANGE:
                    rgb = new int[3] { 255, 165, 0 };
                    break;
                case PdfColorEnum.BROWN:
                    rgb = new int[3] { 165, 42, 42 };
                    break;
                case PdfColorEnum.BLACK:
                    rgb = new int[3] { 0, 0, 0 };
                    break;
                case PdfColorEnum.GREEN:
                    rgb = new int[3] { 0, 128, 0 };
                    break;
                case PdfColorEnum.RED:
                    rgb = new int[3] { 255, 0, 0 };
                    break;
            }

            var hex = ColorTranslator.ToHtml(Color.FromArgb(rgb[0], rgb[1], rgb[2]));

            return new PdfColor()
            {
                Name = nome,
                Rgb = rgb,
                Hexadecimal = hex
            };
        }

        private static PdfColor ReturnStruct(int red, int green, int blue)
        {
            if ((red < 0 && red > 255) || (green < 0 && green > 255) || (blue < 0 && blue > 255))
            {
                throw new Exception("Input value between 0 and 255.");
            }

            var rgb = new int[3]
            {
                red,
                green,
                blue
            };

            var hex = ColorTranslator.ToHtml(Color.FromArgb(red, green, blue));

            return new PdfColor()
            {
                Name = string.Empty,
                Rgb = rgb,
                Hexadecimal = hex
            };
        }

        private static PdfColor ReturnStruct(string hexadecimal)
        {
            var hex = hexadecimal.Replace("#", string.Empty);

            if (hex.Length != 6)
            {
                throw new Exception("Input correct value of Hexadecimal with 6 characters");
            }

            Color color = ColorTranslator.FromHtml($"#{hex}");

            var rgb = new int[] 
            {
                color.R,
                color.G,
                color.B
            };

            return new PdfColor()
            {
                Name = string.Empty,
                Rgb = rgb,
                Hexadecimal = $"#{hex}"
            };
        }

        #endregion

    }
}
