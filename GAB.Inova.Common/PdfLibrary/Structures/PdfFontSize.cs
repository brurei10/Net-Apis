﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace GAB.Inova.Common.PdfLibrary.Structures
{
    [ComVisible(true)]
    public struct PdfFontSize
        : IFormattable, IConvertible, IComparable, IComparable<PdfFontSize>, IEquatable<PdfFontSize>
    {

        #region [Constantes]

        public const sbyte MIN_VALUE = 1;
        public const sbyte MAX_VALUE = 60;

        #endregion
        
        #region [Properties]

        sbyte Size { get; set; }

        #endregion
        
        #region [Interfaces implements]

        #region [IConvertible]

        public TypeCode GetTypeCode()
        {
            return TypeCode.SByte;
        }

        bool IConvertible.ToBoolean(IFormatProvider formatProvider)
        {
            return Convert.ToBoolean(Size);
        }

        byte IConvertible.ToByte(IFormatProvider formatProvider)
        {
            return Convert.ToByte(Size);
        }

        char IConvertible.ToChar(IFormatProvider formatProvider)
        {
            return Convert.ToChar(Size);
        }

        DateTime IConvertible.ToDateTime(IFormatProvider AProvider)
        {
            return Convert.ToDateTime(Size);
        }

        decimal IConvertible.ToDecimal(IFormatProvider formatProvider)
        {
            return Convert.ToDecimal(Size);
        }

        double IConvertible.ToDouble(IFormatProvider formatProvider)
        {
            return Convert.ToDouble(Size);
        }

        short IConvertible.ToInt16(IFormatProvider formatProvider)
        {
            return Convert.ToInt16(Size);
        }

        int IConvertible.ToInt32(IFormatProvider formatProvider)
        {
            return Convert.ToInt32(Size);
        }

        long IConvertible.ToInt64(IFormatProvider formatProvider)
        {
            return Convert.ToInt64(Size);
        }

        sbyte IConvertible.ToSByte(IFormatProvider formatProvider)
        {
            return Convert.ToSByte(Size);
        }

        float IConvertible.ToSingle(IFormatProvider formatProvider)
        {
            return Convert.ToSingle(Size);
        }

        object IConvertible.ToType(Type type, IFormatProvider formatProvider)
        {
            if (type == null) throw new ArgumentNullException("type");
            return Convert.ChangeType(Size, type, formatProvider);
        }

        ushort IConvertible.ToUInt16(IFormatProvider formatProvider)
        {
            return Convert.ToUInt16(Size);
        }

        uint IConvertible.ToUInt32(IFormatProvider formatProvider)
        {
            return Convert.ToUInt32(Size);
        }

        ulong IConvertible.ToUInt64(IFormatProvider formatProvider)
        {
            return Convert.ToUInt64(Size);
        }

        #endregion
        
        #region [IComparable]

        public int CompareTo(object value)
        {
            if (value == null) return 1;

            var entity = (PdfFontSize)value;
            
            if (!(value is SByte)) throw new ArgumentException("Value is not a System.Int32");
            if (Size == entity.Size) return 0;
            if (Size > entity.Size) return 1;
            return -1;
        }

        public int CompareTo(PdfFontSize value)
        {
            if (Size == value.Size) return 0;
            if (Size > value.Size) return 1;
            return -1;
        }

        #endregion
        
        #region [IEquatable]

        public bool Equals(PdfFontSize value)
        {
            return value.Size == Size;
        }

        public override bool Equals(object value)
        {
            if (!(value is PdfFontSize)) return false;
            return ((PdfFontSize)value).Size == Size;
        }

        public override int GetHashCode()
        {
            int hash = 31;
            PropertyInfo[] theProperties = GetType().GetProperties();
            foreach (var info in theProperties)
            {
                if (info != null)
                {
                    var value = info.GetValue(this, null);
                    if (value != null)
                    {
                        unchecked
                        {
                            hash = 29 * hash ^ value.GetHashCode();
                        }
                    }
                }
            }
            return hash;
        }

        #endregion
        
        #region [IFormattable]

        public override string ToString()
        {
            return Size.ToString();
        }

        public string ToString(IFormatProvider formatProvider)
        {
            return Size.ToString(formatProvider);
        }

        public string ToString(string format)
        {
            return Size.ToString(format, null);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return Size.ToString(format, formatProvider);
        }

        #endregion

        #endregion

        #region [Public Methods]

        public static implicit operator PdfFontSize(Int16 value)
        {
            return ReturnStruct(value);
        }

        public static implicit operator PdfFontSize(sbyte value)
        {
            return ReturnStruct(value);
        }

        public static implicit operator PdfFontSize(Int32 value)
        {
            return ReturnStruct(value);
        }

        #endregion

        #region [Private Methods]

        private static PdfFontSize ReturnStruct(object value)
        {
            var convertable = Convert.ToSByte(value);

            // Valida o registro
            if (convertable < MIN_VALUE || convertable > MAX_VALUE)
            {
                throw new Exception(string.Format("Input values between {0} and {1}", MIN_VALUE, MAX_VALUE));
            }

            return new PdfFontSize()
            {
                Size = convertable
            };
        }

        #endregion

    }
}
