﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace GAB.Inova.Common.PdfLibrary.Structures
{
    [ComVisible(true)]
    public struct PdfSmallInt
        : IFormattable, IConvertible, IComparable, IComparable<PdfSmallInt>, IComparable<int>, IEquatable<PdfSmallInt>, IEquatable<int>
    {

        #region [Constantes]

        public const sbyte MIN_VALUE = 0;
        public const sbyte MAX_VALUE = 60;

        #endregion

        #region [Attributes]

        sbyte Size { get; set; }

        #endregion
        
        #region [Interface Methods]

        #region [IConvertible]

        public TypeCode GetTypeCode()
        {
            return TypeCode.SByte;
        }

        bool IConvertible.ToBoolean(IFormatProvider formatProvider)
        {
            return Convert.ToBoolean(Size);
        }

        byte IConvertible.ToByte(IFormatProvider formatProvider)
        {
            return Convert.ToByte(Size);
        }

        char IConvertible.ToChar(IFormatProvider formatProvider)
        {
            return Convert.ToChar(Size);
        }

        DateTime IConvertible.ToDateTime(IFormatProvider formatProvider)
        {
            return Convert.ToDateTime(Size);
        }

        decimal IConvertible.ToDecimal(IFormatProvider formatProvider)
        {
            return Convert.ToDecimal(Size);
        }

        double IConvertible.ToDouble(IFormatProvider formatProvider)
        {
            return Convert.ToDouble(Size);
        }

        short IConvertible.ToInt16(IFormatProvider formatProvider)
        {
            return Convert.ToInt16(Size);
        }

        int IConvertible.ToInt32(IFormatProvider formatProvider)
        {
            return Convert.ToInt32(Size);
        }

        long IConvertible.ToInt64(IFormatProvider formatProvider)
        {
            return Convert.ToInt64(Size);
        }

        sbyte IConvertible.ToSByte(IFormatProvider formatProvider)
        {
            return Convert.ToSByte(Size);
        }

        float IConvertible.ToSingle(IFormatProvider formatProvider)
        {
            return Convert.ToSingle(Size);
        }

        object IConvertible.ToType(Type type, IFormatProvider formatProvider)
        {
            if (type == null) throw new ArgumentNullException("ATargetType");
            return Convert.ChangeType(Size, type, formatProvider);
        }

        ushort IConvertible.ToUInt16(IFormatProvider formatProvider)
        {
            return Convert.ToUInt16(Size);
        }

        uint IConvertible.ToUInt32(IFormatProvider formatProvider)
        {
            return Convert.ToUInt32(Size);
        }

        ulong IConvertible.ToUInt64(IFormatProvider formatProvider)
        {
            return Convert.ToUInt64(Size);
        }

        #endregion
        
        #region [IComparable]

        public int CompareTo(int value)
        {
            var pdfSmallInt = (PdfSmallInt)value;
            if (Size == pdfSmallInt.Size) return 0;
            if (Size > pdfSmallInt.Size) return 1;
            return -1;
        }

        public int CompareTo(object value)
        {
            if (value == null) return 1;

            var pdfSmallInt = (PdfSmallInt)value;
            
            if (!(value is SByte)) throw new ArgumentException("Value is not a System.Int32");
            if (Size == pdfSmallInt.Size) return 0;
            if (Size > pdfSmallInt.Size) return 1;
            return -1;
        }

        public int CompareTo(PdfSmallInt value)
        {
            if (Size == value.Size) return 0;
            if (Size > value.Size) return 1;
            return -1;
        }

        #endregion
        
        #region [IEquatable]

        public bool Equals(PdfSmallInt value)
        {
            return value.Size == Size;
        }

        public bool Equals(int value)
        {
            try
            {
                return value == Convert.ToInt32(Size);
            }
            catch
            {
                return false;
            }
        }

        public override bool Equals(object value)
        {
            if (!(value is PdfSmallInt)) return false;
            return ((PdfSmallInt)value).Size == Size;
        }

        public override int GetHashCode()
        {
            PropertyInfo[] theProperties = GetType().GetProperties();

            int hash = 31;

            foreach (PropertyInfo info in theProperties)
            {
                if (info != null)
                {
                    var value = info.GetValue(this, null);
                    if (value != null)
                    {
                        unchecked
                        {
                            hash = 29 * hash ^ value.GetHashCode();
                        }
                    }
                }
            }
            return hash;
        }

        #endregion
        
        #region [IFormattable]

        public override string ToString()
        {
            return Size.ToString();
        }

        public string ToString(IFormatProvider formatProvider)
        {
            return Size.ToString(formatProvider);
        }

        public string ToString(string format)
        {
            return Size.ToString(format, null);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return Size.ToString(format, formatProvider);
        }
        #endregion

        #endregion

        #region [Public operators Methods]

        public static implicit operator int(PdfSmallInt value)
        {
            return value.Size;
        }

        public static implicit operator float(PdfSmallInt value)
        {
            return value.Size;
        }

        public static implicit operator decimal(PdfSmallInt value)
        {
            return value.Size;
        }

        public static implicit operator double(PdfSmallInt value)
        {
            return value.Size;
        }

        public static implicit operator Int64(PdfSmallInt value)
        {
            return value.Size;
        }

        public static implicit operator PdfSmallInt(Int16 value)
        {
            return ReturnStruct(value);
        }

        public static implicit operator PdfSmallInt(sbyte value)
        {
            return ReturnStruct(value);
        }

        public static implicit operator PdfSmallInt(Int32 value)
        {
            return ReturnStruct(value);
        }

        #endregion

        #region [Private methods]

        private static PdfSmallInt ReturnStruct(object value)
        {
            var convertable = Convert.ToSByte(value);

            // Valida o registro
            if (convertable < MIN_VALUE || convertable > MAX_VALUE)
            {
                throw new Exception(string.Format("Input values between {0} and {1}", MIN_VALUE, MAX_VALUE));
            }

            return new PdfSmallInt()
            {
                Size = convertable
            };
        }

        #endregion

    }
}
