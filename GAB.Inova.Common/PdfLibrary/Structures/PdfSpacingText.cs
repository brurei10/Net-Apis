﻿using System;
using System.Runtime.InteropServices;

namespace GAB.Inova.Common.PdfLibrary.Structures
{
    [ComVisible(true)]
    public struct PdfSpacingText
    {

        #region [Properties]

        public float Value { get; private set; }
        public string Name { get; private set; }

        #endregion

        #region [Public methods]

        public static PdfSpacingText SIMPLE
        {
            get { return ReturnStruct(1); }
        }

        public static PdfSpacingText DOUBLE
        {
            get { return ReturnStruct(2); }
        }

        public static PdfSpacingText ONE_HALF
        {
            get { return ReturnStruct(3); }
        }

        public static PdfSpacingText SetValue(float value)
        {
            return ReturnStruct(value);
        }

        #endregion
        
        #region [Private methods]

        private static PdfSpacingText ReturnStruct(int id)
        {
            var name = string.Empty;
            var value = (float)0;
            switch (id)
            {
                case 1:
                    name = "SIMPLE";
                    value = 1;
                    break;
                case 2:
                    name = "DOUBLE";
                    value = 2;
                    break;
                case 3:
                    name = "ONE_HALF";
                    value = 1.5f;
                    break;
            }
            return new PdfSpacingText()
            {
                Name = name,
                Value = value
            };
        }

        private static PdfSpacingText ReturnStruct(float value)
        {
            if (value < 0) throw new Exception("Negative value not accepted");
            if (value > 4) throw new Exception("Greater value than 4 not accepted");
            return new PdfSpacingText()
            {
                Name = string.Empty,
                Value = value
            };
        }

        #endregion

    }
}
