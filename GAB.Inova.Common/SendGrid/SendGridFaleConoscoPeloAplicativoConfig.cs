﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.SendGrid.Base;
using Microsoft.Extensions.Configuration;

namespace GAB.Inova.Common.SendGrid
{
    public class SendGridFaleConoscoPeloAplicativoConfig
        : SendGridConfig, IIntegradorConfig
    {

        const string CONFIG_SEND_GRID_TEMPLATE_ID = "TemplateId-FaleConoscoByApp";

        public SendGridFaleConoscoPeloAplicativoConfig(IConfiguration configuration,
                                                       ITokenProvider tokenProvider,
                                                       IEnvironmentVariables environmentVariables)
            : base(configuration, tokenProvider, environmentVariables)
        {
            TemplateId = _configuration.GetSection(CONFIG_SEND_GRID).GetValue<string>(CONFIG_SEND_GRID_TEMPLATE_ID);
        }

        public MensagemIntegradorTipoEnum Tipo => MensagemIntegradorTipoEnum.FaleConoscoPeloAplicativo;

    }
}
