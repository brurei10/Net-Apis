﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace GAB.Inova.Common.SendGrid.Base
{
    public abstract class SendGridConfig
        : IIntegradorConfig
    {
        protected const string CONFIG_SEND_GRID = "Sendgrid";
        protected const string CONFIG_HMLDEV = "ConfiguracoesHmlDev";
        protected const string CONFIG_SEND_GRID_EMAIL_WHITE_LIST = "ListaEmailsPermitidosEmDevHml";
        protected const string CONFIG_EMPRESA_EMAIL_FROM = "EmpresaCaixaEmail";
        protected const string NOME_AMBIENTE_PRODUCAO = "Prod";
        protected const string NOME_REMETENTE_GAB = "Grupo Águas do Brasil";

        protected readonly ITokenProvider _tokenProvider;
        protected readonly IConfiguration _configuration;
        protected readonly IEnvironmentVariables _environmentVariables;

        protected SendGridConfig(IConfiguration configuration,
                              ITokenProvider tokenProvider,
                              IEnvironmentVariables environmentVariables)
        {
            _configuration = configuration;
            _tokenProvider = tokenProvider;
            _environmentVariables = environmentVariables;
        }
        
        public EmpresaTipoEnum CodigoEmpresaEnum => _tokenProvider.ObterEmpresaContextoAtualAsync().GetAwaiter().GetResult();
        public string TipoAmbiente => _environmentVariables.Environment ?? "Hml";
        public string RemetenteEnderecoEmail => _configuration.GetSection(CONFIG_EMPRESA_EMAIL_FROM)?.GetValue<string>(CodigoEmpresa.ToString());
        public string CodigoEmpresa => CodigoEmpresaEnum.ToString();
        public string RemetenteNome => CodigoEmpresaEnum == EmpresaTipoEnum.NULO ? NOME_REMETENTE_GAB : CodigoEmpresaEnum.GetDescription();
        public string TemplateId { get; protected set; }

        public string TratarERetornarListaDestinatariosConcatenados(string listaEmailsDestinatarios)
        {
            if (TipoAmbiente == NOME_AMBIENTE_PRODUCAO) return listaEmailsDestinatarios;
            return string.Join(';', TratarERetornarListaDestinatarios(listaEmailsDestinatarios));
        }

        public IEnumerable<string> TratarERetornarListaDestinatarios(string listaEmailsDestinatarios)
        {
            return TratarERetornarListaDestinatarios(listaEmailsDestinatarios.Split(';', System.StringSplitOptions.RemoveEmptyEntries));
        }

        public IEnumerable<string> TratarERetornarListaDestinatarios(IEnumerable<string> listaEmailsDestinatarios)
        {
            if (TipoAmbiente == NOME_AMBIENTE_PRODUCAO) return listaEmailsDestinatarios;

            var result = new HashSet<string>();
            
            var mailWhiteList = _configuration.GetSection(CONFIG_HMLDEV)
                .GetValue<string>(CONFIG_SEND_GRID_EMAIL_WHITE_LIST)?
                .ToLower()
                .Split(';', System.StringSplitOptions.RemoveEmptyEntries);
                        
            foreach (var emailDestinatario in listaEmailsDestinatarios)
            {
                var emailDestinatarioLower = emailDestinatario.ToLower();
                if (mailWhiteList.Contains(emailDestinatarioLower))
                {
                    result.Add(emailDestinatarioLower);
                }
            }

            return result;
        }
    }
}
