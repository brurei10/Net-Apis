﻿using System;

namespace GAB.Inova.Common.Extensions
{
    public static class DoubleExtension
    {

        public static double Truncate(this double value, int precision)
        {
            var fator = Math.Pow(10d, precision);
            var truncatedValue = Math.Floor((value * fator));
            return (Math.Floor(Math.Round(truncatedValue, precision)) / fator);
        }

    }
}
