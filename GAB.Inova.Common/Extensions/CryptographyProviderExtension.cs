﻿using GAB.Inova.Common.Interfaces.Providers;
using System;

namespace GAB.Inova.Common.Extensions
{
    public static class CryptographyProviderExtension
    {
        public static string AddCryptographyt(this ICryptographyProvider v, string cipherString)
        {
            switch (new Random().Next(0, 5))
            {
                case 0:
                    return v.Md5Encrypt(cipherString);
                case 1:
                    return v.Hash1Encrypt(cipherString);
                case 2:
                    return v.Hash256Encrypt(cipherString);
                case 3:
                    return v.Hash384Encrypt(cipherString);
                default:
                    return v.Md5Encrypt(cipherString);
            }
        }
    }
}