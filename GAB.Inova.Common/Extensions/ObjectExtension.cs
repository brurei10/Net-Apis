﻿namespace GAB.Inova.Common.Extensions
{
    public static class ObjectExtension
    {

        public static T GetPropertyValue<T>(this object obj, string name)
        {
            try
            {
                var value = GetPropertyValue(obj, name);
                if (value is null) return default(T);
                return (T)value;
            }
            catch
            {
                return default(T);
            }
        }

        public static object GetPropertyValue(this object obj, string name)
        {
            if (obj is null) return null;
            foreach (var part in name.Split('.'))
            {
                var propertyInfo = obj.GetType()?.GetProperty(part);
                if (propertyInfo is null) return null;
                obj = propertyInfo.GetValue(obj, null);
            }
            return obj;
        }

    }
}
