﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GAB.Inova.Common.Extensions
{
    public static class EnumExtension
    {

        public static TEnum ToEnum<TEnum>(this int enumValue, TEnum defaultValue)
        {
            if (!Enum.IsDefined(typeof(TEnum), enumValue)) return defaultValue;

            return (TEnum)Enum.Parse(typeof(TEnum), enumValue.ToString());
        }

        public static TEnum ToEnum<TEnum>(this string enumValue, TEnum defaultValue)
        {
            if (!Enum.IsDefined(typeof(TEnum), enumValue)) return defaultValue;

            return (TEnum)Enum.Parse(typeof(TEnum), enumValue.ToString());
        }

        public static string GetDescription(this Enum source)
        {
            var description = string.Empty;
            var attributes = default(Attribute);
            var mi = source.GetType().GetMember(source.ToString()).FirstOrDefault();

            attributes = (DescriptionAttribute)mi.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault();

            if (attributes != null)
            {
                description = (attributes as DescriptionAttribute).Description;
            }
            else
            {
                attributes = (DisplayAttribute)mi.GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault();

                if (attributes != null)
                {
                    description = (attributes as DisplayAttribute).Description;
                }
            }

            return string.IsNullOrEmpty(description) ? source.ToString() : description;
        }
        
    }
}
