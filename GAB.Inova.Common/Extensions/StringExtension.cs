﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace GAB.Inova.Common.Extensions
{
    public static class StringExtension
    {

        #region [Métodos públicos]

        public static int ToInt(this string value)
        {
            return int.TryParse(value, out int result) ? result : default(int);
        }

        public static T ToInt<T>(this string value)
        {
            object retorno = default(T);

            if (typeof(T) == typeof(int))
            {
                retorno = int.TryParse(value, out int result) ? result : default(int);
            }
            else if (typeof(T) == typeof(Int16))
            {
                retorno = Int16.TryParse(value, out Int16 result) ? result : default(Int16);
            }
            else if (typeof(T) == typeof(Int32))
            {
                retorno = Int32.TryParse(value, out Int32 result) ? result : default(Int32);
            }
            else if (typeof(T) == typeof(Int64))
            {
                retorno = Int64.TryParse(value, out Int64 result) ? result : default(Int64);
            }

            return (T)Convert.ChangeType(retorno, typeof(T));
        }

        public static long ToLong(this string value)
        {
            return long.TryParse(value, out long result) ? result : default(long);
        }

        public static bool IsValidCpfOrCnpj(this string value)
        {
            value = Regex.Replace(value, "[^0-9]", string.Empty);
            if (value.Length == 11) return value.IsValidCpf();
            if (value.Length == 14) return value.IsValidCnpj();
            return false;
        }

        public static bool IsValidCpf(this string value)
        {
            value = Regex.Replace(value, "[^0-9]", string.Empty);
            if (value.Length != 11) return false;

            var multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            var multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            for (int j = 0; j < 10; j++)
            {
                if (j.ToString().PadLeft(11, char.Parse(j.ToString())) == value) return false;
            }

            var tempCpf = value.Substring(0, 9);
            int soma = 0;

            for (int i = 0; i < 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }

            int resto = soma % 11;
            if (resto < 2) resto = 0; else resto = 11 - resto;

            string digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }

            resto = soma % 11;
            if (resto < 2) resto = 0; else resto = 11 - resto;

            digito = digito + resto.ToString();

            return value.EndsWith(digito);
        }

        public static bool IsValidCnpj(this string value)
        {
            value = Regex.Replace(value, "[^0-9]", string.Empty);
            if (value.Length != 14) return false;

            var multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            var multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            
            var tempCnpj = value.Substring(0, 12);
            int soma = 0;

            for (int i = 0; i < 12; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            }

            int resto = (soma % 11);
            if (resto < 2) resto = 0; else resto = 11 - resto;

            var digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            }

            resto = (soma % 11);
            if (resto < 2) resto = 0; else resto = 11 - resto;

            digito = digito + resto.ToString();

            return value.EndsWith(digito);
        }

        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrWhiteSpace(email)) return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", RegexOptions.IgnoreCase);
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static string ToAnoMes(this string value)
        {
            if (string.IsNullOrEmpty(value)) return value;
            if (value.Length == 6) return value.Substring(2, 4) + value.Substring(0, 2);
            if (value.Length == 7) return value.Substring(3, 4) + value.Substring(0, 2);
            return value;
        }

        public static string ToReferencia(this string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length != 6) return value;
            return $"{value.Substring(4, 2)}/{value.Substring(0, 4)}";
        }

        /// <summary>
        /// Formatar em código de barras [xxxxxxxxxxx-x xxxxxxxxxxx-x xxxxxxxxxxx-x xxxxxxxxxxx-x]
        /// </summary>
        /// <param name="value">Indica um codigo de barras</param>
        /// <returns>o código de barras formatado</returns>
        public static string ToBarCode(this string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length != 48) return value;

            return Regex.Replace(value, @"(\d{11})(\d{1})(\d{11})(\d{1})(\d{11})(\d{1})(\d{11})(\d{1})", @"$1-$2 $3-$4 $5-$6 $7-$8");
        }        
        
        /// <summary>
        /// Obter substring de texto à partir de texto inicial e final
        /// </summary>
        /// <param name="value">Texto completo</param>
        /// <param name="textoInicial">Texto inicial. Será obtido à partir do primeiro caracter dele.</param>
        /// <param name="textoFinal">Texto final. Será obtido à partir do último caracter dele.</param>
        /// <returns>Retorna a substring obtida </returns>
        public static string Slice(this string value, string textoInicial, string textoFinal)
        {
            var startInt = value.IndexOf(textoInicial);
            var startEnd = value.IndexOf(textoFinal) + textoFinal.Length;

            if (startEnd < 0)
            {
                startEnd = value.Length + startEnd;
            }

            if (startInt < 0 || startEnd < 0)
            {
                return string.Empty;
            }

            int len = startEnd - startInt;

            return value.Substring(startInt, len);
        }

        #endregion

    }
}
