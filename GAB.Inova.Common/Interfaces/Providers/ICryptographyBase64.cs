﻿using System;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface ICryptographyBase64 : IDisposable
    {
        /// <summary>
        /// Converts an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
        /// </summary>
        /// <param name="str">input</param>
        /// <returns>string encode base64</returns>
        string Base64Encoded(string str);


        /// <summary>
        /// onverts the specified string, which encodes binary data as base-64 digits, to an equivalent 8-bit unsigned integer array.
        /// </summary>
        /// <param name="base64Encoded">input</param>
        /// <returns>string decode</returns>
        string Base64Decoded(string base64Encoded);
    }
}