﻿namespace GAB.Inova.Common.Interfaces.Providers
{
    /// <inheritdoc cref="ICryptographyMd5" />
    /// <summary>
    /// Interfaces:
    /// ICryptographyProvider: implements ICryptographyMd5, ICryptographyHash, ICryptographyBase64
    ///
    /// Representa a classe base com implementações de alguns algoritmos de System.Security.Cryptography.
    /// ICryptographyMd5: mplements the basic conversion of Hash MD5
    /// ICryptographyHash: implements the basic conversion of Hash 1, Hash256, Hash 384, Hash 512
    /// ICryptProviderBase64:  implements the basic conversion of Base64.
    /// 
    /// Paulo Cesar P Luna. 27/10/2017
    /// </summary>
    public interface ICryptographyProvider : ICryptographyMd5, ICryptographyHash, ICryptographyBase64
    {
    }
}