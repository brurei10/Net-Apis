﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.ViewModels.Token;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface ITokenProvider
    {

        Task<TokenResponseViewModel> GerarTokenModelAsync(IEnumerable<KeyValuePair<string, string>> claims, int? segundosParaExpirar = null);
        Task<string> ObterClaimAsync(string token, string claimName);
        Task<ClaimsPrincipal> ObterTokenAsync(string token);
        Task<string> ObterTokenContextoAtualAsync();
        Task<EmpresaTipoEnum> ObterEmpresaContextoAtualAsync();
        Task<int> ObterTempoExpiracaoAsync();
    }
}
