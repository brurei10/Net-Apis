﻿using System;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface ICryptographyMd5 : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherString">text secret</param>
        /// <param name="securityKey">Key security</param>
        /// <param name="useHashing">use hash?</param>
        /// <returns></returns>
        string Md5Encrypt(string cipherString, string securityKey, bool useHashing);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherString">text secret</param>
        /// <param name="securityKey">Key security</param>
        /// <param name="useHashing">use hash?</param>
        /// <returns></returns>
        string Md5Dencrypt(string cipherString, string securityKey, bool useHashing);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherString">text secret</param>
        /// <returns></returns>
        string Md5Encrypt(string cipherString);
    }
}