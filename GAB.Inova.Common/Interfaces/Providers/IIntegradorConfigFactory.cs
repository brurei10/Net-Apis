﻿using GAB.Inova.Common.Enums;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface IIntegradorConfigFactory
    {
        IIntegradorConfig Construir(MensagemIntegradorTipoEnum tipo);
    }
}
