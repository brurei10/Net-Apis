﻿using iTextSharp.text;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface IHtmlToPdfProvider
    {
        Task<byte[]> GerarPdfAsync(string fileName, string filePath, string bodyHtml, string cssHtml);
    }
}
