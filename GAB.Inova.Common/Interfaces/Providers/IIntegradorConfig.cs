﻿using GAB.Inova.Common.Enums;
using System.Collections.Generic;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface IIntegradorConfig
    {
        string TemplateId { get; }
        string RemetenteNome { get; }
        string RemetenteEnderecoEmail { get; }        
        string CodigoEmpresa { get; }

        EmpresaTipoEnum CodigoEmpresaEnum { get; }
        string TipoAmbiente { get; }

        string TratarERetornarListaDestinatariosConcatenados(string listaEmailsDestinatarios);
        IEnumerable<string> TratarERetornarListaDestinatarios(string listaEmailsDestinatarios);
        IEnumerable<string> TratarERetornarListaDestinatarios(IEnumerable<string> listaEmailsDestinatarios);
    }
}
