﻿using System.Threading.Tasks;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface IMultitenantConnectionProvider
    {

        Task<string> ObterConnectionStringAsync();

    }
}
