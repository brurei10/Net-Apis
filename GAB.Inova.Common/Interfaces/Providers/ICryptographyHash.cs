﻿using System;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface ICryptographyHash : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        string Hash1Encrypt(string str);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        string Hash256Encrypt(string str);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        string Hash384Encrypt(string str);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        string Hash512Encrypt(string str);
    }
}