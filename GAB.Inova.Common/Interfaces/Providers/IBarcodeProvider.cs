﻿using GAB.Inova.Common.Enums;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Interfaces.Providers
{
    public interface IBarcodeProvider
    {

        Task<string> GerarCodigoBarrasAsync(TipoContaEnum tipoDocumento, long numeroDocumento, decimal valorDocumento, string matricula, string codigoFebraban);

        Task<string> GerarImagemCodigoBarrasBase64Async(string CodigoBarra);

    }
}
