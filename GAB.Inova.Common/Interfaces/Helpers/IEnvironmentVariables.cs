﻿namespace GAB.Inova.Common.Interfaces.Helpers
{
    public interface IEnvironmentVariables
    {

        string Environment { get; }
        long IdRotina { get; }
        long IdUsuario { get; }

    }
}
