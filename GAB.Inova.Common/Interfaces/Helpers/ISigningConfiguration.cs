﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.Interfaces.Helpers
{
    public interface ISigningConfiguration
    {

        SecurityKey Key { get; }

        SigningCredentials SigningCredentials { get; }

        List<SecurityKey> SecurityKeys { get; }

    }
}
