﻿namespace GAB.Inova.Common.Interfaces.Helpers
{
    public interface IAuditConfiguration
    {

        string ObterEnderecoIP();

        long ObterRotina();

        long ObterUsuarioId();

    }
}
