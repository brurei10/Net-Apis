﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAB.Inova.Common.Interfaces.Helpers
{
    public interface ITokenConfiguration
    {

        string Audience { get; }

        string Issuer { get; }

        int Seconds { get; }

    }
}
