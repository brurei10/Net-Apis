﻿using GAB.Inova.Common.Interfaces.Providers;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Providers
{
    public class MultitenantConnectionProvider
        : IMultitenantConnectionProvider
    {

        readonly IConfiguration _configuration;
        readonly ITokenProvider _tokenProvider;

        public MultitenantConnectionProvider(IConfiguration configuration, ITokenProvider tokenProvider)
        {
            _configuration = configuration;
            _tokenProvider = tokenProvider;
        }

        private string _connectionString;

        public async Task<string> ObterConnectionStringAsync()
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                var empresa = await _tokenProvider.ObterEmpresaContextoAtualAsync();

                _connectionString = _configuration
                    .GetSection("ConnectionStrings")
                    .AsEnumerable()
                    .FirstOrDefault(a => a.Key.ToLower().Equals($"connectionstrings:{empresa.ToString().ToLower()}"))
                    .Value;

                if (string.IsNullOrEmpty(_connectionString)) throw new ArgumentNullException("Conexão com o banco de dados não definado na configuração.");
            }

            return _connectionString;
        }
    }
}
