﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.SendGrid;
using Microsoft.Extensions.Configuration;
using System;

namespace GAB.Inova.Common.Providers
{
    public class IntegradorConfigFactory
        : IIntegradorConfigFactory
    {

       readonly ITokenProvider _tokenProvider;
       readonly IConfiguration _configuration;
       readonly IEnvironmentVariables _environmentVariables;

        public IntegradorConfigFactory(ITokenProvider tokenProvider, IConfiguration configuration, IEnvironmentVariables environmentVariables)
        {
            _tokenProvider = tokenProvider;
            _configuration = configuration;
            _environmentVariables = environmentVariables;
        }

        public IIntegradorConfig Construir(MensagemIntegradorTipoEnum tipo)
        {
            switch (tipo)
            {
                case MensagemIntegradorTipoEnum.ContaDigitalAdesao:
                    return new SendGridEmailAdesaoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.ContaDigitalExclusao:
                    return new SendGridEmailCancelamentoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.ContaDigitalAlteracao:
                    return new SendGridEmailAlteracaoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.ContaDigitalBemVindo:
                    return new SendGridEmailBoasVindasConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.Informativo:
                    break;
                case MensagemIntegradorTipoEnum.ContaDigitalConta:
                    return new SendGridContaDigitalConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.ContaDigitalContaClienteSemAdesao:
                    return new SendGridContaDigitalClienteSemAdesaoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.ContaDigitalErroEnvioConta:
                    break;
                case MensagemIntegradorTipoEnum.ContaDigitalErroGeracaoArquivo:
                    break;
                case MensagemIntegradorTipoEnum.ContaDigitalErroRecebimentoEmail:
                    break;
                case MensagemIntegradorTipoEnum.SegundaViaDeContaPeloAplicativo:
                    return new SendGridEmailSegundaViaContaAplicativoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.FaleConoscoPeloAplicativo:
                    return new SendGridFaleConoscoPeloAplicativoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.OuvidoriaPeloAplicativo:
                    return new SendGridOuvidoriaPeloAplicativoConfig(_configuration, _tokenProvider, _environmentVariables);
                case MensagemIntegradorTipoEnum.QuitacaoDebito:
                    return new SendGridQuitacaoDebitoConfig(_configuration, _tokenProvider, _environmentVariables);
            }
            throw new InvalidOperationException("Não foi possível criar o objeto de configuração do integrador externo");
        }
    }
}
