﻿using GAB.Inova.Common.Interfaces.Providers;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace GAB.Inova.Common.Providers
{
    /// <inheritdoc />
    public class CryptographyProvider : ICryptographyProvider
    {
        /// <inheritdoc />
        public string Md5Encrypt(string cipherString, string securityKey, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string
            var toEncryptArray = Encoding.UTF8.GetBytes(cipherString);

            //AppSettingsReader settingsReader = new AppSettingsReader();
            //// Get the key from config file
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(securityKey));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice
                hashmd5.Clear();
            }
            else
            {
                keyArray = Encoding.UTF8.GetBytes(securityKey);
            }

            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            //set the secret key for the tripleDES algorithm
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            //padding mode(if any extra byte added)


            var cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <inheritdoc />
        public string Md5Dencrypt(string cipherString, string securityKey, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string
            var toEncryptArray = Convert.FromBase64String(cipherString);

            //System.Configuration.AppSettingsReader settingsReader =
            //    new AppSettingsReader();
            ////Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(securityKey));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = Encoding.UTF8.GetBytes(securityKey);
            }

            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            //set the secret key for the tripleDES algorithm
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            //padding mode(if any extra byte added)

            var cTransform = tdes.CreateDecryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return Encoding.UTF8.GetString(resultArray);
        }

        public string Md5Encrypt(string cipherString)
        {
            using (var md5Hash = MD5.Create())
            {
                var data = md5Hash.ComputeHash(Encoding.Default.GetBytes(cipherString));
                return ByteArrayToString(data);
            }
        }

        /// <inheritdoc />
        public string Hash1Encrypt(string str)
        {
            var sha1Hasher = new SHA1CryptoServiceProvider();
            var hashedDataBytes = sha1Hasher.ComputeHash(Encoding.UTF8.GetBytes(str));
            return ByteArrayToString(hashedDataBytes);
        }

        /// <inheritdoc />
        public string Hash256Encrypt(string str)
        {
            var sha256Hasher = new SHA256Managed();
            var hashedDataBytes = sha256Hasher.ComputeHash(Encoding.UTF8.GetBytes(str));
            return ByteArrayToString(hashedDataBytes);
        }

        /// <inheritdoc />
        public string Hash384Encrypt(string str)
        {
            var sha384Hasher = new SHA384Managed();
            var hashedDataBytes = sha384Hasher.ComputeHash(Encoding.UTF8.GetBytes(str));
            return ByteArrayToString(hashedDataBytes);
        }

        /// <inheritdoc />
        public string Hash512Encrypt(string str)
        {
            var sha512Hasher = new SHA512Managed();
            var hashedDataBytes = sha512Hasher.ComputeHash(Encoding.UTF8.GetBytes(str));
            return ByteArrayToString(hashedDataBytes);
        }

        public string Base64Encoded(string str)
        {
            var toEncodeAsBytes = Encoding.ASCII.GetBytes(str);
            var returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        /// <inheritdoc />
        public string Base64Decoded(string base64Encoded)
        {
            var data = Convert.FromBase64String(base64Encoded);
            var base64Decoded = Encoding.ASCII.GetString(data);
            return base64Decoded;
        }

        private static string ByteArrayToString(IEnumerable<byte> inputArray)
        {
            var output = new StringBuilder("");
            foreach (var t in inputArray)
            {
                output.Append(t.ToString("X2"));
            }
            return output.ToString();
        }

        public void Dispose()
        {
        }
    }
}