﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Token;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Providers
{
    public class TokenProvider
        : ITokenProvider
    {

        readonly IHttpContextAccessor _httpContextAccessor;
        readonly ISigningConfiguration _signingConfiguration;
        readonly ITokenConfiguration _tokenConfiguration;

        public TokenProvider(IHttpContextAccessor httpContextAccessor, 
                             ISigningConfiguration signingConfiguration, 
                             ITokenConfiguration tokenConfiguration)
        {
            _httpContextAccessor = httpContextAccessor;
            _signingConfiguration = signingConfiguration;
            _tokenConfiguration = tokenConfiguration;
        }

        #region [Métodos públicos]

        public async Task<TokenResponseViewModel> GerarTokenModelAsync(IEnumerable<KeyValuePair<string, string>> claims, int? segundosParaExpirar = null)
        {
            var securityToken = await GerarTokenAsync(claims, segundosParaExpirar);

            var handler = new JwtSecurityTokenHandler();

            var token = handler.WriteToken(securityToken);

            return new TokenResponseViewModel()
            {
                Token = token
            };
        }

        public async Task<string> ObterClaimAsync(string token, string claimName)
        {
            var jwt = await ObterTokenAsync(token);

            return jwt.Claims.FirstOrDefault(a => a.Type.ToLower() == claimName.ToLower())?.Value;
        }

        public async Task<ClaimsPrincipal> ObterTokenAsync(string token)
        {
            try
            {
                TokenValidationParameters validationParameters =
                    new TokenValidationParameters
                    {
                        ValidIssuer = _tokenConfiguration.Issuer,
                        ValidAudiences = new[] { _tokenConfiguration.Audience },
                        IssuerSigningKeys = _signingConfiguration.SecurityKeys,
                        RequireExpirationTime = true,
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidateIssuerSigningKey = true
                    };

                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                return await Task.FromResult(handler.ValidateToken(token, validationParameters, out SecurityToken validatedToken));
            }
            catch
            {
                throw new InvalidOperationException("Token inválido!");
            }
        }

        public async Task<string> ObterTokenContextoAtualAsync()
        {
            var token = string.Empty;
            var context = _httpContextAccessor.HttpContext;

            if (context is null) return await Task.FromResult(token);

            if (context.Request.Headers.ContainsKey("Authorization"))
            {
                return context.Request.Headers["Authorization"].FirstOrDefault().Replace("Bearer ", "");
            }

            if (string.IsNullOrEmpty(token) && context.Request.QueryString.HasValue)
            {
                return context.Request.Query["token"].FirstOrDefault();
            }

            return await Task.FromResult(token);
        }

        public async Task<EmpresaTipoEnum> ObterEmpresaContextoAtualAsync()
        {
            var token = await ObterTokenContextoAtualAsync();
            if (string.IsNullOrEmpty(token)) throw new InvalidOperationException("Token de autenticação não localizado.");

            var claimEmpresa = await ObterClaimAsync(token, "CodigoEmpresa");
            if (string.IsNullOrEmpty(claimEmpresa)) throw new InvalidOperationException("Código da Empresa não localizado no token de autenticação!");

            return (EmpresaTipoEnum)Enum.Parse(typeof(EmpresaTipoEnum), claimEmpresa, true);
        }

        #endregion

        #region [Métodos privados]

        private async Task<SecurityToken> GerarTokenAsync(IEnumerable<KeyValuePair<string, string>> claims, int? segundosParaExpirar = null)
        {
            var claimsParaAdicionar = new List<Claim>();

            if (claims.Any())
            {
                claimsParaAdicionar.AddRange(claims.Select(s => new Claim(s.Key, s.Value)));
            }
            
            var identity = new ClaimsIdentity(new GenericIdentity(Guid.NewGuid().ToString(), "Hash"), claimsParaAdicionar);

            var dataCriacao = DateTime.UtcNow;
            var dataExpiracao = dataCriacao + TimeSpan.FromSeconds(segundosParaExpirar ?? _tokenConfiguration.Seconds);

            var handler = new JwtSecurityTokenHandler();

            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfiguration.Issuer,
                Audience = _tokenConfiguration.Audience,
                SigningCredentials = _signingConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });

            return await Task.FromResult(securityToken);
        }

        public async Task<int> ObterTempoExpiracaoAsync()
        {
            return await Task.FromResult(_tokenConfiguration.Seconds);
        }

        #endregion

    }
}
