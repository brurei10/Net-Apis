﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Interfaces.Providers;
using iTextSharp.text.pdf;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace GAB.Inova.Common.Providers
{
    public class BarcodeProvider
        : IBarcodeProvider
    {

        #region [Constantes]

        /// <summary>
        /// Nota Fiscal
        /// </summary>
        const string NOTA_FISCAL = "01";

        /// <summary>
        /// Faturão
        /// </summary>
        const string FATURAO = "97";

        /// <summary>
        /// Quitação de débito
        /// </summary>
        const string QUITACAO_DEBITO = "98";

        /// <summary>
        /// Aviso de débito
        /// </summary>
        const string AVISO_DEBITO = "99";

        #endregion

        #region [Métodos públicos]

        /// <summary>
        /// Gerar o código de barras de um documento
        /// </summary>
        /// <param name="tipoDocumento">Indica o tipo do documento de geração do código de barras</param>
        /// <param name="numeroDocumento">Indica o número do documento</param>
        /// <param name="valorDocumento">Indica o valor do documento</param>
        /// <param name="matricula">Indica a matricula/ligação do cliente do documento</param>
        /// <param name="codigoFebraban">Indica o código Febraban da empresa do documento</param>
        /// <returns>o código de barras</returns>
        public async Task<string> GerarCodigoBarrasAsync(TipoContaEnum tipoDocumento, long numeroDocumento, decimal valorDocumento, string matricula, string codigoFebraban)
        {
            var documento = string.Empty;
            switch (tipoDocumento)
            {
                case TipoContaEnum.AvisoDeDebito:
                    documento = AVISO_DEBITO;
                    break;
                case TipoContaEnum.ContaNormal:
                case TipoContaEnum.NotaFiscalAvulsa:
                    documento = NOTA_FISCAL;
                    break;
                case TipoContaEnum.DocumentoNaoFiscal:
                    documento = QUITACAO_DEBITO;
                    break;
                default:
                    throw new InvalidOperationException($"Tipo de documento inválido para geração de código de barras [{tipoDocumento}].");
            }

            if (numeroDocumento <= 0) throw new InvalidOperationException($"Número de documento inválido [{numeroDocumento}].");
            if (valorDocumento <= 0 || valorDocumento >= 99999999999) throw new InvalidOperationException($"Valor do documento inválido [{valorDocumento}].");
            if (string.IsNullOrEmpty(matricula)) throw new InvalidOperationException($"Número da matrícula nula ou vazia [{matricula}].");
            if (string.IsNullOrEmpty(codigoFebraban)) throw new InvalidOperationException($"Código Febraban nulo ou vazio [{codigoFebraban}].");

            var valorConta = (valorDocumento * 100).ToString("00000000000");
            var numeroDocumentoFormatado = numeroDocumento.ToString("0000000000");

            var barcode = $"826{valorConta}{codigoFebraban}{numeroDocumentoFormatado}0{matricula}0{documento}2";

            return await Task.FromResult(barcode);
        }

        public async Task<string> GerarImagemCodigoBarrasBase64Async(string CodigoBarra)
        {
            if (string.IsNullOrEmpty(CodigoBarra)) throw new InvalidOperationException($"Código de barras inválido [{CodigoBarra}].");

            var somenteNumeros = CodigoBarra.Replace(" ", "").Replace("-", "");

            var bc = new BarcodeInter25()
            {
                Code = $@"{somenteNumeros.Substring(0, 11)}
                          {somenteNumeros.Substring(12, 11)}
                          {somenteNumeros.Substring(24, 11)}
                          {somenteNumeros.Substring(36, 11)}",
                StartStopText = false,
                CodeType = BarcodeInter25.CODE128,
                AltText = string.Empty
            };

            Image tif = bc.CreateDrawingImage(Color.Black, Color.White);

            MemoryStream ms = new MemoryStream();
            tif.Save(ms, ImageFormat.Png);

            var b64String = Convert.ToBase64String(ms.ToArray());
            var dataUrl = $"data:image/png;base64,{b64String}";

            return await Task.FromResult(dataUrl);
        }

        #endregion

    }
}
