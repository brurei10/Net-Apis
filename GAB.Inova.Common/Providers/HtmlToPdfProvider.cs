﻿extern alias textSharp;

using GAB.Inova.Common.HtmlToPdf.Processors;
using GAB.Inova.Common.Interfaces.Providers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using textSharp::iTextSharp.tool.xml;
using textSharp::iTextSharp.tool.xml.html;
using textSharp::iTextSharp.tool.xml.parser;
using textSharp::iTextSharp.tool.xml.pipeline.css;
using textSharp::iTextSharp.tool.xml.pipeline.end;
using textSharp::iTextSharp.tool.xml.pipeline.html;

namespace GAB.Inova.Common.Providers
{
    public class HtmlToPdfProvider: IHtmlToPdfProvider
    {
        readonly ILogger<HtmlToPdfProvider> _logger;

        public HtmlToPdfProvider(ILogger<HtmlToPdfProvider> logger)
        {
            _logger = logger;
        }

        #region [Métodos públicos]

        /// <summary>
        /// Gerar o PDF de um documento à partir de um HTML
        /// </summary>
        /// <param name="fileName">Nome do arquivo PDF</param>
        /// <param name="path">Path do arquivo PDF</param>
        /// <param name="headerHtml">HTML do header</param>
        /// <param name="bodyHtml">HTML a ser convertido</param>
        /// <param name="cssHtml">css do HTML</param>
        /// <returns>o código de barras</returns>headerHtml
        public async Task<byte[]> GerarPdfAsync(string fileName, string filePath, string bodyHtml, string cssHtml)
        {
            if (string.IsNullOrEmpty(fileName)) throw new InvalidOperationException($"Nome do arquivo inválido [{fileName}].");
            if (string.IsNullOrEmpty(filePath)) throw new InvalidOperationException($"Path do arquivo inválido [{filePath}].");
            if (string.IsNullOrEmpty(bodyHtml)) throw new InvalidOperationException($"HTML do body inválido [{bodyHtml}].");
            if (string.IsNullOrEmpty(cssHtml)) throw new InvalidOperationException($"CSS do HTML inválido [{cssHtml}].");

            _logger.LogInformation($"Deletando arquivo caso exista. Arquivo [{Path.Combine(filePath, fileName)}.pdf].");

            if (File.Exists($"{Path.Combine(filePath, fileName)}.pdf"))
            {
                File.Delete($"{Path.Combine(filePath, fileName)}.pdf");
            }

            _logger.LogInformation($"Criando diretório caso não exista. Diretório [{filePath}.pdf].");

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            using (var stream = new FileStream($"{Path.Combine(filePath, fileName)}.pdf", FileMode.OpenOrCreate))
            {
                _logger.LogInformation($"Criou arquivo PDF para preenchimento.");

                using (var document = new Document(PageSize.A4))
                {
                    var writer = PdfWriter.GetInstance(document, stream);
                    document.Open();

                    var tagProcessorFactory = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();

                    tagProcessorFactory.RemoveProcessor(HTML.Tag.IMG);
                    tagProcessorFactory.AddProcessor(new TableDataProcessor(), new string[] { HTML.Tag.TD });
                    tagProcessorFactory.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor());

                    var htmlPipelineContext = new HtmlPipelineContext(null);
                    htmlPipelineContext.SetTagFactory(tagProcessorFactory);

                    var pdfWriterPipeline = new PdfWriterPipeline(document, writer);
                    var htmlPipeline = new HtmlPipeline(htmlPipelineContext, pdfWriterPipeline);

                    var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
                    cssResolver.AddCss(cssHtml, "utf-8", true);
                    var cssResolverPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

                    var worker = new XMLWorker(cssResolverPipeline, true);
                    var parser = new XMLParser(worker);

                    using (var stringReader = new StringReader(GeraArquivoXHtml(bodyHtml)))
                    {
                        parser.Parse(stringReader);
                    }
                }
            }

            _logger.LogInformation($"Arquivo gerado. Gerando bytes do arquivo. Arquivo [{Path.Combine(filePath, fileName)}.pdf]");

            if (File.Exists($"{Path.Combine(filePath, fileName)}.pdf"))
            {
                byte[] espelhoPdf = null;

                using (var input = File.OpenRead($"{Path.Combine(filePath, fileName)}.pdf"))
                {
                    espelhoPdf = new byte[input.Length];

                    input.Read(espelhoPdf, 0, espelhoPdf.Length);
                }

                return await Task.FromResult(espelhoPdf);
            }
            else
            {
                _logger.LogCritical($"Arquivo gerado mas não encontrado no diretório. Arquivo [{Path.Combine(filePath, fileName)}.pdf]");

                return null;
            }
        }

        #endregion

        #region [Métodos privados]

        private string GeraArquivoXHtml(string bodyHtml)
        {
            var xHtml = new StringBuilder();

            xHtml.Append($@"<?xml version=""1.0"" encoding=""iso-8859-1""?>
                         <!DOCTYPE html
                             PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                         <html lang=""en"" xml:lang=""en"" xmlns=""http://www.w3.org/1999/xhtml""> 
                             <head>
                             </head>
                             <body class=""body"">
                                {bodyHtml}
                             </body>
                         </html>");

            return xHtml.ToString();
        }

        #endregion

    }
}
