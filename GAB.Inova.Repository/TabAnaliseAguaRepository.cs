﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class TabAnaliseAguaRepository
        : DapperRepository, ITabAnaliseAguaRepository
    {

        readonly ILogger _logger;

        public TabAnaliseAguaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TabAnaliseAguaRepository>();
        }

        public async Task<IEnumerable<TabAnaliseAgua>> ObterAnaliseAguaParaImpressaoContaAsync(long seqOriginal, long idUsuario)
        {
            try
            {
                var sql = @"SELECT TAA.ID,
                                   TAA.ANOMES,
                                   TAA.TIPO,
                                   TAA.DESCRICAO,
                                   TAA.AMOSTRAEXIGIDA,
                                   TAA.AMOSTRAREALIZ,
                                   TAA.VALMEDIODETEC,
                                   TAA.REFERENCIAS,
                                   TAA.SEQMOSTRA,
                                   TAA.DESCR_COLUNA
                            FROM NETUNO.VW_GERA_ARQUIVO_IMPRESSAO T
                            LEFT JOIN NETUNO.TAB_ANALISE_AGUA TAA
                                ON TAA.ANOMES =
                                   SUBSTR(T.ANO_MES_CT, 4, 4) || SUBSTR(T.ANO_MES_CT, 1, 2) -- &PANOMES
                               AND TAA.TIPO = T.ID_SISTEMA_ABASTECIMENTO
                             WHERE T.ID_USUARIO = :P_ID_USUARIO
                               AND T.SEQ_ORIGINAL = :P_SEQ_ORIGINAL";
                
                OracleParameter.Add("P_SEQ_ORIGINAL", seqOriginal, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryAsync<TabAnaliseAgua>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(TabAnaliseAgua)} - para impressão de conta [seqOriginal:{seqOriginal}, idUsuario:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
