﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ConsumoRepository
        : DapperRepository, IConsumoRepository
    {

        readonly ILogger _logger;

        public ConsumoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContaRepository>();
        }

        public async Task<IEnumerable<Consumo>> ObterUltimosDozeConsumosPorMatriculaAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT MATRICULA, ANO_MES_LEITURA, CONFAT, QTD_DIAS_LEITURA
                              FROM (SELECT C.MATRICULA,
                                           C.ANO_MES_LEITURA,
                                           CASE
                                             WHEN (C.CONFAT > 0 AND C.ID_CONF > 0) THEN
                                              C.CONFAT
                                             ELSE
                                              0
                                           END CONFAT,
                                           CASE
                                             WHEN (C.CONFAT > 0 AND C.ID_CONF > 0) THEN
                                              C.QTD_DIAS_LEITURA
                                             ELSE
                                              0
                                           END QTD_DIAS_LEITURA
                                      FROM NETUNO.CONSUMO C
                                     WHERE C.MATRICULA = :P_MATRICULA
                                     ORDER BY C.ANO_MES_LEITURA DESC)
                             WHERE ROWNUM <= 12";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<Consumo>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter os últimos doze - {typeof(Consumo)} [matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
