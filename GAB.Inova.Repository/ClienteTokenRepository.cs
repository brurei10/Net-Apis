﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.ClienteTokenSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ClienteTokenRepository
        : DapperRepository, IClienteTokenRepository
    {

        readonly ILogger _logger;

        public ClienteTokenRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ClienteTokenRepository>();
        }

        public async Task<ClienteToken> ObterPorTokenAsync(string token)
        {
            try
            {
                var sql = GetSelectSQL(" WHERE TOKEN = :P_TOKEN ");

                OracleParameter.Add("P_TOKEN", token, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ClienteToken>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ClienteToken)} [token:{token}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<GrupoEntregaEnum> ObterGrupoEntregaClientePorTokenAsync(string token)
        {
            try
            {
                var sql = @"SELECT CLI.GRUPO_ENTREGA
                              FROM NETUNO.CLIENTE CLI
                              JOIN NETUNO.CLIENTE_TOKEN CTK
                                ON CTK.MATRICULA = CLI.MATRICULA
                             WHERE CTK.TOKEN = :P_TOKEN";

                OracleParameter.Add("P_TOKEN", token, OracleDbType.Varchar2, ParameterDirection.Input);

                var grupoEntrega = await DbConn.QueryFirstOrDefaultAsync<int>(sql, OracleParameter);

                return (GrupoEntregaEnum)grupoEntrega;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter grupo entrega do cliente - {typeof(ClienteToken)} [token:{token}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> ConfirmarAsync(long id)
        {
            try
            {
                var sql = @"UPDATE NETUNO.CLIENTE_TOKEN CT
                               SET CT.DT_CONFIRMACAO = SYSDATE
                             WHERE CT.ID_CLIENTE_TOKEN = :P_ID_CLIENTE_TOKEN";

                OracleParameter.Add("P_ID_CLIENTE_TOKEN", id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao confirmar - {typeof(ClienteToken)} [id:{id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> InserirAsync(ClienteToken entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.CLIENTE_TOKEN
                              (MATRICULA,
                               TOKEN,
                               TIPO,
                               SISTEMA,
                               DT_INCLUSAO,
                               DT_EXPIRACAO,
                               DT_CONFIRMACAO,
                               ID_PROTOCOLO_ATENDIMENTO)
                            VALUES
                              (:P_MATRICULA,
                               :P_TOKEN,
                               :P_TIPO,
                               :P_SISTEMA,
                               SYSDATE,
                               :P_DT_EXPIRACAO,
                               :P_DT_CONFIRMACAO,
                               :P_ID_PROTOCOLO_ATENDIMENTO)
                            RETURNING ID_CLIENTE_TOKEN INTO :V_ID_CLIENTE_TOKEN";

                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TOKEN", entity.Token, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO", (int)entity.Tipo, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_SISTEMA", (int)entity.Sistema, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_DT_EXPIRACAO", entity.DataExpiracao.HasValue ? (object)entity.DataExpiracao.Value : DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DT_CONFIRMACAO", entity.DataConfirmacao.HasValue ? (object)entity.DataConfirmacao.Value : DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", entity.IdProtocoloAtendimento.HasValue ? (object)entity.IdProtocoloAtendimento.Value : DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);

                OracleParameter.Add("V_ID_CLIENTE_TOKEN", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql: sql, param: OracleParameter, commandType: CommandType.Text);
                
                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_CLIENTE_TOKEN").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ClienteToken)} [matricula:{entity?.Matricula}, token:{entity?.Token}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #region [Métodos públicos para validação]

        public ValidationResult PodeConfirmar(ClienteToken entity)
        {
            try
            {
                return new ClienteTokenPodeConfirmarValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar confirmação - {typeof(ClienteToken)}.");

                throw;
            }
        }

        #endregion

        #region [Métodos privados]

        private string GetSelectSQL(string where)
        {
            var builder = new StringBuilder(@"SELECT ID_CLIENTE_TOKEN,
                                                     MATRICULA,
                                                     TOKEN,
                                                     TIPO,
                                                     SISTEMA,
                                                     DT_INCLUSAO,
                                                     DT_EXPIRACAO,
                                                     DT_CONFIRMACAO,
                                                     ID_PROTOCOLO_ATENDIMENTO
                                                FROM NETUNO.CLIENTE_TOKEN ");
            if (!string.IsNullOrEmpty(where))
            {
                builder.AppendLine(where);
            }
            return builder.ToString();
        }

        #endregion

    }
}
