﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Enums;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class PerdasRepository : DapperRepository, IPerdasRepository
    {
        readonly ILogger _logger;

        public PerdasRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<PerdasRepository>();
        }

        public async Task<IEnumerable<TabMeterwiseHD>> ConsultarHidrometros(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                //DateTime data1 = DateTime.Parse(dataInicio.ToString(), new CultureInfo("pt-BR"));
                //DateTime data2 = DateTime.Parse(dataFim.ToString(), new CultureInfo("pt-BR"));

                var sql = @"SELECT TRIM(ID_HD) as ID_HD,
                           MATRICULA as MATRICULA,
                           SISTEMA_ABASTECIMENTO as SISTEMA_ABASTECIMENTO,
                           CAPACIDADE_HD as CAPACIDADE_HD,
                           ANO_FABRICACAO as ANO_FABRICACAO,
                           DATA_INSTALACAO,
                           CATEGORIA as CATEGORIA,
                           FUNCIONAMENTO_HD as FUNCIONAMENTO_HD,
                           MARCA_HD as MARCA_HD,
                           MODELO_HD as MODELO_HD,
                           CLASSE_HD as CLASSE_HD,
                           VAZAO_HD as VAZAO_HD, 
                           CONCAT(TO_CHAR(replace(vazao_hd,'m³/h','') * 2),'m³/h') AS VAZAO_HD_DOBRO,
                           CONDICAO_HD as CONDICAO_HD, 
                           SISTEMA_ABASTECIMENTO as AREA
                      FROM TAB_METERWISE_HD     
                      WHERE TRUNC(DATA_REGISTRO) BETWEEN :P_DATA_INICIO AND :P_DATA_FIM AND DATA_EXPORTADO IS NULL AND FLAG_ULTIMO = 1";


                OracleParameter.Add("P_DATA_INICIO", dataInicio, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_FIM", dataFim, OracleDbType.Date, ParameterDirection.Input);

                var d1 = await DbConn.QueryAsync<TabMeterwiseHD>(sql, OracleParameter);

                fazerUpdateMeterwiseHD(dataInicio, dataFim);


                return d1;

            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(TabMeterwiseHD)} [P_DATA_INICIO:{dataInicio}, P_DATA_FIM{dataFim}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<Leitura>> ConsultarLeituras(DateTime dataInicio, DateTime dataFim)
        {

            try
            {
                DateTime data1 = DateTime.Parse(dataInicio.ToString(), new CultureInfo("pt-BR"));
                DateTime data2 = DateTime.Parse(dataFim.ToString(), new CultureInfo("pt-BR"));

                var sql = @"SELECT TRIM(ID_HD) as ID_HD,
                           DATA_LEITURA as DATA_LEITURA,
                           LEITURA as LEITURA
                      FROM TAB_METERWISE_LEITURA   
                      WHERE TRUNC(DATA_REGISTRO) BETWEEN :P_DATA_INICIO AND :P_DATA_FIM AND DATA_EXPORTADO IS NULL AND FLAG_ULTIMO = 1";


                OracleParameter.Add("P_DATA_INICIO", data1, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_FIM", data2, OracleDbType.Date, ParameterDirection.Input);


                var d1 = await DbConn.QueryAsync<Leitura>(sql, OracleParameter);

                fazerUpdateMeterwiseLeitura(dataInicio, dataFim);

                return d1;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Leitura)} [P_DATA_INICIO:{dataInicio}, P_DATA_FIM{dataFim}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<LeituraMacro>> ConsultarLeiturasMacro()
        {
            try
            {
                var sql = @"SELECT TAG_MACRO AS ID_HD,
                        DATA_REGISTRO AS DATA_LEITURA,
                        VALOR_VARIAVEL AS LEITURA 
                        FROM integra_elipse.VW_TELEMETRIA_CAV
                        WHERE DATA_REGISTRO > SYSDATE - INTERVAL '15' MINUTE AND 
                        TAG_MACRO IS NOT NULL ";

                var lLeituraMacro = await DbConn.QueryAsync<LeituraMacro>(sql, OracleParameter);

                ///* Inseri itens da leitura macro */
                //foreach (var item in lLeituraMacro)
                //{
                //    fazerInsertFlowiseLeitura(item);
                //}

                return lLeituraMacro;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(LeituraMacro)} [{DateTime.Now}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public void fazerUpdateMeterwiseHD(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                DateTime data1 = DateTime.Parse(dataInicio.ToString(), new CultureInfo("pt-BR"));
                DateTime data2 = DateTime.Parse(dataFim.ToString(), new CultureInfo("pt-BR"));

                var sql = @"UPDATE TAB_METERWISE_HD
                            SET DATA_EXPORTADO = SYSDATE
                      WHERE TRUNC(DATA_REGISTRO) BETWEEN :P_DATA_INICIO AND :P_DATA_FIM AND DATA_EXPORTADO IS NULL AND FLAG_ULTIMO = 1";


                OracleParameter.Add("P_DATA_INICIO", data1, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_FIM", data2, OracleDbType.Date, ParameterDirection.Input);

                DbConn.Execute(sql, OracleParameter);

            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar - TAB_METERWISE_HD [P_DATA_INICIO:{dataInicio}, P_DATA_FIM{dataFim}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }

        }

        public void fazerUpdateMeterwiseLeitura(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                DateTime data1 = DateTime.Parse(dataInicio.ToString(), new CultureInfo("pt-BR"));
                DateTime data2 = DateTime.Parse(dataFim.ToString(), new CultureInfo("pt-BR"));

                var sql = @"UPDATE TAB_METERWISE_LEITURA
                            SET DATA_EXPORTADO = SYSDATE
                      WHERE TRUNC(DATA_REGISTRO) BETWEEN :P_DATA_INICIO AND :P_DATA_FIM AND DATA_EXPORTADO IS NULL AND FLAG_ULTIMO = 1";


                OracleParameter.Add("P_DATA_INICIO", data1, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_FIM", data2, OracleDbType.Date, ParameterDirection.Input);

                DbConn.Execute(sql, OracleParameter);

            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar - TAB_METERWISE_LEITURA [P_DATA_INICIO:{dataInicio}, P_DATA_FIM{dataFim}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }

        }

        public void fazerInsertFlowiseLeitura(LeituraMacro leituraMacro)
        {
            try
            {
                var sql = @"INSERT INTO TAB_FLOWISE_LEITURA (ID_HD, DATA_REGISTRO, LEITURA, DATA_EXPORTADO) VALUE (:P_ID_HD, :P_DATA_REGISTRO, :P_LEITURA, SYSDATE)";

                OracleParameter.Add("P_ID_HD", leituraMacro.Code, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_REGISTRO", leituraMacro.Date, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_LEITURA", leituraMacro.Volume, OracleDbType.Int32, ParameterDirection.Input);

                DbConn.Execute(sql, OracleParameter);

            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - TAB_FLOWISE_LEITURA [].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }

        }

        public void InserirLog(StatusCodeResult resultado)
        {

            try
            {
                var sql = @"INSERT INTO TAB_FLOWISE_LEITURA (DATA_REGISTRO, RESULT) VALUES (SYSDATE, :P_RESULTADO)";

                OracleParameter.Add("P_RESULTADO", resultado.StatusCode.ToString(), OracleDbType.Varchar2, ParameterDirection.Input);


                DbConn.Execute(sql, OracleParameter);

            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - TAB_FLOWISE_LEITURA [].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }

        }
    }

}
