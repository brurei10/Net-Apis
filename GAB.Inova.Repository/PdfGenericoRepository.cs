﻿using Dapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class PdfGenericoRepository
        : DapperRepository, IPdfGenericoRepository
    {

        readonly ILogger _logger;

        public PdfGenericoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<PdfGenericoRepository>();
        }

        public async Task<PdfGenModeloDoc> ObterDocumentoLayoutAsync(PdfModeloEnum pdfModelo)
        {
            try
            {
                var sql = @"SELECT MD.ID_MODELO,
                                   MD.NOME_MODELO,
                                   MD.DESCRICAO_MODELO,
                                   MD.ID_TIPO_DOCUMENTO,
                                   MD.TAMANHO_PAGINA,
                                   MD.ORIENTACAO_PAGINA,
                                   MD.ALTURA_PAGINA,
                                   MD.LARGURA_PAGINA,
                                   MD.ITENS_POR_PAGINA,
                                   MD.ORIENTACAO_REPETICOES,
                                   MD.OFFSET_REPETICAO,
                                   MD.ID_ITEM_DOCUMENTO
                              FROM NETUNO.PDFGEN_MODELO_DOC MD
                              JOIN NETUNO.PDFGEN_MODELO_DOC_UTILIZACAO M
                                ON M.ID_MODELO = MD.ID_MODELO
                             WHERE M.SITUACAO = 1
                               AND M.UTILIZACAO = :P_UTILIZACAO";

                OracleParameter.Add("P_UTILIZACAO", pdfModelo.ToString(), OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<PdfGenModeloDoc>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(PdfGenModeloDoc)} [id:{(int)pdfModelo}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<PdfGenItemDoc>> ObterItensDocumentoLayoutAsync(long idItemDocumento)
        {
            try
            {
                var sql = @"SELECT TLID.ID_ITEM_DOCUMENTO,
                                   TLID.NOME_ITEM_DOCUMENTO,
                                   LPAD(NOME_ITEM_DOCUMENTO,
                                        LENGTH(NOME_ITEM_DOCUMENTO) + (LEVEL - 1) * 2,
                                        ' ') AS IDENTED_NOME_ITEM_DOCUMENTO,
                                   LEVEL,
                                   TLID.DESCRICAO_ITEM_DOCUMENTO,
                                   TLID.ID_TIPO_DOCUMENTO,
                                   TLID.TIPO_ITEM_DOCUMENTO,
                                   TLID.ID_ITEM_DOCUMENTO_PARENT,
                                   TLID.ORDEM,
                                   TLID.SOMENTE_PREVIEW,
                                   TLID.PREVIEW_VALUE,
                                   TLID.ESTILO_POSICAO,
                                   TLID.ESTILO_POSICAO_ESQUERDA,
                                   TLID.ESTILO_POSICAO_TOPO,
                                   TLID.ESTILO_ALTURA,
                                   TLID.ESTILO_LARGURA,
                                   TLID.ESTILO_MARGEM_ESQUERDA,
                                   TLID.ESTILO_MARGEM_TOPO,
                                   TLID.ESTILO_FONTE_NOME,
                                   TLID.ESTILO_FONTE_TAMANHO,
                                   TLID.ESTILO_FONTE_FORMAT,
                                   TLID.ESTILO_FONTE_COLOR,
                                   TLID.ESTILO_SOMBRA,
                                   TLID.ESTILO_ALINHAMENTO_VERTICAL,
                                   TLID.ESTILO_ALINHAMENTO_HORIZONTAL,
                                   TLID.ESTILO_REPETICAO_ORIENTACAO,
                                   TLID.ESTILO_REPETICAO_MAXIMO,
                                   TLID.ESTILO_REPETICAO_ESPACAMENTO,
                                   TLID.ESTILO_ESPACO_QUEBRAS_LINHA
                              FROM NETUNO.PDFGEN_ITEM_DOC TLID
                             START WITH TLID.ID_ITEM_DOCUMENTO = :P_ID_ITEM_DOCUMENTO
                            CONNECT BY PRIOR TLID.ID_ITEM_DOCUMENTO = TLID.ID_ITEM_DOCUMENTO_PARENT
                                   AND TLID.SOMENTE_PREVIEW = 0
                                   AND LEVEL <= 2
                             ORDER SIBLINGS BY TLID.ID_ITEM_DOCUMENTO";

                OracleParameter.Add("P_ID_ITEM_DOCUMENTO", idItemDocumento, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryAsync<PdfGenItemDoc>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(PdfGenItemDoc)} [id:{idItemDocumento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<PdfGenItemDocAjustado> ObterItemDocumentoLayoutAjustadoAsync(long idItemDocumento, long idModelo, long idUsuario)
        {
            try
            {
                var sql = @"SELECT PIDA.ID_IMPRESSORA,
                                   PIDA.ID_MODELO,
                                   PIDA.ID_ITEM_DOCUMENTO,
                                   PIDA.ESTILO_MARGEM_ESQUERDA,
                                   PIDA.ESTILO_MARGEM_TOPO
                              FROM NETUNO.PDFGEN_ITEM_DOC_AJUSTADO PIDA
                              JOIN NETUNO.USUARIO_IMPRESSORA UI
                                ON UI.ID = PIDA.ID_IMPRESSORA
                              JOIN NETUNO.USUARIO U
                                ON U.ID_USUARIO_IMPRESSORA = UI.ID
                             WHERE PIDA.ID_ITEM_DOCUMENTO = :P_ID_ITEM_DOCUMENTO
                               AND PIDA.ID_MODELO = :P_ID_MODELO
                               AND U.ID_USUARIO = :P_ID_USUARIO";

                OracleParameter.Add("P_ID_ITEM_DOCUMENTO", idItemDocumento, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_MODELO", idModelo, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<PdfGenItemDocAjustado>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(PdfGenItemDocAjustado)} [idItensDoc:{idItemDocumento}, idModelo:{idModelo}, idUsuario:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
