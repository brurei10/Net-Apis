﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class TarifaDiariaRepository
        : DapperRepository, ITarifaDiariaRepository
    {

        readonly ILogger _logger;

        public TarifaDiariaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TarifaDiariaRepository>();
        }

        public async Task<IEnumerable<TarifaDiaria>> ObterTarifaParaImpressaoContaAsync(TarifaDiaria tarifaDiaria)
        {
            try
            {
                var sql = @"SELECT TD.ID_TARIFA_DIARIA,
                                   TD.TIPO_TARIFA,
                                   TD.DATA_TARIFA,
                                   TD.ID_CATEGORIA,
                                   TD.FAIXA_ANTERIOR,
                                   TD.FAIXA_CONSUMO,
                                   TD.VL_FAIXA,
                                   TD.VL_FAIXA_ESGO,
                                   TD.VL_ACUMULADO,
                                   TD.VL_ACUMULADO_ESGO,
                                   TD.VL_FAIXA_SOCIAL,
                                   TD.VL_ACUMULADO_SOCIAL,
                                   TD.LOCALIDADE,
                                   TD.GRUPO_TARIFA,
                                   TD.ID_TARIFA,
                                   TD.ID_FAIXA_TARIFA
                              FROM NETUNO.TARIFA_DIARIA TD
                             WHERE TD.ID_CATEGORIA = :P_ID_CATEGORIA
                               AND TD.ID_TARIFA = :P_ID_TARIFA
                               AND TD.GRUPO_TARIFA = :P_GRUPO_TARIFA
                               AND TD.FAIXA_ANTERIOR < :P_FAIXA_ANTERIOR
                               AND TD.DATA_TARIFA = :P_DATA_TARIFA
                             ORDER BY TRUNC(TD.FAIXA_ANTERIOR)";

                OracleParameter.Add("P_ID_CATEGORIA", tarifaDiaria.IdCategoria, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_TARIFA", tarifaDiaria.IdTarifa, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_GRUPO_TARIFA", tarifaDiaria.GrupoTarifa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_FAIXA_ANTERIOR", tarifaDiaria.FaixaAnterior, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_TARIFA", tarifaDiaria.DataTarifa, OracleDbType.Date, ParameterDirection.Input);

                return await DbConn.QueryAsync<TarifaDiaria>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(TarifaDiaria)} - para impressão de conta [idCategoria:{tarifaDiaria.IdCategoria}, idTarifa:{tarifaDiaria.IdTarifa}, grupoTarifa:{tarifaDiaria.GrupoTarifa}, faixaAnterior:{tarifaDiaria.FaixaAnterior}, dataTarifa:{tarifaDiaria.DataTarifa.ToString("dd/MM/yyyy")}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
