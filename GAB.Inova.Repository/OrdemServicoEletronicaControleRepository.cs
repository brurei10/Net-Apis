﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.OrdemServicoEletronicaControleSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class OrdemServicoEletronicaControleRepository
        : DapperRepository, IOrdemServicoEletronicaControleRepository
    {

        readonly ILogger _logger;

        public OrdemServicoEletronicaControleRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory) 
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<OrdemServicoEletronicaControleRepository>();
        }

        public OrdemServicoEletronicaControle Obter(long numeroOS, int sequenciaOS)
        {
            try
            {
                var sql = @"SELECT OEC.NRO_OS, OEC.SEQ_OS, OEC.ID_SIT_OS_ELE, OEC.DT_INCL, OEC.DT_ENV, OEC.DT_RET 
                              FROM NETUNO.OS_ELETRONICA_CONTROLE OEC
                             WHERE OEC.NRO_OS = :pNRO_OS 
                               AND OEC.SEQ_OS = :pSEQ_OS ";

                OracleParameter.Add("pNRO_OS", numeroOS, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQ_OS", sequenciaOS, OracleDbType.Int32, ParameterDirection.Input);

                return DbConn.QueryFirstOrDefault<OrdemServicoEletronicaControle>(sql: sql, param: OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter o a OSEletronica Controle NroOS: {numeroOS} SeqOS: {sequenciaOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<OrdemServicoEletronicaControle> ObterAsync(long numeroOS, int sequenciaOS)
        {
            try
            {
                var sql = @"SELECT OEC.NRO_OS, OEC.SEQ_OS, OEC.ID_SIT_OS_ELE, OEC.DT_INCL, OEC.DT_ENV, OEC.DT_RET 
                              FROM NETUNO.OS_ELETRONICA_CONTROLE OEC
                             WHERE OEC.NRO_OS = :pNRO_OS 
                               AND OEC.SEQ_OS = :pSEQ_OS ";

                OracleParameter.Add("pNRO_OS", numeroOS, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQ_OS", sequenciaOS, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<OrdemServicoEletronicaControle>(sql: sql, param: OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter o a OSEletronica Controle NroOS: {numeroOS} SeqOS: {sequenciaOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public void RegistrarRecebimentoOrdemServicoEletronica(OrdemServicoEletronicaControle controle)
        {
            try
            {
                var sql = @"UPDATE NETUNO.OS_ELETRONICA_CONTROLE OEC
                               SET OEC.DT_RET = SYSDATE,
                               OEC.ID_SIT_OS_ELE = 3
                             WHERE OEC.NRO_OS = :PNRO_OS 
                               AND OEC.SEQ_OS = :PSEQ_OS ";

                OracleParameter.Add("pNRO_OS", controle.NumeroOS, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQ_OS", controle.SequenciaOS, OracleDbType.Int32, ParameterDirection.Input);

                DbConn.Execute(sql: sql, param: OracleParameter, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Erro ao registrar recebimento na OSEletronica Controle NroOS: {controle?.NumeroOS} SeqOS: {controle?.SequenciaOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task RegistrarRecebimentoOrdemServicoEletronicaAsync(OrdemServicoEletronicaControle controle)
        {
            try
            {
                var sql = @"UPDATE NETUNO.OS_ELETRONICA_CONTROLE OEC
                               SET OEC.DT_RET = SYSDATE,
                               OEC.ID_SIT_OS_ELE = 3
                             WHERE OEC.NRO_OS = :PNRO_OS 
                               AND OEC.SEQ_OS = :PSEQ_OS ";

                OracleParameter.Add("pNRO_OS", controle.NumeroOS, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQ_OS", controle.SequenciaOS, OracleDbType.Int32, ParameterDirection.Input);

                await DbConn.ExecuteAsync(sql: sql, param: OracleParameter, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Erro ao registrar recebimento na OSEletronica Controle NroOS: {controle?.NumeroOS} SeqOS: {controle?.SequenciaOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public ValidationResult PodeAlterar(OrdemServicoEletronicaControle entity)
        {
            try
            {
                return new OrdemServicoEletronicaControlePodeSerAlteradoValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Erro ao validar se pode ser alterada a OSEletronica Controle NroOS: {entity?.NumeroOS} SeqOS: {entity?.SequenciaOS}.");

                throw;
            }
        }
         
        public ValidationResult PodeExcluir(OrdemServicoEletronicaControle entity) => throw new System.NotImplementedException();

        public ValidationResult PodeIncluir(OrdemServicoEletronicaControle entity) => throw new System.NotImplementedException();

        
    }
}
