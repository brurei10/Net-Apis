﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoDetalheSpec.Validators
{
    public class OrdemServicoDetalheDeveExistirEstarEmSistemaExternoSpec : ISpecification<OrdemServicoDetalhe>
    {
        private readonly IOrdemServicoDetalheRepository _ordemServicoDetalheRepository;

        public OrdemServicoDetalheDeveExistirEstarEmSistemaExternoSpec(IOrdemServicoDetalheRepository ordemServicoDetalheRepository)
        {
            _ordemServicoDetalheRepository = ordemServicoDetalheRepository;
        }

        public bool IsSatisfiedBy(OrdemServicoDetalhe entity)
        {
            return _ordemServicoDetalheRepository.Obter(entity.NumeroOS)?.Status == Common.Enums.StatusDetOrdemServicoEnum.EM_SISTEMA_EXTERNO;
        }
    }
}
