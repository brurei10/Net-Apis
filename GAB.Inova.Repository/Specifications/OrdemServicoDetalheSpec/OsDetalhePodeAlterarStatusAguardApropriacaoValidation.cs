﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.OrdemServicoDetalheSpec.Validators;

namespace GAB.Inova.Repository.Specifications.OrdemServicoDetalheSpec
{
    public class OsDetalhePodeAlterarStatusAguardApropriacaoValidation : Validator<OrdemServicoDetalhe>
    {
        public OsDetalhePodeAlterarStatusAguardApropriacaoValidation(IOrdemServicoDetalheRepository ordemServicoDetalheRepository)
        {
            var validaStatus = new OrdemServicoDetalheDeveExistirEstarEmSistemaExternoSpec(ordemServicoDetalheRepository);

            base.Add("Status", new Rule<OrdemServicoDetalhe>(validaStatus, "Não foi possível localizar a OrdemServicoDetalhe ou a mesma não se encontra no status de 'EM SISTEMA EXTERNO'."));
        }
    }
}
