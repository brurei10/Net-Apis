﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    //TODO: Ajustar no futuro para validar o codigo de serviço da associada na tabela de de para a ser criada
    public class OSEletronicaAssociadaDeveTerCodigoDeServicoSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return entity.EhAssociada ? entity.CodigoServico > 0 : true;
        }
    }
}
