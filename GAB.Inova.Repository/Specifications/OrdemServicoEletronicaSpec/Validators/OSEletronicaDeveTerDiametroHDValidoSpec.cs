﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using System.Linq;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaDeveTerDiametroHDValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            var codigos = new long[3] { 99001, 99002, 99003 };
            var codigosBaixa = new long[10] { 110, 111, 113, 114, 115, 116, 117, 118, 122, 999 };

            if (codigos.Contains(entity.CodigoServico) && 
                (!entity.MotivoBaixaID.HasValue || (entity.MotivoBaixaID.HasValue && !codigosBaixa.Contains(entity.MotivoBaixaID.Value))))
            {
                return entity.DiametroHidrometroID.HasValue && entity.DiametroHidrometroID.Value > 0;
            }
            return true;
            
        }
    }
}
