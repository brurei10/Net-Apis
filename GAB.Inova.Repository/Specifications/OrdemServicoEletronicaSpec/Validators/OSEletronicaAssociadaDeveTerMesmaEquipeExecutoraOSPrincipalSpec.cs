﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaAssociadaDeveTerMesmaEquipeExecutoraOSPrincipalSpec : ISpecification<OrdemServicoEletronica>
    {
        private readonly IOrdemServicoEletronicaRepository _repository;

        public OSEletronicaAssociadaDeveTerMesmaEquipeExecutoraOSPrincipalSpec(IOrdemServicoEletronicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            if (entity.EhAssociada)
            {
                var osEletronicaPai = _repository.ObterPorId(entity.OSEletronicaOrigemID.GetValueOrDefault(0));
                return osEletronicaPai?.EquipeExecutora == entity.EquipeExecutora;
            }
            return true;
        }
    }
}
