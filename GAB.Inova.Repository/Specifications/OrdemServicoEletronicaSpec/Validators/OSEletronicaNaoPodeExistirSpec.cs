﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaNaoPodeExistirSpec
        : ISpecification<OrdemServicoEletronica>
    {

        readonly IOrdemServicoEletronicaRepository _ordemServicoEletronicaRepository;

        public OSEletronicaNaoPodeExistirSpec(IOrdemServicoEletronicaRepository ordemServicoEletronicaRepository)
        {
            _ordemServicoEletronicaRepository = ordemServicoEletronicaRepository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return !entity.EhAssociada ? !_ordemServicoEletronicaRepository.VerificarExistencia(entity) : true;
        }
    }
}
