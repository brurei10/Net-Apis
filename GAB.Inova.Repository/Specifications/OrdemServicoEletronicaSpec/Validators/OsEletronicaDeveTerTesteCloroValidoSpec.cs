﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using System;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OsEletronicaDeveTerTesteCloroValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            if (entity.TesteCloro.HasValue)
            {
                return Enum.IsDefined(typeof(StatusTesteCloroEnum), entity.TesteCloro.Value);
            }
            return true;
        }
    }
}
