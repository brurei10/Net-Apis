﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaAssociadaDeveTerSequencialValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return entity.EhAssociada ? entity.SequencialVinculada.GetValueOrDefault(0) > 0 : true;
        }
    }
}
