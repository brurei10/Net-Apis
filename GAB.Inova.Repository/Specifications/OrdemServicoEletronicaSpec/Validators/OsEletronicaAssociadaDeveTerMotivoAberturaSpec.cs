﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaAssociadaDeveTerMotivoAberturaSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return entity.EhAssociada ? entity.MotivoAberturaID.GetValueOrDefault(0) > 0 : true;
        }
    }
}
