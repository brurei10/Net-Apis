﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using System.Linq;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaDeveTerLocalInstalacaoHDValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            var codigos = new long[3] { 99001, 99002, 99003 };
            if (codigos.Contains(entity.CodigoServico))
            {
                return entity.LocalInstalacaoHidrometroID.HasValue && entity.LocalInstalacaoHidrometroID.Value > 0;
            }
            return true;
        }
    }
}
