﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaDeveTerServicoValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        private readonly IOrdemServicoEletronicaRepository _repository;

        public OSEletronicaDeveTerServicoValidoSpec(IOrdemServicoEletronicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return !entity.EhAssociada ? _repository.VerificarCodigoServico(entity) : true;
        }
    }
}
