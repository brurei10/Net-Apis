﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators
{
    public class OSEletronicaDeveTerTipoLogradouroValidoSpec : ISpecification<OrdemServicoEletronica>
    {
        private readonly IOrdemServicoEletronicaRepository _repository;

        public OSEletronicaDeveTerTipoLogradouroValidoSpec(IOrdemServicoEletronicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronica entity)
        {
            return _repository.VerificarTipoLogradouro(entity);
        }
    }
}
