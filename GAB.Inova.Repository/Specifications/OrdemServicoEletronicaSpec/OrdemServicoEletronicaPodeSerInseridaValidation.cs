﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec.Validators;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec
{
    public class OrdemServicoEletronicaPodeSerInseridaValidation
        : Validator<OrdemServicoEletronica>
    {
        public OrdemServicoEletronicaPodeSerInseridaValidation(IOrdemServicoEletronicaRepository ordemServicoEletronicaRepository, OrdemServicoEletronica osEletronica)
        {
            #region Associada

            var associadaNaoExiste = new OSEletronicaAssociadaNaoPodeExistirSpec(ordemServicoEletronicaRepository);
            var associadaCodigoServico = new OSEletronicaAssociadaDeveTerCodigoDeServicoSpec();
            var associadaIdPai = new OSEletronicaAssociadaDeveTerIdOsPaiSpec();
            //SGAB-56 | var associadaMotivoAbertura = new OSEletronicaAssociadaDeveTerMotivoAberturaSpec();

            var associadaSequencialValido = new OSEletronicaAssociadaDeveTerSequencialValidoSpec();
            //SGAB-56 | var associadaMesmaEquipeExecutora = new OSEletronicaAssociadaDeveTerMesmaEquipeExecutoraOSPrincipalSpec(ordemServicoEletronicaRepository);

            base.Add("Associada", new Rule<OrdemServicoEletronica>(associadaNaoExiste, $"Ha duas ou mais ordens de serviço asssociadas com o numero sequencial '{osEletronica.SequencialVinculada.GetValueOrDefault()}'"));
            base.Add("AssociadaServico", new Rule<OrdemServicoEletronica>(associadaCodigoServico, $"A Ordem de Servico Associada de sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault()}' precisa de serviço informado"));
            base.Add("AssociadaIdPai", new Rule<OrdemServicoEletronica>(associadaIdPai, $"A Ordem de Servico Associada de sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault()}' precisa do id da Ordem de Servico Principal"));
            //SGAB-56 | base.Add("AssociadaMotivoAbertura", new Rule<OrdemServicoEletronica>(associadaMotivoAbertura, $"A Ordem de Servico Associada de sequencia'{osEletronica.SequencialVinculada.GetValueOrDefault()}' precisa do Motivo de Abertura"));

            base.Add("AssociadaSequencial", new Rule<OrdemServicoEletronica>(associadaSequencialValido, "Ha Ordem de Servico Associada sem o numero de sequencia"));
            //SGAB-56 | base.Add("AssociadaMesmaEquipeExecutora", new Rule<OrdemServicoEletronica>(associadaMesmaEquipeExecutora, $"A equipe executora da Ordem de Servico Associada de sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault()}' está divergente da equipe executora da Ordem de Serviço Principal"));

            #endregion

            #region Principal

            var principalNumeroOs = new OSEletronicaDeveTerNumeroOsSpec();
            var principalServico = new OSEletronicaDeveTerServicoValidoSpec(ordemServicoEletronicaRepository);
            var principalNaoExistir = new OSEletronicaNaoPodeExistirSpec(ordemServicoEletronicaRepository);

            base.Add("Principal", new Rule<OrdemServicoEletronica>(principalNaoExistir, "Ja existe uma Ordem de Servico Principal com mesmo numeroOs salva"));
            base.Add("PrincipalServico", new Rule<OrdemServicoEletronica>(principalNaoExistir, "A Ordem de Servico Principal precisa de serviço informado"));
            base.Add("PrincipalNumeroOs", new Rule<OrdemServicoEletronica>(principalNaoExistir, "A Ordem de Servico Principal precisa do numeroOs informado"));

            #endregion

            #region Ambas

            var ambasDiametroHidrometro = new OSEletronicaDeveTerDiametroHDValidoSpec();
            var ambasLocalInstalacaoHd = new OSEletronicaDeveTerLocalInstalacaoHDValidoSpec();

            var ambasEquipeExecutora = new OSEletronicaDeveTerEquipeExecutoraValidaSpec(ordemServicoEletronicaRepository);
            //var ambasTipoLogradouro = new OSEletronicaDeveTerTipoLogradouroValidoSpec(ordemServicoEletronicaRepository);

            var ambasTesteCloro = new OsEletronicaDeveTerTesteCloroValidoSpec();

            base.Add("EquipeExecutora", new Rule<OrdemServicoEletronica>(ambasEquipeExecutora, $"Equipe Executora se informada deve ser válida - {(osEletronica.EhAssociada ? $"Os Associada sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault(0)}'" : "Os principal")}"));
            //base.Add("TipoLogradouro", new Rule<OrdemServicoEletronica>(ambasTipoLogradouro, "Tipo de Logradouro se informado na Ordem de Servico, deve ser válido"));
            base.Add("TesteCloro", new Rule<OrdemServicoEletronica>(ambasTesteCloro, $"O valor do campo Teste de Cloro é inválido - {(osEletronica.EhAssociada ? $"Os Associada sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault(0)}'" : "Os principal")}"));

            base.Add("LocalInstalacaoHD", new Rule<OrdemServicoEletronica>(ambasLocalInstalacaoHd, $"O Campo 'LocalInstalacaoHidrometroID' preenchido corretamente é requisito para os Serviços de código 99001, 99002 e 99003 - {(osEletronica.EhAssociada ? $"Os Associada sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault(0)}'" : "Os principal")}"));
            base.Add("DiametroHidrometro", new Rule<OrdemServicoEletronica>(ambasDiametroHidrometro, $"O Campo 'DiametroHidrometroID' preenchido corretamente é requisito para os Serviços de código 99001, 99002 e 99003 - {(osEletronica.EhAssociada ? $"Os Associada sequencia '{osEletronica.SequencialVinculada.GetValueOrDefault(0)}'" : "Os principal")}"));
            #endregion
        }
    }
}
