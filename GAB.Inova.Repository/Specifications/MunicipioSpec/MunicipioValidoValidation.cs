﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.MunicipioSpec.Validators;

namespace GAB.Inova.Repository.Specifications.MunicipioSpec
{
    public class MunicipioValidoValidation
        : Validator<Municipio>
    {
        public MunicipioValidoValidation(IMunicipioRepository municipioRepository)
        {
            var validaCodigoMunicipio = new MunicipioExisteSpec(municipioRepository);

            base.Add("CodigoMunicipioValido", new Rule<Municipio>(validaCodigoMunicipio, "Município inválido."));
        }

    }
}
