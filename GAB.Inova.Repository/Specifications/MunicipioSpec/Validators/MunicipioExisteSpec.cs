﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.MunicipioSpec.Validators
{
    public class MunicipioExisteSpec
        : ISpecification<Municipio>
    {

        readonly IMunicipioRepository _municipioRepository;

        public MunicipioExisteSpec(IMunicipioRepository municipioRepository)
        {
            _municipioRepository = municipioRepository;
        }

        public bool IsSatisfiedBy(Municipio entity)
        {
            return _municipioRepository.VerificarExistencia(entity);
        }
    }
}

