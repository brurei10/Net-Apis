﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.BairroSpec.Validators;

namespace GAB.Inova.Repository.Specifications.BairroSpec
{
    public class BairroValidoValidation
        : Validator<Bairro>
    {
        public BairroValidoValidation(IBairroRepository bairroRepository)
        {
            var validaIdValido = new BairroExisteSpec(bairroRepository);

            base.Add("IdBairroValido", new Rule<Bairro>(validaIdValido, "Bairro inválido."));
        }

    }
}
