﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.BairroSpec.Validators
{
    public class BairroExisteSpec
        : ISpecification<Bairro>
    {

        readonly IBairroRepository _bairroRepository;

        public BairroExisteSpec(IBairroRepository bairroRepository)
        {
            _bairroRepository = bairroRepository;
        }

        public bool IsSatisfiedBy(Bairro entity)
        {
            return _bairroRepository.VerificarExistencia(entity);
        }
    }
}

