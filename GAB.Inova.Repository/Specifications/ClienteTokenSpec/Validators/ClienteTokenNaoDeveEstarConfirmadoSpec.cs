﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.ClienteTokenSpec.Validators
{
    public class ClienteTokenNaoDeveEstarConfirmadoSpec
        : ISpecification<ClienteToken>
    {
        public bool IsSatisfiedBy(ClienteToken entity)
        {
            return !entity.DataConfirmacao.HasValue;
        }
    }
}
