﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using System;

namespace GAB.Inova.Repository.Specifications.ClienteTokenSpec.Validators
{
    public class ClienteTokenNaoDeveEstarExpiradoSpec
        : ISpecification<ClienteToken>
    {
        public bool IsSatisfiedBy(ClienteToken entity)
        {
            return entity.DataExpiracao > DateTime.Now;
        }
    }
}
