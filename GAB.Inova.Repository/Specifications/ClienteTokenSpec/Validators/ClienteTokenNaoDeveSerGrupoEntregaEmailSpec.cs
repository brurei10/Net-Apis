﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ClienteTokenSpec.Validators
{
    public class ClienteTokenNaoDeveSerGrupoEntregaEmailSpec
        : ISpecification<ClienteToken>
    {

        readonly IClienteTokenRepository _clienteTokenRepository;

        public ClienteTokenNaoDeveSerGrupoEntregaEmailSpec(IClienteTokenRepository clienteTokenRepository)
        {
            _clienteTokenRepository = clienteTokenRepository;
        }

        public bool IsSatisfiedBy(ClienteToken entity)
        {
            if (entity.Tipo == TokenTipoEnum.ContaDigitalAlteracao) return true;

            var grupoEntrega = _clienteTokenRepository.ObterGrupoEntregaClientePorTokenAsync(entity.Token)
                .GetAwaiter()
                .GetResult();

            return grupoEntrega != GrupoEntregaEnum.EMAIL;
        }
    }
}
