﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.ClienteTokenSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ClienteTokenSpec
{
    public class ClienteTokenPodeConfirmarValidation
        : Validator<ClienteToken>
    {

        public ClienteTokenPodeConfirmarValidation(IClienteTokenRepository clienteTokenRepository)
        {
            var naoDeveEstarExpirado = new ClienteTokenNaoDeveEstarExpiradoSpec();
            var naoDeveEstarConfirmado = new ClienteTokenNaoDeveEstarConfirmadoSpec();
            var naoDeveSerGrupoEntregaEmail = new ClienteTokenNaoDeveSerGrupoEntregaEmailSpec(clienteTokenRepository);

            base.Add("NaoDeveEstarExpirado", new Rule<ClienteToken>(naoDeveEstarExpirado, "Prezado Cliente, seu link expirou. Para aderir ao serviço de Conta digital, cadastre-se em nossa Agência Virtual ou entre em contato com nossos canais de atendimento."));
            base.Add("NaoDeveEstarConfirmado", new Rule<ClienteToken>(naoDeveEstarConfirmado, "Prezado Cliente, este link já foi confirmado."));
            base.Add("NaoDeveSerGrupoEntregaEmail", new Rule<ClienteToken>(naoDeveSerGrupoEntregaEmail, "Prezado Cliente, serviço de conta digital já cadastrado em outro e-mail. Em caso de alteração, acesse a opção “Conta Digital” em nossa agência virtual ou entre em contato com nossos canais de atendimento."));
        }

    }
}

