﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec
{
    public class ProtocoloAtencimentoPodeFinalizarValidation
        : Validator<ProtocoloAtendimento>
    {

        public ProtocoloAtencimentoPodeFinalizarValidation(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            var naoPodeFinalizarSemMatricula = new ProtocoloAtendimentoNaoPodeFinalizarSemMatriculaSpec(protocoloAtendimentoRepository);
            var naoPodeFinalizarSemNatureza = new ProtocoloAtendimentoNaoPodeFinalizarSemNaturezaSpec(protocoloAtendimentoRepository);
            var naoPodeFinalizarSemObservacao = new ProtocoloAtendimentoNaoPodeFinalizarSemObservacaoSpec(protocoloAtendimentoRepository);

            base.Add("NaoPodeFinalizarSemMatricula", new Rule<ProtocoloAtendimento>(naoPodeFinalizarSemMatricula, $"Não é possível finalizar protocolo no iNova sem informar uma matrícula."));
            base.Add("NaoPodeFinalizarSemNatureza", new Rule<ProtocoloAtendimento>(naoPodeFinalizarSemNatureza, $"Não é possível finalizar protocolo no iNova sem informar uma natureza."));
            base.Add("NaoPodeFinalizarSemObservacao", new Rule<ProtocoloAtendimento>(naoPodeFinalizarSemObservacao, $"Não é possível finalizar protocolo no iNova sem preencher uma observação."));
        }

    }
}
