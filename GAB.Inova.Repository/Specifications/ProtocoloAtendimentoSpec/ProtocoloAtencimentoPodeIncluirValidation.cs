﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec
{
    public class ProtocoloAtencimentoPodeIncluirValidation
        : Validator<ProtocoloAtendimento>
    {

        public ProtocoloAtencimentoPodeIncluirValidation(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            var naoPodeSerCanalConsultaInterna = new ProtocoloAtendimentoNaoPodeSerCanalConsultaInternaSpec();
            var naoFinalizadoParaUsuario = new ProtocoloAtendimentoEstaoFinalizadosParaUsuarioSpec(protocoloAtendimentoRepository);

            base.Add("NaoPodeSerCanalConsultaInterna", new Rule<ProtocoloAtendimento>(naoPodeSerCanalConsultaInterna, $"Não é possível criar protocolo no iNova através do canal de atendimento: {CanalAtendimentoEnum.ConsultaInterna.GetDescription()}."));
            base.Add("NaoFinalizadoParaUsuario", new Rule<ProtocoloAtendimento>(naoFinalizadoParaUsuario, "Existe(m) protocolo(s) não finalizado(s) para o usuário."));
        }

    }
}
