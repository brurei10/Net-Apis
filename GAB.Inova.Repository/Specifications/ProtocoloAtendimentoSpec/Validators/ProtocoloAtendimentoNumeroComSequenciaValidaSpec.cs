﻿using DomainValidationCore.Interfaces.Specification;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoNumeroComSequenciaValidaSpec
        : ISpecification<string>
    {
        public bool IsSatisfiedBy(string numeroProtocolo)
        {
            if (string.IsNullOrEmpty(numeroProtocolo) || string.IsNullOrWhiteSpace(numeroProtocolo) || numeroProtocolo.Length < 8) return false;

            return long.TryParse(numeroProtocolo.Substring(8), out long sequencia);
        }
    }
}
