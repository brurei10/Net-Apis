﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoEstaoFinalizadosParaUsuarioSpec
        : ISpecification<ProtocoloAtendimento>
    {

        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;

        public ProtocoloAtendimentoEstaoFinalizadosParaUsuarioSpec(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimento entity)
        {
            /// IdUsuario igual a sistema
            if (entity.IdUsuario == 0) return true;

            var protocoloAtendimento = _protocoloAtendimentoRepository.ObterProtocoloNaoFinalizadoPorUsuarioAsync(entity.IdUsuario)
                .GetAwaiter()
                .GetResult();

            return protocoloAtendimento == null;
        }

    }
}
