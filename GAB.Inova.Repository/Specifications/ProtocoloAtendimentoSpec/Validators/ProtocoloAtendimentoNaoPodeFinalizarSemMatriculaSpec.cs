﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoNaoPodeFinalizarSemMatriculaSpec
        : ISpecification<ProtocoloAtendimento>
    {

        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;

        public ProtocoloAtendimentoNaoPodeFinalizarSemMatriculaSpec(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimento entity)
        {
            var protocoloAtendimento = _protocoloAtendimentoRepository.ObterPorIdAsync(entity.Id)
                .GetAwaiter()
                .GetResult();

            return !string.IsNullOrEmpty(protocoloAtendimento?.Matricula);
        }

    }
}
