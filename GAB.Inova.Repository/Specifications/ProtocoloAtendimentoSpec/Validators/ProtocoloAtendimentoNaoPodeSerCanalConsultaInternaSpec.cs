﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoNaoPodeSerCanalConsultaInternaSpec
        : ISpecification<ProtocoloAtendimento>
    {
        public bool IsSatisfiedBy(ProtocoloAtendimento entity)
        {
            return entity.IdTipoAtendimento != CanalAtendimentoEnum.ConsultaInterna;
        }
    }
}
