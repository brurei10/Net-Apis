﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoNaoPodeFinalizarSemNaturezaSpec
        : ISpecification<ProtocoloAtendimento>
    {

        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;

        public ProtocoloAtendimentoNaoPodeFinalizarSemNaturezaSpec(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimento entity)
        {
            return _protocoloAtendimentoRepository.VerificarCadastramentoDeNaturezaAsync(entity.Id)
                .GetAwaiter()
                .GetResult();
        }

    }
}
