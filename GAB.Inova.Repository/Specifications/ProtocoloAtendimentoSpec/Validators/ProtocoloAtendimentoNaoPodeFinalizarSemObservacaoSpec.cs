﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators
{
    public class ProtocoloAtendimentoNaoPodeFinalizarSemObservacaoSpec
        : ISpecification<ProtocoloAtendimento>
    {

        readonly IProtocoloAtendimentoRepository _protocoloAtendimentoRepository;

        public ProtocoloAtendimentoNaoPodeFinalizarSemObservacaoSpec(IProtocoloAtendimentoRepository protocoloAtendimentoRepository)
        {
            _protocoloAtendimentoRepository = protocoloAtendimentoRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimento entity)
        {
            var protocoloAtendimento = _protocoloAtendimentoRepository.ObterPorIdAsync(entity.Id)
                .GetAwaiter()
                .GetResult();

            return !string.IsNullOrEmpty(protocoloAtendimento?.Observacao);
        }
    }
}
