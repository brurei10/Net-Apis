﻿using DomainValidationCore.Validation;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec
{
    public class ProtocoloAtendimentoGeracaoNumeroValidation
        : Validator<string>
    {

        public ProtocoloAtendimentoGeracaoNumeroValidation()
        {
            var numeroComSequenciaValida = new ProtocoloAtendimentoNumeroComSequenciaValidaSpec();

            base.Add("NumeroComSequenciaValida", new Rule<string>(numeroComSequenciaValida, "Não foi possível gerar protocolo no iNova."));
        }

    }
}
