﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.OrdemServicoAnexoExternoSpec.Validators;

namespace GAB.Inova.Repository.Specifications.OrdemServicoAnexoExternoSpec
{
    public class OSAnexoExternoPodeSerInseridoValidation
        : Validator<OrdemServicoAnexoExterno>
    {
        public OSAnexoExternoPodeSerInseridoValidation(IOrdemServicoAnexoExternoRepository ordemServicoAnexoExternoRepository)
        {
            var validaExiste = new OSAnexoExternoNaoDeveExistirSpec(ordemServicoAnexoExternoRepository);

            base.Add("Existe", new Rule<OrdemServicoAnexoExterno>(validaExiste, "Não foi possível salvar o retorno da Ordem de Servico pois existe um Anexo com duplicidade de Link."));
        }
    }
}
