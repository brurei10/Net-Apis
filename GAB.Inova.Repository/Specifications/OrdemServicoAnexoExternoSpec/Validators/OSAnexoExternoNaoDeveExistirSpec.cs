﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoAnexoExternoSpec.Validators
{
    public class OSAnexoExternoNaoDeveExistirSpec 
        : ISpecification<OrdemServicoAnexoExterno>
    {

        readonly IOrdemServicoAnexoExternoRepository _ordemServicoAnexoExternoRepository;

        public OSAnexoExternoNaoDeveExistirSpec(IOrdemServicoAnexoExternoRepository ordemServicoAnexoExternoRepository)
        {
            _ordemServicoAnexoExternoRepository = ordemServicoAnexoExternoRepository;
        }

        public bool IsSatisfiedBy(OrdemServicoAnexoExterno entity) => !_ordemServicoAnexoExternoRepository.VerificarExistenciaAsync(entity).GetAwaiter().GetResult();

    }
}
