﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators
{
    public class ProtocoloAtendimentoGfaEstaoFinalizadosParaUsuarioSpec
        : ISpecification<ProtocoloAtendimentoGfa>
    {

        readonly IProtocoloAtendimentoGfaRepository _protocoloAtendimentoGfaRepository;

        public ProtocoloAtendimentoGfaEstaoFinalizadosParaUsuarioSpec(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            _protocoloAtendimentoGfaRepository = protocoloAtendimentoGfaRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimentoGfa entity)
        {
            var protocoloAtendimentoGfa = _protocoloAtendimentoGfaRepository.ObterAtendimentoNaoFinalizadoPorUsuarioAsync(entity.UsuarioGfa)
                .GetAwaiter()
                .GetResult();

            return protocoloAtendimentoGfa == null;
        }

    }
}
