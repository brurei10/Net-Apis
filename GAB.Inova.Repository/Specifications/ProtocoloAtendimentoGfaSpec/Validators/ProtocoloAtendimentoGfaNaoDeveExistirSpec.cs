﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators
{
    /// <summary>
    /// A classe é responsável em verificar se o numero do protocolo e senha enviados pela integração do GFA não existem
    /// </summary>
    public class ProtocoloAtendimentoGfaNaoDeveExistirSpec
        : ISpecification<ProtocoloAtendimentoGfa>
    {

        readonly IProtocoloAtendimentoGfaRepository _protocoloAtendimentoGfaRepository;

        public ProtocoloAtendimentoGfaNaoDeveExistirSpec(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            _protocoloAtendimentoGfaRepository = protocoloAtendimentoGfaRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimentoGfa entity)
        {
            var protocoloAtendimentoGfa = _protocoloAtendimentoGfaRepository.ObterPorNumeroESenhaAsync(entity.NumeroProtocoloGfa, entity.SenhaGfa)
                .GetAwaiter()
                .GetResult();
            
            return protocoloAtendimentoGfa == null;
        }

    }
}
