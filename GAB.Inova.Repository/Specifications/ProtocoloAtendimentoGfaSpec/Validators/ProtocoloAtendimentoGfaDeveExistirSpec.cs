﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators
{
    public class ProtocoloAtendimentoGfaDeveExistirSpec
        : ISpecification<ProtocoloAtendimentoGfa>
    {

        readonly IProtocoloAtendimentoGfaRepository _protocoloAtendimentoGfaRepository;

        public ProtocoloAtendimentoGfaDeveExistirSpec(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            _protocoloAtendimentoGfaRepository = protocoloAtendimentoGfaRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimentoGfa entity)
        {
            var protocoloAtendimentoGfa = _protocoloAtendimentoGfaRepository.ObterPorNumeroESenhaAsync(entity.NumeroProtocoloGfa, entity.SenhaGfa)
                .GetAwaiter()
                .GetResult();

            return protocoloAtendimentoGfa != null;
        }

    }
}
