﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using System.Linq;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators
{
    public class ProtocoloAtendimentoGfaNaoPodeExistirProtocoloInovaAbertoSpec
        : ISpecification<ProtocoloAtendimentoGfa>
    {

        readonly IProtocoloAtendimentoGfaRepository _protocoloAtendimentoGfaRepository;

        public ProtocoloAtendimentoGfaNaoPodeExistirProtocoloInovaAbertoSpec(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            _protocoloAtendimentoGfaRepository = protocoloAtendimentoGfaRepository;
        }

        public bool IsSatisfiedBy(ProtocoloAtendimentoGfa entity)
        {
            var protocoloAtendimentoGfa = _protocoloAtendimentoGfaRepository.ObterPorNumeroESenha(entity.NumeroProtocoloGfa, entity.SenhaGfa);

            var existe = protocoloAtendimentoGfa?.ProtocoloAtendimentos.Any(q => q.DataFim == null);

            return !(existe.HasValue && existe.Value && !protocoloAtendimentoGfa.DataFinalAtendimento.HasValue);
        }

    }
}
