﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec
{
    public class ProtocoloAtendimentoGfaPodeAlterarValidation
        : Validator<ProtocoloAtendimentoGfa>
    {

        public ProtocoloAtendimentoGfaPodeAlterarValidation(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            var deveExistir = new ProtocoloAtendimentoGfaDeveExistirSpec(protocoloAtendimentoGfaRepository);
            var naoPodeEstarFinalizado = new ProtocoloAtendimentoGfaNaoPodeEstarFinalizadoSpec(protocoloAtendimentoGfaRepository);

            base.Add("DeveExistir", new Rule<ProtocoloAtendimentoGfa>(deveExistir, "O número de protocolo e senha do GFA não existem."));
            base.Add("NaoPodeEstarFinalizado", new Rule<ProtocoloAtendimentoGfa>(naoPodeEstarFinalizado, "O atendimento foi finalizado para o número de protocolo e senha do GFA informado."));
        }

    }
}
