﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec
{
    public class ProtocoloAtendimentoGfaPodeIncluirValidation
        : Validator<ProtocoloAtendimentoGfa>
    {

        public ProtocoloAtendimentoGfaPodeIncluirValidation(IProtocoloAtendimentoGfaRepository protocoloAtendimentoGfaRepository)
        {
            var naoDeveExistir = new ProtocoloAtendimentoGfaNaoDeveExistirSpec(protocoloAtendimentoGfaRepository);
            var naoFinalizadoParaUsuario = new ProtocoloAtendimentoGfaEstaoFinalizadosParaUsuarioSpec(protocoloAtendimentoGfaRepository);

            base.Add("NaoDeveExistir", new Rule<ProtocoloAtendimentoGfa>(naoDeveExistir, "O número de protocolo e senha do GFA já existem."));
            base.Add("NaoFinalizadoParaUsuario", new Rule<ProtocoloAtendimentoGfa>(naoFinalizadoParaUsuario, "Existe(m) atendimento(s) não finalizado(s) para o usuário."));
        }

    }
}
