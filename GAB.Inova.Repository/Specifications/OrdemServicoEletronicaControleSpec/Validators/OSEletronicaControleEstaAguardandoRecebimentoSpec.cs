﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaControleSpec.Validators
{
    public class OSEletronicaControleEstaAguardandoRecebimentoSpec
        : ISpecification<OrdemServicoEletronicaControle>
    {

        readonly IOrdemServicoEletronicaControleRepository _ordemServicoEletronicaControleRepository;

        public OSEletronicaControleEstaAguardandoRecebimentoSpec(IOrdemServicoEletronicaControleRepository ordemServicoEletronicaControleRepository)
        {
            _ordemServicoEletronicaControleRepository = ordemServicoEletronicaControleRepository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronicaControle entity)
        {
            var controle = _ordemServicoEletronicaControleRepository.Obter(entity.NumeroOS, entity.SequenciaOS);
            if(controle != null)
            {
                return controle.Situacao == SituacaoOSEletronicaEnum.ENVIADA;
            }
            return true; 
        }
    }
}
