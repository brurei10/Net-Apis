﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaControleSpec.Validators
{
    public class OSEletronicaControleDeveExistirSpec
        : ISpecification<OrdemServicoEletronicaControle>
    {

        readonly IOrdemServicoEletronicaControleRepository _ordemServicoEletronicaControleRepository;

        public OSEletronicaControleDeveExistirSpec(IOrdemServicoEletronicaControleRepository ordemServicoEletronicaControleRepository)
        {
            _ordemServicoEletronicaControleRepository = ordemServicoEletronicaControleRepository;
        }

        public bool IsSatisfiedBy(OrdemServicoEletronicaControle entity)
        {
            return _ordemServicoEletronicaControleRepository.Obter(entity.NumeroOS, entity.SequenciaOS) != null;
        }

    }
}
