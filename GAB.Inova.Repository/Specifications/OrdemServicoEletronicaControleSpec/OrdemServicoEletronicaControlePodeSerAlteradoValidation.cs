﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Specifications.OrdemServicoEletronicaControleSpec.Validators;

namespace GAB.Inova.Repository.Specifications.OrdemServicoEletronicaControleSpec
{
    public class OrdemServicoEletronicaControlePodeSerAlteradoValidation
        : Validator<OrdemServicoEletronicaControle>
    {
        public OrdemServicoEletronicaControlePodeSerAlteradoValidation(IOrdemServicoEletronicaControleRepository ordemServicoEletronicaControleRepository)
        {
            var validaExiste = new OSEletronicaControleDeveExistirSpec(ordemServicoEletronicaControleRepository);
            var validaStatus = new OSEletronicaControleEstaAguardandoRecebimentoSpec(ordemServicoEletronicaControleRepository);

            base.Add("Existe", new Rule<OrdemServicoEletronicaControle>(validaExiste, "Não foi possível salvar o retorno da Ordem de Servico Eletrônica pois não foi localizado o registro de controle de envio ao Sistema Especialista."));
            base.Add("Status", new Rule<OrdemServicoEletronicaControle>(validaStatus, "Não foi possível salvar o retorno da Ordem de Servico Eletrônica pois o registro de controle está com status diferente de 'Enviada'."));
        }
    }
}
