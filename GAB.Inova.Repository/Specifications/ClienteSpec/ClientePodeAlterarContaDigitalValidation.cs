﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Repository.Specifications.ClienteSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ClienteSpec
{
    public class ClientePodeAlterarContaDigitalValidation
        : Validator<Cliente>
    {

        public ClientePodeAlterarContaDigitalValidation()
        {
            var deveSerContaDigital = new ClienteDeveSerContaDigitalSpec();

            base.Add("DeveSerContaDigital", new Rule<Cliente>(deveSerContaDigital, "Cliente não possui Conta Digital."));
        }

    }
}
