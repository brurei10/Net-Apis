﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Repository.Specifications.ClienteSpec.Validators;

namespace GAB.Inova.Repository.Specifications.ClienteSpec
{
    public class ClientePodeAderirContaDigitalValidation
        : Validator<Cliente>
    {

        public ClientePodeAderirContaDigitalValidation()
        {
            var naoDeveSerContaDigital = new ClienteNaoDeveSerContaDigitalSpec();
            var deveSerFaturaNormal = new ClienteDeveSerFaturaNormalSpec();

            base.Add("NaoDeveSerContaDigital", new Rule<Cliente>(naoDeveSerContaDigital, "Você já aderiu ao serviço de conta digital. Para mais informações, entre em contato com nossa central de relacionamento."));
            base.Add("DeveSerFaturaNormal", new Rule<Cliente>(deveSerFaturaNormal, "Ação não permitida. Para mais informações, entre em contato com nossa central de relacionamento."));
        }

    }
}
