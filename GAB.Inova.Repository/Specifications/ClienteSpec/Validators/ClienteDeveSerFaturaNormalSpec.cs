﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.ClienteSpec.Validators
{
    public class ClienteDeveSerFaturaNormalSpec
        : ISpecification<Cliente>
    {

        public bool IsSatisfiedBy(Cliente entity)
        {
            return entity.IdTipoFatura == TipoFaturaEnum.FaturaNormal;
        }

    }
}
