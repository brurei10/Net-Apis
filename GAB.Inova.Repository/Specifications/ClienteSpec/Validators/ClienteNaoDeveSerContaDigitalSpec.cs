﻿using DomainValidationCore.Interfaces.Specification;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Specifications.ClienteSpec.Validators
{
    public class ClienteNaoDeveSerContaDigitalSpec
        : ISpecification<Cliente>
    {
        public bool IsSatisfiedBy(Cliente entity)
        {
            return entity.IdGrupoEntrega != GrupoEntregaEnum.EMAIL;
        }
    }
}
