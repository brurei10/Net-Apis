﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class VwDadosDebitoAnualRepository : DapperRepository, IVwDadosDebitoAnualRepository
    {

        readonly ILogger _logger;

        public VwDadosDebitoAnualRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<VwDadosDebitoAnualRepository>();
        }

        public async Task<IEnumerable<VwDadosDebitoAnual>> ObterAsync(long seqDeclaracaoAnual)
        {
            try
            {
                var sql = @"SELECT *
                              FROM VW_DDS_DBT_NL
                             WHERE SEQ_DECLARACAO = :P_SEQ_DECLARACAO";

                OracleParameter.Add("P_SEQ_DECLARACAO", seqDeclaracaoAnual, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryAsync<VwDadosDebitoAnual>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwDadosDebitoAnual)} [seqDeclaracaoAnual:{seqDeclaracaoAnual}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
