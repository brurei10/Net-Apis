﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Enums;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ProtocoloAtendimentoRepository
        : DapperRepository, IProtocoloAtendimentoRepository
    {

        readonly ILogger _logger;

        public ProtocoloAtendimentoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ProtocoloAtendimentoRepository>();
        }
        
        public async Task<ProtocoloAtendimento> ObterPorIdAsync(long idProtocoloAtendimento)
        {
            try
            {
                return await ObterAsync<long>(ProtocoloAtendimentoDbTypes.Id, idProtocoloAtendimento);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ProtocoloAtendimento)} [id:{idProtocoloAtendimento}].");

                throw;
            }
        }

        public async Task<ProtocoloAtendimento> ObterPorNumeroAsync(string numeroProtocolo)
        {
            try
            {
                return await ObterAsync<string>(ProtocoloAtendimentoDbTypes.NumeroProtocolo, numeroProtocolo);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ProtocoloAtendimento)} [nº do protocolo:{numeroProtocolo}].");

                throw;
            }
        }

        public async Task<ProtocoloAtendimento> ObterProtocoloNaoFinalizadoPorUsuarioAsync(long idUsuario)
        {
            try
            {
                var sql = @"SELECT PA.ID_PROTOCOLO_ATENDIMENTO,
                                   PA.MATRICULA,
                                   PA.ID_CONTRATO,
                                   PA.DATA_INICIO,
                                   PA.DATA_FIM,
                                   PA.ID_USUARIO,
                                   PA.ID_TIPO_ATENDIMENTO,
                                   PA.CPF_CNPJ,
                                   PA.OBS,
                                   PA.NOME_SOLICITANTE,
                                   PA.TELEFONE,
                                   PA.ID_LOCAL_ATENDIMENTO,
                                   PA.TIPO_PROTOCOLO,
                                   PA.PREFIXO_PROTOCOLO,
                                   PA.SEQUENCIA_PROTOCOLO,
                                   PA.ORIGEM_INOVA,
                                   PA.ID_PROTOCOLO_ATEND_GFA,
                                   PA.NR_PROTOCOLO_PAI,
                                   PA.DT_INTEGRACAO
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                             WHERE PA.DATA_FIM IS NULL
                               AND PA.TIPO_PROTOCOLO = 1
                               AND PA.ID_USUARIO = :P_ID_USUARIO";

                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ProtocoloAtendimento>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter protocolo iNova não finalizado por usuário - {typeof(ProtocoloAtendimento)} [id usuário:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<ProtocoloAtendimento>> ListarProtocolosAbertosPorNumeroESenhaGfaAsync(string numeroProtocoloGfa, string senhaGfa)
        {
            try
            {
                var sql = @"SELECT PA.ID_PROTOCOLO_ATENDIMENTO,
                                   PA.MATRICULA,
                                   PA.ID_CONTRATO,
                                   PA.DATA_INICIO,
                                   PA.DATA_FIM,
                                   PA.ID_USUARIO,
                                   PA.ID_TIPO_ATENDIMENTO,
                                   PA.CPF_CNPJ,
                                   PA.OBS,
                                   PA.NOME_SOLICITANTE,
                                   PA.TELEFONE,
                                   PA.ID_LOCAL_ATENDIMENTO,
                                   PA.TIPO_PROTOCOLO,
                                   PA.PREFIXO_PROTOCOLO,
                                   PA.SEQUENCIA_PROTOCOLO,
                                   PA.ORIGEM_INOVA,
                                   PA.ID_PROTOCOLO_ATEND_GFA,
                                   PA.NR_PROTOCOLO_PAI,
                                   PA.DT_INTEGRACAO
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                              JOIN NETUNO.PROTOCOLO_ATEND_GFA PAG
                                ON PAG.ID_PROTOCOLO_ATEND_GFA = PA.ID_PROTOCOLO_ATEND_GFA
                             WHERE PA.DATA_FIM IS NULL
                               AND PAG.NR_SENHA_GFA = :P_NR_SENHA_GFA
                               AND PAG.NR_PROTOCOLO_GFA = :P_NR_PROTOCOLO_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", senhaGfa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", numeroProtocoloGfa, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<ProtocoloAtendimento>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao listar protocolo(s) aberto(s) - {typeof(ProtocoloAtendimento)} [GFA - nº protocolo:{numeroProtocoloGfa}, senha:{senhaGfa}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<ProtocoloAtendimento>> ListarProtocolosNaoIntegradosPorNumeroESenhaGfaAsync(string numeroProtocoloGfa, string senhaGfa)
        {
            try
            {
                var sql = @"SELECT PA.ID_PROTOCOLO_ATENDIMENTO,
                                   PA.MATRICULA,
                                   PA.ID_CONTRATO,
                                   PA.DATA_INICIO,
                                   PA.DATA_FIM,
                                   PA.ID_USUARIO,
                                   PA.ID_TIPO_ATENDIMENTO,
                                   PA.CPF_CNPJ,
                                   PA.OBS,
                                   PA.NOME_SOLICITANTE,
                                   PA.TELEFONE,
                                   PA.ID_LOCAL_ATENDIMENTO,
                                   PA.TIPO_PROTOCOLO,
                                   PA.PREFIXO_PROTOCOLO,
                                   PA.SEQUENCIA_PROTOCOLO,
                                   PA.ORIGEM_INOVA,
                                   PA.ID_PROTOCOLO_ATEND_GFA,
                                   PA.NR_PROTOCOLO_PAI,
                                   PA.DT_INTEGRACAO
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                              JOIN NETUNO.PROTOCOLO_ATEND_GFA PAG
                                ON PAG.ID_PROTOCOLO_ATEND_GFA = PA.ID_PROTOCOLO_ATEND_GFA
                             WHERE PA.DT_INTEGRACAO IS NULL
                               AND PAG.NR_SENHA_GFA = :P_NR_SENHA_GFA
                               AND PAG.NR_PROTOCOLO_GFA = :P_NR_PROTOCOLO_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", senhaGfa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", numeroProtocoloGfa, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<ProtocoloAtendimento>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao listar protocolo(s) não integrado(s) - {typeof(ProtocoloAtendimento)} [GFA - nº protocolo:{numeroProtocoloGfa}, senha:{senhaGfa}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<TabNaturezaSolicitacao>> ObterNaturezasSolicitacaoConsolidadoAsync(string numeroProtocoloPai)
        {
            try
            {
                var sql = @"SELECT DISTINCT (PAD.ID_NATUREZA), TNS.DESCRICAO
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                              JOIN NETUNO.PROTOCOLO_ATEND_DETALHE PAD
                                ON PAD.ID_PROTOCOLO_ATEND = PA.ID_PROTOCOLO_ATENDIMENTO
                              JOIN NETUNO.TAB_NATUREZA_SOLICITACAO TNS
                                ON TNS.ID_NATUREZA = PAD.ID_NATUREZA
                             WHERE PA.NR_PROTOCOLO_PAI = :P_NR_PROTOCOLO_PAI
                             ORDER BY 1";

                OracleParameter.Add("P_NR_PROTOCOLO_PAI", numeroProtocoloPai, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<TabNaturezaSolicitacao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter natureza(s) consolidada(s) - {typeof(ProtocoloAtendimento)} [id:{numeroProtocoloPai}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarCadastramentoDeNaturezaAsync(long idProtocoloAtendimento)
        {
            try
            {
                var sql = @"SELECT COUNT(PAD.ID_NATUREZA)
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                              JOIN NETUNO.PROTOCOLO_ATEND_DETALHE PAD
                                ON PAD.ID_PROTOCOLO_ATEND = PA.ID_PROTOCOLO_ATENDIMENTO
                               AND PAD.ID_NATUREZA IS NOT NULL
                             WHERE PA.ID_PROTOCOLO_ATENDIMENTO = :P_ID_PROTOCOLO_ATENDIMENTO";

                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", idProtocoloAtendimento, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteScalarAsync<int>(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar se natureza(s) - {typeof(ProtocoloAtendimento)} [id:{idProtocoloAtendimento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        
        public async Task<bool> AtualizarIntegracaoGfaAsync(ProtocoloAtendimento protocoloAtendimento)
        {
            try
            {
                var sql = @"UPDATE NETUNO.PROTOCOLO_ATENDIMENTO PA
                                   SET PA.ID_PROTOCOLO_ATEND_GFA = :P_ID_PROTOCOLO_ATEND_GFA,
                                       PA.NR_PROTOCOLO_PAI = :P_NR_PROTOCOLO_PAI
                                 WHERE PA.ID_PROTOCOLO_ATENDIMENTO = :P_ID_PROTOCOLO_ATENDIMENTO";

                OracleParameter.Add("P_ID_PROTOCOLO_ATEND_GFA", protocoloAtendimento.IdProtocoloAtendimentoGfa, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_PAI", protocoloAtendimento.NumeroProtocoloPai, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", protocoloAtendimento.Id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar dados de integração GFA - {typeof(ProtocoloAtendimento)} [id:{protocoloAtendimento.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarDadosClienteAsync(ProtocoloAtendimento protocoloAtendimento)
        {
            try
            {
                var sql = @"UPDATE NETUNO.PROTOCOLO_ATENDIMENTO PA
                               SET PA.MATRICULA   = :P_MATRICULA,
                                   PA.CPF_CNPJ    = :P_CPF_CNPJ,
                                   PA.ID_CONTRATO = :P_ID_CONTRATO
                             WHERE PA.ID_PROTOCOLO_ATENDIMENTO = :P_ID_PROTOCOLO_ATENDIMENTO";

                OracleParameter.Add("P_MATRICULA", protocoloAtendimento.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CPF_CNPJ", (string.IsNullOrEmpty(protocoloAtendimento.CpfCnpj) ? DBNull.Value : (object)protocoloAtendimento.CpfCnpj), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CONTRATO", ((protocoloAtendimento.IdContrato == default(long)) ? DBNull.Value : (object)protocoloAtendimento.IdContrato), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", protocoloAtendimento.Id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar dados de cliente - {typeof(ProtocoloAtendimento)} [id:{protocoloAtendimento.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarDataIntegracaoAsync(string numeroProtocoloPai)
        {
            try
            {
                var sql = @"UPDATE NETUNO.PROTOCOLO_ATENDIMENTO PA
                                   SET PA.DT_INTEGRACAO = SYSDATE
                                 WHERE PA.NR_PROTOCOLO_PAI = :P_NR_PROTOCOLO_PAI";

                OracleParameter.Add("P_NR_PROTOCOLO_PAI", numeroProtocoloPai, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar data de integração - {typeof(ProtocoloAtendimento)} [nº protocolo pai:{numeroProtocoloPai}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> FinalizarAsync(ProtocoloAtendimento protocoloAtendimento)
        {
            try
            {
                var sql = @"UPDATE NETUNO.PROTOCOLO_ATENDIMENTO PA
                                   SET PA.DATA_FIM         = SYSDATE,
                                       PA.OBS              = :P_OBS
                                 WHERE PA.ID_PROTOCOLO_ATENDIMENTO = :P_ID_PROTOCOLO_ATENDIMENTO";

                OracleParameter.Add("P_OBS", (!string.IsNullOrEmpty(protocoloAtendimento.Observacao) ? protocoloAtendimento.Observacao : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", protocoloAtendimento.Id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar - {typeof(ProtocoloAtendimento)} [id:{protocoloAtendimento.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<string> GerarAsync(ProtocoloAtendimento protocoloAtendimento)
        {
            try
            {
                var sql = @"PACK_PROTOCOLO_ATENDIMENTO.PR_GERAR";

                var idContrato = (protocoloAtendimento.Contrato?.Id > 0 ? protocoloAtendimento.Contrato.Id : (object)DBNull.Value);
                var idCanalAtendimento = (int)protocoloAtendimento.IdTipoAtendimento;
                var idLocalAtendimento = (int)protocoloAtendimento.IdLocalAtendimento;

                OracleParameter.Add("P_ID_EMPRESA", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_USUARIO", protocoloAtendimento.IdUsuario, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CANAL_ATENDIMENTO", idCanalAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ORIGEM", DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", (!string.IsNullOrEmpty(protocoloAtendimento.Matricula) ? protocoloAtendimento.Matricula : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CONTRATO", idContrato, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_FIM", (protocoloAtendimento.DataFim.HasValue ? protocoloAtendimento.DataFim.Value : (object)DBNull.Value), OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_CPF_CNPJ", (!string.IsNullOrEmpty(protocoloAtendimento.CpfCnpj) ? protocoloAtendimento.CpfCnpj : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_OBS", (!string.IsNullOrEmpty(protocoloAtendimento.Observacao) ? protocoloAtendimento.Observacao : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NOME_SOLICITANTE", (!string.IsNullOrEmpty(protocoloAtendimento.Solicitante) ? protocoloAtendimento.Solicitante : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TELEFONE", (!string.IsNullOrEmpty(protocoloAtendimento.Telefone) ? protocoloAtendimento.Telefone : (object)DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_IDENT_ATEND", DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_IDENT_OBS", DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_PROTOCOLO", (protocoloAtendimento.TipoProtocolo > 0 ? protocoloAtendimento.TipoProtocolo : (object)DBNull.Value), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_LOCAL_ATENDIMENTO", idLocalAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_LOCALIDADE", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_OUT_NUMERO_PROTOCOLO", dbType: OracleDbType.Varchar2, direction: ParameterDirection.Output, size: 50);
                OracleParameter.Add("P_ORIGEM_INOVA", (protocoloAtendimento.OrigemInova ? 1 : 0), OracleDbType.Int32, ParameterDirection.Input);

                await DbConn.ExecuteAsync(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);

                var output = OracleParameter.Get<OracleString>("P_OUT_NUMERO_PROTOCOLO");

                return output.IsNull ? string.Empty : output.Value;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao gerar - {typeof(ProtocoloAtendimento)} [canal:{protocoloAtendimento.IdTipoAtendimento}, local:{protocoloAtendimento.IdLocalAtendimento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task AssociarLigacaoAsync(string protocolo, string matricula)
        {
            try
            {
                var sql = @"PACK_PROTOCOLO_ATENDIMENTO.PR_ASSOCIAR_LIGACAO";

                OracleParameter.Add("P_PROTOCOLO", protocolo, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NUMLIGACAO", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                await DbConn.ExecuteAsync(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao associar protocolo a matricula  [protocolo:{protocolo}, matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<Atendimento> FinalizarAsync(string protocolo, string resolucao)
        {
            try
            {
                OracleParameter.Add("P_PROTOCOLO", protocolo, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_RESOLUCAO", resolucao, OracleDbType.Varchar2, ParameterDirection.Input);

                OracleParameter.Add("P_OUT_CUR_ATENDIMENTO", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
                OracleParameter.Add("P_OUT_CUR_NATUREZAS", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

                await DbConn.ExecuteAsync("PACK_PROTOCOLO_ATENDIMENTO.PR_FINALIZAR", param: OracleParameter, commandType: CommandType.StoredProcedure);

                var outputAtendimento = OracleParameter.Get<OracleRefCursor>("P_OUT_CUR_ATENDIMENTO");
                var outputNaturezas = OracleParameter.Get<OracleRefCursor>("P_OUT_CUR_NATUREZAS");

                var dr = outputAtendimento.GetDataReader();

                var atendimentoCallCenter = new Atendimento();

                while (dr.Read())
                {
                    atendimentoCallCenter.nomeCliente = dr.GetString("nomeCliente");
                    atendimentoCallCenter.documento = dr.GetString("documento");

                    var drNaturezas = outputNaturezas.GetDataReader();

                    while (drNaturezas.Read())
                    {
                        var naturezaCallCenter = new Natureza();

                        naturezaCallCenter.codNatureza = drNaturezas.GetInt32("codNatureza");
                        naturezaCallCenter.descNatureza = drNaturezas.GetString("descNatureza");
                        naturezaCallCenter.datahora = drNaturezas.GetDateTime("datahora");

                        atendimentoCallCenter.naturezas.Add(naturezaCallCenter);
                    }
                }

                return atendimentoCallCenter;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar atendimento call center - {typeof(Atendimento)} [protocolo:{protocolo}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }


        #region [Métodos privados]

        private async Task<ProtocoloAtendimento> ObterAsync<T>(ProtocoloAtendimentoDbTypes dbType, T parameter)
        {
            try
            {
                var sql = new StringBuilder(@"SELECT PA.ID_PROTOCOLO_ATENDIMENTO,
                                                     PA.MATRICULA,
                                                     PA.ID_CONTRATO,
                                                     PA.DATA_INICIO,
                                                     PA.DATA_FIM,
                                                     PA.ID_USUARIO,
                                                     PA.ID_TIPO_ATENDIMENTO,
                                                     PA.CPF_CNPJ,
                                                     PA.OBS,
                                                     PA.NOME_SOLICITANTE,
                                                     PA.TELEFONE,
                                                     PA.ID_LOCAL_ATENDIMENTO,
                                                     PA.TIPO_PROTOCOLO,
                                                     PA.PREFIXO_PROTOCOLO,
                                                     PA.SEQUENCIA_PROTOCOLO,
                                                     PA.ORIGEM_INOVA,
                                                     PA.ID_PROTOCOLO_ATEND_GFA,
                                                     PA.NR_PROTOCOLO_PAI,
                                                     PA.DT_INTEGRACAO
                                                FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                                               WHERE 1 = 1");

                if (dbType == ProtocoloAtendimentoDbTypes.Id)
                {
                    sql.Append(" AND PA.ID_PROTOCOLO_ATENDIMENTO = :P_ID_PROTOCOLO_ATENDIMENTO");

                    OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", parameter, OracleDbType.Int32, ParameterDirection.Input);
                }

                if (dbType == ProtocoloAtendimentoDbTypes.NumeroProtocolo)
                {
                    sql.Append(" AND PA.NUM_PROTOCOLO = :P_NUM_PROTOCOLO");

                    OracleParameter.Add("P_NUM_PROTOCOLO", parameter, OracleDbType.Varchar2, ParameterDirection.Input);
                }

                return await DbConn.QueryFirstOrDefaultAsync<ProtocoloAtendimento>(sql.ToString(), OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #endregion

        #region [Métodos públicos para validação]

        public ValidationResult NumeroProtocoloValidar(string numeroProtocolo)
        {
            try
            {
                return new ProtocoloAtendimentoGeracaoNumeroValidation().Validate(numeroProtocolo);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar - {typeof(ProtocoloAtendimento)} [nº protocolo:{numeroProtocolo}].");

                throw;
            }
        }

        public ValidationResult PodeAlterar(ProtocoloAtendimento entity) => throw new NotImplementedException();

        public ValidationResult PodeExcluir(ProtocoloAtendimento entity) => throw new NotImplementedException();

        public ValidationResult PodeFinalizar(ProtocoloAtendimento entity)
        {
            try
            {
                return new ProtocoloAtencimentoPodeFinalizarValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar finalização - {typeof(ProtocoloAtendimento)}.");

                throw;
            }
        }

        public ValidationResult PodeIncluir(ProtocoloAtendimento entity)
        {
            try
            {
                return new ProtocoloAtencimentoPodeIncluirValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar inclusão - {typeof(ProtocoloAtendimento)}.");

                throw;
            }
        }

        #endregion

    }
}
