﻿using Dapper;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ParametroSistemaRepository
        : DapperRepository, IParametroSistemaRepository
    {

        readonly ILogger _logger;

        public ParametroSistemaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ParametroSistemaRepository>();
        }

        public async Task<ParametroSistema> ObterAsync(long id)
        {
            try
            {
                var sql = @"SELECT P.ID_PARM,
                                   P.DESCR_PARAMETRO,
                                   P.VL_PARAMETRO,
                                   P.UNID_MEDIDA,
                                   P.DATA_INCLUSAO_PARM,
                                   P.TIPO_SIT,
                                   U.ID_UNID_MEDIDA,
                                   U.DESCR_UNID_MEDIDA,
                                   U.SIGLA_UNID
                              FROM TAB_PARAMETRO P
                              JOIN TAB_UNID_MEDIDA U
                                ON P.UNID_MEDIDA = U.ID_UNID_MEDIDA
                             WHERE P.ID_PARM = :P_ID_PARM";

                OracleParameter.Add("P_ID_PARM", id, OracleDbType.Long, ParameterDirection.Input);

                var parametrosSistema = await DbConn.QueryAsync<ParametroSistema, UnidadeMedida, ParametroSistema>(sql,
                    map: (parametroSistema, unidadeMedida) =>
                    {
                        parametroSistema.UnidadeMedida = unidadeMedida;

                        return parametroSistema;
                    },
                    param: OracleParameter,
                    splitOn: "ID_PARM,ID_UNID_MEDIDA"
                );

                return parametrosSistema.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ParametroSistema)} [id:{id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
