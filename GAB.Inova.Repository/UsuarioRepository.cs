﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class UsuarioRepository
        : DapperRepository, IUsuarioRepository
    {

        readonly ILogger _logger;

        public UsuarioRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<UsuarioRepository>();
        }

        public async Task<Usuario> ObterPorIdAsync(long idUsuario)
        {
            try
            {
                var sql = GetSelectSQL("WHERE USU.ID_USUARIO = :P_ID_USUARIO");

                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Usuario>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Usuario)} [id:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<Usuario> ObterPorNomeAcessoAsync(string nomeAcesso, bool? ativo = null)
        {
            try
            {
                var where = new StringBuilder("WHERE UPPER(USU.NOME_ACESSO) = UPPER(:P_NOME_ACESSO) ");

                if (ativo.HasValue)
                {
                    where.Append("AND USU.SITUACAO = :P_SITUACAO ");

                    OracleParameter.Add("P_SITUACAO", ativo.Value ? 1 : 0, OracleDbType.Int16, ParameterDirection.Input);
                }

                var sql = GetSelectSQL(where.ToString());
                
                OracleParameter.Add("P_NOME_ACESSO", nomeAcesso, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Usuario>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Usuario)} [nome acesso:{nomeAcesso}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #region [Métodos privados]

        private string GetSelectSQL(string where)
        {
            var builder = new StringBuilder(@"SELECT USU.ID_USUARIO,
                                                     USU.NOME_ACESSO,
                                                     USU.NOME_COMPLETO,
                                                     USU.EMAIL,
                                                     USU.SITUACAO,
                                                     USU.SITUACAO_AD
                                                FROM NETUNO.USUARIO USU ");

            if (!string.IsNullOrEmpty(where))
            {
                builder.AppendLine(where);
            }

            return builder.ToString();
        }

        #endregion

    }
}
