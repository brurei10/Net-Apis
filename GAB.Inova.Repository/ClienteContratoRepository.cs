﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ClienteContratoRepository
        : DapperRepository, IClienteContratoRepository
    {

        readonly ILogger _logger;

        public ClienteContratoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ClienteContratoRepository>();
        }

        public async Task<ClienteContrato> ObterAsync(string documento, string matricula)
        {
            try
            {
                var sql = @"SELECT ROWNUM AS Id,
                                   E.ID_EMPRESA as EmpresaId,
                                   R.ID_REGIAO as CodigoInova,
                                   E.Sigla      as Sigla,
                                   E.Nome_Empresa as NomeEmpresa,
                                   C.MATRICULA as Matricula,
                                   CT.ID_CONTRATO as IdContrato,
                                   CT.CPF_CNPJ as CpfCnpj,
                                   CT.NOME_CLIENTE as NomeCliente,
                                   LT.LTP_ABREVIATURA || ' ' || DECODE(NB.ID_NOBREZA, NULL, '', NB.NOME_NOBREZA || ' ') || LG.NOME_LOGRADOURO AS Logradouro,
                                   C.NUM_IMOVEL as Numero,
                                   DECODE(TRIM(C.COMPLEMENTO), NULL, ' ', ' ' || C.COMPLEMENTO) AS Complemento,
                                   B.NOME_BAIRRO as Bairro,
                                   CD.NOME_CIDADE as Cidade,
                                   ET.UF as Uf,
                                   LG.LOGRADOURO_CEP AS Cep,
                                   C.ECO_RES as EcoRes, 
                                   C.ECO_COM as EcoCom, 
                                   C.ECO_IND as EcoInd,
                                   C.ECO_PUB as EcoPub, 
                                   C.ECO_RES + C.ECO_COM + C.ECO_IND + C.ECO_PUB AS QtdEco,
                                   F_VALIDA_CPF_CNPJ(CT.CPF_CNPJ) AS CpfCnpjValido,
                                   C.PONTO_REFERENCIA AS PontoReferencia,
                                   CT.Email as Email,
                                   CT.Telefone_RES as TelefoneFixo,
                                   CT.Telefone_COM as TelefoneComercial,
                                   CT.TELEFONE_CEL as TelefoneCelular,
                                   CT.TELEFONE_FAX as TelefoneCelular2,
                                   CASE WHEN LENGTH(CT.CPF_CNPJ) = 11 THEN 'F' 
                                        WHEN LENGTH(CT.CPF_CNPJ) = 14 THEN 'J' 
                                        ELSE NULL
                                   END as TipoPessoa,
                                   CT.DATA_NASCIMENTO AS  DataNascimento,
                                   CT.NUM_RG AS RG,
                                   CT.ORGAO_EXPEDIDOR AS OrgaoExpedidor,
                                   CT.DATA_EXPEDICAO AS DataExpedicao,
                                   CT.NOME_PAI AS NomePai,
                                   CT.NOME_MAE AS NomeMae
                              FROM NETUNO.CLIENTE C
                              JOIN NETUNO.CONTRATO CT
                                ON C.MATRICULA = CT.MATRICULA
                              JOIN NETUNO.EMPRESA E
                                ON CT.ID_EMPRESA = E.ID_EMPRESA
                              JOIN REGIAO R
                                ON R.ID_EMPRESA = E.ID_EMPRESA
                              JOIN NETUNO.LOGRADOURO LG
                                ON LG.ID_LOGRADOURO = C.ID_RUA
                              JOIN NETUNO.LOGRADOURO_TIPO LT
                                ON LT.LTP_ID = LG.LTP_ID
                              LEFT JOIN NETUNO.NOBREZA NB
                                ON NB.ID_NOBREZA = LG.ID_NOBREZA
                              JOIN NETUNO.BAIRRO B
                                ON B.ID_BAIRRO = LG.ID_BAIRRO
                              JOIN NETUNO.CIDADE CD
                                ON CD.ID_CIDADE = B.ID_CIDADE
                              JOIN NETUNO.ESTADO ET
                                ON ET.ID_ESTADO = CD.ID_ESTADO
                             WHERE (C.SIT_AGUA <> 9 OR C.SIT_ESGOTO <> 9)
                               AND  C.MATRICULA = :P_MATRICULA
                               AND  CT.CPF_CNPJ = :P_CPF_CNPJ ";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CPF_CNPJ", documento, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ClienteContrato>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ClienteContrato)} [matricula:{matricula}, documento:{documento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
