﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class MensagemIntegradorMovimentoRepository
        : DapperRepository, IMensagemIntegradorMovimentoRepository
    {

        readonly ILogger _logger;

        public MensagemIntegradorMovimentoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<MensagemIntegradorRepository>();
        }

        public async Task<bool> InserirAsync(MensagemIntegradorMovimento entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.MENSAGEM_INTEGRADOR_MOV
                              (ID_MENSAGEM_INTEGRADOR, DT_INCLUSAO, STATUS_TIPO, MENSAGEM)
                            VALUES
                              (:P_ID_MENSAGEM_INTEGRADOR, SYSDATE, :P_STATUS_TIPO, :P_MENSAGEM)
                            RETURNING ID_MENSAGEM_INTEGRADOR_MOV INTO :V_ID_MENSAGEM_INTEGRADOR_MOV";

                OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", entity.IdMensagemIntegrador, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_MENSAGEM", entity.Mensagem, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_STATUS_TIPO", (int)entity.Status, OracleDbType.Int32, ParameterDirection.Input);

                OracleParameter.Add("V_ID_MENSAGEM_INTEGRADOR_MOV", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_MENSAGEM_INTEGRADOR_MOV").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(MensagemIntegradorMovimento)} [id mensagem integrador:{entity?.Id}, mensagem:{entity?.Mensagem}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
