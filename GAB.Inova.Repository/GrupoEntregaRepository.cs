﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class GrupoEntregaRepository
        : DapperRepository, IGrupoEntregaRepository
    {

        readonly ILogger _logger;

        public GrupoEntregaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<GrupoEntregaRepository>();
        }

        public async Task<GrupoEntrega> ObterAsync(long id)
        {
            try
            {
                var sql = @"SELECT ID_GRUPO_ENTREGA, DESCR_GRUPO_ENTREGA, IMPRIME_LIS, SITUACAO
                              FROM NETUNO.GRUPO_ENTREGA
                             WHERE ID_GRUPO_ENTREGA = :P_ID_GRUPO_ENTREGA";

                OracleParameter.Add("P_ID_GRUPO_ENTREGA", id, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<GrupoEntrega>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(GrupoEntrega)} [id:{id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
