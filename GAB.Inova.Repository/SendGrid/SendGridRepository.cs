﻿using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GAB.Inova.Repository.SendGrid
{
    public class SendGridRepository
        : IEmailIntegradorRepository
    {
        protected const string CONFIG_SEND_GRID = "Sendgrid";
        protected const string CONDIG_SEND_GRID_KEY = "key";
        readonly ILogger _logger;
        readonly IConfiguration _configuration;
        
        public SendGridRepository(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<SendGridRepository>();
            _configuration = configuration;
        }
        
        public async Task<EmailIntegradorResponse> EnviarEmailAsync(EmailRequestViewModel model)
        {
            try
            {
                var client = new SendGridClient(_configuration.GetSection(CONFIG_SEND_GRID).GetValue<string>(CONDIG_SEND_GRID_KEY));

                var msg = new SendGridMessage
                {
                    From = new EmailAddress(model.RemetenteEnderecoEmail, model.RemetenteNome)
                };

                msg.AddCustomArg("codigoEmpresa", model.CodigoEmpresa);
                foreach (var arg in model.CustomArgs)
                {
                    msg.AddCustomArg(arg.Key, arg.Value);
                }

                foreach (var email in model.Destinatarios.Split(";"))
                {
                    msg.AddTo(email.Trim());
                }

                if (!string.IsNullOrWhiteSpace(model.DestinatariosCopia))
                {
                    foreach (var email in model.DestinatariosCopia.Split(";"))
                    {
                        msg.AddCc(email);
                    }
                }

                if (!string.IsNullOrWhiteSpace(model.DestinatariosCopiaOculta))
                {
                    foreach (var email in model.DestinatariosCopiaOculta.Split(";"))
                    {
                        msg.AddBcc(email);
                    }
                }

                if (!string.IsNullOrWhiteSpace(model.ReplayToNome) && !string.IsNullOrWhiteSpace(model.ReplayToEmail))
                {
                    msg.ReplyTo = new EmailAddress
                    {
                        Name = model.ReplayToNome,
                        Email = model.ReplayToEmail
                    };
                }

                if (!string.IsNullOrWhiteSpace(model.TemplateId))
                {
                    msg.SetTemplateId(model.TemplateId);
                    msg.SetTemplateData(model.Conteudo);
                }
                else
                {
                    //TODO: Implementar envio de mensagem sem template!
                    msg.AddContent("text/html", model.Conteudo.ToString());
                    msg.SetSubject(model.Tipo.GetDescription());
                }

                if (model.Anexos != null)
                {
                    foreach (var anexo in model.Anexos)
                    {
                        msg.AddAttachment(new Attachment
                        {
                            Content = anexo.ArquivoBase64,
                            Filename = anexo.NomeArquivo,
                            Type = anexo.Tipo
                        });
                    }
                }

                var response = await client.SendEmailAsync(msg);

                _logger.LogInformation($"Response SendGrid - Empresa: {model?.CodigoEmpresa} - Destinatarios: {model.Destinatarios} - Status: {response?.StatusCode} - Headers: {JsonConvert.SerializeObject(response?.Headers)} - Body: {JsonConvert.SerializeObject(response?.Body)}");

                switch (response.StatusCode)
                {
                    case HttpStatusCode.Accepted:
                        return new EmailIntegradorResponse(response?.Headers?.GetValues("X-message-Id").FirstOrDefault());
                    case HttpStatusCode.BadRequest:
                    case HttpStatusCode.Unauthorized:
                    case HttpStatusCode.RequestEntityTooLarge:
                    case HttpStatusCode.Forbidden:
                    default:
                        return new EmailIntegradorResponse(response.StatusCode, JsonConvert.SerializeObject(response?.Body));
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Ocorreu um erro ao tentar enviar email pelo SENDGRID [ex:{ex.Message}");
                return new EmailIntegradorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
