﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.OrdemServicoEletronicaSpec;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class OrdemServicoEletronicaRepository
        : DapperRepository, IOrdemServicoEletronicaRepository
    {

        readonly ILogger _logger;

        public OrdemServicoEletronicaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<OrdemServicoEletronicaRepository>();
        }

        public bool VerificarExistencia(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1) FROM NETUNO.OS_ELETRONICA OS WHERE OS.ID_UNICO = :pID_UNICO ";

                OracleParameter.Add("pID_UNICO", os.IdentificadorOS, OracleDbType.Varchar2, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia da OSEletronica de IdentificadorUnico: {os?.IdentificadorOS} ou OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarExistenciaAsync(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1) FROM NETUNO.OS_ELETRONICA OS WHERE OS.ID_UNICO = :pID_UNICO ";

                OracleParameter.Add("pID_UNICO", os.IdentificadorOS, OracleDbType.Varchar2, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia da OSEletronica de IdentificadorUnico: {os?.IdentificadorOS} ou OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public bool VerificarExistenciaOrdemServicoEletronicaAssociada(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1)
                              FROM NETUNO.OS_ELETRONICA OS
                             WHERE OS.ID_OS_ELETRONICA = :pID_OS_ELETRONICA
                               AND OS.SEQUENCIAL_VINCULADA = :pSEQUENCIAL_VINCULADA";

                OracleParameter.Add("pID_OS_ELETRONICA", os.OSEletronicaOrigemID.GetValueOrDefault(0), OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQUENCIAL_VINCULADA", os.SequencialVinculada.GetValueOrDefault(0), OracleDbType.Int32, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia de OSEletronica Associada - OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarExistenciaOrdemServicoEletronicaAssociadaAsync(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1)
                              FROM NETUNO.OS_ELETRONICA OS
                             WHERE OS.ID_OS_ELETRONICA = :pID_OS_ELETRONICA
                               AND OS.SEQUENCIAL_VINCULADA = :pSEQUENCIAL_VINCULADA";

                OracleParameter.Add("pID_OS_ELETRONICA", os.OSEletronicaOrigemID.GetValueOrDefault(0), OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQUENCIAL_VINCULADA", os.SequencialVinculada.GetValueOrDefault(0), OracleDbType.Int32, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia de OSEletronica Associada - OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public bool VerificarCodigoServico(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1) FROM NETUNO.SERVICO S WHERE S.COD_SERVICO = :pCODIGO_SERVICO ";

                OracleParameter.Add("pCODIGO_SERVICO", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar se o Código de Servico é Válido - OS Eletronica {os?.IdentificadorOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarCodigoServicoAsync(OrdemServicoEletronica os)
        {
            try
            {
                var sql = @"SELECT COUNT(1) FROM NETUNO.SERVICO S WHERE S.COD_SERVICO = :pCODIGO_SERVICO ";

                OracleParameter.Add("pCODIGO_SERVICO", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar se o Código de Servico é Válido - OS Eletronica {os?.IdentificadorOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public void Inserir(OrdemServicoEletronica os)
        {
            try
            {
                #region SQL

                var sql = @"INSERT INTO NETUNO.OS_ELETRONICA(NUM_OS,
                                 SEQUENCIAL_VINCULADA,
                                 COD_SERVICO, 
                                 ID_UNICO,
                                 ID_REQUEST,
                                 ID_EXTERNO,
                                 ID_OS_ELETRONICA_PAI,
                                 ID_MOTIVO_ABERTURA,
                                 ID_SIT_IMOVEL,
                                 EQUIPE_EXECUTORA,
                                 ID_TIPO_IRREGULARIDADE,
                                 ID_TIPO_FONTE_ALTERNATIVA,
                                 DT_FIM_EXECUCAO,
                                 DT_INICIO_EXECUCAO,
                                 CENTRO_OPERATIVO,
                                 LONGITUDE,
                                 LATITUDE, 
                                 NUMERO_NOTIFICACAO,
                                 LAUDO,
                                 OBSERVACAO,
                                 ID_CATEGORIA,
                                 ID_SUB_CATEGORIA,
                                 ID_TIPO_PAV_RUA,
                                 ID_TIPO_PAV_CALCADA,
                                 ID_FREQ_UTILIZ_IMOVEL,
                                 ID_DIAMETRO_REDE,
                                 ID_PADRAO_INST_HD,
                                 ID_PROFUNDIDADE_REDE,
                                 ID_PROFUNDIDADE_RAMAL,
                                 ID_DIAMETRO_RAMAL,
                                 ID_DIAMETRO_HD,
                                 MATERIAL_RAMAL,
                                 MATERIAL_REDE,
                                 ID_LOCALIZACAO_RAMAL,
                                 ID_LOCAL_INST_HD,
                                 EH_TROCA_TITULARIDADE,
                                 EH_AREA_RISCO,
                                 EH_CLIENTE_FLUTUANTE,
                                 TEM_BOIA_CX_DAGUA,
                                 TEM_BOIA_CISTERNA,
                                 TEM_VALVULA_RETENCAO,
                                 TEM_TELEMETRIA_ACOPLADA,
                                 TEM_RELOAJOARIA_EQUIP,
                                 TEM_FONTE_ALTERNATIVA,
                                 TEM_BOMBA,
                                 TEM_CAIXA_CORREIOS,
                                 VOLUME_CAIXA_DAGUA,
                                 VOLUME_CISTENA,
                                 VOLUME_PISCINA,
                                 VAZAO_COM_BOMBA,
                                 VAZAO_SEM_BOMBA,
                                 QTD_SALAS,
                                 QTD_LOJAS,
                                 QTD_FUNCIONARIOS,
                                 QTD_MORADORES,
                                 QTD_BANHEIROS_LAVABOS,
                                 QTD_QUARTOS,
                                 QTD_ECO_PUBLICO,
                                 QTD_ECO_INDUSTRIAL,
                                 QTD_ECO_RESIDENCIAL,
                                 QTD_ECO_COMERCIAL,
                                 ID_HIDROMETRO,
                                 ID_HIDROMETRO_INST,
                                 NUM_SELO_VIROLA,
                                 NUM_LACRE,
                                 LEITURA_RETIRADA,
                                 LEITURA_INSTALACAO,
                                 LEITURA_HD,
                                 NOME_RAZAO_SOCIAL,
                                 NOME_MAE,
                                 DATA_NASCIMENTO,
                                 TELEFONE,
                                 CELULAR,
                                 NUM_RG,
                                 CPF_CNPJ,                                 
                                 TIPO_LOGRADOURO,
                                 ID_BAIRRO,
                                 COD_MUNICIPIO,
                                 LOGRADOURO,
                                 NUMERO,
                                 COMPLEMETO,
                                 PONTO_REFER,
                                 ID_MOTIVO_BAIXA)
                          VALUES(:pNUM_OS,
                                 :pSEQUENCIAL_VINCULADA,
                                 :pCOD_SERVICO, 
                                 :pID_UNICO,
                                 :pID_REQUEST,
                                 :pID_EXTERNO,
                                 :pID_OS_ELETRONICA_PAI,
                                 :pID_MOTIVO_ABERTURA,
                                 :pID_SIT_IMOVEL,
                                 :pEQUIPE_EXECUTORA,
                                 :pID_TIPO_IRREGULARIDADE,
                                 :pID_TIPO_FONTE_ALTERNATIVA,
                                 :pDT_FIM_EXECUCAO,
                                 :pDT_INICIO_EXECUCAO,
                                 :pCENTRO_OPERATIVO,
                                 :pLONGITUDE,
                                 :pLATITUDE, 
                                 :pNUMERO_NOTIFICACAO,
                                 :pLAUDO,
                                 :pOBSERVACAO,
                                 :pID_CATEGORIA,
                                 :pID_SUB_CATEGORIA,
                                 :pID_TIPO_PAV_RUA,
                                 :pID_TIPO_PAV_CALCADA,
                                 :pID_FREQ_UTILIZ_IMOVEL,
                                 :pID_DIAMETRO_REDE,
                                 :pID_PADRAO_INST_HD,
                                 :pID_PROFUNDIDADE_REDE,
                                 :pID_PROFUNDIDADE_RAMAL,
                                 :pID_DIAMETRO_RAMAL,
                                 :pID_DIAMETRO_HD,
                                 :pMATERIAL_RAMAL,
                                 :pMATERIAL_REDE,
                                 :pID_LOCALIZACAO_RAMAL,
                                 :pID_LOCAL_INST_HD,
                                 :pEH_TROCA_TITULARIDADE,
                                 :pEH_AREA_RISCO,
                                 :pEH_CLIENTE_FLUTUANTE,
                                 :pTEM_BOIA_CX_DAGUA,
                                 :pTEM_BOIA_CISTERNA,
                                 :pTEM_VALVULA_RETENCAO,
                                 :pTEM_TELEMETRIA_ACOPLADA,
                                 :pTEM_RELOAJOARIA_EQUIP,
                                 :pTEM_FONTE_ALTERNATIVA,
                                 :pTEM_BOMBA,
                                 :pTEM_CAIXA_CORREIOS,
                                 :pVOLUME_CAIXA_DAGUA,
                                 :pVOLUME_CISTENA,
                                 :pVOLUME_PISCINA,
                                 :pVAZAO_COM_BOMBA,
                                 :pVAZAO_SEM_BOMBA,
                                 :pQTD_SALAS,
                                 :pQTD_LOJAS,
                                 :pQTD_FUNCIONARIOS,
                                 :pQTD_MORADORES,
                                 :pQTD_BANHEIROS_LAVABOS,
                                 :pQTD_QUARTOS,
                                 :pQTD_ECO_PUBLICO,
                                 :pQTD_ECO_INDUSTRIAL,
                                 :pQTD_ECO_RESIDENCIAL,
                                 :pQTD_ECO_COMERCIAL,
                                 :pID_HIDROMETRO,
                                 :pID_HIDROMETRO_INST,
                                 :pNUM_SELO_VIROLA,
                                 :pNUM_LACRE,
                                 :pLEITURA_RETIRADA,
                                 :pLEITURA_INSTALACAO,
                                 :pLEITURA_HD,
                                 :pNOME_RAZAO_SOCIAL,
                                 :pNOME_MAE,
                                 :pDATA_NASCIMENTO,
                                 :pTELEFONE,
                                 :pCELULAR,
                                 :pNUM_RG,
                                 :pCPF_CNPJ,                                 
                                 :pTIPO_LOGRADOURO,
                                 :pID_BAIRRO,
                                 :pCOD_MUNICIPIO,
                                 :pLOGRADOURO,
                                 :pNUMERO,
                                 :pCOMPLEMETO,
                                 :pPONTO_REFER,
                                 :pID_MOTIVO_BAIXA) 
                 RETURNING ID_OS_ELETRONICA INTO :pID_OS_ELETRONICA ";

                #endregion

                #region PARAMETERS

                OracleParameter.Clean();

                OracleParameter.Add("pNUM_OS", os.NumeroOS.HasValue ? (object)os.NumeroOS.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQUENCIAL_VINCULADA", os.SequencialVinculada.HasValue ? (object)os.SequencialVinculada.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pCOD_SERVICO", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_UNICO", !string.IsNullOrEmpty(os.IdentificadorOS) ? (object)os.IdentificadorOS : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_REQUEST", !string.IsNullOrEmpty(os.IdentificadorRequisicao) ? (object)os.IdentificadorRequisicao : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_EXTERNO", !string.IsNullOrEmpty(os.IdentificadorExterno) ? (object)os.IdentificadorExterno : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_OS_ELETRONICA_PAI", os.OSEletronicaOrigemID.HasValue ? (object)os.OSEletronicaOrigemID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_MOTIVO_ABERTURA", os.MotivoAberturaID.HasValue ? (object)os.MotivoAberturaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_SIT_IMOVEL", os.SituacaoImovelID.HasValue ? (object)os.SituacaoImovelID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pEQUIPE_EXECUTORA", !string.IsNullOrEmpty(os.EquipeExecutora) ? (object)os.EquipeExecutora : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_IRREGULARIDADE", os.TipoIrregularidadeID.HasValue ? (object)os.TipoIrregularidadeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_FONTE_ALTERNATIVA", os.TipoFonteAlternativaID.HasValue ? (object)os.TipoFonteAlternativaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pDT_FIM_EXECUCAO", os.DataHoraFimExecucao.HasValue ? (object)os.DataHoraFimExecucao.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pDT_INICIO_EXECUCAO", os.DataHoraInicioExecucao.HasValue ? (object)os.DataHoraInicioExecucao.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pCENTRO_OPERATIVO", !string.IsNullOrEmpty(os.CentroOperativo) ? (object)os.CentroOperativo : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLONGITUDE", !string.IsNullOrEmpty(os.Longitude) ? (object)os.Longitude : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLATITUDE", !string.IsNullOrEmpty(os.Latitude) ? (object)os.Latitude : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUMERO_NOTIFICACAO", os.NumeroNotificacao.HasValue ? (object)os.NumeroNotificacao.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pLAUDO", !string.IsNullOrEmpty(os.Laudo) ? (object)os.Laudo : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pOBSERVACAO", !string.IsNullOrEmpty(os.Observacao) ? (object)os.Observacao : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_CATEGORIA", os.CategoriaID.HasValue ? (object)os.CategoriaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_SUB_CATEGORIA", os.SubCategoriaID.HasValue ? (object)os.SubCategoriaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_PAV_RUA", os.TipoPavimentoRuaID.HasValue ? (object)os.TipoPavimentoRuaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_PAV_CALCADA", os.TipoPavimentoCalcadaID.HasValue ? (object)os.TipoPavimentoCalcadaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_FREQ_UTILIZ_IMOVEL", os.FrequenciaUtilizacaoImovelID.HasValue ? (object)os.FrequenciaUtilizacaoImovelID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_REDE", os.DiametroRedeID.HasValue ? (object)os.DiametroRedeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PADRAO_INST_HD", os.PadraoInstalacaoHidrometroID.HasValue ? (object)os.PadraoInstalacaoHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PROFUNDIDADE_REDE", os.ProfundidadeRedeID.HasValue ? (object)os.ProfundidadeRedeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PROFUNDIDADE_RAMAL", os.ProfundidadeRamalID.HasValue ? (object)os.ProfundidadeRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_RAMAL", os.DiametroRamalID.HasValue ? (object)os.DiametroRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_HD", os.DiametroHidrometroID.HasValue ? (object)os.DiametroHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pMATERIAL_RAMAL", !string.IsNullOrEmpty(os.MaterialRamal) ? (object)os.MaterialRamal : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pMATERIAL_REDE", !string.IsNullOrEmpty(os.MaterialRede) ? (object)os.MaterialRede : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_LOCALIZACAO_RAMAL", os.LocalizacaoRamalID.HasValue ? (object)os.LocalizacaoRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_LOCAL_INST_HD", os.LocalInstalacaoHidrometroID.HasValue ? (object)os.LocalInstalacaoHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pEH_TROCA_TITULARIDADE", os.EhTrocaTitularidadeID.HasValue ? (object)(os.EhTrocaTitularidadeID.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pEH_AREA_RISCO", os.EhAreaRisco.HasValue ? (object)(os.EhAreaRisco.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pEH_CLIENTE_FLUTUANTE", os.EhClienteFlutuante.HasValue ? (object)(os.EhClienteFlutuante.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOIA_CX_DAGUA", os.TemBoiaCaixaDagua.HasValue ? (object)(os.TemBoiaCaixaDagua.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOIA_CISTERNA", os.TemBoiaSisterna.HasValue ? (object)(os.TemBoiaSisterna.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_VALVULA_RETENCAO", os.TemValvulaRetencao.HasValue ? (object)(os.TemValvulaRetencao.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_TELEMETRIA_ACOPLADA", os.TemEquipamentoTelemetriaAcoplada.HasValue ? (object)(os.TemEquipamentoTelemetriaAcoplada.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_RELOAJOARIA_EQUIP", os.TemReloajoariaPreEquipada.HasValue ? (object)(os.TemReloajoariaPreEquipada.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_FONTE_ALTERNATIVA", os.TemFonteAlternativa.HasValue ? (object)(os.TemFonteAlternativa.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOMBA", os.PossuiBomba.HasValue ? (object)(os.PossuiBomba.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_CAIXA_CORREIOS", os.TemCaixaCorreios.HasValue ? (object)(os.TemCaixaCorreios.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_CAIXA_DAGUA", os.VolumeCaixaDagua.HasValue ? (object)os.VolumeCaixaDagua.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_CISTENA", os.VolumeCisterna.HasValue ? (object)os.VolumeCisterna.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_PISCINA", os.VolumePiscina.HasValue ? (object)os.VolumePiscina.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVAZAO_COM_BOMBA", os.VazaoComBomba.HasValue ? (object)os.VazaoComBomba.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVAZAO_SEM_BOMBA", os.VazaoSemBomba.HasValue ? (object)os.VazaoSemBomba.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pQTD_SALAS", os.QuantidadeSalas.HasValue ? (object)os.QuantidadeSalas.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_LOJAS", os.QuantidadeLojas.HasValue ? (object)os.QuantidadeLojas.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_FUNCIONARIOS", os.NumeroFuncionarios.HasValue ? (object)os.NumeroFuncionarios.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_MORADORES", os.NumeroMoradores.HasValue ? (object)os.NumeroMoradores.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_BANHEIROS_LAVABOS", os.QuantidadeBanheirosOuLavabos.HasValue ? (object)os.QuantidadeBanheirosOuLavabos.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_QUARTOS", os.QuantidadeQuartosImovel.HasValue ? (object)os.QuantidadeQuartosImovel.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_PUBLICO", os.NumeroEconomiasPublico.HasValue ? (object)os.NumeroEconomiasPublico.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_INDUSTRIAL", os.NumeroEconomiasIndustrial.HasValue ? (object)os.NumeroEconomiasIndustrial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_RESIDENCIAL", os.NumeroEconomiasResidencial.HasValue ? (object)os.NumeroEconomiasResidencial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_COMERCIAL", os.NumeroEconomiasComercial.HasValue ? (object)os.NumeroEconomiasComercial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pID_HIDROMETRO", !string.IsNullOrEmpty(os.HidrometroID) ? (object)os.HidrometroID : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_HIDROMETRO_INST", !string.IsNullOrEmpty(os.HidrometroInstaladoID) ? (object)os.HidrometroInstaladoID : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_SELO_VIROLA", !string.IsNullOrEmpty(os.NumeroSeloVirola) ? (object)os.NumeroSeloVirola : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_LACRE", !string.IsNullOrEmpty(os.NumeroLacreInmetro) ? (object)os.NumeroLacreInmetro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_RETIRADA", os.LeituraDeRetirada.HasValue ? (object)os.LeituraDeRetirada.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_INSTALACAO", os.LeituraDeInstalacao.HasValue ? (object)os.LeituraDeInstalacao.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_HD", os.Leitura.HasValue ? (object)os.Leitura.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pNOME_RAZAO_SOCIAL", !string.IsNullOrEmpty(os.NomeOuRazaoSocial) ? (object)os.NomeOuRazaoSocial : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNOME_MAE", !string.IsNullOrEmpty(os.NomeMae) ? (object)os.NomeMae : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pDATA_NASCIMENTO", os.DataNascimento.HasValue ? (object)os.DataNascimento.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pTELEFONE", !string.IsNullOrEmpty(os.Telefone) ? (object)os.Telefone : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pCELULAR", !string.IsNullOrEmpty(os.Celular) ? (object)os.Celular : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_RG", !string.IsNullOrEmpty(os.NumeroRG) ? (object)os.NumeroRG : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pCPF_CNPJ", !string.IsNullOrEmpty(os.CPFCNPJ) ? (object)os.CPFCNPJ : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pTIPO_LOGRADOURO", !string.IsNullOrEmpty(os.TipoLogradouro) ? (object)os.TipoLogradouro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_BAIRRO", os.BairroID.HasValue ? (object)os.BairroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pCOD_MUNICIPIO", !string.IsNullOrEmpty(os.CodigoMunicipio) ? (object)os.CodigoMunicipio : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLOGRADOURO", !string.IsNullOrEmpty(os.Logradouro) ? (object)os.Logradouro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUMERO", os.Numero.HasValue ? (object)os.Numero.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pCOMPLEMETO", !string.IsNullOrEmpty(os.Complemento) ? (object)os.Complemento : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pPONTO_REFER", !string.IsNullOrEmpty(os.PontoDeReferencia) ? (object)os.PontoDeReferencia : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_MOTIVO_BAIXA", os.MotivoBaixaID.HasValue ? (object)os.MotivoBaixaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);

                OracleParameter.Add("pID_OS_ELETRONICA", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                #endregion

                DbConn.Execute(sql, param: OracleParameter);

                os.Id = Convert.ToInt64(Convert.ToString(OracleParameter.Get<dynamic>("pID_OS_ELETRONICA")));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir a OSEletronica de IdentificadorUnico: {os?.IdentificadorOS} ou OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task InserirAsync(OrdemServicoEletronica os)
        {
            try
            {
                #region SQL

                var sql = @"INSERT INTO NETUNO.OS_ELETRONICA(NUM_OS,
                                 SEQUENCIAL_VINCULADA,
                                 COD_SERVICO, 
                                 ID_UNICO,
                                 ID_REQUEST,
                                 ID_EXTERNO,
                                 ID_OS_ELETRONICA_PAI,
                                 ID_MOTIVO_ABERTURA,
                                 ID_SIT_IMOVEL,
                                 EQUIPE_EXECUTORA,
                                 ID_TIPO_IRREGULARIDADE,
                                 ID_TIPO_FONTE_ALTERNATIVA,
                                 DT_FIM_EXECUCAO,
                                 DT_INICIO_EXECUCAO,
                                 CENTRO_OPERATIVO,
                                 LONGITUDE,
                                 LATITUDE, 
                                 NUMERO_NOTIFICACAO,
                                 LAUDO,
                                 OBSERVACAO,
                                 ID_CATEGORIA,
                                 ID_SUB_CATEGORIA,
                                 ID_TIPO_PAV_RUA,
                                 ID_TIPO_PAV_CALCADA,
                                 ID_FREQ_UTILIZ_IMOVEL,
                                 ID_DIAMETRO_REDE,
                                 ID_PADRAO_INST_HD,
                                 ID_PROFUNDIDADE_REDE,
                                 ID_PROFUNDIDADE_RAMAL,
                                 ID_DIAMETRO_RAMAL,
                                 ID_DIAMETRO_HD,
                                 MATERIAL_RAMAL,
                                 MATERIAL_REDE,
                                 ID_LOCALIZACAO_RAMAL,
                                 ID_LOCAL_INST_HD,
                                 EH_TROCA_TITULARIDADE,
                                 EH_AREA_RISCO,
                                 EH_CLIENTE_FLUTUANTE,
                                 TEM_BOIA_CX_DAGUA,
                                 TEM_BOIA_CISTERNA,
                                 TEM_VALVULA_RETENCAO,
                                 TEM_TELEMETRIA_ACOPLADA,
                                 TEM_RELOAJOARIA_EQUIP,
                                 TEM_FONTE_ALTERNATIVA,
                                 TEM_BOMBA,
                                 TEM_CAIXA_CORREIOS,
                                 VOLUME_CAIXA_DAGUA,
                                 VOLUME_CISTENA,
                                 VOLUME_PISCINA,
                                 VAZAO_COM_BOMBA,
                                 VAZAO_SEM_BOMBA,
                                 QTD_SALAS,
                                 QTD_LOJAS,
                                 QTD_FUNCIONARIOS,
                                 QTD_MORADORES,
                                 QTD_BANHEIROS_LAVABOS,
                                 QTD_QUARTOS,
                                 QTD_ECO_PUBLICO,
                                 QTD_ECO_INDUSTRIAL,
                                 QTD_ECO_RESIDENCIAL,
                                 QTD_ECO_COMERCIAL,
                                 ID_HIDROMETRO,
                                 ID_HIDROMETRO_INST,
                                 NUM_SELO_VIROLA,
                                 NUM_LACRE,
                                 LEITURA_RETIRADA,
                                 LEITURA_INSTALACAO,
                                 LEITURA_HD,
                                 NOME_RAZAO_SOCIAL,
                                 NOME_MAE,
                                 DATA_NASCIMENTO,
                                 TELEFONE,
                                 CELULAR,
                                 NUM_RG,
                                 CPF_CNPJ,                                 
                                 TIPO_LOGRADOURO,
                                 ID_BAIRRO,
                                 COD_MUNICIPIO,
                                 LOGRADOURO,
                                 NUMERO,
                                 COMPLEMETO,
                                 PONTO_REFER,
                                 ID_MOTIVO_BAIXA)
                          VALUES(:pNUM_OS,
                                 :pSEQUENCIAL_VINCULADA,
                                 :pCOD_SERVICO, 
                                 :pID_UNICO,
                                 :pID_REQUEST,
                                 :pID_EXTERNO,
                                 :pID_OS_ELETRONICA_PAI,
                                 :pID_MOTIVO_ABERTURA,
                                 :pID_SIT_IMOVEL,
                                 :pEQUIPE_EXECUTORA,
                                 :pID_TIPO_IRREGULARIDADE,
                                 :pID_TIPO_FONTE_ALTERNATIVA,
                                 :pDT_FIM_EXECUCAO,
                                 :pDT_INICIO_EXECUCAO,
                                 :pCENTRO_OPERATIVO,
                                 :pLONGITUDE,
                                 :pLATITUDE, 
                                 :pNUMERO_NOTIFICACAO,
                                 :pLAUDO,
                                 :pOBSERVACAO,
                                 :pID_CATEGORIA,
                                 :pID_SUB_CATEGORIA,
                                 :pID_TIPO_PAV_RUA,
                                 :pID_TIPO_PAV_CALCADA,
                                 :pID_FREQ_UTILIZ_IMOVEL,
                                 :pID_DIAMETRO_REDE,
                                 :pID_PADRAO_INST_HD,
                                 :pID_PROFUNDIDADE_REDE,
                                 :pID_PROFUNDIDADE_RAMAL,
                                 :pID_DIAMETRO_RAMAL,
                                 :pID_DIAMETRO_HD,
                                 :pMATERIAL_RAMAL,
                                 :pMATERIAL_REDE,
                                 :pID_LOCALIZACAO_RAMAL,
                                 :pID_LOCAL_INST_HD,
                                 :pEH_TROCA_TITULARIDADE,
                                 :pEH_AREA_RISCO,
                                 :pEH_CLIENTE_FLUTUANTE,
                                 :pTEM_BOIA_CX_DAGUA,
                                 :pTEM_BOIA_CISTERNA,
                                 :pTEM_VALVULA_RETENCAO,
                                 :pTEM_TELEMETRIA_ACOPLADA,
                                 :pTEM_RELOAJOARIA_EQUIP,
                                 :pTEM_FONTE_ALTERNATIVA,
                                 :pTEM_BOMBA,
                                 :pTEM_CAIXA_CORREIOS,
                                 :pVOLUME_CAIXA_DAGUA,
                                 :pVOLUME_CISTENA,
                                 :pVOLUME_PISCINA,
                                 :pVAZAO_COM_BOMBA,
                                 :pVAZAO_SEM_BOMBA,
                                 :pQTD_SALAS,
                                 :pQTD_LOJAS,
                                 :pQTD_FUNCIONARIOS,
                                 :pQTD_MORADORES,
                                 :pQTD_BANHEIROS_LAVABOS,
                                 :pQTD_QUARTOS,
                                 :pQTD_ECO_PUBLICO,
                                 :pQTD_ECO_INDUSTRIAL,
                                 :pQTD_ECO_RESIDENCIAL,
                                 :pQTD_ECO_COMERCIAL,
                                 :pID_HIDROMETRO,
                                 :pID_HIDROMETRO_INST,
                                 :pNUM_SELO_VIROLA,
                                 :pNUM_LACRE,
                                 :pLEITURA_RETIRADA,
                                 :pLEITURA_INSTALACAO,
                                 :pLEITURA_HD,
                                 :pNOME_RAZAO_SOCIAL,
                                 :pNOME_MAE,
                                 :pDATA_NASCIMENTO,
                                 :pTELEFONE,
                                 :pCELULAR,
                                 :pNUM_RG,
                                 :pCPF_CNPJ,                                 
                                 :pTIPO_LOGRADOURO,
                                 :pID_BAIRRO,
                                 :pCOD_MUNICIPIO,
                                 :pLOGRADOURO,
                                 :pNUMERO,
                                 :pCOMPLEMETO,
                                 :pPONTO_REFER,
                                 :pID_MOTIVO_BAIXA) 
                 RETURNING ID_OS_ELETRONICA INTO :pID_OS_ELETRONICA ";

                #endregion

                #region PARAMETERS

                OracleParameter.Clean();

                OracleParameter.Add("pNUM_OS", os.NumeroOS.HasValue ? (object)os.NumeroOS.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pSEQUENCIAL_VINCULADA", os.SequencialVinculada.HasValue ? (object)os.SequencialVinculada.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pCOD_SERVICO", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_UNICO", !string.IsNullOrEmpty(os.IdentificadorOS) ? (object)os.IdentificadorOS : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_REQUEST", !string.IsNullOrEmpty(os.IdentificadorRequisicao) ? (object)os.IdentificadorRequisicao : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_EXTERNO", !string.IsNullOrEmpty(os.IdentificadorExterno) ? (object)os.IdentificadorExterno : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_OS_ELETRONICA_PAI", os.OSEletronicaOrigemID.HasValue ? (object)os.OSEletronicaOrigemID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_MOTIVO_ABERTURA", os.MotivoAberturaID.HasValue ? (object)os.MotivoAberturaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_SIT_IMOVEL", os.SituacaoImovelID.HasValue ? (object)os.SituacaoImovelID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pEQUIPE_EXECUTORA", !string.IsNullOrEmpty(os.EquipeExecutora) ? (object)os.EquipeExecutora : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_IRREGULARIDADE", os.TipoIrregularidadeID.HasValue ? (object)os.TipoIrregularidadeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_FONTE_ALTERNATIVA", os.TipoFonteAlternativaID.HasValue ? (object)os.TipoFonteAlternativaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pDT_FIM_EXECUCAO", os.DataHoraFimExecucao.HasValue ? (object)os.DataHoraFimExecucao.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pDT_INICIO_EXECUCAO", os.DataHoraInicioExecucao.HasValue ? (object)os.DataHoraInicioExecucao.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pCENTRO_OPERATIVO", !string.IsNullOrEmpty(os.CentroOperativo) ? (object)os.CentroOperativo : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLONGITUDE", !string.IsNullOrEmpty(os.Longitude) ? (object)os.Longitude : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLATITUDE", !string.IsNullOrEmpty(os.Latitude) ? (object)os.Latitude : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUMERO_NOTIFICACAO", os.NumeroNotificacao.HasValue ? (object)os.NumeroNotificacao.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pLAUDO", !string.IsNullOrEmpty(os.Laudo) ? (object)os.Laudo : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pOBSERVACAO", !string.IsNullOrEmpty(os.Observacao) ? (object)os.Observacao : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_CATEGORIA", os.CategoriaID.HasValue ? (object)os.CategoriaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_SUB_CATEGORIA", os.SubCategoriaID.HasValue ? (object)os.SubCategoriaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_PAV_RUA", os.TipoPavimentoRuaID.HasValue ? (object)os.TipoPavimentoRuaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_TIPO_PAV_CALCADA", os.TipoPavimentoCalcadaID.HasValue ? (object)os.TipoPavimentoCalcadaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_FREQ_UTILIZ_IMOVEL", os.FrequenciaUtilizacaoImovelID.HasValue ? (object)os.FrequenciaUtilizacaoImovelID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_REDE", os.DiametroRedeID.HasValue ? (object)os.DiametroRedeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PADRAO_INST_HD", os.PadraoInstalacaoHidrometroID.HasValue ? (object)os.PadraoInstalacaoHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PROFUNDIDADE_REDE", os.ProfundidadeRedeID.HasValue ? (object)os.ProfundidadeRedeID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_PROFUNDIDADE_RAMAL", os.ProfundidadeRamalID.HasValue ? (object)os.ProfundidadeRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_RAMAL", os.DiametroRamalID.HasValue ? (object)os.DiametroRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_DIAMETRO_HD", os.DiametroHidrometroID.HasValue ? (object)os.DiametroHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pMATERIAL_RAMAL", !string.IsNullOrEmpty(os.MaterialRamal) ? (object)os.MaterialRamal : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pMATERIAL_REDE", !string.IsNullOrEmpty(os.MaterialRede) ? (object)os.MaterialRede : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_LOCALIZACAO_RAMAL", os.LocalizacaoRamalID.HasValue ? (object)os.LocalizacaoRamalID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pID_LOCAL_INST_HD", os.LocalInstalacaoHidrometroID.HasValue ? (object)os.LocalInstalacaoHidrometroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pEH_TROCA_TITULARIDADE", os.EhTrocaTitularidadeID.HasValue ? (object)(os.EhTrocaTitularidadeID.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pEH_AREA_RISCO", os.EhAreaRisco.HasValue ? (object)(os.EhAreaRisco.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pEH_CLIENTE_FLUTUANTE", os.EhClienteFlutuante.HasValue ? (object)(os.EhClienteFlutuante.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOIA_CX_DAGUA", os.TemBoiaCaixaDagua.HasValue ? (object)(os.TemBoiaCaixaDagua.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOIA_CISTERNA", os.TemBoiaSisterna.HasValue ? (object)(os.TemBoiaSisterna.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_VALVULA_RETENCAO", os.TemValvulaRetencao.HasValue ? (object)(os.TemValvulaRetencao.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_TELEMETRIA_ACOPLADA", os.TemEquipamentoTelemetriaAcoplada.HasValue ? (object)(os.TemEquipamentoTelemetriaAcoplada.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_RELOAJOARIA_EQUIP", os.TemReloajoariaPreEquipada.HasValue ? (object)(os.TemReloajoariaPreEquipada.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_FONTE_ALTERNATIVA", os.TemFonteAlternativa.HasValue ? (object)(os.TemFonteAlternativa.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_BOMBA", os.PossuiBomba.HasValue ? (object)(os.PossuiBomba.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pTEM_CAIXA_CORREIOS", os.TemCaixaCorreios.HasValue ? (object)(os.TemCaixaCorreios.Value ? 1 : 0) : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_CAIXA_DAGUA", os.VolumeCaixaDagua.HasValue ? (object)os.VolumeCaixaDagua.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_CISTENA", os.VolumeCisterna.HasValue ? (object)os.VolumeCisterna.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVOLUME_PISCINA", os.VolumePiscina.HasValue ? (object)os.VolumePiscina.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVAZAO_COM_BOMBA", os.VazaoComBomba.HasValue ? (object)os.VazaoComBomba.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVAZAO_SEM_BOMBA", os.VazaoSemBomba.HasValue ? (object)os.VazaoSemBomba.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pQTD_SALAS", os.QuantidadeSalas.HasValue ? (object)os.QuantidadeSalas.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_LOJAS", os.QuantidadeLojas.HasValue ? (object)os.QuantidadeLojas.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_FUNCIONARIOS", os.NumeroFuncionarios.HasValue ? (object)os.NumeroFuncionarios.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_MORADORES", os.NumeroMoradores.HasValue ? (object)os.NumeroMoradores.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_BANHEIROS_LAVABOS", os.QuantidadeBanheirosOuLavabos.HasValue ? (object)os.QuantidadeBanheirosOuLavabos.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_QUARTOS", os.QuantidadeQuartosImovel.HasValue ? (object)os.QuantidadeQuartosImovel.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_PUBLICO", os.NumeroEconomiasPublico.HasValue ? (object)os.NumeroEconomiasPublico.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_INDUSTRIAL", os.NumeroEconomiasIndustrial.HasValue ? (object)os.NumeroEconomiasIndustrial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_RESIDENCIAL", os.NumeroEconomiasResidencial.HasValue ? (object)os.NumeroEconomiasResidencial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pQTD_ECO_COMERCIAL", os.NumeroEconomiasComercial.HasValue ? (object)os.NumeroEconomiasComercial.Value : (object)DBNull.Value, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("pID_HIDROMETRO", !string.IsNullOrEmpty(os.HidrometroID) ? (object)os.HidrometroID : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_HIDROMETRO_INST", !string.IsNullOrEmpty(os.HidrometroInstaladoID) ? (object)os.HidrometroInstaladoID : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_SELO_VIROLA", !string.IsNullOrEmpty(os.NumeroSeloVirola) ? (object)os.NumeroSeloVirola : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_LACRE", !string.IsNullOrEmpty(os.NumeroLacreInmetro) ? (object)os.NumeroLacreInmetro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_RETIRADA", os.LeituraDeRetirada.HasValue ? (object)os.LeituraDeRetirada.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_INSTALACAO", os.LeituraDeInstalacao.HasValue ? (object)os.LeituraDeInstalacao.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pLEITURA_HD", os.Leitura.HasValue ? (object)os.Leitura.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pNOME_RAZAO_SOCIAL", !string.IsNullOrEmpty(os.NomeOuRazaoSocial) ? (object)os.NomeOuRazaoSocial : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNOME_MAE", !string.IsNullOrEmpty(os.NomeMae) ? (object)os.NomeMae : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pDATA_NASCIMENTO", os.DataNascimento.HasValue ? (object)os.DataNascimento.Value : (object)DBNull.Value, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("pTELEFONE", !string.IsNullOrEmpty(os.Telefone) ? (object)os.Telefone : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pCELULAR", !string.IsNullOrEmpty(os.Celular) ? (object)os.Celular : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUM_RG", !string.IsNullOrEmpty(os.NumeroRG) ? (object)os.NumeroRG : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pCPF_CNPJ", !string.IsNullOrEmpty(os.CPFCNPJ) ? (object)os.CPFCNPJ : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pTIPO_LOGRADOURO", !string.IsNullOrEmpty(os.TipoLogradouro) ? (object)os.TipoLogradouro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_BAIRRO", os.BairroID.HasValue ? (object)os.BairroID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pCOD_MUNICIPIO", !string.IsNullOrEmpty(os.CodigoMunicipio) ? (object)os.CodigoMunicipio : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pLOGRADOURO", !string.IsNullOrEmpty(os.Logradouro) ? (object)os.Logradouro : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pNUMERO", os.Numero.HasValue ? (object)os.Numero.Value : (object)DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("pCOMPLEMETO", !string.IsNullOrEmpty(os.Complemento) ? (object)os.Complemento : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pPONTO_REFER", !string.IsNullOrEmpty(os.PontoDeReferencia) ? (object)os.PontoDeReferencia : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pID_MOTIVO_BAIXA", os.MotivoBaixaID.HasValue ? (object)os.MotivoBaixaID.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);

                OracleParameter.Add("pID_OS_ELETRONICA", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                #endregion

                await DbConn.ExecuteAsync(sql, param: OracleParameter);

                os.Id = Convert.ToInt64(Convert.ToString(OracleParameter.Get<dynamic>("pID_OS_ELETRONICA")));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir a OSEletronica de IdentificadorUnico: {os?.IdentificadorOS} ou OS Eletronica de Origem {os?.OSEletronicaOrigemID} e Sequencia: {os?.SequencialVinculada}");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public void GerarOSComOSEletronicaAssociada(OrdemServicoEletronica os)
        {
            if (os.EhAssociada)
            {
                try
                {
                    var sql = @"P_GERA_OS_ELETRONICA_VINCULADA";

                    OracleParameter.Add("vIdOsEletronica", os.Id, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIdOsPai", os.OSEletronicaOrigemID, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vCodServico", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vidusuario", 0, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIs_iniciativa_empresa", 1, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIsCobraServ", 1, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vObservacaoOs", os.Observacao, OracleDbType.Varchar2, ParameterDirection.Input);
                    OracleParameter.Add("vOSVinculada", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                    DbConn.Execute(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    _logger.LogCritical(ex, $"Erro ao executar a procedure P_GERA_OS_ELETRONICA_VINCULADA - OS Eletronica '{0}' - {1}", os?.IdentificadorOS, ex.Message);
                    _logger.LogCritical($"objeto com erro: {JsonConvert.SerializeObject(os)}");
                    throw;
                }
                finally
                {
                    OracleParameter.Clean();
                }
            }
        }

        public async Task GerarOSComOSEletronicaAssociadaAsync(OrdemServicoEletronica os)
        {
            if (os.EhAssociada)
            {
                try
                {
                    var sql = @"P_GERA_OS_ELETRONICA_VINCULADA";

                    OracleParameter.Add("vIdOsEletronica", os.Id, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIdOsPai", os.OSEletronicaOrigemID, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vCodServico", os.CodigoServico, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vidusuario", 0, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIs_iniciativa_empresa", 1, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vIsCobraServ", 1, OracleDbType.Int64, ParameterDirection.Input);
                    OracleParameter.Add("vObservacaoOs", os.Observacao, OracleDbType.Varchar2, ParameterDirection.Input);
                    OracleParameter.Add("vOSVinculada", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                    await DbConn.ExecuteAsync(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    _logger.LogCritical(ex, $"Erro ao executar a procedure P_GERA_OS_ELETRONICA_VINCULADA - OS Eletronica '{0}' - {1}", os?.IdentificadorOS, ex.Message);
                    throw;
                }
                finally
                {
                    OracleParameter.Clean();
                }
            }
        }

        public OrdemServicoEletronica ObterPorId(long P_ID_OS_ELETRONICA)
        {
            try
            {
                var sql = @"SELECT OS.*
                              FROM NETUNO.OS_ELETRONICA OS
                             WHERE OS.ID_OS_ELETRONICA = :P_ID_OS_ELETRONICA ";


                OracleParameter.Add("P_ID_OS_ELETRONICA", P_ID_OS_ELETRONICA, OracleDbType.Int64, ParameterDirection.Input);

                return DbConn.QueryFirstOrDefault<OrdemServicoEletronica>(sql, OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<OrdemServicoEletronica> ObterPorIdAsync(long P_ID_OS_ELETRONICA)
        {
            try
            {
                var sql = @"SELECT OS.*
                              FROM NETUNO.OS_ELETRONICA OS
                             WHERE OS.ID_OS_ELETRONICA = :P_ID_OS_ELETRONICA ";


                OracleParameter.Add("P_ID_OS_ELETRONICA", P_ID_OS_ELETRONICA, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<OrdemServicoEletronica>(sql, OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public bool VerificarEquipeExecutora(OrdemServicoEletronica os)
        {
            try
            {
                if (string.IsNullOrEmpty(os?.EquipeExecutora)) return true; //Não é campo obrigatorio

                var sql = @"SELECT COUNT(1) FROM NETUNO.EXECUTOR WHERE SITUACAO = 1 AND UPPER(NOME_EXECUTOR) = :P_EQUIPE_EXECUTORA ";

                OracleParameter.Add("P_EQUIPE_EXECUTORA", os.EquipeExecutora.ToUpper(), OracleDbType.Varchar2, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia da Equipe Executora de Nome: '{0}' - {1}", os?.EquipeExecutora, ex.Message);
                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarEquipeExecutoraAsync(OrdemServicoEletronica os)
        {
            try
            {
                if (string.IsNullOrEmpty(os?.EquipeExecutora)) return true; //Não é campo obrigatorio

                var sql = @"SELECT COUNT(1) FROM NETUNO.EXECUTOR WHERE SITUACAO = 1 AND UPPER(NOME_EXECUTOR) = :P_EQUIPE_EXECUTORA ";

                OracleParameter.Add("P_EQUIPE_EXECUTORA", os.EquipeExecutora.ToUpper(), OracleDbType.Varchar2, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia da Equipe Executora de Nome: '{0}' - {1}", os?.EquipeExecutora, ex.Message);
                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public bool VerificarTipoLogradouro(OrdemServicoEletronica os)
        {
            try
            {
                if (string.IsNullOrEmpty(os?.TipoLogradouro)) return true; //Não é campo obrigatorio

                var sql = @"SELECT COUNT(1) FROM NETUNO.LOGRADOURO_TIPO WHERE LTP_ABREVIATURA = :P_TIPO_LOGRADOURO ";

                OracleParameter.Add("P_TIPO_LOGRADOURO", os.TipoLogradouro.ToUpper(), OracleDbType.Varchar2, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia do Tipo Logradouro de sigla: '{0}' - {1}", os?.TipoLogradouro, ex.Message);
                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarTipoLogradouroAsync(OrdemServicoEletronica os)
        {
            try
            {
                if (string.IsNullOrEmpty(os?.TipoLogradouro)) return true; //Não é campo obrigatorio

                var sql = @"SELECT COUNT(1) FROM NETUNO.LOGRADOURO_TIPO WHERE LTP_ABREVIATURA = :P_TIPO_LOGRADOURO ";

                OracleParameter.Add("P_TIPO_LOGRADOURO", os.TipoLogradouro.ToUpper(), OracleDbType.Varchar2, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia do Tipo Logradouro de sigla: '{0}' - {1}", os?.TipoLogradouro, ex.Message);
                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public ValidationResult PodeAlterar(OrdemServicoEletronica entity) => throw new NotImplementedException();

        public ValidationResult PodeExcluir(OrdemServicoEletronica entity) => throw new NotImplementedException();

        public ValidationResult PodeIncluir(OrdemServicoEletronica entity)
        {
            try
            {
                return new OrdemServicoEletronicaPodeSerInseridaValidation(this, entity).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar se pode ser inserida a OSEletronica de IdentificadorUnico: '{0}' ou OS Eletronica de Origem '{1}' e Sequencia: '{2}' - {3}",
                    entity?.IdentificadorOS, entity?.OSEletronicaOrigemID, entity?.SequencialVinculada, ex.Message);

                throw;
            }
        }
    }
}
