﻿using Dapper;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class FeriadoRepository
        : DapperRepository, IFeriadoRepository
    {

        readonly ILogger _logger;

        public FeriadoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<FeriadoRepository>();
        }

        public async Task<DateTime> ObterProximoDiaUtil(DateTime data, int qtdeDias, int indice)
        {
            try
            {
                var sql = @"SELECT NVL(COUNT(*), 0) FROM NETUNO.FERIADO F 
                             WHERE F.DATA = TO_DATE(:P_DATA, 'DD/MM/RRRR')";

                var ehDiaUtil = true;
                var ehFeriado = default(int);

                for (var i = 1; i <= qtdeDias; i++)
                {
                    ehDiaUtil = true;
                    ehFeriado = default(int);

                    while (ehDiaUtil)
                    {
                        data = data.AddDays(indice);

                        OracleParameter.Add("P_DATA", data.ToShortDateString(), OracleDbType.Varchar2, ParameterDirection.Input);

                        ehFeriado = await DbConn.ExecuteScalarAsync<int>(sql, OracleParameter);
                        
                        if (ehFeriado == 0 && data.DayOfWeek != DayOfWeek.Saturday && data.DayOfWeek != DayOfWeek.Sunday) ehDiaUtil = false;
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter o próximo dia útil [Data:{data}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
