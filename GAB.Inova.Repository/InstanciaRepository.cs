﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class InstanciaRepository
        : DapperRepository, IInstanciaRepository
    {

        readonly ILogger _logger;

        public InstanciaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<InstanciaRepository>();
        }

        public async Task<InstanciaCallCenter> GerarAutenticacaoAsync(string loginOperador, string protocolo, string chavePrivada)
        {
            try
            {
                OracleParameter.Add("P_PROTOCOLO", protocolo, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NOME_ACESSO", loginOperador, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TOKEN", chavePrivada, OracleDbType.Varchar2, ParameterDirection.Input);

                OracleParameter.Add("P_OUT_CUR_DADOS", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

                await DbConn.ExecuteAsync("PACK_CALL_CENTER.GERAR_AUTENTICACAO", param: OracleParameter, commandType: CommandType.StoredProcedure);

                var output = OracleParameter.Get<OracleRefCursor>("P_OUT_CUR_DADOS");

                var dr = output.GetDataReader();

                var instanciaCallCenter = new InstanciaCallCenter();

                while (dr.Read())
                {
                    instanciaCallCenter.NomeCliente = dr.IsDBNull("NOMECLIENTE") ? string.Empty : dr.GetString("NOMECLIENTE");
                    instanciaCallCenter.Documento = dr.IsDBNull("DOCUMENTO") ? string.Empty : dr.GetString("DOCUMENTO");
                    instanciaCallCenter.Url = dr.IsDBNull("URL") ? string.Empty : dr.GetString("URL");
                    instanciaCallCenter.Permitido = dr.IsDBNull("PERMITIDO") ? false : dr.GetBoolean("PERMITIDO");
                    instanciaCallCenter.IdControleAutenticacao = dr.IsDBNull("IDCONTROLEAUTENTICACAO") ? (int?)null : dr.GetInt32("IDCONTROLEAUTENTICACAO");
                }

                return instanciaCallCenter;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao gerar autenticação  - {typeof(InformacaoConta)} [protocolo:{protocolo}, loginOperador:{loginOperador}, chavePrivada:{chavePrivada}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
