﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ImprimeGRPRepository: DapperRepository, IImprimeGRPRepository
    {

        readonly ILogger _logger;

        public ImprimeGRPRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ImprimeGRPRepository>();
        }

        public async Task<bool> InserirAsync(ImprimeGRP entity)
        {
            try
            {
                var sql = @"INSERT 
                              INTO IMPRIME_GRP (SEQ_ORIGINAL, 
                                                MATRICULA, 
                                                TITULO, 
                                                NOME_CLIENTE, 
                                                ENDERECO, 
                                                NOME_LOCAL, 
                                                CONS_ANT_01, 
                                                CONS_ANT_02, 
                                                CONS_ANT_03,
                                                CONS_ANT_04, 
                                                CONS_ANT_05, 
                                                CONS_ANT_06, 
                                                CONS_ANT_07, 
                                                CONS_ANT_08, 
                                                CONS_ANT_09, 
                                                CONS_ANT_10, 
                                                CONS_ANT_11, 
                                                CONS_ANT_12, 
                                                ECO_RES, 
                                                ECO_COM, 
                                                ECO_IND, 
                                                ECO_PUB, 
                                                ID_CICLO, 
                                                SETOR_ROT_SEQ, 
                                                ID_HD, 
                                                TIPO_ENTREGA, 
                                                ANO_MES_CT, 
                                                DATA_LEIT_ATUAL, 
                                                DATA_LEIT_ANT, 
                                                CONS_FAT,                                                 
                                                LEITURA_ATUAL, 
                                                LEITURA_ANT, 
                                                MEDIA, 
                                                VL_GRP, 
                                                DATA_VENC, 
                                                DATA_VAL, 
                                                OBSERVACAO, 
                                                COD_BARRA, 
                                                COMP_CT_01, 
                                                COMP_CT_02, 
                                                COMP_CT_03,                                                 
                                                COMP_CT_04, 
                                                COMP_CT_05, 
                                                COMP_CT_06, 
                                                COMP_CT_07, 
                                                COMP_CT_08,
                                                COMP_CT_09, 
                                                COMP_CT_10, 
                                                ESTRUT_TAR_01, 
                                                ESTRUT_TAR_02, 
                                                ESTRUT_TAR_03, 
                                                ESTRUT_TAR_04, 
                                                ESTRUT_TAR_05, 
                                                ESTRUT_TAR_06, 
                                                ESTRUT_TAR_07, 
                                                ESTRUT_TAR_08, 
                                                ESTRUT_TAR_09, 
                                                ESTRUT_TAR_10, 
                                                ESTRUT_TAR_11, 
                                                ESTRUT_TAR_12, 
                                                ESTRUT_TAR_13, 
                                                ESTRUT_TAR_14, 
                                                ESTRUT_TAR_15, 
                                                ESTRUT_TAR_16, 
                                                COMP_VL_01, 
                                                COMP_VL_02, 
                                                COMP_VL_03, 
                                                COMP_VL_04, 
                                                COMP_VL_05, 
                                                COMP_VL_06, 
                                                COMP_VL_07,
                                                COMP_VL_08, 
                                                COMP_VL_09, 
                                                COMP_VL_10, 
                                                ID_SOLICITACAO) 
                                        VALUES (:P_SEQORIGINAL, 
                                                :P_MATRICULA, 
                                                :P_TITULO, 
                                                :P_NOMECLI, 
                                                :P_ENDERECO, 
                                                :P_NOMELOCAL, 
                                                :P_CONSANT01, 
                                                :P_CONSANT02, 
                                                :P_CONSANT03, 
                                                :P_CONSANT04, 
                                                :P_CONSANT05, 
                                                :P_CONSANT06, 
                                                :P_CONSANT07, 
                                                :P_CONSANT08, 
                                                :P_CONSANT09, 
                                                :P_CONSANT10, 
                                                :P_CONSANT11, 
                                                :P_CONSANT12, 
                                                :P_ECORES, 
                                                :P_ECOCOM, 
                                                :P_ECOIND, 
                                                :P_ECOPUB, 
                                                :P_IDCICLO, 
                                                :P_SETORROTSEQ, 
                                                :P_IDMEDIDOR, 
                                                :P_TIPOENTREGA, 
                                                :P_ANOMESCT, 
                                                NULL, 
                                                NULL, 
                                                :P_CONSFAT, 
                                                :P_LEITURAATUAL, 
                                                :P_LEITURAANT, 
                                                :P_MEDIA, 
                                                :P_VLGRP, 
                                                :P_DATAVENC, 
                                                :P_DATAVAL, 
                                                :P_OBSERVACAO, 
                                                :P_CODBARRA, 
                                                :P_COMPCT01, 
                                                :P_COMPCT02, 
                                                :P_COMPCT03, 
                                                :P_COMPCT04, 
                                                :P_COMPCT05, 
                                                :P_COMPCT06, 
                                                :P_COMPCT07, 
                                                :P_COMPCT08, 
                                                :P_COMPCT09, 
                                                :P_COMPCT10, 
                                                :P_ESTRUTTAR01, 
                                                :P_ESTRUTTAR02, 
                                                :P_ESTRUTTAR03, 
                                                :P_ESTRUTTAR04, 
                                                :P_ESTRUTTAR05, 
                                                :P_ESTRUTTAR06, 
                                                :P_ESTRUTTAR07, 
                                                :P_ESTRUTTAR08, 
                                                :P_ESTRUTTAR09, 
                                                :P_ESTRUTTAR10, 
                                                :P_ESTRUTTAR11, 
                                                :P_ESTRUTTAR12, 
                                                :P_ESTRUTTAR13,
                                                :P_ESTRUTTAR14, 
                                                :P_ESTRUTTAR15, 
                                                :P_ESTRUTTAR16, 
                                                :P_COMPVL01, 
                                                :P_COMPVL02, 
                                                :P_COMPVL03, 
                                                :P_COMPVL04, 
                                                :P_COMPVL05, 
                                                :P_COMPVL06, 
                                                :P_COMPVL07, 
                                                :P_COMPVL08, 
                                                :P_COMPVL09, 
                                                :P_COMPVL10, 
                                                :P_ID_SOLICITACAO)";

                OracleParameter.Add("P_SEQORIGINAL", entity.Id, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TITULO", entity.Titulo, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT01", entity.ConsumoAnterior01, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT02", entity.ConsumoAnterior02, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT03", entity.ConsumoAnterior03, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT04", entity.ConsumoAnterior04, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT05", entity.ConsumoAnterior05, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT06", entity.ConsumoAnterior06, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT07", entity.ConsumoAnterior07, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT08", entity.ConsumoAnterior08, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT09", entity.ConsumoAnterior09, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT10", entity.ConsumoAnterior10, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT11", entity.ConsumoAnterior11, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSANT12", entity.ConsumoAnterior12, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ANOMESCT", entity.AnoMesCt, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONSFAT", entity.ConsumoFaturado, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_LEITURAATUAL", entity.LeituraAtual, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_LEITURAANT", entity.LeituraAnterior, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_MEDIA", entity.Media, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT01", entity.CompoeCt01, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT02", entity.CompoeCt02, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT03", entity.CompoeCt03, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT04", entity.CompoeCt04, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT05", entity.CompoeCt05, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT06", entity.CompoeCt06, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT07", entity.CompoeCt07, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT08", entity.CompoeCt08, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT09", entity.CompoeCt09, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPCT10", entity.CompoeCt10, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL01", entity.CompoeValor01, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL02", entity.CompoeValor02, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL03", entity.CompoeValor03, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL04", entity.CompoeValor04, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL05", entity.CompoeValor05, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL06", entity.CompoeValor06, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL07", entity.CompoeValor07, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL08", entity.CompoeValor08, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL09", entity.CompoeValor09, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_COMPVL10", entity.CompoeValor10, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR01", entity.EstruturaTarifaria01, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR02", entity.EstruturaTarifaria02, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR03", entity.EstruturaTarifaria03, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR04", entity.EstruturaTarifaria04, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR05", entity.EstruturaTarifaria05, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR06", entity.EstruturaTarifaria06, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR07", entity.EstruturaTarifaria07, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR08", entity.EstruturaTarifaria08, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR09", entity.EstruturaTarifaria09, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR10", entity.EstruturaTarifaria10, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR11", entity.EstruturaTarifaria11, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR12", entity.EstruturaTarifaria12, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR13", entity.EstruturaTarifaria13, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR14", entity.EstruturaTarifaria14, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR15", entity.EstruturaTarifaria15, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ESTRUTTAR16", entity.EstruturaTarifaria16, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CODBARRA", entity.CodigoBarra, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NOMECLI", entity.NomeCliente, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ENDERECO", entity.Endereco, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NOMELOCAL", entity.NomeLocal, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ECORES", entity.EconomiaResidencial, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ECOCOM", entity.EconomiaComercial, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ECOIND", entity.EconomiaIndustrial, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ECOPUB", entity.EconomiaPublica, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_IDCICLO", entity.IdCiclo, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_SETORROTSEQ", entity.SetorRotaSequencia, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_IDMEDIDOR", entity.IdHd, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TIPOENTREGA", entity.TipoEntrega, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_VLGRP", entity.ValorGRP, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVENC", entity.DataVencimento, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVAL", entity.DataValor, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_OBSERVACAO", entity.Observacao, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_SOLICITACAO", entity.IdSolicitacao, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ImprimeGRP)} [seq original:{entity.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<ImprimeGRP> ObterPorSeqOriginal(long seqOriginal)
        {
            var sql = @"SELECT *
                          FROM NETUNO.IMPRIME_GRP
                             WHERE SEQ_ORIGINAL = :P_SEQ_ORIGINAL";

            OracleParameter.Add("P_SEQ_ORIGINAL", seqOriginal, OracleDbType.Int64, ParameterDirection.Input);

            return await DbConn.QueryFirstOrDefaultAsync<ImprimeGRP>(sql, OracleParameter);
        }
    }
}
