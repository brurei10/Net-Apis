﻿using Dapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    /// <summary>
    /// Classe de acesso a dados referente ao Aviso de Débito usando ORM Dapper
    /// </summary>
    public class AvisoDebitoRepository
        : DapperRepository, IAvisoDebitoRepository
    {

        readonly ILogger _logger;

        public AvisoDebitoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<AvisoDebitoRepository>();
        }

        #region [Métodos públicos]

        public void ApropriarOs(long seqOriginal, DateTime dataEntrega, bool cancelamento, MotivoCancelamentoContaDigitalEnum? motivo = null)
        {
            try
            {
                var sql = @"PACK_AVISO_DEBITO.PR_APROPRIAR_OS";

                var idFila = ObterIdFila();

                var idRotina = 7600; //Apropriação Automática O. S. Entrega Aviso

                var idLocalEntrega = 11; //CORREIO ELETRÔNICO

                OracleParameter.Add("P_SEQ_ORIGINAL_VINCULO_AVISO", seqOriginal, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_FILA", idFila, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_ID_ROTINA", idRotina, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CHAVE_PARAMETRO", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_LEITURA", dataEntrega, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_ID_LOCAL_ENTREGA", idLocalEntrega, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_LEITURISTA", DBNull.Value, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_QTD_IMPRESSAO", DBNull.Value, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CD_TIPO_GERACAO_AVISO", 2, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CONTA_DIGITAL", "S", OracleDbType.Char, ParameterDirection.Input);
                OracleParameter.Add("P_CANCELA_AVISO_CONTA_DIGITAL", (cancelamento ? "S" : "N"), OracleDbType.Char, ParameterDirection.Input);

                if (motivo.HasValue)
                {
                    OracleParameter.Add("P_MOTIVO_CANCELAMENTO_AVISO", (Int32)motivo, OracleDbType.Int32, ParameterDirection.Input);
                }

                DbConn.Execute(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task ApropriarOsAsync(long seqOriginal, DateTime dataEntrega, bool cancelamento, MotivoCancelamentoContaDigitalEnum? motivo = null)
        {
            try
            {
                var sql = @"PACK_AVISO_DEBITO.PR_APROPRIAR_OS";

                var idFila = await ObterIdFilaAsync();

                var idRotina = 7600; //Apropriação Automática O. S. Entrega Aviso

                var idLocalEntrega = 11; //CORREIO ELETRÔNICO

                OracleParameter.Add("P_SEQ_ORIGINAL_VINCULO_AVISO", seqOriginal, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_FILA", idFila, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_ID_ROTINA", idRotina, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CHAVE_PARAMETRO", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_LEITURA", dataEntrega, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_ID_LOCAL_ENTREGA", idLocalEntrega, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_LEITURISTA", DBNull.Value, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_QTD_IMPRESSAO", DBNull.Value, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CD_TIPO_GERACAO_AVISO", 2, OracleDbType.Long, ParameterDirection.Input);
                OracleParameter.Add("P_CONTA_DIGITAL", "S", OracleDbType.Char, ParameterDirection.Input);
                OracleParameter.Add("P_CANCELA_AVISO_CONTA_DIGITAL", (cancelamento ? "S" : "N"), OracleDbType.Char, ParameterDirection.Input);

                if (motivo.HasValue)
                {
                    OracleParameter.Add("P_MOTIVO_CANCELAMENTO_AVISO", (Int32)motivo, OracleDbType.Int32, ParameterDirection.Input);
                }

                await DbConn.ExecuteAsync(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public bool ExisteAvisoAtivo(long seqOriginal)
        {
            try
            {
                var sql = @"SELECT COUNT(1)
                              FROM NETUNO.CONTA_COBRANCA CC
                             WHERE CC.AVISO_ATIVO = 1
                               AND CC.SEQ_ORIGINAL_VINCULO_AVISO = :P_SEQ_ORIGINAL_VINCULO_AVISO";

                OracleParameter.Add("P_SEQ_ORIGINAL_VINCULO_AVISO", seqOriginal, OracleDbType.Long, ParameterDirection.Input);

                var qtde = DbConn.QueryFirst<int>(sql, param: OracleParameter, commandType: CommandType.Text);

                return qtde > 0;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarSeExisteAvisoAtivoAsync(long seqOriginal)
        {
            try
            {
                var sql = @"SELECT COUNT(1)
                              FROM NETUNO.CONTA_COBRANCA CC
                             WHERE CC.AVISO_ATIVO = 1
                               AND CC.SEQ_ORIGINAL_VINCULO_AVISO = :P_SEQ_ORIGINAL_VINCULO_AVISO";

                OracleParameter.Add("P_SEQ_ORIGINAL_VINCULO_AVISO", seqOriginal, OracleDbType.Long, ParameterDirection.Input);

                var qtde = await DbConn.QueryFirstAsync<int>(sql, param: OracleParameter, commandType: CommandType.Text);

                return qtde > 0;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #endregion

        #region [Métodos privados]

        private long ObterIdFila()
        {
            var sql = "SELECT NETUNO.SEQ_LOGPROC.NextVal FROM DUAL";

            return DbConn.QueryFirst<long>(sql);
        }

        private async Task<long> ObterIdFilaAsync()
        {
            var sql = "SELECT NETUNO.SEQ_LOGPROC.NextVal FROM DUAL";

            return await DbConn.QueryFirstAsync<long>(sql);
        }

        #endregion

    }
}
