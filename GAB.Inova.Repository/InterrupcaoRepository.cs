﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class InterrupcaoRepository : DapperRepository, IInterrupcaoRepository
    {

        readonly ILogger _logger;

        public InterrupcaoRepository(IDbConnection dbConnection, 
                                     ILoggerFactory loggerFactory) : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<InterrupcaoRepository>();
        }

        public async Task<Interrupcao> ConsultarInterrupcaoAsync(VwDadosClienteInterrupcao dadosClienteInterrupcao)
        {
            try
            {
                var sql = @"SELECT *
                              FROM INTERRUPCAO INT
                              JOIN INTERRUPCAO_DETALHE DET
                                ON DET.ID_INTERRUPCAO = INT.ID_INTERRUPCAO
                              JOIN INTERRUPCAO_STATUS STS
                                ON STS.ID_INTERRUPCAO_STATUS = INT.ID_INTERRUPCAO_STATUS
                              JOIN INTERRUPCAO_TIPO ITP
                                ON ITP.ID_INTERRUPCAO_TIPO = INT.ID_INTERRUPCAO_TIPO
                              JOIN INTERRUPCAO_TIPO_ABERTURA ITA
                                ON ITA.ID_INTERRUPCAO_TP_ABERTURA = INT.ID_INTERRUPCAO_TP_ABERTURA
                             WHERE INT.DT_HR_FIM > SYSDATE
                               AND STS.ID_INTERRUPCAO_STATUS = 3 --EM ANDAMENTO
                               AND (  (    ITA.ID_INTERRUPCAO_TP_ABERTURA = 2 --SETOR ABASTECIMENTO
                                       AND DET.ID_SETOR_OPER IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NULL 
                                       AND DET.ID_LOGRADOURO IS NULL 
                                       AND DET.ID_SETOR_OPER = :P_SETOR) 
                                    OR(    ITA.ID_INTERRUPCAO_TP_ABERTURA = 2
                                       AND DET.ID_SETOR_OPER IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NOT NULL 
                                       AND DET.ID_LOGRADOURO IS NULL 
                                       AND DET.ID_SETOR_OPER = :P_SETOR 
                                       AND DET.ID_BAIRRO = :P_BAIRRO) 
                                    OR(    ITA.ID_INTERRUPCAO_TP_ABERTURA = 2
                                       AND DET.ID_SETOR_OPER IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NOT NULL 
                                       AND DET.ID_LOGRADOURO IS NOT NULL 
                                       AND DET.ID_SETOR_OPER = :P_SETOR 
                                       AND DET.ID_LOGRADOURO = :P_LOGRADOURO) 
                                    OR(    ITA.ID_INTERRUPCAO_TP_ABERTURA = 1 --MUNICÍPIO
                                       AND DET.ID_CIDADE IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NULL 
                                       AND DET.ID_LOGRADOURO IS NULL 
                                       AND DET.ID_CIDADE = :P_CIDADE) 
                                    OR(    ITA.ID_INTERRUPCAO_TP_ABERTURA = 1
                                       AND DET.ID_CIDADE IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NOT NULL 
                                       AND DET.ID_LOGRADOURO IS NULL 
                                       AND DET.ID_BAIRRO = :P_BAIRRO) 
                                    OR(    ITA.ID_INTERRUPCAO_TP_ABERTURA = 1 
                                       AND DET.ID_CIDADE IS NOT NULL 
                                       AND DET.ID_BAIRRO IS NOT NULL 
                                       AND DET.ID_LOGRADOURO IS NOT NULL 
                                       AND DET.ID_LOGRADOURO = :P_LOGRADOURO))
                             ORDER BY INT.DT_HR_FIM DESC";

                OracleParameter.Add("P_SETOR", dadosClienteInterrupcao.IdSetorOperacional, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_BAIRRO", dadosClienteInterrupcao.IdBairro, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_LOGRADOURO", dadosClienteInterrupcao.IdLogradouro, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_CIDADE", dadosClienteInterrupcao.IdCidade, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Interrupcao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao consultar interrupção. VwDadosClienteInterrupcao: { JsonConvert.SerializeObject(dadosClienteInterrupcao)}");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}