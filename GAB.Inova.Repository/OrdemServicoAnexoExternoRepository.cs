﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.OrdemServicoAnexoExternoSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class OrdemServicoAnexoExternoRepository
        : DapperRepository, IOrdemServicoAnexoExternoRepository
    {

        readonly ILogger _logger;

        public OrdemServicoAnexoExternoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory) 
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<OrdemServicoAnexoExternoRepository>();
        }
        
        public async Task InserirAsync(OrdemServicoAnexoExterno entity)
        {
            try
            {
                var sql = @" INSERT INTO NETUNO.OS_ANEXO_EXTERNO(ID_OS_ELETRONICA, NUM_OS, DESCRICAO, IDENTIFICADOR) VALUES (:pID_OS_ELETRONICA, :pNUM_OS, :pDESCRICAO, :pIDENTIFICADOR) ";

                OracleParameter.Add("pID_OS_ELETRONICA", entity.OSEletronicaID, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pNUM_OS", entity.NumeroOS.HasValue ? (object)entity.NumeroOS.Value : (object)DBNull.Value, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pDESCRICAO", !string.IsNullOrWhiteSpace(entity.Descricao) ? (object)entity.Descricao : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("pIDENTIFICADOR", !string.IsNullOrWhiteSpace(entity.Identificador) ? (object)entity.Identificador : (object)DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);

                await DbConn.ExecuteAsync(sql, param: OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir o anexo Descrição: {entity?.Descricao} Identificador: {entity?.Identificador}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> VerificarExistenciaAsync(OrdemServicoAnexoExterno entity)
        {
            try
            {
                var sql = @"SELECT COUNT(1) 
                              FROM NETUNO.OS_ANEXO_EXTERNO ANX
                             WHERE ANX.ID_OS_ELETRONICA = :pID_OS_ELETRONICA
                               AND ANX.IDENTIFICADOR = :pIDENTIFICADOR ";

                OracleParameter.Add("pID_OS_ELETRONICA", entity.OSEletronicaID, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pIDENTIFICADOR", entity.Identificador, OracleDbType.Varchar2, ParameterDirection.Input);

                var result = await DbConn.ExecuteScalarAsync<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existencia do anexo Descrição: {entity?.Descricao} Identificador: {entity?.Identificador}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public ValidationResult PodeAlterar(OrdemServicoAnexoExterno entity) => throw new NotImplementedException();

        public ValidationResult PodeExcluir(OrdemServicoAnexoExterno entity) => throw new NotImplementedException();

        public ValidationResult PodeIncluir(OrdemServicoAnexoExterno entity)
        {
            try
            {
                return new OSAnexoExternoPodeSerInseridoValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar se pode ser inserido o anexo Descrição: {entity?.Descricao} Identificador: {entity?.Identificador}.");

                throw;
            }
        }
    }
}
