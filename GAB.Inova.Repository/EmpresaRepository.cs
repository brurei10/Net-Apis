﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class EmpresaRepository
        : DapperRepository, IEmpresaRepository
    {

        readonly ILogger _logger;

        public EmpresaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<EmpresaRepository>();
        }
        
        public async Task<Empresa> ObterAsync()
        {
            try
            {
                var sql = @"SELECT E.ID_EMPRESA, E.NOME_EMPRESA, E.SIGLA, E.ID_FEBRABAN, E.URL_ENDPOINT 
                              FROM NETUNO.EMPRESA E";

                return await DbConn.QueryFirstOrDefaultAsync<Empresa>(sql, OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
