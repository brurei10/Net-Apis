﻿using GAB.Inova.Common.Interfaces.Providers;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;

namespace GAB.Inova.Repository
{

    public class OracleMultitenantConnection : OracleConnectionResolver, IDbConnection
    {

        public OracleMultitenantConnection(IConfiguration configuration, ITokenProvider tokenProvider) 
            : base(configuration, tokenProvider) { }

        public string ConnectionString
        {
            get => Conn.ConnectionString;
            set => Conn.ConnectionString = value;
        }

        public int ConnectionTimeout => Conn.ConnectionTimeout;

        public string Database => Conn.Database;

        public ConnectionState State => Conn.State;

        public IDbTransaction BeginTransaction()
        {
            Open();

            return Conn.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            Open();

            return Conn.BeginTransaction(isolationLevel);
        }

        public void ChangeDatabase(string databaseName)
        {
            if (Conn == null) throw new NullReferenceException("Conexão não inicializada.");

            Conn.ChangeDatabase(databaseName);
        }

        public void Close()
        {
            if (Conn == null) throw new NullReferenceException("Conexão não inicializada.");

            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
            }
        }

        public IDbCommand CreateCommand()
        {
            if (Conn == null) throw new NullReferenceException("Conexão não inicializada.");

            return Conn.CreateCommand();
        }

        public void Dispose()
        {
            if (Conn != null)
            {
                Conn.Dispose();
            }
        }

        public void Open()
        {
            if (Conn == null) throw new NullReferenceException("Conexão não inicializada.");

            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn.Open();
            }
        }
    }
}
