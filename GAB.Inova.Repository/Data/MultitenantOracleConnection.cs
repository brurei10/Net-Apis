﻿using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace GAB.Inova.Repository.Data
{
    public sealed class MultitenantOracleConnection
        : IDbConnection
    {

        readonly IDbConnection _dbConnection;
        readonly IAuditConfiguration _auditConfiguration;
        readonly IMultitenantConnectionProvider _multitenantConnectionProvider;

        public MultitenantOracleConnection(IAuditConfiguration auditConfiguration, IMultitenantConnectionProvider multitenantConnectionProvider)
        {
            _auditConfiguration = auditConfiguration;
            _multitenantConnectionProvider = multitenantConnectionProvider;

            _dbConnection = new OracleConnection();
        }

        #region [Implementação da interface IDbConnection]

        public string ConnectionString
        {
            get => _dbConnection.ConnectionString;
            set => _dbConnection.ConnectionString = value;
        }

        public int ConnectionTimeout => _dbConnection.ConnectionTimeout;

        public string Database => _dbConnection.Database;

        public ConnectionState State => _dbConnection.State;

        public IDbTransaction BeginTransaction()
        {
            Open();

            return _dbConnection.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            Open();

            return _dbConnection.BeginTransaction(il);
        }

        public void ChangeDatabase(string databaseName)
        {
            ValidateDbConnectionInstance();

            _dbConnection.ChangeDatabase(databaseName);
        }

        public void Close()
        {
            ValidateDbConnectionInstance();

            if (_dbConnection.State == ConnectionState.Open)
            {
                _dbConnection.Close();
            }
        }

        public IDbCommand CreateCommand()
        {
            ValidateDbConnectionInstance();

            return _dbConnection.CreateCommand();
        }

        public void Dispose()
        {
            Close();

            _dbConnection?.Dispose();
        }

        public void Open()
        {
            if (_dbConnection.State == ConnectionState.Open) return;

            ValidateDbConnectionInstance();

            if (string.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = _multitenantConnectionProvider.ObterConnectionStringAsync()
                    .GetAwaiter()
                    .GetResult();
            }

            if (_dbConnection.State == ConnectionState.Closed || _dbConnection.State == ConnectionState.Broken)
            {
                _dbConnection.Open();

                IniciarAuditoria();
            }
        }

        #endregion

        #region [Métodos privados]

        private void ValidateDbConnectionInstance()
        {
            if (_dbConnection is null) throw new NullReferenceException("Conexão não inicializada.");
        }

        private void IniciarAuditoria()
        {
            using (var cmd = CreateCommand())
            {
                cmd.CommandText = $@"begin INOVA_AUDIT.PACK_AUDITORIA.PR_SEQUENCIA_AUDITORIA({_auditConfiguration.ObterUsuarioId()}, {_auditConfiguration.ObterRotina()}, '{_auditConfiguration.ObterEnderecoIP()}'); end; ";
                cmd.ExecuteNonQuery();
            }
        }

        #endregion


    }
}
