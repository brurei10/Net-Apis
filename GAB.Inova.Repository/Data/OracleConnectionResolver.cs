﻿using GAB.Inova.Common.Exception;
using GAB.Inova.Common.Interfaces.Providers;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;

namespace GAB.Inova.Repository
{
    public abstract class OracleConnectionResolver
    {
        
        protected readonly IDbConnection Conn;

        public OracleConnectionResolver(IConfiguration configuration, ITokenProvider tokenProvider)
        {
            try
            {
                var tokenDoHeader = tokenProvider.ObterTokenContextoAtual();

                if (string.IsNullOrEmpty(tokenDoHeader)) throw new InovaValidationException("Token de autorização não localizada");

                var empresa = tokenProvider.ObterClaim(tokenDoHeader, "CodigoEmpresa");

                if (empresa == null) throw new InovaValidationException("Código da Empresa não localizada");

                Conn = new OracleConnection(configuration
                                                .GetSection("ConnectionStrings")
                                                .AsEnumerable()
                                                .FirstOrDefault(a => a.Key.ToLower().Equals($"connectionstrings:{empresa.Trim().ToLower()}"))
                                                .Value);
            }
            catch (InovaValidationException ex)
            {
                throw (ex);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
