﻿using Dapper;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class HistoricoLigacaoRepository
        : DapperRepository, IHistoricoLigacaoRepository
    {
        
        readonly ILogger _logger;

        public HistoricoLigacaoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<HistoricoLigacaoRepository>();
        }

        public async Task<HistoricoLigacao> ObterPorMatriculaAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT HL.ID_HISTORICO_LIGACAO,
                                   HL.DATA_HIST,
                                   HL.MATRICULA,
                                   HL.ID_CICLO,
                                   HL.SIT_AGUA,
                                   HL.SIT_ESGOTO,
                                   HL.SIT_HD,
                                   HL.ANO_MES,
                                   HL.ID_GRUPO_ENTREGA,
                                   HL.ID_CONTRATO,
                                   HL.DEBITO_AUTOMATICO
                              FROM NETUNO.HISTORICO_LIGACAO HL
                             WHERE HL.MATRICULA = :P_MATRICULA";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<HistoricoLigacao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(HistoricoLigacao)} [matrícula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<HistoricoLigacao> ObterPorMatriculaEReferenciaAsync(string matricula, string anoMes)
        {
            try
            {
                var sql = @"SELECT HL.ID_HISTORICO_LIGACAO,
                                   HL.DATA_HIST,
                                   HL.MATRICULA,
                                   HL.ID_CICLO,
                                   HL.SIT_AGUA,
                                   HL.SIT_ESGOTO,
                                   HL.SIT_HD,
                                   HL.ANO_MES,
                                   HL.ID_GRUPO_ENTREGA,
                                   HL.ID_CONTRATO,
                                   HL.DEBITO_AUTOMATICO
                              FROM NETUNO.HISTORICO_LIGACAO HL
                             WHERE HL.MATRICULA = :P_MATRICULA
                               AND HL.ANO_MES = :P_ANO_MES";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ANO_MES", anoMes, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<HistoricoLigacao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter por matricula e referência - {typeof(HistoricoLigacao)} [matrícula:{matricula}, referencia:{anoMes.ToReferencia()}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
