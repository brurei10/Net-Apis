﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ProtocoloAtendimentoHistoricoRepository
        : DapperRepository, IProtocoloAtendimentoHistoricoRepository
    {

        readonly ILogger _logger;

        public ProtocoloAtendimentoHistoricoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ProtocoloAtendimentoHistoricoRepository>();
        }

        public async Task<bool> InserirAsync(ProtocoloAtendimentoHistorico entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.PROTOCOLO_ATEND_HISTORICO
                                (ID_PROTOCOLO_ATENDIMENTO,
                                HISTORICO,
                                ID_USUARIO,
                                DATA_HISTORICO,
                                TEMPO,
                                NIVEL_SATISFACAO,
                                ID_LOCAL_ATEND,
                                ID_TIPO_ATENDIMENTO,
                                MATRICULA,
                                LOCALIDADE,
                                ID_MODULO,
                                TIPO_PROTOCOLO)
                            VALUES
                                (:P_ID_PROTOCOLO_ATENDIMENTO,
                                :P_HISTORICO,
                                :P_ID_USUARIO,
                                SYSDATE,
                                :P_TEMPO,
                                :P_NIVEL_SATISFACAO,
                                :P_ID_LOCAL_ATEND,
                                :P_ID_TIPO_ATENDIMENTO,
                                :P_MATRICULA,
                                :P_LOCALIDADE,
                                :P_ID_MODULO,
                                :P_TIPO_PROTOCOLO)
                            RETURNING ID_PROTOCOLO_ATEND_OBS INTO :V_ID_PROTOCOLO_ATEND_OBS";

                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", entity.IdProtocoloAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_HISTORICO", (!string.IsNullOrEmpty(entity.Historico) ? (object)entity.Historico.ToUpper() : DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_USUARIO", entity.IdUsuario, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TEMPO", (entity.Tempo.HasValue ?(object)entity.Tempo.Value : DBNull.Value), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_NIVEL_SATISFACAO", (entity.NivelSatisfacao.HasValue ? (object)entity.NivelSatisfacao.Value : DBNull.Value), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_LOCAL_ATEND", (int)entity.IdLocalAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_TIPO_ATENDIMENTO", (int)entity.IdTipoAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", (!string.IsNullOrEmpty(entity.Matricula) ? (object)entity.Matricula : DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_LOCALIDADE", (!string.IsNullOrEmpty(entity.Localidade) ? (object)entity.Localidade : DBNull.Value), OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_MODULO", (entity.IdModulo.HasValue ? (object)entity.IdModulo.Value : DBNull.Value), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_PROTOCOLO", entity.TipoProtocolo, OracleDbType.Int32, ParameterDirection.Input);

                OracleParameter.Add("V_ID_PROTOCOLO_ATEND_OBS", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_PROTOCOLO_ATEND_OBS").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ProtocoloAtendimentoHistorico)} [id protocolo atendimento:{entity.IdProtocoloAtendimento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
