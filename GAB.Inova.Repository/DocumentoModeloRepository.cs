﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class DocumentoModeloRepository : DapperRepository, IDocumentoModeloRepository
    {
        readonly ILogger _logger;

        public DocumentoModeloRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<VwClienteRepository>();
        }

        public async Task<DocumentoModelo> ObterPorDocumentoModeloTipoAsync(long modeloTipo)
        {
            try
            {
                var sql = @"SELECT *
                              FROM NETUNO.DOCUMENTO_MODELO
                             WHERE ID_DOCUMENTO_MODELO_TIPO = :P_ID_DOCUMENTO_MODELO_TIPO
                               AND ORIGEM = 2 ";

                OracleParameter.Add("P_ID_DOCUMENTO_MODELO_TIPO", modeloTipo, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<DocumentoModelo>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(DocumentoModelo)} [Modelo tipo:{modeloTipo}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
