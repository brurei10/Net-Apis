﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class TempImprimeContaRepository
        : DapperRepository, ITempImprimeContaRepository
    {

        readonly ILogger _logger;

        public TempImprimeContaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TempImprimeContaRepository>();
        }

        public async Task<bool> DeletarTempSeqAsync(TempImprimeConta entity)
        {
            try
            {
                var sql = @"DELETE FROM NETUNO.TEMP_IMP_MATRICULA TIM
                             WHERE TIM.ID_USUARIO = :P_ID_USUARIO";

                OracleParameter.Add("P_ID_USUARIO", entity.IdUsuario, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao deletar - {typeof(TempImprimeConta)} [idUsuario:{entity.IdUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> InserirTempSeqAsync(TempImprimeConta entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.TEMP_IMP_MATRICULA
                              (MATRICULA, ID_USUARIO, SEQ_ORIGINAL)
                            VALUES
                              (:P_MATRICULA, :P_ID_USUARIO, :P_SEQ_ORIGINAL)";

                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_USUARIO", entity.IdUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_SEQ_ORIGINAL", entity.Id, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(TempImprimeConta)} [matricula:{entity.Matricula}, seqOriginal:{entity.Id}, idUsuario:{entity.IdUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> GerarTempContaAsync(TempImprimeConta entity)
        {
            try
            {
                var sql = @"P_GERATEMPCONTA";

                OracleParameter.Add("pIDUSUARIO", entity.IdUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pTIPOIMP", entity.TipoImpressao, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("pVIA", entity.Via, OracleDbType.Int64, ParameterDirection.Input);

                await DbConn.ExecuteAsync(sql, param: OracleParameter, commandType: CommandType.StoredProcedure);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao gerar - {typeof(TempImprimeConta)} [idUsuario:{entity.IdUsuario}, via:{entity.Via}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
