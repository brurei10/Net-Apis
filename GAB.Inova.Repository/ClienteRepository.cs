﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.ClienteSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ClienteRepository
        : DapperRepository, IClienteRepository
    {

        readonly ILogger _logger;

        public ClienteRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ClienteRepository>();
        }
        
        public async Task<Cliente> ObterPorMatriculaAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT CLI.LOCALIZACAO,
                                   CLI.DV_LOCALIZ,
                                   CLI.MATRICULA,
                                   CLI.NOME_CLIENTE,
                                   CLI.TIPO_ENTREGA,
                                   CLI.TIPO_CLIENTE,
                                   CLI.SIT_AGUA,
                                   CLI.SIT_ESGOTO,
                                   CLI.SIT_HD,
                                   CLI.VENC_CONTA,
                                   CLI.DV_MATR,
                                   CLI.DEBITO_AUTOMATICO,
                                   CLI.GRUPO_ENTREGA
                              FROM NETUNO.CLIENTE CLI 
                             WHERE CLI.MATRICULA = :P_MATRICULA";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Cliente>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Cliente)} [matrícula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<DateTime> ObterDataProximaLeituraAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT P.DATA_MINIMA AS DT_PROXIMA_LEITURA
                              FROM NETUNO.PROGRAMACAO P
                             WHERE P.ID_ATIVIDADE = 2
                               AND P.ID_CICLO = (SELECT S.ID_CICLO
                                                   FROM SETOR S
                                                  INNER JOIN CLIENTE C
                                                     ON C.ID_SETOR = S.ID_SETOR
                                                  WHERE C.MATRICULA = :P_MATRICULA)
                               AND P.ANO_MES_PROGRAMACAO =
                                   (SELECT MIN(ANO_MES_PROGRAMACAO)
                                      FROM PROGRAMACAO
                                     WHERE PROGRAMACAO.ID_CICLO = P.ID_CICLO
                                       AND PROGRAMACAO.ID_ATIVIDADE = 1
                                       AND PROGRAMACAO.SIT_CICLO = 1)";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<DateTime>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter data da próxima leitura - {typeof(Cliente)} [matrícula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<Cliente> ObterPorMatriculaECpfCnpjAsync(string matricula, string cpfCnpj)
        {
            try
            {
                var sql = @"SELECT CLI.LOCALIZACAO,
                                   CLI.DV_LOCALIZ,
                                   CLI.MATRICULA,
                                   CLI.SIT_AGUA,
                                   CLI.SIT_ESGOTO,
                                   CLI.SIT_HD,
                                   CTR.ID_CONTRATO,
                                   CTR.CPF_CNPJ,
                                   CTR.MATRICULA,
                                   CTR.NOME_CLIENTE,
                                   CTR.TELEFONE_RES,
                                   CTR.TELEFONE_COM,
                                   CTR.TELEFONE_CEL,
                                   CTR.EMAIL,
                                   CTR.NUMERO,
                                   CTR.COMPLEMENTO,
                                   CTR.PONTO_REFER,
                                   CTR.EMAIL_CONTA_DIGITAL,
                                   L.ID_LOGRADOURO,
                                   L.LOGRADOURO_CEP,
                                   L.NOME_LOGRADOURO,
                                   LT.LTP_ID,
                                   LT.NOME_LOGRADOURO_TIPO,
                                   B.ID_BAIRRO,
                                   B.NOME_BAIRRO,
                                   C.ID_CIDADE,
                                   C.NOME_CIDADE,
                                   E.ID_ESTADO,
                                   E.NOME_ESTADO
                              FROM NETUNO.CLIENTE CLI
                              JOIN NETUNO.CONTRATO CTR
                                ON CTR.MATRICULA = CLI.MATRICULA
                              JOIN NETUNO.LOGRADOURO L
                                ON L.ID_LOGRADOURO = CTR.ID_LOGRADOURO
                              JOIN NETUNO.LOGRADOURO_TIPO LT
                                ON LT.LTP_ID = L.LTP_ID
                              JOIN NETUNO.BAIRRO B
                                ON B.ID_BAIRRO = L.ID_BAIRRO
                              JOIN NETUNO.CIDADE C
                                ON C.ID_CIDADE = L.ID_CIDADE
                              JOIN NETUNO.ESTADO E
                                ON E.ID_ESTADO = C.ID_ESTADO
                             WHERE (CLI.SIT_AGUA != 9 OR CLI.SIT_ESGOTO != 9)
                               AND CLI.MATRICULA = :P_MATRICULA
                               AND CTR.CPF_CNPJ  = :P_CPF_CNPJ";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CPF_CNPJ", cpfCnpj, OracleDbType.Varchar2, ParameterDirection.Input);

                var clientes = await DbConn.QueryAsync<Cliente, Contrato, Logradouro, LogradouroTipo, Bairro, Cidade, Estado, Cliente>(sql,
                    map: (cliente, contrato, logradouro, logradouroTipo, bairro, cidade, estado) =>
                    {
                        logradouro.Bairro = bairro;
                        logradouro.Cidade = cidade;
                        logradouro.Estado = estado;
                        logradouro.LogradouroTipo = logradouroTipo;

                        contrato.Logradouro = logradouro;

                        cliente.Contrato = contrato;

                        return cliente;
                    },
                    param: OracleParameter,
                    splitOn: "ID_CONTRATO,ID_LOGRADOURO,LTP_ID,ID_BAIRRO,ID_CIDADE,ID_ESTADO"
                );

                return clientes.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter por matricula e cpf/cnpj - {typeof(Cliente)} [matrícula:{matricula}, cpfCnpj:{cpfCnpj}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizaGrupoEntregaAsync(string matricula, long idGrupoEntrega)
        {
            try
            {
                var sql = @"UPDATE NETUNO.CLIENTE SET GRUPO_ENTREGA = :P_GRUPO_ENTREGA WHERE MATRICULA = :P_MATRICULA ";

                OracleParameter.Add("P_GRUPO_ENTREGA", idGrupoEntrega, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar grupo entrega - {typeof(Cliente)} [matrícula:{matricula}, grupo entrega:{idGrupoEntrega}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        
        #region [Métodos públicos para validação]

        public ValidationResult PodeAderirContaDigital(Cliente entity)
        {
            try
            {
                return new ClientePodeAderirContaDigitalValidation().Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar adesão de conta digital - {typeof(Cliente)}.");

                throw;
            }
        }

        public ValidationResult PodeAlterarContaDigital(Cliente entity)
        {
            try
            {
                return new ClientePodeAlterarContaDigitalValidation().Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar alteração de conta digital - {typeof(Cliente)}.");

                throw;
            }
        }

        public ValidationResult PodeCancelarContaDigital(Cliente entity)
        {
            try
            {
                return new ClientePodeAlterarContaDigitalValidation().Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar cancelamento de conta digital - {typeof(Cliente)}.");

                throw;
            }
        }

        #endregion

    }
}
