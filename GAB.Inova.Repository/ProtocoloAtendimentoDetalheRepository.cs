﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ProtocoloAtendimentoDetalheRepository
        : DapperRepository, IProtocoloAtendimentoDetalheRepository
    {

        readonly ILogger _logger;

        public ProtocoloAtendimentoDetalheRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ProtocoloAtendimentoDetalheRepository>();
        }

        public async Task<bool> InserirAsync(ProtocoloAtendimentoDetalhe entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.PROTOCOLO_ATEND_DETALHE
                              (ID_PROTOCOLO_ATEND,
                               ID_ROTINA,
                               DATA_INICIO,
                               NUMERO_OS,
                               NUMERO_DOC,
                               ID_NATUREZA,
                               TIPO_PROTOCOLO)
                            VALUES
                              (:P_ID_PROTOCOLO_ATEND,
                               :P_ID_ROTINA,
                               SYSDATE,
                               :P_NUMERO_OS,
                               :P_NUMERO_DOC,
                               :P_ID_NATUREZA,
                               :P_TIPO_PROTOCOLO)
                            RETURNING ID_PROT_ATEND_DET INTO :V_ID_PROT_ATEND_DET";

                OracleParameter.Add("P_ID_PROTOCOLO_ATEND", entity.IdProtocoloAtendimento, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_ROTINA", (entity.IdRotina.HasValue ? (object)entity.IdRotina.Value : DBNull.Value), OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_NUMERO_OS", (entity.NumeroOs.HasValue ? (object)entity.NumeroOs.Value : DBNull.Value), OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_NUMERO_DOC", (entity.NumeroDoc.HasValue ? (object)entity.NumeroDoc.Value : DBNull.Value), OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_NATUREZA", entity.IdNatureza, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_PROTOCOLO", entity.TipoProtocolo, OracleDbType.Int32, ParameterDirection.Input);

                OracleParameter.Add("V_ID_PROT_ATEND_DET", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_PROT_ATEND_DET").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ProtocoloAtendimentoDetalhe)} [id protocolo atendimento:{entity.IdProtocoloAtendimento}, id natureza:{entity.IdNatureza}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        
        public async Task<bool> VerificarExistenciaDeNaturezaAsync(long idProtocoloAtendimento, int idNatureza)
        {
            try
            {
                var sql = @"SELECT COUNT(1)
                              FROM NETUNO.PROTOCOLO_ATEND_DETALHE PAD
                             WHERE PAD.ID_PROTOCOLO_ATEND = :P_ID_PROTOCOLO_ATEND
                               AND PAD.ID_NATUREZA = :P_ID_NATUREZA";

                OracleParameter.Add("P_ID_PROTOCOLO_ATEND", idProtocoloAtendimento, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_NATUREZA", idNatureza, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteScalarAsync<int>(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar existência de natureza - {typeof(ProtocoloAtendimentoDetalhe)} [id protocolo atendimento:{idProtocoloAtendimento}, id natureza:{idNatureza}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}

