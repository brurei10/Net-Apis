﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class TempHistoricoConsumoRepository
        : DapperRepository, ITempHistoricoConsumoRepository
    {

        readonly ILogger _logger;

        public TempHistoricoConsumoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TempHistoricoConsumoRepository>();
        }

        public async Task<IEnumerable<TempHistoricoConsumo>> ObterTempHistoricoConsumoAsync(string matricula, long idUsuario)
        {
            try
            {
                var sql = @"SELECT THC.REFERENCIA,
                                   THC.CONSUMO,
                                   THC.QTDEDIAS,
                                   THC.ANOMES,
                                   THC.MATRICULA,
                                   THC.ID_USUARIO,
                                   THC.ID_TEMP_HISTORICO_CONSUMO
                              FROM NETUNO.TEMP_HISTORICO_CONSUMO THC
                             WHERE THC.ID_USUARIO = :P_ID_USUARIO
                               AND THC.MATRICULA = :P_MATRICULA
                            ORDER BY SUBSTR(THC.REFERENCIA, 4, 4) || SUBSTR(THC.REFERENCIA, 1, 2) DESC";

                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<TempHistoricoConsumo>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(TempHistoricoConsumo)} [matricula:{matricula}, idUsuario:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
