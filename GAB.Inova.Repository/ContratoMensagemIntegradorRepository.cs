﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ContratoMensagemIntegradorRepository
        : DapperRepository, IContratoMensagemIntegradorRepository
    {

        readonly ILogger _logger;

        public ContratoMensagemIntegradorRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
           : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContratoRepository>();
        }
        
        public async Task<bool> InserirAsync(ContratoMensagemIntegrador entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.CONTRATO_MENSAGEM_INTEGRA
                              (ID_MENSAGEM_INTEGRADOR, DT_INCLUSAO, ID_CONTRATO)
                            VALUES
                              (:P_ID_MENSAGEM_INTEGRADOR, SYSDATE, :P_ID_CONTRATO)
                            RETURNING ID_CONTRATO_MENSAGEM_INTEGRA INTO :V_ID_CONTRATO_MENSAGEM_INTEGRA";

                OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", entity.IdMensagemIntegrador, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CONTRATO", entity.IdContrato, OracleDbType.Int32, ParameterDirection.Input);

                OracleParameter.Add("V_ID_CONTRATO_MENSAGEM_INTEGRA", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql: sql, param: OracleParameter, commandType: CommandType.Text);

                entity.Id = Convert.ToInt64(Convert.ToString(OracleParameter.Get<dynamic>("V_ID_CONTRATO_MENSAGEM_INTEGRA")));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ContratoMensagemIntegrador)} [id contrato:{entity?.IdContrato}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
