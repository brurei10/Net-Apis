﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ParcelaRepository : DapperRepository, IParcelaRepository
    {

        readonly ILogger _logger;

        public ParcelaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ParcelaRepository>();
        }

        public async Task<double> ObterTotalPorMatriculaSituacaoContratoAsync(string matricula, int situacao, long contrato)
        {
            try
            {
                var sql = @"SELECT NVL(SUM(VL_PARCELA), 0) AS VALOR
                              FROM PARCELA 
                             WHERE MATRICULA = :P_MATRICULA
                               AND SIT_PARCELA = :P_SIT_PARCELA
                               AND ID_CONTRATO = :P_ID_CONTRATO";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_SIT_PARCELA", (int)situacao, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CONTRATO", contrato, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<double>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter total - {typeof(Parcela)} [matricula:{matricula}, situacao:{situacao}, contrato:{contrato}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}