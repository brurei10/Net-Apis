﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class GRPRepository: DapperRepository, IGRPRepository
    {

        readonly ILogger _logger;

        public GRPRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory): base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<GRPRepository>();
        }

        public async Task<bool> InserirAsync(GRP entity)
        {
            try
            {
                var sql = @"INSERT 
                              INTO GRP (SEQ_ORIGINAL, 
                                        DATA_HORA_EMISSAO_GRP, 
                                        MATRICULA, 
                                        LOCALIZACAO, 
                                        NRO_OS, 
                                        VL_GRP, 
                                        DATA_VENC_GRP, 
                                        DATA_VL_GRP, 
                                        NOME_SOLICITANTE, 
                                        IDENT_SOLICITANTE, 
                                        OBSERVACAO, 
                                        ID_USUARIO, 
                                        ID_SOLICITACAO, 
                                        ID_PROTOCOLO_ATENDIMENTO) 
                                VALUES (:P_SEQORIGINAL, 
                                        :P_DATAHREMISSAOGRP, 
                                        :P_MATRICULA, 
                                        :P_LOCALIZACAO, 
                                        :P_NROOS, 
                                        :P_VLGRP, 
                                        :P_DATAVENCGRP, 
                                        :P_DATAVALGRP, 
                                        :P_NOMESOLICITANTE, 
                                        :P_IDENTSOLICITANTE, 
                                        :P_OBSERVACAO, 
                                        :P_IDUSUARIO, 
                                        :P_IDSOLICITACAO, 
                                        :P_ID_PROTOCOLO_ATENDIMENTO)";

                OracleParameter.Add("P_SEQORIGINAL", entity.Id, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_DATAHREMISSAOGRP", entity.DataHoraEmissaoGRP, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_LOCALIZACAO", entity.Localizacao, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NROOS", entity.NroOs, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_VLGRP", entity.ValorGRP, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVENCGRP", entity.DataVencimentoGRP, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVALGRP", entity.DataValorGRP, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_NOMESOLICITANTE", entity.NomeSolicitante, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_IDENTSOLICITANTE", entity.IdentidadeSolicitante, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_OBSERVACAO", entity.Observacao, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_IDUSUARIO", entity.IdUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_IDSOLICITACAO", entity.IdSolicitacao, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ID_PROTOCOLO_ATENDIMENTO", entity.IdProtocoloAtendimento, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(GRP)} [seq original:{entity.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
