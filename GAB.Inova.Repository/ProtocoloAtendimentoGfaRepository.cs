﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.ProtocoloAtendimentoGfaSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ProtocoloAtendimentoGfaRepository
        : DapperRepository, IProtocoloAtendimentoGfaRepository
    {

        readonly ILogger _logger;

        public ProtocoloAtendimentoGfaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ProtocoloAtendimentoGfaRepository>();
        }

        public async Task<ProtocoloAtendimentoGfa> ObterPorNumeroESenhaAsync(string numeroProtocolo, string senha)
        {
            try
            {
                var sql = @"SELECT PAG.ID_PROTOCOLO_ATEND_GFA,
                                   PAG.NR_SENHA_GFA,
                                   PAG.NR_PROTOCOLO_GFA,
                                   PAG.NM_USUARIO_GFA,
                                   PAG.ID_INTEGRADOR,
                                   PAG.DT_INI_ATEND,
                                   PAG.DT_FIM_ATEND
                              FROM NETUNO.PROTOCOLO_ATEND_GFA PAG
                             WHERE PAG.NR_SENHA_GFA = :P_NR_SENHA_GFA
                               AND PAG.NR_PROTOCOLO_GFA = :P_NR_PROTOCOLO_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", senha, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", numeroProtocolo, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ProtocoloAtendimentoGfa>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ProtocoloAtendimentoGfa)} [nº protocolo:{numeroProtocolo}, senha:{senha}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<string> ObterNumeroProtocoloAtendimentoPaiAsync(string numeroProtocolo, string senha)
        {
            try
            {
                var sql = @"SELECT PA.NR_PROTOCOLO_PAI
                              FROM NETUNO.PROTOCOLO_ATENDIMENTO PA
                              JOIN NETUNO.PROTOCOLO_ATEND_GFA PAG
                                ON PAG.ID_PROTOCOLO_ATEND_GFA = PA.ID_PROTOCOLO_ATEND_GFA
                             WHERE PA.NUM_PROTOCOLO = PA.NR_PROTOCOLO_PAI
                               AND PAG.NR_SENHA_GFA = :P_NR_SENHA_GFA
                               AND PAG.NR_PROTOCOLO_GFA = :P_NR_PROTOCOLO_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", senha, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", numeroProtocolo, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.ExecuteScalarAsync<string>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter nº protocolo pai iNova - {typeof(ProtocoloAtendimentoGfa)} [nº protocolo:{numeroProtocolo}, senha:{senha}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<ProtocoloAtendimentoGfa> ObterAtendimentoNaoFinalizadoPorUsuarioAsync(string usuario)
        {
            try
            {
                var sql = @"SELECT PAG.ID_PROTOCOLO_ATEND_GFA,
                                   PAG.NR_SENHA_GFA,
                                   PAG.NR_PROTOCOLO_GFA,
                                   PAG.NM_USUARIO_GFA,
                                   PAG.ID_INTEGRADOR,
                                   PAG.DT_INI_ATEND
                              FROM NETUNO.PROTOCOLO_ATEND_GFA PAG
                             WHERE PAG.DT_FIM_ATEND IS NULL
                               AND UPPER(PAG.NM_USUARIO_GFA) = UPPER(:P_NM_USUARIO_GFA)
                             ORDER BY PAG.DT_INI_ATEND DESC";

                OracleParameter.Add("P_NM_USUARIO_GFA", usuario, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ProtocoloAtendimentoGfa>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter protocolo iNova não finalizado por usuário - {typeof(ProtocoloAtendimentoGfa)} [usuário:{usuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> FinalizarAsync(string numeroProtocolo, string senha)
        {
            try
            {
                var sql = @"UPDATE NETUNO.PROTOCOLO_ATEND_GFA PAG
                               SET PAG.DT_FIM_ATEND = SYSDATE
                             WHERE PAG.NR_SENHA_GFA = :P_NR_SENHA_GFA
                               AND PAG.NR_PROTOCOLO_GFA = :P_NR_PROTOCOLO_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", senha, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", numeroProtocolo, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar - {typeof(ProtocoloAtendimentoGfa)} [nº protocolo:{numeroProtocolo}, senha:{senha}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> InserirAsync(ProtocoloAtendimentoGfa entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.PROTOCOLO_ATEND_GFA
                              (NR_SENHA_GFA,
                               NR_PROTOCOLO_GFA,
                               NM_USUARIO_GFA,
                               ID_INTEGRADOR,
                               DT_INI_ATEND,
                               DT_FIM_ATEND)
                            VALUES
                              (:P_NR_SENHA_GFA,
                               :P_NR_PROTOCOLO_GFA,
                               :P_NM_USUARIO_GFA,
                               :P_ID_INTEGRADOR,
                               SYSDATE,
                               :P_DT_FIM_ATEND)
                            RETURNING ID_PROTOCOLO_ATEND_GFA INTO :V_ID_PROTOCOLO_ATEND_GFA";

                OracleParameter.Add("P_NR_SENHA_GFA", entity.SenhaGfa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NR_PROTOCOLO_GFA", entity.NumeroProtocoloGfa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_NM_USUARIO_GFA", entity.UsuarioGfa, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_INTEGRADOR", (int)entity.IdIntegrador, OracleDbType.Int16, ParameterDirection.Input);
                OracleParameter.Add("P_DT_FIM_ATEND", (entity.DataFinalAtendimento.HasValue ? (object)entity.DataFinalAtendimento.Value : DBNull.Value), OracleDbType.Date, ParameterDirection.Input);

                OracleParameter.Add("V_ID_PROTOCOLO_ATEND_GFA", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_PROTOCOLO_ATEND_GFA").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ProtocoloAtendimentoGfa)} [nº protocolo:{entity.NumeroProtocoloGfa}, senha:{entity.SenhaGfa}, usuário:{entity.UsuarioGfa}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #region [Métodos públicos para validação]

        public ValidationResult PodeAlterar(ProtocoloAtendimentoGfa entity) => new ProtocoloAtendimentoGfaPodeAlterarValidation(this).Validate(entity);

        public ValidationResult PodeExcluir(ProtocoloAtendimentoGfa entity) => throw new System.NotImplementedException();

        public ValidationResult PodeIncluir(ProtocoloAtendimentoGfa entity) => new ProtocoloAtendimentoGfaPodeIncluirValidation(this).Validate(entity);

        #endregion

    }
}
