﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class MensagemIntegradorRepository
        : DapperRepository, IMensagemIntegradorRepository
    {

        readonly ILogger _logger;
        
        public MensagemIntegradorRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<MensagemIntegradorRepository>();
        }

        public async Task<MensagemIntegrador> ObterPorGuidAsync(string guid)
        {
            try
            {
                var sql = @"SELECT MI.ID_MENSAGEM_INTEGRADOR,
                                   MI.GUID_MENSAGEM,
                                   MI.TEMPLATE_ID,
                                   MI.CONTEUDO,
                                   MI.DT_INCLUSAO,
                                   MI.MENSAGEM_INTEGRADOR_TIPO,
                                   MI.INTEGRADOR_TIPO,
                                   MI.STATUS_TIPO,
                                   MI.DESTINATARIOS,
                                   MI.DESTINATARIOS_COPIA,
                                   MI.DESTINATARIOS_COPIA_OCULTA,
                                   MI.DE
                              FROM NETUNO.MENSAGEM_INTEGRADOR MI
                             WHERE MI.GUID_MENSAGEM = :P_GUID_MENSAGEM ";

                OracleParameter.Add("P_GUID_MENSAGEM", guid, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<MensagemIntegrador>(sql, OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> InserirAsync(MensagemIntegrador entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.MENSAGEM_INTEGRADOR
                              (GUID_MENSAGEM,
                               TEMPLATE_ID,
                               CONTEUDO,
                               DT_INCLUSAO,
                               MENSAGEM_INTEGRADOR_TIPO,
                               INTEGRADOR_TIPO,
                               STATUS_TIPO,
                               DESTINATARIOS,
                               DESTINATARIOS_COPIA,
                               DESTINATARIOS_COPIA_OCULTA,
                               DE)
                            VALUES
                              (:P_GUID_MENSAGEM,
                               :P_TEMPLATE_ID,
                               :P_CONTEUDO,
                               SYSDATE,
                               :P_MENSAGEM_INTEGRADOR_TIPO,
                               :P_INTEGRADOR_TIPO,
                               :P_STATUS_TIPO,
                               :P_DESTINATARIOS,
                               :P_DESTINATARIOS_COPIA,
                               :P_DESTINATARIOS_COPIA_OCULTA,
                               :P_DE)
                            RETURNING ID_MENSAGEM_INTEGRADOR INTO :V_ID_MENSAGEM_INTEGRADOR";

                OracleParameter.Add("P_GUID_MENSAGEM", entity.GuidMensagem, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_TEMPLATE_ID", !string.IsNullOrEmpty(entity.TemplateId) ? (object)entity.TemplateId : DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CONTEUDO", entity.Conteudo, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_MENSAGEM_INTEGRADOR_TIPO", (int)entity.Tipo, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_INTEGRADOR_TIPO", (int)entity.Integrador, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_STATUS_TIPO", (int)entity.Status, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_DESTINATARIOS", !string.IsNullOrEmpty(entity.Destinatarios) ? (object)entity.Destinatarios : DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DESTINATARIOS_COPIA", !string.IsNullOrEmpty(entity.DestinatariosCopia) ? (object)entity.DestinatariosCopia : DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DESTINATARIOS_COPIA_OCULTA", !string.IsNullOrEmpty(entity.DestinatariosCopiaOculta) ? (object)entity.DestinatariosCopiaOculta : DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DE", !string.IsNullOrEmpty(entity.De) ? (object)entity.De : DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);

                OracleParameter.Add("V_ID_MENSAGEM_INTEGRADOR", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_MENSAGEM_INTEGRADOR").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(MensagemIntegrador)} [template id:{entity.TemplateId}, destinatario(s):{entity.Destinatarios}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarAsync(MensagemIntegrador entity)
        {
            try
            {
                var sql = @"UPDATE NETUNO.MENSAGEM_INTEGRADOR MI
                               SET MI.GUID_MENSAGEM = :P_GUID_MENSAGEM, 
                                   MI.STATUS_TIPO = :P_STATUS_TIPO
                             WHERE MI.ID_MENSAGEM_INTEGRADOR = :P_ID_MENSAGEM_INTEGRADOR";

                OracleParameter.Add("P_GUID_MENSAGEM", entity.GuidMensagem, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_STATUS_TIPO", (int)entity.Status, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", entity.Id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar - {typeof(MensagemIntegrador)} [id:{entity?.Id}, guid:{entity?.GuidMensagem}, status:{entity?.Status}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        
    }
}
