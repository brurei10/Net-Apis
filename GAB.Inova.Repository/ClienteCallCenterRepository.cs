﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ClienteCallCenterRepository
        : DapperRepository, IClienteCallCenterRepository
    {

        readonly ILogger _logger;

        public ClienteCallCenterRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ClienteCallCenterRepository>();
        }

        public async Task<ClienteCallCenter> ObterAsync(string protocolo, string documento, string matricula)
        {
            try
            {
                OracleParameter.Add("P_PROTOCOLO", protocolo, OracleDbType.Varchar2, ParameterDirection.Input);

                if (!string.IsNullOrEmpty(documento))
                {
                    OracleParameter.Add("P_DOCUMENTO", documento, OracleDbType.Varchar2, ParameterDirection.Input);
                }
                else
                {
                    OracleParameter.Add("P_DOCUMENTO", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                }

                if (!string.IsNullOrEmpty(matricula))
                {
                    OracleParameter.Add("P_NUMLIGACAO", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                }
                else
                {
                    OracleParameter.Add("P_NUMLIGACAO", DBNull.Value, OracleDbType.Varchar2, ParameterDirection.Input);
                }

                OracleParameter.Add("P_OUT_CLIENTE", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
                OracleParameter.Add("P_OUT_LIGACOES", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

                await DbConn.ExecuteAsync("PACK_CALL_CENTER.OBTER_CLIENTE", param: OracleParameter, commandType: CommandType.StoredProcedure);

                var outputCliente = OracleParameter.Get<OracleRefCursor>("P_OUT_CLIENTE");
                var outputLigacoes = OracleParameter.Get<OracleRefCursor>("P_OUT_LIGACOES");

                var dr = outputCliente.GetDataReader();

                var clienteCallCenter = new ClienteCallCenter();

                while (dr.Read())
                {
                    clienteCallCenter.nomeCliente = dr.GetString("nomeCliente");
                    clienteCallCenter.documento = dr.GetString("documento");
                    clienteCallCenter.possuiCelular = dr.GetInt16("possuiCelular");
                    clienteCallCenter.possuiEmail = dr.GetInt16("possuiEmail");

                    var drLigacoes = outputLigacoes.GetDataReader();

                    while (drLigacoes.Read())
                    {
                        var ligacaoCallCenter = new LigacaoCallCenter();

                        ligacaoCallCenter.numLigacao = drLigacoes.GetString("numLigacao");
                        ligacaoCallCenter.flgLigacaoEspecial = drLigacoes.GetInt16("flgLigacaoEspecial");
                        ligacaoCallCenter.flgInterrupcaoAbast = drLigacoes.GetInt16("flgInterrupcaoAbast");
                        ligacaoCallCenter.previsaoRetorno = drLigacoes.IsDBNull("previsaoRetorno") ? (int?)null : drLigacoes.GetInt16("previsaoRetorno");
                        ligacaoCallCenter.flgTipoClienteGrande = drLigacoes.GetInt16("flgTipoClienteGrande");

                        clienteCallCenter.ligacoes.Add(ligacaoCallCenter);
                    }
                }

                return clienteCallCenter;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter cliente - {typeof(ClienteCallCenter)} [protocolo:{protocolo}, documento:{documento}, matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
