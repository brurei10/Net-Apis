﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class CompoeCtRepository: DapperRepository, ICompoeCtRepository
    {

        readonly ILogger _logger;

        public CompoeCtRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<CompoeCtRepository>();
        }

        public async Task<bool> InserirAsync(CompoeCt entity)
        {
            try
            {
                var sql = @"INSERT 
                              INTO COMPOE_CT (DOC_PAGO, 
                                              DOC_ORIGINAL, 
                                              VL_DO_MES, 
                                              TIPO_CT_DOC_PAGO) 
                                      VALUES (:P_DOCPAGO,
                                              :P_DOCORIGINAL, 
                                              :P_VLDOMES, 
                                              :P_TIPOCTDOCPAGO)";

                OracleParameter.Add("P_DOCPAGO", entity.DocumentoPago, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_DOCORIGINAL", entity.DocumentoOriginal, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_VLDOMES", entity.Valor, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_TIPOCTDOCPAGO", entity.TipoContaDocumentoPago, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(CompoeCt)} [documento pago:{entity.DocumentoPago}, documento original:{entity.DocumentoOriginal}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
