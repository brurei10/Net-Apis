﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class FaturamentoRepository
        : DapperRepository, IFaturamentoRepository
    {

        readonly ILogger _logger;

        public FaturamentoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<FaturamentoRepository>();
        }

        public async Task<IEnumerable<Faturamento>> ObterFaturamentoParaImpressaoContaAsync(Faturamento faturamento)
        {
            try
            {
                var sql = @"SELECT F.ID_FATURAMENTO,
                                   F.ANO_MES_FATURAMENTO,
                                   F.MATRICULA,
                                   F.CATEGORIA,
                                   F.SEQ_ORIGINAL,
                                   F.VOL_POR_ECO_FAT,
                                   F.VOL_TOTAL_FAT,
                                   F.TIPO_SERV_FATURAMENTO,
                                   F.VL_FATURAMENTO
                              FROM NETUNO.FATURAMENTO F
                             WHERE F.MATRICULA = :P_MATRICULA
                               AND F.CATEGORIA = :P_CATEGORIA
                               AND F.SEQ_ORIGINAL = :P_SEQ_ORIGINAL
                               AND F.ANO_MES_FATURAMENTO = :P_ANO_MES_FATURAMENTO
                             ORDER BY F.SEQ_ORIGINAL DESC, F.TIPO_SERV_FATURAMENTO ASC";

                OracleParameter.Add("P_MATRICULA", faturamento.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_CATEGORIA", faturamento.Categoria, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_SEQ_ORIGINAL", faturamento.SeqOriginal, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ANO_MES_FATURAMENTO", faturamento.AnoMesFaturamento, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<Faturamento>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Faturamento)} - para impressão de conta [seqOriginal:{faturamento.SeqOriginal}, categoria:{faturamento.Categoria}, anoMesFaturamento:{faturamento.AnoMesFaturamento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
