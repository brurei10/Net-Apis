﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.OrdemServicoDetalheSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class OrdemServicoDetalheRepository : DapperRepository, IOrdemServicoDetalheRepository
    {
        readonly ILogger _logger;

        public OrdemServicoDetalheRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<OrdemServicoDetalheRepository>();
        }

        public bool AlterarStatus(long NumeroOS, StatusDetOrdemServicoEnum novoStatus)
        {
            try
            {
                var sql = @"UPDATE NETUNO.DET_ORDEM_SERVICO SET SIT_OS = :P_SIT_OS WHERE NUM_OS = :P_NUM_OS";

                OracleParameter.Add("P_SIT_OS", (int)novoStatus, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_NUM_OS", NumeroOS, OracleDbType.Int64, ParameterDirection.Input);

                return DbConn.Execute(sql: sql, param: OracleParameter, commandType: CommandType.Text) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao alterar status da det {NumeroOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AlterarStatusAsync(long NumeroOS, StatusDetOrdemServicoEnum novoStatus)
        {
            try
            {
                var sql = @"UPDATE NETUNO.DET_ORDEM_SERVICO SET SIT_OS = :P_SIT_OS WHERE NUM_OS = :P_NUM_OS";

                OracleParameter.Add("P_SIT_OS", (int)novoStatus, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_NUM_OS", NumeroOS, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql: sql, param: OracleParameter, commandType: CommandType.Text) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao alterar status da det {NumeroOS}.");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public OrdemServicoDetalhe Obter(long NumeroOS)
        {
            try
            {
                var sql = new StringBuilder(ObterSqlOrdemServicoDetalhe());

                sql.Append(" WHERE NUM_OS = :P_NUM_OS");

                OracleParameter.Add("P_NUM_OS", NumeroOS, OracleDbType.Int64, ParameterDirection.Input);

                return DbConn.QueryFirstOrDefault<OrdemServicoDetalhe>(sql: sql.ToString(), param: OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<OrdemServicoDetalhe> ObterAsync(long NumeroOS)
        {
            try
            {
                var sql = new StringBuilder(ObterSqlOrdemServicoDetalhe());

                sql.Append(" WHERE NUM_OS = :P_NUM_OS");

                OracleParameter.Add("P_NUM_OS", NumeroOS, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<OrdemServicoDetalhe>(sql: sql.ToString(), param: OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public ValidationResult PodeAlterarStatusParaAguardandoApropriacao(OrdemServicoDetalhe entity)
        {
            return new OsDetalhePodeAlterarStatusAguardApropriacaoValidation(this).Validate(entity);
        }

        #region [Métodos privados]

        private string ObterSqlOrdemServicoDetalhe()
        {
            return @"SELECT NRO_OS, 
                            SEQ_OS, 
                            SERV_SOLICITADO_OS, 
                            SIT_OS, 
                            NUM_OS,
                            DATA_EXEC_PROG_CLIENTE,
                            DATA_PROG,
                            DATA_BAIXA
                       FROM NETUNO.DET_ORDEM_SERVICO";
        }

        #endregion
    }
}
