﻿using System.Data;

namespace GAB.Inova.Repository.Bases
{
    /// <summary>
    /// Classe a ser herdada por todos os repositorios dapper
    /// </summary>
    public abstract class DapperRepository
    {

        readonly IDbConnection _dbConn;
        
        /// <summary>
        /// Contrutor
        /// </summary>
        /// <param name="dbConnection">Indica uma conexão utilizada pelo contexto</param>
        protected DapperRepository(IDbConnection dbConnection)
        {
            _dbConn = dbConnection;
            OracleParameter = new OracleDynamicParameters();
        }

        protected readonly OracleDynamicParameters OracleParameter;

        protected IDbConnection DbConn
        {
            get
            {
                _dbConn?.Open();
                return _dbConn;
            }
        }

    }
}
