﻿using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using System.Data;

namespace GAB.Inova.Repository.Bases
{

    public sealed class UnitOfWork
        : IUnitOfWork
    {

        IDbConnection _dbConnection;
        IDbTransaction _dbTransaction;

        public UnitOfWork(IDbConnection dbConnection) 
        {
            _dbConnection = dbConnection;
        }

        #region [Implementações da interface IUnitOfWork]
        
        public void BeginTransaction()
        {
            _dbConnection?.Open();
            _dbTransaction = _dbConnection?.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                _dbTransaction?.Commit();
            }
            catch
            {
                _dbTransaction?.Rollback();
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            try
            {
                _dbTransaction?.Rollback();
            }
            finally
            {
                Dispose();
            }
        }

        public void Dispose()
        {
            _dbTransaction?.Dispose();
            _dbTransaction = null;
        }

        #endregion

    }
}
