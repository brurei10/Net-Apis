﻿using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using NHibernate;
using System.Linq;

namespace GAB.Inova.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {

        readonly IUnitOfWork _unitOfWork;
        
        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        protected ISession DbContext { get => _unitOfWork.GetSession<ISession>(); }

        public void Deletar(object id)
        {
            DbContext.Delete(DbContext.Load<T>(id));
        }

        public T Obter(object id)
        {
            return DbContext.Get<T>(id);
        }

        public IQueryable<T> ObterTodos()
        {
            return DbContext.Query<T>();
        }

        public void Persistir(T entity)
        {
            DbContext.SaveOrUpdate(entity);
            DbContext.Flush();
        }

    }
}
