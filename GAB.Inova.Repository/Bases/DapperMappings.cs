﻿using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;
using GAB.Inova.Repository.Mappings;

namespace GAB.Inova.Repository.Bases
{
    public static class DapperMappings
    {
        public static void Register()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new BairroMap());
                config.AddMap(new CidadeMap());
                config.AddMap(new ClienteMap());
                config.AddMap(new ClienteTokenMap());
                config.AddMap(new ConsumoMap());
                config.AddMap(new ContaDigitalEnvioMap());
                config.AddMap(new ContaDigitalEnvioMovimentoMap());
                config.AddMap(new ContaLocalizacaoMap());
                config.AddMap(new ContaMap());
                config.AddMap(new ContratoMap());
                config.AddMap(new ContratoMensagemIntegradorMap());
                config.AddMap(new EmpresaMap());
                config.AddMap(new EstadoMap());
                config.AddMap(new FaturamentoMap());
                config.AddMap(new GrupoEntregaMap());
                config.AddMap(new LogradouroMap());
                config.AddMap(new LogradouroTipoMap());
                config.AddMap(new MensagemIntegradorMap());
                config.AddMap(new MensagemIntegradorMovimentoMap());
                config.AddMap(new OrdemServicoAnexoExternoMap());
                config.AddMap(new OrdemServicoEletronicaControleMap());
                config.AddMap(new OrdemServicoEletronicaMap());
                config.AddMap(new ParametroSistemaMap());
                config.AddMap(new PdfGenItemDocAjustadoMap());
                config.AddMap(new PdfGenItemDocMap());
                config.AddMap(new PdfGenModeloDocMap());
                config.AddMap(new ProcessoClienteMap());
                config.AddMap(new ProcessoMap());
                config.AddMap(new ProtocoloAtendimentoDetalheMap());
                config.AddMap(new ProtocoloAtendimentoGfaMap());
                config.AddMap(new ProtocoloAtendimentoHistoricoMap());
                config.AddMap(new ProtocoloAtendimentoMap());
                config.AddMap(new TabAnaliseAguaMap());
                config.AddMap(new TabNaturezaSolicitacaoMap());
                config.AddMap(new TarifaDiariaMap());
                config.AddMap(new TempHistoricoConsumoMap());
                config.AddMap(new TempImprimeContaMap());
                config.AddMap(new UnidadeMedidaMap());
                config.AddMap(new UsuarioMap());
                config.AddMap(new VwGeraArquivoImpressaoMap());
                config.AddMap(new OrdemServicoDetalheMap());
                config.AddMap(new VwClienteMap());
                config.AddMap(new CompoeCtMap());
                config.AddMap(new GRPMap());
                config.AddMap(new ImprimeGRPMap());
                config.AddMap(new TempCobrancaCtMap());
                config.AddMap(new ParcelaMap());
                config.AddMap(new DocumentoModeloMap());
                config.AddMap(new InterrupcaoMap());
                config.AddMap(new VwDadosClienteInterrupcaoMap());
                config.AddMap(new VwDadosConsultaOrdemServicoMap());
                config.AddMap(new DeclaracaoAnualMap());
                config.AddMap(new VwDadosDebitoAnualMap());
                config.AddMap(new HistoricoLigacaoMap());
                config.AddMap(new HidrometroMap());
                config.AddMap(new LeituraMap());
                config.AddMap(new LeituraMacroMap());


                config.ForDommel();
            });
        }
    }
}
