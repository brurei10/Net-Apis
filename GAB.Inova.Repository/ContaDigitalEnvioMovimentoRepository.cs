﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ContaDigitalEnvioMovimentoRepository
        : DapperRepository, IContaDigitalEnvioMovimentoRepository
    {

        readonly ILogger _logger;

        public ContaDigitalEnvioMovimentoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
           : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContaDigitalEnvioRepository>();
        }

        public async Task<bool> InserirAsync(ContaDigitalEnvioMovimento entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.CONTA_DIGITAL_ENVIO_MOV
                              (ID_CONTA_DIGITAL_ENVIO, DATA_INCLUSAO, STATUS, MENSAGEM)
                            VALUES
                              (:P_ID_CONTA_DIGITAL_ENVIO, SYSDATE, :P_STATUS, :P_MENSAGEM)
                            RETURNING ID_CONTA_DIGITAL_ENVIO_MOV INTO :V_ID_CONTA_DIGITAL_ENVIO_MOV";

                OracleParameter.Add("P_ID_CONTA_DIGITAL_ENVIO", entity.IdContaDigitalEnvio, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_STATUS", (int)entity.Status, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_MENSAGEM", (string.IsNullOrEmpty(entity.Mensagem) ? DBNull.Value : (object)entity.Mensagem), OracleDbType.Varchar2, ParameterDirection.Input);

                OracleParameter.Add("V_ID_CONTA_DIGITAL_ENVIO_MOV", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_CONTA_DIGITAL_ENVIO_MOV").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ContaDigitalEnvioMovimento)} [id conta digital envio:{entity.IdContaDigitalEnvio}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
