﻿using Dapper;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ContratoRepository
        : DapperRepository, IContratoRepository
    {

        readonly ILogger _logger;

        public ContratoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
           : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContratoRepository>();
        }

        public async Task<Contrato> ObterPorMatriculaAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT CTR.ID_CONTRATO,
                                   CTR.CPF_CNPJ,
                                   CTR.MATRICULA,
                                   CTR.NOME_CLIENTE,
                                   CTR.TELEFONE_RES,
                                   CTR.TELEFONE_COM,
                                   CTR.TELEFONE_CEL,
                                   TRIM(LOWER(CTR.EMAIL)) AS EMAIL,
                                   CTR.NUMERO,
                                   CTR.COMPLEMENTO,
                                   CTR.PONTO_REFER,
                                   TRIM(LOWER(CTR.EMAIL_CONTA_DIGITAL)) AS EMAIL_CONTA_DIGITAL,
                                   L.ID_LOGRADOURO,
                                   L.LOGRADOURO_CEP,
                                   L.NOME_LOGRADOURO,
                                   LT.LTP_ID,
                                   LT.NOME_LOGRADOURO_TIPO,
                                   B.ID_BAIRRO,
                                   B.NOME_BAIRRO,
                                   C.ID_CIDADE,
                                   C.NOME_CIDADE,
                                   E.ID_ESTADO,
                                   E.NOME_ESTADO
                              FROM NETUNO.CONTRATO CTR
                              JOIN NETUNO.LOGRADOURO L
                                ON L.ID_LOGRADOURO = CTR.ID_LOGRADOURO
                              JOIN NETUNO.LOGRADOURO_TIPO LT
                                ON LT.LTP_ID = L.LTP_ID
                              JOIN NETUNO.BAIRRO B
                                ON B.ID_BAIRRO = L.ID_BAIRRO
                              JOIN NETUNO.CIDADE C
                                ON C.ID_CIDADE = L.ID_CIDADE
                              JOIN NETUNO.ESTADO E
                                ON E.ID_ESTADO = C.ID_ESTADO
                             WHERE CTR.ID_SITUACAO = 1
                               AND CTR.MATRICULA = :P_MATRICULA";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                var contratos = await DbConn.QueryAsync<Contrato, Logradouro, LogradouroTipo, Bairro, Cidade, Estado, Contrato>(sql,
                    map: (contrato, logradouro, logradouroTipo, bairro, cidade, estado) =>
                    {
                        logradouro.Bairro = bairro;
                        logradouro.Cidade = cidade;
                        logradouro.Estado = estado;
                        logradouro.LogradouroTipo = logradouroTipo;

                        contrato.Logradouro = logradouro;

                        return contrato;
                    },
                    param: OracleParameter,
                    splitOn: "ID_LOGRADOURO,LTP_ID,ID_BAIRRO,ID_CIDADE,ID_ESTADO"
                );

                return contratos.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Contrato)} [matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarEmailsAsync(Contrato entity)
        {
            try
            {
                var sql = @"UPDATE NETUNO.CONTRATO CTR
                               SET CTR.EMAIL               = :P_EMAIL,
                                   CTR.EMAIL_CONTA_DIGITAL = :P_EMAIL_CONTA_DIGITAL
                             WHERE CTR.MATRICULA = :P_MATRICULA";

                OracleParameter.Add("P_EMAIL", entity.Email, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_EMAIL_CONTA_DIGITAL", entity.EmailContaDigital, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar e-mail(s) - {typeof(Contrato)} [matricula:{entity.Matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
