﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class VwDadosClienteInterrupcaoRepository : DapperRepository, IVwDadosClienteInterrupcaoRepository
    {

        readonly ILogger _logger;

        public VwDadosClienteInterrupcaoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<VwDadosClienteInterrupcaoRepository>();
        }

        public async Task<VwDadosClienteInterrupcao> ObterAsync(string matricula)
        {
            try
            {
                var sql = @"SELECT *
                              FROM VW_DDS_CLNT_INTRRPC
                             WHERE MATRICULA = :P_MATRICULA";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<VwDadosClienteInterrupcao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwDadosClienteInterrupcao)} [matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
