﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.MunicipioSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;

namespace GAB.Inova.Repository
{
    public class MunicipioRepository
        : DapperRepository, IMunicipioRepository
    {

        readonly ILogger _logger;

        public MunicipioRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<UsuarioRepository>();
        }

        public bool VerificarExistencia(Municipio municipio)
        {
            try
            {
                var sql = GetSelectSQL("WHERE MUN.COD_MUNICIPIO = :P_COD_MUNICIPIO");

                OracleParameter.Add("P_COD_MUNICIPIO", municipio.Codigo, OracleDbType.Varchar2, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Municipio)} [id:{municipio.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        #region [Métodos privados]

        private string GetSelectSQL(string where)
        {
            var builder = new StringBuilder(@"SELECT MUN.ID_MUNICIPIO,
                                                     MUN.COD_MUNICIPIO,
                                                     MUN.NOME_MUNICIPIO
                                                FROM NETUNO.MUNICIPIO MUN ");

            if (!string.IsNullOrEmpty(where))
            {
                builder.AppendLine(where);
            }

            return builder.ToString();
        }

        #endregion

        #region [Métodos públicos para validação]

        public ValidationResult MunicipioValido(Municipio entity)
        {
            try
            {
                return new MunicipioValidoValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar município Codigo: {entity?.Codigo}.");

                throw;
            }
        }

        #endregion

    }
}
