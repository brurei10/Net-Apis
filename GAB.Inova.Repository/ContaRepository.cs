﻿using Dapper;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ContaRepository: DapperRepository, IContaRepository
    {
        readonly ILogger _logger;

        public ContaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContaRepository>();
        }

        /// <summary>
        /// Obter as conta pelo numero da conta (sequência original)
        /// </summary>
        /// <param name="seqOriginal">Indica uma chave identificadora de uma conta</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>uma conta</returns>
        public async Task<Conta> ObterContaPorSeqOriginalAsync(long seqOriginal, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal)
        {
            try
            {
                var sql = new StringBuilder(ObterSqlContas());

                sql.Append("WHERE CTA.SEQ_ORIGINAL = :P_SEQ_ORIGINAL ");
                sql.Append("AND CTA.TIPO_CT = :P_TIPO_CT ");
                sql.Append("ORDER BY CTA.ANO_MES_CT DESC");

                OracleParameter.Add("P_SEQ_ORIGINAL", seqOriginal, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_CT", (int)tipoConta, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Conta>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Conta)} [SeqOriginal:{seqOriginal}, TipoConta:{tipoConta.GetDescription()}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        /// <summary>
        /// Obter as contas pelo numeros das contas (sequência original)
        /// </summary>
        /// <param name="seqOriginal">Indica uma lista de chaves identificadoras de contas</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>uma conta</returns>
        public async Task<IEnumerable<Conta>> ObterContasPorSeqOriginalAsync(IEnumerable<long> seqOriginais, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal)
        {
            try
            {
                var sql = new StringBuilder(ObterSqlContas());

                sql.Append($"WHERE CTA.SEQ_ORIGINAL IN({string.Join(", ", seqOriginais)}) ");
                sql.Append("AND CTA.TIPO_CT = :P_TIPO_CT ");

                OracleParameter.Add("P_TIPO_CT", (int)tipoConta, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryAsync<Conta>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Conta)} [SeqOriginal:{string.Join(", ", seqOriginais)}, TipoConta:{tipoConta.GetDescription()}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        /// <summary>
        /// Obter as contas com situação em aberto pelo contrato do cliente
        /// </summary>
        /// <param name="idContrato">Indica uma chave identificadora de um contrato</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>Uma lista de contas</returns>
        public async Task<IEnumerable<Conta>> ObterContasEmAbertoPorContratoAsync(long idContrato, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal)
        {
            try
            {
                var sql = new StringBuilder(ObterSqlContas());

                sql.Append("WHERE CTA.ID_CONTRATO = :P_ID_CONTRATO ");
                sql.Append("AND CTA.TIPO_CT = :P_TIPO_CT ");
                sql.Append("AND CTA.SITUACAO_CT = 0 ");
                sql.Append("AND CASE WHEN CLI.VENC_CONTA IS NULL THEN  CTA.DATA_VENC ");
                sql.Append("WHEN CLI.VENC_CONTA > TO_NUMBER(TO_CHAR(LAST_DAY(CTA.DATA_VENC), 'DD')) THEN TO_DATE(TO_CHAR(CTA.DATA_VENC, 'RRRRMM') || TO_CHAR(LAST_DAY(CTA.DATA_VENC), 'DD'), 'RRRRMMDD') ");
                sql.Append("ELSE TO_DATE(TO_CHAR(CTA.DATA_VENC, 'RRRRMM') || LPAD(CLI.VENC_CONTA, 2, '0'), 'RRRRMMDD') END < TRUNC(SYSDATE) ");
                sql.Append("ORDER BY CTA.ANO_MES_CT DESC");

                OracleParameter.Add("P_ID_CONTRATO", idContrato, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_CT", (int)tipoConta, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryAsync<Conta>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Conta)} [IdContrato:{idContrato}, TipoConta:{tipoConta.GetDescription()}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        /// <summary>
        /// Obter as últimas 12 contas com situação: em aberto ou pagas
        /// </summary>
        /// <param name="idContrato">Indica uma chave identificadora de um contrato</param>
        /// <param name="tipoConta">Indica o tipo da conta [0 = NF (Conta Normal); 1 = Aviso de Débito; 2 = DNF - Documento Não Fiscal (Quitação de Débito); 3 = Fatura; 4 = NFA (Nota Fiscal Avulsa)]</param>
        /// <returns>Uma lista de contas</returns>
        public async Task<IEnumerable<Conta>> ObterUltimasDozeContasEmAbertoOuPagasPorContratoAsync(long idContrato, TipoContaEnum tipoConta = TipoContaEnum.ContaNormal)
        {
            try
            {
                var subSql = new StringBuilder(ObterSqlContas());

                subSql.Append("WHERE CTA.ID_CONTRATO = :P_ID_CONTRATO ");
                subSql.Append("AND CTA.TIPO_CT = :P_TIPO_CT ");
                subSql.Append("AND CTA.SITUACAO_CT IN (0, 1, 2, 3) ");
                subSql.Append("ORDER BY CTA.ANO_MES_CT DESC");

                var sql = $"SELECT * FROM ({subSql.ToString()}) WHERE ROWNUM <= 12";

                OracleParameter.Add("P_ID_CONTRATO", idContrato, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_TIPO_CT", (int)tipoConta, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.QueryAsync<Conta>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter as últimas doze - {typeof(Conta)} - com situação em aberto ou paga [IdContato:{idContrato}, TipoConta:{tipoConta.GetDescription()}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<ContaLocalizacao> ObterContaLocalizacaoAsync(long seqOriginal)
        {
            try
            {
                var sql = @"SELECT CTA.SEQ_ORIGINAL,
                                   (SELECT E.SIGLA FROM EMPRESA E) AS SIGLA_EMPRESA,
                                   NVL(CTA.DATA_EMISSAO, CTA.DATA_FAT) AS DATA_EMISSSAO,
                                   CTA.ID_CICLO,
                                   LPAD(NVL(HL.ROTA, '0'), 3, '0') AS ROTA,
                                   CTA.SITUACAO_CT
                              FROM NETUNO.CONTA CTA
                              LEFT JOIN NETUNO.HISTORICO_LIGACAO HL
                                ON HL.ANO_MES = CTA.ANO_MES_CT
                               AND HL.MATRICULA = CTA.MATRICULA
                             WHERE CTA.SEQ_ORIGINAL = :P_SEQ_ORGINAL";

                OracleParameter.Add("P_SEQ_ORGINAL", seqOriginal, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ContaLocalizacao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(ContaLocalizacao)} [seqoriginal:{seqOriginal}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<VwGeraArquivoImpressao> ObterArquivoImpressaoAsync(long seqOriginal, long idUsuario)
        {
            try
            {
                #region [SQL Query]

                var sql = @"SELECT T.SEQ_ORIGINAL,
                                   T.MATRICULA,
                                   T.NOME_CLIENTE,
                                   T.ENDERECO,
                                   T.COMPLEMENTO,
                                   (SUBSTR(T.ANO_MES_CT, 4, 4) || SUBSTR(T.ANO_MES_CT, 1, 2)) AS ANO_MES_CT,
                                   T.DATA_EMISSAO,
                                   T.DATA_VENC,
                                   T.ID_HD,
                                   T.LEITURA_ATUAL,
                                   T.LEITURA_ANT,
                                   T.DATA_LEIT_ATUAL,
                                   T.DATA_LEIT_ANT,
                                   T.DATA_PROX_LEIT,
                                   T.NOME_TIPO_ENTREGA,
                                   T.CPF_CNPJ,
                                   T.INSC_MUN_EST,
                                   T.ECO_RES,
                                   T.ECO_COM,
                                   T.ECO_IND,
                                   T.ECO_PUB,
                                   T.QTDE_DIAS_CONS,
                                   T.CONS_MEDIDO,
                                   T.CONS_CREDITO,
                                   T.CONS_PIPA,
                                   T.CONS_RESIDUAL,
                                   T.CONS_FAT,
                                   T.NOME_TIPO_FATURAMENTO,
                                   T.COMP_CT_01,
                                   T.COMP_CT_02,
                                   T.COMP_CT_03,
                                   T.COMP_CT_04,
                                   T.COMP_CT_05,
                                   T.COMP_CT_06,
                                   T.COMP_CT_07,
                                   T.COMP_CT_08,
                                   T.COMP_CT_09,
                                   T.COMP_CT_10,
                                   T.COMP_VL_01,
                                   T.COMP_VL_02,
                                   T.COMP_VL_03,
                                   T.COMP_VL_04,
                                   T.COMP_VL_05,
                                   T.COMP_VL_06,
                                   T.COMP_VL_07,
                                   T.COMP_VL_08,
                                   T.COMP_VL_09,
                                   T.COMP_VL_10,
                                   T.VL_ESGOTO,
                                   T.VL_TOTAL,
                                   T.VL_RET_IMPOSTO,
                                   T.IMP_RETENCAO_1,
                                   T.MENSAGEM,
                                   T.OBSERVACAO,
                                   T.COD_BARRA,
                                   T.NOME_USUARIO,
                                   T.IMP_COD_BARRA,
                                   T.ID_SOLICITACAO,
                                   T.ID_USUARIO,
                                   T.ID_TARIFA,
                                   T.SIT_HD,
                                   T.SIT_ESGOTO,
                                   T.ROTEIRIZACAO,
                                   T.SISTEMA_ABASTECIMENTO,
                                   T.TIPO_IMPRESSAO,
                                   T.DV_MATR,
                                   T.COMP_CT_11,
                                   T.COMP_CT_12,
                                   T.COMP_CT_13,
                                   T.COMP_CT_14,
                                   T.COMP_CT_15,
                                   T.COMP_CT_16,
                                   T.COMP_VL_11,
                                   T.COMP_VL_12,
                                   T.COMP_VL_13,
                                   T.COMP_VL_14,
                                   T.COMP_VL_15,
                                   T.COMP_VL_16,
                                   T.RECURSO_HIDRICO,
                                   T.ESGOTO_ALTERNATIVO,
                                   T.ID_SISTEMA_ABASTECIMENTO,
                                   T.PERC_AGUA,
                                   T.PERC_ESGOTO,
                                   T.ROTA,
                                   T.VL_AGUA,
                                   T.ECONOMIA,
                                   T.IMPDADOSCONSUMO,
                                   T.MODO_FAT,
                                   T.ROTA_ORDEM,
                                   T.CEP_ORDEM,
                                   T.MENSAGEM_PADRAO_01,
                                   T.MENSAGEM_PADRAO_02,
                                   T.MENSAGEM_PADRAO_03,
                                   T.MENSAGEM_PADRAO_04,
                                   T.MENSAGEM_PADRAO_05,
                                   T.MENSAGEM_PADRAO_06,
                                   T.MENSAGEM_PADRAO_07,
                                   T.MENSAGEM_PADRAO_08,
                                   T.MENSAGEM_PADRAO_09,
                                   T.MENSAGEM_PADRAO_10,
                                   T.MENSAGEM_DEB_ANT_01,
                                   T.MENSAGEM_DEB_ANT_02,
                                   T.MENSAGEM_DEB_ANT_03,
                                   T.MENSAGEM_DEB_ANT_04,
                                   T.MENSAGEM_DEB_ANT_05,
                                   T.MENSAGEM_DEB_ANT_06,
                                   T.MENSAGEM_DEB_ANT_07,
                                   T.MENSAGEM_DEB_ANT_08,
                                   T.MENSAGEM_DEB_ANT_09,
                                   T.MENSAGEM_DEB_ANT_10,
                                   T.MENSAGEM_DEB_ANT_11,
                                   T.MENSAGEM_DEB_ANT_12,
                                   T.MENSAGEM_DEB_ANT_13,
                                   T.MENSAGEM_DEB_ANT_14,
                                   T.MENSAGEM_DEB_ANT_15,
                                   T.MENSAGEM_DEB_ANT_16,
                                   T.MENSAGEM_DEB_ANT_17,
                                   T.MENSAGEM_DEB_ANT_18,
                                   T.MENSAGEM_DEB_ANT_19,
                                   T.MENSAGEM_DEB_ANT_20,
                                   T.MENSAGEM_DEB_ANT_21,
                                   T.MENSAGEM_DEB_ANT_22,
                                   T.MENSAGEM_DEB_ANT_23,
                                   T.MENSAGEM_DEB_ANT_24,
                                   T.MENSAGEM_DEB_ANT_25,
                                   T.MENSAGEM_DEB_ANT_26,
                                   T.MENSAGEM_DEB_ANT_27,
                                   T.MENSAGEM_DEB_ANT_28,
                                   T.MENSAGEM_DEB_ANT_29,
                                   T.MENSAGEM_DEB_ANT_30,
                                   T.FAIXA_ECONOMIA_01,
                                   T.FAIXA_CONSUMO_01,
                                   T.FAIXA_CONS_FATURADO_01,
                                   T.FAIXA_VL_TARIFA_01,
                                   T.FAIXA_VL_TARIFA_ESG_01,
                                   T.FAIXA_ECONOMIA_02,
                                   T.FAIXA_CONSUMO_02,
                                   T.FAIXA_CONS_FATURADO_02,
                                   T.FAIXA_VL_TARIFA_02,
                                   T.FAIXA_VL_TARIFA_ESG_02,
                                   T.FAIXA_ECONOMIA_03,
                                   T.FAIXA_CONSUMO_03,
                                   T.FAIXA_CONS_FATURADO_03,
                                   T.FAIXA_VL_TARIFA_03,
                                   T.FAIXA_VL_TARIFA_ESG_03,
                                   T.FAIXA_ECONOMIA_04,
                                   T.FAIXA_CONSUMO_04,
                                   T.FAIXA_CONS_FATURADO_04,
                                   T.FAIXA_VL_TARIFA_04,
                                   T.FAIXA_VL_TARIFA_ESG_04,
                                   T.FAIXA_ECONOMIA_05,
                                   T.FAIXA_CONSUMO_05,
                                   T.FAIXA_CONS_FATURADO_05,
                                   T.FAIXA_VL_TARIFA_05,
                                   T.FAIXA_VL_TARIFA_ESG_05,
                                   T.FAIXA_ECONOMIA_06,
                                   T.FAIXA_CONSUMO_06,
                                   T.FAIXA_CONS_FATURADO_06,
                                   T.FAIXA_VL_TARIFA_06,
                                   T.FAIXA_VL_TARIFA_ESG_06,
                                   T.FAIXA_ECONOMIA_07,
                                   T.FAIXA_CONSUMO_07,
                                   T.FAIXA_CONS_FATURADO_07,
                                   T.FAIXA_VL_TARIFA_07,
                                   T.FAIXA_VL_TARIFA_ESG_07,
                                   T.FAIXA_ECONOMIA_08,
                                   T.FAIXA_CONSUMO_08,
                                   T.FAIXA_CONS_FATURADO_08,
                                   T.FAIXA_VL_TARIFA_08,
                                   T.FAIXA_VL_TARIFA_ESG_08,
                                   T.FAIXA_ECONOMIA_09,
                                   T.FAIXA_CONSUMO_09,
                                   T.FAIXA_CONS_FATURADO_09,
                                   T.FAIXA_VL_TARIFA_09,
                                   T.FAIXA_VL_TARIFA_ESG_09,
                                   T.FAIXA_ECONOMIA_10,
                                   T.FAIXA_CONSUMO_10,
                                   T.FAIXA_CONS_FATURADO_10,
                                   T.FAIXA_VL_TARIFA_10,
                                   T.FAIXA_VL_TARIFA_ESG_10,
                                   T.LOTE_AVISO_DEBITO,
                                   T.NUMERO_AVISO_DEBITO,
                                   T.VALOR_AVISO_DEBITO,
                                   T.COD_BARRA_AVISO_DEBITO,
                                   T.TEXTO_MENSAGEM_AVISO,
                                   T.PERC_CARGA_TRIBUTARIA,
                                   TO_NUMBER(SUBSTR(T.VIA, 0, 1)) AS VIA,
                                   T.SITUACAO_CT,
                                   T.SIGLA_EMPRESA
                              FROM NETUNO.VW_GERA_ARQUIVO_IMPRESSAO T
                             WHERE T.ID_USUARIO = :P_ID_USUARIO
                               AND T.SEQ_ORIGINAL = :P_SEQ_ORIGINAL
                             GROUP BY T.SEQ_ORIGINAL,
                                      T.MATRICULA,
                                      T.NOME_CLIENTE,
                                      T.ENDERECO,
                                      T.COMPLEMENTO,
                                      T.ANO_MES_CT,
                                      T.DATA_EMISSAO,
                                      T.DATA_VENC,
                                      T.ID_HD,
                                      T.LEITURA_ATUAL,
                                      T.LEITURA_ANT,
                                      T.DATA_LEIT_ATUAL,
                                      T.DATA_LEIT_ANT,
                                      T.DATA_PROX_LEIT,
                                      T.NOME_TIPO_ENTREGA,
                                      T.CPF_CNPJ,
                                      T.INSC_MUN_EST,
                                      T.ECO_RES,
                                      T.ECO_COM,
                                      T.ECO_IND,
                                      T.ECO_PUB,
                                      T.QTDE_DIAS_CONS,
                                      T.CONS_MEDIDO,
                                      T.CONS_CREDITO,
                                      T.CONS_PIPA,
                                      T.CONS_RESIDUAL,
                                      T.CONS_FAT,
                                      T.NOME_TIPO_FATURAMENTO,
                                      T.COMP_CT_01,
                                      T.COMP_CT_02,
                                      T.COMP_CT_03,
                                      T.COMP_CT_04,
                                      T.COMP_CT_05,
                                      T.COMP_CT_06,
                                      T.COMP_CT_07,
                                      T.COMP_CT_08,
                                      T.COMP_CT_09,
                                      T.COMP_CT_10,
                                      T.COMP_VL_01,
                                      T.COMP_VL_02,
                                      T.COMP_VL_03,
                                      T.COMP_VL_04,
                                      T.COMP_VL_05,
                                      T.COMP_VL_06,
                                      T.COMP_VL_07,
                                      T.COMP_VL_08,
                                      T.COMP_VL_09,
                                      T.COMP_VL_10,
                                      T.VL_ESGOTO,
                                      T.VL_TOTAL,
                                      T.VL_RET_IMPOSTO,
                                      T.IMP_RETENCAO_1,
                                      T.MENSAGEM,
                                      T.OBSERVACAO,
                                      T.COD_BARRA,
                                      T.NOME_USUARIO,
                                      T.IMP_COD_BARRA,
                                      T.ID_SOLICITACAO,
                                      T.ID_USUARIO,
                                      T.ID_TARIFA,
                                      T.SIT_HD,
                                      T.SIT_ESGOTO,
                                      T.ROTEIRIZACAO,
                                      T.SISTEMA_ABASTECIMENTO,
                                      T.TIPO_IMPRESSAO,
                                      T.DV_MATR,
                                      T.COMP_CT_11,
                                      T.COMP_CT_12,
                                      T.COMP_CT_13,
                                      T.COMP_CT_14,
                                      T.COMP_CT_15,
                                      T.COMP_CT_16,
                                      T.COMP_VL_11,
                                      T.COMP_VL_12,
                                      T.COMP_VL_13,
                                      T.COMP_VL_14,
                                      T.COMP_VL_15,
                                      T.COMP_VL_16,
                                      T.RECURSO_HIDRICO,
                                      T.ESGOTO_ALTERNATIVO,
                                      T.ID_SISTEMA_ABASTECIMENTO,
                                      T.PERC_AGUA,
                                      T.PERC_ESGOTO,
                                      T.ROTA,
                                      T.VL_AGUA,
                                      T.ECONOMIA,
                                      T.IMPDADOSCONSUMO,
                                      T.MODO_FAT,
                                      T.ROTA_ORDEM,
                                      T.CEP_ORDEM,
                                      T.MENSAGEM_PADRAO_01,
                                      T.MENSAGEM_PADRAO_02,
                                      T.MENSAGEM_PADRAO_03,
                                      T.MENSAGEM_PADRAO_04,
                                      T.MENSAGEM_PADRAO_05,
                                      T.MENSAGEM_PADRAO_06,
                                      T.MENSAGEM_PADRAO_07,
                                      T.MENSAGEM_PADRAO_08,
                                      T.MENSAGEM_PADRAO_09,
                                      T.MENSAGEM_PADRAO_10,
                                      T.MENSAGEM_DEB_ANT_01,
                                      T.MENSAGEM_DEB_ANT_02,
                                      T.MENSAGEM_DEB_ANT_03,
                                      T.MENSAGEM_DEB_ANT_04,
                                      T.MENSAGEM_DEB_ANT_05,
                                      T.MENSAGEM_DEB_ANT_06,
                                      T.MENSAGEM_DEB_ANT_07,
                                      T.MENSAGEM_DEB_ANT_08,
                                      T.MENSAGEM_DEB_ANT_09,
                                      T.MENSAGEM_DEB_ANT_10,
                                      T.MENSAGEM_DEB_ANT_11,
                                      T.MENSAGEM_DEB_ANT_12,
                                      T.MENSAGEM_DEB_ANT_13,
                                      T.MENSAGEM_DEB_ANT_14,
                                      T.MENSAGEM_DEB_ANT_15,
                                      T.MENSAGEM_DEB_ANT_16,
                                      T.MENSAGEM_DEB_ANT_17,
                                      T.MENSAGEM_DEB_ANT_18,
                                      T.MENSAGEM_DEB_ANT_19,
                                      T.MENSAGEM_DEB_ANT_20,
                                      T.MENSAGEM_DEB_ANT_21,
                                      T.MENSAGEM_DEB_ANT_22,
                                      T.MENSAGEM_DEB_ANT_23,
                                      T.MENSAGEM_DEB_ANT_24,
                                      T.MENSAGEM_DEB_ANT_25,
                                      T.MENSAGEM_DEB_ANT_26,
                                      T.MENSAGEM_DEB_ANT_27,
                                      T.MENSAGEM_DEB_ANT_28,
                                      T.MENSAGEM_DEB_ANT_29,
                                      T.MENSAGEM_DEB_ANT_30,
                                      T.FAIXA_ECONOMIA_01,
                                      T.FAIXA_CONSUMO_01,
                                      T.FAIXA_CONS_FATURADO_01,
                                      T.FAIXA_VL_TARIFA_01,
                                      T.FAIXA_VL_TARIFA_ESG_01,
                                      T.FAIXA_ECONOMIA_02,
                                      T.FAIXA_CONSUMO_02,
                                      T.FAIXA_CONS_FATURADO_02,
                                      T.FAIXA_VL_TARIFA_02,
                                      T.FAIXA_VL_TARIFA_ESG_02,
                                      T.FAIXA_ECONOMIA_03,
                                      T.FAIXA_CONSUMO_03,
                                      T.FAIXA_CONS_FATURADO_03,
                                      T.FAIXA_VL_TARIFA_03,
                                      T.FAIXA_VL_TARIFA_ESG_03,
                                      T.FAIXA_ECONOMIA_04,
                                      T.FAIXA_CONSUMO_04,
                                      T.FAIXA_CONS_FATURADO_04,
                                      T.FAIXA_VL_TARIFA_04,
                                      T.FAIXA_VL_TARIFA_ESG_04,
                                      T.FAIXA_ECONOMIA_05,
                                      T.FAIXA_CONSUMO_05,
                                      T.FAIXA_CONS_FATURADO_05,
                                      T.FAIXA_VL_TARIFA_05,
                                      T.FAIXA_VL_TARIFA_ESG_05,
                                      T.FAIXA_ECONOMIA_06,
                                      T.FAIXA_CONSUMO_06,
                                      T.FAIXA_CONS_FATURADO_06,
                                      T.FAIXA_VL_TARIFA_06,
                                      T.FAIXA_VL_TARIFA_ESG_06,
                                      T.FAIXA_ECONOMIA_07,
                                      T.FAIXA_CONSUMO_07,
                                      T.FAIXA_CONS_FATURADO_07,
                                      T.FAIXA_VL_TARIFA_07,
                                      T.FAIXA_VL_TARIFA_ESG_07,
                                      T.FAIXA_ECONOMIA_08,
                                      T.FAIXA_CONSUMO_08,
                                      T.FAIXA_CONS_FATURADO_08,
                                      T.FAIXA_VL_TARIFA_08,
                                      T.FAIXA_VL_TARIFA_ESG_08,
                                      T.FAIXA_ECONOMIA_09,
                                      T.FAIXA_CONSUMO_09,
                                      T.FAIXA_CONS_FATURADO_09,
                                      T.FAIXA_VL_TARIFA_09,
                                      T.FAIXA_VL_TARIFA_ESG_09,
                                      T.FAIXA_ECONOMIA_10,
                                      T.FAIXA_CONSUMO_10,
                                      T.FAIXA_CONS_FATURADO_10,
                                      T.FAIXA_VL_TARIFA_10,
                                      T.FAIXA_VL_TARIFA_ESG_10,
                                      T.LOTE_AVISO_DEBITO,
                                      T.NUMERO_AVISO_DEBITO,
                                      T.VALOR_AVISO_DEBITO,
                                      T.COD_BARRA_AVISO_DEBITO,
                                      T.TEXTO_MENSAGEM_AVISO,
                                      T.PERC_CARGA_TRIBUTARIA,
                                      T.VIA,
                                      T.SITUACAO_CT,
                                      T.SIGLA_EMPRESA
                             ORDER BY T.ROTA_ORDEM";

                #endregion

                OracleParameter.Add("P_ID_USUARIO", idUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_SEQ_ORIGINAL", seqOriginal, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<VwGeraArquivoImpressao>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwGeraArquivoImpressao)} [seqOriginal:{seqOriginal}, idUsuario:{idUsuario}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        /// <summary>
        /// Obter o dígito do código de barras
        /// </summary>
        /// <param name="codigoBarras">Indica o codigo de barras de uma conta (barcode43)</param>
        /// <returns>uma string contendo o codigo de barras com os dígitos (barCode48)</returns>
        public async Task<string> ObterDigitoCodigoBarrasAsync(string codigoBarras)
        {
            try
            {
                var sql = @"SELECT F_BARCODE48(:P_CODIGO_43) AS BARCODE48 FROM DUAL";

                OracleParameter.Add("P_CODIGO_43", codigoBarras, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<string>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter digitos de [barCode43:{codigoBarras}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        /// <summary>
        /// Obter um novo número (SeqOriginal) para um documento não fiscal (DNF)
        /// </summary>
        /// <returns>o novo número para o documento não fiscal (DNF)</returns>
        public async Task<long> ObterNovoSeqOriginalDnfAsync()
        {
            try
            {
                var sql = @"SELECT SEQ_DOC_NAO_FISCAL.NEXTVAL FROM DUAL";

                return await DbConn.QueryFirstOrDefaultAsync<long>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter novo número para documento não fiscal (DNF).");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<long> InserirAsync(Conta entity)
        {
            try
            {
                var sql = @"INSERT 
                              INTO CONTA (SEQ_ORIGINAL, 
                                          TIPO_CT, 
                                          MATRICULA, 
                                          LOCALIZACAO, 
                                          ANO_MES_CT, 
                                          ID_CICLO, 
                                          SITUACAO_CT, 
                                          DATA_FAT, 
                                          DATA_VENC, 
                                          TIPO_ENTREGA, 
                                          ID_CENTRALIZADOR, 
                                          QTD_DEBITO, 
                                          QTD_PAG, 
                                          ID_MEN_MES, 
                                          ID_AVISO, 
                                          DATA_CANC, 
                                          PGTO_PRAZO, 
                                          PGTO_FORA, 
                                          ANO_MES_FAT_MULTA, 
                                          VL_AGUA, 
                                          VL_ESGOTO, 
                                          VL_SERVICO, 
                                          VL_COMERC, 
                                          VL_ICMS, 
                                          VL_MULTA, 
                                          VL_ICFRF, 
                                          VL_DESCONTO, 
                                          VL_JURO, 
                                          VL_TERCEIRO, 
                                          VL_CORRECAO_MONET, 
                                          VL_DEVOLUCAO, 
                                          DATA_CONTABIL, 
                                          ID_CONTRATO) 
                                  VALUES (SEQ_DOC_NAO_FISCAL.NEXTVAL, 
                                          :P_TIPOCT, 
                                          :P_MATRICULA, 
                                          :P_LOCALIZACAO, 
                                          :P_ANOMESCT, 
                                          :P_IDCICLO, 
                                          :P_SITUACAOCT, 
                                          SYSDATE, 
                                          :P_DATAVENC, 
                                          :P_TIPOENTREGA, 
                                          :P_IDCENTRALIZADOR, 
                                          :P_QTDDEBITOS, 
                                          :P_QTDPAG, 
                                          :P_IDMENMES, 
                                          :P_IDAVISO, 
                                          NULL, 
                                          :P_PGTOPRAZO, 
                                          :P_PGTOFORA, 
                                          :P_ANOMESFATMULTA, 
                                          :P_VLAGUA, 
                                          :P_VLESGOTO, 
                                          :P_VLSERVICOS, 
                                          :P_VLCOMERC, 
                                          :P_VLICMS, 
                                          :P_VLMULTA, 
                                          :P_VLICFRF, 
                                          :P_VLDESCONTO, 
                                          :P_VLJURO, 
                                          :P_VLTERCEIROS, 
                                          :P_VLCORREMONET, 
                                          :P_VLDEVOLUCAO, 
                                          NULL, 
                                          :P_IDCONTRATO)
                                 RETURNING SEQ_ORIGINAL INTO :V_SEQ_ORIGINAL";

                OracleParameter.Add("V_SEQ_ORIGINAL", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                OracleParameter.Add("P_TIPOCT", (int)entity.TipoConta, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_LOCALIZACAO", entity.Localizacao, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ANOMESCT", entity.AnoMes, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_IDCICLO", entity.IdCiclo, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_SITUACAOCT", (int)entity.SituacaoConta, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVENC", entity.Vencimento, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_TIPOENTREGA", entity.TipoEntrega, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_IDCENTRALIZADOR", entity.IdCentralizador, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_QTDDEBITOS", entity.QuantidadeDebito, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_QTDPAG", entity.QuantidadePagamento, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_IDMENMES", entity.IdMensagemMes, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_IDAVISO", entity.IdAviso, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_PGTOPRAZO", entity.PagamentoPrazo, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_PGTOFORA", entity.PagamentoFora, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ANOMESFATMULTA", string.Empty);
                OracleParameter.Add("P_VLAGUA", entity.ValorAgua, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLESGOTO", entity.ValorEsgoto, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLSERVICOS", entity.ValorServico, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLCOMERC", entity.ValorComercial, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLICMS", entity.ValorICMS, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLMULTA", entity.ValorMulta, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLICFRF", entity.ValorIcfrf, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLDESCONTO", entity.ValorDesconto, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLJURO", entity.ValorJuro, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLTERCEIROS", entity.ValorTerceiro, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLCORREMONET", entity.ValorCorrecaoMonetaria, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_VLDEVOLUCAO", entity.ValorDevolucao, OracleDbType.Decimal, ParameterDirection.Input);
                OracleParameter.Add("P_IDCONTRATO", entity.IdContrato, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                if (result > 0)
                {
                    entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_SEQ_ORIGINAL").ToString(), typeof(long));

                    return entity.Id;
                }
                else
                {
                    return Int64.MinValue;
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(Conta)} [matricula:{entity.Matricula}, ano/mes:{entity.AnoMes}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<Conta> ObterQuitacaoDebitoAsync(string matricula, DateTime vencimento, Decimal valor)
        {
            try
            {
                var sql = $@"SELECT *
                               FROM ({new StringBuilder(ObterSqlContas())}
                                     WHERE CTA.TIPO_CT = 2
                                       AND CTA.SITUACAO_CT = 0
                                       AND CTA.MATRICULA = :P_MATRICULA
                                       AND CTA.DATA_VENC = :P_DATA_VENC
                                       AND CTA.VL_DO_MES = :P_VALOR
                                     ORDER BY CTA.SEQ_ORIGINAL DESC)
                              WHERE ROWNUM = 1 ";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DATA_VENC", vencimento, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_VALOR", valor, OracleDbType.Decimal, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<Conta>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Conta)} [Matricula:{matricula}, Vencimento:{vencimento.ToShortDateString()}, Valor:{valor}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<IEnumerable<Conta>> ObterContasQuitacaoDebitoAsync(long seqOriginal)
        {
            try
            {
                var sql = $@"SELECT *
                               FROM ({new StringBuilder(ObterSqlContas())}
                                     WHERE CTA.SEQ_ORIGINAL IN (SELECT DOC_ORIGINAL 
                                                                FROM COMPOE_CT
                                                               WHERE DOC_PAGO = :P_DOC_PAGO)
                                    )
                              ORDER BY ANO_MES_CT DESC";

                OracleParameter.Add("P_DOC_PAGO", seqOriginal, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryAsync<Conta>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Conta)} [seqOriginal {seqOriginal}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #region [Métodos privados]

        private string ObterSqlContas()
        {
            return @"SELECT CTA.SEQ_ORIGINAL,
                            CTA.MATRICULA,
                            CTA.ANO_MES_CT,
                            CTA.TIPO_CT,
                            CTA.SITUACAO_CT,
                            CASE
                                WHEN CLI.VENC_CONTA IS NULL THEN
                                CTA.DATA_VENC
                                WHEN CLI.VENC_CONTA >
                                    TO_NUMBER(TO_CHAR(LAST_DAY(CTA.DATA_VENC), 'DD')) THEN
                                TO_DATE(TO_CHAR(CTA.DATA_VENC, 'RRRRMM') ||
                                        TO_CHAR(LAST_DAY(CTA.DATA_VENC), 'DD'),
                                        'RRRRMMDD')
                                ELSE
                                TO_DATE(TO_CHAR(CTA.DATA_VENC, 'RRRRMM') ||
                                        LPAD(CLI.VENC_CONTA, 2, '0'),
                                        'RRRRMMDD')
                            END AS DATA_VENC,
                            CTA.VL_AGUA,
                            CTA.VL_ESGOTO,
                            CTA.VL_SERVICO,
                            CTA.VL_MULTA,
                            CTA.VL_ICFRF,
                            CTA.VL_DESCONTO,
                            CTA.VL_JURO,
                            CTA.VL_DEVOLUCAO,
                            CTA.VL_DO_MES,
                            NVL(CTA.DATA_EMISSAO, CTA.DATA_FAT) AS DATA_EMISSAO,
                            CTA.ID_CONTRATO,
                            CTA.VL_RECURSOS_HIDRICOS_AGUA,
                            CTA.VL_RECURSOS_HIDRICOS_ESG,
                            CTA.VL_DESCONTO_RES_SOCIAL,
                            CTA.VL_DESCONTO_PEQ_COMERCIO,
                            CTA.VL_ESGOTO_ALTERNATIVO,
                            CTA.VL_DESCONTO_LIG_ESTIMADA,
                            CTA.VL_DESCONTO_RETIFICADO,
                            CTA.VL_DESCONTO_PP_CONCEDENTE,
                            CTA.CD_SIT_NEGATIVACAO,
                            CTA.LOCALIZACAO,
                            CTA.ID_CICLO
                       FROM NETUNO.CONTA CTA
                       JOIN NETUNO.CLIENTE CLI
                         ON CLI.MATRICULA = CTA.MATRICULA ";
        }

        #endregion

    }
}
