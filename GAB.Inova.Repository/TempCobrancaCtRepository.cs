﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class TempCobrancaCtRepository : DapperRepository, ITempCobrancaCtRepository
    {

        readonly ILogger _logger;

        public TempCobrancaCtRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TempCobrancaCtRepository>();
        }

        public async Task<bool> InserirAsync(TempCobrancaCt entity)
        {
            try
            {
                var sql = @"INSERT 
                              INTO TEMPCOBRANCACT (MATRICULA,
                                                   ANOMESCT,
                                                   DATAVENCIMENTO,
                                                   DATAPAGAMENTO,
                                                   IDUSUARIO,
                                                   TIPOCOBRANCA,
                                                   ORIGEM,
                                                   VALORORIGINAL,
                                                   INDICEVENC,
                                                   SEQORIGINALPAGO,
                                                   PERCCORRECAO,
                                                   LOCALIZACAO,
                                                   DATAFATURAMENTO)
                                                VALUES
                                                  (:P_MATRICULA,
                                                   :P_ANOMESCT,
                                                   :P_DATAVENC,
                                                   TRUNC(SYSDATE),
                                                   :P_USUARIO,
                                                   :P_TIPOCOBRANCA,
                                                   :P_ORIGEM,
                                                   :P_VLDOMES,
                                                   :P_INDICEVENC,
                                                   :P_SEQORIGINAL,
                                                   :P_PERCCORRECAO,
                                                   :P_LOCALIZACAO,
                                                   TRUNC(SYSDATE))";

                OracleParameter.Add("P_MATRICULA", entity.Matricula, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_ANOMESCT", entity.AnoMesCt, OracleDbType.Varchar2, ParameterDirection.Input);
                OracleParameter.Add("P_DATAVENC", entity.DataVencimento, OracleDbType.Date, ParameterDirection.Input);
                OracleParameter.Add("P_USUARIO", entity.IdUsuario, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_TIPOCOBRANCA", entity.TipoCobranca, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_ORIGEM", entity.Origem, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_VLDOMES", entity.ValorOriginal, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_INDICEVENC", entity.IndiceVencimento, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_SEQORIGINAL", entity.SeqOriginalPago, OracleDbType.Int64, ParameterDirection.Input);
                OracleParameter.Add("P_PERCCORRECAO", entity.PercentualCorrecao, OracleDbType.Double, ParameterDirection.Input);
                OracleParameter.Add("P_LOCALIZACAO", entity.Localizacao, OracleDbType.Varchar2, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(TempCobrancaCt)} [matricula:{entity.Matricula}, seq original:{entity.SeqOriginalPago}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
