﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class VwClienteRepository: DapperRepository, IVwClienteRepository
    {

        readonly ILogger _logger;

        public VwClienteRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<VwClienteRepository>();
        }

        public async Task<IEnumerable<VwCliente>> ObterPorDocumentoAsync(string documento)
        {
            try
            {
                var sql = new StringBuilder(ObterSql());

                sql.Append(" WHERE C.CPFCLI = :P_CPFCLI ");

                OracleParameter.Add("P_CPFCLI", documento, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryAsync<VwCliente>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwCliente)} [documento:{documento}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<VwCliente> ObterPorMatriculaAsync(string matricula)
        {
            try
            {
                var sql = new StringBuilder(ObterSql());

                sql.Append(" WHERE C.MATRICULA = :P_MATRICULA ");

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                return await DbConn.QueryFirstAsync<VwCliente>(sql.ToString(), OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwCliente)} [matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        #region [Métodos privados]

        private string ObterSql()
        {
            return @"SELECT C.MATRICULA,
                            C.NOME_CLIENTE,
                            C.ENDERECO,
                            C.NUM_IMOVEL,
                            C.COMPLEMENTO,
                            C.NOME_BAIRRO,
                            C.NOME_MUNICIPIO,
                            C.NOME_LOCAL,
                            C.ECO_RES,
                            C.ECO_COM,
                            C.ECO_IND,
                            C.ECO_PUB,
                            C.ID_CICLO,
                            C.ID_SETOR,
                            C.ROTA,
                            C.SEQUENCIA,
                            C.ID_HD,
                            C.TIPO_ENTREGA
                       FROM VW_CLIENTES C ";
        }

        #endregion
    }
}
