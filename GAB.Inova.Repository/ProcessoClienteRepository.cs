﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ProcessoClienteRepository
        : DapperRepository, IProcessoClienteRepository
    {

        readonly ILogger _logger;

        public ProcessoClienteRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ProcessoClienteRepository>();
        }

        public async Task<ProcessoCliente> VerificarClienteSobJudice(string matricula)
        {
            try
            {
                var sql = @"SELECT PC.ID_PROC_CLIENTE,
                                   PC.ID_PROCESSO,
                                   PC.MATRICULA,
                                   PC.ID_CONTRATO,
                                   PRO.ID_PROCESSO,
                                   PRO.PROCESSO_NOVO,
                                   PRO.ANO_PROCESSO,
                                   PRO.VINCULAR_CONTA_FUTURA,
                                   PRO.DESMEMBRAR_CONTA,
                                   PRO.CUMPRIR_TUTELA_CAUTELAR,
                                   PRO.PMT_ACAO_COBRANCA,
                                   PRO.PMT_AUTO_INFRACAO,
                                   PRO.PMT_ALT_CADASTRAL,
                                   PRO.PMT_EMISSAO_CORTE,
                                   PRO.PMT_CONTA,
                                   PRO.PMT_NOTIFICACAO,
                                   PRO.PMT_PARCELAMENTO,
                                   PRO.PMT_ORDEM_SERVICO,
                                   PRO.PMT_RETIFICACAO,
                                   PRO.PMT_SEGUNDA_VIA,
                                   PRO.PMT_QUITACAO_DEBITO,
                                   PRO.PMT_NEGATIVACAO
                              FROM NETUNO.PROCESSO_CLIENTE PC
                              JOIN NETUNO.PROCESSO PRO
                                ON PRO.ID_PROCESSO = PC.ID_PROCESSO
                              JOIN NETUNO.CLIENTE CLI
                                ON CLI.MATRICULA = PC.MATRICULA
                              JOIN NETUNO.CONTRATO CTR
                                ON CTR.ID_CONTRATO = PC.ID_CONTRATO
                               AND CTR.ID_SITUACAO = 1
                             WHERE PC.MATRICULA = :P_MATRICULA
                               AND PRO.CD_PROC_SITUACAO = 1";

                OracleParameter.Add("P_MATRICULA", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                var processos = await DbConn.QueryAsync<ProcessoCliente, Processo, ProcessoCliente>(sql,
                    map: (processoCliente, processo) =>
                    {
                        processoCliente.Processo = processo;

                        return processoCliente;
                    },
                    param: OracleParameter,
                    splitOn: "ID_PROCESSO"
                );

                return processos.FirstOrDefault();
            }
            catch(Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar sob judice - {typeof(ProcessoCliente)} [matrícula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
