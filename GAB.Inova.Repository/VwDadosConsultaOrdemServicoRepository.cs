﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class VwDadosConsultaOrdemServicoRepository : DapperRepository, IVwDadosConsultaOrdemServicoRepository
    {

        readonly ILogger _logger;

        public VwDadosConsultaOrdemServicoRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<VwDadosConsultaOrdemServicoRepository>();
        }

        public async Task<VwDadosConsultaOrdemServico> ObterAsync(long numeroOS)
        {
            try
            {
                var sql = @"SELECT *
                              FROM VW_DDS_CNSLT_OS
                             WHERE NUMERO_OS = :P_NUMERO_OS";

                OracleParameter.Add("P_NUMERO_OS", numeroOS, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<VwDadosConsultaOrdemServico>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(VwDadosConsultaOrdemServico)} [numeroOS:{numeroOS}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

    }
}
