﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class DeclaracaoAnualRepository : DapperRepository, IDeclaracaoAnualRepository
    {

        readonly ILogger _logger;

        public DeclaracaoAnualRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<DeclaracaoAnualRepository>();
        }

        public async Task<IEnumerable<DeclaracaoAnual>> ObterDeclaracoesAnuaisAsync(long IdContrato)
        {
            try
            {
                var sql = @"SELECT *
                              FROM (SELECT DA.*
                                      FROM DECLARACAO_ANUAL DA
                                      JOIN CONTRATO CTR
                                        ON DA.MATRICULA = CTR.MATRICULA
                                     WHERE CTR.ID_SITUACAO = 1
                                       AND EXISTS (SELECT C.MATRICULA
                                                     FROM CONTA C
                                                    WHERE MATRICULA = CTR.MATRICULA
                                                      AND SUBSTR(ANO_MES_CT, 1, 4) = DA.ANO_REF
                                                      AND ID_CONTRATO = :P_ID_CONTRATO)
                                     ORDER BY DA.ANO_REF DESC)
                             WHERE ROWNUM <= 5
                            ";

                OracleParameter.Add("P_ID_CONTRATO", IdContrato, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryAsync<DeclaracaoAnual>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(DeclaracaoAnual)} [contrato:{IdContrato}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<DeclaracaoAnual> ObterAsync(long seqDeclaracaoAnual)
        {
            try
            {
                var sql = @"SELECT *
                                  FROM DECLARACAO_ANUAL
                                 WHERE SEQ_DECLARACAO = :P_SEQ_DECLARACAO";

                OracleParameter.Add("P_SEQ_DECLARACAO", seqDeclaracaoAnual, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<DeclaracaoAnual>(sql, OracleParameter);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(DeclaracaoAnual)} [seqDeclaracao:{seqDeclaracaoAnual}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarImpressaoAsync(long seqDeclaracaoAnual)
        {

            try
            {
                var sql = @"UPDATE DECLARACAO_ANUAL 
                               SET IMPRESSAO = IMPRESSAO + 1 
                             WHERE SEQ_DECLARACAO = :P_SEQ_DECLARACAO";

                OracleParameter.Add("P_SEQ_DECLARACAO", seqDeclaracaoAnual, OracleDbType.Int64, ParameterDirection.Input);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar - {typeof(DeclaracaoAnual)} [seqDeclaracaoAnual:{seqDeclaracaoAnual}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
