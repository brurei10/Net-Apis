﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class ContaDigitalEnvioRepository
        : DapperRepository, IContaDigitalEnvioRepository
    {

        readonly ILogger _logger;

        public ContaDigitalEnvioRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
           : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<ContaDigitalEnvioRepository>();
        }

        public async Task<bool> InserirAsync(ContaDigitalEnvio entity)
        {
            try
            {
                var sql = @"INSERT INTO NETUNO.CONTA_DIGITAL_ENVIO
                              (SEQ_ORIGINAL, DT_ENVIO, STATUS, ID_MENSAGEM_INTEGRADOR)
                            VALUES
                              (:P_SEQ_ORIGINAL, SYSDATE, :P_STATUS, :P_ID_MENSAGEM_INTEGRADOR)
                            RETURNING ID_CONTA_DIGITAL_ENVIO INTO :V_ID_CONTA_DIGITAL_ENVIO";

                OracleParameter.Add("P_SEQ_ORIGINAL", entity.SeqOriginal, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_STATUS", (int)entity.Status, OracleDbType.Int32, ParameterDirection.Input);

                if(entity.IdMensagemIntegrador.HasValue)
                    OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", entity.IdMensagemIntegrador.Value, OracleDbType.Int32, ParameterDirection.Input);
                else
                    OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);

                OracleParameter.Add("V_ID_CONTA_DIGITAL_ENVIO", dbType: OracleDbType.Int64, direction: ParameterDirection.Output);

                var result = await DbConn.ExecuteAsync(sql, OracleParameter);

                entity.Id = (long)Convert.ChangeType(OracleParameter.Get<dynamic>("V_ID_CONTA_DIGITAL_ENVIO").ToString(), typeof(long));

                return result > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao inserir - {typeof(ContaDigitalEnvio)} [seqoriginal:{entity.SeqOriginal}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<bool> AtualizarAsync(ContaDigitalEnvio entity)
        {
            try
            {
                var sql = @"UPDATE NETUNO.CONTA_DIGITAL_ENVIO CDE
                               SET CDE.STATUS = :P_STATUS,
                                   CDE.ID_MENSAGEM_INTEGRADOR = :P_ID_MENSAGEM_INTEGRADOR
                             WHERE CDE.ID_CONTA_DIGITAL_ENVIO = :P_ID_CONTA_DIGITAL_ENVIO";

                OracleParameter.Add("P_STATUS", (int)entity.Status, OracleDbType.Int32, ParameterDirection.Input);
                if(entity.IdMensagemIntegrador.HasValue)
                    OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", entity.IdMensagemIntegrador.Value, OracleDbType.Int32, ParameterDirection.Input);
                else
                    OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", DBNull.Value, OracleDbType.Int32, ParameterDirection.Input);
                OracleParameter.Add("P_ID_CONTA_DIGITAL_ENVIO", entity.Id, OracleDbType.Int32, ParameterDirection.Input);

                return await DbConn.ExecuteAsync(sql, OracleParameter) > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao atualizar - {typeof(ContaDigitalEnvio)} [id:{entity.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }

        public async Task<ContaDigitalEnvio> ObterPorIdMensagemEnvioAsync(long idMensagemEnvio)
        {
            try
            {
                var sql = @"SELECT ID_CONTA_DIGITAL_ENVIO, 
                                   SEQ_ORIGINAL,
                                   DT_ENVIO, 
                                   STATUS, 
                                   ID_MENSAGEM_INTEGRADOR  
                             FROM NETUNO.CONTA_DIGITAL_ENVIO
                            WHERE ID_MENSAGEM_INTEGRADOR = :P_ID_MENSAGEM_INTEGRADOR ";

                OracleParameter.Add("P_ID_MENSAGEM_INTEGRADOR", idMensagemEnvio, OracleDbType.Int64, ParameterDirection.Input);

                return await DbConn.QueryFirstOrDefaultAsync<ContaDigitalEnvio>(sql, OracleParameter);
            }
            finally
            {
                OracleParameter.Clean();
            }            
        }

    }
}
