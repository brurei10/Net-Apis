﻿using Dapper;
using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Specifications.BairroSpec;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Text;

namespace GAB.Inova.Repository
{
    public class BairroRepository
        : DapperRepository, IBairroRepository
    {

        readonly ILogger _logger;

        public BairroRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<UsuarioRepository>();
        }

        public bool VerificarExistencia(Bairro bairro)
        {
            try
            {
                var sql = GetSelectSQL("WHERE B.ID_BAIRRO = :P_ID_BAIRRO");

                OracleParameter.Add("P_ID_BAIRRO", bairro.Id, OracleDbType.Int64, ParameterDirection.Input);

                var result = DbConn.ExecuteScalar<int>(sql, param: OracleParameter);

                return result != default(int);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter - {typeof(Bairro)} [id:{bairro.Id}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
        #region [Métodos privados]

        private string GetSelectSQL(string where)
        {
            var builder = new StringBuilder(@"SELECT B.ID_BAIRRO,
                                                     B.NOME_BAIRRO
                                                FROM NETUNO.BAIRRO B ");

            if (!string.IsNullOrEmpty(where))
            {
                builder.AppendLine(where);
            }

            return builder.ToString();
        }

        #endregion

        #region [Métodos públicos para validação]

        public ValidationResult BairroValido(Bairro entity)
        {
            try
            {
                return new BairroValidoValidation(this).Validate(entity);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao validar bairro Id: {entity?.Id}.");

                throw;
            }
        }

        #endregion

    }
}
