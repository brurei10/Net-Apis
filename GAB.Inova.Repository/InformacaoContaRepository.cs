﻿using Dapper;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.Bases;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Data;
using System.Threading.Tasks;

namespace GAB.Inova.Repository
{
    public class InformacaoContaRepository
        : DapperRepository, IInformacaoContaRepository
    {

        readonly ILogger _logger;

        public InformacaoContaRepository(IDbConnection dbConnection, ILoggerFactory loggerFactory)
            : base(dbConnection)
        {
            _logger = loggerFactory.CreateLogger<InformacaoContaRepository>();
        }

        public async Task<InformacaoConta> ObterAsync(string protocolo, string matricula)
        {
            try
            {
                OracleParameter.Add("P_PROTOCOLO", protocolo, OracleDbType.Varchar2, ParameterDirection.Input); 
                OracleParameter.Add("P_NUMLIGACAO", matricula, OracleDbType.Varchar2, ParameterDirection.Input);

                OracleParameter.Add("P_OUT_CUR_DADOS", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

                await DbConn.ExecuteAsync("PACK_CALL_CENTER.OBTER_INFORMACAO_CONTA", param: OracleParameter, commandType: CommandType.StoredProcedure);

                var output = OracleParameter.Get<OracleRefCursor>("P_OUT_CUR_DADOS");

                var dr = output.GetDataReader();

                var informacaoConta = new InformacaoConta();

                while (dr.Read())
                {
                    informacaoConta.sitConta = dr.GetInt16("SITCONTA");
                    informacaoConta.dataVencimento = dr.IsDBNull("DATAVENCIMENTO") ? (DateTime?)null : dr.GetDateTime("DATAVENCIMENTO");
                    informacaoConta.valor = dr.IsDBNull("VALOR") ? (Decimal?)null : dr.GetDecimal("VALOR");
                }

                return informacaoConta;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter informações da conta - {typeof(InformacaoConta)} [protocolo:{protocolo}, matricula:{matricula}].");

                throw;
            }
            finally
            {
                OracleParameter.Clean();
            }
        }
    }
}
