﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TarifaDiariaMap
        : DommelEntityMap<TarifaDiaria>
    {

        public TarifaDiariaMap()
        {
            ToTable("TARIFA_DIARIA");

            Map(p => p.Id)
                .ToColumn("ID_TARIFA_DIARIA")
                .IsKey();

            Map(p => p.DataTarifa)
                .ToColumn("DATA_TARIFA");

            Map(p => p.FaixaAnterior)
                .ToColumn("FAIXA_ANTERIOR");

            Map(p => p.FaixaConsumo)
                .ToColumn("FAIXA_CONSUMO");

            Map(p => p.GrupoTarifa)
                .ToColumn("GRUPO_TARIFA");

            Map(p => p.IdCategoria)
                .ToColumn("ID_CATEGORIA");

            Map(p => p.IdFaixaTarifa)
                .ToColumn("ID_FAIXA_TARIFA");

            Map(p => p.IdTarifa)
                .ToColumn("ID_TARIFA");

            Map(p => p.Localidade)
                .ToColumn("LOCALIDADE");

            Map(p => p.TipoTarifa)
                .ToColumn("TIPO_TARIFA");

            Map(p => p.ValorAcumulado)
                .ToColumn("VL_ACUMULADO");

            Map(p => p.ValorAcumuladoEsgoto)
                .ToColumn("VL_ACUMULADO_ESGO");

            Map(p => p.ValorAcumuladoSocial)
                .ToColumn("VL_ACUMULADO_SOCIAL");

            Map(p => p.ValorFaixa)
                .ToColumn("VL_FAIXA");

            Map(p => p.ValorFaixaEsgoto)
                .ToColumn("VL_FAIXA_ESGO");

            Map(p => p.ValorFaixaSocial)
                .ToColumn("VL_FAIXA_SOCIAL");
        }

    }
}
