﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ImprimeGRPMap : DommelEntityMap<ImprimeGRP>
    {
        public ImprimeGRPMap()
        {
            ToTable("IMPRIME_GRP");

            Map(p => p.Id)
                .ToColumn("SEQ_ORIGINAL")
                .IsKey();

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Titulo)
                .ToColumn("TITULO");

            Map(p => p.NomeCliente)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.Endereco)
                .ToColumn("ENDERECO");

            Map(p => p.NomeLocal)
                .ToColumn("NOME_LOCAL");

            Map(p => p.ConsumoAnterior01)
                .ToColumn("CONS_ANT_01");

            Map(p => p.ConsumoAnterior02)
                .ToColumn("CONS_ANT_02");

            Map(p => p.ConsumoAnterior03)
                .ToColumn("CONS_ANT_03");

            Map(p => p.ConsumoAnterior04)
                .ToColumn("CONS_ANT_04");

            Map(p => p.ConsumoAnterior05)
                .ToColumn("CONS_ANT_05");

            Map(p => p.ConsumoAnterior06)
                .ToColumn("CONS_ANT_06");

            Map(p => p.ConsumoAnterior07)
                .ToColumn("CONS_ANT_07");

            Map(p => p.ConsumoAnterior08)
                .ToColumn("CONS_ANT_08");

            Map(p => p.ConsumoAnterior09)
                .ToColumn("CONS_ANT_09");

            Map(p => p.ConsumoAnterior10)
                .ToColumn("CONS_ANT_10");

            Map(p => p.ConsumoAnterior11)
                .ToColumn("CONS_ANT_11");

            Map(p => p.ConsumoAnterior12)
                .ToColumn("CONS_ANT_12");

            Map(p => p.EconomiaResidencial)
                .ToColumn("ECO_RES");

            Map(p => p.EconomiaComercial)
                .ToColumn("ECO_COM");

            Map(p => p.EconomiaIndustrial)
                .ToColumn("ECO_IND");

            Map(p => p.EconomiaPublica)
                .ToColumn("ECO_PUB");

            Map(p => p.IdCiclo)
                .ToColumn("ID_CICLO");

            Map(p => p.SetorRotaSequencia)
                .ToColumn("SETOR_ROT_SEQ");

            Map(p => p.IdHd)
                .ToColumn("ID_HD");

            Map(p => p.TipoEntrega)
                .ToColumn("TIPO_ENTREGA");

            Map(p => p.AnoMesCt)
                .ToColumn("ANO_MES_CT");

            Map(p => p.DataLeituraAtual)
                .ToColumn("DATA_LEIT_ATUAL");

            Map(p => p.DataLeituraAnterior)
                .ToColumn("DATA_LEIT_ANT");

            Map(p => p.ConsumoFaturado)
                .ToColumn("CONS_FAT");

            Map(p => p.LeituraAtual)
                .ToColumn("LEITURA_ATUAL");

            Map(p => p.LeituraAnterior)
                .ToColumn("LEITURA_ANT");

            Map(p => p.Media)
                .ToColumn("MEDIA");

            Map(p => p.ValorGRP)
                .ToColumn("VL_GRP");

            Map(p => p.DataVencimento)
                .ToColumn("DATA_VENC");

            Map(p => p.DataValor)
                .ToColumn("DATA_VAL");

            Map(p => p.Observacao)
                .ToColumn("OBSERVACAO");

            Map(p => p.CodigoBarra)
                .ToColumn("COD_BARRA");

            Map(p => p.CompoeCt01)
                .ToColumn("COMP_CT_01");

            Map(p => p.CompoeCt02)
                .ToColumn("COMP_CT_02");

            Map(p => p.CompoeCt03)
                .ToColumn("COMP_CT_03");

            Map(p => p.CompoeCt04)
                .ToColumn("COMP_CT_04");

            Map(p => p.CompoeCt05)
                .ToColumn("COMP_CT_05");

            Map(p => p.CompoeCt06)
                .ToColumn("COMP_CT_06");

            Map(p => p.CompoeCt07)
                .ToColumn("COMP_CT_07");

            Map(p => p.CompoeCt08)
                .ToColumn("COMP_CT_08");

            Map(p => p.CompoeCt09)
                .ToColumn("COMP_CT_09");

            Map(p => p.CompoeCt10)
                .ToColumn("COMP_CT_10");

            Map(p => p.CompoeCt11)
                .ToColumn("COMP_CT_11");

            Map(p => p.CompoeCt12)
                .ToColumn("COMP_CT_12");

            Map(p => p.CompoeCt13)
                .ToColumn("COMP_CT_13");

            Map(p => p.CompoeCt14)
                .ToColumn("COMP_CT_14");

            Map(p => p.CompoeCt15)
                .ToColumn("COMP_CT_15");

            Map(p => p.CompoeCt16)
                .ToColumn("COMP_CT_16");

            Map(p => p.EstruturaTarifaria01)
                .ToColumn("ESTRUT_TAR_01");

            Map(p => p.EstruturaTarifaria02)
                .ToColumn("ESTRUT_TAR_02");

            Map(p => p.EstruturaTarifaria03)
                .ToColumn("ESTRUT_TAR_03");

            Map(p => p.EstruturaTarifaria04)
                .ToColumn("ESTRUT_TAR_04");

            Map(p => p.EstruturaTarifaria05)
                .ToColumn("ESTRUT_TAR_05");

            Map(p => p.EstruturaTarifaria06)
                .ToColumn("ESTRUT_TAR_06");

            Map(p => p.EstruturaTarifaria07)
                .ToColumn("ESTRUT_TAR_07");

            Map(p => p.EstruturaTarifaria08)
                .ToColumn("ESTRUT_TAR_08");

            Map(p => p.EstruturaTarifaria09)
                .ToColumn("ESTRUT_TAR_09");

            Map(p => p.EstruturaTarifaria10)
                .ToColumn("ESTRUT_TAR_10");

            Map(p => p.EstruturaTarifaria11)
                .ToColumn("ESTRUT_TAR_11");

            Map(p => p.EstruturaTarifaria12)
                .ToColumn("ESTRUT_TAR_12");

            Map(p => p.EstruturaTarifaria13)
                .ToColumn("ESTRUT_TAR_13");

            Map(p => p.EstruturaTarifaria14)
                .ToColumn("ESTRUT_TAR_14");

            Map(p => p.EstruturaTarifaria15)
                .ToColumn("ESTRUT_TAR_15");

            Map(p => p.EstruturaTarifaria16)
                .ToColumn("ESTRUT_TAR_16");

            Map(p => p.CompoeValor01)
                .ToColumn("COMP_VL_01");

            Map(p => p.CompoeValor02)
                .ToColumn("COMP_VL_02");

            Map(p => p.CompoeValor03)
                .ToColumn("COMP_VL_03");

            Map(p => p.CompoeValor04)
                .ToColumn("COMP_VL_04");

            Map(p => p.CompoeValor05)
                .ToColumn("COMP_VL_05");

            Map(p => p.CompoeValor06)
                .ToColumn("COMP_VL_06");

            Map(p => p.CompoeValor07)
                .ToColumn("COMP_VL_07");

            Map(p => p.CompoeValor08)
                .ToColumn("COMP_VL_08");

            Map(p => p.CompoeValor09)
                .ToColumn("COMP_VL_09");

            Map(p => p.CompoeValor10)
                .ToColumn("COMP_VL_10");

            Map(p => p.CompoeValor11)
                .ToColumn("COMP_VL_11");

            Map(p => p.CompoeValor12)
                .ToColumn("COMP_VL_12");

            Map(p => p.CompoeValor13)
                .ToColumn("COMP_VL_13");

            Map(p => p.CompoeValor14)
                .ToColumn("COMP_VL_14");

            Map(p => p.CompoeValor15)
                .ToColumn("COMP_VL_15");

            Map(p => p.CompoeValor16)
                .ToColumn("COMP_VL_16");

            Map(p => p.IdSolicitacao)
                .ToColumn("ID_SOLICITACAO");

            Map(p => p.DataEmissao)
                .ToColumn("DATA_EMISSAO");

            Map(p => p.DiasConsumo)
                .ToColumn("DIASCONS");
        }
    }
}
