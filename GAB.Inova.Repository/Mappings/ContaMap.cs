﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContaMap
        : DommelEntityMap<Conta>
    {
        public ContaMap()
        {
            ToTable("CONTA");

            Map(p => p.Id)
                .ToColumn("SEQ_ORIGINAL")
                .IsKey();

            Map(p => p.AnoMes)
                .ToColumn("ANO_MES_CT");

            Map(p => p.Emissao)
                .ToColumn("DATA_EMISSAO");

            Map(p => p.IdContrato)
                .ToColumn("ID_CONTRATO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.SituacaoConta)
                .ToColumn("SITUACAO_CT");

            Map(p => p.SituacaoNegativacao)
                .ToColumn("CD_SIT_NEGATIVACAO");

            Map(p => p.TipoConta)
                .ToColumn("TIPO_CT");

            Map(p => p.ValorAgua)
                .ToColumn("VL_AGUA");

            Map(p => p.ValorDesconto)
                .ToColumn("VL_DESCONTO");

            Map(p => p.ValorDescontoLigacaoEstimada)
                .ToColumn("VL_DESCONTO_LIG_ESTIMADA");

            Map(p => p.ValorDescontoPequenoComercio)
                .ToColumn("VL_DESCONTO_PEQ_COMERCIO");

            Map(p => p.ValorDescontoPoderPublicoConcedente)
                .ToColumn("VL_DESCONTO_PP_CONCEDENTE");

            Map(p => p.ValorDescontoResidencialSocial)
                .ToColumn("VL_DESCONTO_RES_SOCIAL");

            Map(p => p.ValorDescontoRetificado)
                .ToColumn("VL_DESCONTO_RETIFICADO");

            Map(p => p.ValorDevolucao)
                .ToColumn("VL_DEVOLUCAO");

            Map(p => p.ValorEsgoto)
                .ToColumn("VL_ESGOTO");

            Map(p => p.ValorEsgotoAlternativo)
                .ToColumn("VL_ESGOTO_ALTERNATIVO");

            Map(p => p.ValorIcfrf)
                .ToColumn("VL_ICFRF");

            Map(p => p.ValorJuro)
                .ToColumn("VL_JURO");

            Map(p => p.ValorMulta)
                .ToColumn("VL_MULTA");

            Map(p => p.ValorRecursosHidricosAgua)
                .ToColumn("VL_RECURSOS_HIDRICOS_AGUA");

            Map(p => p.ValorRecursosHidricosEsgoto)
                .ToColumn("VL_RECURSOS_HIDRICOS_ESG");

            Map(p => p.ValorServico)
                .ToColumn("VL_SERVICO");

            Map(p => p.ValorTotal)
                .ToColumn("VL_DO_MES");

            Map(p => p.Vencimento)
                .ToColumn("DATA_VENC");

            Map(p => p.Localizacao)
                .ToColumn("LOCALIZACAO");

            Map(p => p.IdCiclo)
                .ToColumn("ID_CICLO");

            Map(p => p.TipoEntrega)
                .ToColumn("TIPO_ENTREGA");

            Map(p => p.IdCentralizador)
                .ToColumn("ID_CENTRALIZADOR");

            Map(p => p.QuantidadeDebito)
                .ToColumn("QTD_DEBITO");

            Map(p => p.QuantidadePagamento)
                .ToColumn("QTD_PAG");

            Map(p => p.IdMensagemMes)
                .ToColumn("ID_MEN_MES");

            Map(p => p.IdAviso)
                .ToColumn("ID_AVISO");

            Map(p => p.PagamentoPrazo)
                .ToColumn("PGTO_PRAZO");

            Map(p => p.PagamentoFora)
                .ToColumn("PGTO_FORA");

            Map(p => p.ValorComercial)
                .ToColumn("VL_COMERC");

            Map(p => p.ValorICMS)
                .ToColumn("VL_ICMS");

            Map(p => p.ValorTerceiro)
                .ToColumn("VL_TERCEIRO");

            Map(p => p.ValorCorrecaoMonetaria)
                .ToColumn("VL_CORRECAO_MONET");
        }
    }
}
