﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContratoMap
        : DommelEntityMap<Contrato>
    {

        public ContratoMap()
        {
            ToTable("CONTRATO");

            Map(p => p.Id)
                .ToColumn("ID_CONTRATO")
                .IsKey();

            Map(p => p.CpfCnpj)
                .ToColumn("CPF_CNPJ");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.NomeCliente)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.TelefoneComercial)
                .ToColumn("TELEFONE_COM");

            Map(p => p.TelefoneResidencial)
                .ToColumn("TELEFONE_RES");

            Map(p => p.Celular)
                .ToColumn("TELEFONE_CEL");

            Map(p => p.Email)
                .ToColumn("EMAIL");

            Map(p => p.EmailContaDigital)
                .ToColumn("EMAIL_CONTA_DIGITAL");

            Map(p => p.Numero)
                .ToColumn("NUMERO");

            Map(p => p.Complemento)
                .ToColumn("COMPLEMENTO");

            Map(p => p.PontoReferencia)
                .ToColumn("PONTO_REFER");

        }

    }
}
