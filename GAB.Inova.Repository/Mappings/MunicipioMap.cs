﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class MunicipioMap
        : DommelEntityMap<Municipio>
    {
        public MunicipioMap()
        {
            ToTable("MUNICIPIO");

            Map(p => p.Id)
                .ToColumn("ID_MUNICIPIO")
                .IsKey();

            Map(p => p.Codigo)
                .ToColumn("COD_MUNICIPIO");

            Map(p => p.Nome)
                .ToColumn("NOME_MUNICIPIO");
        }
    }
}
