﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class CidadeMap
        : DommelEntityMap<Cidade>
    {
        public CidadeMap()
        {
            ToTable("CIDADE");

            Map(p => p.Id)
                .ToColumn("ID_CIDADE")
                .IsKey();

            Map(p => p.Nome)
                .ToColumn("NOME_CIDADE");
        }
    }
}
