﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class VwGeraArquivoImpressaoMap
        : DommelEntityMap<VwGeraArquivoImpressao>
    {

        public VwGeraArquivoImpressaoMap()
        {
            Map(p => p.AnoMesCt)
                .ToColumn("ANO_MES_CT");

            Map(p => p.CepOrdem)
                .ToColumn("CEP_ORDEM");

            Map(p => p.CodigoBarra)
                .ToColumn("COD_BARRA");

            Map(p => p.CodigoBarraAvisoDebito)
                .ToColumn("COD_BARRA_AVISO_DEBITO");

            Map(p => p.Complemento)
                .ToColumn("COMPLEMENTO");

            Map(p => p.CompoeCt01)
                .ToColumn("COMP_CT_01");

            Map(p => p.CompoeCt02)
                .ToColumn("COMP_CT_02");

            Map(p => p.CompoeCt03)
                .ToColumn("COMP_CT_03");

            Map(p => p.CompoeCt04)
                .ToColumn("COMP_CT_04");

            Map(p => p.CompoeCt05)
                .ToColumn("COMP_CT_05");

            Map(p => p.CompoeCt06)
                .ToColumn("COMP_CT_06");

            Map(p => p.CompoeCt07)
                .ToColumn("COMP_CT_07");

            Map(p => p.CompoeCt08)
                .ToColumn("COMP_CT_08");

            Map(p => p.CompoeCt09)
                .ToColumn("COMP_CT_09");

            Map(p => p.CompoeCt10)
                .ToColumn("COMP_CT_10");

            Map(p => p.CompoeCt11)
                .ToColumn("COMP_CT_11");

            Map(p => p.CompoeCt12)
                .ToColumn("COMP_CT_12");

            Map(p => p.CompoeCt13)
                .ToColumn("COMP_CT_13");

            Map(p => p.CompoeCt14)
                .ToColumn("COMP_CT_14");

            Map(p => p.CompoeCt15)
                .ToColumn("COMP_CT_15");

            Map(p => p.CompoeCt16)
                .ToColumn("COMP_CT_16");

            Map(p => p.CompoeVl01)
                .ToColumn("COMP_VL_01");

            Map(p => p.CompoeVl02)
                .ToColumn("COMP_VL_02");

            Map(p => p.CompoeVl03)
                .ToColumn("COMP_VL_03");

            Map(p => p.CompoeVl04)
                .ToColumn("COMP_VL_04");

            Map(p => p.CompoeVl05)
                .ToColumn("COMP_VL_05");

            Map(p => p.CompoeVl06)
                .ToColumn("COMP_VL_06");

            Map(p => p.CompoeVl07)
                .ToColumn("COMP_VL_07");

            Map(p => p.CompoeVl08)
                .ToColumn("COMP_VL_08");

            Map(p => p.CompoeVl09)
                .ToColumn("COMP_VL_09");

            Map(p => p.CompoeVl10)
                .ToColumn("COMP_VL_10");

            Map(p => p.CompoeVl11)
                .ToColumn("COMP_VL_11");

            Map(p => p.CompoeVl12)
                .ToColumn("COMP_VL_12");

            Map(p => p.CompoeVl13)
                .ToColumn("COMP_VL_13");

            Map(p => p.CompoeVl14)
                .ToColumn("COMP_VL_14");

            Map(p => p.CompoeVl15)
                .ToColumn("COMP_VL_15");

            Map(p => p.CompoeVl16)
                .ToColumn("COMP_VL_16");

            Map(p => p.ConsumoCredito)
                .ToColumn("CONS_CREDITO");

            Map(p => p.ConsumoFaturado)
                .ToColumn("CONS_FAT");

            Map(p => p.ConsumoMedido)
                .ToColumn("CONS_MEDIDO");

            Map(p => p.ConsumoPipa)
                .ToColumn("CONS_PIPA");

            Map(p => p.ConsumoResidual)
                .ToColumn("CONS_RESIDUAL");

            Map(p => p.CpfCnpj)
                .ToColumn("CPF_CNPJ");

            Map(p => p.DataEmissao)
                .ToColumn("DATA_EMISSAO");

            Map(p => p.DataLeituraAnterior)
                .ToColumn("DATA_LEIT_ANT");

            Map(p => p.DataLeituraAtual)
                .ToColumn("DATA_LEIT_ATUAL");

            Map(p => p.DataProximaLeitura)
                .ToColumn("DATA_PROX_LEIT");

            Map(p => p.DataVencimento)
                .ToColumn("DATA_VENC");

            Map(p => p.DigitoMatricula)
                .ToColumn("DV_MATR");

            Map(p => p.Economia)
                .ToColumn("ECONOMIA");

            Map(p => p.EconomiaComercial)
                .ToColumn("ECO_COM");

            Map(p => p.EconomiaIndustrial)
                .ToColumn("ECO_IND");

            Map(p => p.EconomiaPublica)
                .ToColumn("ECO_PUB");

            Map(p => p.EconomiaResidencial)
                .ToColumn("ECO_RES");

            Map(p => p.Endereco)
                .ToColumn("ENDERECO");

            Map(p => p.FaixaConsumo01)
                .ToColumn("FAIXA_CONSUMO_01");

            Map(p => p.FaixaConsumo02)
                .ToColumn("FAIXA_CONSUMO_02");

            Map(p => p.FaixaConsumo03)
                .ToColumn("FAIXA_CONSUMO_03");

            Map(p => p.FaixaConsumo04)
                .ToColumn("FAIXA_CONSUMO_04");

            Map(p => p.FaixaConsumo05)
                .ToColumn("FAIXA_CONSUMO_05");

            Map(p => p.FaixaConsumo06)
                .ToColumn("FAIXA_CONSUMO_06");

            Map(p => p.FaixaConsumo07)
                .ToColumn("FAIXA_CONSUMO_07");

            Map(p => p.FaixaConsumo08)
                .ToColumn("FAIXA_CONSUMO_08");

            Map(p => p.FaixaConsumo09)
                .ToColumn("FAIXA_CONSUMO_09");

            Map(p => p.FaixaConsumo10)
                .ToColumn("FAIXA_CONSUMO_10");

            Map(p => p.FaixaConsumoFaturado01)
                .ToColumn("FAIXA_CONS_FATURADO_01");

            Map(p => p.FaixaConsumoFaturado02)
                .ToColumn("FAIXA_CONS_FATURADO_02");

            Map(p => p.FaixaConsumoFaturado03)
                .ToColumn("FAIXA_CONS_FATURADO_03");

            Map(p => p.FaixaConsumoFaturado04)
                .ToColumn("FAIXA_CONS_FATURADO_04");

            Map(p => p.FaixaConsumoFaturado05)
                .ToColumn("FAIXA_CONS_FATURADO_05");

            Map(p => p.FaixaConsumoFaturado06)
                .ToColumn("FAIXA_CONS_FATURADO_06");

            Map(p => p.FaixaConsumoFaturado07)
                .ToColumn("FAIXA_CONS_FATURADO_07");

            Map(p => p.FaixaConsumoFaturado08)
                .ToColumn("FAIXA_CONS_FATURADO_08");

            Map(p => p.FaixaConsumoFaturado09)
                .ToColumn("FAIXA_CONS_FATURADO_09");

            Map(p => p.FaixaConsumoFaturado10)
                .ToColumn("FAIXA_CONS_FATURADO_10");

            Map(p => p.FaixaEconomia01)
                .ToColumn("FAIXA_ECONOMIA_01");

            Map(p => p.FaixaEconomia02)
                .ToColumn("FAIXA_ECONOMIA_02");

            Map(p => p.FaixaEconomia03)
                .ToColumn("FAIXA_ECONOMIA_03");

            Map(p => p.FaixaEconomia04)
                .ToColumn("FAIXA_ECONOMIA_04");

            Map(p => p.FaixaEconomia05)
                .ToColumn("FAIXA_ECONOMIA_05");

            Map(p => p.FaixaEconomia06)
                .ToColumn("FAIXA_ECONOMIA_06");

            Map(p => p.FaixaEconomia07)
                .ToColumn("FAIXA_ECONOMIA_07");

            Map(p => p.FaixaEconomia08)
                .ToColumn("FAIXA_ECONOMIA_08");

            Map(p => p.FaixaEconomia09)
                .ToColumn("FAIXA_ECONOMIA_09");

            Map(p => p.FaixaEconomia10)
                .ToColumn("FAIXA_ECONOMIA_10");

            Map(p => p.FaixaValorTarifa01)
                .ToColumn("FAIXA_VL_TARIFA_01");

            Map(p => p.FaixaValorTarifa02)
                .ToColumn("FAIXA_VL_TARIFA_02");

            Map(p => p.FaixaValorTarifa03)
                .ToColumn("FAIXA_VL_TARIFA_03");

            Map(p => p.FaixaValorTarifa04)
                .ToColumn("FAIXA_VL_TARIFA_04");

            Map(p => p.FaixaValorTarifa05)
                .ToColumn("FAIXA_VL_TARIFA_05");

            Map(p => p.FaixaValorTarifa06)
                .ToColumn("FAIXA_VL_TARIFA_06");

            Map(p => p.FaixaValorTarifa07)
                .ToColumn("FAIXA_VL_TARIFA_07");

            Map(p => p.FaixaValorTarifa08)
                .ToColumn("FAIXA_VL_TARIFA_08");

            Map(p => p.FaixaValorTarifa09)
                .ToColumn("FAIXA_VL_TARIFA_09");

            Map(p => p.FaixaValorTarifa10)
                .ToColumn("FAIXA_VL_TARIFA_10");

            Map(p => p.FaixaValorTarifaEsgoto01)
                .ToColumn("FAIXA_VL_TARIFA_ESG_01");

            Map(p => p.FaixaValorTarifaEsgoto02)
                .ToColumn("FAIXA_VL_TARIFA_ESG_02");

            Map(p => p.FaixaValorTarifaEsgoto03)
                .ToColumn("FAIXA_VL_TARIFA_ESG_03");

            Map(p => p.FaixaValorTarifaEsgoto04)
                .ToColumn("FAIXA_VL_TARIFA_ESG_04");

            Map(p => p.FaixaValorTarifaEsgoto05)
                .ToColumn("FAIXA_VL_TARIFA_ESG_05");

            Map(p => p.FaixaValorTarifaEsgoto06)
                .ToColumn("FAIXA_VL_TARIFA_ESG_06");

            Map(p => p.FaixaValorTarifaEsgoto07)
                .ToColumn("FAIXA_VL_TARIFA_ESG_07");

            Map(p => p.FaixaValorTarifaEsgoto08)
                .ToColumn("FAIXA_VL_TARIFA_ESG_08");

            Map(p => p.FaixaValorTarifaEsgoto09)
                .ToColumn("FAIXA_VL_TARIFA_ESG_09");

            Map(p => p.FaixaValorTarifaEsgoto10)
                .ToColumn("FAIXA_VL_TARIFA_ESG_10");

            Map(p => p.IdHd)
                .ToColumn("ID_HD");

            Map(p => p.IdSistemaAbastecimento)
                .ToColumn("ID_SISTEMA_ABASTECIMENTO");

            Map(p => p.IdSolicitacao)
                 .ToColumn("ID_SOLICITACAO");

            Map(p => p.IdTarifa)
                .ToColumn("ID_TARIFA");

            Map(p => p.IdUsuario)
                .ToColumn("ID_USUARIO");

            Map(p => p.ImpressaoCodigoBarra)
                .ToColumn("IMP_COD_BARRA");

            Map(p => p.ImpressaoConsumo)
                .ToColumn("IMPDADOSCONSUMO");

            Map(p => p.InscricaoMunicipalEstadual)
                .ToColumn("INSC_MUN_EST");

            Map(p => p.LeituraAnterior)
                .ToColumn("LEITURA_ANT");

            Map(p => p.LeituraAtual)
                .ToColumn("LEITURA_ATUAL");

            Map(p => p.LoteAvisoDebito)
                .ToColumn("LOTE_AVISO_DEBITO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Mensagem)
                .ToColumn("MENSAGEM");

            Map(p => p.MensagemDebitoAnterior01)
                .ToColumn("MENSAGEM_DEB_ANT_01");

            Map(p => p.MensagemDebitoAnterior02)
                .ToColumn("MENSAGEM_DEB_ANT_02");

            Map(p => p.MensagemDebitoAnterior03)
                .ToColumn("MENSAGEM_DEB_ANT_03");

            Map(p => p.MensagemDebitoAnterior04)
                .ToColumn("MENSAGEM_DEB_ANT_04");

            Map(p => p.MensagemDebitoAnterior05)
                .ToColumn("MENSAGEM_DEB_ANT_05");

            Map(p => p.MensagemDebitoAnterior06)
                .ToColumn("MENSAGEM_DEB_ANT_06");

            Map(p => p.MensagemDebitoAnterior07)
                .ToColumn("MENSAGEM_DEB_ANT_07");

            Map(p => p.MensagemDebitoAnterior08)
                .ToColumn("MENSAGEM_DEB_ANT_08");

            Map(p => p.MensagemDebitoAnterior09)
                .ToColumn("MENSAGEM_DEB_ANT_09");

            Map(p => p.MensagemDebitoAnterior10)
                .ToColumn("MENSAGEM_DEB_ANT_10");

            Map(p => p.MensagemDebitoAnterior11)
                .ToColumn("MENSAGEM_DEB_ANT_11");

            Map(p => p.MensagemDebitoAnterior12)
                .ToColumn("MENSAGEM_DEB_ANT_12");

            Map(p => p.MensagemDebitoAnterior13)
                .ToColumn("MENSAGEM_DEB_ANT_13");

            Map(p => p.MensagemDebitoAnterior14)
                .ToColumn("MENSAGEM_DEB_ANT_14");

            Map(p => p.MensagemDebitoAnterior15)
                .ToColumn("MENSAGEM_DEB_ANT_15");

            Map(p => p.MensagemDebitoAnterior16)
                .ToColumn("MENSAGEM_DEB_ANT_16");

            Map(p => p.MensagemDebitoAnterior17)
                .ToColumn("MENSAGEM_DEB_ANT_17");

            Map(p => p.MensagemDebitoAnterior18)
                .ToColumn("MENSAGEM_DEB_ANT_18");

            Map(p => p.MensagemDebitoAnterior19)
                .ToColumn("MENSAGEM_DEB_ANT_19");

            Map(p => p.MensagemDebitoAnterior20)
                .ToColumn("MENSAGEM_DEB_ANT_20");

            Map(p => p.MensagemDebitoAnterior21)
                .ToColumn("MENSAGEM_DEB_ANT_21");

            Map(p => p.MensagemDebitoAnterior22)
                .ToColumn("MENSAGEM_DEB_ANT_22");

            Map(p => p.MensagemDebitoAnterior23)
                .ToColumn("MENSAGEM_DEB_ANT_23");

            Map(p => p.MensagemDebitoAnterior24)
                .ToColumn("MENSAGEM_DEB_ANT_24");

            Map(p => p.MensagemDebitoAnterior25)
                .ToColumn("MENSAGEM_DEB_ANT_25");

            Map(p => p.MensagemDebitoAnterior26)
                .ToColumn("MENSAGEM_DEB_ANT_26");

            Map(p => p.MensagemDebitoAnterior27)
                .ToColumn("MENSAGEM_DEB_ANT_27");

            Map(p => p.MensagemDebitoAnterior28)
                .ToColumn("MENSAGEM_DEB_ANT_28");

            Map(p => p.MensagemDebitoAnterior29)
                .ToColumn("MENSAGEM_DEB_ANT_29");

            Map(p => p.MensagemDebitoAnterior30)
                .ToColumn("MENSAGEM_DEB_ANT_30");

            Map(p => p.MensagemPadrao01)
                .ToColumn("MENSAGEM_PADRAO_01");

            Map(p => p.MensagemPadrao02)
                .ToColumn("MENSAGEM_PADRAO_02");

            Map(p => p.MensagemPadrao03)
                .ToColumn("MENSAGEM_PADRAO_03");

            Map(p => p.MensagemPadrao04)
                .ToColumn("MENSAGEM_PADRAO_04");

            Map(p => p.MensagemPadrao05)
                .ToColumn("MENSAGEM_PADRAO_05");

            Map(p => p.MensagemPadrao06)
                .ToColumn("MENSAGEM_PADRAO_06");

            Map(p => p.MensagemPadrao07)
                .ToColumn("MENSAGEM_PADRAO_07");

            Map(p => p.MensagemPadrao08)
                .ToColumn("MENSAGEM_PADRAO_08");

            Map(p => p.MensagemPadrao09)
                .ToColumn("MENSAGEM_PADRAO_09");

            Map(p => p.MensagemPadrao10)
                .ToColumn("MENSAGEM_PADRAO_10");

            Map(p => p.ModoFaturamento)
                .ToColumn("MODO_FAT");

            Map(p => p.NomeCliente)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.NomeTipoEntrega)
                .ToColumn("NOME_TIPO_ENTREGA");

            Map(p => p.NomeTipoFaturamento)
                .ToColumn("NOME_TIPO_FATURAMENTO");

            Map(p => p.NomeUsuario)
                .ToColumn("NOME_USUARIO");

            Map(p => p.NumeroAvisoDebito)
                .ToColumn("NUMERO_AVISO_DEBITO");

            Map(p => p.Observacao)
                .ToColumn("OBSERVACAO");

            Map(p => p.ObservacaoGrp)
                .ToColumn("OBSERVACAO_GRP");

            Map(p => p.PercentualAgua)
                .ToColumn("PERC_AGUA");

            Map(p => p.PercentualCargaTributaria)
                .ToColumn("PERC_CARGA_TRIBUTARIA");

            Map(p => p.PercentualEsgoto)
                .ToColumn("PERC_ESGOTO");

            Map(p => p.QtdeDiasConsumo)
                .ToColumn("QTDE_DIAS_CONS");

            Map(p => p.Rota)
                .ToColumn("ROTA");

            Map(p => p.RotaOrdem)
                .ToColumn("ROTA_ORDEM");

            Map(p => p.Roteirizacao)
                .ToColumn("ROTEIRIZACAO");

            Map(p => p.SeqOriginal)
                .ToColumn("SEQ_ORIGINAL");

            Map(p => p.SiglaEmpresa)
                .ToColumn("SIGLA_EMPRESA");

            Map(p => p.SistemaAbastecimento)
                .ToColumn("SISTEMA_ABASTECIMENTO");

            Map(p => p.SituacaoConta)
                .ToColumn("SITUACAO_CT");

            Map(p => p.SituacaoEsgoto)
                .ToColumn("SIT_ESGOTO");

            Map(p => p.SituacaoHd)
                .ToColumn("SIT_HD");

            Map(p => p.TextoMensagemAvisoDebito)
                .ToColumn("TEXTO_MENSAGEM_AVISO");

            Map(p => p.TipoImpressao)
                .ToColumn("TIPO_IMPRESSAO");

            Map(p => p.ValorAgua)
                .ToColumn("VL_AGUA");

            Map(p => p.ValorAvisoDebito)
                .ToColumn("VALOR_AVISO_DEBITO");

            Map(p => p.ValorCustoRegulacaoFiscalizacao)
                .ToColumn("IMP_RETENCAO_1");

            Map(p => p.ValorEsgoto)
                .ToColumn("VL_ESGOTO");

            Map(p => p.ValorEsgotoAlternativo)
                .ToColumn("ESGOTO_ALTERNATIVO");

            Map(p => p.ValorRecursosHidricos)
                .ToColumn("RECURSO_HIDRICO");

            Map(p => p.ValorRetecaoImposto)
                .ToColumn("VL_RET_IMPOSTO");

            Map(p => p.ValorTotal)
                .ToColumn("VL_TOTAL");

            Map(p => p.Via)
                .ToColumn("VIA");
            
        }

    }
}
