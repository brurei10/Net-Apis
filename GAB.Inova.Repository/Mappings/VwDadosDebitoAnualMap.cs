﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    public class VwDadosDebitoAnualMap : DommelEntityMap<VwDadosDebitoAnual>
    {
        public VwDadosDebitoAnualMap()
        {
            ToTable("VW_DDS_DBT_NL");

            Map(p => p.Id)
                .ToColumn("SEQ_DECLARACAO");

            Map(p => p.AnoMesCt)
                .ToColumn("ANOMESCT");

            Map(p => p.SeqOriginalPago)
                .ToColumn("SEQORIGINALPAGO");

            Map(p => p.DataVencimento)
                .ToColumn("DATAVENCIMENTO");

            Map(p => p.ValorOriginal)
                .ToColumn("VALORORIGINAL");
        }
    }
}
