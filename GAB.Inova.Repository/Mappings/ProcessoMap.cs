﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    public class ProcessoMap
        : DommelEntityMap<Processo>
    {

        public ProcessoMap()
        {
            ToTable("PROCESSO");

            Map(p => p.Id)
                .ToColumn("ID_PROCESSO")
                .IsKey();

            Map(p => p.AcaoCobranca)
                .ToColumn("PMT_ACAO_COBRANCA");

            Map(p => p.AlteracaoCadastral)
                .ToColumn("PMT_ALT_CADASTRAL");

            Map(p => p.AnoProcesso)
                .ToColumn("ANO_PROCESSO");

            Map(p => p.AutoInfracao)
                .ToColumn("PMT_AUTO_INFRACAO");

            Map(p => p.CumprirTutelaCautelar)
                .ToColumn("CUMPRIR_TUTELA_CAUTELAR");

            Map(p => p.DesmembrarConta)
                .ToColumn("DESMEMBRAR_CONTA");

            Map(p => p.EmissaoConta)
                .ToColumn("PMT_CONTA");

            Map(p => p.EmissaoCorte)
                .ToColumn("PMT_EMISSAO_CORTE");

            Map(p => p.EmissaoNotificacao)
                .ToColumn("PMT_NOTIFICACAO");

            Map(p => p.EmissaoOrdemServico)
                .ToColumn("PMT_ORDEM_SERVICO");

            Map(p => p.EmissaoQuitacaoDebito)
                .ToColumn("PMT_QUITACAO_DEBITO");

            Map(p => p.EmissaoSegundaVia)
                .ToColumn("PMT_SEGUNDA_VIA");

            Map(p => p.Negativacao)
                .ToColumn("PMT_NEGATIVACAO");

            Map(p => p.ParcelamentoDebitos)
                .ToColumn("PMT_PARCELAMENTO");

            Map(p => p.ProcessoNovo)
                .ToColumn("PROCESSO_NOVO");

            Map(p => p.RetificacaoConta)
                .ToColumn("PMT_RETIFICACAO");

            Map(p => p.VincularContaFutura)
                .ToColumn("VINCULAR_CONTA_FUTURA");
        }

    }
}
