﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContaLocalizacaoMap
        : DommelEntityMap<ContaLocalizacao>
    {

        public ContaLocalizacaoMap()
        {
            Map(p => p.Id)
                .ToColumn("SEQ_ORIGINAL")
                .IsKey();

            Map(p => p.Emissao)
                .ToColumn("DATA_EMISSSAO");

            Map(p => p.IdCiclo)
                .ToColumn("ID_CICLO");

            Map(p => p.Rota)
                .ToColumn("ROTA");

            Map(p => p.SiglaEmpresa)
                .ToColumn("SIGLA_EMPRESA");

            Map(p => p.SituacaoConta)
                .ToColumn("SITUACAO_CT");
        }

    }
}
