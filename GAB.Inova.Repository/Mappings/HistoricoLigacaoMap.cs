﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class HistoricoLigacaoMap
        : DommelEntityMap<HistoricoLigacao>
    {

        public HistoricoLigacaoMap()
        {
            ToTable("HISTORICO_LIGACAO");

            Map(p => p.Id)
                .ToColumn("ID_HISTORICO_LIGACAO")
                .IsKey();

            Map(p => p.AnoMes)
                .ToColumn("ANO_MES");

            Map(p => p.DataHistorico)
                .ToColumn("DATA_HIST");

            Map(p => p.DebitoAutomatico)
                .ToColumn("DEBITO_AUTOMATICO");

            Map(p => p.IdCiclo)
                .ToColumn("ID_CICLO");

            Map(p => p.IdGrupoEntrega)
                .ToColumn("ID_GRUPO_ENTREGA");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.SituacaoAgua)
                .ToColumn("SIT_AGUA");

            Map(p => p.SituacaoEsgoto)
                .ToColumn("SIT_ESGOTO");

            Map(p => p.SituacaoHd)
                .ToColumn("SIT_HD");
        }

    }
}
