﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class OrdemServicoDetalheMap : DommelEntityMap<OrdemServicoDetalhe>
    {
        public OrdemServicoDetalheMap()
        {
            ToTable("DET_ORDEM_SERVICO");

            Map(x => x.NroOS).ToColumn("NRO_OS");
            Map(x => x.SeqOs).ToColumn("SEQ_OS");
            Map(x => x.NumeroOS).ToColumn("NUM_OS");
            Map(x => x.CodigoServico).ToColumn("SERV_SOLICITADO_OS");
            Map(x => x.Status).ToColumn("SIT_OS");
            Map(x => x.DataProgramadaExecucaoCliente).ToColumn("DATA_EXEC_PROG_CLIENTE");
            Map(x => x.DataProgramacao).ToColumn("DATA_PROG");
            Map(x => x.DataBaixa).ToColumn("DATA_BAIXA");

            Map(x => x.ValidationResult).Ignore();
        }
    }
}
