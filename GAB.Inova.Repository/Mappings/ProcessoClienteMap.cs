﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ProcessoClienteMap
        : DommelEntityMap<ProcessoCliente>
    {

        public ProcessoClienteMap()
        {
            ToTable("PROCESSO_CLIENTE");

            Map(p => p.Id)
                .ToColumn("ID_PROC_CLIENTE")
                .IsKey();

            Map(p => p.IdContrato)
                .ToColumn("ID_CONTRATO");

            Map(p => p.IdProcesso)
                .ToColumn("ID_PROCESSO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

        }

    }
}
