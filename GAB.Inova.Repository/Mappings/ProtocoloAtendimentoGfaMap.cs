﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ProtocoloAtendimentoGfaMap
        : DommelEntityMap<ProtocoloAtendimentoGfa>
    {

        public ProtocoloAtendimentoGfaMap()
        {
            ToTable("PROTOCOLO_ATEND_GFA");

            Map(p => p.Id)
                .ToColumn("ID_PROTOCOLO_ATEND_GFA")
                .IsKey();

            Map(p => p.DataFinalAtendimento)
                .ToColumn("DT_FIM_ATEND");

            Map(p => p.DataInicioAtendimento)
                .ToColumn("DT_INI_ATEND");

            Map(p => p.IdIntegrador)
                .ToColumn("ID_INTEGRADOR");

            Map(p => p.NumeroProtocoloGfa)
                .ToColumn("NR_PROTOCOLO_GFA");

            Map(p => p.SenhaGfa)
                .ToColumn("NR_SENHA_GFA");

            Map(p => p.UsuarioGfa)
                .ToColumn("NM_USUARIO_GFA");
        }

    }
}
