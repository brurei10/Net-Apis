﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class OrdemServicoEletronicaMap
        : DommelEntityMap<OrdemServicoEletronica>
    {
        public OrdemServicoEletronicaMap()
        {
            ToTable("OS_ELETRONICA");

            Map(x => x.Id).ToColumn("ID_OS_ELETRONICA");
            Map(x => x.OSEletronicaOrigemID).ToColumn("ID_OS_ELETRONICA_PAI");
            Map(x => x.IdentificadorRequisicao).ToColumn("ID_REQUEST");
            Map(x => x.Latitude).ToColumn("LATITUDE");
            Map(x => x.Longitude).ToColumn("LONGITUDE");
            Map(x => x.CentroOperativo).ToColumn("CENTRO_OPERATIVO");
            Map(x => x.IdentificadorOS).ToColumn("ID_UNICO");
            Map(x => x.NumeroOS).ToColumn("NUM_OS");
            Map(x => x.IdentificadorExterno).ToColumn("ID_EXTERNO");
            Map(x => x.CodigoServico).ToColumn("COD_SERVICO");
            Map(x => x.SequencialVinculada).ToColumn("SEQUENCIAL_VINCULADA");
            Map(x => x.DataHoraInicioExecucao).ToColumn("DT_INICIO_EXECUCAO");
            Map(x => x.DataHoraFimExecucao).ToColumn("DT_FIM_EXECUCAO");
            Map(x => x.EquipeExecutora).ToColumn("EQUIPE_EXECUTORA");
            Map(x => x.MotivoAberturaID).ToColumn("ID_MOTIVO_ABERTURA");
            Map(x => x.PossuiBomba).ToColumn("TEM_BOMBA");
            Map(x => x.NumeroNotificacao).ToColumn("NUMERO_NOTIFICACAO");
            Map(x => x.EhTrocaTitularidadeID).ToColumn("EH_TROCA_TITULARIDADE");
            Map(x => x.SituacaoImovelID).ToColumn("ID_SIT_IMOVEL");
            Map(x => x.HidrometroID).ToColumn("ID_HIDROMETRO");
            Map(x => x.LeituraDeRetirada).ToColumn("LEITURA_RETIRADA");
            Map(x => x.HidrometroInstaladoID).ToColumn("ID_HIDROMETRO_INST");
            Map(x => x.LeituraDeInstalacao).ToColumn("LEITURA_INSTALACAO");
            Map(x => x.LocalInstalacaoHidrometroID).ToColumn("ID_LOCAL_INST_HD");
            Map(x => x.PadraoInstalacaoHidrometroID).ToColumn("ID_PADRAO_INST_HD");
            Map(x => x.NumeroSeloVirola).ToColumn("NUM_SELO_VIROLA");
            Map(x => x.NumeroLacreInmetro).ToColumn("NUM_LACRE");
            Map(x => x.Leitura).ToColumn("LEITURA_HD");
            Map(x => x.Laudo).ToColumn("LAUDO");
            Map(x => x.Observacao).ToColumn("OBSERVACAO");
            Map(x => x.NomeOuRazaoSocial).ToColumn("NOME_RAZAO_SOCIAL");
            Map(x => x.CPFCNPJ).ToColumn("CPF_CNPJ");
            Map(x => x.Celular).ToColumn("CELULAR");
            Map(x => x.Telefone).ToColumn("TELEFONE");
            Map(x => x.DataNascimento).ToColumn("DATA_NASCIMENTO");
            Map(x => x.NomeMae).ToColumn("NOME_MAE");
            Map(x => x.NumeroRG).ToColumn("NUM_RG");
            Map(x => x.TipoLogradouro).ToColumn("TIPO_LOGRADOURO");
            Map(x => x.Logradouro).ToColumn("LOGRADOURO");
            Map(x => x.Numero).ToColumn("NUMERO");
            Map(x => x.Complemento).ToColumn("COMPLEMETO");
            Map(x => x.PontoDeReferencia).ToColumn("PONTO_REFER");
            Map(x => x.BairroID).ToColumn("ID_BAIRRO");
            Map(x => x.CodigoMunicipio).ToColumn("COD_MUNICIPIO");
            Map(x => x.CategoriaID).ToColumn("ID_CATEGORIA");
            Map(x => x.SubCategoriaID).ToColumn("ID_SUB_CATEGORIA");
            Map(x => x.EhAreaRisco).ToColumn("EMPRESA");
            Map(x => x.NumeroEconomiasComercial).ToColumn("QTD_ECO_COMERCIAL");
            Map(x => x.NumeroEconomiasResidencial).ToColumn("QTD_ECO_RESIDENCIAL");
            Map(x => x.NumeroEconomiasIndustrial).ToColumn("QTD_ECO_INDUSTRIAL");
            Map(x => x.NumeroEconomiasPublico).ToColumn("QTD_ECO_PUBLICO");
            Map(x => x.DiametroRedeID).ToColumn("ID_DIAMETRO_REDE");
            Map(x => x.DiametroHidrometroID).ToColumn("ID_DIAMETRO_HD");
            Map(x => x.DiametroRamalID).ToColumn("ID_DIAMETRO_RAMAL");
            Map(x => x.FrequenciaUtilizacaoImovelID).ToColumn("ID_FREQ_UTILIZ_IMOVEL");
            Map(x => x.LocalizacaoRamalID).ToColumn("ID_LOCALIZACAO_RAMAL");
            Map(x => x.MaterialRede).ToColumn("MATERIAL_REDE");
            Map(x => x.MaterialRamal).ToColumn("MATERIAL_RAMAL");
            Map(x => x.ProfundidadeRedeID).ToColumn("ID_PROFUNDIDADE_REDE");
            Map(x => x.ProfundidadeRamalID).ToColumn("ID_PROFUNDIDADE_RAMAL");
            Map(x => x.TipoFonteAlternativaID).ToColumn("ID_TIPO_FONTE_ALTERNATIVA");
            Map(x => x.TipoIrregularidadeID).ToColumn("ID_TIPO_IRREGULARIDADE");
            Map(x => x.TipoPavimentoCalcadaID).ToColumn("ID_TIPO_PAV_CALCADA");
            Map(x => x.TipoPavimentoRuaID).ToColumn("ID_TIPO_PAV_RUA");
            Map(x => x.TemCaixaCorreios).ToColumn("TEM_CAIXA_CORREIOS");
            Map(x => x.EhClienteFlutuante).ToColumn("EH_CLIENTE_FLUTUANTE");
            Map(x => x.TemReloajoariaPreEquipada).ToColumn("TEM_RELOAJOARIA_EQUIP");
            Map(x => x.TemFonteAlternativa).ToColumn("TEM_FONTE_ALTERNATIVA");
            Map(x => x.TemBoiaCaixaDagua).ToColumn("TEM_BOIA_CX_DAGUA");
            Map(x => x.TemBoiaSisterna).ToColumn("TEM_BOIA_CISTERNA");
            Map(x => x.TemEquipamentoTelemetriaAcoplada).ToColumn("TEM_TELEMETRIA_ACOPLADA");
            Map(x => x.TemValvulaRetencao).ToColumn("TEM_VALVULA_RETENCAO");
            Map(x => x.NumeroFuncionarios).ToColumn("QTD_FUNCIONARIOS");
            Map(x => x.NumeroMoradores).ToColumn("QTD_MORADORES");
            Map(x => x.QuantidadeBanheirosOuLavabos).ToColumn("QTD_BANHEIROS_LAVABOS");
            Map(x => x.QuantidadeLojas).ToColumn("QTD_LOJAS");
            Map(x => x.QuantidadeQuartosImovel).ToColumn("QTD_QUARTOS");
            Map(x => x.QuantidadeSalas).ToColumn("QTD_SALAS");
            Map(x => x.VazaoComBomba).ToColumn("VAZAO_COM_BOMBA");
            Map(x => x.VazaoSemBomba).ToColumn("VAZAO_SEM_BOMBA");
            Map(x => x.VolumeCaixaDagua).ToColumn("VOLUME_CAIXA_DAGUA");
            Map(x => x.VolumeCisterna).ToColumn("VOLUME_CISTENA");
            Map(x => x.VolumePiscina).ToColumn("VOLUME_PISCINA");
            Map(x => x.MotivoBaixaID).ToColumn("ID_MOTIVO_BAIXA");
            Map(x => x.TesteCloro).ToColumn("TESTE_CLORO");

            Map(x => x.Fotos).Ignore();
            Map(x => x.ValidationResult).Ignore();
            Map(x => x.EhAssociada).Ignore();
        }
    }
}
