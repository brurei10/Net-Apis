﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TempHistoricoConsumoMap
        : DommelEntityMap<TempHistoricoConsumo>
    {

        public TempHistoricoConsumoMap()
        {
            ToTable("TEMP_HISTORICO_CONSUMO");

            Map(p => p.Id)
                .ToColumn("ID_TEMP_HISTORICO_CONSUMO")
                .IsKey();

            Map(p => p.AnoMes)
                .ToColumn("ANOMES");

            Map(p => p.Consumo)
                .ToColumn("CONSUMO");

            Map(p => p.IdUsuario)
                .ToColumn("ID_USUARIO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.QtdeDias)
                .ToColumn("QTDEDIAS");

            Map(p => p.Referencia)
                .ToColumn("REFERENCIA");
        }

    }
}
