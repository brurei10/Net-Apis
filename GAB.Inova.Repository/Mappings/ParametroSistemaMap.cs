﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ParametroSistemaMap
        : DommelEntityMap<ParametroSistema>
    {

        public ParametroSistemaMap()
        {
            ToTable("TAB_PARAMETRO");

            Map(p => p.Id)
                .ToColumn("ID_PARM")
                .IsKey();

            Map(p => p.Descricao)
                .ToColumn("DESCR_PARAMETRO");

            Map(p => p.IdUnidadeMedida)
                .ToColumn("UNID_MEDIDA");

            Map(p => p.Valor)
                .ToColumn("VL_PARAMETRO");

            Map(p => p.DataInclusao)
                .ToColumn("DATA_INCLUSAO_PARM");

            Map(p => p.Situacao)
                .ToColumn("TIPO_SIT");
            
        }

    }
}
