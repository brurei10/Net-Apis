﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class EstadoMap
        : DommelEntityMap<Estado>
    {

        public EstadoMap()
        {
            ToTable("ESTADO");

            Map(p => p.Id)
                .ToColumn("ID_ESTADO")
                .IsKey();

            Map(p => p.Nome)
                .ToColumn("NOME_ESTADO");
        }

    }
}
