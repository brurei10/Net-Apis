﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class OrdemServicoAnexoExternoMap
        : DommelEntityMap<OrdemServicoAnexoExterno>
    {
        public OrdemServicoAnexoExternoMap()
        {
            ToTable("OS_ANEXO_EXTERNO");

            Map(p => p.Id)
                .ToColumn("ID_OS_ANEXO_EXTERNO")
                .IsKey();

            Map(p => p.OSEletronicaID)
                .ToColumn("ID_OS_ELETRONICA");

            Map(p => p.NumeroOS)
                .ToColumn("NUM_OS");

            Map(p => p.Descricao)
                .ToColumn("DESCRICAO");

            Map(p => p.Identificador)
                .ToColumn("IDENTIFICADOR");

            Map(p => p.ValidationResult)
                .Ignore();
        }
    }
}
