﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class FaturamentoMap
        : DommelEntityMap<Faturamento>
    {

        public FaturamentoMap()
        {
            ToTable("FATURAMENTO");

            Map(p => p.Id)
                .ToColumn("ID_FATURAMENTO")
                .IsKey();

            Map(p => p.AnoMesFaturamento)
                .ToColumn("ANO_MES_FATURAMENTO");

            Map(p => p.Categoria)
                .ToColumn("CATEGORIA");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.SeqOriginal)
                .ToColumn("SEQ_ORIGINAL");

            Map(p => p.TipoServicoFaturado)
                .ToColumn("TIPO_SERV_FATURAMENTO");

            Map(p => p.ValorFaturamento)
                .ToColumn("VL_FATURAMENTO");

            Map(p => p.VolumePorEconomiaFaturado)
                .ToColumn("VOL_POR_ECO_FAT");

            Map(p => p.VolumeTotalFaturado)
                .ToColumn("VOL_TOTAL_FAT");
        }

    }
}
