﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ProtocoloAtendimentoDetalheMap
        : DommelEntityMap<ProtocoloAtendimentoDetalhe>
    {

        public ProtocoloAtendimentoDetalheMap()
        {
            ToTable("PROTOCOLO_ATEND_DETALHE");

            Map(p => p.Id)
                .ToColumn("ID_PROT_ATEND_DET")
                .IsKey();

            Map(p => p.IdNatureza)
                .ToColumn("ID_NATUREZA");

            Map(p => p.IdProtocoloAtendimento)
                .ToColumn("ID_PROTOCOLO_ATEND");

            Map(p => p.IdRotina)
                .ToColumn("ID_ROTINA");

            Map(p => p.NumeroDoc)
                .ToColumn("NUMERO_DOC");

            Map(p => p.NumeroOs)
                .ToColumn("NUMERO_OS");

            Map(p => p.TipoProtocolo)
                .ToColumn("TIPO_PROTOCOLO");
        }

    }
}
