﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class LeituraMap : DommelEntityMap<Leitura>
    {
        public LeituraMap()
        {
            ToTable("TAB_METERWISE_LEITURA");

            Map(p => p.MeterSerialNumber)
                .ToColumn("ID_HD");

            Map(p => p.Date)
                .ToColumn("DATA_LEITURA");

            Map(p => p.Volume)
                .ToColumn("LEITURA");


        }
    }
}
