﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class UsuarioMap
        : DommelEntityMap<Usuario>
    {

        public UsuarioMap()
        {
            ToTable("USUARIO");

            Map(p => p.Id)
                .ToColumn("ID_USUARIO")
                .IsKey();

            Map(p => p.Email)
                .ToColumn("EMAIL");

            Map(p => p.NomeAcesso)
                .ToColumn("NOME_ACESSO");

            Map(p => p.NomeCompleto)
                .ToColumn("NOME_COMPLETO");

            Map(p => p.Situacao)
                .ToColumn("SITUACAO");

            Map(p => p.SituacaoAd)
                .ToColumn("SITUACAO_AD");
        }

    }
}
