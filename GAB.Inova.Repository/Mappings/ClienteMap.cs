﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ClienteMap
        : DommelEntityMap<Cliente>
    {

        public ClienteMap()
        {
            ToTable("CLIENTE");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA")
                .IsKey();

            Map(p => p.Localizacao)
                .ToColumn("LOCALIZACAO");

            Map(p => p.DigitoLocalizacao)
                .ToColumn("DV_LOCALIZ");

            Map(p => p.Nome)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.IdTipoFatura)
                .ToColumn("TIPO_ENTREGA");

            Map(p => p.IdTipoCliente)
                .ToColumn("TIPO_CLIENTE");

            Map(p => p.SituacaoAgua)
                .ToColumn("SIT_AGUA");

            Map(p => p.SituacaoEsgoto)
                .ToColumn("SIT_ESGOTO");

            Map(p => p.SituacaoHd)
                .ToColumn("SIT_HD");

            Map(p => p.DiaVencimentoConta)
                .ToColumn("VENC_CONTA");

            Map(p => p.DigitoMatricula)
                .ToColumn("DV_MATR");

            Map(p => p.DebitoAutomatico)
                .ToColumn("DEBITO_AUTOMATICO");

            Map(p => p.IdGrupoEntrega)
                .ToColumn("GRUPO_ENTREGA");
        }

    }
}
