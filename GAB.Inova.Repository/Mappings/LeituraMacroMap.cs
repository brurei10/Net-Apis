﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class LeituraMacroMap : DommelEntityMap<LeituraMacro>
    {
        public LeituraMacroMap()
        {
            ToTable("TAB_METERWISE_LEITURA");

            Map(p => p.Code)
                .ToColumn("ID_HD");

            Map(p => p.Date)
                .ToColumn("DATA_LEITURA");

            Map(p => p.Volume)
                .ToColumn("LEITURA");


        }
    }
}

