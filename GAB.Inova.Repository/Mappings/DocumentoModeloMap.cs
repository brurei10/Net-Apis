﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class DocumentoModeloMap : DommelEntityMap<DocumentoModelo>
    {
        public DocumentoModeloMap()
        {
            ToTable("DOCUMENTO_MODELO");

            Map(p => p.Id)
                .ToColumn("ID_DOCUMENTO_MODELO")
                .IsKey();

            Map(p => p.IdDocumentoModeloTipo)
                .ToColumn("ID_DOCUMENTO_MODELO_TIPO");

            Map(p => p.DataInicio)
                .ToColumn("DATA_INICIO");

            Map(p => p.DataInclusao)
                .ToColumn("DATA_INCLUSAO");

            Map(p => p.Situacao)
                .ToColumn("SITUACAO");

            Map(p => p.HeaderHtml)
                .ToColumn("HEADER_HTML");

            Map(p => p.BodyHtml)
                .ToColumn("BODY_HTML");

            Map(p => p.FooterHtml)
                .ToColumn("FOOTER_HTML");

            Map(p => p.CssHtml)
                .ToColumn("CSS_HTML");

            Map(p => p.HeaderTamanho)
                .ToColumn("HEADER_TAMANHO");

            Map(p => p.FooterTamanho)
                .ToColumn("FOOTER_TAMANHO");

            Map(p => p.DocTamanho)
                .ToColumn("DOC_TAMANHO");

            Map(p => p.DocOrientacao)
                .ToColumn("DOC_ORIENTACAO");

            Map(p => p.DocMargemSuperior)
                .ToColumn("DOC_MARGEM_SUPERIOR");

            Map(p => p.DocMargemInferior)
                .ToColumn("DOC_MARGEM_INFERIOR");

            Map(p => p.DocMargemEsquerda)
                .ToColumn("DOC_MARGEM_ESQUERDA");

            Map(p => p.DocMargemDireita)
                .ToColumn("DOC_MARGEM_DIREITA");

            Map(p => p.Observacao)
                .ToColumn("OBSERVACAO");

            Map(p => p.IdUsuarioCriacao)
                .ToColumn("ID_USUARIO_CRIACAO");

            Map(p => p.DataCriacaoRegistro)
                .ToColumn("DATA_CRIACAO_REGISTRO");

            Map(p => p.IdUsuarioUltimaModif)
                .ToColumn("ID_USUARIO_ULTIMA_MODIF");

            Map(p => p.DataUltimaModificacao)
                .ToColumn("DATA_ULTIMA_MODIFICACAO");

            Map(p => p.Origem)
                .ToColumn("ORIGEM");
        }
    }
}