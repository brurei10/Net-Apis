﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ProtocoloAtendimentoMap
        : DommelEntityMap<ProtocoloAtendimento>
    {

        public ProtocoloAtendimentoMap()
        {
            ToTable("PROTOCOLO_ATENDIMENTO");

            Map(p => p.Id)
                .ToColumn("ID_PROTOCOLO_ATENDIMENTO")
                .IsKey();

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.DataInicio)
                .ToColumn("DATA_INICIO");

            Map(p => p.DataFim)
                .ToColumn("DATA_FIM");

            Map(p => p.IdTipoAtendimento)
               .ToColumn("ID_TIPO_ATENDIMENTO");

            Map(p => p.CpfCnpj)
               .ToColumn("CPF_CNPJ");

            Map(p => p.Observacao)
               .ToColumn("OBS");

            Map(p => p.Solicitante)
               .ToColumn("NOME_SOLICITANTE");

            Map(p => p.Telefone)
               .ToColumn("TELEFONE");

            Map(p => p.IdLocalAtendimento)
               .ToColumn("ID_LOCAL_ATENDIMENTO");

            Map(p => p.TipoProtocolo)
               .ToColumn("TIPO_PROTOCOLO");

            Map(p => p.Prexifo)
               .ToColumn("PREFIXO_PROTOCOLO");

            Map(p => p.Sequencia)
               .ToColumn("SEQUENCIA_PROTOCOLO");

            Map(p => p.Numero)
               .ToColumn("NUM_PROTOCOLO");

            Map(p => p.OrigemInova)
               .ToColumn("ORIGEM_INOVA");

            Map(p => p.NumeroProtocoloPai)
               .ToColumn("NR_PROTOCOLO_PAI");

            Map(p => p.DataIntegracao)
               .ToColumn("DT_INTEGRACAO");

            Map(p => p.IdContrato)
               .ToColumn("ID_CONTRATO");

            Map(p => p.IdProtocoloAtendimentoGfa)
               .ToColumn("ID_PROTOCOLO_ATEND_GFA");

            Map(p => p.IdUsuario)
               .ToColumn("ID_USUARIO");
        }

    }
}
