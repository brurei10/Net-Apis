﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class MensagemIntegradorMovimentoMap
        : DommelEntityMap<MensagemIntegradorMovimento>
    {
        public MensagemIntegradorMovimentoMap()
        {
            ToTable("MENSAGEM_INTEGRADOR_MOV");

            Map(p => p.Id)
                .ToColumn("ID_MENSAGEM_INTEGRADOR_MOV")
                .IsKey();

            Map(p => p.IdMensagemIntegrador)
                .ToColumn("ID_MENSAGEM_INTEGRADOR");

            Map(p => p.DataInclusao)
                .ToColumn("DT_INCLUSAO");

            Map(p => p.Status)
                .ToColumn("STATUS_TIPO");

            Map(p => p.Mensagem)
                .ToColumn("MENSAGEM");
        }
    }
}
