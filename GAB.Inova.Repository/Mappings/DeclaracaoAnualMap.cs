﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class DeclaracaoAnualMap : DommelEntityMap<DeclaracaoAnual>
    {

        public DeclaracaoAnualMap()
        {
            ToTable("DECLARACAO_ANUAL");

            Map(p => p.Id)
                .ToColumn("SEQ_DECLARACAO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.AnoRef)
                .ToColumn("ANO_REF");

            Map(p => p.DataEnvio)
                .ToColumn("DATA_ENVIO");

            Map(p => p.DataRetorno)
                .ToColumn("DATA_RETORNO");

            Map(p => p.AnoMesRef)
                .ToColumn("ANO_MES_REF");

            Map(p => p.IndiceA)
                .ToColumn("INDICEA");

            Map(p => p.TipoDeclaracao)
                .ToColumn("TIPO_DECLARACAO");

            Map(p => p.Impressao)
                .ToColumn("IMPRESSAO");

            Map(p => p.TipoDeclaraParcial)
                .ToColumn("TIPO_DECLARA_PARCIAL");
        }
    }
}