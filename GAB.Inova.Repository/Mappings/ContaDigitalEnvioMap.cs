﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContaDigitalEnvioMap
        : DommelEntityMap<ContaDigitalEnvio>
    {
        public ContaDigitalEnvioMap()
        {
            ToTable("CONTA_DIGITAL_ENVIO");

            Map(p => p.Id)
                .ToColumn("ID_CONTA_DIGITAL_ENVIO")
                .IsKey();

            Map(p => p.SeqOriginal)
                .ToColumn("SEQ_ORIGINAL");

            Map(p => p.DataEnvio)
                .ToColumn("DT_ENVIO");

            Map(p => p.Status)
                .ToColumn("STATUS");

            Map(p => p.IdMensagemIntegrador)
                .ToColumn("ID_MENSAGEM_INTEGRADOR");
          
        }
    }
}
