﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class BairroMap
        : DommelEntityMap<Bairro>
    {

        public BairroMap()
        {
            ToTable("BAIRRO");

            Map(p => p.Id)
                .ToColumn("ID_BAIRRO")
                .IsKey();

            Map(p => p.Nome)
                .ToColumn("NOME_BAIRRO");
        }

    }
}
