﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContaDigitalEnvioMovimentoMap
        : DommelEntityMap<ContaDigitalEnvioMovimento>
    {
        public ContaDigitalEnvioMovimentoMap()
        {
            ToTable("CONTA_DIGITAL_ENVIO_MOV");

            Map(p => p.Id)
                .ToColumn("ID_CONTA_DIGITAL_ENVIO_MOV")
                .IsKey();

            Map(p => p.IdContaDigitalEnvio)
                .ToColumn("ID_CONTA_DIGITAL_ENVIO");

            Map(p => p.DataInclusao)
                .ToColumn("DATA_INCLUSAO");

            Map(p => p.Status)
                .ToColumn("STATUS");           
        }
    }
}
