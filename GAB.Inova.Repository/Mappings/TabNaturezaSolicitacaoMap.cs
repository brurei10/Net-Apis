﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TabNaturezaSolicitacaoMap
        : DommelEntityMap<TabNaturezaSolicitacao>
    {

        public TabNaturezaSolicitacaoMap()
        {
            ToTable("TAB_NATUREZA_SOLICITACAO");

            Map(p => p.Id)
                .ToColumn("ID_NATUREZA")
                .IsKey();

            Map(p => p.Descricao)
                .ToColumn("DESCRICAO");
        }

    }
}
