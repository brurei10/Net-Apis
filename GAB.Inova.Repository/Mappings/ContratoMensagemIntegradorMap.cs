﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ContratoMensagemIntegradorMap
        : DommelEntityMap<ContratoMensagemIntegrador>
    {

        public ContratoMensagemIntegradorMap()
        {
            ToTable("CONTRATO_MENSAGEM_INTEGRA");

            Map(p => p.Id)
                .ToColumn("ID_CONTRATO_MENSAGEM_INTEGRA")
                .IsKey();

            Map(p => p.DataInclusao)
                .ToColumn("DT_INCLUSAO");

            Map(p => p.IdMensagemIntegrador)
                .ToColumn("ID_MENSAGEM_INTEGRADOR");

            Map(p => p.IdContrato)
                .ToColumn("ID_CONTRATO");
        }

    }
}
