﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class GRPMap : DommelEntityMap<GRP>
    {
        public GRPMap()
        {
            ToTable("GRP");

            Map(p => p.Id)
                .ToColumn("SEQ_ORIGINAL")
                .IsKey();

            Map(p => p.DataHoraEmissaoGRP)
                .ToColumn("DATA_HORA_EMISSAO_GRP");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Localizacao)
                .ToColumn("LOCALIZACAO");

            Map(p => p.NroOs)
                .ToColumn("NRO_OS");

            Map(p => p.ValorGRP)
                .ToColumn("VL_GRP");

            Map(p => p.DataVencimentoGRP)
                .ToColumn("DATA_VENC_GRP");

            Map(p => p.DataValorGRP)
                .ToColumn("DATA_VL_GRP");

            Map(p => p.NomeSolicitante)
                .ToColumn("NOME_SOLICITANTE");

            Map(p => p.IdentidadeSolicitante)
                .ToColumn("IDENT_SOLICITANTE");

            Map(p => p.Observacao)
                .ToColumn("OBSERVACAO");

            Map(p => p.IdUsuario)
                .ToColumn("ID_USUARIO");

            Map(p => p.IdSolicitacao)
                .ToColumn("ID_SOLICITACAO");

            Map(p => p.IdProtocoloAtendimento)
                .ToColumn("ID_PROTOCOLO_ATENDIMENTO");
        }
    }
}
