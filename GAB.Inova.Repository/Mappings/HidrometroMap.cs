﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class HidrometroMap : DommelEntityMap<TabMeterwiseHD>
    {
        public HidrometroMap()
        {
            ToTable("TAB_METERWISE_HD");

            Map(p => p.SerialNumber)
                .ToColumn("ID_HD");

            Map(p => p.ClientId)
                .ToColumn("MATRICULA");

            Map(p => p.Location)
                .ToColumn("SISTEMA_ABASTECIMENTO");

            Map(p => p.Area)
                .ToColumn("AREA");

            Map(p => p.Diameter)
                .ToColumn("CAPACIDADE_HD");

            Map(p => p.ManufactureYear)
                .ToColumn("ANO_FABRICACAO");

            Map(p => p.InstallDate)
                .ToColumn("DATA_INSTALACAO");

            Map(p => p.ServiceType)
                .ToColumn("CATEGORIA");

            Map(p => p.EquipmentType)
                .ToColumn("MODELO_HD");
  
            Map(p => p.Brand)
                .ToColumn("MARCA_HD");

            Map(p => p.Model)
                .ToColumn("MODELO_HD");

            Map(p => p.PrecisionClass)
                .ToColumn("CLASSE_HD");

            Map(p => p.MinimumFlow)
                .ToColumn("VAZAO_HD");

            Map(p => p.NominalFlow)
                .ToColumn("VAZAO_HD");

            Map(p => p.MaximumFlow)
                .ToColumn("VAZAO_HD_DOBRO");

            Map(p => p.Condition)
                .ToColumn("CONDICAO_HD");

        }
    }
}
