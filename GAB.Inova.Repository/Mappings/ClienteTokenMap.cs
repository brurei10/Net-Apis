﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ClienteTokenMap
        : DommelEntityMap<ClienteToken>
    {

        public ClienteTokenMap()
        {
            ToTable("CLIENTE_TOKEN");

            Map(p => p.Id)
                .ToColumn("ID_CLIENTE_TOKEN")
                .IsKey();

            Map(p => p.DataConfirmacao)
                .ToColumn("DT_CONFIRMACAO");

            Map(p => p.DataExpiracao)
                .ToColumn("DT_EXPIRACAO");

            Map(p => p.DataInclusao)
                .ToColumn("DT_INCLUSAO");

            Map(p => p.IdProtocoloAtendimento)
                .ToColumn("ID_PROTOCOLO_ATENDIMENTO");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Sistema)
                .ToColumn("SISTEMA");

            Map(p => p.Tipo)
                .ToColumn("TIPO");

            Map(p => p.Token)
                .ToColumn("TOKEN");
        }

    }
}
