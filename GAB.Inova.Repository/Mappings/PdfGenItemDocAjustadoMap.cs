﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class PdfGenItemDocAjustadoMap
        : DommelEntityMap<PdfGenItemDocAjustado>
    {

        public PdfGenItemDocAjustadoMap()
        {
            ToTable("PDFGEN_ITEM_DOC_AJUSTADO");

            Map(p => p.EstiloMargemEsquerda)
                .ToColumn("ESTILO_MARGEM_ESQUERDA");

            Map(p => p.EstiloMargemTopo)
                .ToColumn("ESTILO_MARGEM_TOPO");

            Map(p => p.IdImpressora)
                .ToColumn("ID_IMPRESSORA");

            Map(p => p.IdItemDocumento)
                .ToColumn("ID_ITEM_DOCUMENTO");

            Map(p => p.IdModelo)
                .ToColumn("ID_MODELO");
        }

    }
}
