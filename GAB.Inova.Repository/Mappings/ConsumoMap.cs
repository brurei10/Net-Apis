﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ConsumoMap
        : DommelEntityMap<Consumo>
    {

        public ConsumoMap()
        {
            ToTable("CONSUMO");

            Map(p => p.Id)
                .ToColumn("ANO_MES_LEITURA")
                .IsKey();

            Map(p => p.Faturado)
                .ToColumn("CONFAT");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.QtdeDiasLeitura)
                .ToColumn("QTD_DIAS_LEITURA");
        }

    }
}
