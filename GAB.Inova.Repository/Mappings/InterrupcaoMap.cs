﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class InterrupcaoMap : DommelEntityMap<Interrupcao>
    {
        public InterrupcaoMap()
        {
            ToTable("INTERRUPCAO");

            Map(p => p.Id)
                .ToColumn("ID_INTERRUPCAO")
                .IsKey();

            Map(p => p.IdStatus)
                .ToColumn("ID_INTERRUPCAO_STATUS");

            Map(p => p.DataHoraInicio)
                .ToColumn("DT_HR_INICIO");

            Map(p => p.DataHoraFim)
                .ToColumn("DT_HR_FIM");
        }
    }
}