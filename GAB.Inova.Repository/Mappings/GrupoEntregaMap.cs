﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class GrupoEntregaMap
        : DommelEntityMap<GrupoEntrega>
    {

        public GrupoEntregaMap()
        {
            ToTable("GRUPO_ENTREGA");

            Map(p => p.Id)
                .ToColumn("ID_GRUPO_ENTREGA")
                .IsKey();

            Map(p => p.Descricao)
                .ToColumn("DESCR_GRUPO_ENTREGA");

            Map(p => p.ImprimeLis)
                .ToColumn("IMPRIME_LIS");

            Map(p => p.Situacao)
                .ToColumn("SITUACAO");
        }

    }
}
