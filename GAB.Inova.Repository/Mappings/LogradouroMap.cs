﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class LogradouroMap
        : DommelEntityMap<Logradouro>
    {

        public LogradouroMap()
        {
            ToTable("LOGRADOURO");

            Map(p => p.Id)
                .ToColumn("ID_LOGRADOURO")
                .IsKey();

            Map(p => p.Cep)
                .ToColumn("LOGRADOURO_CEP");

            Map(p => p.Nome)
                .ToColumn("NOME_LOGRADOURO");
        }
    }
}
