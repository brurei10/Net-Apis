﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    public class VwDadosClienteInterrupcaoMap : DommelEntityMap<VwDadosClienteInterrupcao>
    {
        public VwDadosClienteInterrupcaoMap()
        {
            ToTable("VW_DDS_CLNT_INTRRPC");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.NomeCliente)
                .ToColumn("NOMECLIENTE");

            Map(p => p.IdLogradouro)
                .ToColumn("IDLOGRADOURO");

            Map(p => p.IdBairro)
                .ToColumn("IDBAIRRO");

            Map(p => p.IdCidade)
                .ToColumn("IDCIDADE");

            Map(p => p.IdSetorOperacional)
                .ToColumn("IDSETOROPERACIONAL");
        }
    }
}
