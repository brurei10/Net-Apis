﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TempImprimeContaMap
        : DommelEntityMap<TempImprimeConta>
    {

        public TempImprimeContaMap()
        {
            ToTable("TEMP_IMPRIME_CONTA");

            Map(p => p.Id)
                .ToColumn("ID_TEMP_IMPRIME_CONTA")
                .IsKey();

            Map(p => p.AnoMesCt)
                .ToColumn("ANO_MES_CT");

            Map(p => p.CodigoBarra)
                .ToColumn("COD_BARRA");

            Map(p => p.Complemento)
                .ToColumn("COMPLEMENTO");

            Map(p => p.CompoeCt01)
                .ToColumn("COMP_CT_01");

            Map(p => p.CompoeCt02)
                .ToColumn("COMP_CT_02");

            Map(p => p.CompoeCt03)
                .ToColumn("COMP_CT_03");

            Map(p => p.CompoeCt04)
                .ToColumn("COMP_CT_04");

            Map(p => p.CompoeCt05)
                .ToColumn("COMP_CT_05");

            Map(p => p.CompoeCt06)
                .ToColumn("COMP_CT_06");

            Map(p => p.CompoeCt07)
                .ToColumn("COMP_CT_07");

            Map(p => p.CompoeCt08)
                .ToColumn("COMP_CT_08");

            Map(p => p.CompoeCt09)
                .ToColumn("COMP_CT_09");

            Map(p => p.CompoeCt10)
                .ToColumn("COMP_CT_10");

            Map(p => p.CompoeCt11)
                .ToColumn("COMP_CT_11");

            Map(p => p.CompoeCt12)
                .ToColumn("COMP_CT_12");

            Map(p => p.CompoeCt13)
                .ToColumn("COMP_CT_13");

            Map(p => p.CompoeCt14)
                .ToColumn("COMP_CT_14");

            Map(p => p.CompoeCt15)
                .ToColumn("COMP_CT_15");

            Map(p => p.CompoeCt16)
                .ToColumn("COMP_CT_16");

            Map(p => p.CompoeVl01)
                .ToColumn("COMP_VL_01");

            Map(p => p.CompoeVl02)
                .ToColumn("COMP_VL_02");

            Map(p => p.CompoeVl03)
                .ToColumn("COMP_VL_03");

            Map(p => p.CompoeVl04)
                .ToColumn("COMP_VL_04");

            Map(p => p.CompoeVl05)
                .ToColumn("COMP_VL_05");

            Map(p => p.CompoeVl06)
                .ToColumn("COMP_VL_06");

            Map(p => p.CompoeVl07)
                .ToColumn("COMP_VL_07");

            Map(p => p.CompoeVl08)
                .ToColumn("COMP_VL_08");

            Map(p => p.CompoeVl09)
                .ToColumn("COMP_VL_09");

            Map(p => p.CompoeVl10)
                .ToColumn("COMP_VL_10");

            Map(p => p.CompoeVl11)
                .ToColumn("COMP_VL_11");

            Map(p => p.CompoeVl12)
                .ToColumn("COMP_VL_12");

            Map(p => p.CompoeVl13)
                .ToColumn("COMP_VL_13");

            Map(p => p.CompoeVl14)
                .ToColumn("COMP_VL_14");

            Map(p => p.CompoeVl15)
                .ToColumn("COMP_VL_15");

            Map(p => p.CompoeVl16)
                .ToColumn("COMP_VL_16");

            Map(p => p.ConsumoCredito)
                .ToColumn("CONS_CREDITO");

            Map(p => p.ConsumoFaturado)
                .ToColumn("CONS_FAT");

            Map(p => p.ConsumoMedido)
                .ToColumn("CONS_MEDIDO");

            Map(p => p.ConsumoPipa)
                .ToColumn("CONS_PIPA");

            Map(p => p.ConsumoResidual)
                .ToColumn("CONS_RESIDUAL");

            Map(p => p.CpfCnpj)
                .ToColumn("CPF_CNPJ");

            Map(p => p.DataEmissao)
                .ToColumn("DATA_EMISSAO");

            Map(p => p.DataLeituraAnterior)
                .ToColumn("DATA_LEIT_ANT");

            Map(p => p.DataLeituraAtual)
                .ToColumn("DATA_LEIT_ATUAL");

            Map(p => p.DataProximaLeitura)
                .ToColumn("DATA_PROX_LEIT");

            Map(p => p.DataVencimento)
                .ToColumn("DATA_VENC");

            Map(p => p.DigitoMatricula)
                .ToColumn("DV_MATR");

            Map(p => p.Economia)
                .ToColumn("ECONOMIA");

            Map(p => p.EconomiaComercial)
                .ToColumn("ECO_COM");

            Map(p => p.EconomiaIndustrial)
                .ToColumn("ECO_IND");

            Map(p => p.EconomiaPublica)
                .ToColumn("ECO_PUB");

            Map(p => p.EconomiaResidencial)
                .ToColumn("ECO_RES");

            Map(p => p.Endereco)
                .ToColumn("ENDERECO");

            Map(p => p.IdHd)
                .ToColumn("ID_HD");

            Map(p => p.IdSistemaAbastecimento)
                .ToColumn("ID_SISTEMA_ABASTECIMENTO");

            Map(p => p.IdSolicitacao)
                .ToColumn("ID_SOLICITACAO");

            Map(p => p.IdTarifa)
                .ToColumn("ID_TARIFA");

            Map(p => p.IdUsuario)
                .ToColumn("ID_USUARIO");

            Map(p => p.ImpressaoCodigoBarra)
                .ToColumn("IMP_COD_BARRA");

            Map(p => p.ImpressaoConsumo)
                .ToColumn("IMPDADOSCONSUMO");

            Map(p => p.InscricaoMunicipalEstadual)
                .ToColumn("INSC_MUN_EST");

            Map(p => p.LeituraAnterior)
                .ToColumn("LEITURA_ANT");

            Map(p => p.LeituraAtual)
                .ToColumn("LEITURA_ATUAL");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Mensagem)
                .ToColumn("MENSAGEM");

            Map(p => p.ModoFaturamento)
                .ToColumn("MODO_FAT");

            Map(p => p.NomeCliente)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.NomeTipoEntrega)
                .ToColumn("NOME_TIPO_ENTREGA");

            Map(p => p.NomeTipoFaturamento)
                .ToColumn("NOME_TIPO_FATURAMENTO");

            Map(p => p.NomeUsuario)
                .ToColumn("NOME_USUARIO");

            Map(p => p.Observacao)
                .ToColumn("OBSERVACAO");

            Map(p => p.PercentualAgua)
                .ToColumn("PERC_AGUA");

            Map(p => p.PercentualEsgoto)
                .ToColumn("PERC_ESGOTO");

            Map(p => p.QtdeDiasConsumo)
                .ToColumn("QTDE_DIAS_CONS");

            Map(p => p.Rota)
                .ToColumn("ROTA");

            Map(p => p.Roteirizacao)
                .ToColumn("ROTEIRIZACAO");

            Map(p => p.SeqOriginal)
                .ToColumn("SEQ_ORIGINAL");

            Map(p => p.SistemaAbastecimento)
                .ToColumn("SISTEMA_ABASTECIMENTO");

            Map(p => p.SituacaoEsgoto)
                .ToColumn("SIT_ESGOTO");

            Map(p => p.SituacaoHd)
                .ToColumn("SIT_HD");

            Map(p => p.TipoImpressao)
                .ToColumn("TIPO_IMPRESSAO");

            Map(p => p.ValorAgua)
                .ToColumn("VL_AGUA");

            Map(p => p.ValorEsgotoAlternativo)
                .ToColumn("ESGOTO_ALTERNATIVO");

            Map(p => p.ValorRecursosHidricos)
                .ToColumn("RECURSO_HIDRICO");

            Map(p => p.ValorRetecaoImposto)
                .ToColumn("VL_RET_IMPOSTO");

            Map(p => p.ValorTotal)
                .ToColumn("VL_TOTAL");

            Map(p => p.Via)
                .ToColumn("VIA");
        }

    }
}
