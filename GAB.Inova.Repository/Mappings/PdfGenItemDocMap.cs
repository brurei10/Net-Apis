﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class PdfGenItemDocMap
        : DommelEntityMap<PdfGenItemDoc>
    {

        public PdfGenItemDocMap()
        {
            ToTable("PDFGEN_ITEM_DOC");

            Map(p => p.Id)
                .ToColumn("ID_ITEM_DOCUMENTO")
                .IsKey();

            Map(p => p.Descricao)
                .ToColumn("DESCRICAO_ITEM_DOCUMENTO");

            Map(p => p.EstiloAlinhamentoHorizontal)
                .ToColumn("ESTILO_ALINHAMENTO_HORIZONTAL");

            Map(p => p.EstiloAlinhamentoVertical)
                .ToColumn("ESTILO_ALINHAMENTO_VERTICAL");

            Map(p => p.EstiloAltura)
                .ToColumn("ESTILO_ALTURA");

            Map(p => p.EstiloEspacoQuebraLinha)
                .ToColumn("ESTILO_ESPACO_QUEBRAS_LINHA");

            Map(p => p.EstiloFonteColor)
                .ToColumn("ESTILO_FONTE_COLOR");

            Map(p => p.EstiloFonteFormato)
                .ToColumn("ESTILO_FONTE_FORMAT");

            Map(p => p.EstiloFonteNome)
                .ToColumn("ESTILO_FONTE_NOME");

            Map(p => p.EstiloFonteTamanho)
                .ToColumn("ESTILO_FONTE_TAMANHO");

            Map(p => p.EstiloLargura)
                .ToColumn("ESTILO_LARGURA");

            Map(p => p.EstiloMargemEsquerda)
                .ToColumn("ESTILO_MARGEM_ESQUERDA");

            Map(p => p.EstiloMargemTopo)
                .ToColumn("ESTILO_MARGEM_TOPO");

            Map(p => p.EstiloPosicao)
                .ToColumn("ESTILO_POSICAO");

            Map(p => p.EstiloPosicaoEsquerda)
                .ToColumn("ESTILO_POSICAO_ESQUERDA");

            Map(p => p.EstiloPosicaoTopo)
                .ToColumn("ESTILO_POSICAO_TOPO");

            Map(p => p.EstiloRepeticaoEspacamento)
                .ToColumn("ESTILO_REPETICAO_ESPACAMENTO");

            Map(p => p.EstiloRepeticaoMaxima)
                .ToColumn("ESTILO_REPETICAO_MAXIMO");

            Map(p => p.EstiloRepeticaoOrientacao)
                .ToColumn("ESTILO_REPETICAO_ORIENTACAO");

            Map(p => p.EstiloSombra)
                .ToColumn("ESTILO_SOMBRA");

            Map(p => p.IdentacaoNome)
                .ToColumn("IDENTED_NOME_ITEM_DOCUMENTO");

            Map(p => p.IdItemDocumentoParent)
                .ToColumn("ID_ITEM_DOCUMENTO_PARENT");

            Map(p => p.IdTipoDocumento)
                .ToColumn("ID_TIPO_DOCUMENTO");

            Map(p => p.Level)
                .ToColumn("LEVEL");

            Map(p => p.Nome)
                .ToColumn("NOME_ITEM_DOCUMENTO");

            Map(p => p.Ordem)
                .ToColumn("ORDEM");

            Map(p => p.SomentePreview)
                .ToColumn("SOMENTE_PREVIEW");

            Map(p => p.TipoItemDocumento)
                .ToColumn("TIPO_ITEM_DOCUMENTO");

            Map(p => p.ValorPreview)
                .ToColumn("PREVIEW_VALUE");
        }

    }
}
