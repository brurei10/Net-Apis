﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ParcelaMap : DommelEntityMap<Parcela>
    {
        public ParcelaMap()
        {
            ToTable("PARCELA");

            Map(p => p.Id)
                .ToColumn("ID_PARCELA")
                .IsKey();

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.SituacaoParcela)
                .ToColumn("SIT_PARCELA");

            Map(p => p.IdContrato)
                .ToColumn("ID_CONTRATO");

            Map(p => p.ValorParcela)
                .ToColumn("VL_PARCELA");
        }
    }
}
