﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    public class VwDadosConsultaOrdemServicoMap : DommelEntityMap<VwDadosConsultaOrdemServico>
    {
        public VwDadosConsultaOrdemServicoMap()
        {
            ToTable("VW_DDS_CNSLT_OS");

            Map(p => p.NumeroOs)
                .ToColumn("NUMERO_OS");

            Map(p => p.SituacaoOs)
                .ToColumn("SITUACAO_OS");

            Map(p => p.DescricaoSituacaoOs)
                .ToColumn("DESCRICAO_SITUACAO_OS");

            Map(p => p.DataPrevista)
                .ToColumn("DATA_PREVISTA");

            Map(p => p.DataProgramada)
                .ToColumn("DATA_PROGRAMADA");

            Map(p => p.DataBaixa)
                .ToColumn("DATA_BAIXA");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Documento)
                .ToColumn("DOCUMENTO");

            Map(p => p.CodigoServico)
                .ToColumn("CODIGO_SERVICO");

            Map(p => p.DescricaoServico)
                .ToColumn("DESCRICAO_SERVICO");
        }
    }
}
