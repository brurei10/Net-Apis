﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class UnidadeMedidaMap 
        : DommelEntityMap<UnidadeMedida>
    {

        public UnidadeMedidaMap()
        {
            ToTable("TAB_UNID_MEDIDA");

            Map(p => p.Id)
                .ToColumn("ID_UNID_MEDIDA")
                .IsKey();

            Map(p => p.Descricao)
                .ToColumn("DESCR_UNID_MEDIDA");

            Map(p => p.Sigla)
                .ToColumn("SIGLA_UNID");
        }

    }
}
