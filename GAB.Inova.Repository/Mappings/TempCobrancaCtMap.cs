﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TempCobrancaCtMap : DommelEntityMap<TempCobrancaCt>
    {
        public TempCobrancaCtMap()
        {
            ToTable("TEMPCOBRANCACT");

            Map(p => p.Id)
                .ToColumn("IDCOBRANCA")
                .IsKey();

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.AnoMesCt)
                .ToColumn("ANOMESCT");

            Map(p => p.DataVencimento)
                .ToColumn("DATAVENCIMENTO");

            Map(p => p.DataPagamento)
                .ToColumn("DATAPAGAMENTO");

            Map(p => p.TipoCobranca)
                .ToColumn("TIPOCOBRANCA");

            Map(p => p.Origem)
                .ToColumn("ORIGEM");

            Map(p => p.IndiceVencimento)
                .ToColumn("INDICEVENC");

            Map(p => p.ValorOriginal)
                .ToColumn("VALORORIGINAL");

            Map(p => p.SeqOriginalPago)
                .ToColumn("SEQORIGINALPAGO");

            Map(p => p.PercentualCorrecao)
                .ToColumn("PERCCORRECAO");

            Map(p => p.Localizacao)
                .ToColumn("LOCALIZACAO");

            Map(p => p.IdUsuario)
                .ToColumn("IDUSUARIO");
        }
    }
}
