﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class LogradouroTipoMap
        : DommelEntityMap<LogradouroTipo>
    {

        public LogradouroTipoMap()
        {
            ToTable("LOGRADOURO_TIPO");

            Map(p => p.Id)
                .ToColumn("LTP_ID")
                .IsKey();

            Map(p => p.Nome)
                .ToColumn("NOME_LOGRADOURO_TIPO");
        }

    }
}
