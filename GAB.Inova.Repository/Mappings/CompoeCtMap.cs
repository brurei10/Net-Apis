﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class CompoeCtMap: DommelEntityMap<CompoeCt>
    {
        public CompoeCtMap()
        {
            ToTable("COMPOE_CT");

            Map(p => p.DocumentoPago)
                .ToColumn("DOC_PAGO")
                .IsKey();

            Map(p => p.DocumentoOriginal)
                .ToColumn("DOC_ORIGINAL")
                .IsKey();

            Map(p => p.Valor)
                .ToColumn("VL_DO_MES");

            Map(p => p.TipoContaDocumentoPago)
                .ToColumn("TIPO_CT_DOC_PAGO");
        }
    }
}
