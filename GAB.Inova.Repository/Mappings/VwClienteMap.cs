﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class VwClienteMap : DommelEntityMap<VwCliente>
    {

        public VwClienteMap()
        {
            ToTable("VW_CLIENTES");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.Nome)
                .ToColumn("NOME_CLIENTE");

            Map(p => p.Endereco)
                .ToColumn("ENDERECO");

            Map(p => p.NumeroImovel)
                .ToColumn("NUM_IMOVEL");

            Map(p => p.Complemento)
                .ToColumn("COMPLEMENTO");

            Map(p => p.NomeBairro)
                .ToColumn("NOME_BAIRRO");

            Map(p => p.NomeCidade)
                .ToColumn("NOME_MUNICIPIO");

            Map(p => p.NomeLocal)
                .ToColumn("NOME_LOCAL");

            Map(p => p.EconomiaResidencial)
                .ToColumn("ECO_RES");

            Map(p => p.EconomiaComercial)
                .ToColumn("ECO_COM");

            Map(p => p.EconomiaIndustrial)
                .ToColumn("ECO_IND");

            Map(p => p.EconomiaPublica)
                .ToColumn("ECO_PUB");

            Map(p => p.IdCiclo)
                .ToColumn("ID_CICLO");

            Map(p => p.IdSetor)
                .ToColumn("ID_SETOR");

            Map(p => p.Rota)
                .ToColumn("ROTA");

            Map(p => p.Sequencia)
                .ToColumn("SEQUENCIA");

            Map(p => p.IdHd)
                .ToColumn("ID_HD");

            Map(p => p.TipoEntrega)
                .ToColumn("TIPO_ENTREGA");

            Map(p => p.Documento)
                .ToColumn("CPFCLI");
        }
    }
}
