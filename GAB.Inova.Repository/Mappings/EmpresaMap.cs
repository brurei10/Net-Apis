﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class EmpresaMap
        : DommelEntityMap<Empresa>
    {

        public EmpresaMap()
        {
            ToTable("EMPRESA");

            Map(p => p.Id)
                .ToColumn("ID_EMPRESA")
                .IsKey();

            Map(p => p.Nome)
                .ToColumn("NOME_EMPRESA");

            Map(p => p.Sigla)
                .ToColumn("SIGLA");

            Map(p => p.IdFebraban)
                .ToColumn("ID_FEBRABAN");

            Map(p => p.UrlEndPoint)
                .ToColumn("URL_ENDPOINT");
        }

    }
}
