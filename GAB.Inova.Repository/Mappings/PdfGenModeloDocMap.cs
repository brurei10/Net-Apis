﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class PdfGenModeloDocMap
        : DommelEntityMap<PdfGenModeloDoc>
    {

        public PdfGenModeloDocMap()
        {
            ToTable("PDFGEN_MODELO_DOC");

            Map(p => p.Id)
                .ToColumn("ID_MODELO")
                .IsKey();

            Map(p => p.AlturaPagina)
                .ToColumn("ALTURA_PAGINA");

            Map(p => p.Descricao)
                .ToColumn("DESCRICAO_MODELO");

            Map(p => p.IdItemDocumento)
                .ToColumn("ID_ITEM_DOCUMENTO");

            Map(p => p.IdTipoDocumento)
                .ToColumn("ID_TIPO_DOCUMENTO");

            Map(p => p.ItensPorPagina)
                .ToColumn("ITENS_POR_PAGINA");

            Map(p => p.LarguraPagina)
                .ToColumn("LARGURA_PAGINA");

            Map(p => p.Nome)
                .ToColumn("NOME_MODELO");

            Map(p => p.OffsetRepeticao)
                .ToColumn("OFFSET_REPETICAO");

            Map(p => p.OrientacaoPagina)
                .ToColumn("ORIENTACAO_PAGINA");

            Map(p => p.OrientacaoRepeticoes)
                .ToColumn("ORIENTACAO_REPETICOES");

            Map(p => p.TamanhoPagina)
                .ToColumn("TAMANHO_PAGINA");
        }

    }
}
