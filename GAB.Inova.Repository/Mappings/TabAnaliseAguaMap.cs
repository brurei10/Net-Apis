﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class TabAnaliseAguaMap
        : DommelEntityMap<TabAnaliseAgua>
    {

        public TabAnaliseAguaMap()
        {
            ToTable("TAB_ANALISE_AGUA");

            Map(p => p.Id)
                .ToColumn("ID")
                .IsKey();

            Map(p => p.AmostraExigida)
                .ToColumn("AMOSTRAEXIGIDA");

            Map(p => p.AmostraRealizada)
                .ToColumn("AMOSTRAREALIZ");

            Map(p => p.AnoMes)
                .ToColumn("ANOMES");

            Map(p => p.Descricao)
                .ToColumn("DESCRICAO");

            Map(p => p.DescricaoColuna)
                .ToColumn("DESCR_COLUNA");

            Map(p => p.Referencias)
                .ToColumn("REFERENCIAS");

            Map(p => p.SeqAmostra)
                .ToColumn("SEQMOSTRA");

            Map(p => p.Tipo)
                .ToColumn("TIPO");

            Map(p => p.ValorMedioDetectado)
                .ToColumn("VALMEDIODETEC");
        }

    }
}
