﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class MensagemIntegradorMap
        : DommelEntityMap<MensagemIntegrador>
    {
        public MensagemIntegradorMap()
        {
            ToTable("MENSAGEM_INTEGRADOR");

            Map(p => p.Id)
                .ToColumn("ID_MENSAGEM_INTEGRADOR")
                .IsKey();

            Map(p => p.GuidMensagem)
                .ToColumn("GUID_MENSAGEM");

            Map(p => p.TemplateId)
                .ToColumn("TEMPLATE_ID");

            Map(p => p.Conteudo)
                .ToColumn("CONTEUDO");

            Map(p => p.DataCriacao)
                .ToColumn("DT_INCLUSAO");

            Map(p => p.Tipo)
                .ToColumn("MENSAGEM_INTEGRADOR_TIPO");

            Map(p => p.Integrador)
                .ToColumn("INTEGRADOR_TIPO");

            Map(p => p.Status)
                .ToColumn("STATUS_TIPO");

            Map(p => p.Destinatarios)
                .ToColumn("DESTINATARIOS");

            Map(p => p.DestinatariosCopia)
                .ToColumn("DESTINATARIOS_COPIA");

            Map(p => p.DestinatariosCopiaOculta)
                .ToColumn("DESTINATARIOS_COPIA_OCULTA");

            Map(p => p.De).ToColumn("DE");

            Map(p => p.Movimentos)
                .Ignore();
        }
    }
}
