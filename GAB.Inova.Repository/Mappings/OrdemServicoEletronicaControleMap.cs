﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class OrdemServicoEletronicaControleMap
        : DommelEntityMap<OrdemServicoEletronicaControle>
    {
        public OrdemServicoEletronicaControleMap()
        {
            ToTable("OS_ELETRONICA_CONTROLE");

            Map(p => p.NumeroOS)
                .ToColumn("NRO_OS");

            Map(p => p.SequenciaOS)
                .ToColumn("SEQ_OS");

            Map(p => p.Situacao)
                .ToColumn("ID_SIT_OS_ELE");

            Map(p => p.DataInclusao)
                .ToColumn("DT_INCL");

            Map(p => p.DataEnvio)
                .ToColumn("DT_ENV");

            Map(p => p.DataRetorno)
                .ToColumn("DT_RET");

            Map(x => x.ValidationResult)
                .Ignore();
        }
    }
}
