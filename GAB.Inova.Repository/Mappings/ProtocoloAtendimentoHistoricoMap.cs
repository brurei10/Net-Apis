﻿using Dapper.FluentMap.Dommel.Mapping;
using GAB.Inova.Domain;

namespace GAB.Inova.Repository.Mappings
{
    internal class ProtocoloAtendimentoHistoricoMap
        : DommelEntityMap<ProtocoloAtendimentoHistorico>
    {

        public ProtocoloAtendimentoHistoricoMap()
        {
            ToTable("PROTOCOLO_ATEND_HISTORICO");

            Map(p => p.Id)
                .ToColumn("ID_PROTOCOLO_ATEND_OBS")
                .IsKey();

            Map(p => p.Historico)
                .ToColumn("HISTORICO");

            Map(p => p.IdLocalAtendimento)
                .ToColumn("ID_LOCAL_ATEND");

            Map(p => p.IdModulo)
                .ToColumn("ID_MODULO");

            Map(p => p.IdProtocoloAtendimento)
                .ToColumn("ID_PROTOCOLO_ATENDIMENTO");

            Map(p => p.IdTipoAtendimento)
                .ToColumn("ID_TIPO_ATENDIMENTO");

            Map(p => p.IdUsuario)
                .ToColumn("ID_USUARIO");

            Map(p => p.Localidade)
                .ToColumn("LOCALIDADE");

            Map(p => p.Matricula)
                .ToColumn("MATRICULA");

            Map(p => p.NivelSatisfacao)
                .ToColumn("NIVEL_SATISFACAO");

            Map(p => p.Tempo)
                .ToColumn("TEMPO");

            Map(p => p.TipoProtocolo)
                .ToColumn("TIPO_PROTOCOLO");
        }

    }
}
