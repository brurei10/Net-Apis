using GAB.Inova.Common.Extensions;
using Xunit;

namespace GAB.Inova.IntegrationTest.Common
{
    public class CommonTest
    {

        /// <summary>
        /// Validar uma conta de e-mail
        /// </summary>
        /// <param name="email">Indica um conta de e-mail</param>
        [Theory]
        [InlineData("tanare_@hotmail.com")]
        public void TestValidaEmailAsync(string email)
        {
            var result = email.IsValidEmail();

            Assert.True(result);
        }

    }
}
