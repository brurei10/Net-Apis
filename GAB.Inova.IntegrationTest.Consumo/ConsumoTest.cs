using GAB.Inova.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace GAB.Inova.IntegrationTest.Consumo
{
    public class ConsumoTest : IClassFixture<TestFixture<Startup>>
    {
        #region [Vari�veis de aplica��o]

        readonly TestFixture<Startup> _fixture;
        readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region [Vari�veis de business]

        readonly IConsumoBusiness _consumoBusiness;

        #endregion

        #region [Construtores]

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="fixture">Indica a classe acess�aria</param>
        public ConsumoTest(TestFixture<Startup> fixture)
        {
            _fixture = fixture;

            #region [Instanciando vari�veis de aplica��o]

            _httpContextAccessor = _fixture?
                .ServiceProvider?
                .GetService<IHttpContextAccessor>();

            #endregion

            #region [Instanciando vari�veis business]

            _consumoBusiness = _fixture?
                .ServiceProvider?
                .GetService<IConsumoBusiness>();

            #endregion
        }
        #endregion

        [Theory]
        [InlineData("20200111213222")]
        public async Task TestObterHistoricoConsumosAsync(string numeroProtocolo)
        {
            var result = await _consumoBusiness.ObterHistoricoConsumosAsync(numeroProtocolo);

            Assert.True(result.Sucesso);
        }
    }
}
