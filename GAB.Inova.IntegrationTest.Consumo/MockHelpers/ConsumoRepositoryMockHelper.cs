﻿using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Consumo.MockHelpers
{
    /// <summary>
    /// Mock para o repositório da tabela consumo
    /// </summary>
    /// <returns><see cref="IConsumoRepository"/></returns>
    public static class ConsumoRepositoryMockHelper
    {
        public static IConsumoRepository GetConsumoRepository()
        {
            var listaConsumo = GeraListaConsumo();

            var mockConsumoRepository = new Mock<IConsumoRepository>();

            mockConsumoRepository.Setup(m => m.ObterUltimosDozeConsumosPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(listaConsumo);

            return mockConsumoRepository.Object;
        }

        #region [Metodos privados]

        private static IEnumerable<Domain.Consumo> GeraListaConsumo()
        {
            return new List<Domain.Consumo>
            {
                new Domain.Consumo
                {
                  Id =  "202006",
                  Matricula =  "0100000004",
                  Faturado =  12,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "202005",
                  Matricula =  "0100000004",
                  Faturado =  16,
                  QtdeDiasLeitura =  30
                },
                new Domain.Consumo
                {
                  Id =  "202004",
                  Matricula =  "0100000004",
                  Faturado =  17,
                  QtdeDiasLeitura =  30
                },
                new Domain.Consumo
                {
                  Id =  "202003",
                  Matricula =  "0100000004",
                  Faturado =  19,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "202002",
                  Matricula =  "0100000004",
                  Faturado =  16,
                  QtdeDiasLeitura =  30
                },
                new Domain.Consumo
                {
                  Id =  "202001",
                  Matricula =  "0100000004",
                  Faturado =  17,
                  QtdeDiasLeitura =  33
                },
                new Domain.Consumo
                {
                  Id =  "201912",
                  Matricula =  "0100000004",
                  Faturado =  16,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "201911",
                  Matricula =  "0100000004",
                  Faturado =  16,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "201910",
                  Matricula =  "0100000004",
                  Faturado =  19,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "201909",
                  Matricula =  "0100000004",
                  Faturado =  19,
                  QtdeDiasLeitura =  30
                },
                new Domain.Consumo
                {
                  Id =  "201908",
                  Matricula =  "0100000004",
                  Faturado =  18,
                  QtdeDiasLeitura =  31
                },
                new Domain.Consumo
                {
                  Id =  "201907",
                  Matricula =  "0100000004",
                  Faturado =  17,
                  QtdeDiasLeitura =  30
                }
            };
        }

        #endregion
    }
}