﻿using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IntegrationTest.Consumo.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Consumo
{
    /// <summary>
    /// Classe acessória
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public class TestFixture<TStartup>
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            var hostingEnvironment = ConfigurationMockHelper.GetHostingEnvironment();

            services.AddSingleton<IWebHostEnvironment>(hostingEnvironment);
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor());
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(hostingEnvironment));
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());
            services.AddSingleton<IProtocoloAtendimentoRepository>(ProtocoloAtendimentoRepositoryMockHelper.GetProtocoloAtendimentoRepository());
            services.AddSingleton<IConsumoRepository>(ConsumoRepositoryMockHelper.GetConsumoRepository());
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }

    }
}