﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Faturamento;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.API.Faturamento.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Faturamento
    /// </summary>
    /// 
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class ContasController : ControllerBase
    {

        const long ID_ROTINA = 9999999984;                    //API Inova 2.0 - Faturamento
        const int ID_NATUREZA_HIST_CONTA = 16;                //INFORMAÇÃO SOBRE COMPOSIÇÃO DA CONTA
        const int ID_NATUREZA_INFO_DEBITOS = 17;              //INFORMAÇÃO SOBRE DÉBITO
        const int ID_NATUREZA_QUITACAO_DEBITO = 25;           //GERAÇÃO DA QUITAÇÃO DE DÉBITO
        const int ID_NATUREZA_SEGUNDA_VIA = 1;                //2ª VIA DE NOTA FISCAL/ CONTA
        
        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly IFaturamentoBusiness _faturamentoBusiness;
        readonly ILogger<ContasController> _logger;

        public ContasController(ILogger<ContasController> logger,
                                IAtendimentoBusiness atendimentoBusiness, 
                                IFaturamentoBusiness faturamentoBusiness)
        {
            _logger = logger;
            _atendimentoBusiness = atendimentoBusiness;
            _faturamentoBusiness = faturamentoBusiness;
        }

        [HttpGet]
        [Route("ObterHistoricoContas")]
        [ProducesResponseType(typeof(HistoricoContasResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterHistoricoContasAsync([FromHeader]long? rotinaId, [FromQuery]string numeroProtocolo)
        {
            try
            {
                _logger.LogInformation($"Obter histórico de contas para protocolo [nº:{numeroProtocolo}].");

                var result = await _faturamentoBusiness.ObterHistoricoContasAsync(numeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_HIST_CONTA, 
                        NumeroProtocolo = numeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter histórico de contas para protocolo [nº:{numeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Não foi possível carregar informações, tente novamente mais tarde.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("ObterDebitos")]
        [ProducesResponseType(typeof(HistoricoContasResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterDebitosAsync([FromHeader] long? rotinaId, [FromQuery] string numeroProtocolo)
        {
            try
            {
                _logger.LogInformation($"Obter débitos para protocolo [nº:{numeroProtocolo}].");

                var result = await _faturamentoBusiness.ObterHistoricoContasEmAbertoAsync(numeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_INFO_DEBITOS,
                        NumeroProtocolo = numeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter débitos para protocolo [nº:{numeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Não foi possível carregar informações, tente novamente mais tarde.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("ObterSegundaVia")]
        [ProducesResponseType(typeof(SegundaViaContaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterSegundaViaAsync([FromHeader] long? rotinaId, [FromBody] SegundaViaContaRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation($"Obter segunda via de conta [nº:{viewModel.SeqOriginal}] para protocolo [nº:{viewModel.NumeroProtocolo}].");

                var result = await _faturamentoBusiness.ObterSegundaViaAsync(viewModel.SeqOriginal);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                        NumeroProtocolo = viewModel.NumeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter segunda via de conta [nº:{viewModel.SeqOriginal}] para protocolo [nº:{viewModel.NumeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        

        [HttpPost]
        [Route("EnviarEmailSegundaVia")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EnviarEmailSegundaViaAsync([FromHeader] long? rotinaId, [FromBody] EnviarEmailSegundaViaRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation($"Enviar e-mail de segunda via da conta [nº:{viewModel.SeqOriginal}] para protocolo [nº:{viewModel.NumeroProtocolo}].");

                var result = await _faturamentoBusiness.EnviarEmailSegundaViaAsync(viewModel.SeqOriginal, viewModel.Email);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                        NumeroProtocolo = viewModel.NumeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao enviar e-mail de segunda via da conta [nº:{viewModel.SeqOriginal}] para protocolo [nº:{viewModel.NumeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        

        [HttpPost]
        [Route("ObterCodigoBarrasConta")]
        [ProducesResponseType(typeof(CodigoBarrasResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterCodigoBarrasContaAsync([FromHeader] long? rotinaId, [FromBody] CodigoBarrasRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation($"Obter código de barras da conta [nº:{viewModel.SeqOriginal}].");

                var result = await _faturamentoBusiness.ObterCodigoBarrasNotaFiscalAsync(viewModel.SeqOriginal);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                        NumeroProtocolo = viewModel.NumeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter código de barras da conta [nº:{viewModel.SeqOriginal}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("ObterCodigoBarrasQuitacaoDebito")]
        [ProducesResponseType(typeof(CodigoBarrasResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterCodigoBarraQuitacaoDebitoAsync([FromBody] QuitacaoDebitoRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation("Gerando código de barras da quitação de débito", "");
                _logger.LogDebug($"ModelRecebido: { JsonConvert.SerializeObject(viewModel)}");

                var result = await _faturamentoBusiness.ObterCodigoBarrasQuitacaoDebitoAsync(viewModel.Contas, viewModel.NumeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                        NumeroProtocolo = viewModel.NumeroProtocolo,
                        RotinaId = ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter código de barras quitação de débito [seqOriginal:{string.Join(";", viewModel.Contas)}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        

        [HttpPost]
        [Route("ObterPdfQuitacaoDebito")]
        [ProducesResponseType(typeof(PdfQuitacaoDebitoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterPdfQuitacaoDebitoAsync([FromBody] QuitacaoDebitoRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation("Gerando PDF da quitação de débito", "");
                _logger.LogDebug($"ModelRecebido: { JsonConvert.SerializeObject(viewModel)}");

                var result = await _faturamentoBusiness.ObterPdfQuitacaoDebitoAsync(viewModel.Contas, viewModel.NumeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                        NumeroProtocolo = viewModel.NumeroProtocolo,
                        RotinaId = ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter Pdf quitação de débito [seqOriginal:{string.Join(";", viewModel.Contas)}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }
    }
}
