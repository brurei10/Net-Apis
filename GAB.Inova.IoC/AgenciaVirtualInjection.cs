﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class AgenciaVirtualInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureAgenciaVirtualServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddProvider()
                .AddRepository()
                .AddSendGridServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("AgenciaVirtual", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IAgenciaVirtualBusiness, AgenciaVirtualBusiness>();
            services
                .TryAddScoped<IAtendimentoBusiness, AtendimentoBusiness>();
            services
                .TryAddScoped<IFaturamentoBusiness, FaturamentoBusiness>();

            return services;
        }

        private static IServiceCollection AddProvider(this IServiceCollection services)
        {
            services
                .TryAddScoped<IBarcodeProvider, BarcodeProvider>();
            services
                .TryAddScoped<IHtmlToPdfProvider, HtmlToPdfProvider>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IContaRepository, ContaRepository>();
            services
                .TryAddScoped<IFaturamentoRepository, FaturamentoRepository>();
            services
                .TryAddScoped<IParametroSistemaRepository, ParametroSistemaRepository>();
            services
                .TryAddScoped<IPdfGenericoRepository, PdfGenericoRepository>();
            services
                .TryAddScoped<ITabAnaliseAguaRepository, TabAnaliseAguaRepository>();
            services
                .TryAddScoped<ITarifaDiariaRepository, TarifaDiariaRepository>();
            services
                .TryAddScoped<ITarifaDiariaRepository, TarifaDiariaRepository>();
            services
                .TryAddScoped<ITempHistoricoConsumoRepository, TempHistoricoConsumoRepository>();
            services
                .TryAddScoped<ITempImprimeContaRepository, TempImprimeContaRepository>();
            services
                .TryAddScoped<IDocumentoModeloRepository, DocumentoModeloRepository>();
            services
                .TryAddScoped<IImprimeGRPRepository, ImprimeGRPRepository>();
            services
                .TryAddScoped<IEmpresaRepository, EmpresaRepository>();
            services
                .TryAddScoped<IClienteContratoRepository, ClienteContratoRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();
            services
                .TryAddScoped<IProcessoClienteRepository, ProcessoClienteRepository>();
            services
                .TryAddScoped<IContratoRepository, ContratoRepository>();
            services
                .TryAddScoped<IDeclaracaoAnualRepository, DeclaracaoAnualRepository>();
            services
                .TryAddScoped<IVwDadosDebitoAnualRepository, VwDadosDebitoAnualRepository>();
            services
                .TryAddScoped<IVwClienteRepository, VwClienteRepository>();
            services
                .TryAddScoped<IParcelaRepository, ParcelaRepository>();
            services
                .TryAddScoped<IFeriadoRepository, FeriadoRepository>();
            services
                .TryAddScoped<ITempCobrancaCtRepository, TempCobrancaCtRepository>();
            services
                .TryAddScoped<ICompoeCtRepository, CompoeCtRepository>();
            services
                .TryAddScoped<IGRPRepository, GRPRepository>();
            services
                .TryAddScoped<IClienteRepository, ClienteRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoDetalheRepository, ProtocoloAtendimentoDetalheRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoGfaRepository, ProtocoloAtendimentoGfaRepository>();
            services
                .TryAddScoped<IUsuarioRepository, UsuarioRepository>();

            return services;
        }

        #endregion

    }
}
