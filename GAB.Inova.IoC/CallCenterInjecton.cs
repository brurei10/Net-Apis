﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class CallCenterInjecton
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureCallCenterServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddProvider()
                .AddRepository()
                .AddSendGridServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("CallCenter", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<ICallCenterBusiness, CallCenterBusiness>();
            services
                .TryAddScoped<IAtendimentoBusiness, AtendimentoBusiness>();

            return services;
        }

        private static IServiceCollection AddProvider(this IServiceCollection services)
        {
            services
                .TryAddScoped<ITokenProvider, TokenProvider>();
            services
                .TryAddScoped<ICryptographyProvider, CryptographyProvider>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IEmpresaRepository, EmpresaRepository>();
            services
                .TryAddScoped<IInstanciaRepository, InstanciaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();
            services
                .TryAddScoped<IClienteCallCenterRepository, ClienteCallCenterRepository>();
            services
                .TryAddScoped<IVwDadosClienteInterrupcaoRepository, VwDadosClienteInterrupcaoRepository>();
            services
                .TryAddScoped<IInterrupcaoRepository, InterrupcaoRepository>();
            services
                .TryAddScoped<IInformacaoContaRepository, InformacaoContaRepository>();
            services
                .TryAddScoped<IClienteRepository, ClienteRepository>();
            services
                .TryAddScoped<IContratoRepository, ContratoRepository>();
            services
                .TryAddScoped<IParametroSistemaRepository, ParametroSistemaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoDetalheRepository, ProtocoloAtendimentoDetalheRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoGfaRepository, ProtocoloAtendimentoGfaRepository>();
            services
                .TryAddScoped<IUsuarioRepository, UsuarioRepository>();

            return services;
        }

        #endregion

    }
}
