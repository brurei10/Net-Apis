﻿using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Repository.SendGrid;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC.Bases
{
    public static class SendGridInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection AddSendGridServices(this IServiceCollection services)
        {
            services
                .AddFactory()
                .AddRepository();

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddFactory(this IServiceCollection services)
        {
            services
                .TryAddScoped<IIntegradorConfigFactory, IntegradorConfigFactory>();

            return services;
        }
        
        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            
            services
                .TryAddScoped<IEmailIntegradorRepository, SendGridRepository>();
            
            return services;
        }

        #endregion

    }
}
