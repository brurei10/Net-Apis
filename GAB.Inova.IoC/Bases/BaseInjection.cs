﻿using AutoMapper;
using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Business.Mappings;
using GAB.Inova.Common;
using GAB.Inova.Common.Helpers;
using GAB.Inova.Common.Interfaces.Helpers;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using GAB.Inova.Repository.Bases;
using GAB.Inova.Repository.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GAB.Inova.IoC.Bases
{
    internal static class InjectionBase
    {

        #region [Métodos internos]

        internal static IServiceCollection AddBaseServices(this IServiceCollection services)
        {
            services
                .AddBusiness()
                .AddDataContext()
                .AddProvider()
                .TryAddScoped(typeof(IAuditConfiguration), typeof(AuditConfiguration));

            using (var sp = services.BuildServiceProvider())
            {
                services.TryAddSingleton<IConfiguration>(sp.GetService<IConfiguration>());
                services.TryAddSingleton<IWebHostEnvironment>(sp.GetService<IWebHostEnvironment>());
            }

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IEnvironmentVariables, EnvironmentVariables>();
            services.TryAddSingleton<ISigningConfiguration, SigningConfiguration>();
            services.TryAddSingleton<ITokenConfiguration, TokenConfiguration>();

            return services;
        }

        internal static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        {
            using (var sp = services.BuildServiceProvider())
            {
                var signingConfigurations = sp.GetService<ISigningConfiguration>();
                var tokenConfigurations = sp.GetService<ITokenConfiguration>();

                services.AddAuthentication(authOptions =>
                {
                    authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(bearerOptions =>
                {
                    var paramsValidation = bearerOptions.TokenValidationParameters;

                    paramsValidation.IssuerSigningKey = signingConfigurations?.Key;
                    paramsValidation.ValidAudience = tokenConfigurations?.Audience;
                    paramsValidation.ValidIssuer = tokenConfigurations?.Issuer;

                    // Valida a assinatura de um token recebido
                    paramsValidation.ValidateIssuerSigningKey = true;

                    // Verifica se um token recebido ainda é válido
                    paramsValidation.ValidateLifetime = true;

                    // Tempo de tolerância para a expiração de um token (utilizado
                    // caso haja problemas de sincronismo de horário entre diferentes
                    // computadores envolvidos no processo de comunicação)
                    paramsValidation.ClockSkew = TimeSpan.Zero;
                });
            }

            // Ativa o uso do token como forma de autorizar o acesso
            // a recursos deste projeto
            services
                .AddAuthorization(auth =>
                {
                    auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                       .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                       .RequireAuthenticatedUser()
                       .Build());
                });

            return services;
        }

        internal static IServiceCollection ConfigureCompression(this IServiceCollection services)
        {
            services
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<BrotliCompressionProvider>();
                });

            return services;
        }

        internal static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            DapperMappings.Register();

            // Registering Mappings automatically only works if the 
            // Automapper Profile classes are in ASP.NET project
            AutoMapperConfig.RegisterMappings();

            return services;
        }

        internal static IServiceCollection ConfigureSwaggerApi(this IServiceCollection services, string apiName, bool useJwtAuthentication)
        {
            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Version = "v1",
                        Title = $"GAB - {apiName}",
                        Description = $"Grupo Aguas do Brasil - API de {apiName}"
                    });

                    if (useJwtAuthentication)
                    {
                        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                        {
                            Description = "Para acessar, é necessário no header o atributo Authorization com o token obtido na API de token. Examplo \"Authorization: Bearer {token}\"",
                            Name = "Authorization",
                            In = ParameterLocation.Header,
                            Type = SecuritySchemeType.ApiKey,
                            Scheme = "Bearer"
                        });

                        c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                        {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference
                                    {
                                        Type = ReferenceType.SecurityScheme,
                                        Id = "Bearer"
                                    },
                                    Scheme = "oauth2",
                                    Name = "Bearer",
                                    In = ParameterLocation.Header,

                                },
                                new List<string>()
                            }
                        });
                    }
                });

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddDataContext(this IServiceCollection services)
        {
            services
                .TryAddScoped<IDbConnection, MultitenantOracleConnection>();
            services
                .TryAddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

        private static IServiceCollection AddProvider(this IServiceCollection services)
        {
            services
                .TryAddScoped<IMultitenantConnectionProvider, MultitenantConnectionProvider>();
            services
                .TryAddScoped<ITokenProvider, TokenProvider>();

            return services;
        }

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<ITokenBusiness, TokenBusiness>();

            return services;
        }

        #endregion

    }
}
