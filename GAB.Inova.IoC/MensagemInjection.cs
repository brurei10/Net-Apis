﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class MensagemInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureMensagemServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddSendGridServices()
                .AddBusiness()
                .AddRepository()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Mensagem", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IMensagemIntegradorBusiness, MensagemIntegradorBusiness>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IAvisoDebitoRepository, AvisoDebitoRepository>();
            services
                .TryAddScoped<IClienteRepository, ClienteRepository>();
            services
                .TryAddScoped<IMensagemIntegradorRepository, MensagemIntegradorRepository>();
            services
                .TryAddScoped<IMensagemIntegradorMovimentoRepository, MensagemIntegradorMovimentoRepository>();
            services
                .TryAddScoped<IContaDigitalEnvioRepository, ContaDigitalEnvioRepository>();
            services
                .TryAddScoped<IContaDigitalEnvioMovimentoRepository, ContaDigitalEnvioMovimentoRepository>();
            services
                .TryAddScoped<IContaRepository, ContaRepository>();
            services
                .TryAddScoped<IHistoricoLigacaoRepository, HistoricoLigacaoRepository>();

            return services;
        }

        #endregion

    }
}
