﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class ConsumoInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureConsumoServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddRepository()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Consumo", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IConsumoBusiness, ConsumoBusiness>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IConsumoRepository, ConsumoRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();

            return services;
        }

        #endregion

    }
}
