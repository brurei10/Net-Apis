﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class ProtheusInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureProtheusServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddSendGridServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Fechamento", true);

            return services;
        }

        #endregion



    }
}
