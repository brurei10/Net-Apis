﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{

    public static class AtendimentoInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureAtendimentoServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddRepository()
                .AddSendGridServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Atendimento", true);

            return services;
        }

        public static IServiceCollection AddAtendimentoServices(this IServiceCollection services)
        {
            services
                .AddBusiness()
                .AddRepository()
                .AddSendGridServices();

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IAtendimentoBusiness, AtendimentoBusiness>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IClienteRepository, ClienteRepository>();
            services
                .TryAddScoped<IContratoRepository, ContratoRepository>();
            services
                .TryAddScoped<IParametroSistemaRepository, ParametroSistemaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoDetalheRepository, ProtocoloAtendimentoDetalheRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoGfaRepository, ProtocoloAtendimentoGfaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoHistoricoRepository, ProtocoloAtendimentoHistoricoRepository>();
            services
                .TryAddScoped<IUsuarioRepository, UsuarioRepository>();

            return services;
        }

        #endregion

    }
}
