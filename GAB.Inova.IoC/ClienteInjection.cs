﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class ClienteInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureClienteServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddSendGridServices()
                .AddBusiness()
                .AddRepository()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Clientes", true);

            return services;
        }

        #endregion

        #region [Métodos privados]
        
        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IAutenticacaoBusiness, AutenticacaoBusiness>();
            services
                .TryAddScoped<IClienteBusiness, ClienteBusiness>();
            services
                .TryAddScoped<IContaDigitalBusiness, ContaDigitalBusiness>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IClienteRepository, ClienteRepository>();
            services
                .TryAddScoped<IClienteTokenRepository, ClienteTokenRepository>();
            services
                .TryAddScoped<IContaDigitalEnvioRepository, ContaDigitalEnvioRepository>();
            services
                .TryAddScoped<IContaDigitalEnvioMovimentoRepository, ContaDigitalEnvioMovimentoRepository>();
            services
                .TryAddScoped<IContaRepository, ContaRepository>();
            services
                .TryAddScoped<IContratoMensagemIntegradorRepository, ContratoMensagemIntegradorRepository>();
            services
                .TryAddScoped<IContratoRepository, ContratoRepository>();
            services
                .TryAddScoped<IGrupoEntregaRepository, GrupoEntregaRepository>();
            services
                .TryAddScoped<IMensagemIntegradorRepository, MensagemIntegradorRepository>();
            services
                .TryAddScoped<IMensagemIntegradorMovimentoRepository, MensagemIntegradorMovimentoRepository>();
            services
                .TryAddScoped<IParametroSistemaRepository, ParametroSistemaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoDetalheRepository, ProtocoloAtendimentoDetalheRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoGfaRepository, ProtocoloAtendimentoGfaRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoHistoricoRepository, ProtocoloAtendimentoHistoricoRepository>();
            services
                .TryAddScoped<IProcessoClienteRepository, ProcessoClienteRepository>();
            services
                .TryAddScoped<IUsuarioRepository, UsuarioRepository>();
            services
                .TryAddScoped<IVwClienteRepository, VwClienteRepository>();
            services
                .TryAddScoped<IClienteCallCenterRepository, ClienteCallCenterRepository>();
            services
                .TryAddScoped<IInterrupcaoRepository, InterrupcaoRepository>();
            services
                .TryAddScoped<IVwDadosClienteInterrupcaoRepository, VwDadosClienteInterrupcaoRepository>();
            services
                .TryAddScoped<IInformacaoContaRepository, InformacaoContaRepository>();
            services
                .TryAddScoped<IInstanciaRepository, InstanciaRepository>();
            services
                .TryAddScoped<IEmpresaRepository, EmpresaRepository>();
            services
                .TryAddScoped<IClienteContratoRepository, ClienteContratoRepository>();

            return services;
        }

        #endregion

    }
}
