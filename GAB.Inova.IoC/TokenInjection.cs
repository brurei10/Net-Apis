﻿using GAB.Inova.IoC.Bases;
using Microsoft.Extensions.DependencyInjection;

namespace GAB.Inova.IoC
{
    public static class TokenInjection
    {

        public static void ConfigureTokenServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Token", false);
        }

    }
}
