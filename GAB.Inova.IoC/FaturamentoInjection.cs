﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class FaturamentoInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureFaturamentoServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddProvider()
                .AddRepository()
                .AddSendGridServices()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Faturamento", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IFaturamentoBusiness, FaturamentoBusiness>();

            return services;
        }

        private static IServiceCollection AddProvider(this IServiceCollection services)
        {
            services
                .TryAddScoped<IBarcodeProvider, BarcodeProvider>();
            services
                .TryAddScoped<IHtmlToPdfProvider, HtmlToPdfProvider>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IContaRepository, ContaRepository>();
            services
                .TryAddScoped<IContratoRepository, ContratoRepository>();
            services
                .TryAddScoped<IFaturamentoRepository, FaturamentoRepository>();
            services
                .TryAddScoped<IFeriadoRepository, FeriadoRepository>();
            services
                .TryAddScoped<IParametroSistemaRepository, ParametroSistemaRepository>();
            services
                .TryAddScoped<IPdfGenericoRepository, PdfGenericoRepository>();
            services
                .TryAddScoped<IProcessoClienteRepository, ProcessoClienteRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();
            services
                .TryAddScoped<ITabAnaliseAguaRepository, TabAnaliseAguaRepository>();
            services
                .TryAddScoped<ITarifaDiariaRepository, TarifaDiariaRepository>();
            services
                .TryAddScoped<ITempHistoricoConsumoRepository, TempHistoricoConsumoRepository>();
            services
                .TryAddScoped<ITempImprimeContaRepository, TempImprimeContaRepository>();
            services
                .TryAddScoped<IEmpresaRepository, EmpresaRepository>();
            services
                .TryAddScoped<ICompoeCtRepository, CompoeCtRepository>();
            services
                .TryAddScoped<IGRPRepository, GRPRepository>();
            services
                .TryAddScoped<IImprimeGRPRepository, ImprimeGRPRepository>();
            services
                .TryAddScoped<ITempCobrancaCtRepository, TempCobrancaCtRepository>();
            services
                .TryAddScoped<IParcelaRepository, ParcelaRepository>();
            services
                .TryAddScoped<IVwClienteRepository, VwClienteRepository>();
            services
                .TryAddScoped<IDocumentoModeloRepository, DocumentoModeloRepository>();
            services
                .TryAddScoped<IDeclaracaoAnualRepository, DeclaracaoAnualRepository>();
            services
                .TryAddScoped<IVwDadosDebitoAnualRepository, VwDadosDebitoAnualRepository>();

            return services;
        }

        #endregion

    }
}
