﻿using GAB.Inova.Business;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IoC.Bases;
using GAB.Inova.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GAB.Inova.IoC
{
    public static class ServicoInjection
    {

        #region [Métodos públicos]

        public static IServiceCollection ConfigureServicoServices(this IServiceCollection services)
        {
            services
                .AddBaseServices()
                .AddBusiness()
                .AddRepository()
                .ConfigureAuthentication()
                .ConfigureAutoMapper()
                .ConfigureCompression()
                .ConfigureSwaggerApi("Servicos", true);

            return services;
        }

        #endregion

        #region [Métodos privados]

        private static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services
                .TryAddScoped<IOrdemServicoEletronicaBusiness, OrdemServicoEletronicaBusiness>();
            services
                .TryAddScoped<IOperacaoAguaBusiness, OperacaoAguaBusiness>();
            services
                .TryAddScoped<IOrdemServicoBusiness, OrdemServicoBusiness>();

            return services;
        }

        private static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .TryAddScoped<IOrdemServicoEletronicaRepository, OrdemServicoEletronicaRepository>();
            services
                .TryAddScoped<IOrdemServicoAnexoExternoRepository, OrdemServicoAnexoExternoRepository>();
            services
                .TryAddScoped<IOrdemServicoEletronicaControleRepository, OrdemServicoEletronicaControleRepository>();
            services
                .TryAddScoped<IOrdemServicoDetalheRepository, OrdemServicoDetalheRepository>();
            services
                .TryAddScoped<IMunicipioRepository, MunicipioRepository>();
            services
                .TryAddScoped<IBairroRepository, BairroRepository>();
            services
                .TryAddScoped<IInterrupcaoRepository, InterrupcaoRepository>();
            services
                .TryAddScoped<IVwDadosClienteInterrupcaoRepository, VwDadosClienteInterrupcaoRepository>();
            services
                .TryAddScoped<IVwDadosConsultaOrdemServicoRepository, VwDadosConsultaOrdemServicoRepository>();
            services
                .TryAddScoped<IProtocoloAtendimentoRepository, ProtocoloAtendimentoRepository>();

            return services;
        }

        #endregion

    }
}
