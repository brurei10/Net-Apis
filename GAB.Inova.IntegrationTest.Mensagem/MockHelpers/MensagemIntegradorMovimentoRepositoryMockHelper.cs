﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Mensagem.MockHelpers
{
    /// <summary>
    /// Obter o repositório de mensagem do integrador movimento
    /// </summary>
    /// <returns><see cref="IMensagemIntegradorMovimentoRepository"/></returns>
    public static class MensagemIntegradorMovimentoRepositoryMockHelper
    {
        public static IMensagemIntegradorMovimentoRepository GetMensagemIntegradorMovimentoRepository()
        {
            var mockMensagemIntegradorMovimentoRepository = new Mock<IMensagemIntegradorMovimentoRepository>();

            mockMensagemIntegradorMovimentoRepository.Setup(m => m.InserirAsync(It.IsAny<MensagemIntegradorMovimento>())).ReturnsAsync(true);

            return mockMensagemIntegradorMovimentoRepository.Object;
        }
    }
}
