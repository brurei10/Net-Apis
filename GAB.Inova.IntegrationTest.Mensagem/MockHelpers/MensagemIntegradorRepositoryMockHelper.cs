﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Mensagem.MockHelpers
{
    /// <summary>
    /// Mock para o repositório da conta
    /// </summary>
    /// <returns><see cref="IMensagemIntegradorRepository"/></returns>
    public static class MensagemIntegradorRepositoryMockHelper
    {
        public static IMensagemIntegradorRepository GetMensagemIntegradorRepository()
        {
            var mensagemIntegrador = GeraMensagemIntegrador();

            var mockMensagemIntegradorRepository = new Mock<IMensagemIntegradorRepository>();

            mockMensagemIntegradorRepository.Setup(m => m.ObterPorGuidAsync(It.IsAny<string>())).ReturnsAsync(mensagemIntegrador);
            mockMensagemIntegradorRepository.Setup(m => m.AtualizarAsync(It.IsAny<MensagemIntegrador>())).ReturnsAsync(true);
            mockMensagemIntegradorRepository.Setup(m => m.InserirAsync(It.IsAny<MensagemIntegrador>())).ReturnsAsync(true);

            return mockMensagemIntegradorRepository.Object;
        }

        #region [Metodos privados]

        private static MensagemIntegrador GeraMensagemIntegrador()
        {
            return new MensagemIntegrador
            {
                Id = 743,
                GuidMensagem = "c2lclvBIRsOMfLmH5AcKlQ",
                TemplateId = null,
                Conteudo = "\"Erro ao enviar email da conta 1333555 da matrícula 0100000852.\"",
                DataCriacao = Convert.ToDateTime("10/07/2020 14:00:00"),
                Tipo = MensagemIntegradorTipoEnum.ContaDigitalErroEnvioConta,
                Integrador = IntegradorTipoEnum.SendGrid,
                Status = MensagemIntegradoStatusEnum.Processado,
                Destinatarios = "teste@grupoaguasdobrasil.com.br",
                DestinatariosCopia = null,
                DestinatariosCopiaOculta = null,
                De = "git_inova@grupoaguasdobrasil.com.br",
                Movimentos = default(List<MensagemIntegradorMovimento>)
            };
        }
        #endregion
    }
}
