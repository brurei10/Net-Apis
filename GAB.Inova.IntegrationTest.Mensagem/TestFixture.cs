﻿using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using GAB.Inova.IntegrationTest.Mensagem.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Mensagem
{
    /// <summary>
    /// Classe acessória
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public class TestFixture<TStartup>
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            var hostingEnvironment = ConfigurationMockHelper.GetHostingEnvironment();

            services.AddSingleton<IWebHostEnvironment>(hostingEnvironment);
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor());
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(hostingEnvironment));
            services.AddSingleton<IUnitOfWork>(UnitOfWorkMockHelper.GetUnitOfWork());
            services.AddSingleton<IMensagemIntegradorRepository>(MensagemIntegradorRepositoryMockHelper.GetMensagemIntegradorRepository());
            services.AddSingleton<IMensagemIntegradorMovimentoRepository>(MensagemIntegradorMovimentoRepositoryMockHelper.GetMensagemIntegradorMovimentoRepository());
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }

    }
}