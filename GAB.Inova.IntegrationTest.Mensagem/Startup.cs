﻿using GAB.Inova.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Microsoft.Extensions.DependencyInjection;

namespace GAB.Inova.IntegrationTest.Mensagem
{
    /// <summary>
    /// Classe para inicio de uma orquestração
    /// </summary>
    public class Startup
    {

        /// <summary>
        /// Construtor
        /// </summary>
        public Startup() { }

        /// <summary>
        /// Adicionar serviços ao contêiner
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .ConfigureMensagemServices();

            services
                .AddControllers()
                .AddNewtonsoftJson(opcoes => opcoes.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);
        }

        /// <summary>
        /// Configurar o pipeline de solicitação de HTTP
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}