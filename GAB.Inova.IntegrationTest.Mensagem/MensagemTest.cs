using GAB.Inova.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Common.ViewModels.Mensagem;
using GAB.Inova.Common.Enums;

namespace GAB.Inova.IntegrationTest.Mensagem
{
    public class MensagemTest : IClassFixture<TestFixture<Startup>>
    {
        #region [Vari�veis de aplica��o]

        readonly TestFixture<Startup> _fixture;
        readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region [Vari�veis de business]

        readonly IMensagemIntegradorBusiness _mensagemIntegradorBusiness;

        #endregion

        #region [Construtores]

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="fixture">Indica a classe acess�aria</param>
        public MensagemTest(TestFixture<Startup> fixture)
        {
            _fixture = fixture;

            #region [Instanciando vari�veis de aplica��o]

            _httpContextAccessor = _fixture?
                .ServiceProvider?
                .GetService<IHttpContextAccessor>();

            #endregion

            #region [Instanciando vari�veis business]

            _mensagemIntegradorBusiness = _fixture?
                .ServiceProvider?
                .GetService<IMensagemIntegradorBusiness>();

            #endregion
        }
        #endregion

        [Theory]
        [InlineData("teste@grupoaguasdobrasil.com.br", "git_inova@grupoaguasdobrasil.com.br", "Git Inova")]
        public async Task TestEnviarMensagemAsync(string email, string emailRemetente, string nomeRemetente)
        {
            var emailRequestViewModel = new EmailRequestViewModel
            {
                CodigoEmpresa = "CAA",
                RemetenteEnderecoEmail = emailRemetente,
                RemetenteNome = nomeRemetente,
                Destinatarios = email,
                Conteudo = "Ocorreu um erro no processamento do envio de email da conta digital [matricula: 0100000004 - seqOriginal: 1450926].",
                TemplateId = null,
                Tipo = MensagemIntegradorTipoEnum.ContaDigitalErroEnvioConta
            };

            var result = await _mensagemIntegradorBusiness.EnviarMensagemAsync(emailRequestViewModel);

            Assert.True(result.Sucesso);
        }

        [Theory]
        [InlineData("open")]
        public async Task TestAtualizarStatusAsync(string evento)
        {
            var atualizarStatusMensagemViewModel = new AtualizarStatusMensagemViewModel
            {
                Timestamp = 1594400400,
                Event = evento,
                MensagemId = "7kJ2bAk3Ty6R50H_AHusQg"
            };

            var result = await _mensagemIntegradorBusiness.AtualizarStatusAsync(atualizarStatusMensagemViewModel);

            Assert.True(result.Sucesso);
        }
    }
}
