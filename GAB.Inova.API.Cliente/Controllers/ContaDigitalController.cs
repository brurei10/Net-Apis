﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Cliente.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Conta Digital
    /// </summary>
    [ApiController]
    public class ContaDigitalController : ControllerBase
    {

        readonly ILogger<ContaDigitalController> _logger;
        readonly IContaDigitalBusiness _contaDigitalBusiness;
        
        /// <summary>
        /// Contrutor da classe
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="contaDigitalBusiness">Instância do objeto com as regras de negócio referente a Conta Digital</param>
        public ContaDigitalController(ILogger<ContaDigitalController> logger, IContaDigitalBusiness contaDigitalBusiness)
        {
            _logger = logger;
            _contaDigitalBusiness = contaDigitalBusiness;
        }

        /// <summary>
        /// Confirmar uma solicitação de adesão ou alteração de e-mail da Conta Digital
        /// </summary>
        /// <param name="token">Indica um token válido</param>
        /// <param name="protocolo">Indica um protocolo de atendimento do iNova válido</param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/ConfirmaToken")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConfirmarSolicitacaoAsync([FromQuery]string token, [FromQuery]string protocolo)
        {
            try
            {
                _logger.LogInformation($"CONFIRMAR TOKEN DE CONTA DIGITAL [PROTOCOLO:{protocolo} TOKEN:{token}] - INICIO");

                var result = await _contaDigitalBusiness.ConfirmarSolicitacaoAsync(token, protocolo);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao confirmar token de conta digital [protocolo:{protocolo} token: {token}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"CONFIRMAR TOKEN DE CONTA DIGITAL [PROTOCOLO:{protocolo} TOKEN:{token}] - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao confirmar token de conta digital. PROTOCOLO:{protocolo}; TOKEN:{token}");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Serviço indisponível no momento. Favor entrar em contato com um de nossos canais de atendimento.");

                _logger.LogInformation($"CONFIRMAR TOKEN DE CONTA DIGITAL [PROTOCOLO:{protocolo} TOKEN:{token}] - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Solicitar a adesão ao serviço de Conta Digital
        /// </summary>
        /// <param name="model"><see cref="AderirContaDigitalRequestViewModel"/></param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/SolicitaInclusao")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SolicitarAdesaoAsync(AderirContaDigitalRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"SOLICITAR ADESÃO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - INICIO");

                var result = await _contaDigitalBusiness.SolicitarAdesaoAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao solicitar adesão da conta digital [matricula: {model.Matricula} protocolo:{model.Protocolo}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"SOLICITAR ADESÃO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - FIM");
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao solicitar adesão de conta digital. PROTOCOLO:{model.Protocolo}; SISTEMA:{model.Sistema}");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Serviço indisponível no momento. Favor entrar em contato com um de nossos canais de atendimento.");

                _logger.LogInformation($"SOLICITAR ADESÃO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - INICIO");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Solicitar a alteração de e-mail(s) principal e/ou do serviço de Conta Digital
        /// </summary>
        /// <param name="model"><see cref="AlterarContaDigitalRequestViewModel"/></param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/SolicitaAlteracao")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SolicitarAlteracaoAsync(AlterarContaDigitalRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"SOLICITAR ALTERAÇAO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - INICIO");

                var result = await _contaDigitalBusiness.SolicitarAlteracaoAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao solicitar alteração de conta digital [matricula: {model.Matricula} protocolo:{model.Protocolo}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"SOLICITAR ALTERAÇAO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao solicitar alteração de conta digital. PROTOCOLO:{model.Protocolo}; SISTEMA:{model.Sistema}");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Serviço indisponível no momento. Favor entrar em contato com um de nossos canais de atendimento.");

                _logger.LogInformation($"SOLICITAR ALTERAÇAO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Solicitar o cancelamento do serviço de Conta Digital
        /// </summary>
        /// <param name="model"><see cref="CancelarContaDigitalRequestViewModel"/></param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/SolicitaCancelamento")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SolicitarCancelamentoAsync(CancelarContaDigitalRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"SOLICITAR CANCELAMENTRO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - INICIO");

                var result = await _contaDigitalBusiness.SolicitarCancelamentoAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao solicitar cancelamento de conta digital [matricula: {model.Matricula} protocolo:{model.Protocolo}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"SOLICITAR CANCELAMENTRO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao solicitar cancelamento de conta digital. PROTOCOLO:{model.Protocolo}; SISTEMA:{model.Sistema}");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Serviço indisponível no momento. Favor entrar em contato com um de nossos canais de atendimento.");

                _logger.LogInformation($"SOLICITAR CANCELAMENTRO DE CONTA DIGITAL [PROTOCOLO:{model.Protocolo} SISTEMA:{model.Sistema} MATRICULA:{model.Matricula}] - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Enviar a Conta Digital
        /// </summary>
        /// <param name="model"><see cref="EnviarContaDigitalRequestViewModel"/></param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/EnvioConta")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EnviarContaAsync(EnviarContaDigitalRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"SOLICITAR ENVIO DE CONTA DIGITAL [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}] - INICIO");
                
                var result = await _contaDigitalBusiness.EnviarContaAsync(model);

                if(!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao solicitar envio de conta digital [matricula: {model.Matricula} CONTA:{model.SeqOriginal}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"SOLICITAR ENVIO DE CONTA DIGITAL [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}] - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro no envio de conta digital. MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem($"Ocorreu um erro inesperado na execução do serviço. - {ex.Message}");

                _logger.LogInformation($"SOLICITAR ENVIO DE CONTA DIGITAL [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}] - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Enviar conta digital para clientes sem adesão
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/v1/[controller]/EnvioContaClienteSemAdesao")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EnviarContaClienteSemAdesaoAsync(EnviarContaDigitalRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"SOLICITAR ENVIO DE CONTA DIGITAL DE CLIENTE SEM ADESÃO [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}] - INICIO");

                var result = await _contaDigitalBusiness.EnviarContaClienteSemAdesaoAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao solicitar envio de conta digital [matricula: {model.Matricula} CONTA:{model.SeqOriginal}] - {string.Join(';', result.Mensagens)}");
                }
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"ERRO NO ENVIO DE CONTA DIGITAL DE CLIENTE SEM ADESÃO [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}]");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem($"Ocorreu um erro inesperado na execução do serviço. - {ex.Message}");
                
                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
            finally
            {
                _logger.LogInformation($"SOLICITAR ENVIO DE CONTA DIGITAL DE CLIENTE SEM ADESÃO [MATRICULA:{model.Matricula}; CONTA:{model.SeqOriginal}] - FIM");
            }
        }

    }
}
