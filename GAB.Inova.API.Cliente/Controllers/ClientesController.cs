﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Cliente.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Cliente
    /// </summary>
    /// 
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class ClientesController : ControllerBase
    {

        readonly IClienteBusiness _clienteBusiness;
        readonly ILogger<ClientesController> _logger;

        /// <summary>
        /// Contrutor da classe
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="clienteBusiness">Instância do objeto com as regras de negócio referente a Cliente</param>
        public ClientesController(ILogger<ClientesController> logger, IClienteBusiness clienteBusiness)
        {
            _logger = logger;
            _clienteBusiness = clienteBusiness;
        }

        /// <summary>
        /// Obter informações de cliente através da matrícula
        /// </summary>
        /// <param name="matricula">Indica uma matrícula válida</param>
        /// <returns><see cref="ClienteResponseViewModel"/></returns>
        [HttpGet]
        [Route("{matricula}")]
        [ProducesResponseType(typeof(ClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterClientePorMatriculaAsync(string matricula)
        {
            try
            {
                _logger.LogInformation($"Obter cliente por matricula [nº:{matricula}].");

                var result = await _clienteBusiness.ObterClientePorMatriculaAsync(matricula);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter cliente por matricula [nº:{matricula}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Obter informações de cliente através de um token
        /// </summary>
        /// <param name="token">Indica um token válido</param>
        /// <returns><see cref="ClienteTokenResponseViewModel"/></returns>
        [HttpGet]
        [Route("Token")]
        [ProducesResponseType(typeof(ClienteTokenResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterClientePorTokenAsync([FromQuery]string token)
        {
            try
            {
                _logger.LogInformation($"Obter cliente por token [token:{token}]");

                var result = await _clienteBusiness.ObterClientePorTokenAsync(token);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter cliente por token [token:{token}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Verificar se um cliente está sob judice
        /// </summary>
        /// <param name="matricula">Indica um matrícula válida</param>
        /// <returns><see cref="ClienteSobJudiceResponseViewModel"/></returns>
        [HttpGet]
        [Route("clienteSobJudice")]
        [ProducesResponseType(typeof(ClienteSobJudiceResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> VerificarClienteSobJudiceAsync([FromQuery]string matricula)
        {
            try
            {
                _logger.LogInformation($"Verificar cliente sob judice por matricula [nº:{matricula}].");

                var result = await _clienteBusiness.VerificarClienteSobJudiceAsync(matricula);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar cliente sob judice por matricula [nº:{matricula}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Retorna todas as ligações vinculadas ao CPF / CNPJ
        /// </summary>
        /// <param name="documento">Número do documento a ser pesquisado</param>
        /// <returns><see cref="ObterLigacoesResponseViewModel"/></returns>
        [HttpGet]
        [Route("ObterLigacoes")]
        [ProducesResponseType(typeof(ObterLigacoesResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterLigacoesAsync([FromQuery] string documento)
        {
            try
            {
                _logger.LogInformation($"Obter ligações vinculadas ao documento [nº:{documento}].");

                var result = await _clienteBusiness.ObterLigacoesAsync(documento);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter ligações vinculadas ao documento [nº:{documento}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

    }
}
