﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Cliente;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GAB.Inova.API.Cliente.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Autenticação
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AutenticacaoController : ControllerBase
    {
        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly IAutenticacaoBusiness _autenticacaoBusiness;
        readonly ILogger<AutenticacaoController> _logger;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="atendimentoBusiness">Instância do objeto com as regras de negócio referente a Atendimento</param>
        /// <param name="autenticacaoBusiness">Instância do objeto com as regras de negócio referente a Autenticação</param>
        public AutenticacaoController(ILogger<AutenticacaoController> logger,
                                      IAtendimentoBusiness atendimentoBusiness,
                                      IAutenticacaoBusiness autenticacaoBusiness)
        {
            _logger = logger;
            _atendimentoBusiness = atendimentoBusiness;
            _autenticacaoBusiness = autenticacaoBusiness;
        }

        /// <summary>
        /// Autenticar um cliente via aplicativo (celular, tablet, etc)
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="AutenticarClienteResponseViewModel"/><seealso cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [Route("AutenticarClienteViaAplicativo")]
        [ProducesResponseType(typeof(AutenticarClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AutenticarClienteViaAplicativoAsync(AutenticarClienteRequestViewModel viewModel)
        {
            return await AutenticarCliente(viewModel, CanalAtendimentoEnum.Aplicativo, new List<EmpresaTipoEnum>
            {
                EmpresaTipoEnum.CAV
            });
        }

        /// <summary>
        /// Autenticar um cliente via WhatsApp
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="AutenticarClienteResponseViewModel"/><seealso cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [Route("AutenticarClienteViaWhatsApp")]
        [ProducesResponseType(typeof(AutenticarClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AutenticarClienteViaWhatsAppAsync(AutenticarClienteRequestViewModel viewModel)
        {
            return await AutenticarCliente(viewModel, CanalAtendimentoEnum.Whatsapp);
        }

        /// <summary>
        /// Autenticar um cliente via Web Chat
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="AutenticarClienteResponseViewModel"/><seealso cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [Route("AutenticarClienteViaWebChat")]
        [ProducesResponseType(typeof(AutenticarClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AutenticarClienteViaWebChatAsync(AutenticarClienteRequestViewModel viewModel)
        {
            return await AutenticarCliente(viewModel, CanalAtendimentoEnum.WebChat);
        }

        /// <summary>
        /// Autenticar um cliente via Facebook
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="AutenticarClienteResponseViewModel"/><seealso cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [Route("AutenticarClienteViaFacebook")]
        [ProducesResponseType(typeof(AutenticarClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AutenticarClienteViaFacebookAsync(AutenticarClienteRequestViewModel viewModel)
        {
            return await AutenticarCliente(viewModel, CanalAtendimentoEnum.RedesSociais);
        }

        #region [Métodos privados]

        private async Task<IActionResult> AutenticarCliente(AutenticarClienteRequestViewModel viewModel, CanalAtendimentoEnum canalAtendimento, IEnumerable<EmpresaTipoEnum> indisponibilidades = null)
        {
            try
            {
                _logger.LogInformation($"Autenticar {canalAtendimento.GetDescription()} [ligação:{viewModel.Ligacao}, documento:{viewModel.CpfCnpj}].");

                var result = await _autenticacaoBusiness.AutenticarClienteAsync(viewModel, indisponibilidades);

                if (result.Sucesso)
                {
                    _logger.LogInformation("Cliente autenticado com sucesso.");

                    var protocoloResponse = await _atendimentoBusiness.GerarProtocoloAtendimentoAsync(viewModel.Ligacao, canalAtendimento);

                    if (protocoloResponse.Sucesso)
                    {
                        result.NumeroProtocolo = protocoloResponse.NumeroProtocolo;

                        _logger.LogInformation($"Protocolo de atendimento [nº:{result.NumeroProtocolo}] gerado via {canalAtendimento.GetDescription()}.");

                        return Ok(result);
                    }

                    result.AdicionaMensagem("Sistema indisponível! Tente novamente.");
                    result.Sucesso = false;

                    _logger.LogInformation($"Não foi possível gerar protocolo de atendimento via {canalAtendimento.GetDescription()} para o cliente [matricula:{result.ClienteAutenticado.Ligacao}].");
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao Autenticar via {canalAtendimento.GetDescription()} [ligação:{viewModel.Ligacao}, documento:{viewModel.CpfCnpj}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        #endregion

    }
}