﻿using GAB.Inova.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace GAB.Inova.API.Cliente
{
    /// <summary>
    /// Classe para inicio de uma orquestração
    /// </summary>
    public class Startup
    {

        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public Startup(IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            _configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        /// <summary>
        /// Adicionar serviços ao contêiner
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .ConfigureClienteServices()
                .AddAtendimentoServices();

            services
                .AddControllers()
                .AddNewtonsoftJson(opcoes => opcoes.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);

            services.AddApplicationInsightsTelemetry();
        }

        /// <summary>
        /// Configurar o pipeline de solicitação de HTTP
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Grupo Aguas do Brasil");
            });

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
