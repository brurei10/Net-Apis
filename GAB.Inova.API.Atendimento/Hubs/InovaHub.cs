﻿using GAB.Inova.API.Atendimento.Interfaces;
using GAB.Inova.API.Atendimento.Models;
using GAB.Inova.Common.Enums;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Atendimento.Hubs
{
    public class InovaHub 
        : Hub
    {

        readonly ILogger<InovaHub> _logger;
        readonly IInovaTicker _inovaTicker;

        public InovaHub(ILogger<InovaHub> logger, IInovaTicker inovaTicker)
        {
            _logger = logger;
            _inovaTicker = inovaTicker;
        }
        
        #region [Sobrecargas]

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _inovaTicker.RemoveInovaConnection(Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        #endregion

        public void LogIn(string username, string siglaEmpresa)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(siglaEmpresa))
            {
                if (!Enum.IsDefined(typeof(EmpresaTipoEnum), siglaEmpresa.ToUpper())) return;

                var empresaTipo = (EmpresaTipoEnum)Enum.Parse(typeof(EmpresaTipoEnum), siglaEmpresa.ToUpper());
                var userManager = new UserManager()
                {
                    ConnectionId = Context.ConnectionId,
                    EmpresaTipo = empresaTipo,
                    Username = username.ToLower()
                };

                _logger.LogDebug($"Login [username:{userManager.Username}, empresa:{userManager.EmpresaTipo.ToString()}].");

                _inovaTicker.AddInovaConnection(userManager);
                _inovaTicker.UsuarioConectadoAoGfa(userManager.Username, siglaEmpresa);
            }
        }

    }
}
