﻿using GAB.Inova.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GAB.Inova.API.Atendimento.Hubs;
using GAB.Inova.API.Atendimento.Interfaces;
using GAB.Inova.API.Atendimento.Implemented;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using Microsoft.Extensions.Hosting;

namespace GAB.Inova.API.Atendimento
{
    /// <summary>
    /// Classe para inicio de uma orquestração
    /// </summary>
    public class Startup
    {

        const string CORS_POLICY_NAME = "AllowCors";

        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public Startup(IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();
        }

        /// <summary>
        /// Adicionar serviços ao contêiner
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .ConfigureAtendimentoServices();

            services.TryAddSingleton<IInovaTicker, InovaTicker>();

            services
                .AddSignalR(options => options.EnableDetailedErrors = true);

            services
                .AddControllers()
                .AddJsonOptions(opcoes => opcoes.JsonSerializerOptions.IgnoreNullValues = true);

            var origins = _configuration.GetValue<string>("UrlsCorsWithOrigins");

            services
                .AddCors(o => o.AddPolicy(CORS_POLICY_NAME,
                    builder =>
                    {
                        builder
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .WithOrigins(origins.Split(";"));
                    }));
        }

        /// <summary>
        /// Configurar o pipeline de solicitação de HTTP
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Grupo Aguas do Brasil");
            });

            app.UseStaticFiles();
            
            app.UseRouting();
            app.UseAuthorization();
            app.UseWebSockets();
            app.UseCors(CORS_POLICY_NAME);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<InovaHub>("/inovaHub");
            });
        }
    }
    
}
