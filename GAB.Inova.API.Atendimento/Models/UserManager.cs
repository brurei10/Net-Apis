﻿using GAB.Inova.Common.Enums;
using System;

namespace GAB.Inova.API.Atendimento.Models
{
    public class UserManager
    {
        public UserManager()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; }
        public string Username { get; set; }
        public string ConnectionId { get; set; }
        public EmpresaTipoEnum EmpresaTipo { get; set; }

    }
}
