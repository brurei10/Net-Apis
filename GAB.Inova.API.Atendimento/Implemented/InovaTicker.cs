﻿using GAB.Inova.API.Atendimento.Hubs;
using GAB.Inova.API.Atendimento.Interfaces;
using GAB.Inova.API.Atendimento.Models;
using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAB.Inova.API.Atendimento.Implemented
{
    public class InovaTicker 
        : IInovaTicker
    {

        readonly ILogger<InovaTicker> _logger;
        readonly IHubContext<InovaHub> _hubContext;
        readonly ITokenProvider _tokenProvider;

        readonly Lazy<IList<UserManager>> _gfaConnections;
        readonly Lazy<IList<UserManager>> _inovaConnections;
        
        public InovaTicker(ILogger<InovaTicker> logger, IHubContext<InovaHub> hubContext, ITokenProvider tokenProvider)
        {
            _logger = logger;
            _hubContext = hubContext;
            _tokenProvider = tokenProvider;

            _gfaConnections = new Lazy<IList<UserManager>>(() => new List<UserManager>());
            _inovaConnections = new Lazy<IList<UserManager>>(() => new List<UserManager>());
        }
        
        #region [Gerenciamento das conexões de usuários]

        public Task AddGfaConnection(UserManager userManager)
        {
            if (userManager is null) return Task.CompletedTask;
            if (!_gfaConnections.Value.Any(u => u.Username == userManager.Username && u.EmpresaTipo == userManager.EmpresaTipo))
            {
                _logger.LogDebug($"GfaConnections.Count => {_gfaConnections.Value.Count}");

                _gfaConnections.Value.Add(userManager);
                
                _logger.LogDebug($"GfaConnection adicionada [username:{userManager.Username}, empresa:{userManager.EmpresaTipo.ToString()}].");
                _logger.LogDebug($"GfaConnections.Count => {_gfaConnections.Value.Count}");
                _logger.LogDebug($"GfaConnections => [{string.Join(", ", _gfaConnections.Value.Select(q => q.Username))}]");
            }
            else _logger.LogDebug($"Já existe GfaConnection para [username:{userManager.Username}, empresa:{userManager.EmpresaTipo.ToString()}].");
            return Task.CompletedTask;
        }

        public Task AddInovaConnection(UserManager userManager)
        {
            if (userManager is null) return Task.CompletedTask;
            if (!_inovaConnections.Value.Any(u => u.Username == userManager.Username && u.EmpresaTipo == userManager.EmpresaTipo))
            {
                _logger.LogDebug($"InovaConnections.Count => {_inovaConnections.Value.Count}");

                _inovaConnections.Value.Add(userManager);
                
                _logger.LogDebug($"InovaConnection adicionada [username:{userManager.Username}, empresa:{userManager.EmpresaTipo.ToString()}].");
                _logger.LogDebug($"InovaConnections.Count => {_inovaConnections.Value.Count}");
                _logger.LogDebug($"InovaConnections => [{string.Join(", ", _inovaConnections.Value.Select(q => q.Username))}]");
            }
            else _logger.LogDebug($"Já existe InovaConnection para [username:{userManager?.Username}, empresa:{userManager?.EmpresaTipo}].");
            return Task.CompletedTask;
        }

        public Task RemoveGfaConnection(string username, string empresaTipo)
        {
            _logger.LogDebug($"GfaConnections.Count => {_gfaConnections.Value.Count}");

            var userManager = _gfaConnections.Value.FirstOrDefault(u => u.Username.ToUpper() == username.ToUpper() && u.EmpresaTipo.ToString() == empresaTipo);
            if (userManager != null && _gfaConnections.Value.Remove(userManager))
            {
                _logger.LogDebug($"GfaConnection removida [username:{userManager?.Username}, empresa:{userManager?.EmpresaTipo}].");
                _logger.LogDebug($"GfaConnections.Count => {_gfaConnections.Value.Count}");
                _logger.LogDebug($"GfaConnections => [{string.Join(", ", _gfaConnections.Value.Select(q => q.Username))}]");
            }
            return Task.CompletedTask;
        }

        public Task RemoveInovaConnection(string connectionId)
        {
            _logger.LogDebug($"InovaConnections.Count => {_inovaConnections.Value.Count}");

            var userManager = _inovaConnections.Value.FirstOrDefault(u => u.ConnectionId == connectionId);
            if (userManager != null && _inovaConnections.Value.Remove(userManager))
            {
                _logger.LogDebug($"InovaConnection removida [username:{userManager?.Username}, empresa:{userManager?.EmpresaTipo}].");
                _logger.LogDebug($"InovaConnections.Count => {_inovaConnections.Value.Count}");
                _logger.LogDebug($"InovaConnections => [{string.Join(", ", _inovaConnections.Value.Select(q => q.Username))}]");
            }
            return Task.CompletedTask;
        }

        public async Task<TModel> UsuarioConectadoAoInova<TModel>(string username) where TModel : new()
        {
            TModel model = new TModel();

            if (!typeof(BaseResponseViewModel).IsInstanceOfType(model)) return await Task.FromResult(default(TModel));

            var result = model as BaseResponseViewModel;

            var empresaTipo = await _tokenProvider.ObterEmpresaContextoAtualAsync();

            if (string.IsNullOrEmpty(username))
            {
                result.Sucesso = false;
                result.AdicionaMensagem($"Não foi possível identificar o usuário conectado ao iNova: parâmetro nulo ou vazio.");

                return await Task.FromResult(model);
            }

            _logger.LogDebug($"InovaConnections.Count => {_inovaConnections.Value.Count}");
            _logger.LogDebug($"InovaConnections => [{string.Join(", ", _inovaConnections.Value.Select(q => q.Username))}]");

            var userManager = _inovaConnections.Value.FirstOrDefault(q => q.Username == username && q.EmpresaTipo == empresaTipo);

            result.Sucesso = (userManager != null);
            
            if (!result.Sucesso)
            {
                //result.AdicionaMensagem($"O usuário [{username}] não está conectado ao iNova de {empresaTipo}.");
                result.AdicionaMensagem("Favor logar ou pressionar as teclas CTRL+F5 no iNova. Caso o erro persista, favor entrar em contato com a TI.");
            }
            return await Task.FromResult(model);
        }

        public Task UsuarioConectadoAoGfa(string username, string empresaTipo)
        {
            if (_gfaConnections.Value.Any(u => u.Username.ToUpper() == username.ToUpper() && u.EmpresaTipo.ToString() == empresaTipo))
            {
                return BroadcastInformarSituacaoUsuarioNoPontoDeAtendimento(username, true);
            }
            return Task.CompletedTask;
        }

        #endregion

        #region [Métodos públicos]

        public Task BroadcastAbrirModalFinalizarProtocolo(FinalizarIntegracaoGfaResponseViewModel model)
        {
            var connectionId = ObterConnectionIdUsuarioAsync(model.UsuarioGfa)
                .GetAwaiter()
                .GetResult();

            if (!string.IsNullOrEmpty(connectionId))
            {
                return _hubContext.Clients.Client(connectionId)
                    .SendAsync("abrirModalFinalizarProtocolo", JsonConvert.SerializeObject(model));
            }
            return Task.CompletedTask;
        }

        public Task BroadcastCancelarAtendimento(CancelarIntegracaoGfaResponseViewModel model)
        {
            var connectionId = ObterConnectionIdUsuarioAsync(model.UsuarioGfa)
                .GetAwaiter()
                .GetResult();

            if (!string.IsNullOrEmpty(connectionId))
            {
                return _hubContext.Clients.Client(connectionId)
                    .SendAsync("cancelarAtendimento", JsonConvert.SerializeObject(model));
            }
            return Task.CompletedTask;
        }

        public Task BroadcastFinalizarAtendimento(FinalizarIntegracaoGfaResponseViewModel model)
        {
            var connectionId = ObterConnectionIdUsuarioAsync(model.UsuarioGfa)
                .GetAwaiter()
                .GetResult();

            if (!string.IsNullOrEmpty(connectionId))
            {
                return _hubContext.Clients.Client(connectionId)
                    .SendAsync("finalizarAtendimento", JsonConvert.SerializeObject(model));
            }
            return Task.CompletedTask;
        }

        public Task BroadcastIniciarAtendimento(IniciarIntegracaoGfaResponseViewModel model)
        {
            var connectionId = ObterConnectionIdUsuarioAsync(model.UsuarioGfa)
                .GetAwaiter()
                .GetResult();

            if (!string.IsNullOrEmpty(connectionId))
            {
                return _hubContext.Clients.Client(connectionId)
                    .SendAsync("iniciarAtendimento", JsonConvert.SerializeObject(model));
            }
            return Task.CompletedTask;
        }

        public Task BroadcastInformarSituacaoUsuarioNoPontoDeAtendimento(string username, bool conectado)
        {
            var connectionId = ObterConnectionIdUsuarioAsync(username)
                .GetAwaiter()
                .GetResult();

            if (!string.IsNullOrEmpty(connectionId))
            {
                return _hubContext.Clients.Client(connectionId)
                    .SendAsync("informarSituacaoUsuarioNoPontoDeAtendimento", (conectado ? 1 : 0));
            }
            return Task.CompletedTask;
        }

        #endregion

        #region [Métodos privados]

        private async Task<string> ObterConnectionIdUsuarioAsync(string username)
        {
            var inovaConnections = _inovaConnections.Value.Where(u => u.Username == username);
            if (inovaConnections.Any())
            {
                return await Task.FromResult(inovaConnections
                    .FirstOrDefault()
                    .ConnectionId);
            }
            return await Task.FromResult(string.Empty);
        }

        #endregion

    }
}
