﻿using System;
using System.Threading.Tasks;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GAB.Inova.API.Atendimento.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class OuvidoriaController 
        : ControllerBase
    {

        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly ILogger<OuvidoriaController> _logger;

        public OuvidoriaController(ILogger<OuvidoriaController> logger, IAtendimentoBusiness atendimentoBusiness)
        {
            _logger = logger;
            _atendimentoBusiness = atendimentoBusiness;
        }

        [Authorize("Bearer")]
        [HttpPost]
        [Route("EnviarEmail")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EnviarEmailAsync(EnviarEmailOuvidoriaByAplicativoRequestViewModel viewModel)
        {
            try
            {
                var result = await _atendimentoBusiness.EnviarEmailOuvidoriaByAplicativoAsync(viewModel);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Erro ao enviar e-mail de ouvidoria.");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

    }
}