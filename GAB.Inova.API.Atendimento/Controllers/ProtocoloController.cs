﻿using GAB.Inova.API.Atendimento.Interfaces;
using GAB.Inova.API.Atendimento.Models;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Atendimento.Controllers
{

    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class ProtocoloController : ControllerBase
    {
        const long ID_ROTINA = 9999999986; //API Inova 2.0 - Atendimento

        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly IInovaTicker _inovaTicker;
        readonly ILogger<ProtocoloController> _logger;

        public ProtocoloController(ILogger<ProtocoloController> logger, IInovaTicker inovaTicker, IAtendimentoBusiness atendimentoBusiness)
        {
            _logger = logger;
            _inovaTicker = inovaTicker;
            _atendimentoBusiness = atendimentoBusiness;
        }

        [HttpGet]
        [Route("Online")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> OnlineAsync()
        {
            try
            {
                _logger.LogInformation("Verificar se o serviço está on-line.");

                var result = await _atendimentoBusiness.OnlineAsync();

                if (result.Sucesso)
                {
                    _logger.LogInformation("Serviço on-line.");

                    return Ok(result);
                }
                
                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Erro ao verificar se o serviço está on-line.");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("VerificarAtendimentoEmAberto/{username}")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> VerificarAtendimentoEmAbertoAsync(string username)
        {
            try
            {
                _logger.LogInformation($"Verificar atendimento em aberto [usuário:{username}].");

                var result = await _atendimentoBusiness.VerificarAtendimentoEmAbertoAsync(username);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar protocolo(s) em aberto [usuário:{username}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("VerificarUsuarioLogado/{username}")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> VerificarUsuarioLogadoAsync(string username)
        {
            try
            {
                _logger.LogInformation($"Verificar se usuário está logado no iNova [{username}].");

                var result = await _inovaTicker.UsuarioConectadoAoInova<BaseResponseViewModel>(username.ToLower());

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao verificar se usuário está logado no iNova [{username}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        [HttpPost]
        [Route("CancelarIntegracaoGfa")]
        [ProducesResponseType(typeof(CancelarIntegracaoGfaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CancelarIntegracaoGfaAsync(CancelarIntegracaoGfaRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Cancelar atendimento integrado ao GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var resultTicker = await _atendimentoBusiness.OnlineAsync();

                if (!resultTicker.Sucesso) return BadRequest(resultTicker);

                var result = await _atendimentoBusiness.CancelarIntegracaoGfaAsync(model);

                if (result.Sucesso)
                {
                    resultTicker = await _inovaTicker.UsuarioConectadoAoInova<CancelarIntegracaoGfaResponseViewModel>(model.UsuarioGfa.ToLower());

                    if (resultTicker.Sucesso) await _inovaTicker.BroadcastCancelarAtendimento(result);

                    return Ok(result);
                }
                else
                {
                    foreach(var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao cancelar atendimento integrado ao GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        [HttpPost]
        [Route("ConectarUsuarioAoPontoDeAtendimento")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConectarUsuarioAoPontoDeAtendimentoAsync(UsuarioPontoAtendimentoRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Conectar usuário a um ponto de atendimento no GFA [usuário:{model?.Username}, empresa:{model?.CodigoEmpresa}].");

                var userManager = new UserManager()
                {
                    ConnectionId = Guid.NewGuid().ToString(),
                    Username = model.Username
                };

                if (Enum.IsDefined(typeof(EmpresaTipoEnum), model.CodigoEmpresa.ToUpper()))
                {
                    userManager.EmpresaTipo = (EmpresaTipoEnum)Enum.Parse(typeof(EmpresaTipoEnum), model.CodigoEmpresa.ToUpper());
                }

                await _inovaTicker.AddGfaConnection(userManager);

                await _inovaTicker.BroadcastInformarSituacaoUsuarioNoPontoDeAtendimento(model.Username, true);

                return Ok(new BaseResponseViewModel()
                {
                    Sucesso = true
                });
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao conectar usuário a um ponto de atendimento no GFA [usuário:{model?.Username}, empresa:{model?.CodigoEmpresa}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        [HttpPost]
        [Route("DesconectarUsuarioAoPontoDeAtendimento")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DesconectarUsuarioAoPontoDeAtendimentoAsync(UsuarioPontoAtendimentoRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Desconectar usuário a um ponto de atendimento no GFA [usuário:{model?.Username}, empresa:{model?.CodigoEmpresa}].");

                await _inovaTicker.RemoveGfaConnection(model.Username, model.CodigoEmpresa);

                await _inovaTicker.BroadcastInformarSituacaoUsuarioNoPontoDeAtendimento(model.Username, false);

                return Ok(new BaseResponseViewModel()
                {
                    Sucesso = true
                });
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao desconectar usuário a um ponto de atendimento no GFA [usuário:{model?.Username}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        [HttpPost]
        [Route("FinalizarIntegracaoGfa")]
        [ProducesResponseType(typeof(FinalizarIntegracaoGfaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> FinalizarIntegracaoGfaAsync(FinalizarIntegracaoGfaRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Finalizar atendimento integrado ao GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var resultTicker = await _atendimentoBusiness.OnlineAsync();

                if (!resultTicker.Sucesso) return BadRequest(resultTicker);

                resultTicker = await _inovaTicker.UsuarioConectadoAoInova<FinalizarIntegracaoGfaResponseViewModel>(model.UsuarioGfa.ToLower());

                var result = await _atendimentoBusiness.FinalizarIntegracaoGfaAsync(model);

                if (result.Sucesso)
                {
                    if (resultTicker.Sucesso) await _inovaTicker.BroadcastFinalizarAtendimento(result);

                    return Ok(result);
                }
                else
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                if (result.TemErroValidacaoProtocoloAtendimento && resultTicker.Sucesso)
                {
                    await _inovaTicker.BroadcastAbrirModalFinalizarProtocolo(result);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar atendimento integrado ao GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }


        [HttpPost]
        [Route("IniciarIntegracaoGfa")]
        [ProducesResponseType(typeof(IniciarIntegracaoGfaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> IniciarIntegracaoGfaAsync(IniciarIntegracaoGfaRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Iniciar integração com GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var resultTicker = await _atendimentoBusiness.OnlineAsync();

                if (!resultTicker.Sucesso) return BadRequest(resultTicker);

                resultTicker = await _inovaTicker.UsuarioConectadoAoInova<IniciarIntegracaoGfaResponseViewModel>(model.UsuarioGfa.ToLower());

                if (!resultTicker.Sucesso) return BadRequest(resultTicker);

                var result = await _atendimentoBusiness.IniciarIntegracaoGfaAsync(model);

                if (result.Sucesso)
                {
                    await _inovaTicker.BroadcastIniciarAtendimento(result);

                    return Ok(result);
                }
                else
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao Iniciar integração com GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("VerificarIntegracaoGfa")]
        [ProducesResponseType(typeof(VerificarIntegracaoGfaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> VerificarIntegracaoGfaAsync(VerificarIntegracaoGfaRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"Verificar integração com GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var result = await _atendimentoBusiness.VerificarIntegracaoGfaAsync(model);

                if (result.Sucesso) return Ok(result);

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao Verificar integração com GFA [nº protocolo:{model?.NumeroProtocoloGfa}; senha:{model?.SenhaGfa}; usuário:{model?.UsuarioGfa}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("RegistrarNaturezaAtendimento")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RegistrarNaturezaAsync(RegistrarNaturezaViewModel model)
        {
            try
            {
                _logger.LogInformation($"Registrar natureza do atendimento [nº protocolo:{model?.NumeroProtocolo}; id natureza:{model?.NaturezaId}].");

                var result = await _atendimentoBusiness.RegistrarNaturezaAsync(model);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao registrar natureza do atendimento [nº protocolo:{model?.NumeroProtocolo}; id natureza:{model?.NaturezaId}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpPost]
        [Route("GerarProtocoloViaWhatsApp")]
        [ProducesResponseType(typeof(GerarProtocoloAtendimentoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GerarProtocoloViaWhatsAppAsync(ProtocoloAtendimentoRequestViewModel protocoloAtendimentoRequestViewModel)
        {
            return await GerarProtocoloAsync(protocoloAtendimentoRequestViewModel, CanalAtendimentoEnum.Whatsapp);
        }

        [HttpPost]
        [Route("GerarProtocoloViaWebChat")]
        [ProducesResponseType(typeof(GerarProtocoloAtendimentoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GerarProtocoloViaWebChatAsync(ProtocoloAtendimentoRequestViewModel protocoloAtendimentoRequestViewModel)
        {
            return await GerarProtocoloAsync(protocoloAtendimentoRequestViewModel, CanalAtendimentoEnum.WebChat);
        }

        [HttpPost]
        [Route("GerarProtocoloViaFacebook")]
        [ProducesResponseType(typeof(GerarProtocoloAtendimentoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GerarProtocoloViaFacebookAsync(ProtocoloAtendimentoRequestViewModel protocoloAtendimentoRequestViewModel)
        {
            return await GerarProtocoloAsync(protocoloAtendimentoRequestViewModel, CanalAtendimentoEnum.RedesSociais);
        }

        [HttpGet]
        [Route("FinalizarProtocoloAtendimento")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> FinalizarProtocoloAtendimentoAsync([FromQuery]string numeroProtocolo)
        {
            try
            {
                var result = await _atendimentoBusiness.FinalizarProtocoloAtendimentoAsync(numeroProtocolo);

                if (result.Sucesso) return Ok(result);

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao finalizar protocolo de atendimento [nº:{numeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        #region [Private methods]

        private async Task<IActionResult> GerarProtocoloAsync(ProtocoloAtendimentoRequestViewModel protocoloAtendimentoRequestViewModel, CanalAtendimentoEnum canalAtendimento)
        {
            try
            {
                _logger.LogInformation($"Criar protocolo {canalAtendimento.GetDescription()} [ligação:{protocoloAtendimentoRequestViewModel?.Matricula}].");

                var result = await _atendimentoBusiness.GerarProtocoloAtendimentoAsync(protocoloAtendimentoRequestViewModel?.Matricula, 
                                                                                       canalAtendimento,
                                                                                       protocoloAtendimentoRequestViewModel?.DataFim,
                                                                                       false);

                if (result.Sucesso)
                {
                    if (protocoloAtendimentoRequestViewModel?.NaturezaSolicitacao != null)
                    {
                        var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                        {
                            NaturezaId = (int)protocoloAtendimentoRequestViewModel.NaturezaSolicitacao,
                            NumeroProtocolo = result.NumeroProtocolo,
                            RotinaId = ID_ROTINA
                        });

                        if (!registrarNaturezaResult.Sucesso)
                        {
                            foreach (var message in registrarNaturezaResult.Mensagens)
                            {
                                _logger.LogError(message);
                            }
                        }
                    }

                    _logger.LogInformation($"Protocolo de atendimento [nº:{result.NumeroProtocolo}] gerado via {canalAtendimento.GetDescription()}.");

                    return Created(String.Empty, result);
                }

                result.AdicionaMensagem("Sistema indisponível! Tente novamente.");
                result.Sucesso = false;

                _logger.LogInformation($"Não foi possível gerar protocolo de atendimento via {canalAtendimento.GetDescription()} para o cliente [matricula:{protocoloAtendimentoRequestViewModel?.Matricula}].");

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao Autenticar via {canalAtendimento.GetDescription()} [matricula:{protocoloAtendimentoRequestViewModel?.Matricula}].");

                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        #endregion

    }
}
