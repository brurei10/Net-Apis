﻿using GAB.Inova.API.Atendimento.Models;
using GAB.Inova.Common.ViewModels.Atendimento;
using System.Threading.Tasks;

namespace GAB.Inova.API.Atendimento.Interfaces
{
    public interface IInovaTicker
    {

        Task AddGfaConnection(UserManager userManager);

        Task AddInovaConnection(UserManager userManager);

        Task RemoveGfaConnection(string username, string empresaTipo);

        Task RemoveInovaConnection(string connectionId);

        Task BroadcastAbrirModalFinalizarProtocolo(FinalizarIntegracaoGfaResponseViewModel model);

        Task BroadcastCancelarAtendimento(CancelarIntegracaoGfaResponseViewModel model);

        Task BroadcastFinalizarAtendimento(FinalizarIntegracaoGfaResponseViewModel model);

        Task BroadcastIniciarAtendimento(IniciarIntegracaoGfaResponseViewModel model);

        Task BroadcastInformarSituacaoUsuarioNoPontoDeAtendimento(string username, bool conectado);

        Task<TModel> UsuarioConectadoAoInova<TModel>(string username) where TModel : new();

        Task UsuarioConectadoAoGfa(string username, string empresaTipo);

    }
}
