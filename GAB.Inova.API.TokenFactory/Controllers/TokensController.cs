﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Token;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.TokenFactory.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Toekn
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/")]
    public class TokensController : ControllerBase
    {

        readonly ITokenBusiness _tokenBusiness;
        readonly ILogger<TokensController> _logger;

        /// <summary>
        /// Contrutor da classe
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="tokenBusiness">Instância do objeto com as regras de negócio referente a Toekn</param>
        public TokensController(ILogger<TokensController> logger, ITokenBusiness tokenBusiness)
        {
            _tokenBusiness = tokenBusiness;
            _logger = logger;
        }

        /// <summary>
        /// Obter token válido
        /// </summary>
        /// <param name="tokenRequest">Uma lista de Claims</param>
        /// <returns>Um token válido</returns>
        [HttpPost]
        [ProducesResponseType(typeof(TokenResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostAsync(TokenRequestViewModel tokenRequest)
        {
            try
            {
                _logger.LogInformation("Geração de token.");

                var result = await _tokenBusiness.GerarTokenAsync(tokenRequest.Claims, null);

                if (result.Sucesso) return Ok(result);

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Erro ao Gerar token.");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };
                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Validar um token
        /// </summary>
        /// <param name="token">Token</param>
        /// <returns>Validação do token</returns>
        [HttpGet]
        [Route("Valida")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ValidarAsync([FromQuery]string token)
        {
            try
            {
                _logger.LogInformation($"Validação de token [{token}].");

                await _tokenBusiness.ValidarTokenAsync(token);

                return Ok(new BaseResponseViewModel());
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Erro ao validar token.");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };
                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }
    }
}
