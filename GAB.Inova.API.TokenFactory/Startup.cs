﻿using GAB.Inova.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace GAB.Inova.API.TokenFactory
{
    /// <summary>
    /// Classe para inicio de uma orquestração
    /// </summary>
    public class Startup
    {
        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public Startup(IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
               .AddEnvironmentVariables();

            _configuration = builder.Build();
        }

        /// <summary>
        /// Adicionar serviços ao contêiner
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {

            services
                .ConfigureTokenServices();

            services
                .AddControllers()
                .AddNewtonsoftJson(opcoes => opcoes.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);
        }

        /// <summary>
        /// Configurar o pipeline de solicitação de HTTP
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="env"><see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Grupo Aguas do Brasil");
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
