﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Servico;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GAB.Inova.API.Servico.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de operação de água
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class OperacaoAguaController : Controller
    {
        const long ID_ROTINA = 9999999987;                   //API Inova 2.0 - Serviço
        const int ID_NATUREZA_INFORMACAO_ABASTECIMENTO = 15; //INFORMAÇÃO SOBRE INTERRUPCAO DE ABASTECIMENTO
        const int ID_NATUREZA_RECLAMACAO_ABASTECIMENTO = 26; //RECLAÇÃO DE INTERRUPCAO DE ABASTECIMENTO

        private readonly ILogger<OperacaoAguaController> _logger;

        private readonly IOperacaoAguaBusiness _operacaoAguaBusiness;
        private readonly IAtendimentoBusiness _atendimentoBusiness;

        public OperacaoAguaController(ILogger<OperacaoAguaController> logger,
                                      IOperacaoAguaBusiness operacaoAguaBusiness,
                                      IAtendimentoBusiness atendimentoBusiness)
        {
            _logger = logger;
            _operacaoAguaBusiness = operacaoAguaBusiness;
            _atendimentoBusiness = atendimentoBusiness;
        }

        /// <summary>
        /// Método que retorna se há interrupção de abastecimento
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ConsultarInterrupcaoAbastecimento")]
        [ProducesResponseType(typeof(InterrupcaoAbastecimentoResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConsultarInterrupcaoAbastecimentoAsync([FromQuery] string numeroProtocolo)
        {
            BaseResponseViewModel result = new InterrupcaoAbastecimentoResponseViewModel();

            try
            {
                _logger.LogInformation($"Consultando interrução de abastecimento");

                result = await _operacaoAguaBusiness.ConsultarInterrupcaoAbastecimentoAsync(numeroProtocolo);

                if (result.Sucesso)
                {
                    result.Sucesso = result.Mensagens.Count() == 0;

                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        //se existir interrupção, registra reclamação, senão informação.
                        NaturezaId = result.Sucesso ? ID_NATUREZA_RECLAMACAO_ABASTECIMENTO : ID_NATUREZA_INFORMACAO_ABASTECIMENTO,
                        NumeroProtocolo = numeroProtocolo,
                        RotinaId = ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Consultar interrução de abastecimento");

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

    }
}
