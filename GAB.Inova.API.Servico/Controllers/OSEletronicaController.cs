﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Servico;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Servico.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de ordem de serviço eletrônica
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class OSEletronicaController : ControllerBase
    {
        private IOrdemServicoEletronicaBusiness _osEletronicaBusiness { get; set; }
        private readonly ILogger<OSEletronicaController> _logger;

        public OSEletronicaController(ILogger<OSEletronicaController> logger, 
                                      IOrdemServicoEletronicaBusiness osEletronicaBusiness)
        {
            _logger = logger;
            _osEletronicaBusiness = osEletronicaBusiness;
        }

        /// <summary>
        /// Método que recebe a Ordem de Serviço Eletrônica do Sistema Especialista
        /// </summary>
        /// <param name="value">DTO</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ReceberOrdemServico")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ReceberOrdemServicoAsync(OsEletronicaRequestViewModel value)
        {
            var retorno = new BaseResponseViewModel();

            try
            {
                _logger.LogInformation("Incluindo OS Eletrônica", "");
                _logger.LogDebug($"ModelRecebido: { JsonConvert.SerializeObject(value)}");

                retorno = await _osEletronicaBusiness.SalvarRequisicaoAsync(value);

                if (retorno.Sucesso)
                {
                    var msg = $"OS Eletronica '{(value?.OrdemServicoPrincipal?.IdentificadorExterno ?? "nao identificada")}' recebida com sucesso!";
                    _logger.LogInformation(msg);
                    retorno.AdicionaMensagem(msg);
                    return Ok(retorno);
                }
                else
                {
                    foreach (var mensagemErro in retorno.Mensagens)
                    {
                        _logger.LogError($"Erro ao receber a OS Eletronica '{(value?.OrdemServicoPrincipal?.IdentificadorExterno ?? "nao identificada")}' - {mensagemErro}");
                    }

                    if (!_logger.IsEnabled(LogLevel.Debug))
                    {
                        _logger.LogError($"Incluindo OS Eletrônica - ModelRecebido: { JsonConvert.SerializeObject(value)}");
                    }

                    retorno.Sucesso = false;
                    return BadRequest(retorno);
                }
            }
            catch (Exception ex)
            {
                var msg = $"Ocorreu uma excecao ao receber a OS Eletronica '{(value?.OrdemServicoPrincipal?.IdentificadorExterno ?? "nao identificada")}' - {ex.Message}.";
                _logger.LogCritical(ex, msg);

                if (!_logger.IsEnabled(LogLevel.Debug))
                {
                    _logger.LogCritical($"Incluindo OS Eletrônica - ModelRecebido: { JsonConvert.SerializeObject(value)}");
                }

                retorno.AdicionaMensagem(msg);
                retorno.Sucesso = false;
                return StatusCode(500, retorno);
            }
        }
    }
}
