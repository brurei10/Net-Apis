﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Servico;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GAB.Inova.API.Servico.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de ordem de serviço não eletrônica
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize("Bearer")]
    public class OrdemServicoController : Controller
    {        
        private readonly ILogger<OrdemServicoController> _logger;

        private readonly IOrdemServicoBusiness _ordemServicoBusiness;

        public OrdemServicoController(ILogger<OrdemServicoController> logger,
                                      IOrdemServicoBusiness ordemServicoBusiness)
        {
            _logger = logger;
            _ordemServicoBusiness = ordemServicoBusiness;
        }

        /// <summary>
        /// Método que retorna dados da ordem de serviço
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ConsultarOrdemServico")]
        [ProducesResponseType(typeof(ConsultaOsResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ConsultarOrdemServicoAsync([FromQuery] long numeroOrdemServico)
        {
            BaseResponseViewModel result = new ConsultaOsResponseViewModel();

            try
            {
                _logger.LogInformation($"Consultando ordem de serviço [{numeroOrdemServico}]");

                result = await _ordemServicoBusiness.ConsultarOrdemServicoAsync(numeroOrdemServico);

                if (result.Sucesso)
                {
                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Consultando ordem de serviço [{numeroOrdemServico}]");

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço.");

                result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

    }
}
