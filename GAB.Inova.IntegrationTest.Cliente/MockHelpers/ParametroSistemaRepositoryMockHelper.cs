﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório do parametro do sistema
    /// </summary>
    /// <returns><see cref="IParametroSistemaRepository"/></returns>
    public static class ParametroSistemaRepositoryMockHelper
    {
        public static IParametroSistemaRepository GetParametroSistemaRepository()
        {
            var parametroSistema708 = GeraParametroSistema708();

            var mockParametroSistemaRepository = new Mock<IParametroSistemaRepository>();

            mockParametroSistemaRepository.Setup(m => m.ObterAsync(It.IsAny<long>())).ReturnsAsync(parametroSistema708);

            return mockParametroSistemaRepository.Object;
        }

        #region [Metodos privados]

        private static ParametroSistema GeraParametroSistema708()
        {
            return new ParametroSistema
            {
                Id = 708,
                Descricao = "TEMPO EXPIRAÇÃO TOKEN DE SOLICITAÇÃO CONTA DIGITAL",
                IdUnidadeMedida = 88,
                Situacao = 1,
                Valor = 120,
                UnidadeMedida = new UnidadeMedida
                {
                    Id = 88,
                    Descricao = "MINUTOS",
                    Sigla = "MIN   "
                }
            };
        }

        #endregion
    }
}
