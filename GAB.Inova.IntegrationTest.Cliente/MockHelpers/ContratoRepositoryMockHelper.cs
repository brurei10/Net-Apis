﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela contrato
    /// </summary>
    /// <returns><see cref="IContratoRepository"/></returns>
    public static class ContratoRepositoryMockHelper
    {
        public static IContratoRepository GetContratoRepository()
        {
            var contrato = GeraContrato();

            var mockContratoRepository = new Mock<IContratoRepository>();

            mockContratoRepository.Setup(m => m.ObterPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(contrato);
            mockContratoRepository.Setup(m => m.AtualizarEmailsAsync(It.IsAny<Contrato>())).ReturnsAsync(true);

            return mockContratoRepository.Object;
        }

        #region [Metodos privados]

        private static Contrato GeraContrato()
        {
            return new Contrato
            {
                Id = 278720,
                CpfCnpj = "07804814887",
                Matricula = "0100000004",
                NomeCliente = "JOSE GUIRAO SANCHES",
                TelefoneResidencial = "1532813356 ",
                TelefoneComercial = null,
                Celular = "15997059108",
                Email = "teste@grupoaguasdobrasil.com.br",
                EmailContaDigital = "teste@grupoaguasdobrasil.com.br",
                Numero = "352",
                Complemento = " ",
                PontoReferencia = "PERTO DA LOJA DE GAZ",
                Logradouro = new Logradouro
                {
                    Id = 4262,
                    Nome = "AFONSO VERGUEIRO",
                    Cep = "18190000",
                    Bairro = new Bairro
                    {
                        Id = 2141,
                        Nome = "CENTRO"
                    },
                    Cidade = new Cidade
                    {
                        Id = 6672,
                        Nome = "ARAÇOIABA DA SERRA"
                    },
                    Estado = new Estado
                    {
                        Id = 24,
                        Nome = "SÃO PAULO"
                    },
                    LogradouroTipo = new LogradouroTipo
                    {
                        Id = 41,
                        Nome = "RUA"
                    }
                }
            };
        }

        #endregion
    }
}
