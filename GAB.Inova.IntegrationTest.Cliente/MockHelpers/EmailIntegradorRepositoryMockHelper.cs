﻿using GAB.Inova.Common.ViewModels.SendGrid;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório de mensagem do integrador movimento
    /// </summary>
    /// <returns><see cref="IEmailIntegradorRepository"/></returns>
    public static class EmailIntegradorRepositoryMockHelper
    {
        public static IEmailIntegradorRepository GetEmailIntegradorRepository()
        {
            var mockEmailIntegradorRepository = new Mock<IEmailIntegradorRepository>();

            mockEmailIntegradorRepository.Setup(m => m.EnviarEmailAsync(It.IsAny<EmailRequestViewModel>())).ReturnsAsync(new EmailIntegradorResponse("1"));

            return mockEmailIntegradorRepository.Object;
        }
    }
}
