﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório de histórico de protocolo de atendimento
    /// </summary>
    /// <returns><see cref="IProtocoloAtendimentoHistoricoRepository"/></returns>
    public static class ProtocoloAtendimentoHistoricoRepositoryMockHelper
    {
        public static IProtocoloAtendimentoHistoricoRepository GetProtocoloAtendimentoHistoricoRepository()
        {
            var mockProtocoloAtendimentoHistoricoRepository = new Mock<IProtocoloAtendimentoHistoricoRepository>();

            mockProtocoloAtendimentoHistoricoRepository.Setup(m => m.InserirAsync(It.IsAny<ProtocoloAtendimentoHistorico>())).ReturnsAsync(true);

            return mockProtocoloAtendimentoHistoricoRepository.Object;
        }
    }
}
