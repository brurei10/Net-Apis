﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela processo cliente
    /// </summary>
    /// <returns><see cref="IProcessoClienteRepository"/></returns>
    public static class ProcessoClienteRepositoryMockHelper
    {
        public static IProcessoClienteRepository GetProcessoClienteRepository()
        {
            var mockProcessoClienteRepository = new Mock<IProcessoClienteRepository>();

            mockProcessoClienteRepository.Setup(m => m.VerificarClienteSobJudice(It.IsAny<string>())).ReturnsAsync(default(ProcessoCliente));

            return mockProcessoClienteRepository.Object;
        }
    }
}
