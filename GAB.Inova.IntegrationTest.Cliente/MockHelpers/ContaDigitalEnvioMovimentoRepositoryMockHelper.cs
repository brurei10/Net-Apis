﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da conta digital envio movimento
    /// </summary>
    /// <returns><see cref="IContaDigitalEnvioMovimentoRepository"/></returns>
    public static class ContaDigitalEnvioMovimentoRepositoryMockHelper
    {
        public static IContaDigitalEnvioMovimentoRepository GetContaDigitalEnvioMovimentoRepository()
        {
            var mockContaDigitalEnvioMovimentoRepository = new Mock<IContaDigitalEnvioMovimentoRepository>();

            mockContaDigitalEnvioMovimentoRepository.Setup(m => m.InserirAsync(It.IsAny<ContaDigitalEnvioMovimento>())).ReturnsAsync(true);

            return mockContaDigitalEnvioMovimentoRepository.Object;
        }
    }
}
