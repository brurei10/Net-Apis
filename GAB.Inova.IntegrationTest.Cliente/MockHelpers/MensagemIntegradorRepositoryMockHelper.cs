﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório de mensagem do integrador
    /// </summary>
    /// <returns><see cref="IMensagemIntegradorRepository"/></returns>
    public static class MensagemIntegradorRepositoryMockHelper
    {
        public static IMensagemIntegradorRepository GetMensagemIntegradorRepository()
        {
            var mockMensagemIntegradorRepository = new Mock<IMensagemIntegradorRepository>();

            mockMensagemIntegradorRepository.Setup(m => m.InserirAsync(It.IsAny<MensagemIntegrador>())).ReturnsAsync(true);
            mockMensagemIntegradorRepository.Setup(m => m.AtualizarAsync(It.IsAny<MensagemIntegrador>())).ReturnsAsync(true);

            return mockMensagemIntegradorRepository.Object;
        }
    }
}
