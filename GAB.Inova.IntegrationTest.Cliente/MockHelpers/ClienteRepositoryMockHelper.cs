﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela cliente
    /// </summary>
    /// <returns><see cref="IClienteRepository"/></returns>
    public static class ClienteRepositoryMockHelper
    {
        public static IClienteRepository GetClienteRepository()
        {
            var cliente = GeraCliente();

            var mockClienteRepository = new Mock<IClienteRepository>();

            mockClienteRepository.Setup(m => m.ObterPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(cliente);
            mockClienteRepository.Setup(m => m.ObterPorMatriculaECpfCnpjAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(cliente);
            mockClienteRepository.Setup(m => m.AtualizaGrupoEntregaAsync(It.IsAny<string>(), It.IsAny<long>())).ReturnsAsync(true);
            mockClienteRepository.Setup(m => m.ObterDataProximaLeituraAsync(It.IsAny<string>())).ReturnsAsync(Convert.ToDateTime("15/07/2020"));
            mockClienteRepository.Setup(m => m.PodeAderirContaDigital(It.IsAny<Domain.Cliente>())).Returns(new ValidationResult());
            mockClienteRepository.Setup(m => m.PodeAlterarContaDigital(It.IsAny<Domain.Cliente>())).Returns(new ValidationResult());
            mockClienteRepository.Setup(m => m.PodeCancelarContaDigital(It.IsAny<Domain.Cliente>())).Returns(new ValidationResult());

            return mockClienteRepository.Object;
        }

        #region [Metodos privados]

        private static Domain.Cliente GeraCliente()
        {
            return new Domain.Cliente
            {
                Localizacao = "01100103012334017021024001",
                DigitoLocalizacao = "2",
                Matricula = "0100000004",
                Nome = "CLIENTE PARA TESTE AUTOMATIZADO",
                IdTipoFatura = 0,
                IdTipoCliente = 0,
                SituacaoAgua = 1,
                SituacaoEsgoto = 1,
                SituacaoHd = 3,
                DiaVencimentoConta = 0,
                DigitoMatricula = null,
                DebitoAutomatico = false,
                IdGrupoEntrega = 0,
                Contrato = new Contrato
                {
                    Id = 278720,
                    CpfCnpj = "07804814887",
                    Matricula = "0100000004",
                    NomeCliente = "CLIENTE PARA TESTE AUTOMATIZADO",
                    TelefoneResidencial = "1532813356 ",
                    TelefoneComercial = null,
                    Celular = "15997059108",
                    Email = "teste@grupoaguasdobrasil.com.br",
                    EmailContaDigital = "teste@grupoaguasdobrasil.com.br",
                    Numero = "352",
                    Complemento = " ",
                    PontoReferencia = "PERTO DA LOJA DE GAZ",
                    Logradouro = new Logradouro
                    {
                        Id = 4262,
                        Nome = "AFONSO VERGUEIRO",
                        Cep = "18190000",
                        Bairro = new Bairro
                        {
                            Id = 2141,
                            Nome = "CENTRO"
                        },
                        Cidade = new Cidade
                        {
                            Id = 6672,
                            Nome = "ARAÇOIABA DA SERRA"
                        },
                        Estado = new Estado
                        {
                            Id = 24,
                            Nome = "SÃO PAULO"
                        },
                        LogradouroTipo = new LogradouroTipo
                        {
                            Id = 41,
                            Nome = "RUA"
                        }
                    }
                },
                ValidationResult = null
            };
        }

        #endregion
    }
}
