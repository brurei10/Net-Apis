﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da conta digital envio
    /// </summary>
    /// <returns><see cref="IContaDigitalEnvioRepository"/></returns>
    public static class ContaDigitalEnvioRepositoryMockHelper
    {
        public static IContaDigitalEnvioRepository GetContaDigitalEnvioRepository()
        {
            var mockContaDigitalEnvioRepository = new Mock<IContaDigitalEnvioRepository>();

            mockContaDigitalEnvioRepository.Setup(m => m.InserirAsync(It.IsAny<ContaDigitalEnvio>())).ReturnsAsync(true);
            mockContaDigitalEnvioRepository.Setup(m => m.AtualizarAsync(It.IsAny<ContaDigitalEnvio>())).ReturnsAsync(true);

            return mockContaDigitalEnvioRepository.Object;
        }
    }
}
