﻿using DomainValidationCore.Validation;
using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela cliente token
    /// </summary>
    /// <returns><see cref="IClienteTokenRepository"/></returns>
    public static class ClienteTokenRepositoryMockHelper
    {
        public static IClienteTokenRepository GetClienteTokenRepository()
        {
            var clienteToken = GeraClienteToken();

            var mockClienteTokenRepository = new Mock<IClienteTokenRepository>();

            mockClienteTokenRepository.Setup(m => m.ObterPorTokenAsync(It.IsAny<string>())).ReturnsAsync(clienteToken);
            mockClienteTokenRepository.Setup(m => m.PodeConfirmar(It.IsAny<ClienteToken>())).Returns(new ValidationResult());
            mockClienteTokenRepository.Setup(m => m.ConfirmarAsync(It.IsAny<long>())).ReturnsAsync(true);
            mockClienteTokenRepository.Setup(m => m.InserirAsync(It.IsAny<ClienteToken>())).ReturnsAsync(true);

            return mockClienteTokenRepository.Object;
        }

        #region [Metodos privados]

        private static ClienteToken GeraClienteToken()
        {
            return new ClienteToken
            {
                Id = 2648,
                IdProtocoloAtendimento = 264904,
                Matricula = "0100000004",
                Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjFjNTc0ZDZmLTNkZDItNDcwZi1iZWUzLWI2M2FjMjEwNDVmYSIsIkNvZGlnb0VtcHJlc2EiOiJDQUEiLCJNYXRyaWN1bGEiOiIwMTAwMDAwMDA0IiwiRW1haWxDYWRhc3RybyI6InRlc3RlQGdydXBvYWd1YXNkb2JyYXNpbC5jb20uYnIiLCJNb2RpZmljYUVtYWlsQ29udGF0byI6ImZhbHNlIiwibmJmIjoxNTkzODgyOTcwLCJleHAiOjE5MDk0MTU3NzAsImlhdCI6MTU5Mzg4Mjk3NywiaXNzIjoiaHR0cDovL2FwaS5ncnVwb2FndWFzZG9icmFzaWwuY29tLmJyIiwiYXVkIjoiSW5vdmEifQ.x6VplWbcad1UcMUXAT9Ba2slvetrjb7m2H1gH6dHGGU",
                Tipo = TokenTipoEnum.ContaDigitalAdesao,
                Sistema = SistemaEnum.Inova
            };
        }

        #endregion
    }
}
