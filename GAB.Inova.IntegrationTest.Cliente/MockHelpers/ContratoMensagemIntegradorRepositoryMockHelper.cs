﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório de mensagem do integrador movimento
    /// </summary>
    /// <returns><see cref="IContratoMensagemIntegradorRepository"/></returns>
    public static class ContratoMensagemIntegradorRepositoryMockHelper
    {
        public static IContratoMensagemIntegradorRepository GetContratoMensagemIntegradorRepository()
        {
            var mockContratoMensagemIntegradorRepository = new Mock<IContratoMensagemIntegradorRepository>();

            mockContratoMensagemIntegradorRepository.Setup(m => m.InserirAsync(It.IsAny<ContratoMensagemIntegrador>())).ReturnsAsync(true);

            return mockContratoMensagemIntegradorRepository.Object;
        }
    }
}
