﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da conta
    /// </summary>
    /// <returns><see cref="IContaRepository"/></returns>
    public static class ContaRepositoryMockHelper
    {
        public static IContaRepository GetContaRepository()
        {
            var conta = GeraContaNaoPaga();

            var mockContaRepository = new Mock<IContaRepository>();

            mockContaRepository.Setup(m => m.ObterContaPorSeqOriginalAsync(It.IsAny<long>(), It.IsAny<TipoContaEnum>())).ReturnsAsync(conta);

            return mockContaRepository.Object;
        }

        #region [Metodos privados]

        private static Conta GeraContaNaoPaga()
        {
            return new Conta
            {
                Id = 1450926,
                TipoConta = 0,
                Matricula = "0100000004",
                AnoMes = "202005",
                SituacaoConta = SituacaoContaEnum.NaoPaga,
                Vencimento = Convert.ToDateTime("01/06/2020"),
                ValorAgua = 52.54M,
                ValorEsgoto = 50.15M,
                ValorServico = 0.0M,
                ValorMulta = 0.0M,
                ValorIcfrf = 0.0M,
                ValorDesconto = 0.0M,
                ValorJuro = 0.0M,
                ValorDevolucao = 0.0M,
                ValorTotal = 102.69M,
                Emissao = Convert.ToDateTime("15/05/2020"),
                IdContrato = 278720,
                ValorRecursosHidricosAgua = 0.0M,
                ValorRecursosHidricosEsgoto = 0.0M,
                ValorDescontoResidencialSocial = 0.0M,
                ValorDescontoPequenoComercio = 0.0M,
                ValorEsgotoAlternativo = 0.0M,
                ValorDescontoLigacaoEstimada = 0.0M,
                ValorDescontoRetificado = 0.0M,
                ValorDescontoPoderPublicoConcedente = 0.0M,
                SituacaoNegativacao = 1,
                Localizacao = "01100103012334017021024001",
                IdCiclo = 17,
                TipoEntrega = 0,
                IdCentralizador = 0,
                QuantidadeDebito = 0,
                QuantidadePagamento = 0,
                IdMensagemMes = 0,
                IdAviso = 0,
                PagamentoPrazo = 0,
                PagamentoFora = 0,
                ValorComercial = 0.0M,
                ValorICMS = 0.0M,
                ValorTerceiro = 0.0M,
                ValorCorrecaoMonetaria = 0.0M
            };
        }

        #endregion
    }
}
