﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Cliente.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view cliente
    /// </summary>
    /// <returns><see cref="IVwClienteRepository"/></returns>
    public static class VwClienteRepositoryMockHelper
    {
        public static IVwClienteRepository GetVwClienteRepository()
        {
            var listaVwCliente = GeraListaVwCliente();

            var mockVwClienteRepository = new Mock<IVwClienteRepository>();

            mockVwClienteRepository.Setup(m => m.ObterPorDocumentoAsync(It.IsAny<string>())).ReturnsAsync(listaVwCliente);

            return mockVwClienteRepository.Object;
        }

        #region [Metodos privados]
       
        private static IEnumerable<VwCliente> GeraListaVwCliente()
        {
            return new List<VwCliente>
            {
                new VwCliente
                {
                    Matricula = "0100000004",
                    Endereco = "RUA DOUTOR AFONSO VERGUEIRO",
                    NumeroImovel = "352",
                    NomeBairro = "CENTRO",
                    Documento = "07804814887"
                }
            };
        }

        #endregion
    }
}
