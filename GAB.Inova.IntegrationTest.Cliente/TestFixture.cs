﻿using GAB.Inova.Common.Interfaces.Providers;
using GAB.Inova.Common.Providers;
using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using GAB.Inova.IntegrationTest.Cliente.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Cliente
{
    /// <summary>
    /// Classe acessória
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public class TestFixture<TStartup> 
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            #region [Aplicação]

            var webHostEnvironment = ConfigurationMockHelper.GetHostingEnvironment();
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor()); 
            services.AddSingleton<IWebHostEnvironment>(webHostEnvironment);
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(webHostEnvironment));

            #endregion

            #region [Repository]

            services.AddSingleton<IUnitOfWork>(UnitOfWorkMockHelper.GetUnitOfWork());
            services.AddSingleton<IClienteRepository>(ClienteRepositoryMockHelper.GetClienteRepository());
            services.AddSingleton<IVwClienteRepository>(VwClienteRepositoryMockHelper.GetVwClienteRepository());
            services.AddSingleton<IClienteTokenRepository>(ClienteTokenRepositoryMockHelper.GetClienteTokenRepository());
            services.AddSingleton<IContratoRepository>(ContratoRepositoryMockHelper.GetContratoRepository());
            services.AddSingleton<IProcessoClienteRepository>(ProcessoClienteRepositoryMockHelper.GetProcessoClienteRepository());
            services.AddSingleton<IProtocoloAtendimentoRepository>(ProtocoloAtendimentoRepositoryMockHelper.GetProtocoloAtendimentoRepository());
            services.AddSingleton<IProtocoloAtendimentoDetalheRepository>(ProtocoloAtendimentoDetalheRepositoryMockHelper.GetProtocoloAtendimentoDetalheRepository());
            services.AddSingleton<IProtocoloAtendimentoHistoricoRepository>(ProtocoloAtendimentoHistoricoRepositoryMockHelper.GetProtocoloAtendimentoHistoricoRepository());
            services.AddSingleton<IMensagemIntegradorRepository>(MensagemIntegradorRepositoryMockHelper.GetMensagemIntegradorRepository());
            services.AddSingleton<IMensagemIntegradorMovimentoRepository>(MensagemIntegradorMovimentoRepositoryMockHelper.GetMensagemIntegradorMovimentoRepository());
            services.AddSingleton<IContratoMensagemIntegradorRepository>(ContratoMensagemIntegradorRepositoryMockHelper.GetContratoMensagemIntegradorRepository());
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());
            services.AddSingleton<IParametroSistemaRepository>(ParametroSistemaRepositoryMockHelper.GetParametroSistemaRepository());
            services.AddSingleton<IContaRepository>(ContaRepositoryMockHelper.GetContaRepository());
            services.AddSingleton<IContaDigitalEnvioRepository>(ContaDigitalEnvioRepositoryMockHelper.GetContaDigitalEnvioRepository());
            services.AddSingleton<IContaDigitalEnvioMovimentoRepository>(ContaDigitalEnvioMovimentoRepositoryMockHelper.GetContaDigitalEnvioMovimentoRepository());

            #endregion
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }
        
    }
}