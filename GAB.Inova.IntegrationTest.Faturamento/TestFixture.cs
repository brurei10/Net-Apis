﻿using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.Domain.Interfaces.Repositories.Bases;
using GAB.Inova.IntegrationTest.Faturamento.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Faturamento
{
    /// <summary>
    /// Classe acessória
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public class TestFixture<TStartup> 
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            #region [Aplicação]

            var webHostEnvironment = ConfigurationMockHelper.GetHostingEnvironment();
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor());
            services.AddSingleton<IWebHostEnvironment>(webHostEnvironment);
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(webHostEnvironment));

            #endregion

            #region [Repository]
            
            services.AddSingleton<IContaRepository>(ContaRepositoryMockHelper.GetContaRepository());
            services.AddSingleton<IPdfGenericoRepository>(PdfGenericoRepositoryMockHelper.GetPdfGenericoRepository());
            services.AddSingleton<ITempImprimeContaRepository>(TempImprimeContaRepositoryMockHelper.GetTempImprimeContaRepository());
            services.AddSingleton<ITempHistoricoConsumoRepository>(TempHistoricoConsumoRepositoryMockHelper.GetTempHistoricoConsumoRepository());
            services.AddSingleton<ITabAnaliseAguaRepository>(TabAnaliseAguaRepositoryMockHelper.GetTabAnaliseAguaRepository());
            services.AddSingleton<IFaturamentoRepository>(FaturamentoRepositoryMockHelper.GetFaturamentoRepository());
            services.AddSingleton<ITarifaDiariaRepository>(TarifaDiariaRepositoryMockHelper.GetTarifaDiariaRepository());
            services.AddSingleton<IParametroSistemaRepository>(ParametroSistemaRepositoryMockHelper.GetParametroSistemaRepository());
            services.AddSingleton<IProtocoloAtendimentoRepository>(ProtocoloAtendimentoRepositoryMockHelper.GetProtocoloAtendimentoRepository());
            services.AddSingleton<IContratoRepository>(ContratoRepositoryMockHelper.GetContratoRepository());
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());
            services.AddSingleton<IProcessoClienteRepository>(ProcessoClienteRepositoryMockHelper.GetProcessoClienteRepository());
            services.AddSingleton<IEmpresaRepository>(EmpresaRepositoryMockHelper.GetEmpresaRepository());
            services.AddSingleton<IFeriadoRepository>(FeriadoRepositoryMockHelper.GetFeriadoRepository());
            services.AddSingleton<ITempCobrancaCtRepository>(TempCobrancaCtRepositoryMockHelper.GetTempCobrancaCtRepository());
            services.AddSingleton<ICompoeCtRepository>(CompoeCtRepositoryMockHelper.GetCompoeCtRepository());
            services.AddSingleton<IGRPRepository>(GRPRepositoryMockHelper.GetGRPRepository());
            services.AddSingleton<IUnitOfWork>(UnitOfWorkMockHelper.GetUnitOfWork());
            services.AddSingleton<IImprimeGRPRepository>(ImprimeGRPRepositoryMockHelper.GetImprimeGRPRepository());
            services.AddSingleton<IVwClienteRepository>(VwClienteRepositoryMockHelper.GetVwClienteRepository());
            services.AddSingleton<IParcelaRepository>(ParcelaRepositoryMockHelper.GetParcelaRepository());
            services.AddSingleton<IDocumentoModeloRepository>(DocumentoModeloRepositoryMockHelper.GetDocumentoModeloRepository());

            #endregion
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }
        
    }
}