using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Token;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace GAB.Inova.IntegrationTest.Faturamento
{
    public class FaturamentoTest
        : IClassFixture<TestFixture<Startup>>
    {
        readonly TestFixture<Startup> _fixture;
        readonly IHttpContextAccessor _httpContextAccessor;

        readonly IFaturamentoBusiness _faturamentoBusiness;
        readonly ITokenBusiness _tokenBusiness;

        #region [Construtores]

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="fixture">Indica a classe acess�aria</param>
        public FaturamentoTest(TestFixture<Startup> fixture)
        {
            _fixture = fixture;

            #region [Instanciando vari�veis de aplica��o]

            _httpContextAccessor = _fixture?
                .ServiceProvider?
                .GetService<IHttpContextAccessor>();

            #endregion

            #region [Instanciando vari�veis business]

            _faturamentoBusiness = _fixture?
                .ServiceProvider?
                .GetService<IFaturamentoBusiness>();

            _tokenBusiness = _fixture?
                .ServiceProvider?
                .GetService<ITokenBusiness>();

            #endregion

        }

        #endregion

        #region [M�todos privados]

        private async Task ObterTokenAsync()
        {
            var tokenRequest = new TokenRequestViewModel();

            tokenRequest.Claims.Add(new KeyValuePair<string, string>("CodigoEmpresa", EmpresaTipoEnum.CAA.ToString()));

            var tokenResponse = await _tokenBusiness.GerarTokenAsync(tokenRequest.Claims, 600);

            _httpContextAccessor?
                .HttpContext?
                .Request?
                .Headers?
                .Clear();

            if (!string.IsNullOrEmpty(tokenResponse.Token))
            {
                _httpContextAccessor?
                    .HttpContext?
                    .Request?
                    .Headers?
                    .Add("Authorization", $"Bearer {tokenResponse.Token}");
            }
        }

        #endregion

        [Theory]
        [InlineData(1450926)]
        public async Task TestObterSegundaViaAsync(long seqOriginal)
        {  
            var retorno = await _faturamentoBusiness.ObterSegundaViaAsync(seqOriginal);

            Assert.True(retorno.Sucesso);

            if (retorno.Sucesso)
            {
                if (!Directory.Exists(@"C:\\temp\\testes_automatizados"))
                {
                    Directory.CreateDirectory(@"C:\\temp\\testes_automatizados");
                }

                File.WriteAllBytes($"C:\\temp\\testes_automatizados\\ApiEspelhoConta-{DateTime.Now:yyyyMMddHHmmss}.pdf", Convert.FromBase64String(retorno.EspelhoDaConta));
            }
        }

        [Theory]
        [InlineData(1450926, "teste@grupoaguasdobrasil.com.br")]
        public async Task TestEnviarEmailSegundaViaAsync(long seqOriginal, string email)
        {
            await ObterTokenAsync();

            var retorno = await _faturamentoBusiness.EnviarEmailSegundaViaAsync(seqOriginal, email);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("20200111213222")]
        public async Task TestObterHistoricoContasAsync(string numeroProtocolo)
        {
            var retorno = await _faturamentoBusiness.ObterHistoricoContasAsync(numeroProtocolo);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("20200111213222")]
        public async Task TestObterHistoricoContasEmAbertoAsync(string numeroProtocolo)
        {
            var retorno = await _faturamentoBusiness.ObterHistoricoContasEmAbertoAsync(numeroProtocolo);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData(1)]
        public async Task TestObterCodigoBarrasNotaFiscalAsync(long seqOriginal)
        {
            var retorno = await _faturamentoBusiness.ObterCodigoBarrasNotaFiscalAsync(seqOriginal);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("20200111213222", 1436287, 1450926)]
        public async Task TestObterCodigoBarrasQuitacaoDebitoAsync(string numeroProtocolo, long seqOriginal1, long seqOriginal2)
        {
            var retorno = await _faturamentoBusiness.ObterCodigoBarrasQuitacaoDebitoAsync(new List<long>
            {
                seqOriginal1, 
                seqOriginal2
            }, 
            numeroProtocolo);

            Assert.True(retorno.Sucesso);
        }

        [Theory]
        [InlineData("20200111213222", 1436287, 1450926)]
        public async Task TestObterPdfQuitacaoDebitoAsync(string numeroProtocolo, long seqOriginal1, long seqOriginal2)
        {
            var retorno = await _faturamentoBusiness.ObterPdfQuitacaoDebitoAsync(new List<long>
            {
                seqOriginal1,
                seqOriginal2
            },
            numeroProtocolo);

            Assert.True(retorno.Sucesso);

            if (retorno.Sucesso)
            {
                if (!Directory.Exists(@"C:\\temp\\testes_automatizados"))
                {
                    Directory.CreateDirectory(@"C:\\temp\\testes_automatizados");
                }

                File.WriteAllBytes($"C:\\temp\\testes_automatizados\\ApiEspelhoQuitacao-{DateTime.Now:yyyyMMddHHmmss}.pdf", Convert.FromBase64String(retorno.EspelhoDaQuitacao));
            }
        }
    }
}
