﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela temp GRP
    /// </summary>
    /// <returns><see cref="IGRPRepository"/></returns>
    public static class GRPRepositoryMockHelper
    {
        public static IGRPRepository GetGRPRepository()
        {
            var mockGRPRepository = new Mock<IGRPRepository>();

            mockGRPRepository.Setup(m => m.InserirAsync(It.IsAny<GRP>())).ReturnsAsync(true);

            return mockGRPRepository.Object;
        }
    }
}
