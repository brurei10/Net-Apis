﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela temp compoe CT
    /// </summary>
    /// <returns><see cref="ICompoeCtRepository"/></returns>
    public static class CompoeCtRepositoryMockHelper
    {
        public static ICompoeCtRepository GetCompoeCtRepository()
        {
            var mockCompoeCtRepository = new Mock<ICompoeCtRepository>();

            mockCompoeCtRepository.Setup(m => m.InserirAsync(It.IsAny<CompoeCt>())).ReturnsAsync(true);

            return mockCompoeCtRepository.Object;
        }
    }
}
