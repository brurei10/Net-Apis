﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view de cliente
    /// </summary>
    /// <returns><see cref="IVwClienteRepository"/></returns>
    public static class VwClienteRepositoryMockHelper
    {
        public static IVwClienteRepository GetVwClienteRepository()
        {
            var vwCliente = GeraVwCliente();

            var mockVwClienteRepository = new Mock<IVwClienteRepository>();

            mockVwClienteRepository.Setup(m => m.ObterPorMatriculaAsync(It.IsAny<string>())).ReturnsAsync(vwCliente);

            return mockVwClienteRepository.Object;
        }

        #region [Metodos privados]

        private static VwCliente GeraVwCliente()
        {
            return new VwCliente
            {
                Matricula = "0100000004",
                Nome = "CLIENTE PARA TESTE AUTOMATIZADO",
                Endereco = "RUA DOUTOR AFONSO VERGUEIRO",
                NumeroImovel = "352",
                Complemento = " ",
                NomeBairro = "CENTRO",
                NomeLocal = "ARAÇOIABA DA SERRA",
                EconomiaResidencial = 1,
                EconomiaComercial = 0,
                EconomiaIndustrial = 0,
                EconomiaPublica = 0,
                IdCiclo = 17,
                IdSetor = "0110010017",
                Rota = "001",
                Sequencia = "03970",
                IdHd = "A14S676985     ",
                TipoEntrega = 0,
                Documento = null
            };
        }

        #endregion
    }
}
