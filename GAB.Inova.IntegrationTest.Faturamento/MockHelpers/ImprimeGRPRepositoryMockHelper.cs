﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela imprime GRP
    /// </summary>
    /// <returns><see cref="IImprimeGRPRepository"/></returns>
    public static class ImprimeGRPRepositoryMockHelper
    {
        public static IImprimeGRPRepository GetImprimeGRPRepository()
        {
            var mockImprimeGRPRepository = new Mock<IImprimeGRPRepository>();

            mockImprimeGRPRepository.Setup(m => m.ObterPorSeqOriginal(It.IsAny<long>())).ReturnsAsync(default(ImprimeGRP));
            mockImprimeGRPRepository.Setup(m => m.InserirAsync(It.IsAny<ImprimeGRP>())).ReturnsAsync(true);
            

            return mockImprimeGRPRepository.Object;
        }

        #region [Metodos privados]

        private static ImprimeGRP GeraImprimeGRP()
        {
            return new ImprimeGRP
            {
                Id = 909284718,
                Matricula = "0100000004",
                Titulo = "GUIA DE PAGAMENTO",
                NomeCliente = "JOSE GUIRAO SANCHES",
                Endereco = "RUA DOUTOR AFONSO VERGUEIRO",
                NomeLocal = "ARAÇOIABA DA SERRA",
                ConsumoAnterior01 = "",
                ConsumoAnterior02 = "",
                ConsumoAnterior03 = "",
                ConsumoAnterior04 = "",
                ConsumoAnterior05 = "",
                ConsumoAnterior06 = "",
                ConsumoAnterior07 = "",
                ConsumoAnterior08 = "",
                ConsumoAnterior09 = "",
                ConsumoAnterior10 = "",
                ConsumoAnterior11 = "",
                ConsumoAnterior12 = "",
                EconomiaResidencial = 1,
                EconomiaComercial = 0,
                EconomiaIndustrial = 0,
                EconomiaPublica = 0,
                IdCiclo = 17,
                SetorRotaSequencia = "010.001.03970",
                IdHd = "A14S676985     ",
                TipoEntrega = 0,
                AnoMesCt = "",
                DataLeituraAtual = null,
                DataLeituraAnterior = null,
                ConsumoFaturado = 0,
                LeituraAtual = 0,
                LeituraAnterior = 0,
                Media = 0,
                ValorGRP = 217.47,
                DataVencimento = Convert.ToDateTime("10/07/2020"),
                DataValor = Convert.ToDateTime("10/07/2020"),
                Observacao = "",
                CodigoBarra = "82630000002-1 17471217090-4 92847180010-4 00000040982-1",
                CompoeCt01 = "Pagamento de débito(s)",
                CompoeCt02 = "Parcelas existentes",
                CompoeCt03 = "",
                CompoeCt04 = "",
                CompoeCt05 = "",
                CompoeCt06 = "",
                CompoeCt07 = "",
                CompoeCt08 = "",
                CompoeCt09 = "",
                CompoeCt10 = "",
                CompoeCt11 = null,
                CompoeCt12 = null,
                CompoeCt13 = null,
                CompoeCt14 = null,
                CompoeCt15 = null,
                CompoeCt16 = null,
                EstruturaTarifaria01 = "",
                EstruturaTarifaria02 = "",
                EstruturaTarifaria03 = "",
                EstruturaTarifaria04 = "",
                EstruturaTarifaria05 = "",
                EstruturaTarifaria06 = "",
                EstruturaTarifaria07 = "",
                EstruturaTarifaria08 = "",
                EstruturaTarifaria09 = "",
                EstruturaTarifaria10 = "",
                EstruturaTarifaria11 = "",
                EstruturaTarifaria12 = "",
                EstruturaTarifaria13 = "",
                EstruturaTarifaria14 = "",
                EstruturaTarifaria15 = "",
                EstruturaTarifaria16 = "",
                CompoeValor01 = 217.47,
                CompoeValor02 = 0.0,
                CompoeValor03 = 0.0,
                CompoeValor04 = 0.0,
                CompoeValor05 = 0.0,
                CompoeValor06 = 0.0,
                CompoeValor07 = 0.0,
                CompoeValor08 = 0.0,
                CompoeValor09 = 0.0,
                CompoeValor10 = 0.0,
                CompoeValor11 = 0.0,
                CompoeValor12 = 0.0,
                CompoeValor13 = 0.0,
                CompoeValor14 = 0.0,
                CompoeValor15 = 0.0,
                CompoeValor16 = 0.0,
                IdSolicitacao = 8,
                DataEmissao = Convert.ToDateTime("10/07/2020"),
                DiasConsumo = 0
            };
        }

        #endregion
    }
}
