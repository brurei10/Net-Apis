﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela tarifa diária
    /// </summary>
    /// <returns><see cref="ITarifaDiariaRepository"/></returns>
    public static class TarifaDiariaRepositoryMockHelper
    {
        public static ITarifaDiariaRepository GetTarifaDiariaRepository()
        {
            var listaTarifaDiaria = GeraListaTarifaDiaria();

            var mockTarifaDiariaRepository = new Mock<ITarifaDiariaRepository>();

            mockTarifaDiariaRepository.Setup(m => m.ObterTarifaParaImpressaoContaAsync(It.IsAny<TarifaDiaria>())).ReturnsAsync(listaTarifaDiaria);

            return mockTarifaDiariaRepository.Object;
        }

        #region [Metodos privados]
        private static IEnumerable<TarifaDiaria> GeraListaTarifaDiaria()
        {
            return new List<TarifaDiaria>
            {
                new TarifaDiaria
                {
                    Id = 2458786,
                    TipoTarifa = "1",
                    DataTarifa = Convert.ToDateTime("15/05/2020"),
                    IdCategoria = 1,
                    FaixaAnterior = 0.0,
                    FaixaConsumo = 10.0,
                    ValorFaixa = 2.8566,
                    ValorFaixaEsgoto = 2.7445,
                    ValorAcumulado = 0.0,
                    ValorAcumuladoEsgoto = 0.0,
                    ValorFaixaSocial = null,
                    ValorAcumuladoSocial = null,
                    Localidade = "0110010",
                    GrupoTarifa = "1",
                    IdTarifa = 1,
                    IdFaixaTarifa = 1
                },
                new TarifaDiaria
                {
                    Id = 2458787,
                    TipoTarifa = "1",
                    DataTarifa = Convert.ToDateTime("15/05/2020"),
                    IdCategoria = 1,
                    FaixaAnterior = 10.01,
                    FaixaConsumo = 20.0,
                    ValorFaixa = 3.9966,
                    ValorFaixaEsgoto = 3.7847,
                    ValorAcumulado = 28.5655,
                    ValorAcumuladoEsgoto = 27.4448,
                    ValorFaixaSocial = null,
                    ValorAcumuladoSocial = null,
                    Localidade = "0110010",
                    GrupoTarifa = "1",
                    IdTarifa = 1,
                    IdFaixaTarifa = 2
                }
            };
        }
        #endregion
    }
}
