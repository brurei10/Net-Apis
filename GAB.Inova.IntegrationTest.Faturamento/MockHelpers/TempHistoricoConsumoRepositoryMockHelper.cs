﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela temp historico consumo
    /// </summary>
    /// <returns><see cref="ITempHistoricoConsumoRepository"/></returns>
    public static class TempHistoricoConsumoRepositoryMockHelper
    {
        public static ITempHistoricoConsumoRepository GetTempHistoricoConsumoRepository()
        {
            var listaTempHistoricoConsumo = GeraListaTempHistoricoConsumo();

            var mockTempHistoricoConsumoRepository = new Mock<ITempHistoricoConsumoRepository>();

            mockTempHistoricoConsumoRepository.Setup(m => m.ObterTempHistoricoConsumoAsync(It.IsAny<string>(), It.IsAny<long>())).ReturnsAsync(listaTempHistoricoConsumo);

            return mockTempHistoricoConsumoRepository.Object;
        }

        #region [Metodos privados]
        private static IEnumerable<TempHistoricoConsumo> GeraListaTempHistoricoConsumo()
        {
            return new List<TempHistoricoConsumo>
            {
                new TempHistoricoConsumo
                {
                    Id         = 2955364,
                    Referencia = "04/2020",
                    Consumo    = 17,
                    QtdeDias   = 30,
                    AnoMes     = "05/2020",
                    Matricula  = "0100000004",
                    IdUsuario  = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955363,
                    Referencia = "03/2020",
                    Consumo = 19,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955362,
                    Referencia = "02/2020",
                    Consumo = 16,
                    QtdeDias = 30,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955361,
                    Referencia = "01/2020",
                    Consumo = 17,
                    QtdeDias = 33,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955360,
                    Referencia = "12/2019",
                    Consumo = 16,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955359,
                    Referencia = "11/2019",
                    Consumo = 16,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955358,
                    Referencia = "10/2019",
                    Consumo = 19,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955357,
                    Referencia = "09/2019",
                    Consumo = 19,
                    QtdeDias = 30,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955356,
                    Referencia = "08/2019",
                    Consumo = 18,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955355,
                    Referencia = "07/2019",
                    Consumo = 17,
                    QtdeDias = 30,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955354,
                    Referencia = "06/2019",
                    Consumo = 13,
                    QtdeDias = 30,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                },
                new TempHistoricoConsumo
                {
                    Id = 2955353,
                    Referencia = "05/2019",
                    Consumo = 14,
                    QtdeDias = 31,
                    AnoMes = "05/2020",
                    Matricula = "0100000004",
                    IdUsuario = 1969929945
                }
            };
        }

        #endregion
    }
}