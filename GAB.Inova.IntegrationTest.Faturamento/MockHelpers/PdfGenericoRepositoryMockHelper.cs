﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Mock para o repositório do PDF generico
    /// </summary>
    /// <returns><see cref="IPdfGenericoRepository"/></returns>
    public static class PdfGenericoRepositoryMockHelper
    {
        public static IPdfGenericoRepository GetPdfGenericoRepository()
        {
            var pdfGenModeloDoc = GeraPdfGenModeloDoc();

            #region [Listas dos itens do modelo]

            var listaPdfGenItemDoc182 = GeraListaPdfGenItemDoc182();
            var listaPdfGenItemDoc183 = GeraListaPdfGenItemDoc183();
            var listaPdfGenItemDoc184 = GeraListaPdfGenItemDoc184();
            var listaPdfGenItemDoc185 = GeraListaPdfGenItemDoc185();
            var listaPdfGenItemDoc186 = GeraListaPdfGenItemDoc186();
            var listaPdfGenItemDoc187 = GeraListaPdfGenItemDoc187();
            var listaPdfGenItemDoc188 = GeraListaPdfGenItemDoc188();
            var listaPdfGenItemDoc189 = GeraListaPdfGenItemDoc189();
            var listaPdfGenItemDoc190 = GeraListaPdfGenItemDoc190();
            var listaPdfGenItemDoc191 = GeraListaPdfGenItemDoc191();
            var listaPdfGenItemDoc192 = GeraListaPdfGenItemDoc192();
            var listaPdfGenItemDoc193 = GeraListaPdfGenItemDoc193();
            var listaPdfGenItemDoc194 = GeraListaPdfGenItemDoc194();
            var listaPdfGenItemDoc195 = GeraListaPdfGenItemDoc195();
            var listaPdfGenItemDoc196 = GeraListaPdfGenItemDoc196();
            var listaPdfGenItemDoc197 = GeraListaPdfGenItemDoc197();
            var listaPdfGenItemDoc198 = GeraListaPdfGenItemDoc198();
            var listaPdfGenItemDoc199 = GeraListaPdfGenItemDoc199();
            var listaPdfGenItemDoc200 = GeraListaPdfGenItemDoc200();
            var listaPdfGenItemDoc201 = GeraListaPdfGenItemDoc201();
            var listaPdfGenItemDoc202 = GeraListaPdfGenItemDoc202();
            var listaPdfGenItemDoc203 = GeraListaPdfGenItemDoc203();
            var listaPdfGenItemDoc204 = GeraListaPdfGenItemDoc204();
            var listaPdfGenItemDoc205 = GeraListaPdfGenItemDoc205();
            var listaPdfGenItemDoc206 = GeraListaPdfGenItemDoc206();
            var listaPdfGenItemDoc207 = GeraListaPdfGenItemDoc207();
            var listaPdfGenItemDoc208 = GeraListaPdfGenItemDoc208();
            var listaPdfGenItemDoc209 = GeraListaPdfGenItemDoc209();
            var listaPdfGenItemDoc210 = GeraListaPdfGenItemDoc210();
            var listaPdfGenItemDoc211 = GeraListaPdfGenItemDoc211();
            var listaPdfGenItemDoc212 = GeraListaPdfGenItemDoc212();
            var listaPdfGenItemDoc213 = GeraListaPdfGenItemDoc213();
            var listaPdfGenItemDoc214 = GeraListaPdfGenItemDoc214();
            var listaPdfGenItemDoc215 = GeraListaPdfGenItemDoc215();
            var listaPdfGenItemDoc216 = GeraListaPdfGenItemDoc216();
            var listaPdfGenItemDoc217 = GeraListaPdfGenItemDoc217();
            var listaPdfGenItemDoc218 = GeraListaPdfGenItemDoc218();
            var listaPdfGenItemDoc219 = GeraListaPdfGenItemDoc219();
            var listaPdfGenItemDoc220 = GeraListaPdfGenItemDoc220();
            var listaPdfGenItemDoc221 = GeraListaPdfGenItemDoc221();
            var listaPdfGenItemDoc222 = GeraListaPdfGenItemDoc222();
            var listaPdfGenItemDoc223 = GeraListaPdfGenItemDoc223();
            var listaPdfGenItemDoc224 = GeraListaPdfGenItemDoc224();
            var listaPdfGenItemDoc225 = GeraListaPdfGenItemDoc225();
            var listaPdfGenItemDoc226 = GeraListaPdfGenItemDoc226();
            var listaPdfGenItemDoc227 = GeraListaPdfGenItemDoc227();
            var listaPdfGenItemDoc228 = GeraListaPdfGenItemDoc228();
            var listaPdfGenItemDoc232 = GeraListaPdfGenItemDoc232();
            var listaPdfGenItemDoc233 = GeraListaPdfGenItemDoc233();
            var listaPdfGenItemDoc234 = GeraListaPdfGenItemDoc234();
            var listaPdfGenItemDoc235 = GeraListaPdfGenItemDoc235();
            var listaPdfGenItemDoc236 = GeraListaPdfGenItemDoc236();
            var listaPdfGenItemDoc237 = GeraListaPdfGenItemDoc237();
            var listaPdfGenItemDoc238 = GeraListaPdfGenItemDoc238();
            var listaPdfGenItemDoc239 = GeraListaPdfGenItemDoc239();
            var listaPdfGenItemDoc240 = GeraListaPdfGenItemDoc240();
            var listaPdfGenItemDoc241 = GeraListaPdfGenItemDoc241();
            var listaPdfGenItemDoc242 = GeraListaPdfGenItemDoc242();
            var listaPdfGenItemDoc243 = GeraListaPdfGenItemDoc243();
            var listaPdfGenItemDoc244 = GeraListaPdfGenItemDoc244();
            var listaPdfGenItemDoc245 = GeraListaPdfGenItemDoc245();
            var listaPdfGenItemDoc246 = GeraListaPdfGenItemDoc246();
            var listaPdfGenItemDoc247 = GeraListaPdfGenItemDoc247();
            var listaPdfGenItemDoc248 = GeraListaPdfGenItemDoc248();
            var listaPdfGenItemDoc249 = GeraListaPdfGenItemDoc249();
            var listaPdfGenItemDoc250 = GeraListaPdfGenItemDoc250();
            var listaPdfGenItemDoc251 = GeraListaPdfGenItemDoc251();
            var listaPdfGenItemDoc252 = GeraListaPdfGenItemDoc252();
            var listaPdfGenItemDoc253 = GeraListaPdfGenItemDoc253();
            var listaPdfGenItemDoc254 = GeraListaPdfGenItemDoc254();
            var listaPdfGenItemDoc255 = GeraListaPdfGenItemDoc255();
            var listaPdfGenItemDoc256 = GeraListaPdfGenItemDoc256();
            var listaPdfGenItemDoc257 = GeraListaPdfGenItemDoc257();
            var listaPdfGenItemDoc258 = GeraListaPdfGenItemDoc258();
            var listaPdfGenItemDoc259 = GeraListaPdfGenItemDoc259();
            var listaPdfGenItemDoc260 = GeraListaPdfGenItemDoc260();
            var listaPdfGenItemDoc261 = GeraListaPdfGenItemDoc261();
            var listaPdfGenItemDoc262 = GeraListaPdfGenItemDoc262();

            #endregion

            var mockPdfGenericoRepository = new Mock<IPdfGenericoRepository>();

            mockPdfGenericoRepository.Setup(m => m.ObterDocumentoLayoutAsync(It.IsAny<PdfModeloEnum>())).ReturnsAsync(pdfGenModeloDoc);

            #region [Mock para retornar as listas]

            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(182)).ReturnsAsync(listaPdfGenItemDoc182);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(183)).ReturnsAsync(listaPdfGenItemDoc183);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(184)).ReturnsAsync(listaPdfGenItemDoc184);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(185)).ReturnsAsync(listaPdfGenItemDoc185);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(186)).ReturnsAsync(listaPdfGenItemDoc186);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(187)).ReturnsAsync(listaPdfGenItemDoc187);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(188)).ReturnsAsync(listaPdfGenItemDoc188);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(189)).ReturnsAsync(listaPdfGenItemDoc189);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(190)).ReturnsAsync(listaPdfGenItemDoc190);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(191)).ReturnsAsync(listaPdfGenItemDoc191);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(192)).ReturnsAsync(listaPdfGenItemDoc192);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(193)).ReturnsAsync(listaPdfGenItemDoc193);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(194)).ReturnsAsync(listaPdfGenItemDoc194);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(195)).ReturnsAsync(listaPdfGenItemDoc195);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(196)).ReturnsAsync(listaPdfGenItemDoc196);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(197)).ReturnsAsync(listaPdfGenItemDoc197);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(198)).ReturnsAsync(listaPdfGenItemDoc198);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(199)).ReturnsAsync(listaPdfGenItemDoc199);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(200)).ReturnsAsync(listaPdfGenItemDoc200);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(201)).ReturnsAsync(listaPdfGenItemDoc201);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(202)).ReturnsAsync(listaPdfGenItemDoc202);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(203)).ReturnsAsync(listaPdfGenItemDoc203);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(204)).ReturnsAsync(listaPdfGenItemDoc204);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(205)).ReturnsAsync(listaPdfGenItemDoc205);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(206)).ReturnsAsync(listaPdfGenItemDoc206);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(207)).ReturnsAsync(listaPdfGenItemDoc207);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(208)).ReturnsAsync(listaPdfGenItemDoc208);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(209)).ReturnsAsync(listaPdfGenItemDoc209);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(210)).ReturnsAsync(listaPdfGenItemDoc210);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(211)).ReturnsAsync(listaPdfGenItemDoc211);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(212)).ReturnsAsync(listaPdfGenItemDoc212);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(213)).ReturnsAsync(listaPdfGenItemDoc213);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(214)).ReturnsAsync(listaPdfGenItemDoc214);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(215)).ReturnsAsync(listaPdfGenItemDoc215);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(216)).ReturnsAsync(listaPdfGenItemDoc216);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(217)).ReturnsAsync(listaPdfGenItemDoc217);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(218)).ReturnsAsync(listaPdfGenItemDoc218);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(219)).ReturnsAsync(listaPdfGenItemDoc219);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(220)).ReturnsAsync(listaPdfGenItemDoc220);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(221)).ReturnsAsync(listaPdfGenItemDoc221);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(222)).ReturnsAsync(listaPdfGenItemDoc222);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(223)).ReturnsAsync(listaPdfGenItemDoc223);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(224)).ReturnsAsync(listaPdfGenItemDoc224);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(225)).ReturnsAsync(listaPdfGenItemDoc225);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(226)).ReturnsAsync(listaPdfGenItemDoc226);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(227)).ReturnsAsync(listaPdfGenItemDoc227);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(228)).ReturnsAsync(listaPdfGenItemDoc228);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(232)).ReturnsAsync(listaPdfGenItemDoc232);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(233)).ReturnsAsync(listaPdfGenItemDoc233);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(234)).ReturnsAsync(listaPdfGenItemDoc234);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(235)).ReturnsAsync(listaPdfGenItemDoc235);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(236)).ReturnsAsync(listaPdfGenItemDoc236);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(237)).ReturnsAsync(listaPdfGenItemDoc237);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(238)).ReturnsAsync(listaPdfGenItemDoc238);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(239)).ReturnsAsync(listaPdfGenItemDoc239);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(240)).ReturnsAsync(listaPdfGenItemDoc240);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(241)).ReturnsAsync(listaPdfGenItemDoc241);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(242)).ReturnsAsync(listaPdfGenItemDoc242);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(243)).ReturnsAsync(listaPdfGenItemDoc243);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(244)).ReturnsAsync(listaPdfGenItemDoc244);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(245)).ReturnsAsync(listaPdfGenItemDoc245);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(246)).ReturnsAsync(listaPdfGenItemDoc246);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(247)).ReturnsAsync(listaPdfGenItemDoc247);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(248)).ReturnsAsync(listaPdfGenItemDoc248);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(249)).ReturnsAsync(listaPdfGenItemDoc249);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(250)).ReturnsAsync(listaPdfGenItemDoc250);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(251)).ReturnsAsync(listaPdfGenItemDoc251);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(252)).ReturnsAsync(listaPdfGenItemDoc252);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(253)).ReturnsAsync(listaPdfGenItemDoc253);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(254)).ReturnsAsync(listaPdfGenItemDoc254);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(255)).ReturnsAsync(listaPdfGenItemDoc255);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(256)).ReturnsAsync(listaPdfGenItemDoc256);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(257)).ReturnsAsync(listaPdfGenItemDoc257);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(258)).ReturnsAsync(listaPdfGenItemDoc258);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(259)).ReturnsAsync(listaPdfGenItemDoc259);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(260)).ReturnsAsync(listaPdfGenItemDoc260);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(261)).ReturnsAsync(listaPdfGenItemDoc261);
            mockPdfGenericoRepository.Setup(m => m.ObterItensDocumentoLayoutAsync(262)).ReturnsAsync(listaPdfGenItemDoc262);

            #endregion

            return mockPdfGenericoRepository.Object;
        }

        #region [Metodos privados]

        private static PdfGenModeloDoc GeraPdfGenModeloDoc()
        {
            return new PdfGenModeloDoc
            {
                Id = 3,
                Nome = "CONTA - SEGUNDA VIA - AGÊNCIA VIRTUAL",
                Descricao = "Modelo usado para imprimir segunda via de conta no formato A4. Uma conta por página.",
                IdTipoDocumento = 1,
                TamanhoPagina = 0,
                OrientacaoPagina = 1,
                AlturaPagina = 297,
                LarguraPagina = 210,
                ItensPorPagina = 1,
                OrientacaoRepeticoes = 0,
                OffsetRepeticao = 0,
                IdItemDocumento = 182
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc182()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                {
                    Id = 182,
                    Nome = "CONTA_SEGUNDA_VIA_A4",
                    IdentacaoNome = "CONTA_SEGUNDA_VIA_A4",
                    Level = 1,
                    Descricao = "Layout padrão para segunda via A4.",
                    IdTipoDocumento = 1,
                    TipoItemDocumento = 0,
                    IdItemDocumentoParent = null,
                    Ordem = 0,
                    SomentePreview = 0,
                    ValorPreview = null,
                    EstiloPosicao = 1,
                    EstiloPosicaoEsquerda = 0,
                    EstiloPosicaoTopo = 0,
                    EstiloAltura = 340,
                    EstiloLargura = 100,
                    EstiloMargemEsquerda = 0,
                    EstiloMargemTopo = 0,
                    EstiloFonteNome = 0,
                    EstiloFonteTamanho = 7,
                    EstiloFonteFormato = 0,
                    EstiloFonteColor = "Black",
                    EstiloSombra = 0,
                    EstiloAlinhamentoVertical = 4,
                    EstiloAlinhamentoHorizontal = 0,
                    EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 183,
                    Nome = "BACKGROUND",
                    IdentacaoNome = "BACKGROUND",
                    Level = 2,
                    Descricao = "Espelho",
                    IdTipoDocumento = 1,
                    TipoItemDocumento = 5,
                    IdItemDocumentoParent = 182,
                    Ordem = 1,
                    SomentePreview = 0,
                    ValorPreview = "imagem/Contas/Bg2aViaContaInterna01CAA.jpg",
                    EstiloPosicao = 1,
                    EstiloPosicaoEsquerda = 0,
                    EstiloPosicaoTopo = 0,
                    EstiloAltura = 290,
                    EstiloLargura = 210,
                    EstiloMargemEsquerda = 0,
                    EstiloMargemTopo = 0,
                    EstiloFonteNome = 0,
                    EstiloFonteTamanho = null,
                    EstiloFonteFormato = 0,
                    EstiloFonteColor = null,
                    EstiloSombra = 0,
                    EstiloAlinhamentoVertical = 4,
                    EstiloAlinhamentoHorizontal = 0,
                    EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 184,
                    Nome = "IDENTIFICACAO_CONTA",
                    IdentacaoNome = "IDENTIFICACAO_CONTA",
                    Level = 2,
                    Descricao = "Bloco 1 - Identificação da conta. Composto pelo N# da ligação, Roteirização, N# Conta, Referência, ... , Categoria/N# de economias.z ",
                    IdTipoDocumento = 1,
                    TipoItemDocumento = 0,
                    IdItemDocumentoParent = 182,
                    Ordem = 2,
                    SomentePreview = 0,
                    ValorPreview = null,
                    EstiloPosicao = 1,
                    EstiloPosicaoEsquerda = 0,
                    EstiloPosicaoTopo = 0,
                    EstiloAltura = 63,
                    EstiloLargura = 100,
                    EstiloMargemEsquerda = 0,
                    EstiloMargemTopo = 0,
                    EstiloFonteNome = 0,
                    EstiloFonteTamanho = 7,
                    EstiloFonteFormato = 0,
                    EstiloFonteColor = null,
                    EstiloSombra = 0,
                    EstiloAlinhamentoVertical = 5,
                    EstiloAlinhamentoHorizontal = 1,
                    EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 208, Nome = "DADOS_CONSUMO", IdentacaoNome = "DADOS_CONSUMO", Level = 2, Descricao = "Histórico do consumo / Informações sobre o faturamento / Faixa de Consumo", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 3, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 6, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 226, Nome = "DISCRIMINACAO_FATURAMENTO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO", Level = 2, Descricao = "Discriminação do Faturamento.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 4, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 5, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 235, Nome = "ESPACO_MENSAGEM", IdentacaoNome = "ESPACO_MENSAGEM", Level = 2, Descricao = "Espaço para mensagens e Identificador de débito automático.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 5, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 148, EstiloAltura = 33, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 5, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 238, Nome = "INFORMACOES_QUALIDADE_AGUA", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA", Level = 2, Descricao = "Informações sobre a qualidade da água.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 6, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 182, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 5, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 248, Nome = "VIA_BANCO.IDENTIFICACAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO", Level = 2, Descricao = "Via do banco.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 7, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 208, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 5, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 255, Nome = "CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS", Level = 2, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 8, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 182, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 5, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                },
                new PdfGenItemDoc
                {
                    Id = 259, Nome = "AVISO_DEBITO", IdentacaoNome = "AVISO_DEBITO", Level = 2, Descricao = "Bloco 7 - Aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 9, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 257, EstiloAltura = 83, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = null, EstiloFonteTamanho = 6, EstiloFonteFormato = null, EstiloFonteColor = null, EstiloSombra = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc183()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                {
                    Id = 183, Nome = "BACKGROUND", IdentacaoNome = "BACKGROUND", Level = 1, Descricao = "Espelho", IdTipoDocumento = 1, TipoItemDocumento = 5, IdItemDocumentoParent = 182, Ordem = 1, SomentePreview = 0, ValorPreview = "imagem/Contas/Bg2aViaContaInterna01CAA.jpg", EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 0, EstiloAltura = 290, EstiloLargura = 210, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc184()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 184, Nome = "IDENTIFICACAO_CONTA", IdentacaoNome = "IDENTIFICACAO_CONTA", Level = 1, Descricao = "Bloco 1 - Identificação da conta. Composto pelo N# da ligação, Roteirização, N# Conta, Referência, ... , Categoria/N# de economias.z ", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 2, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 0, EstiloAltura = 63, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 185, Nome = "IDENTIFICACAO_CONTA.NUMERO_LIGACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.NUMERO_LIGACAO", Level = 2, Descricao = "Número da ligação na conta. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 1, SomentePreview = 0, ValorPreview = "11000000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.746, EstiloPosicaoTopo = (float)10.692, EstiloAltura = (float)5.579, EstiloLargura = (float)27.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 9, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 186, Nome = "IDENTIFICACAO_CONTA.ROTEIRIZACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.ROTEIRIZACAO", Level = 2, Descricao = "Roteirização. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 2, SomentePreview = 0, ValorPreview = "18.005.1.1.40", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)71.6, EstiloPosicaoTopo = (float)16.4, EstiloAltura = (float)3.681, EstiloLargura = (float)27.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 187, Nome = "IDENTIFICACAO_CONTA.CONTA_NUMERO", IdentacaoNome = "IDENTIFICACAO_CONTA.CONTA_NUMERO", Level = 2, Descricao = "Número da Conta.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 3, SomentePreview = 0, ValorPreview = "17204457", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.144, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)27.441, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 188, Nome = "IDENTIFICACAO_CONTA.REFERENCIA", IdentacaoNome = "IDENTIFICACAO_CONTA.REFERENCIA", Level = 2, Descricao = "Referência.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 4, SomentePreview = 0, ValorPreview = "03/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)30.238, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)17.881, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 189, Nome = "IDENTIFICACAO_CONTA.DATA_EMISSAO", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_EMISSAO", Level = 2, Descricao = "Data Emissão.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 5, SomentePreview = 0, ValorPreview = "03/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.677, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)22.237, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 190, Nome = "IDENTIFICACAO_CONTA.DATA_VENCIMENTO", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_VENCIMENTO", Level = 2, Descricao = "Vencimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 6, SomentePreview = 0, ValorPreview = "18/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)71.556, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)22.056, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 191, Nome = "IDENTIFICACAO_CONTA.VIA", IdentacaoNome = "IDENTIFICACAO_CONTA.VIA", Level = 2, Descricao = "Via.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 7, SomentePreview = 0, ValorPreview = "2ª", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)95.5, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)6.163, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 192, Nome = "IDENTIFICACAO_CONTA.NOME_RAZAO_SOCIAL", IdentacaoNome = "IDENTIFICACAO_CONTA.NOME_RAZAO_SOCIAL", Level = 2, Descricao = "Nome / Razão Social.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 8, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = (float)29.876, EstiloAltura = (float)4.82, EstiloLargura = (float)94.937, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 193, Nome = "IDENTIFICACAO_CONTA.ENDERECO_LIGACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.ENDERECO_LIGACAO", Level = 2, Descricao = "Endereço Ligação.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 9, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR, LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR SIT AMET.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = 38, EstiloAltura = (float)3.7, EstiloLargura = (float)94.937, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 194, Nome = "IDENTIFICACAO_CONTA.COMPLEMENTO", IdentacaoNome = "IDENTIFICACAO_CONTA.COMPLEMENTO", Level = 2, Descricao = "Complemento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 10, SomentePreview = 0, ValorPreview = "LOREM 999 IPSUM 999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = (float)45.5, EstiloAltura = (float)4.82, EstiloLargura = (float)66.252, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 195, Nome = "IDENTIFICACAO_CONTA.NUM_HIDROMETRO", IdentacaoNome = "IDENTIFICACAO_CONTA.NUM_HIDROMETRO", Level = 2, Descricao = "Nº Hidrômetro.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 11, SomentePreview = 0, ValorPreview = "AA160G00000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.98, EstiloPosicaoTopo = (float)45.5, EstiloAltura = (float)4.82, EstiloLargura = (float)28.071, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 196, Nome = "IDENTIFICACAO_CONTA.LEITURA_ANTERIOR", IdentacaoNome = "IDENTIFICACAO_CONTA.LEITURA_ANTERIOR", Level = 2, Descricao = "Leitura Anterior.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 12, SomentePreview = 0, ValorPreview = "9999999999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 2, EstiloPosicaoTopo = 54, EstiloAltura = (float)3.6, EstiloLargura = 15, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 197, Nome = "IDENTIFICACAO_CONTA.LEITURA_ATUAL", IdentacaoNome = "IDENTIFICACAO_CONTA.LEITURA_ATUAL", Level = 2, Descricao = "Leitura Atual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 13, SomentePreview = 0, ValorPreview = "9999999999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)18.2, EstiloPosicaoTopo = 54, EstiloAltura = (float)3.6, EstiloLargura = 15, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 198, Nome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ANT", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ANT", Level = 2, Descricao = "Data da leitura anterior.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 14, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 33, EstiloPosicaoTopo = (float)53.7, EstiloAltura = (float)3.6, EstiloLargura = (float)14.233, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 199, Nome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ATUAL", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ATUAL", Level = 2, Descricao = "Data da leitura atual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 15, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)44.5, EstiloPosicaoTopo = (float)53.7, EstiloAltura = (float)3.6, EstiloLargura = (float)14.233, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 200, Nome = "IDENTIFICACAO_CONTA.PREV_PROX_LEITURA", IdentacaoNome = "IDENTIFICACAO_CONTA.PREV_PROX_LEITURA", Level = 2, Descricao = "Data Prevista da próxima leitura.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 16, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 58, EstiloPosicaoTopo = 53, EstiloAltura = (float)4.82, EstiloLargura = (float)18.672, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 201, Nome = "IDENTIFICACAO_CONTA.TIPO_ENTREGA", IdentacaoNome = "IDENTIFICACAO_CONTA.TIPO_ENTREGA", Level = 2, Descricao = "Tipo de entrega.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 17, SomentePreview = 0, ValorPreview = "LOREM IPSUM", EstiloPosicao = 1, EstiloPosicaoEsquerda = 78, EstiloPosicaoTopo = 53, EstiloAltura = (float)4.82, EstiloLargura = (float)23.918, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 202, Nome = "IDENTIFICACAO_CONTA.CPF_CNPJ", IdentacaoNome = "IDENTIFICACAO_CONTA.CPF_CNPJ", Level = 2, Descricao = "CPF/CNPJ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 18, SomentePreview = 0, ValorPreview = "999.999.999-99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.193, EstiloPosicaoTopo = 60, EstiloAltura = (float)4.82, EstiloLargura = (float)31.788, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 203, Nome = "IDENTIFICACAO_CONTA.INSCRICAO_ESTADUAL_MUNICIPAL", IdentacaoNome = "IDENTIFICACAO_CONTA.INSCRICAO_ESTADUAL_MUNICIPAL", Level = 2, Descricao = "Inscricao Estadual / Municipal.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 19, SomentePreview = 0, ValorPreview = "9999.999.999-99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)33.299, EstiloPosicaoTopo = 60, EstiloAltura = (float)4.82, EstiloLargura = (float)26.768, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 204, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.RES", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.RES", Level = 2, Descricao = "Categoria / nº economias - Residêncial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 20, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)65.9, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 205, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.COM", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.COM", Level = 2, Descricao = "Categoria / nº economias - Comercial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 21, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.618, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 206, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.IND", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.IND", Level = 2, Descricao = "Categoria / nº economias - Industrial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 22, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.536, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 207, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.PUB", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.PUB", Level = 2, Descricao = "Categoria / nº economias - Público.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 23, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)89.5, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc185()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 185, Nome = "IDENTIFICACAO_CONTA.NUMERO_LIGACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.NUMERO_LIGACAO", Level = 1, Descricao = "Número da ligação na conta. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 1, SomentePreview = 0, ValorPreview = "11000000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.746, EstiloPosicaoTopo = (float)10.692, EstiloAltura = (float)5.579, EstiloLargura = (float)27.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 9, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc186()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 186, Nome = "IDENTIFICACAO_CONTA.ROTEIRIZACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.ROTEIRIZACAO", Level = 1, Descricao = "Roteirização. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 2, SomentePreview = 0, ValorPreview = "18.005.1.1.40", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)71.6, EstiloPosicaoTopo = (float)16.4, EstiloAltura = (float)3.681, EstiloLargura = (float)27.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc187()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 187, Nome = "IDENTIFICACAO_CONTA.CONTA_NUMERO", IdentacaoNome = "IDENTIFICACAO_CONTA.CONTA_NUMERO", Level = 1, Descricao = "Número da Conta.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 3, SomentePreview = 0, ValorPreview = "17204457", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.144, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)27.441, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc188()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 188, Nome = "IDENTIFICACAO_CONTA.REFERENCIA", IdentacaoNome = "IDENTIFICACAO_CONTA.REFERENCIA", Level = 1, Descricao = "Referência.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 4, SomentePreview = 0, ValorPreview = "03/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)30.238, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)17.881, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc189()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 189, Nome = "IDENTIFICACAO_CONTA.DATA_EMISSAO", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_EMISSAO", Level = 1, Descricao = "Data Emissão.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 5, SomentePreview = 0, ValorPreview = "03/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.677, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)22.237, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc190()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 190, Nome = "IDENTIFICACAO_CONTA.DATA_VENCIMENTO", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_VENCIMENTO", Level = 1, Descricao = "Vencimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 6, SomentePreview = 0, ValorPreview = "18/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)71.556, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)22.056, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc191()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 191, Nome = "IDENTIFICACAO_CONTA.VIA", IdentacaoNome = "IDENTIFICACAO_CONTA.VIA", Level = 1, Descricao = "Via.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 7, SomentePreview = 0, ValorPreview = "2ª", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)95.5, EstiloPosicaoTopo = (float)22.423, EstiloAltura = (float)4.52, EstiloLargura = (float)6.163, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc192()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 192, Nome = "IDENTIFICACAO_CONTA.NOME_RAZAO_SOCIAL", IdentacaoNome = "IDENTIFICACAO_CONTA.NOME_RAZAO_SOCIAL", Level = 1, Descricao = "Nome / Razão Social.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 8, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = (float)29.876, EstiloAltura = (float)4.82, EstiloLargura = (float)94.937, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc193()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 193, Nome = "IDENTIFICACAO_CONTA.ENDERECO_LIGACAO", IdentacaoNome = "IDENTIFICACAO_CONTA.ENDERECO_LIGACAO", Level = 1, Descricao = "Endereço Ligação.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 9, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR, LOREM IPSUM DOLOR SIT AMET, LOREM IPSUM DOLOR SIT AMET.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = 38, EstiloAltura = (float)3.7, EstiloLargura = (float)94.937, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc194()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 194, Nome = "IDENTIFICACAO_CONTA.COMPLEMENTO", IdentacaoNome = "IDENTIFICACAO_CONTA.COMPLEMENTO", Level = 1, Descricao = "Complemento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 10, SomentePreview = 0, ValorPreview = "LOREM 999 IPSUM 999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)2.853, EstiloPosicaoTopo = (float)45.5, EstiloAltura = (float)4.82, EstiloLargura = (float)66.252, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc195()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 195, Nome = "IDENTIFICACAO_CONTA.NUM_HIDROMETRO", IdentacaoNome = "IDENTIFICACAO_CONTA.NUM_HIDROMETRO", Level = 1, Descricao = "Nº Hidrômetro.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 11, SomentePreview = 0, ValorPreview = "AA160G00000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.98, EstiloPosicaoTopo = (float)45.5, EstiloAltura = (float)4.82, EstiloLargura = (float)28.071, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc196()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 196, Nome = "IDENTIFICACAO_CONTA.LEITURA_ANTERIOR", IdentacaoNome = "IDENTIFICACAO_CONTA.LEITURA_ANTERIOR", Level = 1, Descricao = "Leitura Anterior.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 12, SomentePreview = 0, ValorPreview = "9999999999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 2, EstiloPosicaoTopo = 54, EstiloAltura = (float)3.6, EstiloLargura = 15, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc197()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 197, Nome = "IDENTIFICACAO_CONTA.LEITURA_ATUAL", IdentacaoNome = "IDENTIFICACAO_CONTA.LEITURA_ATUAL", Level = 1, Descricao = "Leitura Atual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 13, SomentePreview = 0, ValorPreview = "9999999999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)18.2, EstiloPosicaoTopo = 54, EstiloAltura = (float)3.6, EstiloLargura = 15, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc198()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 198, Nome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ANT", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ANT", Level = 1, Descricao = "Data da leitura anterior.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 14, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 33, EstiloPosicaoTopo = (float)53.7, EstiloAltura = (float)3.6, EstiloLargura = (float)14.233, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc199()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 199, Nome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ATUAL", IdentacaoNome = "IDENTIFICACAO_CONTA.DATA_LEITURA_ATUAL", Level = 1, Descricao = "Data da leitura atual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 15, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)44.5, EstiloPosicaoTopo = (float)53.7, EstiloAltura = (float)3.6, EstiloLargura = (float)14.233, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc200()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 200, Nome = "IDENTIFICACAO_CONTA.PREV_PROX_LEITURA", IdentacaoNome = "IDENTIFICACAO_CONTA.PREV_PROX_LEITURA", Level = 1, Descricao = "Data Prevista da próxima leitura.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 16, SomentePreview = 0, ValorPreview = "99/99/9999", EstiloPosicao = 1, EstiloPosicaoEsquerda = 58, EstiloPosicaoTopo = 53, EstiloAltura = (float)4.82, EstiloLargura = (float)18.672, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc201()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 201, Nome = "IDENTIFICACAO_CONTA.TIPO_ENTREGA", IdentacaoNome = "IDENTIFICACAO_CONTA.TIPO_ENTREGA", Level = 1, Descricao = "Tipo de entrega.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 17, SomentePreview = 0, ValorPreview = "LOREM IPSUM", EstiloPosicao = 1, EstiloPosicaoEsquerda = 78, EstiloPosicaoTopo = 53, EstiloAltura = (float)4.82, EstiloLargura = (float)23.918, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc202()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 202, Nome = "IDENTIFICACAO_CONTA.CPF_CNPJ", IdentacaoNome = "IDENTIFICACAO_CONTA.CPF_CNPJ", Level = 1, Descricao = "CPF/CNPJ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 18, SomentePreview = 0, ValorPreview = "999.999.999-99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.193, EstiloPosicaoTopo = 60, EstiloAltura = (float)4.82, EstiloLargura = (float)31.788, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc203()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 203, Nome = "IDENTIFICACAO_CONTA.INSCRICAO_ESTADUAL_MUNICIPAL", IdentacaoNome = "IDENTIFICACAO_CONTA.INSCRICAO_ESTADUAL_MUNICIPAL", Level = 1, Descricao = "Inscricao Estadual / Municipal.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 19, SomentePreview = 0, ValorPreview = "9999.999.999-99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)33.299, EstiloPosicaoTopo = 60, EstiloAltura = (float)4.82, EstiloLargura = (float)26.768, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc204()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 204, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.RES", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.RES", Level = 1, Descricao = "Categoria / nº economias - Residêncial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 20, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)65.9, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc205()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 205, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.COM", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.COM", Level = 1, Descricao = "Categoria / nº economias - Comercial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 21, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.618, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc206()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 206, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.IND", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.IND", Level = 1, Descricao = "Categoria / nº economias - Industrial.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 22, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.536, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc207()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 207, Nome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.PUB", IdentacaoNome = "IDENTIFICACAO_CONTA.CATEGORIA_NUMERO_ECONOMIAS.PUB", Level = 1, Descricao = "Categoria / nº economias - Público.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 184, Ordem = 23, SomentePreview = 0, ValorPreview = "999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)89.5, EstiloPosicaoTopo = 61, EstiloAltura = (float)3.6, EstiloLargura = (float)7.718, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc208()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 208, Nome = "DADOS_CONSUMO", IdentacaoNome = "DADOS_CONSUMO", Level = 1, Descricao = "Histórico do consumo / Informações sobre o faturamento / Faixa de Consumo", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 3, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 209, Nome = "DADOS_CONSUMO.HISTORICO_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.HISTORICO_CONSUMO", Level = 2, Descricao = "Histórico de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 1, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 213, Nome = "DADOS_CONSUMO.INFORMACOES_FATURAMENTO_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.INFORMACOES_FATURAMENTO_CONSUMO", Level = 2, Descricao = "Informacoes sobre o faturamento do consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 2, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 28, EstiloPosicaoTopo = 63, EstiloAltura = 12, EstiloLargura = 72, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 221, Nome = "DADOS_CONSUMO.FAIXA_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.FAIXA_CONSUMO", Level = 2, Descricao = "Dados da faixa de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 3, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 28, EstiloPosicaoTopo = 75, EstiloAltura = 40, EstiloLargura = 72, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc209()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 209, Nome = "DADOS_CONSUMO.HISTORICO_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.HISTORICO_CONSUMO", Level = 1, Descricao = "Histórico de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 1, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 210, Nome = "HISTORICO_CONSUMO.REFERENCIA", IdentacaoNome = "HISTORICO_CONSUMO.REFERENCIA", Level = 2, Descricao = "Representa uma linha de referência em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 1, SomentePreview = 0, ValorPreview = "01/2017;02/2017;03/2017;04/2017;05/2017;06/2017;07/2017;08/2017;09/2017;10/2017;11/2017;12/2017;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.7, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)9.1, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 211, Nome = "HISTORICO_CONSUMO.CONSUMO", IdentacaoNome = "HISTORICO_CONSUMO.CONSUMO", Level = 2, Descricao = "Representa uma linha de consumo em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 2, SomentePreview = 0, ValorPreview = "99999;99999;99999;99999;99999;99999;99999;99999;99999;99999;99999;9999999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)12.674, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)9.1, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 212, Nome = "HISTORICO_CONSUMO.DIAS", IdentacaoNome = "HISTORICO_CONSUMO.DIAS", Level = 2, Descricao = "Representa uma linha de dia em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 3, SomentePreview = 0, ValorPreview = "30;30;30;30;30;30;30;30;30;30;30;30;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.3, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)5.4, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc210()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 210, Nome = "HISTORICO_CONSUMO.REFERENCIA", IdentacaoNome = "HISTORICO_CONSUMO.REFERENCIA", Level = 1, Descricao = "Representa uma linha de referência em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 1, SomentePreview = 0, ValorPreview = "01/2017;02/2017;03/2017;04/2017;05/2017;06/2017;07/2017;08/2017;09/2017;10/2017;11/2017;12/2017;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)1.7, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)9.1, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc211()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 211, Nome = "HISTORICO_CONSUMO.CONSUMO", IdentacaoNome = "HISTORICO_CONSUMO.CONSUMO", Level = 1, Descricao = "Representa uma linha de consumo em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 2, SomentePreview = 0, ValorPreview = "99999;99999;99999;99999;99999;99999;99999;99999;99999;99999;99999;9999999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)12.674, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)9.1, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc212()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 212, Nome = "HISTORICO_CONSUMO.DIAS", IdentacaoNome = "HISTORICO_CONSUMO.DIAS", Level = 1, Descricao = "Representa uma linha de dia em histórico de consumo (máximo de 12 linhas).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 209, Ordem = 3, SomentePreview = 0, ValorPreview = "30;30;30;30;30;30;30;30;30;30;30;30;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.3, EstiloPosicaoTopo = (float)73.3, EstiloAltura = (float)3.079, EstiloLargura = (float)5.4, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc213()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 213, Nome = "DADOS_CONSUMO.INFORMACOES_FATURAMENTO_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.INFORMACOES_FATURAMENTO_CONSUMO", Level = 1, Descricao = "Informacoes sobre o faturamento do consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 2, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 28, EstiloPosicaoTopo = 63, EstiloAltura = 12, EstiloLargura = 72, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 214, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.DIAS_CONSUMO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.DIAS_CONSUMO", Level = 2, Descricao = "Dias de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 1, SomentePreview = 0, ValorPreview = "31", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)28.084, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)3.6, EstiloLargura = (float)7.686, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 215, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_MEDIDO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_MEDIDO", Level = 2, Descricao = "Consumo Medido.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 2, SomentePreview = 0, ValorPreview = "999.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)36.275, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)8.779, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 216, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CREDITO_CONSUMO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CREDITO_CONSUMO", Level = 2, Descricao = "Consumo Crédito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 3, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)45.004, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)7.775, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 217, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_PIPA", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_PIPA", Level = 2, Descricao = "Consumo Pipa.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 4, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)52.781, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)6.27, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 218, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_RESIDUAL", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_RESIDUAL", Level = 2, Descricao = "Consumo Residual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 5, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)59.179, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)5.816, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 219, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_FATURADO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_FATURADO", Level = 2, Descricao = "Consumo Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 6, SomentePreview = 0, ValorPreview = "99.999.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)68.4, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)3.53, EstiloLargura = (float)13.703, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 220, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.TIPO_FATURAMENTO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.TIPO_FATURAMENTO", Level = 2, Descricao = "Tipo Faturamento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 7, SomentePreview = 0, ValorPreview = "LOREM IPSUM", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.8, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)4.715, EstiloLargura = (float)19.269, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc214()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 214, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.DIAS_CONSUMO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.DIAS_CONSUMO", Level = 1, Descricao = "Dias de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 1, SomentePreview = 0, ValorPreview = "31", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)28.084, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)3.6, EstiloLargura = (float)7.686, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc215()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 215, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_MEDIDO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_MEDIDO", Level = 1, Descricao = "Consumo Medido.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 2, SomentePreview = 0, ValorPreview = "999.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)36.275, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)8.779, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc216()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 216, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CREDITO_CONSUMO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CREDITO_CONSUMO", Level = 1, Descricao = "Consumo Crédito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 3, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)45.004, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)7.775, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc217()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 217, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_PIPA", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_PIPA", Level = 1, Descricao = "Consumo Pipa.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 4, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)52.781, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)6.27, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc218()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 218, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_RESIDUAL", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_RESIDUAL", Level = 1, Descricao = "Consumo Residual.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 5, SomentePreview = 0, ValorPreview = "99.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)59.179, EstiloPosicaoTopo = (float)74.6, EstiloAltura = (float)3.6, EstiloLargura = (float)5.816, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc219()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 219, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_FATURADO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.CONSUMO_FATURADO", Level = 1, Descricao = "Consumo Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 6, SomentePreview = 0, ValorPreview = "99.999.999", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)68.4, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)3.53, EstiloLargura = (float)13.703, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc220()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 220, Nome = "INFORMACOES_FATURAMENTO_CONSUMO.TIPO_FATURAMENTO", IdentacaoNome = "INFORMACOES_FATURAMENTO_CONSUMO.TIPO_FATURAMENTO", Level = 1, Descricao = "Tipo Faturamento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 213, Ordem = 7, SomentePreview = 0, ValorPreview = "LOREM IPSUM", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.8, EstiloPosicaoTopo = (float)74.5, EstiloAltura = (float)4.715, EstiloLargura = (float)19.269, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc221()
        {
            return new List<PdfGenItemDoc>
            {new PdfGenItemDoc
                    {
                        Id = 221, Nome = "DADOS_CONSUMO.FAIXA_CONSUMO", IdentacaoNome = "DADOS_CONSUMO.FAIXA_CONSUMO", Level = 1, Descricao = "Dados da faixa de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 208, Ordem = 3, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 28, EstiloPosicaoTopo = 75, EstiloAltura = 40, EstiloLargura = 72, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 222, Nome = "FAIXA_CONSUMO.FAIXA_CONSUMO", IdentacaoNome = "FAIXA_CONSUMO.FAIXA_CONSUMO", Level = 2, Descricao = "Faixas de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 1, SomentePreview = 0, ValorPreview = "Pub 0 a 15;Pub 15 a 999;Pub 999 a 9.999;Pub 9.999 a 99.999;Pub 99.999 a 999.999;Pub 999.999 a 9.999.999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)29.579, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)21.394, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 223, Nome = "FAIXA_CONSUMO.CONSUMO_FATURADO", IdentacaoNome = "FAIXA_CONSUMO.CONSUMO_FATURADO", Level = 2, Descricao = "Consumo Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 2, SomentePreview = 0, ValorPreview = "15,00;999,00;9.999,00;99.999,00;999.999,00;9.999.999,00;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)52.008, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)13.076, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 224, Nome = "FAIXA_CONSUMO.TARIFA_AGUA", IdentacaoNome = "FAIXA_CONSUMO.TARIFA_AGUA", Level = 2, Descricao = "Tarifa de água.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 3, SomentePreview = 0, ValorPreview = "99,99;999,99;9.999,99;99.999,99;999.999,99;9.999.999,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)66.15, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)15.379, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 225, Nome = "FAIXA_CONSUMO.TARIFA_ESGOTO", IdentacaoNome = "FAIXA_CONSUMO.TARIFA_ESGOTO", Level = 2, Descricao = "Tarifa de esgoto.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 4, SomentePreview = 0, ValorPreview = "99,99;999,99;9.999,99;99.999,99;999.999,99;9.999.999,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.295, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)15.379, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc222()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 222, Nome = "FAIXA_CONSUMO.FAIXA_CONSUMO", IdentacaoNome = "FAIXA_CONSUMO.FAIXA_CONSUMO", Level = 1, Descricao = "Faixas de consumo.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 1, SomentePreview = 0, ValorPreview = "Pub 0 a 15;Pub 15 a 999;Pub 999 a 9.999;Pub 9.999 a 99.999;Pub 99.999 a 999.999;Pub 999.999 a 9.999.999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)29.579, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)21.394, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc223()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 223, Nome = "FAIXA_CONSUMO.CONSUMO_FATURADO", IdentacaoNome = "FAIXA_CONSUMO.CONSUMO_FATURADO", Level = 1, Descricao = "Consumo Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 2, SomentePreview = 0, ValorPreview = "15,00;999,00;9.999,00;99.999,00;999.999,00;9.999.999,00;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)52.008, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)13.076, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc224()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 224, Nome = "FAIXA_CONSUMO.TARIFA_AGUA", IdentacaoNome = "FAIXA_CONSUMO.TARIFA_AGUA", Level = 1, Descricao = "Tarifa de água.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 3, SomentePreview = 0, ValorPreview = "99,99;999,99;9.999,99;99.999,99;999.999,99;9.999.999,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)66.15, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)15.379, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc225()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 225, Nome = "FAIXA_CONSUMO.TARIFA_ESGOTO", IdentacaoNome = "FAIXA_CONSUMO.TARIFA_ESGOTO", Level = 1, Descricao = "Tarifa de esgoto.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 221, Ordem = 4, SomentePreview = 0, ValorPreview = "99,99;999,99;9.999,99;99.999,99;999.999,99;9.999.999,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.295, EstiloPosicaoTopo = (float)88.7, EstiloAltura = (float)3.209, EstiloLargura = (float)15.379, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 5, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc226()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 226, Nome = "DISCRIMINACAO_FATURAMENTO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO", Level = 1, Descricao = "Discriminação do Faturamento.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 4, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 63, EstiloAltura = 43, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 5, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 227, Nome = "DISCRIMINACAO_FATURAMENTO.ESPECIFICACAO_SERVICO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.ESPECIFICACAO_SERVICO", Level = 2, Descricao = "Especificação do serviço.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 1, SomentePreview = 0, ValorPreview = "AGUA;ESGOTO;RECURSOS HIDRICOS;DESCONTO;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)3.8, EstiloPosicaoTopo = 117, EstiloAltura = (float)2.479, EstiloLargura = (float)67.446, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 228, Nome = "DISCRIMINACAO_FATURAMENTO.VALOR_FATURADO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.VALOR_FATURADO", Level = 2, Descricao = "Valor Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 2, SomentePreview = 0, ValorPreview = "255.080,00;140.294,37;1.858,25;48.100,61;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.7, EstiloPosicaoTopo = 117, EstiloAltura = (float)2.479, EstiloLargura = (float)20.8, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 232, Nome = "DISCRIMINACAO_FATURAMENTO.RETENCAO_TRUBUTOS", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.RETENCAO_TRUBUTOS", Level = 2, Descricao = "Retenção de Tributos.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 3, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = 24, EstiloPosicaoTopo = (float)142.158, EstiloAltura = (float)5.5, EstiloLargura = (float)20.381, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 233, Nome = "DISCRIMINACAO_FATURAMENTO.TOTAL_PAGAR", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.TOTAL_PAGAR", Level = 2, Descricao = "Total a Pagar.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 4, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)66.248, EstiloPosicaoTopo = (float)141.658, EstiloAltura = (float)6.2, EstiloLargura = (float)34.542, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 234, Nome = "DISCRIMINACAO_FATURAMENTO.CARGA_TRIBUTARIA_SERVICOS", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.CARGA_TRIBUTARIA_SERVICOS", Level = 2, Descricao = "Carga tributária sobre o valor dos serviços.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 5, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.449, EstiloPosicaoTopo = (float)148.339, EstiloAltura = (float)4.43, EstiloLargura = (float)19.831, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc227()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 227, Nome = "DISCRIMINACAO_FATURAMENTO.ESPECIFICACAO_SERVICO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.ESPECIFICACAO_SERVICO", Level = 1, Descricao = "Especificação do serviço.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 1, SomentePreview = 0, ValorPreview = "AGUA;ESGOTO;RECURSOS HIDRICOS;DESCONTO;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)3.8, EstiloPosicaoTopo = 117, EstiloAltura = (float)2.479, EstiloLargura = (float)67.446, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc228()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 228, Nome = "DISCRIMINACAO_FATURAMENTO.VALOR_FATURADO", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.VALOR_FATURADO", Level = 1, Descricao = "Valor Faturado.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 2, SomentePreview = 0, ValorPreview = "255.080,00;140.294,37;1.858,25;48.100,61;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.7, EstiloPosicaoTopo = 117, EstiloAltura = (float)2.479, EstiloLargura = (float)20.8, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 2, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc232()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 232, Nome = "DISCRIMINACAO_FATURAMENTO.RETENCAO_TRUBUTOS", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.RETENCAO_TRUBUTOS", Level = 1, Descricao = "Retenção de Tributos.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 3, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = 24, EstiloPosicaoTopo = (float)142.158, EstiloAltura = (float)5.5, EstiloLargura = (float)20.381, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc233()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 233, Nome = "DISCRIMINACAO_FATURAMENTO.TOTAL_PAGAR", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.TOTAL_PAGAR", Level = 1, Descricao = "Total a Pagar.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 4, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)66.248, EstiloPosicaoTopo = (float)141.658, EstiloAltura = (float)6.2, EstiloLargura = (float)34.542, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc234()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 234, Nome = "DISCRIMINACAO_FATURAMENTO.CARGA_TRIBUTARIA_SERVICOS", IdentacaoNome = "DISCRIMINACAO_FATURAMENTO.CARGA_TRIBUTARIA_SERVICOS", Level = 1, Descricao = "Carga tributária sobre o valor dos serviços.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 226, Ordem = 5, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)81.449, EstiloPosicaoTopo = (float)148.339, EstiloAltura = (float)4.43, EstiloLargura = (float)19.831, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 6, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc235()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 235, Nome = "ESPACO_MENSAGEM", IdentacaoNome = "ESPACO_MENSAGEM", Level = 1, Descricao = "Espaço para mensagens e Identificador de débito automático.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 5, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 148, EstiloAltura = 33, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 5, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 236, Nome = "ESPACO_MENSAGEM.MENSAGEM", IdentacaoNome = "ESPACO_MENSAGEM.MENSAGEM", Level = 2, Descricao = "Mensagens.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 235, Ordem = 1, SomentePreview = 0, ValorPreview = "LINHA1;LINHA2;LINHA 3;LINHA 4;LINHA 5;LINHA 6;LINHA 7;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)3.147, EstiloPosicaoTopo = (float)156.888, EstiloAltura = 3, EstiloLargura = (float)94.253, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 237, Nome = "ESPACO_MENSAGEM.IDENTIFICADOR_DEBITO_AUTOMATICO", IdentacaoNome = "ESPACO_MENSAGEM.IDENTIFICADOR_DEBITO_AUTOMATICO", Level = 2, Descricao = "Identificador de débito automático.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 235, Ordem = 2, SomentePreview = 0, ValorPreview = "9999999-9", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)42.351, EstiloPosicaoTopo = (float)183.251, EstiloAltura = (float)4.664, EstiloLargura = (float)56.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc236()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 236, Nome = "ESPACO_MENSAGEM.MENSAGEM", IdentacaoNome = "ESPACO_MENSAGEM.MENSAGEM", Level = 1, Descricao = "Mensagens.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 235, Ordem = 1, SomentePreview = 0, ValorPreview = "LINHA1;LINHA2;LINHA 3;LINHA 4;LINHA 5;LINHA 6;LINHA 7;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)3.147, EstiloPosicaoTopo = (float)156.888, EstiloAltura = 3, EstiloLargura = (float)94.253, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc237()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 237, Nome = "ESPACO_MENSAGEM.IDENTIFICADOR_DEBITO_AUTOMATICO", IdentacaoNome = "ESPACO_MENSAGEM.IDENTIFICADOR_DEBITO_AUTOMATICO", Level = 1, Descricao = "Identificador de débito automático.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 235, Ordem = 2, SomentePreview = 0, ValorPreview = "9999999-9", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)42.351, EstiloPosicaoTopo = (float)183.251, EstiloAltura = (float)4.664, EstiloLargura = (float)56.891, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc238()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 238, Nome = "INFORMACOES_QUALIDADE_AGUA", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA", Level = 1, Descricao = "Informações sobre a qualidade da água.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 6, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 182, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 5, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 239, Nome = "INFORMACOES_QUALIDADE_AGUA.SISTEMA_ABASTECIMENTO", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.SISTEMA_ABASTECIMENTO", Level = 2, Descricao = "Sistema de Abastecimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 1, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.502, EstiloPosicaoTopo = (float)192.15, EstiloAltura = (float)3.904, EstiloLargura = (float)76.92, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 240, Nome = "INFORMACOES_QUALIDADE_AGUA.FLUOR", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.FLUOR", Level = 2, Descricao = "Fluor.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 2, SomentePreview = 0, ValorPreview = "99;99;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = 23, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 241, Nome = "INFORMACOES_QUALIDADE_AGUA.CLORO", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.CLORO", Level = 2, Descricao = "Cloro.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 3, SomentePreview = 0, ValorPreview = "999;999;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)34.467, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 242, Nome = "INFORMACOES_QUALIDADE_AGUA.TURBIDEZ", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.TURBIDEZ", Level = 2, Descricao = "Turbidez.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 4, SomentePreview = 0, ValorPreview = "999;999;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)45.503, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 243, Nome = "INFORMACOES_QUALIDADE_AGUA.COR", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.COR", Level = 2, Descricao = "Cor.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 5, SomentePreview = 0, ValorPreview = "99;99;99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)56.792, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 244, Nome = "INFORMACOES_QUALIDADE_AGUA.PH", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.PH", Level = 2, Descricao = "Ph.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 6, SomentePreview = 0, ValorPreview = "99;99;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)67.851, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 245, Nome = "INFORMACOES_QUALIDADE_AGUA.COLIFORMESTOTAIS", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.COLIFORMESTOTAIS", Level = 2, Descricao = "Coliformes totais.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 7, SomentePreview = 0, ValorPreview = "999;999;999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)79.3, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 246, Nome = "INFORMACOES_QUALIDADE_AGUA.ESCHERICHIACOLI", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.ESCHERICHIACOLI", Level = 2, Descricao = "Escherichia Coli.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 8, SomentePreview = 0, ValorPreview = "999;999;999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)90.1, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    },
                new PdfGenItemDoc
                    {
                        Id = 247, Nome = "INFORMACOES_QUALIDADE_AGUA.OBSERVACOES", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.OBSERVACOES", Level = 2, Descricao = "Observações (qualidade da água).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 9, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.402, EstiloPosicaoTopo = (float)209.429, EstiloAltura = (float)3.904, EstiloLargura = (float)60.293, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc239()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 239, Nome = "INFORMACOES_QUALIDADE_AGUA.SISTEMA_ABASTECIMENTO", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.SISTEMA_ABASTECIMENTO", Level = 1, Descricao = "Sistema de Abastecimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 1, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.502, EstiloPosicaoTopo = (float)192.15, EstiloAltura = (float)3.904, EstiloLargura = (float)76.92, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc240()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 240, Nome = "INFORMACOES_QUALIDADE_AGUA.FLUOR", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.FLUOR", Level = 1, Descricao = "Fluor.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 2, SomentePreview = 0, ValorPreview = "99;99;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = 23, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc241()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 241, Nome = "INFORMACOES_QUALIDADE_AGUA.CLORO", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.CLORO", Level = 1, Descricao = "Cloro.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 3, SomentePreview = 0, ValorPreview = "999;999;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)34.467, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc242()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 242, Nome = "INFORMACOES_QUALIDADE_AGUA.TURBIDEZ", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.TURBIDEZ", Level = 1, Descricao = "Turbidez.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 4, SomentePreview = 0, ValorPreview = "999;999;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)45.503, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc243()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 243, Nome = "INFORMACOES_QUALIDADE_AGUA.COR", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.COR", Level = 1, Descricao = "Cor.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 5, SomentePreview = 0, ValorPreview = "99;99;99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)56.792, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc244()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 244, Nome = "INFORMACOES_QUALIDADE_AGUA.PH", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.PH", Level = 1, Descricao = "Ph.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 6, SomentePreview = 0, ValorPreview = "99;99;99,99;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)67.851, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc245()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 245, Nome = "INFORMACOES_QUALIDADE_AGUA.COLIFORMESTOTAIS", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.COLIFORMESTOTAIS", Level = 1, Descricao = "Coliformes totais.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 7, SomentePreview = 0, ValorPreview = "999;999;999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)79.3, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc246()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 246, Nome = "INFORMACOES_QUALIDADE_AGUA.ESCHERICHIACOLI", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.ESCHERICHIACOLI", Level = 1, Descricao = "Escherichia Coli.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 8, SomentePreview = 0, ValorPreview = "999;999;999;", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)90.1, EstiloPosicaoTopo = (float)199.421, EstiloAltura = (float)2.906, EstiloLargura = (float)10.707, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 2
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc247()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 247, Nome = "INFORMACOES_QUALIDADE_AGUA.OBSERVACOES", IdentacaoNome = "INFORMACOES_QUALIDADE_AGUA.OBSERVACOES", Level = 1, Descricao = "Observações (qualidade da água).", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 238, Ordem = 9, SomentePreview = 0, ValorPreview = "LOREM IPSUM DOLOR SIT AMET.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)22.402, EstiloPosicaoTopo = (float)209.429, EstiloAltura = (float)3.904, EstiloLargura = (float)60.293, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc248()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 248, Nome = "VIA_BANCO.IDENTIFICACAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO", Level = 1, Descricao = "Via do banco.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 7, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 208, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 5, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 249, Nome = "VIA_BANCO.IDENTIFICACAO.NUMERO_LIGACAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.NUMERO_LIGACAO", Level = 2, Descricao = "Número da ligação. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 1, SomentePreview = 0, ValorPreview = "11000000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.259, EstiloPosicaoTopo = (float)223.269, EstiloAltura = (float)5.941, EstiloLargura = (float)25.264, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 9, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 250, Nome = "VIA_BANCO.IDENTIFICACAO.CONTA_NUMERO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.CONTA_NUMERO", Level = 2, Descricao = "Número da Conta.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 2, SomentePreview = 0, ValorPreview = "17204457", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)11.907, EstiloPosicaoTopo = (float)229.877, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 251, Nome = "VIA_BANCO.IDENTIFICACAO.REFERENCIA", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.REFERENCIA", Level = 2, Descricao = "Referência.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 3, SomentePreview = 0, ValorPreview = "03/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.467, EstiloPosicaoTopo = (float)229.877, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 252, Nome = "VIA_BANCO.IDENTIFICACAO.DATA_EMISSAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.DATA_EMISSAO", Level = 2, Descricao = "Data Emissão.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 4, SomentePreview = 0, ValorPreview = "03/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)17.454, EstiloPosicaoTopo = 235, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 253, Nome = "VIA_BANCO.IDENTIFICACAO.DATA_VENCIMENTO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.DATA_VENCIMENTO", Level = 2, Descricao = "Vencimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 5, SomentePreview = 0, ValorPreview = "18/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.467, EstiloPosicaoTopo = 235, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 254, Nome = "VIA_BANCO.IDENTIFICACAO.TOTAL_PAGAR", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.TOTAL_PAGAR", Level = 2, Descricao = "Total a pagar.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 6, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.51, EstiloPosicaoTopo = (float)233.079, EstiloAltura = (float)4.973, EstiloLargura = (float)24.764, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc249()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 249, Nome = "VIA_BANCO.IDENTIFICACAO.NUMERO_LIGACAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.NUMERO_LIGACAO", Level = 1, Descricao = "Número da ligação. ", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 1, SomentePreview = 0, ValorPreview = "11000000001", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)72.259, EstiloPosicaoTopo = (float)223.269, EstiloAltura = (float)5.941, EstiloLargura = (float)25.264, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 9, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc250()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 250, Nome = "VIA_BANCO.IDENTIFICACAO.CONTA_NUMERO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.CONTA_NUMERO", Level = 1, Descricao = "Número da Conta.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 2, SomentePreview = 0, ValorPreview = "17204457", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)11.907, EstiloPosicaoTopo = (float)229.877, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc251()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 251, Nome = "VIA_BANCO.IDENTIFICACAO.REFERENCIA", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.REFERENCIA", Level = 1, Descricao = "Referência.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 3, SomentePreview = 0, ValorPreview = "03/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.467, EstiloPosicaoTopo = (float)229.877, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc252()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 252, Nome = "VIA_BANCO.IDENTIFICACAO.DATA_EMISSAO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.DATA_EMISSAO", Level = 1, Descricao = "Data Emissão.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 4, SomentePreview = 0, ValorPreview = "03/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)17.454, EstiloPosicaoTopo = 235, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc253()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 253, Nome = "VIA_BANCO.IDENTIFICACAO.DATA_VENCIMENTO", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.DATA_VENCIMENTO", Level = 1, Descricao = "Vencimento.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 5, SomentePreview = 0, ValorPreview = "18/04/2017", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)48.467, EstiloPosicaoTopo = 235, EstiloAltura = 6, EstiloLargura = (float)19.842, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc254()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 254, Nome = "VIA_BANCO.IDENTIFICACAO.TOTAL_PAGAR", IdentacaoNome = "VIA_BANCO.IDENTIFICACAO.TOTAL_PAGAR", Level = 1, Descricao = "Total a pagar.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 248, Ordem = 6, SomentePreview = 0, ValorPreview = "9.999.999,99", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)73.51, EstiloPosicaoTopo = (float)233.079, EstiloAltura = (float)4.973, EstiloLargura = (float)24.764, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc255()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 255, Nome = "CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS", Level = 1, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 8, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 182, EstiloAltura = 26, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 5, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 256, Nome = "CODIGO_BARRAS.LINHA_DIGITAVEL", IdentacaoNome = "CODIGO_BARRAS.LINHA_DIGITAVEL", Level = 2, Descricao = "Linha digitável do código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 255, Ordem = 1, SomentePreview = 0, ValorPreview = "12345678901-1 12345678901-1 12345678901-1 12345678901-1", EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = (float)242.079, EstiloAltura = (float)4.973, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 257, Nome = "CODIGO_BARRAS.CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS.CODIGO_BARRAS", Level = 2, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 3, IdItemDocumentoParent = 255, Ordem = 2, SomentePreview = 0, ValorPreview = "123456789011123456789011123456789011123456789011", EstiloPosicao = 1, EstiloPosicaoEsquerda = 2, EstiloPosicaoTopo = (float)245.271, EstiloAltura = 20, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 258, Nome = "CODIGO_BARRAS.TEXTO_ALTERNNATIVO_CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS.TEXTO_ALTERNNATIVO_CODIGO_BARRAS", Level = 2, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 255, Ordem = 3, SomentePreview = 0, ValorPreview = "CONTA EM DÉBITO AUTOMÁTICO", EstiloPosicao = 1, EstiloPosicaoEsquerda = 5, EstiloPosicaoTopo = (float)245.271, EstiloAltura = 20, EstiloLargura = 90, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc256()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 256, Nome = "CODIGO_BARRAS.LINHA_DIGITAVEL", IdentacaoNome = "CODIGO_BARRAS.LINHA_DIGITAVEL", Level = 1, Descricao = "Linha digitável do código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 255, Ordem = 1, SomentePreview = 0, ValorPreview = "12345678901-1 12345678901-1 12345678901-1 12345678901-1", EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = (float)242.079, EstiloAltura = (float)4.973, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc257()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 257, Nome = "CODIGO_BARRAS.CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS.CODIGO_BARRAS", Level = 1, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 3, IdItemDocumentoParent = 255, Ordem = 2, SomentePreview = 0, ValorPreview = "123456789011123456789011123456789011123456789011", EstiloPosicao = 1, EstiloPosicaoEsquerda = 2, EstiloPosicaoTopo = (float)245.271, EstiloAltura = 20, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc258()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 258, Nome = "CODIGO_BARRAS.TEXTO_ALTERNNATIVO_CODIGO_BARRAS", IdentacaoNome = "CODIGO_BARRAS.TEXTO_ALTERNNATIVO_CODIGO_BARRAS", Level = 1, Descricao = "Código de barras.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 255, Ordem = 3, SomentePreview = 0, ValorPreview = "CONTA EM DÉBITO AUTOMÁTICO", EstiloPosicao = 1, EstiloPosicaoEsquerda = 5, EstiloPosicaoTopo = (float)245.271, EstiloAltura = 20, EstiloLargura = 90, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 0, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 5, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc259()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 259, Nome = "AVISO_DEBITO", IdentacaoNome = "AVISO_DEBITO", Level = 1, Descricao = "Bloco 7 - Aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 0, IdItemDocumentoParent = 182, Ordem = 9, SomentePreview = 0, ValorPreview = null, EstiloPosicao = 1, EstiloPosicaoEsquerda = 0, EstiloPosicaoTopo = 257, EstiloAltura = 83, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteTamanho = 6, EstiloFonteColor = null, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 260, Nome = "AVISO_DEBITO.AVISODEBITO_MENSAGEM_PARTE", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_MENSAGEM_PARTE", Level = 2, Descricao = "Mensagem do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 259, Ordem = 1, SomentePreview = 0, ValorPreview = @"Aviso nº: AAA                                 Lote nº: LLL

                        Prezado (a) cliente,

                        Verificamos que até DD/MM/YYYY constava em nossos registros, o débito total de: R$ V.VVV,VV. O não pagamento da (s) conta (s), resultará em:
                          - Suspensão do Fornecimento de água, conforme legislação.
                          -Inclusão nos órgãos de proteção ao crédito.
                        Caso o corte seja efetivado, pode haver custo adicional para religação.
                        Caso já tenha efetuado o pagamento, favor desconsiderar este aviso.

                        Mês / Ano x Valor  | Mês / Ano x Valor   | Mês / Ano x Valor
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99

                        Valores sujeitos à inclusão de juros e multa.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)108.45, EstiloPosicaoTopo = (float)7.85, EstiloAltura = 50, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 7, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 261, Nome = "AVISO_DEBITO.AVISODEBITO_LINHA_DIGITAVEL", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_LINHA_DIGITAVEL", Level = 2, Descricao = "Linha digitável do código de barras do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 259, Ordem = 2, SomentePreview = 0, ValorPreview = "12345678901-1 12345678901-1 12345678901-1 12345678901-1", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)108.45, EstiloPosicaoTopo = 58, EstiloAltura = 5, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = (float)6.5, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    },
                new PdfGenItemDoc
                    {
                        Id = 262, Nome = "AVISO_DEBITO.AVISODEBITO_CODIGOBARRAS", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_CODIGOBARRAS", Level = 2, Descricao = "Código de barras do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 3, IdItemDocumentoParent = 259, Ordem = 3, SomentePreview = 0, ValorPreview = "123456789011123456789011123456789011123456789011", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)109.45, EstiloPosicaoTopo = 70, EstiloAltura = 20, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 7, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc260()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 260, Nome = "AVISO_DEBITO.AVISODEBITO_MENSAGEM_PARTE", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_MENSAGEM_PARTE", Level = 1, Descricao = "Mensagem do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 259, Ordem = 1, SomentePreview = 0, ValorPreview = @"Aviso nº: AAA                                 Lote nº: LLL

                        Prezado (a) cliente,

                        Verificamos que até DD/MM/YYYY constava em nossos registros, o débito total de: R$ V.VVV,VV. O não pagamento da (s) conta (s), resultará em:
                          - Suspensão do Fornecimento de água, conforme legislação.
                          -Inclusão nos órgãos de proteção ao crédito.
                        Caso o corte seja efetivado, pode haver custo adicional para religação.
                        Caso já tenha efetuado o pagamento, favor desconsiderar este aviso.

                        Mês / Ano x Valor  | Mês / Ano x Valor   | Mês / Ano x Valor
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99
                        MM / YYYY 9999,99 | MM / YYYY 9999,99 | MM / YYYY 9999,99

                        Valores sujeitos à inclusão de juros e multa.", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)108.45, EstiloPosicaoTopo = (float)7.85, EstiloAltura = 50, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 7, EstiloFonteTamanho = 7, EstiloFonteFormato = 0, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 0, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc261()
        {
            return new List<PdfGenItemDoc>
            {
                new PdfGenItemDoc
                    {
                        Id = 261, Nome = "AVISO_DEBITO.AVISODEBITO_LINHA_DIGITAVEL", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_LINHA_DIGITAVEL", Level = 1, Descricao = "Linha digitável do código de barras do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 1, IdItemDocumentoParent = 259, Ordem = 2, SomentePreview = 0, ValorPreview = "12345678901-1 12345678901-1 12345678901-1 12345678901-1", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)108.45, EstiloPosicaoTopo = 58, EstiloAltura = 5, EstiloLargura = 100, EstiloMargemEsquerda = 0, EstiloMargemTopo = (float)6.5, EstiloFonteNome = 0, EstiloFonteTamanho = 8, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        private static IEnumerable<PdfGenItemDoc> GeraListaPdfGenItemDoc262()
        {
            return new List<PdfGenItemDoc>
            {
               new PdfGenItemDoc
                    {
                        Id = 262, Nome = "AVISO_DEBITO.AVISODEBITO_CODIGOBARRAS", IdentacaoNome = "AVISO_DEBITO.AVISODEBITO_CODIGOBARRAS", Level = 1, Descricao = "Código de barras do aviso de débito.", IdTipoDocumento = 1, TipoItemDocumento = 3, IdItemDocumentoParent = 259, Ordem = 3, SomentePreview = 0, ValorPreview = "123456789011123456789011123456789011123456789011", EstiloPosicao = 1, EstiloPosicaoEsquerda = (float)109.45, EstiloPosicaoTopo = 70, EstiloAltura = 20, EstiloLargura = 98, EstiloMargemEsquerda = 0, EstiloMargemTopo = 0, EstiloFonteNome = 7, EstiloFonteTamanho = 7, EstiloFonteFormato = 1, EstiloFonteColor = null, EstiloSombra = 0, EstiloAlinhamentoVertical = 4, EstiloAlinhamentoHorizontal = 1, EstiloRepeticaoOrientacao = 0
                    }
            };
        }

        #endregion
    }
}
