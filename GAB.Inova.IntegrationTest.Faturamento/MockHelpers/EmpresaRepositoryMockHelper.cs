﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório de empresa
    /// </summary>
    /// <returns><see cref="IEmpresaRepository"/></returns>
    public static class EmpresaRepositoryMockHelper
    {
        public static IEmpresaRepository GetEmpresaRepository()
        {
            var empresa = GeraEmpresa();

            var mockEmpresaRepositoryRepository = new Mock<IEmpresaRepository>();

            mockEmpresaRepositoryRepository.Setup(m => m.ObterAsync()).ReturnsAsync(empresa);

            return mockEmpresaRepositoryRepository.Object;
        }

        #region [Metodos privados]

        private static Empresa GeraEmpresa()
        {
            return new Empresa
            {
                Id = "0101",
                Nome = "ÁGUAS DE ARAÇOIABA S/A",
                Sigla = "CAA",
                IdFebraban = "1217"
            };
        }

        #endregion
    }
}
