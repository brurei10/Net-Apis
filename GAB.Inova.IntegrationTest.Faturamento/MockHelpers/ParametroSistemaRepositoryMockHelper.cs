﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela parametro do sistema
    /// </summary>
    /// <returns><see cref="IParametroSistemaRepository"/></returns>
    public static class ParametroSistemaRepositoryMockHelper
    {
        public static IParametroSistemaRepository GetParametroSistemaRepository()
        {
            var parametroSistema708 = GeraParametroSistema708();
            var parametroSistema361 = GeraParametroSistema361();

            var mockParametroSistemaRepository = new Mock<IParametroSistemaRepository>();

            mockParametroSistemaRepository.Setup(m => m.ObterAsync(708)).ReturnsAsync(parametroSistema708);
            mockParametroSistemaRepository.Setup(m => m.ObterAsync(361)).ReturnsAsync(parametroSistema361);

            return mockParametroSistemaRepository.Object;
        }

        #region [Metodos privados]

        private static ParametroSistema GeraParametroSistema708()
        {
            return new ParametroSistema
            {
                Id = 708,
                Descricao = "TEMPO EXPIRAÇÃO TOKEN DE SOLICITAÇÃO CONTA DIGITAL",
                IdUnidadeMedida = 88,
                Situacao = 1,
                Valor = 120,
                UnidadeMedida = new UnidadeMedida
                {
                    Id = 88,
                    Descricao = "MINUTOS",
                    Sigla = "MIN   "
                }
            };
        }

        private static ParametroSistema GeraParametroSistema361()
        {
            return new ParametroSistema
            {
                Id = 361,
                Descricao = "QUANTIDADE DE DIAS UTEIS PARA VENCIMENTO DA DNF",
                IdUnidadeMedida = 76,
                Situacao = 1,
                Valor = 5,
                UnidadeMedida = new UnidadeMedida
                {
                    Id = 76,
                    Descricao = "DIA",
                    Sigla = "D"
                }
            };
        }

        #endregion
    }
}
