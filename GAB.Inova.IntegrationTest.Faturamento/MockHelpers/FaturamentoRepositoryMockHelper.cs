﻿using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela faturamento
    /// </summary>
    /// <returns><see cref="IFaturamentoRepository"/></returns>
    public static class FaturamentoRepositoryMockHelper
    {
        public static IFaturamentoRepository GetFaturamentoRepository()
        {
            var listaFaturamento = GeraListaFaturamento();

            var mockFaturamentoRepository = new Mock<IFaturamentoRepository>();

            mockFaturamentoRepository.Setup(m => m.ObterFaturamentoParaImpressaoContaAsync(It.IsAny<Domain.Faturamento>())).ReturnsAsync(listaFaturamento);

            return mockFaturamentoRepository.Object;
        }

        #region [Metodos privados]
        private static IEnumerable<Domain.Faturamento> GeraListaFaturamento()
        {
            return new List<Domain.Faturamento>
            {
                new Domain.Faturamento
                {
                    Id = 19389885,
                    AnoMesFaturamento = "202005",
                    Matricula = "0100000004",
                    Categoria = 1,
                    SeqOriginal = 1450926,
                    TipoServicoFaturado = 1,
                    ValorFaturamento = 52.54,
                    VolumePorEconomiaFaturado = 16.0,
                    VolumeTotalFaturado = 16.0
                },
                new Domain.Faturamento
                {
                    Id = 19389886,
                    AnoMesFaturamento = "202005",
                    Matricula = "0100000004",
                    Categoria = 1,
                    SeqOriginal = 1450926,
                    TipoServicoFaturado = 2,
                    ValorFaturamento = 50.15,
                    VolumePorEconomiaFaturado = 16.0,
                    VolumeTotalFaturado = 16.0
                }
            };
        }

        #endregion
    }
}
