﻿using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela de feriado
    /// </summary>
    /// <returns><see cref="IFeriadoRepository"/></returns>
    public static class FeriadoRepositoryMockHelper
    {
        public static IFeriadoRepository GetFeriadoRepository()
        {
            var mockFeriadoRepository = new Mock<IFeriadoRepository>();

            mockFeriadoRepository.Setup(m => m.ObterProximoDiaUtil(It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(Convert.ToDateTime("17/07/2020"));

            return mockFeriadoRepository.Object;
        }
    }
}
