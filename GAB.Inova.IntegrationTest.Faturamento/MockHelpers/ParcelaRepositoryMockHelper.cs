﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view de cliente
    /// </summary>
    /// <returns><see cref="IParcelaRepository"/></returns>
    public static class ParcelaRepositoryMockHelper
    {
        public static IParcelaRepository GetParcelaRepository()
        {
            var mockParcelaRepository = new Mock<IParcelaRepository>();

            mockParcelaRepository.Setup(m => m.ObterTotalPorMatriculaSituacaoContratoAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<long>()))
                .ReturnsAsync(0);

            return mockParcelaRepository.Object;
        }
    }
}
