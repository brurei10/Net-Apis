﻿using GAB.Inova.Common.Enums;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections.Generic;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela análise de água.
    /// </summary>
    public static class TabAnaliseAguaRepositoryMockHelper
    {
        /// <summary>
        /// The GetTabAnaliseAguaRepository.
        /// </summary>
        /// <returns>The <see cref="ITabAnaliseAguaRepository"/>.</returns>
        public static ITabAnaliseAguaRepository GetTabAnaliseAguaRepository()
        {
            var listaTabAnaliseAgua = GeraListaTabAnaliseAgua();

            var mockTabAnaliseAguaRepository = new Mock<ITabAnaliseAguaRepository>();

            mockTabAnaliseAguaRepository.Setup(m => m.ObterAnaliseAguaParaImpressaoContaAsync(It.IsAny<long>(), It.IsAny<long>())).ReturnsAsync(listaTabAnaliseAgua);

            return mockTabAnaliseAguaRepository.Object;
        }

        /// <summary>
        /// The GeraListaTabAnaliseAgua.
        /// </summary>
        /// <returns>The <see cref="IEnumerable{TabAnaliseAgua}"/>.</returns>
        private static IEnumerable<TabAnaliseAgua> GeraListaTabAnaliseAgua()
        {
            return new List<TabAnaliseAgua>
            {
                new TabAnaliseAgua
                {
                    Id = 5391,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Coliformes totais",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "0",
                    Referencias = "Ausentes",
                    SeqAmostra = 6,
                    DescricaoColuna = TipoAnaliseAguaEnum.COLTOTAL
                },
                new TabAnaliseAgua
                {
                    Id = 5386,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Flúor",
                    AmostraExigida = "0",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "0,76",
                    Referencias = "< 1,5 mg/l",
                    SeqAmostra = 1,
                    DescricaoColuna = TipoAnaliseAguaEnum.FLUOR
                },
                new TabAnaliseAgua
                {
                    Id = 5388,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Turbidez",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "0,25",
                    Referencias = "< 5 NTU",
                    SeqAmostra = 3,
                    DescricaoColuna = TipoAnaliseAguaEnum.TURBIDEZ
                },
                new TabAnaliseAgua
                {
                    Id = 5392,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Colif. termotolerantes",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "0",
                    Referencias = "Ausentes",
                    SeqAmostra = 7,
                    DescricaoColuna = TipoAnaliseAguaEnum.COLTERM
                },
                new TabAnaliseAgua
                {
                    Id = 5390,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "pH",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "7,25",
                    Referencias = "6,0 – 9,0 Sorensen",
                    SeqAmostra = 5,
                    DescricaoColuna = TipoAnaliseAguaEnum.PH
                },
                new TabAnaliseAgua
                {
                    Id = 5389,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Cor",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "1,57",
                    Referencias = "< 15 mg/l Pt",
                    SeqAmostra = 4,
                    DescricaoColuna = TipoAnaliseAguaEnum.COR
                },
                new TabAnaliseAgua
                {
                    Id = 5387,
                    AnoMes = "202005",
                    Tipo = 1,
                    Descricao = "Cloro",
                    AmostraExigida = "44",
                    AmostraRealizada = "44",
                    ValorMedioDetectado = "1,39",
                    Referencias = "0,2 – 5,0 mg/l",
                    SeqAmostra = 2,
                    DescricaoColuna = TipoAnaliseAguaEnum.CLORO
                }
            };
        }
    }
}
