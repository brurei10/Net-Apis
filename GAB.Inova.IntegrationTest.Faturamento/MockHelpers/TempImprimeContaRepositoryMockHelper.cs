﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela temp imprime
    /// </summary>
    /// <returns><see cref="ITempImprimeContaRepository"/></returns>
    public static class TempImprimeContaRepositoryMockHelper
    {
        public static ITempImprimeContaRepository GetTempImprimeContaRepository()
        {
            var mockTempImprimeContaRepository = new Mock<ITempImprimeContaRepository>();

            mockTempImprimeContaRepository.Setup(m => m.DeletarTempSeqAsync(It.IsAny<TempImprimeConta>())).ReturnsAsync(true);
            mockTempImprimeContaRepository.Setup(m => m.InserirTempSeqAsync(It.IsAny<TempImprimeConta>())).ReturnsAsync(true);
            mockTempImprimeContaRepository.Setup(m => m.GerarTempContaAsync(It.IsAny<TempImprimeConta>())).ReturnsAsync(true);

            return mockTempImprimeContaRepository.Object;
        }
    }
}
