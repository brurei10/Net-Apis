﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Faturamento.MockHelpers
{
    /// <summary>
    /// Obter o repositório da tabela temp cobrança CT
    /// </summary>
    /// <returns><see cref="ITempCobrancaCtRepository"/></returns>
    public static class TempCobrancaCtRepositoryMockHelper
    {
        public static ITempCobrancaCtRepository GetTempCobrancaCtRepository()
        {
            var mockTempCobrancaCtRepository = new Mock<ITempCobrancaCtRepository>();

            mockTempCobrancaCtRepository.Setup(m => m.InserirAsync(It.IsAny<TempCobrancaCt>())).ReturnsAsync(true);

            return mockTempCobrancaCtRepository.Object;
        }
    }
}
