﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Atendimento;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Consumo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Consumo.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ConsumoController 
        : ControllerBase
    {

        const long ID_ROTINA = 9999999983;          //API Inova 2.0 - Consumo
        const int ID_NATUREZA_HIST_CONSUMO = 19;    //INFORMAÇÃO DE CONSUMO

        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly IConsumoBusiness _consumoBusiness;
        readonly ILogger<ConsumoController> _logger;

        public ConsumoController(ILogger<ConsumoController> logger,
                                 IAtendimentoBusiness atendimentoBusiness,
                                 IConsumoBusiness consumoBusiness)
        {
            _logger = logger;
            _atendimentoBusiness = atendimentoBusiness;
            _consumoBusiness = consumoBusiness;
        }

        [Authorize("Bearer")]
        [HttpGet]
        [Route("ObterHistoricoConsumos")]
        [ProducesResponseType(typeof(HistoricoConsumosResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObterHistoricoContasAsync([FromHeader]long? rotinaId, [FromQuery]string numeroProtocolo)
        {
            try
            {
                _logger.LogInformation($"Obter histórico de consumos para protocolo [nº:{numeroProtocolo}].");

                var result = await _consumoBusiness.ObterHistoricoConsumosAsync(numeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_HIST_CONSUMO, 
                        NumeroProtocolo = numeroProtocolo,
                        RotinaId = rotinaId.HasValue ? rotinaId.Value : ID_ROTINA
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter histórico de consumos para protocolo [nº:{numeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Não foi possível carregar informações, tente novamente mais tarde.");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

    }
}
