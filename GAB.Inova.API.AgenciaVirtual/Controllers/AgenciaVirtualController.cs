﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.Enums;
using GAB.Inova.Common.Extensions;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.AgenciaVirtual;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using GAB.Inova.Common.ViewModels.Atendimento;
using System.Collections.Generic;
using System.Linq;

namespace GAB.Inova.API.AgenciaVirtual.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços da Agência Virtual
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize("Bearer")]
    public class AgenciaVirtualController : ControllerBase
    {
        const int ID_NATUREZA_SEGUNDA_VIA = 1;                //2ª VIA DE NOTA FISCAL/ CONTA
        const int ID_NATUREZA_EMISSAO_CERTIDAO_NEGATIVA = 11; //EMISSÃO DE CERTIDÃO NEGATIVA (DECLARAÇÃO ANUAL)
        const int ID_NATUREZA_HIST_CONTA = 16;                //INFORMAÇÃO SOBRE COMPOSIÇÃO DA CONTA
        const int ID_NATUREZA_INFO_DEBITOS = 17;              //INFORMAÇÃO SOBRE DÉBITO
        const int ID_NATUREZA_QUITACAO_DEBITO = 25;           //GERAÇÃO DA QUITAÇÃO DE DÉBITO
        const int ID_NATUREZA_ACESSO_AGENCIA_VIRTUAL = 33;    //ACESSO AGENCIA VIRTUAL

        readonly IAgenciaVirtualBusiness _agenciaVirtualBusiness;
        readonly IAtendimentoBusiness _atendimentoBusiness;
        readonly IFaturamentoBusiness _faturamentoBusiness;
        readonly ILogger<AgenciaVirtualController> _logger;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="atendimentoBusiness">Instância do objeto com as regras de negócio referente a Atendimento</param>
        /// <param name="faturamentoBusiness">Instância do objeto com as regras de negócio referente a Faturamento</param>
        /// <param name="agenciaVirtualBusiness">Instância do objeto com as regras de negócio referente a Agência Virtual</param>
        public AgenciaVirtualController(ILogger<AgenciaVirtualController> logger,
                                        IAgenciaVirtualBusiness agenciaVirtualBusiness,
                                        IAtendimentoBusiness atendimentoBusiness,
                                        IFaturamentoBusiness faturamentoBusiness)
         {
            _logger = logger;
            _agenciaVirtualBusiness = agenciaVirtualBusiness;
            _atendimentoBusiness = atendimentoBusiness;
            _faturamentoBusiness = faturamentoBusiness;
        }

        /// <summary>
        /// Autenticar um cliente via Agência virtual
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns><see cref="AutenticarClienteResponseViewModel"/></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("AutenticarCliente")]
        [ProducesResponseType(typeof(AutenticarClienteResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AutenticarClienteAsync(AutenticarClienteRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation("Autenticando cliente via agência virtual", "");
                _logger.LogDebug($"ModelRecebido: { JsonConvert.SerializeObject(viewModel)}");

                var result = await _agenciaVirtualBusiness.AutenticarClienteAsync(new AutenticarClienteRequestViewModel
                {
                    matricula = viewModel.matricula,
                    documento = viewModel.documento
                });

                if (result.Sucesso)
                {
                    _logger.LogInformation("Cliente autenticado com sucesso.");

                    var protocoloResponse = await _atendimentoBusiness.GerarProtocoloAtendimentoAsync(viewModel.matricula, CanalAtendimentoEnum.AtendimentoDigital);

                    if (protocoloResponse.Sucesso)
                    {
                        var resultRegistrarNatureza = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel
                        {
                            NaturezaId = ID_NATUREZA_ACESSO_AGENCIA_VIRTUAL,
                            NumeroProtocolo = protocoloResponse.NumeroProtocolo
                        });

                        if (resultRegistrarNatureza.Sucesso)
                        {
                            var clienteAutenticado = JsonConvert.DeserializeObject<ClienteAutenticadoViewModel>(result.ClienteAutenticado);

                            clienteAutenticado.ProtocoloAtendimento = protocoloResponse.NumeroProtocolo;
                            clienteAutenticado.IdProtocoloAtendimento = Convert.ToInt64(protocoloResponse.NumeroProtocolo);

                            result.ClienteAutenticado = JsonConvert.SerializeObject(clienteAutenticado);

                            _logger.LogInformation($"Protocolo de atendimento [nº:{protocoloResponse.NumeroProtocolo}] gerado via {CanalAtendimentoEnum.AtendimentoDigital.GetDescription()}.");

                            return Ok(result);
                        }
                    }

                    result.AdicionaMensagem("Sistema indisponível! Tente novamente.");
                    result.Sucesso = false;

                    _logger.LogInformation($"Não foi possível gerar protocolo de atendimento via {CanalAtendimentoEnum.AtendimentoDigital.GetDescription()} para o cliente [matricula:{viewModel.matricula}].");
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao autenticar cliente via agência virtual. ModelRecebido: { JsonConvert.SerializeObject(viewModel)} ");

                return Unauthorized(ex.Message);
            }
        }

        /// <summary>
        /// Efetuar o logout da Agência virtual
        /// </summary>
        /// <param name="numeroProtocolo"></param>
        [HttpGet]
        [Route("Logout")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> LogoutAsync([FromHeader] string numeroProtocolo)
        {
            try
            {
                _logger.LogInformation($"Efetuando o logout protocolo de atendimento [nº:{numeroProtocolo}].");

                var result = await _atendimentoBusiness.FinalizarProtocoloAtendimentoAsync(numeroProtocolo);

                _logger.LogInformation($"Logout com sucesso protocolo de atendimento [nº:{numeroProtocolo}].");

                if (result.Sucesso) return Ok("Logout successful.");

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao efetuar o logout protocolo de atendimento [nº:{numeroProtocolo}].");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem("Ocorreu um erro inesperado na execução do serviço");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        [HttpGet]
        [Route("ObterHistoricoContas")]
        [ProducesResponseType(typeof(List<ContaResponseViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterHistoricoContasAsync([FromHeader] long protocolo)
        {
            try
            {
                _logger.LogInformation($"Obter histórico de contas da agência virtual para protocolo [{protocolo}].");

                var result = await _agenciaVirtualBusiness.ObterHistoricoContasAsync(protocolo.ToString());

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_HIST_CONTA,
                        NumeroProtocolo = protocolo.ToString()
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.ContasContrato.ToList());
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(";", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter histórico de contas da agência virtual para protocolo [{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("ObterDebitos")]
        [ProducesResponseType(typeof(List<ContaResponseViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterDebitosAsync([FromHeader] string numeroProtocolo)
        {
            try
            {
                _logger.LogInformation($"Obter débitos agência virtual para protocolo [nº:{numeroProtocolo}].");

                var result = await _agenciaVirtualBusiness.ObterHistoricoContasEmAbertoAsync(numeroProtocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_INFO_DEBITOS,
                        NumeroProtocolo = numeroProtocolo
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.ContasContrato.ToList());
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(";", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter débitos para protocolo [nº:{numeroProtocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("ObterSegundaVia")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterSegundaViaAsync([FromHeader] long protocolo, [FromHeader] string seqOriginal)
        {
            try
            {
                _logger.LogInformation($"Obter segunda via de conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                var result = await _faturamentoBusiness.ObterSegundaViaAsync(Convert.ToInt64(seqOriginal));

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                        NumeroProtocolo = protocolo.ToString()
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.EspelhoDaConta);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter segunda via de conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("EnviarEmailSegundaVia")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EnviarEmailSegundaViaAsync([FromHeader] long protocolo, [FromHeader] string seqOriginal, [FromHeader] string email)
        {
            try
            {
                _logger.LogInformation($"Agência virtual enviar e-mail de segunda via da conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                var result = await _faturamentoBusiness.EnviarEmailSegundaViaAsync(Convert.ToInt64(seqOriginal), email);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                {
                    NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                    NumeroProtocolo = protocolo.ToString()
                });

                if (!registrarNaturezaResult.Sucesso)
                {
                    foreach (var message in registrarNaturezaResult.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem($"Erro na agência virtual enviar e-mail de segunda via da conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                _logger.LogCritical(ex, $"Erro na agência virtual enviar e-mail de segunda via da conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("ObterCodigoBarrasConta")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterCodigoBarrasContaAsync([FromHeader] long protocolo, [FromHeader] string seqOriginal)
        {
            try
            {
                _logger.LogInformation($"Obter código de barras de conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                var result = await _faturamentoBusiness.ObterCodigoBarrasNotaFiscalAsync(Convert.ToInt64(seqOriginal));

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_SEGUNDA_VIA,
                        NumeroProtocolo = protocolo.ToString()
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.CodigoBarras);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter código de barras de conta [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("GerarQuitacaoDebito")]
        [ProducesResponseType(typeof(ContaResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GerarQuitacaoDebitoAsync([FromBody] GerarQuitacaoDebitoRequestViewModel viewModel)
        {
            try
            {
                _logger.LogInformation($"Obtendo quitação de débito agência virtual protocolo e seqOriginal [{viewModel.Protocolo}] e [{viewModel.SeqOriginal}].");

                var result = await _agenciaVirtualBusiness.ObterQuitacaoDebitoAsync(viewModel.SeqOriginal, viewModel.Protocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                        NumeroProtocolo = viewModel.Protocolo.ToString()
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter quitação de débito agência virtual protocolo e seqOriginal [{viewModel.Protocolo}] e [{viewModel.SeqOriginal}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("ObterCodigoBarrasQuitacaoDebito")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterCodigoBarrasQuitacaoDebitoAsync([FromHeader] string seqOriginal, [FromHeader] string protocolo)
        {
            try
            {
                _logger.LogInformation($"Obtendo código de barras quitação de débito agência virtual seqOriginal e protocolo [{seqOriginal}] e [{protocolo}].");

                var result = await _agenciaVirtualBusiness.ObterCodigoBarrasQuitacaoDebitoAsync(seqOriginal, protocolo);

                if (result != null)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                        NumeroProtocolo = protocolo
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.CodigoBarras);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter código de barras quitação de débito agência virtual seqOriginal e protocolo [{seqOriginal}] e [{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("ObterPdfQuitacaoDebito")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterPdfQuitacaoDebitoAsync([FromHeader] string seqOriginal, [FromHeader] string protocolo)
        {
            try
            {
                _logger.LogInformation($"Obtendo PDF quitação de débito agência virtual seqOriginal e protocolo [{seqOriginal}] e [{protocolo}].");

                var result = await _agenciaVirtualBusiness.ObterPdfQuitacaoDebitoAsync(seqOriginal, protocolo);

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                        NumeroProtocolo = protocolo
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.EspelhoDaQuitacao);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter PDF quitação de débito agência virtual seqOriginal e protocolo [{seqOriginal}] e [{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("EnviarEmailQuitacaoDebito")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EnviarEmailQuitacaoDebitoAsync([FromHeader] long protocolo, [FromHeader] string seqOriginal, [FromHeader] string email)
        {
            try
            {
                _logger.LogInformation($"Enviar e-mail de quitação de débito [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                var result = await _agenciaVirtualBusiness.EnviarEmailQuitacaoDebitoAsync(seqOriginal, protocolo.ToString(), email);

                if (!result.Sucesso)
                {
                    foreach (var message in result.Mensagens)
                    {
                        _logger.LogError(message);
                    }

                    return BadRequest(result);
                }

                var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                {
                    NaturezaId = ID_NATUREZA_QUITACAO_DEBITO,
                    NumeroProtocolo = protocolo.ToString()
                });

                if (!registrarNaturezaResult.Sucesso)
                {
                    foreach (var message in registrarNaturezaResult.Mensagens)
                    {
                        _logger.LogError(message);
                    }
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new BaseResponseViewModel { Sucesso = false };

                result.AdicionaMensagem($"Erro ao enviar e-mail de quitação de débito [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                _logger.LogCritical(ex, $"Erro ao enviar e-mail de quitação de débito [nº:{seqOriginal}] para protocolo [nº:{protocolo}].");

                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("ObterDeclaracoesAnuais")]
        [ProducesResponseType(typeof(DeclaracoesAnuaisResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterDeclaracoesAnuaisAsync([FromHeader] long protocolo)
        {
            try
            {
                _logger.LogInformation($"Obter declarações anuais para o protocolo [nº:{protocolo}].");

                var result = await _agenciaVirtualBusiness.ObterDeclaracoesAnuaisAsync(protocolo.ToString());

                if (result.Sucesso)
                {
                    return Ok(result.DeclaracoesAnuais.ToList());
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro ao obter declarações anuais para protocolo [nº:{protocolo}].");

                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("ObterPdfDeclaracaoAnual")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ObterPdfDeclaracaoAnualAsync([FromHeader] long protocolo, [FromHeader] string seqDeclaracao)
        {
            try
            {
                _logger.LogInformation($"Gerando PDF da declaração anual seqDeclaracao, protocolo [{seqDeclaracao}], [{protocolo}]");

                var result = await _agenciaVirtualBusiness.ObterPdfDeclaracaoAnualAsync(Convert.ToInt64(seqDeclaracao), protocolo.ToString());

                if (result.Sucesso)
                {
                    var registrarNaturezaResult = await _atendimentoBusiness.RegistrarNaturezaAsync(new RegistrarNaturezaViewModel()
                    {
                        NaturezaId = ID_NATUREZA_EMISSAO_CERTIDAO_NEGATIVA,
                        NumeroProtocolo = protocolo.ToString()
                    });

                    if (!registrarNaturezaResult.Sucesso)
                    {
                        foreach (var message in registrarNaturezaResult.Mensagens)
                        {
                            _logger.LogError(message);
                        }
                    }

                    return Ok(result.EspelhoDaDeclaracaoAnual);
                }

                foreach (var message in result.Mensagens)
                {
                    _logger.LogError(message);
                }

                return BadRequest(string.Join(", ", result.Mensagens));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Obter Pdf declaração anual seqDeclaracao, protocolo [{seqDeclaracao}], [{protocolo}]");

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
