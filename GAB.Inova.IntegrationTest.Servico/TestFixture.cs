﻿using GAB.Inova.Domain.Interfaces.Repositories;
using GAB.Inova.IntegrationTest.Servico.MockHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace GAB.Inova.IntegrationTest.Servico
{
    /// <summary>
    /// Classe acessória
    /// </summary>
    /// <typeparam name="TStartup"></typeparam>
    public class TestFixture<TStartup>
        : IDisposable
    {

        readonly IWebHost _webHost;

        /// <summary>
        /// Provedor dos serviços instaciados
        /// </summary>
        public IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TestFixture()
        {
            _webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(ConfigureTestServices)
                .UseStartup(typeof(TStartup))
                .Build();

            ServiceProvider = _webHost.Services;
        }

        /// <summary>
        /// Configurar a injeção das classes para o serviço
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        protected void ConfigureTestServices(IServiceCollection services)
        {
            var hostingEnvironment = ConfigurationMockHelper.GetHostingEnvironment();

            services.AddSingleton<IWebHostEnvironment>(hostingEnvironment);
            services.AddSingleton<IHttpContextAccessor>(ConfigurationMockHelper.GetHttpContextAccessor());
            services.AddSingleton<IConfiguration>(ConfigurationMockHelper.GetConfiguration(hostingEnvironment));
            //services.AddSingleton<IEmailIntegradorRepository>(EmailIntegradorRepositoryMockHelper.GetEmailIntegradorRepository());
            services.AddSingleton<IOrdemServicoEletronicaControleRepository>(OrdemServicoEletronicaControleRepositoryMockHelper.GetOrdemServicoEletronicaControleRepository()); 
            services.AddSingleton<IOrdemServicoEletronicaRepository>(OrdemServicoEletronicaRepositoryMockHelper.GetOrdemServicoEletronicaRepository());
            services.AddSingleton<IOrdemServicoDetalheRepository>(OrdemServicoDetalheRepositoryMockHelper.GetOrdemServicoDetalheRepository());
            services.AddSingleton<IOrdemServicoAnexoExternoRepository>(OrdemServicoAnexoExternoRepositoryMockHelper.GetOrdemServicoAnexoExternoRepository());
            services.AddSingleton<IMunicipioRepository>(MunicipioRepositoryMockHelper.GetMunicipioRepository());
            services.AddSingleton<IBairroRepository>(BairroRepositoryMockHelper.GetBairroRepository());
            services.AddSingleton<IVwDadosConsultaOrdemServicoRepository>(VwDadosConsultaOrdemServicoRepositoryMockHelper.GetVwDadosConsultaOrdemServicoRepository());
            services.AddSingleton<IProtocoloAtendimentoRepository>(ProtocoloAtendimentoRepositoryMockHelper.GetProtocoloAtendimentoRepository());
            services.AddSingleton<IVwDadosClienteInterrupcaoRepository>(VwDadosClienteInterrupcaoRepositoryMockHelper.GetVwDadosClienteInterrupcaoRepository());
            services.AddSingleton<IInterrupcaoRepository>(InterrupcaoRepositoryMockHelper.GetInterrupcaoRepository());                
        }

        /// <summary>
        /// Implementação da interface IDisposable
        /// </summary>
        public void Dispose()
        {
            _webHost?.Dispose();
        }

    }
}