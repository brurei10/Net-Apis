﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório de controle de ordem de serviço
    /// </summary>
    /// <returns><see cref="IOrdemServicoEletronicaControleRepository"/></returns>
    public static class OrdemServicoEletronicaControleRepositoryMockHelper
    {
        public static IOrdemServicoEletronicaControleRepository GetOrdemServicoEletronicaControleRepository()
        {
            var mockOrdemServicoEletronicaControleRepository = new Mock<IOrdemServicoEletronicaControleRepository>();

            mockOrdemServicoEletronicaControleRepository.Setup(m => m.PodeAlterar(It.IsAny<OrdemServicoEletronicaControle>())).Returns(new ValidationResult());

            return mockOrdemServicoEletronicaControleRepository.Object;
        }
    }
}
