﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório de município
    /// </summary>
    /// <returns><see cref="IMunicipioRepository"/></returns>
    public static class MunicipioRepositoryMockHelper
    {
        public static IMunicipioRepository GetMunicipioRepository()
        {
            var mockMunicipioRepository = new Mock<IMunicipioRepository>();

            mockMunicipioRepository.Setup(m => m.MunicipioValido(It.IsAny<Municipio>())).Returns(new ValidationResult());

            return mockMunicipioRepository.Object;
        }
    }
}
