﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório da ordem de serviço
    /// </summary>
    /// <returns><see cref="IOrdemServicoAnexoExternoRepository"/></returns>
    public static class OrdemServicoAnexoExternoRepositoryMockHelper
    {
        public static IOrdemServicoAnexoExternoRepository GetOrdemServicoAnexoExternoRepository()
        {
            var mockOrdemServicoAnexoExternoRepository = new Mock<IOrdemServicoAnexoExternoRepository>();

            mockOrdemServicoAnexoExternoRepository.Setup(m => m.PodeIncluir(It.IsAny<OrdemServicoAnexoExterno>())).Returns(new ValidationResult());

            return mockOrdemServicoAnexoExternoRepository.Object;
        }
    }
}
