﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view de consulta de ordem de serviço
    /// </summary>
    /// <returns><see cref="IVwDadosConsultaOrdemServicoRepository"/></returns>
    public static class VwDadosConsultaOrdemServicoRepositoryMockHelper
    {
        public static IVwDadosConsultaOrdemServicoRepository GetVwDadosConsultaOrdemServicoRepository()
        {
            var vwDadosConsultaOrdemServicoPrevista = GeraVwDadosConsultaOrdemServicoPrevista();
            var vwDadosConsultaOrdemServicoProgramada = GeraVwDadosConsultaOrdemServicoProgramada();
            var vwDadosConsultaOrdemServicoBaixada = GeraVwDadosConsultaOrdemServicoBaixada();

            var mockVwDadosConsultaOrdemServicoRepository = new Mock<IVwDadosConsultaOrdemServicoRepository>();

            mockVwDadosConsultaOrdemServicoRepository.Setup(m => m.ObterAsync(1)).ReturnsAsync(vwDadosConsultaOrdemServicoPrevista);
            mockVwDadosConsultaOrdemServicoRepository.Setup(m => m.ObterAsync(2)).ReturnsAsync(vwDadosConsultaOrdemServicoProgramada);
            mockVwDadosConsultaOrdemServicoRepository.Setup(m => m.ObterAsync(3)).ReturnsAsync(vwDadosConsultaOrdemServicoBaixada);

            return mockVwDadosConsultaOrdemServicoRepository.Object;
        }

        #region [Metodos privados]

        private static VwDadosConsultaOrdemServico GeraVwDadosConsultaOrdemServicoPrevista()
        {
            return new VwDadosConsultaOrdemServico
            {
                NumeroOs = 1,
                SituacaoOs = 0,
                DescricaoSituacaoOs = "ABERTA",
                DataPrevista = DateTime.Now.AddDays(7),
                Matricula = "0100000004",
                CodigoServico = "167",
                DescricaoServico = "LIGAÇÃO NOVA DE ÁGUA C/ CAIXA PROTETORA PAREDE DN ¾",
                Documento = "07804814887"
            };
        }

        private static VwDadosConsultaOrdemServico GeraVwDadosConsultaOrdemServicoProgramada()
        {
            return new VwDadosConsultaOrdemServico
            {
                NumeroOs = 1,
                SituacaoOs = 0,
                DescricaoSituacaoOs = "ABERTA",
                DataPrevista = DateTime.Now.AddDays(7),
                DataProgramada = DateTime.Now.AddDays(5),
                Matricula = "0100000004",
                CodigoServico = "167",
                DescricaoServico = "LIGAÇÃO NOVA DE ÁGUA C/ CAIXA PROTETORA PAREDE DN ¾",
                Documento = "07804814887"
            };
        }

        private static VwDadosConsultaOrdemServico GeraVwDadosConsultaOrdemServicoBaixada()
        {
            return new VwDadosConsultaOrdemServico
            {
                NumeroOs = 3,
                SituacaoOs = 1,
                DescricaoSituacaoOs = "EXECUTADA",
                DataPrevista = DateTime.Now.AddDays(7),
                DataProgramada = DateTime.Now.AddDays(5),
                DataBaixa = DateTime.Now.AddDays(6),
                Matricula = "0100000004",
                CodigoServico = "167",
                DescricaoServico = "LIGAÇÃO NOVA DE ÁGUA C/ CAIXA PROTETORA PAREDE DN ¾",
                Documento = "07804814887"
            };
        }

        #endregion
    }
}

