﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório da ordem de serviço
    /// </summary>
    /// <returns><see cref="IOrdemServicoEletronicaRepository"/></returns>
    public static class OrdemServicoEletronicaRepositoryMockHelper
    {
        public static IOrdemServicoEletronicaRepository GetOrdemServicoEletronicaRepository()
        {
            var mockOrdemServicoEletronicaRepository = new Mock<IOrdemServicoEletronicaRepository>();

            mockOrdemServicoEletronicaRepository.Setup(m => m.PodeIncluir(It.IsAny<OrdemServicoEletronica>())).Returns(new ValidationResult());

            return mockOrdemServicoEletronicaRepository.Object;
        }
    }
}
