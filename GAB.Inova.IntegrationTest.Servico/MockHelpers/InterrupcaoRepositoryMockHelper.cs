﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;
using System;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view de consulta de ordem de serviço
    /// </summary>
    /// <returns><see cref="IInterrupcaoRepositoryMockHelper"/></returns>
    public static class InterrupcaoRepositoryMockHelper
    {
        public static IInterrupcaoRepository GetInterrupcaoRepository()
        {
            var interrupcao = GeraInterrupcao();

            var mockInterrupcaoRepository = new Mock<IInterrupcaoRepository>();

            mockInterrupcaoRepository.Setup(m => m.ConsultarInterrupcaoAsync(It.IsAny<VwDadosClienteInterrupcao>())).ReturnsAsync(interrupcao);

            return mockInterrupcaoRepository.Object;
        }

        #region [Metodos privados]

        private static Interrupcao GeraInterrupcao()
        {
            return new Interrupcao
            {
                Id = 1682,
                DataHoraInicio = DateTime.Now.AddHours(-2),
                DataHoraFim = DateTime.Now.AddHours(5),
                Observacao = "Bomba do Booster Flora em manutenção, equipes no local, problema eletrico"
            };
        }

        #endregion
    }
}
