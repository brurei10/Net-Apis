﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório de bairro
    /// </summary>
    /// <returns><see cref="IBairroRepository"/></returns>
    public static class BairroRepositoryMockHelper
    {
        public static IBairroRepository GetBairroRepository()
        {
            var mockBairroRepository = new Mock<IBairroRepository>();

            mockBairroRepository.Setup(m => m.BairroValido(It.IsAny<Bairro>())).Returns(new ValidationResult());

            return mockBairroRepository.Object;
        }
    }
}
