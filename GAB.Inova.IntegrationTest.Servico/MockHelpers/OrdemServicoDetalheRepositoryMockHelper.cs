﻿using DomainValidationCore.Validation;
using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório de detalhe da ordem de serviço
    /// </summary>
    /// <returns><see cref="IOrdemServicoDetalheRepository"/></returns>
    public static class OrdemServicoDetalheRepositoryMockHelper
    {
        public static IOrdemServicoDetalheRepository GetOrdemServicoDetalheRepository()
        {
            var mockOrdemServicoDetalheRepository = new Mock<IOrdemServicoDetalheRepository>();

            mockOrdemServicoDetalheRepository.Setup(m => m.PodeAlterarStatusParaAguardandoApropriacao(It.IsAny<OrdemServicoDetalhe>())).Returns(new ValidationResult());

            return mockOrdemServicoDetalheRepository.Object;
        }
    }
}
