﻿using GAB.Inova.Domain;
using GAB.Inova.Domain.Interfaces.Repositories;
using Moq;

namespace GAB.Inova.IntegrationTest.Servico.MockHelpers
{
    /// <summary>
    /// Obter o repositório da view de consulta de ordem de serviço
    /// </summary>
    /// <returns><see cref="IVwDadosClienteInterrupcaoRepository"/></returns>
    public static class VwDadosClienteInterrupcaoRepositoryMockHelper
    {
        public static IVwDadosClienteInterrupcaoRepository GetVwDadosClienteInterrupcaoRepository()
        {
            var vwDadosClienteInterrupcao = GeraVwDadosClienteInterrupcao();

            var mockVwDadosClienteInterrupcaoRepository = new Mock<IVwDadosClienteInterrupcaoRepository>();

            mockVwDadosClienteInterrupcaoRepository.Setup(m => m.ObterAsync(It.IsAny<string>())).ReturnsAsync(vwDadosClienteInterrupcao);

            return mockVwDadosClienteInterrupcaoRepository.Object;
        }

        #region [Metodos privados]

        private static VwDadosClienteInterrupcao GeraVwDadosClienteInterrupcao()
        {
            return new VwDadosClienteInterrupcao
            {
                Matricula = "0100000004",
                NomeCliente = "JOSE GUIRAO SANCHES",
                IdLogradouro = 4262,
                IdBairro = 2141,
                IdCidade = 6672,
                IdSetorOperacional = 46
            };
        }

        #endregion
    }
}
