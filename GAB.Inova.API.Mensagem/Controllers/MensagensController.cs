﻿using GAB.Inova.Business.Interfaces;
using GAB.Inova.Common.ViewModels.Base;
using GAB.Inova.Common.ViewModels.Mensagem;
using GAB.Inova.Common.ViewModels.SendGrid;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GAB.Inova.API.Mensagem.Controllers
{
    /// <summary>
    /// Classe de orquestração ao serviços de Mansagem (SendGrid)
    /// </summary>
    [ApiController]
    [Authorize("Bearer")]
    [Route("api/v1/[controller]/")]
    public class MensagensController : ControllerBase
    {

        readonly ILogger<MensagensController> _logger;
        readonly IMensagemIntegradorBusiness _mensagemIntegradorBusiness;

        /// <summary>
        /// Contrutor da classe
        /// </summary>
        /// <param name="logger">Instância de um objeto para log</param>
        /// <param name="mensagemIntegradorBusiness">Instância do objeto com as regras de negócio referente a Mensagem</param>
        public MensagensController(ILogger<MensagensController> logger, IMensagemIntegradorBusiness mensagemIntegradorBusiness)
        {
            _logger = logger;
            _mensagemIntegradorBusiness = mensagemIntegradorBusiness;
        }

        
        [HttpPost]
        [ProducesResponseType(typeof(EmailResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostAsync(EmailRequestViewModel model)
        {
            try
            {
                _logger.LogInformation($"ENVIO DE MENSAGEM PELO INTEGRADOR EXTERNO - INICIO");

                var result = await _mensagemIntegradorBusiness.EnviarMensagemAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao enviar e-mail pelo integrador: {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"ENVIO DE MENSAGEM PELO INTEGRADOR EXTERNO - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro no envio de mensagem de integrador");

                var result = new BaseResponseViewModel()
                {
                    Sucesso = false
                };

                result.AdicionaMensagem($"Ocorreu um erro inesperado na execução do serviço - {ex.Message}");

                _logger.LogInformation($"ENVIO DE MENSAGEM PELO INTEGRADOR EXTERNO - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }

        /// <summary>
        /// Realizar a atualização de status do e-mail enviado pelo integrador (SendGrid)
        /// </summary>
        /// <param name="model"><see cref="AtualizarStatusMensagemViewModel"/></param>
        /// <returns><see cref="BaseResponseViewModel"/></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("AtualizaStatus")]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BaseResponseViewModel), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AtualizarStatusAsync(AtualizarStatusMensagemViewModel model)
        {
            try
            {
                _logger.LogInformation($"ATUALIZAÇÃO DE STATUS DE MENSAGEM DE INTEGRADOR [ID:{model.MensagemId} STATUS:{model.Event}] - INICIO");

                var result = await _mensagemIntegradorBusiness.AtualizarStatusAsync(model);

                if (!result.Sucesso)
                {
                    _logger.LogError($"Erro(s) ao atualizar o status da mensagem [ID:{model.MensagemId} STATUS:{model.Event}] - {string.Join(';', result.Mensagens)}");
                }

                _logger.LogInformation($"ATUALIZAÇÃO DE STATUS DE MENSAGEM DE INTEGRADOR [ID:{model.MensagemId} STATUS:{model.Event}] - FIM");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Erro inesperado ao atualizar o status da mensagem [ID:{model.MensagemId} STATUS:{model.Event}]");

                var result = new BaseResponseViewModel  { Sucesso = false };

                result.AdicionaMensagem($"Ocorreu um erro inesperado na execução do serviço.  - {ex.Message}");

                _logger.LogInformation($"ATUALIZAÇÃO DE STATUS DE MENSAGEM DE INTEGRADOR [ID:{model.MensagemId} STATUS:{model.Event}] - FIM");

                return StatusCode(StatusCodes.Status500InternalServerError, result);
            }
        }
    }
}
